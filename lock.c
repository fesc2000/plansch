/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#include <pthread.h>

void *taskIdSelf (void)
{
    return (void*)pthread_self();
}

int semTake (sem_t *sem, int wait)
{
    struct timespec ts, delta;
    struct timeval tv;
    int rc = 0;
    uint64_t lwait;

    if ((wait > 0) || (wait == NO_WAIT))
    {
        /* if waiting a specific time, get current time and add the proper offset to get
         * the absolute time required by pthread_cond_timedwait()
         */
        if (wait > 0)
        {
            delta.tv_sec = wait / 100;
            lwait = wait;
            delta.tv_nsec = (((lwait * 1000000000ULL) / 100ULL) % 1000000000ULL);
        }
        else
        {
            /* no wait, use current time as abstime 
             */
            delta.tv_sec = delta.tv_nsec = 0;
        }

        gettimeofday (&tv, NULL);
        ts.tv_sec = tv.tv_sec + delta.tv_sec;
        ts.tv_nsec = tv.tv_usec * 1000 + delta.tv_nsec;
        ts.tv_sec += ts.tv_nsec / 1000000000;
        ts.tv_nsec = ts.tv_nsec % 1000000000;

        wait = 1;
    }
    else
    {
        /* endless wait */
        wait = 0;
    }

    if (sem->isCount)
    {
        /* counting semaphore
        */
        pthread_mutex_lock (&sem->mutex);

        while (sem->count  <= 0)
        {
            if (wait)
            {
                if (pthread_cond_timedwait  (&sem->cond, &sem->mutex, &ts))
                {
                    rc = -1;
                    break;
                }
            }
            else
            {
                pthread_cond_wait  (&sem->cond, &sem->mutex);
            }
        }
        if (rc == 0)
            sem->count--;

        pthread_mutex_unlock (&sem->mutex);
    }
    else if (sem->isMutex)
    {
        /* Recursive Mutex
         */
        if (sem->mutexTakenBy == taskIdSelf())
        {
            /* mutex recursively taken by current thread */
            sem->recurseCount++;
            return 0;
        }

        if (wait)
        {
            if (pthread_cond_timedwait  (&sem->cond, &sem->mutex, &ts))
                rc = -1;
        }
        else
        {
            if (pthread_mutex_lock (&sem->mutex))
                rc = -1;
        }

        if (rc == 0)
        {
            sem->mutexTakenBy = taskIdSelf();
            sem->recurseCount++;
        }
    }
    else /* binary sem. */
    {
        /* binary semaphore. Much like a counting semaphore.
         */
        pthread_mutex_lock (&sem->mutex);

        /* remember how many are waiting here
         */
        sem->waiters ++;

        while (sem->count <= 0)
        {
            if (wait)
            {
                if (pthread_cond_timedwait  (&sem->cond, &sem->mutex, &ts))
                {
                    rc = -1;
                    break;
                }
            }
            else
            {
                if (pthread_cond_wait  (&sem->cond, &sem->mutex))
                {
                    rc = -1;
                    break;
                }
            }
        }

        if (rc == 0)
        {
            sem->count--;
        }

        sem->waiters--;

        pthread_mutex_unlock (&sem->mutex);
    }

    return rc;
}
int semGive (sem_t * sem)
{
    if (sem->isCount)
    {
        pthread_mutex_lock (&sem->mutex);

        sem->count++;

        pthread_cond_signal  (&sem->cond);

        pthread_mutex_unlock (&sem->mutex);
    }
    else if (sem->isMutex)
    {
        if (sem->mutexTakenBy != taskIdSelf())
        {
            fprintf (stderr, "mutex %p release by wrong thread (taken by 0x%p, released by 0x%p)\n",
                    sem, &sem->mutexTakenBy, taskIdSelf());
            BACKTRACE
            return -1;
        }

        if (sem->recurseCount <= 0)
        {
            fprintf (stderr, "mutex %p released but not taken\n",
                    sem);
            return -1;
        }

        sem->recurseCount--;

        if (sem->recurseCount == 0)
        {
            sem->mutexTakenBy = 0;
            pthread_mutex_unlock (&sem->mutex);
        }
    }
    else /* binary */
    {
        pthread_mutex_lock (&sem->mutex);

        sem->count = 1;

        pthread_cond_signal  (&sem->cond);

        pthread_mutex_unlock (&sem->mutex);
    }

    return 0;
}

int semFlush (sem_t * sem)
{
    if (sem->isCount)
    {
        return -1;
    }
    else if (sem->isMutex)
    {
        return -1;
    }
    else /* binary */
    {
        pthread_mutex_lock (&sem->mutex);

        sem->count = sem->waiters;

        pthread_cond_signal  (&sem->cond);

        pthread_mutex_unlock (&sem->mutex);
    }

    return 0;
}

sem_t *  semMCreate(int flags)
{
    sem_t *sem = (sem_t*)malloc(sizeof(sem_t));
    pthread_mutex_t mi = PTHREAD_MUTEX_INITIALIZER;

    memset ((char*)sem,0,  sizeof(*sem));

    sem->isMutex = 1;
    sem->isCount = 0;
    sem->mutex = mi;

    if (pthread_mutex_init (&sem->mutex, NULL))
        return (sem_t *)NULL;

    return (sem_t *)sem;
}

sem_t *  semBCreate(int flags, int state)
{
    sem_t *sem = (sem_t*)malloc(sizeof(sem_t));
    pthread_mutex_t mi = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

    sem->isMutex = 0;
    sem->isCount = 0;
    sem->mutex = mi;
    sem->cond = cond;
    sem->waiters = 0;
    sem->count = state ? 1 : 0;

    if (pthread_mutex_init (&sem->mutex, NULL))
    {
        free (sem);
        return (sem_t *)NULL;
    }

    if (pthread_cond_init (&sem->cond, NULL))
    {
        free (sem);
        return (sem_t *)NULL;
    }

    return (sem_t *)sem;
}

sem_t *  semCCreate(int flags, int state)
{
    sem_t *sem = (sem_t*)malloc(sizeof(sem_t));
    pthread_mutex_t mi = PTHREAD_MUTEX_INITIALIZER;

    sem->isMutex = 0;
    sem->isCount = 1;
    sem->mutex = mi;
    sem->count = state;

    if (pthread_mutex_init (&sem->mutex, NULL))
        return (sem_t *)NULL;

    if (pthread_cond_init (&sem->cond, NULL))
        return (sem_t *)NULL;

    return (sem_t *)sem;
}


