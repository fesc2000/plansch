/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


/* Line/polygon based walls.
 * Wall objects are made up of polygons, each polygon structure refers to a linked list
 * of line objects. 
 * We hold an array of pointers for each screen pixel, referring to the line which 
 * intersects the pixel. This makes collission detection for particles fast, basically
 * we only have to check whether the pixel the particle is at contains a line, calculate
 * the intersection of line and motion vector and do a reflection at the line's normal
 * vector if necessary (plus detection whether the paricle inside/outside the polygon etc.).
 *
 * Problem is, with this approach we must make sure that one pixel has max. on line
 * -> lines can not intersect
 * -> Adjacent lines of the polygon must not intersect within a pixel, only at it's
 *    boundary.
 *
 * Since this restriction would make drawing polygons quite difficult, small temporary
 * lines are added at the intersection pixels if required. These temporary lines
 * provide intersections at the pixel boundaries in the required direction so that the
 * two lines can be connected.
 *
 * TODO:
 * - cutting of polygons when drawing.
 * - hollow objects
 *
 * Note: for performance/effect reasons the particle engines sometimes make particles jump,
 * independently from their actual motion vector. This can cause particles to cross a line
 * and move inside a polygin object. Since this can not be prevented (without major changes to 
 * the particle engines), we can at least make sure that particles inside the object get an
 * impuls to be moved out again.
 * This is accomplished by setting an accellereation vector for all pixels within a object
 * which points to the nearest edge of the object.
 */

#include "plansch.h"
#include <assert.h>

/* controls whether hitting a wall takes a particle's siye into account
 */
#undef HIT_WALL_WITH_SIZE

/* flags */
#define L_INVAL            (1<<0)
#define L_TEMP            (1<<1)
#define L_START_SET (1<<2)
#define L_END_SET   (1<<3)
#define P_LEFT            (1<<4)
#define P_CLOSED    (1<<5)
#define P_MARKED    (1<<6)
#define L_OVERLAP   (1<<8)
#define P_CHANGED   (1<<9)
#define L_VISITED   (1<<10)

#define L_COMPLETE(l)        (((l)->flags & (L_START_SET|L_END_SET)) == (L_START_SET|L_END_SET))

#define SET_INVAL(l,i)        {if (i){(l)->flags |= L_INVAL;}else{(l)->flags &= ~L_INVAL;}}
#define IS_INVAL(l)        ((((l)->flags  & L_INVAL) == 0) ? 0 : 1)

#define SET_OVERLAP(l,i)        {if (i){(l)->flags |= L_OVERLAP;}else{(l)->flags &= ~L_OVERLAP;}}
#define IS_OVERLAP(l)        ((((l)->flags  & L_OVERLAP) == 0) ? 0 : 1)

#define SET_TEMP(l,i)        {if (i){(l)->flags |= L_TEMP;}else{(l)->flags &= ~L_TEMP;}}
#define IS_TEMP(l)        ((((l)->flags  & L_TEMP) == 0) ? 0 : 1)

#define SET_LEFT(l,i)        {if (i){(l)->flags |= P_LEFT;}else{(l)->flags &= ~P_LEFT;}}
#define IS_LEFT(l)        ((((l)->flags  & P_LEFT) == 0) ? 0 : 1)

#define SET_CLOSED(l,i)        {if (i){(l)->flags |= P_CLOSED;}else{(l)->flags &= ~P_CLOSED;}}
#define IS_CLOSED(l)        ((((l)->flags  & P_CLOSED) == 0) ? 0 : 1)

#define SET_MARKED(l,i)        {if (i){(l)->flags |= P_MARKED;}else{(l)->flags &= ~P_MARKED;}}
#define IS_MARKED(l)        ((((l)->flags  & P_MARKED) == 0) ? 0 : 1)

#define SET_CHANGED(l,i)        {if (i){(l)->flags |= P_CHANGED;}else{(l)->flags &= ~P_CHANGED;}}
#define IS_CHANGED(l)        ((((l)->flags  & P_CHANGED) == 0) ? 0 : 1)

#define LDX(l) ((l)->u.line.vec.d.x)
#define LDY(l) ((l)->u.line.vec.d.y)

#define BLEFT_I            0
#define BTOP_I            1
#define BBOT_I            2
#define BRIGHT_I    3

#define BLEFT        (1<<BLEFT_I)
#define BTOP        (1<<BTOP_I)
#define BBOT        (1<<BBOT_I)
#define BRIGHT        (1<<BRIGHT_I)

#define O2VEC(I,O)   lineBuffers[I][O]
#define S2VEC(I,X,Y)   lineBuffers[I][(X) + (Y) * wid]
#define C2VEC(I,P)    lineBuffers[I][SCREENC((P).d.x) + SCREENC((P).d.y) * wid]

#define IS_FIRST(L) ((L) == (L)->poly->u.poly.lines)
#define IS_LAST(L)  (((L)->next == NULL) || ((L)->next == (L)->poly->u.poly.lines))
#define GET_NEXT(L) (IS_LAST(L) ? NULL : (L)->next)

#define DYDX_MAX    1e12

static int cutMode = 0;

#if  (DEBUG == 1)
int cdebug = 0;
#define CPRINTF if (cdebug) printf
#define CDEBUG(v) cdebug = v;
#else
#define CPRINTF(...)
#define CDEBUG(v) 
#endif

static const float _ps_red[4] __attribute__((aligned(16))) = { 1.0, 0, 0, 0 };

void vecCutMode (int m)
{
    cutMode = m;
}

typedef enum
{
    e_deleted = 0,
    e_poly,
    e_line,
    e_point
} etype_t;

typedef struct
{
    vec_t pos, cpos;
    struct element *p;
} cp_t;

/* A polygon or line structure. Polygons are kept in a single linked list,
 * lines are in a circular double linked list.
 */
typedef struct element
{
    struct element *next, *prev;
    uint            flags;
    struct element *poly;   /* pointer to poly (lines) or self (poly) */
    etype_t etype;

    vec_t *pixels;
    int nPixels;
    int maxPixels;

    union
    {
        struct 
        {
            struct element *lines;

            /* bounding box */
            vec_t b1, b2;
            int nLines;
            int buffer; /* line buffer housing the poly */
            struct element *nextIntersected;

            /* pointer to the screen object which draws the
             * polygon
             */
            scrPoly *se;
        } poly;

        line_t line;

        struct point
        {
            /* screen point also contains a line connection the two connection points
             */
            vec_t        start, end;

            float dyDx;    /* dy/dx */
            vec_t vec, normal;
            int intersections;


            /* position in screen coordinates
             */
            vec_t pos;

            /* the 4 connection points
             */
            cp_t cp[4];

            /* connection count (max. 2)
             */
            int connections;

            /* mask with used connection points
             */
            int connected;

        } point;
    } u;

} vecElement;

typedef struct
{
    unsigned char o1;
    unsigned char o2;
} copt_t;

typedef struct
{
    vecElement *l1, *l2;
    vec_t pos;
    uint flag;
} lineIntersection;

/* linked list of polygons */
static vecElement *polyList;

/* free list */
static vecElement *freeLst = NULL;

/* line array (2*wid*hei). Associates a line to each pixel in siArray */
static vecElement **lineArr;

/* three line buffers: index 0 is the main buffer holding the visible polygons (
 * (points to lineBuffer[0]).
 * index 1 holds information about the polygon being drawn (points to lineBuffer[wid*hei])
 * index 3 holds temporary polygon when mwerging polygons
 */
static vecElement **lineBuffers[3];


/* a list holding all intersected lines
 */
static int intersectionListSize = 1000;
static int numIntersections = 0;
static lineIntersection *intersectionList = NULL;
static int doCaptureIntersections = 0;

static int polyRedraw = 0;

/* the poly currently being drawn */
static vecElement *curPoly = NULL;

/* the current point */
static vecElement *curPoint = NULL;
static vecElement *startPoint = NULL;

static void allocFreeList(void);
static void clearElemMarkers (vecElement *l);
static void lineFinish (vecElement *line);
static int linePixelIntersect (vecElement *l, int sx, int sy, vec_t *p1, vec_t *p2);
static void setLineEnd (vecElement *l, int ix, int iy, int noExtend);
void polyToScr (vecElement *poly);
void doFillPoly (vecElement *poly);
void polySetNormals (vecElement *poly);
int lineIntersect (vecElement *l, vec_t *vStart, vec_t *vec, vec_t *iCord, int noEndCheck);
void finishPoint (vecElement *p);
void assignPointToElem (vecElement *e, vec_t *pos);
void disconnectPointWithNext (vecElement *e);
vecElement *nearestLine (vecElement *poly, int ix, int iy, vec_t *v);

int getLine (int off, line_t *line)
{
    vecElement *ve, *l;
    vec_t v;
    int x, y;
    float ll;

    if ((off < 0) || (off >= wid*hei) || (line == NULL))
        return 0;

    ve = O2VEC(0, off);

    if (!ve)
        return 0;

    if (siArray[off].flags & SI_IN_POLY)
    {
        x = INTERNC_M(off % wid);
        y = INTERNC_M(off / wid);

        l = nearestLine (ve->poly, x, y, &v);
        if (!l)
            return 0;

        line->start.d.x = x + v.d.x;
        line->start.d.y = y + v.d.y;
        line->end.d.x = line->start.d.x + v.d.y;
        line->end.d.y = line->start.d.y + v.d.x;
        line->end.d.x = line->start.d.x + v.d.y;
        line->end.d.y = line->start.d.y + v.d.x;

        ll = VEC_LEN(v.d.x, v.d.y) / (float)NORM_LEN;
        line->normal.d.x = (int)((float)v.d.x / ll);
        line->normal.d.y = (int)((float)v.d.y / ll);

        line->dyDx = (v.d.x != 0) ? ((float)v.d.y / (float)v.d.x) : 1e-10f;

        return 1;
    }

    *line = ve->u.line;

    return 1;
}

vecElement *prevPoint (vecElement *p)
{
    vecElement *rc;

    assert (p);
    assert ((p->etype == e_line) || (p->etype == e_point));

    rc = p->prev;
    if (rc->etype == e_point)
        return rc;

    rc = rc->prev;
    assert (rc->etype == e_point);

    return rc;
}

vecElement *nextPoint (vecElement *p)
{
    vecElement *rc;

    assert (p);
    assert ((p->etype == e_line) || (p->etype == e_point));

    rc = p->next;
    if (rc->etype == e_point)
        return rc;

    rc = rc->next;
    assert (rc->etype == e_point);

    return rc;
}

vecElement *nextLine (vecElement *e)
{
    if (e->etype == e_point)
    {
        if (e->next->etype == e_line)
            return e->next;
        return NULL;
    }

    assert (e->next->etype == e_point);

    if (e->next->next->etype == e_line)
        return e->next->next;

    return NULL;
}

vecElement *prevLine (vecElement *e)
{
    if (e->etype == e_point)
    {
        if (e->prev->etype == e_line)
            return e->prev;
        return NULL;
    }

    assert (e->prev->etype == e_point);

    if (e->prev->prev->etype == e_line)
        return e->prev->prev;

    return NULL;
}

static void pointNormalNext (vecElement *p, vec_t *normal)
{
    vecElement *np = nextPoint (p);
    int dx, dy;
    float vl;

    assert (p->etype == e_point);

    dx = np->u.point.pos.d.x - p->u.point.pos.d.x;
    dy = np->u.point.pos.d.y - p->u.point.pos.d.y;
    vl = VEC_LEN(dx, dy);

    if (IS_LEFT(p->poly))
    {
        normal->d.x = -(((float)dy * (float)NORM_LEN) / vl);
        normal->d.y =  (((float)dx * (float)NORM_LEN) / vl);
    }
    else
    {
        normal->d.x =  (((float)dy * (float)NORM_LEN) / vl);
        normal->d.y = -(((float)dx * (float)NORM_LEN) / vl);
    }
}

static void pointLineToNext (vecElement *p, vec_t *v)
{
    vecElement *np = nextPoint (p);

    assert (p->etype == e_point);

    v->d.x = INTERNC_M(np->u.point.pos.d.x) - INTERNC_M(p->u.point.pos.d.x);
    v->d.y = INTERNC_M(np->u.point.pos.d.y) - INTERNC_M(p->u.point.pos.d.y);
}
static void pointLineToPrev (vecElement *p, vec_t *v)
{
    vecElement *pp = prevPoint (p);

    assert (p->etype == e_point);

    v->d.x = INTERNC_M(pp->u.point.pos.d.x) - INTERNC_M(p->u.point.pos.d.x);
    v->d.y = INTERNC_M(pp->u.point.pos.d.y) - INTERNC_M(p->u.point.pos.d.y);
}


/* check whether x/y is on the side of line l where l's normal points to
 */
int pointIsOutside (vecElement *l, vec_t *v)
{
    vec_t dv;

    dv.d.x = v->d.x - l->u.line.start.d.x;
    dv.d.y = v->d.y - l->u.line.start.d.y;

    return SAME_DIR(dv, l->u.line.normal);
}

static int polyInvalid (vecElement *poly)
{
    vecElement *line;

    for (line = poly->u.poly.lines; line != NULL; line = GET_NEXT(line))
    {
        if (IS_INVAL(line))
            return 1;
    }
    return 0;
}


/* define/update the screen object associated to a polygon 
 */
static void polyUpdateScrObject (vecElement *poly)
{
    int flags;
    int col;
    vecElement *l;

    if (poly->u.poly.se == NULL)
    {
        flags = 0;
        if (poly == curPoly)
        {
            /* dont fill the current poly.
             */
            col = COL_LINE;
        }
        else
        {
            flags = POLY_FILLED;
            col = COL_BRICK;
        }

        poly->u.poly.se = polyNew (10000, flags, col);

        if (!poly->u.poly.se)
            return;

        SET_CHANGED(poly, 1);
    }

    if (!IS_CHANGED(poly))
    {
        return;
    }

    polyHide (poly->u.poly.se);
    polyClear (poly->u.poly.se);

    for (l = poly->u.poly.lines; l != NULL; l = GET_NEXT(l))
    {
        col = (IS_TEMP(l) ? COL_BLUE : COL_WPBORDER);

        if (IS_INVAL(l))
            col = COL_WPBORDERI;

        polyAddCoord (poly->u.poly.se, l->u.line.start.d.x, l->u.line.start.d.y, col);
    }

    polyShow (poly->u.poly.se);
}

void clearLineBuffer(int buffer)
{
    memset (lineBuffers[buffer], 0, wid * hei * sizeof(void*));
}

/* return a flag set consisting of up, down,left and right for a given vector,
 * indicating it's direction. 
 */
static inline int vecDir (const vec_t *vec)
{
    int rc = 0;

    if (vec->d.x > 0)
        rc = BRIGHT;
    else if (vec->d.x < 0)
        rc = BLEFT;

    if (vec->d.y > 0)
        rc |= BBOT;
    else if (vec->d.y < 0)
        rc |= BTOP;

    return rc;
}

static vecElement *getElem (etype_t etype)
{
    vecElement *rc;

    allocFreeList();

    rc = freeLst;

    if (!rc)
    {
        return NULL;
    }

    freeLst = rc->next;

    memset (rc, 0, sizeof(*rc));

    rc->poly = rc;
    rc->etype = etype;

    return rc;
}

static void freeElem (vecElement *e)
{
    assert (e->etype != e_deleted);

    e->next = freeLst;

    freeLst = e;

    e->etype = e_deleted;
    if (e->pixels)
    {
        clearElemMarkers (e);
        free (e->pixels);
    }

    e->pixels = NULL;
    e->nPixels = e->maxPixels = 0;
}

#define INIT_FREE_LIST 10000

static void allocFreeList(void)
{
    int i;
    size_t rmem;
    vecElement *vm;

    if (freeLst)
        return;

    rmem = INIT_FREE_LIST * sizeof (vecElement);
    vm = malloc (rmem);

    if (vm == NULL)
    {
        printf ("linesInit: failed to alloc. 0x%x bytes\n", (int)rmem);
        return;
    }

    for (i = 0; i < INIT_FREE_LIST-1; i++)
    {
        vm[i].next = &vm[i+1];
        vm[i].etype = e_deleted;
    }
    vm[i].next = NULL;
    vm[i].etype = e_deleted;

    freeLst = vm;
}

void linesInit()
{
    uint maxElem = wid * hei;
    size_t rmem;
    static int initialized = 0;

    if (initialized)
        return;

    initialized = 1;

    allocFreeList();

    rmem = maxElem * sizeof(vecElement*) * 3;
    lineArr = malloc (rmem);

    if (lineArr == NULL)
    {
        printf ("linesInit: failed to alloc. 0x%x bytes\n", (int)rmem);
        return;
    }

    memset (lineArr, 0, rmem);
    lineBuffers[0] = lineArr;
    lineBuffers[1] = &lineArr[maxElem];
    lineBuffers[2] = &lineArr[2*maxElem];

}


/* line l1 intersects with line l2
 */
int addIntersected (vecElement *l1, vecElement *l2, int x, int y)
{
    vecElement *e;
    vecElement *poly = l1->poly;
    vecElement *intersectedPoly = l2->poly;
    int i;

    assert (intersectedPoly != poly);

    if (!doCaptureIntersections)
        return 0;

    if ((l1->etype == e_poly) || (l2->etype == e_poly))
        return 1;

    if (intersectionList == NULL)
    {
        intersectionList = (lineIntersection*)malloc(intersectionListSize * sizeof(*intersectionList));
        numIntersections = 0;
    }

    if (numIntersections >= intersectionListSize)
    {
        intersectionListSize += 1000;
        intersectionList = (lineIntersection*)realloc(intersectionList, intersectionListSize * sizeof(*intersectionList));
    }

    /* two lines can intersect only once
     */
    for (i = 0; i < numIntersections; i++)
    {
        if (((intersectionList[i].l1 == l1) && (intersectionList[i].l2 == l2)) ||
            ((intersectionList[i].l2 == l1) && (intersectionList[i].l1 == l2)))
        {
            return 1;
        }

#if 0
        if ((SCREENC(intersectionList[i].pos.d.x) == x)  &&
            (SCREENC(intersectionList[i].pos.d.y) == y))
        {
            printf ("skipped double intersection on %d/%d\n", x, y);
            return;
        }
#endif

    }

    intersectionList[numIntersections].pos.d.x = INTERNC_M(x);
    intersectionList[numIntersections].pos.d.y = INTERNC_M(y);

    DBG_MARKER (intersectionList[numIntersections].pos.d.x, intersectionList[numIntersections].pos.d.y, GRID/3, 0xe000ff00);

    intersectionList[numIntersections].l1 = l1;
    intersectionList[numIntersections].l2 = l2;
    intersectionList[numIntersections].flag = 0;

    l1->u.line.intersections++;
    l2->u.line.intersections++;

    numIntersections++;

    /* add 2nd vecElement's poly to list of polys whith which poly of l1 intersects
    */
    for (e = poly->u.poly.nextIntersected; e != NULL; e = e->u.poly.nextIntersected)
        if (e == intersectedPoly)
            break;

    if (e == NULL)
    {
        intersectedPoly->u.poly.nextIntersected = poly->u.poly.nextIntersected;
        poly->u.poly.nextIntersected = intersectedPoly;
    }

    return 0;
}

int removeIntersection (vecElement *l1, vecElement *l2)
{
    int i;

    for (i = 0; i < numIntersections; i++)
    {
        if (((intersectionList[i].l1 == l1) && (intersectionList[i].l2 == l2)) ||
            ((intersectionList[i].l2 == l1) && (intersectionList[i].l1 == l2)))
        {
            intersectionList[i] = intersectionList[numIntersections-1];
            numIntersections--;
            return 1;
        }
    }
    return 0;
}

void cleanupIntersectionList (void)
{
    int i;
    vecElement *l1, *l2;

    for (i = numIntersections-1; i >= 0; i--)
    {
        l1 = intersectionList[i].l1;
        l2 = intersectionList[i].l2;

        if ((l1->etype == e_point) && (l2->etype == e_line))
        {
            removeIntersection (prevLine (l1), l2);
            removeIntersection (nextLine (l1), l2);
        }
        else if ((l2->etype == e_point) && (l1->etype == e_line))
        {
            removeIntersection (prevLine (l2), l1);
            removeIntersection (nextLine (l2), l1);
        }
        else if ((l1->etype == e_point) && (l2->etype == e_point))
        {
            removeIntersection (prevLine (l1), prevLine(l2));
            removeIntersection (prevLine (l1), nextLine(l2));
            removeIntersection (nextLine (l1), prevLine(l2));
            removeIntersection (nextLine (l1), nextLine(l2));
        }

        if (i > numIntersections)
            i = numIntersections;
    }

    for (i = 0; i < numIntersections; i++)
        DBG_MARKER (intersectionList[i].pos.d.x, intersectionList[i].pos.d.y, GRID/4, 0xe000ffff);
}


void clearIntersectionList()
{
    int i;

    for (i = 0; i < numIntersections; i++)
    {
        intersectionList[i].l1->u.line.intersections = 0;
        intersectionList[i].l2->u.line.intersections = 0;
    }
    numIntersections = 0;
}

void captureIntersections (int ena)
{
    if (ena)
        clearIntersectionList();
    doCaptureIntersections = ena;
}

typedef struct
{
    vec_t p1, p2;
} pair_t;

#define MAXP 2*wid

/* get all pixels intersected by a line into the points array
*/
int makeLine (vecElement *l, vec_t *points, int maxPoints)
{
    pair_t *pairs = alloca(10000 * sizeof(pair_t));
    int numPairs = 1;
    vec_t sp1, sp2, spn, spns;
    pair_t cp;
    int np = 0;

    pairs[0].p1 = l->u.line.start;
    pairs[0].p2 = l->u.line.end;

    points[np].d.x = SCREENC(l->u.line.start.d.x);
    points[np].d.y = SCREENC(l->u.line.start.d.y);
    np++;
    points[np].d.x = SCREENC(l->u.line.end.d.x);
    points[np].d.y = SCREENC(l->u.line.end.d.y);
    np++;

    while (numPairs > 0)
    {

        numPairs--;
        cp = pairs[numPairs];

        sp1.d.x = SCREENC(cp.p1.d.x);
        sp1.d.y = SCREENC(cp.p1.d.y);
        sp2.d.x = SCREENC(cp.p2.d.x);
        sp2.d.y = SCREENC(cp.p2.d.y);

        spn.d.x = (cp.p1.d.x + cp.p2.d.x) / 2;
        spn.d.y = (cp.p1.d.y + cp.p2.d.y) / 2;
        spns.d.x = SCREENC(spn.d.x);
        spns.d.y = SCREENC(spn.d.y);

        if (((spns.d.x != sp1.d.x) || (spns.d.y != sp1.d.y)) &&
            ((spns.d.x != sp2.d.x) || (spns.d.y != sp2.d.y)))
        {
            pairs[numPairs].p1 = cp.p1;
            pairs[numPairs].p2 = spn;
            numPairs++;

            pairs[numPairs].p1 = cp.p2;
            pairs[numPairs].p2 = spn;
            numPairs++;

            if (np >= MAXP)
            {
                printf ("too many points in line\n");
                np = 0;
                break;
            }

            points[np++] = spns;
        }
        else
        {
            if ((sp1.d.x != sp2.d.x) && (sp1.d.y != sp2.d.y))
            {
                /* two points are adjacent, but vertically. We need to check which
                 * of the two remaining pixels is intersected by the line
                 */
                vec_t v;
                int b;

                v.d.x = sp2.d.x - sp1.d.x;
                v.d.y = sp2.d.y - sp1.d.y;

                b = vecDir (&v);

                if ((b & BLEFT) && linePixelIntersect (l, sp1.d.x - 1, sp1.d.y, NULL, NULL))
                {
                    points[np].d.x = sp1.d.x - 1;
                    points[np].d.y = sp1.d.y;
                    np++;
                }
                if ((b & BRIGHT) && linePixelIntersect (l, sp1.d.x + 1, sp1.d.y, NULL, NULL))
                {
                    points[np].d.x = sp1.d.x + 1;
                    points[np].d.y = sp1.d.y;
                    np++;
                }
                if ((b & BTOP) && linePixelIntersect (l, sp1.d.x, sp1.d.y - 1, NULL, NULL))
                {
                    points[np].d.x = sp1.d.x;
                    points[np].d.y = sp1.d.y - 1;
                    np++;
                }
                if ((b & BBOT) && linePixelIntersect (l, sp1.d.x, sp1.d.y + 1, NULL, NULL))
                {
                    points[np].d.x = sp1.d.x;
                    points[np].d.y = sp1.d.y + 1;
                    np++;
                }
            }
        }
    }

    return np;
}

static int setLine (vecElement *l)
{
    vec_t *points = alloca (MAXP * sizeof(vec_t));
    int np;
    vec_t *current;
    int overlap = 0;
    int inval = 0;
    vecElement *currentLine, *currentTmpLine;
    int buffer = l->poly->u.poly.buffer;
    int i;
    vecElement *pStart = NULL, *pEnd = NULL;
    int isLine = (l->etype == e_line);

    if (!L_COMPLETE(l))
    {
        printf ("Line not complete\n");
        SET_INVAL (l, 1);
        return 1;
    }

    if (isLine)
    {
        pStart = l->prev;
        pEnd = l->next;

        assert(pStart->etype == e_point);
        assert(pEnd->etype == e_point);

        np = makeLine (l, points, MAXP);
    }
    else
    {
        np = 1;

        points[0] = l->u.point.pos;
    }

    if (np == 0)
    {
        printf ("Line too short ..\n");
        SET_INVAL (l, 1);
        return 1;
    }

    for (i = 0; i < np; i++)
    {
        current = &points[i];
        currentLine = S2VEC(0, current->d.x, current->d.y);
        currentTmpLine = S2VEC(buffer, current->d.x, current->d.y);

        if (isLine &&
            ((current->ll == pStart->u.point.pos.ll) ||
             (current->ll == pEnd->u.point.pos.ll)))
        {
            continue;
        }

        if (isLine && currentTmpLine && (currentTmpLine != l))
        {
            /* current pixel already has a different line of same polygon */
            inval = 1;

            DBG_MARKER_N(INTERNC_M(current->d.x), INTERNC_M(current->d.y), GRID/4, 0xff0000ff, "ELINE");

            DBG_LINE(INTERNC_M(current->d.x), INTERNC_M(current->d.y), l->u.line.start.d.x, l->u.line.start.d.y, 
                0xff0000ff,
                "+l");
            DBG_LINE(INTERNC_M(current->d.x), INTERNC_M(current->d.y), currentTmpLine->u.line.start.d.x, currentTmpLine->u.line.start.d.y, 
                0xff0000ff,
                "+l");
            continue;
        }

        S2VEC(buffer, current->d.x, current->d.y) = l;
        assignPointToElem (l, current);

        if (currentLine != NULL)
        {
            /* line intersects with different polygon */
            overlap = 1;
            addIntersected (l, currentLine, current->d.x, current->d.y);
        }
    }

    SET_INVAL(l, (inval));
//    SET_INVAL(l, (inval|overlap));
    SET_OVERLAP(l, overlap);

    return inval;
}


static void setPoly (vecElement *poly)
{
    vecElement *l;

    poly->u.poly.nextIntersected = NULL;

    for (l = poly->u.poly.lines; l != NULL; l = GET_NEXT(l))
        setLine (l);
}

static vecElement *addElemAfter (vecElement *poly, vecElement *line, etype_t etype)
{
    vecElement *l = getElem(etype);

    if (line == NULL)
    {
        l->next = poly->u.poly.lines;
        if (l->next)
        {
            l->prev = l->next->prev;
            l->next->prev = l;
        }
        else
        {
            l->next = l;
            l->prev = l;
        }
        poly->u.poly.lines = l;
    }
    else
    {
        l->next = line->next;
        if (line->next)
        {
            line->next->prev = l;

            line->next = l;
            l->prev = line;
        }
        else
        {
            printf ("XXX polygon error\n");
            l->prev = line;
            l->next = line;
        }
    }
    l->poly = poly;

    poly->u.poly.nLines++;

    return l;
}

/* remove <line> and return it's successor */
static vecElement *removeLine (vecElement *line)
{
    vecElement *poly = line->poly;
    vecElement *succ = line->next;
    vecElement *prev = line->prev;

    assert (line->etype == e_line);
    assert (succ->etype == e_point);
    assert (prev->etype == e_point);

    prev->next = succ;
    succ->prev = prev;

    if (line == poly->u.poly.lines)
        poly->u.poly.lines = succ;

    line->poly->u.poly.nLines--;
    freeElem (line);

    return succ;
}

/* remove <point> and return it's successor */
static vecElement *removePoint (vecElement *p)
{
    vecElement *poly = p->poly;
    vecElement *succ = p->next;
    vecElement *prev = p->prev;

    assert (p->etype == e_point);
    assert (succ->etype == e_point);
    assert (prev->etype == e_point);

    prev->next = succ;
    succ->prev = prev;

    if (p == poly->u.poly.lines)
        poly->u.poly.lines = succ;

    p->poly->u.poly.nLines--;
    freeElem (p);

    return succ;
}

static void updatePolyBb (vecElement *poly)
{
    int minx = 0x7fffffff;
    int miny = 0x7fffffff;
    int maxx = 0;
    int maxy = 0;
    vecElement *l = poly->u.poly.lines;

    if (l == NULL)
        return;

    while (l != NULL)
    {
        minx = MIN(minx, MIN(l->u.line.start.d.x, l->u.line.end.d.x));
        miny = MIN(miny, MIN(l->u.line.start.d.y, l->u.line.end.d.y));
        maxx = MAX(maxx, MAX(l->u.line.start.d.x, l->u.line.end.d.x));
        maxy = MAX(maxy, MAX(l->u.line.start.d.y, l->u.line.end.d.y));

        l = GET_NEXT(l);
    }

    poly->u.poly.b1.d.x = SCREENC(minx);
    poly->u.poly.b1.d.y = SCREENC(miny);
    poly->u.poly.b2.d.x = SCREENC(maxx);
    poly->u.poly.b2.d.y = SCREENC(maxy);
}

static void setLineStart (vecElement *l, int ix, int iy)
{
    if ((ix == l->u.line.start.d.x) && (iy == l->u.line.start.d.y))
        return;

    clearElemMarkers (l);

    l->u.line.start.d.x = ix;
    l->u.line.start.d.y = iy;
    l->flags |= L_START_SET;

    if (L_COMPLETE(l))
    {
        lineFinish (l);
        setLine (l);
        updatePolyBb (l->poly);
        polySetNormals (l->poly);

        SET_CHANGED(l->poly, 1);
    }
}

static void setLineEnd (vecElement *l, int ix, int iy, int noExtend)
{
    vec_t v;

#if 0
    if ((l->flags & L_START_SET) == 0)
    {
        printf ("line %p: no start set\n", l);
        return;
    }
#endif

#if 0
    /* do not allow vertical lines */
    if (ix == l->u.line.start.d.x)
        ix = ix ^ 1;
#endif

    if ((ix == l->u.line.end.d.x) && (iy == l->u.line.end.d.y))
        return;

    clearElemMarkers (l);

    l->u.line.end.d.x = ix;
    l->u.line.end.d.y = iy;

    if ((l->u.line.end.d.x == l->u.line.start.d.x) && 
        (l->u.line.end.d.y == l->u.line.start.d.y))
    {
        printf ("%p: end set to start\n", l);
        BACKTRACE
        l->flags &= ~L_END_SET;
        return;
    }


    l->flags |= L_END_SET;

    if (!noExtend)
    {
        /* extend line until it hits the border of the end pixel */
        v.d.x = l->u.line.end.d.x - l->u.line.start.d.x;
        v.d.y = l->u.line.end.d.y - l->u.line.start.d.y;
        toBorder (&l->u.line.end, &v);
    }

    if (L_COMPLETE(l))
    {
        lineFinish (l);
        setLine (l);
        updatePolyBb (l->poly);
        polySetNormals (l->poly);

        SET_CHANGED(l->poly, 1);
    }
}

void assignPointToElem (vecElement *e, vec_t *pos)
{
    int allocSize;
    assert (e);
    assert (pos);

    allocSize = (e->etype == e_point) ? 5 : 200;

    if (e->pixels == NULL)
    {
        e->pixels = (vec_t*)malloc (sizeof(vec_t) * allocSize);
        e->maxPixels = allocSize;
        e->nPixels = 0;
    }
    else if (e->nPixels >= e->maxPixels)
    {
        e->maxPixels += allocSize;
        e->pixels = (vec_t*)realloc ((void*)e->pixels, sizeof(vec_t) * e->maxPixels);
    }

    assert(e->pixels);
    assert(e->nPixels < e->maxPixels);

    e->pixels[e->nPixels] = *pos;
    e->nPixels++;

    if (e->etype != e_poly)
    {
        assignPointToElem (e->poly, pos);
    }
}

void checkBuffers (vecElement *e, int mustBeClear)
{
    int x, y;
    int b;
    vecElement *ne;

    for (x = 0; x < wid; x++)
    {
        for (y = 0; y < hei; y++)
        {
            for (b = 0; b < 3; b++)
            {
                ne = S2VEC(b, x, y);

                if (ne == e)
                {
                    if (mustBeClear)
                    {
                        printf ("ERROR: buffer %d has element %p\n", b, e);
                        DBG_MARKER_N(INTERNC_M(x), INTERNC_M(y), GRID/4, 0xff0000ff, "+tmp");
                    }
                }
            }
        }
    }
}

static void clearElemMarkers (vecElement *l)
{
    int buffer;
    int i;
    screeninfo *pSi;
    vec_t *pos;
    vecElement *e;

    if (l->pixels == NULL)
        return;

    for (i = 0, pos = l->pixels; i < l->nPixels; i++, pos++)
    {
        for (buffer = 0; buffer < 3; buffer++)
        {
            e = S2VEC(buffer, pos->d.x, pos->d.y);

            if (e && ((l == e) || (e == l->poly)))
            {
                S2VEC(buffer,pos->d.x, pos->d.y) = NULL;

                if (buffer == 0)
                {
                    pSi = &siArray[pos->d.x + pos->d.y*wid];
                    
                    pSi->flags &= ~SI_POLY_MASK;

                    if (l->etype == e_poly)
                    {
                        pSi->normals[0] = 0;
                        pSi->normals[1] = 0;
                        XACCEL(pSi) = gravVec.d.x;
                        YACCEL(pSi) = gravVec.d.y;
                    }
                }
            }
        }
    }

    l->pixels = 0;
}

static void freePoly (vecElement *poly)
{
    vecElement *line, *next, *p;

    if (poly->etype != e_poly)
        return;

    /* unlink from poly list
     */
    for (p = polyList; p != NULL; p = p->next)
    {
        if (p == poly)
        {
            polyList = poly->next;
            break;
        }
        else if (p->next == poly)
        {
            p->next = poly->next;
            break;
        }
    }

//    if (p == NULL)
//        return;

    for (line = poly->u.poly.lines; line != NULL; line = next)
    {
        assert ((line->etype == e_line) || (line->etype == e_point));
        assert (line->poly == poly);

        next = GET_NEXT(line);
        freeElem (line);
    }

    if (poly->u.poly.se)
    {
        polyDelete (poly->u.poly.se);
    }

    freeElem (poly);
}

static void deletePoly (vecElement *poly)
{
    if (poly->etype != e_poly)
        return;

    if (poly->u.poly.se)
    {
        polyHide (poly->u.poly.se);
    }

    freePoly (poly);
}

vecElement *pointPosUsed (vecElement *poly, int sx, int sy)
{
    vecElement *e;
    for (e = poly->u.poly.lines; e != NULL; e = GET_NEXT(e))
    {
        if ((e->etype == e_point) &&
            (e->u.point.pos.d.x == sx) && 
            (e->u.point.pos.d.y == sy))
        {
            return e;
        }
    }
    return NULL;
}


void setPointPos (vecElement *p, int sx, int sy)
{
    vecElement *e;

    if (sx < 0)
        sx = 0;
    else if (sx > wid-1)
        sx = wid-1;

    if (sy < 0)
        sy = 0;
    else if (sy > hei-1)
        sy = hei-1;

    e = pointPosUsed (p->poly, sx, sy);
    if (e && (e != p))
        return;

    p->u.point.pos.d.x = sx;
    p->u.point.pos.d.y = sy;

    /* the 4 connection points (left, top, bottom, right)
     * The top/bottom points are shifted by 1 pixel to avoid vertical lines
     */
    p->u.point.cp[0].pos.d.x = INTERNC(sx);
    p->u.point.cp[0].pos.d.y = INTERNC(sy) + GRID/2 + (sy & 1);
    p->u.point.cp[0].cpos.d.x = INTERNC(sx) - 1;
    p->u.point.cp[0].cpos.d.y = INTERNC(sy) + GRID/2 + (sy & 1);

    p->u.point.cp[1].pos.d.x = INTERNC(sx) + GRID/2 + (sx & 1);
    p->u.point.cp[1].pos.d.y = INTERNC(sy);
    p->u.point.cp[1].cpos.d.x = INTERNC(sx) + GRID/2 + (sx & 1);
    p->u.point.cp[1].cpos.d.y = INTERNC(sy) - 1;
    
    p->u.point.cp[2].pos.d.x = INTERNC(sx) + GRID/2 + ((sx+1) & 1);
    p->u.point.cp[2].pos.d.y = INTERNC(sy) + GRID-1;
    p->u.point.cp[2].cpos.d.x = INTERNC(sx) + GRID/2 + ((sx+1) & 1);
    p->u.point.cp[2].cpos.d.y = INTERNC(sy) + GRID;
    
    p->u.point.cp[3].pos.d.x = INTERNC(sx) + GRID-1;
    p->u.point.cp[3].pos.d.y = INTERNC(sy) + GRID/2 + ((sy + 1) & 1);
    p->u.point.cp[3].cpos.d.x = INTERNC(sx) + GRID;
    p->u.point.cp[3].cpos.d.y = INTERNC(sy) + GRID/2 + ((sy + 1) & 1);

    setLineStart (p, INTERNC_M(sx), INTERNC_M(sy));
    setLineEnd (p, INTERNC_M(sx) + 1, INTERNC_M(sy), 1);
}

void initPoint (vecElement *p, int sx, int sy)
{
    assert(p->etype == e_point);

    memset ((char*)&p->u.point, 0, sizeof(p->u.point));
    setPointPos (p, sx, sy);

}

vecElement *addPointToPoly (vecElement *poly, int sx, int sy)
{
    vecElement *p;

    assert (poly->etype == e_poly);

    if (pointPosUsed (poly, sx, sy))
        return NULL;

    p = addElemAfter (poly, (poly->u.poly.lines == NULL) ? NULL : poly->u.poly.lines->prev, e_point);

    assert (p != NULL);

    initPoint (p, sx, sy);

    return p;
}

int cpWouldIntersect (vecElement *src, int srci, vecElement *dst, int dsti)
{
    vecElement *dstSucc;
    vec_t pointVecN;
    vec_t tmpVec;
    int s1, s2;

    dstSucc = nextPoint (dst);

    assert (dstSucc);
    assert (dstSucc->etype == e_point);

    tmpVec.d.x = src->u.point.pos.d.x - dst->u.point.pos.d.x;
    tmpVec.d.y = src->u.point.pos.d.y - dst->u.point.pos.d.y;
    s1 = vecDir (&tmpVec);
    tmpVec.d.x = dstSucc->u.point.pos.d.x - dst->u.point.pos.d.x;
    tmpVec.d.y = dstSucc->u.point.pos.d.y - dst->u.point.pos.d.y;
    s2 = vecDir (&tmpVec);

    if ((s1 & s2) == 0)
    {
        CPRINTF ("   no common border (%x %x)\n", s1, s2);
        return 0;
    }


    /* the normal of the connection line of the successor and the successor's successor
     */
    pointVecN.d.x = -(INTERNC_M(dst->u.point.pos.d.y) - INTERNC_M(dstSucc->u.point.pos.d.y));
    pointVecN.d.y = INTERNC_M(dst->u.point.pos.d.x) - INTERNC_M(dstSucc->u.point.pos.d.x);

    tmpVec.d.x = INTERNC_M(src->u.point.pos.d.x) - INTERNC_M(dstSucc->u.point.pos.d.x);
    tmpVec.d.y = INTERNC_M(src->u.point.pos.d.y) - INTERNC_M(dstSucc->u.point.pos.d.y);
    s1 = SIGN(VEC_DOT_LL(pointVecN, tmpVec));

    CPRINTF ("   p->nnp: (%d %d): %d\n", srci, dsti, s1);

    tmpVec.d.x = src->u.point.cp[srci].pos.d.x - INTERNC_M(dstSucc->u.point.pos.d.x);
    tmpVec.d.y = src->u.point.cp[srci].pos.d.y - INTERNC_M(dstSucc->u.point.pos.d.y);
    s2 = SIGN(VEC_DOT_LL(pointVecN, tmpVec));

    CPRINTF ("   scp->nnp: (%d %d): %d\n", srci, dsti, s2);

    if ((s1 != 0) && (s1 != s2))
        return 1; 

    tmpVec.d.x = dst->u.point.cp[dsti].pos.d.x - INTERNC_M(dstSucc->u.point.pos.d.x);
    tmpVec.d.y = dst->u.point.cp[dsti].pos.d.y - INTERNC_M(dstSucc->u.point.pos.d.y);
    s2 = SIGN(VEC_DOT_LL(pointVecN, tmpVec));

    CPRINTF ("   dcp->nnp: (%d %d): %d\n", srci, dsti, s2);

    if ((s1 != 0) && (s1 != s2))
        return 1; 

    return 0;
}

int findDstPoint (vecElement *p, int src, vecElement *np, int dstp)
{
    int64_t maxLen, minLen;
    cp_t *end;
    int dst;
    int64_t dist1, dist2;
    int i;
#if 0
    int dstp2;
#endif
    vecElement *nnp = nextPoint(p);
    vec_t dist;
#if 0
    vec_t npPos, diff2;
#endif
    vec_t diff;

    diff.d.x = p->u.point.pos.d.x - np->u.point.pos.d.x;
    diff.d.y = p->u.point.pos.d.y - np->u.point.pos.d.y;
#if 0
    diff2.d.x = nnp->u.point.pos.d.x - np->u.point.pos.d.x;
    diff2.d.y = nnp->u.point.pos.d.y - np->u.point.pos.d.y;
    dstp2 = vecDir (&diff2);
#endif

#if 0
    npPos.d.x = INTERNC_M(np->u.point.pos.d.x);
    npPos.d.y = INTERNC_M(np->u.point.pos.d.y);
#endif

    maxLen = 0LL;
    minLen = 0LL;
    end = NULL;
    dst = -1;

#if 0
    if ((dstp & dstp2) == 0)
    {
        for (i = 0; i < 4; i++)
        {
            if (dstp & (1<<i))
                continue;
            
            l = DISTQL (p->u.point.start, p->u.point.cp[i].pos);
            l += DISTQL (p->u.point.cp[i].cpos, npPos);

            if ((l < minLen) || (end == NULL))
            {
                minLen = l;
                end = &np->u.point.cp[i];
                dst = i;
            }
        }

        assert (dst != -1);
        return dst;
    }
#endif


    dist1 = DIST(p->u.point.pos, np->u.point.pos);
    dist2 = DIST(nnp->u.point.pos, np->u.point.pos);
    
    CPRINTF (" find dst from src %d to dstp %x (dx=%d dy=%d)\n", src, dstp, diff.d.x, diff.d.y);
    for (i = 0; i < 4; i++)
    {
        if (dstp & (1<<i))
        {
            int64_t l;

            CPRINTF (" dst=%d ..\n", i);

            if (abs(diff.d.y) == 1)
            {
                if (diff.d.x == 0)
                {
                    if (((src == BTOP_I) && (i == BBOT_I)) ||
                        ((src == BBOT_I) && (i == BTOP_I)))
                    {
                        /* adjacent pixel (touching border)
                         */
                        CPRINTF (" v touching at %d\n", i);
                        return i;
                    }
                }

                if (((src == BTOP_I) || (src == BBOT_I)) &&
                    ((i == BTOP_I) || (i == BBOT_I)))
                {
                    CPRINTF (" h connection not used: %d to %d\n", src, i);
                    continue;
                }
            }

            if (abs(diff.d.x) == 1)
            {
                if (diff.d.y == 0)
                {
                    if (((src == BLEFT_I) && (i == BRIGHT_I)) ||
                        ((src == BRIGHT_I) && (i == BLEFT_I)))
                    {
                        /* adjacent pixel (touching border)
                         */
                        CPRINTF (" h touching at %d\n", i);
                        return i;
                    }
                }

                if (((src == BLEFT_I) || (src == BRIGHT_I)) &&
                    ((i == BLEFT_I) || (i == BRIGHT_I)))
                {
                    CPRINTF (" v connection not used: %d to %d\n", src, i);
                    continue;
                }
            }

            CPRINTF (" check intersect %d\n", i); 
             if (cpWouldIntersect (p, src, np, i))
            {
                CPRINTF ("  would intersect s=%d d=%d: I\n", src, i);
                continue;
            }

            if (dist1 < dist2)
            {
                dist.d.x = INTERNC(nnp->u.point.pos.d.x) - np->u.point.cp[i].cpos.d.x;
                dist.d.y = INTERNC(nnp->u.point.pos.d.y) - np->u.point.cp[i].cpos.d.y;

                l = (int64_t)dist.d.x * (int64_t)dist.d.x +
                    (int64_t)dist.d.y * (int64_t)dist.d.y;

                if ((l > maxLen) || (end == NULL))
                {
                    maxLen = l;
                    end = &np->u.point.cp[i];
                    dst = i;
                }
            }
            else 
            {
                dist.d.x = INTERNC(p->u.point.pos.d.x) - np->u.point.cp[i].cpos.d.x;
                dist.d.y = INTERNC(p->u.point.pos.d.y) - np->u.point.cp[i].cpos.d.y;

                l = (int64_t)dist.d.x * (int64_t)dist.d.x +
                    (int64_t)dist.d.y * (int64_t)dist.d.y;

                if ((l < minLen) || (end == NULL))
                {
                    minLen = l;
                    end = &np->u.point.cp[i];
                    dst = i;
                }
            }
            CPRINTF (" check intersect %d\n", i); 
        }
    }

    CPRINTF (" OK: s=%d d=%d: d=%d\n", src, i, dst);
    return dst;
}


int connectPointWithNext (vecElement *p)
{
    vecElement *np = NULL;
    vecElement *newLine = NULL;
    vec_t vec;
    cp_t *start, *end;
    int srcp, dstp;
    int src, dst;
    int i, j;
    int64_t l, minLen;
    int adjacent;


    assert(p->etype == e_point);

    if (p->next == p)
    {
        /* only point in polygon?
        */
        return 0;
    }

    np = nextPoint(p);

    assert(np != NULL);

    assert (p->u.point.connections < 2);

    vec.d.x = (np->u.point.pos.d.x - p->u.point.pos.d.x);
    vec.d.y = (np->u.point.pos.d.y - p->u.point.pos.d.y);

    adjacent = ((abs(vec.d.x) + abs(vec.d.y)) == 1);

    srcp = vecDir (&vec) & ~p->u.point.connected;
    CPRINTF ("srcp: %x %x\n", vecDir (&vec), p->u.point.connected);


    vec.d.x = -vec.d.x;
    vec.d.y = -vec.d.y;
    dstp = vecDir (&vec) & ~np->u.point.connected;
    CPRINTF ("dstp: %x %x\n", vecDir (&vec), np->u.point.connected);

    src = dst = -1;

    start = NULL;
    src = -1;
    if (p->u.point.connections == 0)
    {
        /* no connection yet. Start connection is based on direction to next point
         */

        if (abs(vec.d.x) > abs(vec.d.y))
        {
            /* use horizontal connection point */
            src = (srcp & BLEFT) ? BLEFT_I : BRIGHT_I;
        }
        else
        {
            /* use vertical connection point */
            srcp &= (BTOP | BBOT);
            src = (srcp & BTOP) ? BTOP_I : BBOT_I;
        }

        assert (src != -1);

        start = &p->u.point.cp[src];

        if (bitsSet (dstp, 4) == 1)
        {
            dst = lowestBit (dstp, 4);
        }
        else
        {
            dst = findDstPoint (p, src, np, dstp);
        }
    }
    else
    {
        if (srcp == 0)
        {
            CPRINTF("no source point\n");
            return 0;
        }

        if ((bitsSet (dstp, 4) == 1) && (bitsSet (srcp, 4) == 1))
        {
            src = lowestBit (srcp, 4);
            start = &p->u.point.cp[src];
            dst = lowestBit (dstp, 4);

            CPRINTF ("min. connection: %d %d\n", src,dst);
        }
        else
        {
            src = dst = -1;
            minLen = 0;
            for (i = 0; i < 4; i++)
            {
                if (srcp & (1<<i))
                {
                    CPRINTF ("check src %d\n", i);
                    l = DIST(p->u.point.start, p->u.point.cp[i].cpos);
                    j = findDstPoint (p, i, np, dstp);

                    if (j != -1)
                    {
                        l += DIST(p->u.point.cp[i].cpos, np->u.point.cp[j].cpos);
                        l += DIST(np->u.point.cp[j].cpos, nextPoint(np)->u.point.start);

                        /* use the connection point combination which gives us the
                         * shortest total length of
                         * src.cp1 -> src.cp2 -> dest.cp1 -> destSucc -> pos
                         */
                        if ((src == -1) || (l < minLen))
                        {
                            minLen = l;
                            start = &p->u.point.cp[i];
                            src = i;
                            dst = j;
                        }
                    }
                }
            }
        }
    }

    if (dst == -1)
    {
        CPRINTF ("no destination cp (srcp=%x dstp=%x)\n", srcp, dstp);
        return 0;
    }
    else
    {
        CPRINTF ("srcp=%x dstp=%x : %d %d\n", srcp, dstp, src, dst);
    }

    end = &np->u.point.cp[dst];

    /* now we have the start (maybe) and end of the connection line. 
    */

    /* mark connection as used on source and destination pixel */
    if (start)
    {
        p->u.point.connected  |= (1<<src);
        p->u.point.connections++;
        assert (start->p == NULL);
        start->p = np;
        DBG_BOX(start->pos.d.x, start->pos.d.y, GRID/4, 0xffff00ff, handleStr(start));
    }

    np->u.point.connected |= (1<<dst);
    np->u.point.connections++;
    assert (end->p == NULL);
    end->p = p;
    DBG_BOX(end->pos.d.x, end->pos.d.y, GRID/4, 0xffff00ff, handleStr(end));

    setLineStart (np, end->pos.d.x, end->pos.d.y);

    if (start)
    {
        if (!adjacent)
        {
            newLine = addElemAfter (p->poly, p, e_line);
            setLineEnd (newLine, end->cpos.d.x, end->cpos.d.y, 1);
            setLineStart (newLine, start->cpos.d.x, start->cpos.d.y);
        }
        setLineEnd (p, start->pos.d.x, start->pos.d.y, 1);
    }

    return 1;
}



/* try to remove superfluous points by
 * - Comparing the slope of a point's line to the successor and predecessor (high resolution)
 *   Remove point if identical.
 * - Comparing the slope of a points line to the predecessr and the successor's
 *   successor (medium resolution). If identical, AND the slope to the predecessor to the successor
 *   is also identical (low resolution), remove the point as well.
 * 
 * Do this as long as no more points are removed. This eliminates repeating line patterns with identical slope.
 * Such a pattern happens if the vectorize algorithm follows a long edge with identical
 * slope because it doesn't always track the right pixels.
 */
void mergePoints (vecElement *poly)
{
    vecElement *e, *prev, *next;
    vecElement *start;
    vec_t prevVec;
    vec_t curVec;
    vec_t nextVec;
    int i;
    int removed = 1;

    for (i = 0; (i < 10) && removed; i++)
    {
        removed = 0;
        start = poly->u.poly.lines;

        assert (start->etype == e_point);

        prev = 0;
        for (e = nextPoint(start); e != NULL; e = next)
        {
            prev = prevPoint (e);
            next = nextPoint (e);

            pointLineToNext (prev, &prevVec);
            pointLineToNext (e, &curVec);
            pointLineToNext (next, &nextVec);

            if (next == start)
                break;

            if (sameSlope (&prevVec, &curVec, 100))
            {
                DBG_BOX(INTERNC_M(e->u.point.pos.d.x), INTERNC_M(e->u.point.pos.d.y), GRID, 0xff008080, handleStr(e));
                removePoint (e);
                removed = 1;
            }
            else if (sameSlope (&prevVec, &nextVec, 10) && sameSlope (&prevVec, &curVec, 1))
            {
                DBG_BOX(INTERNC_M(e->u.point.pos.d.x), INTERNC_M(e->u.point.pos.d.y), GRID, 0xff008080, handleStr(e));
                removePoint (e);
                removed = 1;
            }
        }
    }
}

void connectPoints (vecElement *poly)
{
    vecElement *e, *next;
    vecElement *start;
    int count = 0;
    int i;

    assert (poly->etype == e_poly);
    start = poly->u.poly.lines;

    for (e = start; e != NULL; e = next)
    {
        assert (e->etype == e_point);

        if (IS_INVAL(e))
            break;

        next = e->next;
        assert (next->etype == e_point);
        if (next == start)
            next = NULL;

        CDEBUG(0);
        CPRINTF ("#### Point %d\n", count);
        if (connectPointWithNext (e))
        {
            SET_INVAL(e, 0);
        }
        else
        {
//            SET_INVAL(e, 1);
//            return;
        }

        for (i = 0; i < 4; i++)
        {
            if (e->u.point.cp[i].p)
            {
                assert ((e->u.point.cp[i].p == nextPoint(e)) ||
                        (e->u.point.cp[i].p == prevPoint(e)));
            }
        }


        count++;
        CDEBUG(0);

        if (count == 2)
        {
            disconnectPointWithNext (start);
        }
    }

    connectPointWithNext (start);

#if 0
    e = poly->u.poly.lines;

//    if (e->u.point.connections < 2)
    {
        /* 1st point fixup */
        disconnectPointWithNext (e);

        if (connectPointWithNext (e))
        {
            SET_INVAL(e, 0);
        }
        else
        {
            SET_INVAL(e, 1);
        }
    }
#endif
}

void disconnectPointWithNext (vecElement *e)
{
    vecElement *next;
    int i;
    int sd, ed;

    assert (e->etype == e_point);

    next = nextPoint (e);

    /* mark connection points as unconnected
    */
    sd = ed = 0;
    for (i = 0; i < 4; i++)
    {
        if (e->u.point.cp[i].p == next)
        {
            e->u.point.cp[i].p = NULL;
            e->u.point.connected &= ~(1<<i);
            e->u.point.connections--;
            CLEAR_MARKERS(handleStr(&e->u.point.cp[i]));
            sd++;
        }
        if (next->u.point.cp[i].p == e)
        {
            next->u.point.cp[i].p = NULL;
            next->u.point.connected &= ~(1<<i);
            next->u.point.connections--;
            CLEAR_MARKERS(handleStr(&next->u.point.cp[i]));
            ed++;
        }
    }

    if (e->next->etype == e_line)
        removeLine (e->next);

    /*
    assert(sd == 1);
    assert(ed == 1);
    */
}

void disconnectPoints(vecElement *poly)
{
    vecElement *e, *next;
    int i;

    assert (poly->etype == e_poly);

    i = 0;
    for (e = poly->u.poly.lines; e != NULL; e = next)
    {
        next = nextPoint (e);
        if (next == poly->u.poly.lines)
            next = NULL;

        disconnectPointWithNext (e);

        i++;
    }

    for (e = poly->u.poly.lines; e != NULL; e = next)
    {
        int i;
        next = e->next;

        assert(e->etype == e_point);
        assert(e->u.point.connections == 0);
        assert(e->u.point.connected == 0);

        for (i = 0; i < 4; i++)
            assert (e->u.point.cp[i].p == NULL);


        if (next == poly->u.poly.lines)
            next = NULL;

        initPoint (e, e->u.point.pos.d.x, e->u.point.pos.d.y);
    }
}

void polyStart (int sx, int sy, int redraw)
{

    if (sx < 0)
        sx = 0;
    if (sx > wid-1)
        sx = wid-1;
    if (sy < 0)
        sy = 0;
    if (sy > hei-1)
        sy = hei-1;

    polyRedraw = redraw;

    linesInit();

    CLEAR_MARKERS(NULL);

    if (((uint)sx >= wid) || ((uint)sy >= hei))
        return;

    if (curPoly)
    {
        deletePoly (curPoly);
        curPoly = NULL;
    }

    curPoly = getElem(e_poly);

    if (!curPoly)
        return;

    curPoint = NULL;

    curPoly->u.poly.buffer = 1;

    /* start with a single pixel */
    startPoint = addPointToPoly (curPoly, sx, sy);

    assert (startPoint);

    DBG_BOX(INTERNC_M(sx), INTERNC_M(sy), GRID, 0xff00ffff, "start");

    clearLineBuffer (1);

    drawCurrentPoly(0);
}

void polyMoveEdge (int sx, int sy, int autoMode)
{
    CLEAR_MARKERS("+l");

    if (sx < 0)
        sx = 0;
    if (sx > wid-1)
        sx = wid-1;
    if (sy < 0)
        sy = 0;
    if (sy > hei-1)
        sy = hei-1;

    if (polyRedraw)
    {
        disconnectPoints (curPoly);
        clearElemMarkers (curPoly);
    }

    if (curPoint == NULL)
    {
        curPoint = addPointToPoly (curPoly, sx, sy);
    }
    else
    {
        setPointPos (curPoint, sx, sy);
    }
    updatePolyBb (curPoly);

    DBG_BOX(INTERNC_M(sx), INTERNC_M(sy), GRID, 0xffffffff, "current");

#if 0
    if (autoMode && (VEC_LEN(LDX(curLine), LDY(curLine)) > GRID*30))
        polyFix();
#endif

    if (polyRedraw)
    {
        CLEAR_MARKERS("EBOX");
        CLEAR_MARKERS("ELINE");
        if (curPoly->u.poly.nLines > 2)
        {
            connectPoints (curPoly);
        }


        polyUpdateScrObject (curPoly);
    }
}

int polyFix ()
{
    if (!curPoint)
        return 0;

    if (IS_INVAL(curPoint))
        return 0;

    DBG_BOX(INTERNC_M(curPoint->u.point.pos.d.x), INTERNC_M(curPoint->u.point.pos.d.y), GRID, 0xfff00fff, handleStr(curPoint));

    curPoint = NULL;

    return 1;
}

int polyOverlaps (vecElement *poly)
{
    vecElement *line;

    for (line = poly->u.poly.lines; line != NULL; line = GET_NEXT(line))
    {
        if (IS_OVERLAP(line))
            return 1;
    }
    return 0;
}

static void lineFinish (vecElement *line)
{
    int dx = line->u.line.vec.d.x = (line->u.line.end.d.x - line->u.line.start.d.x);
    int dy = line->u.line.vec.d.y = (line->u.line.end.d.y - line->u.line.start.d.y);

    if (line->u.line.vec.d.x != 0)
    {
        line->u.line.dyDx = (float)dy / (float)dx;
    }
    else if (dy < 0)
    {
        line->u.line.dyDx = -DYDX_MAX*2;
    }
    else
    {
        line->u.line.dyDx = DYDX_MAX*2;
    }
}

void polySetNormals (vecElement *poly)
{
    float cross;
    float csum = 0;
    float dx1, dy1;
    float vl;
    vecElement *line, *next, *prev;

    /* calculate the polygon area using the formula
     *
     * SUM(0..n-1){x[i] * (y[i+1] - y[i-1])}
     *
     * to determine whether the polygon is left or right oriented (we need this to determine
     * normal vectors)
     */

    /* predecessor of 1st vector ... */
    prev = poly->u.poly.lines->prev;

    for (line = poly->u.poly.lines; line != NULL; line = GET_NEXT(line))
    {
        /* successor of current vector */
        next = line->next;

        cross = SCREENC(line->u.line.start.d.x) * (SCREENC(next->u.line.start.d.y) - SCREENC(prev->u.line.start.d.y));

        prev = line;

        csum += cross;
    }

    SET_LEFT(poly, (csum < 0.0));

    /* calculate normal vector for all lines of the polygon
     */
    for (line = poly->u.poly.lines; line != NULL; line = GET_NEXT(line))
    {
        dx1 = ((line->u.line.end.d.x - line->u.line.start.d.x));
        dy1 = ((line->u.line.end.d.y - line->u.line.start.d.y));

        vl = VEC_LEN(dx1, dy1);

        if (IS_LEFT(poly))
        {
            line->u.line.normal.d.x = -((dy1 * (float)NORM_LEN) / vl);
            line->u.line.normal.d.y =  ((dx1 * (float)NORM_LEN) / vl);
        }
        else
        {
            line->u.line.normal.d.x =  ((dy1 * (float)NORM_LEN) / vl);
            line->u.line.normal.d.y = -((dx1 * (float)NORM_LEN) / vl);
        }

#if 0
        if (L_COMPLETE(line))
        {
            if (line->etype == e_line)
            {
                DBG_LINE((line->u.line.start.d.x + line->u.line.end.d.x) / 2,
                         (line->u.line.start.d.y + line->u.line.end.d.y) / 2,
                         ((line->u.line.start.d.x + line->u.line.end.d.x) / 2) + line->u.line.normal.d.x * (GRID / NORM_LEN),
                         ((line->u.line.start.d.y + line->u.line.end.d.y) / 2) + line->u.line.normal.d.y * (GRID / NORM_LEN),
                         0xff00ffff, "+l");
            }
            else
            {
                DBG_LINE(INTERNC_M(line->u.point.pos.d.x), INTERNC_M(line->u.point.pos.d.y),
                         INTERNC_M(line->u.point.pos.d.x) + line->u.line.normal.d.x * (GRID / NORM_LEN),
                         INTERNC_M(line->u.point.pos.d.y) + line->u.line.normal.d.y * (GRID / NORM_LEN),
                         0xff00ffff, "+l");
            }
        }
#endif
    }
}



vecElement *getNearestIntersect (vec_t *vStart, int dx, int dy, vec_t *iCord)
{
    float minLen = (float)INTERNC(wid) * (float)INTERNC(hei);
    float len;
    vecElement        *found = NULL;
    vecElement *p, *l;
    vec_t ic;
    vec_t v;

    v.d.x = dx;
    v.d.y = dy;

    if ((dx == 0) && (dy == 0))
        return NULL;

    for (p = polyList; p != NULL; p = p->next)
    {
        for (l = p->u.poly.lines; l != NULL; l = GET_NEXT(l))
        {
            if (lineIntersect (l, vStart, &v, &ic, 0))
            {
                len = VEC_LEN(ic.d.x - vStart->d.x, ic.d.y / vStart->d.y);

                if (len < minLen)
                {
                    minLen = len;
                    found = l;
                    *iCord = ic;
                }
            }
        }
    }

    return found;
}

int lineIntersect (vecElement *l, vec_t *vStart, vec_t *vec, vec_t *iCord, int noEndCheck)
{
    PLINE_FPTYPE ix, iy;
    PLINE_FPTYPE dyDx, y0;
    vec_t rel;
    vec_t v, d;

    rel.d.x = vStart->d.x - l->u.line.start.d.x;
    rel.d.y = vStart->d.y - l->u.line.start.d.y;

    /* check where the lines intersect
     */
    if (vec->d.x == 0)
    {
        if ((l->u.line.dyDx >= DYDX_MAX) || (l->u.line.dyDx <= -DYDX_MAX))
        {
            /* paricle moves vertical, line as well -> never intersect 
             */
            return 0;
        }
        ix = (PLINE_FPTYPE)rel.d.x;
        iy = ix * l->u.line.dyDx;
    }
    else 
    {
        /* get line formula of particle */
        dyDx = (PLINE_FPTYPE)vec->d.y / (PLINE_FPTYPE)vec->d.x;

        y0 = (PLINE_FPTYPE)rel.d.y - (PLINE_FPTYPE)rel.d.x * dyDx;

        /* x/y intersection point */
        ix = (y0) / (l->u.line.dyDx - dyDx);
        iy = ix * l->u.line.dyDx;
    }

    iCord->d.x = (int)ix + l->u.line.start.d.x;
    iCord->d.y = (int)iy + l->u.line.start.d.y;

    if (((uint)iCord->d.x >= INTERNC(wid)) || ((uint)iCord->d.y >= INTERNC(hei)))
        return 0;

    if (noEndCheck)
        return 1;

    v.d.x = iCord->d.x - vStart->d.x;
    v.d.y = iCord->d.y - vStart->d.y;
    if (!SAME_DIR(v, *vec))
        return 0;

    v.d.x = LDX(l);
    v.d.y = LDY(l);
    d.d.x = iCord->d.x - l->u.line.start.d.x;
    d.d.y = iCord->d.y - l->u.line.start.d.y;

    if (!SAME_DIR(v, d))
        return 0;

    v.d.x = -LDX(l);
    v.d.y = -LDY(l);
    d.d.x = iCord->d.x - l->u.line.end.d.x;
    d.d.y = iCord->d.y - l->u.line.end.d.y;

    if (!SAME_DIR(v, d))
        return 0;


    return 1;
}

void seekBorder (int x, int y, int dx, int dy)
{
#if  (DEBUG == 1)
    vec_t ic;
    vecElement *p, *l;
    vec_t s, v;

    s.d.x = x;
    s.d.y = y;
    v.d.x = dx;
    v.d.y = dy;

    CLEAR_MARKERS("tmp");

    if ((dx == 0) && (dy == 0))
        return;

    for (p = polyList; p != NULL; p = p->next)
    {
        for (l = p->u.poly.lines; l != NULL; l = GET_NEXT(l))
        {
            if (lineIntersect (l, &s, &v, &ic, 0))
            {
                DBG_MARKER_N(ic.d.x, ic.d.y, GRID/4, 0xff0000ff, "tmp");
            }
        }
    }

    if (getNearestIntersect (&s, dx, dy, &ic))
        DBG_MARKER_N(ic.d.x, ic.d.y, GRID*4, 0xff0000ff, "tmp");
#endif
}

/* find nearest line of polygon relative to a position ix/iy. ix/iy is expected
 * to be inside the polygon
 */
vecElement *nearestLine (vecElement *poly, int ix, int iy, vec_t *v)
{
    vecElement *l;
    vec_t pos, iPos, diff;
    int len, maxLen;
    vecElement *rc = NULL;
    int buffer = poly->u.poly.buffer;

    pos.d.x = ix;
    pos.d.y = iy;

    maxLen = 0x7fffffff;

    for (l = poly->u.poly.lines; l != NULL; l = GET_NEXT(l))
    {
        /* check start and end-pint of line */
        diff.d.x =  l->u.line.start.d.x - ix;
        diff.d.y =  l->u.line.start.d.y - iy;
        len = VEC_LEN(diff.d.x / GRID, diff.d.y / GRID);

        if (len < maxLen)
        {
            maxLen = len;
            rc = l;

            if (v)
                *v = diff;
        }

        diff.d.x =  l->u.line.end.d.x - ix;
        diff.d.y =  l->u.line.end.d.y - iy;
        len = VEC_LEN(diff.d.x / GRID, diff.d.y / GRID);

        if (len < maxLen)
        {
            maxLen = len;
            rc = l;

            if (v)
                *v = diff;
        }

        /* check if/where the line intersects with the normal vector
         * starting at the req. position
         */
        if (!lineIntersect (l, &pos, &l->u.line.normal, &iPos, 0))
            continue;
        
        /* make sure the difference vector has the same direction as
         * the line
         */
        diff.d.x = iPos.d.x - ix;
        diff.d.y = iPos.d.y - iy;

        if (!SAME_DIR(diff, l->u.line.normal))
            continue;
        

        /* check that the intersection point really houses the line. If not, then
         * the intersection point is before/after the lien start/end
         */
        if (C2VEC(buffer, iPos) != l)
        {
#if 0
            vec_t d1, d2;

            /* check which is nearer: start or end
             */

            d1.d.x = l->u.line.start.d.x - ix;
            d1.d.y = l->u.line.start.d.y - iy;

            d2.d.x = l->u.line.end.d.x - ix;
            d2.d.y = l->u.line.end.d.y - iy;

            if (VEC_LEN(d1.d.x, d1.d.y) > VEC_LEN(d2.d.x, d2.d.y))
                diff = d2;
            else
                diff = d1;
#else
            continue;
#endif
        }

        /* calculate the distance to the intersection and find the lowest one
         */
        len = VEC_LEN(diff.d.x / GRID, diff.d.y / GRID);

        if (len < maxLen)
        {
            maxLen = len;
            rc = l;

            if (v)
                *v = diff;
        }
    }

    return rc;
}

#ifdef HIT_WALL_WITH_SIZE
static void setOuter (vecElement *l, int buffer, int sx, int sy)
{
    int o;
    vecElement *l2;
    screeninfo *tSi;
    vec_t pos;

    if (((unsigned)sx >= wid) || (unsigned)sy >= hei)
        return;
    
    o = sx + sy*wid;

    l2 = O2VEC(buffer, o);
    tSi = &siArray[o];

    if ((l2 == NULL) && (tSi->particleList != SCR_BRICK))
    {
        /* pixel does not yet have a line and is not a brick
        */
        O2VEC(buffer, o) = l;
        pos.d.x = sx;
        pos.d.y = sy;
        assignPointToElem (l, &pos);

        tSi->flags |= SI_LINE|SI_OUTER_LINE;
        FRICTION(tSi) = getValue (&wallFriction);

        DBG_PIXEL(sx, sy, 0x8000ffff);
    }
}
#endif

void movePolyToBuffer (vecElement *poly, int buffer)
{
    vecElement *l, *tmpL;
    int oldBuffer = poly->u.poly.buffer;
    int off;
    int i;
    vec_t *pos;
    int num = poly->nPixels;
    screeninfo *pSi;

    if (oldBuffer == buffer)
        return;

    for (i = 0; i < num; i++)
    {
        pos = &poly->pixels[i];
        off = pos->d.x + pos->d.y * wid;
        tmpL = O2VEC(oldBuffer, off);

        if ((tmpL == NULL) || (tmpL->poly != poly))
            continue;

        l = O2VEC(buffer, off);

        if (l != NULL)
        {
            deletePoly (l->poly);
        }
        O2VEC(buffer, off) = tmpL;
        O2VEC(oldBuffer, off) = NULL;
    }
    poly->u.poly.buffer = buffer;

    if (buffer == 0)
    {
        for (i = 0; i < num; i++)
        {
            pos = &poly->pixels[i];
            off = pos->d.x + pos->d.y * wid;

            pSi = &siArray[off];

            tmpL = O2VEC(buffer, off);

            if ((tmpL == NULL) || (tmpL->poly != poly))
                continue;

            if (tmpL->etype == e_poly)
            {
                vec_t v;
                int x, y;

                x = INTERNC_M(pos->d.x);
                y = INTERNC_M(pos->d.y);


                /* if this polygon is drawn while vectorizing a wall, clear
                 * the old wall pixel but retain it's density (also clear
                 * the marker flag so that the cleanup code in track.c
                 * does not clear the pixel again).
                 * Otherwise apply the current wall density to the pixel
                 */
                if (pSi->flags & SI_TRACK_MARKER)
                {
                    DENSITY_TYPE oldDens = SI_CHARGE(pSi);
                    uint8_t __attribute__ ((unused)) w = WALL_MASS_GET(pSi);
                    setEmpty (pos->d.x, pos->d.y, 1);
                    SI_CHARGE(pSi) = oldDens;
                    WALL_MASS_SET(pSi,w);
                    pSi->flags &= ~SI_TRACK_MARKER;
                }
                else
                {
                    SET_WALL_DENS_DFL(pSi);
                }

                pSi->flags |= SI_LINE|SI_IN_POLY;

                CL_SET_WALL(pos->d.x, pos->d.y);

                if ((nearestLine (poly, x, y, &v)) != NULL)
                {
                    normalize (&v.d.x, &v.d.y);
                    pSi->normals[0] = v.d.x;
                    pSi->normals[1] = v.d.y;
                }
            }
            else
            {
                pSi->flags |= SI_LINE;
                pSi->flags &= ~SI_IN_POLY;
                FRICTION(pSi) = getValue (&wallFriction);

                CL_SET_WALL(pos->d.x, pos->d.y);

                if ((pSi->flags & SI_OUTER_LINE) == 0)
                {
                    DBG_PIXEL(pos->d.x, pos->d.y, 0x8000ff00);

#ifdef HIT_WALL_WITH_SIZE
                    {
                        int b = vecDir (&tmpL->u.line.normal);

                        if (b & BLEFT)
                            setOuter (tmpL, buffer, pos->d.x-1, pos->d.y);
                        if (b & BTOP)
                            setOuter (tmpL, buffer, pos->d.x, pos->d.y-1);
                        if (b & BBOT)
                            setOuter (tmpL, buffer, pos->d.x, pos->d.y+1);
                        if (b & BRIGHT)
                            setOuter (tmpL, buffer, pos->d.x+1, pos->d.y);
                    }
#endif
                }
            }
        }
    }
}

vecElement *findNextLineIntersection (vecElement *l, int *startToEnd, vec_t *pos)
{
    vecElement *rc = NULL;
    vecElement *pPrev;
    lineIntersection *liFound = NULL;
    int i;
    lineIntersection *li;
    float minDist;
    float iDist;
    vec_t vec, lineDir;
    vec_t normal1, normal2;
    int visited;

//    if (l->u.line.intersections == 0)
//        return NULL;

    minDist = 0;

    if (*startToEnd)
    {
        pPrev = prevPoint (l);
        lineDir.d.x = LDX(l);
        lineDir.d.y = LDY(l);
    }
    else
    {
        pPrev = nextPoint (l);
        lineDir.d.x = -LDX(l);
        lineDir.d.y = -LDY(l);
    }

    if (l->etype == e_line)
    {
        normal1 = normal2 = l->u.line.normal;
    }
    else
    {
        /* if the current element (intersecing with something else) is
         * a point, use the vector from the previous point as current
         * direction vector
         */
        lineDir.d.x = l->u.point.pos.d.x - pPrev->u.point.pos.d.x;
        lineDir.d.y = l->u.point.pos.d.y - pPrev->u.point.pos.d.y;

        /* the next point of the intersected polygon must be outside
         * of one of the two lines for the corrent point
         */
        pointNormalNext (pPrev, &normal1);
        pointNormalNext (l, &normal2);
    }

tryAgain:
    for (i = 0, li = intersectionList; i < numIntersections; i++, li++)
    {
        if ((li->l1 != l) && (li->l2 != l))
            continue;

        if (l->etype == e_point)
        {
            /* a point can only intersect one other element
             */
            liFound = li;
            break;
        }

//        if (li->flag)
//            continue;

        /* check whether the intersection is in the right direction
         */
        vec.d.x = li->pos.d.x - pos->d.x;
        vec.d.y = li->pos.d.y - pos->d.y;

        if (!SAME_DIR(lineDir, vec))
        {
            /* intersection is not at the side where the line points to
             */
            continue;
        }
        iDist = VEC_LEN(vec.d.x, vec.d.y);

        if ((liFound != NULL) && (iDist > minDist))
        {
            continue;
        }

        /* found a good candidate
        */
        minDist = iDist;
        liFound = li;
    }

    if (liFound == NULL)
        return NULL;

    /* mark this intersection element so that it's not used any more
     */
    liFound->flag = 1;

    /* decrease the intersection counter for both lines
     */
    if ((liFound->l1->etype != e_point) && (liFound->l2->etype != e_point))
    {
        liFound->l1->u.line.intersections--;
        liFound->l2->u.line.intersections--;
    }

    /* return the intersected position.
     */
    *pos = liFound->pos;

    /* determine which of the two is the intersected line 
     */
    if (liFound->l1 == l)
        rc = liFound->l2;
    else
        rc = liFound->l1;

    visited = rc->flags & L_VISITED;
    visited = 1;

    if (rc->etype == e_line)
    {
        /* Intersecting with a line
         */

        /* now we continue with the intersected line at the intersection point.
         * determine whether we need to extend to the start or the end of
         * the intersected line. This depends on the relationship of the normal of the old line
         * to the direction of the intersected line.
         */

        if (SAME_DIR(normal1, rc->u.line.vec))
        {
            /* normal points into the direction from start to end, so follow the new poly
             * in start-to-end direction
             */
            *startToEnd = 1;
        }
        else
        {
            *startToEnd = 0;
        }

    }
    else
    {
        /* intersecting with a point. We need the normals of the 
         * two lines attached to the point to know how to proceed.
         */
        vec_t l1, l2;
        int currentPolyOrientation = IS_LEFT(l->poly);

        if (!*startToEnd)
            currentPolyOrientation = 1 - currentPolyOrientation;

        pointLineToNext (rc, &l1);
        pointLineToPrev (rc, &l2);

        
        if ((SAME_DIR (normal1, l2) || SAME_DIR(normal2, l2)) && (currentPolyOrientation != IS_LEFT(rc->poly)))
        {
            /* need to follow intersected polygon in end-to-start 
             * direction
             */
            *startToEnd = 0;

            rc = rc->prev;
            if (visited)
            {
                *pos = rc->u.line.end;
            }
        }
        else if ((SAME_DIR (normal1, l1) || SAME_DIR(normal2, l1)) && (currentPolyOrientation == IS_LEFT(rc->poly)))
        {
            /* need to follow intersected polygon in start-to-end
             * direction
             */
            *startToEnd = 1;

            rc = rc->next;
            if (visited)
            {
                *pos = rc->u.line.start;
            }
        }
        else
        {
            /* hitting a point, but both lines from that point  go to the wrong direction.
             */
            if (l->etype == e_line)
            {
                /* if we are a line, remove this intersection and search for another one for the
                 * current line
                 */
                if (removeIntersection (l, rc))
                    goto tryAgain;

                return NULL;
            }

            /* point hits point. */

            printf ("don't know how to proceed at point!\n");
            DBG_MARKER_N(pos->d.x, pos->d.y, GRID*2, 0xff0000ff, NULL);

            if (removeIntersection (l, rc))
                goto tryAgain;

            rc = NULL;
        }
    }
    return rc;
}


/* find the point to start with
 */
vecElement *getFirstIntersectPoint (vecElement *poly)
{
    vecElement *rc = NULL;
    vecElement *sp;
    vecElement *l;
    int maxy = 0;

    for (sp = poly; sp != NULL; sp = sp->u.poly.nextIntersected)
    {
        for (l = sp->u.poly.lines; l != NULL; l = GET_NEXT(l))
        {
            l->flags &= ~L_VISITED;

            if (l->etype != e_point)
                continue;

            if (l->u.point.pos.d.y > maxy)
            {
                maxy = l->u.point.pos.d.y;
                rc = l;
            }
        }
    }

    return rc;
}

#if 1
static int mergedSize = 1000;
static vec_t *merged = NULL;

vecElement *mergePolys (vecElement *poly)
{
    vecElement *l;
    vecElement *newPoly;
    vecElement *iLine;
    vecElement *endLine;
    int startToEnd = 1;
    vec_t ipos;
    vec_t newLineStart;
    vec_t tmpPos;
    int intersect = 0;
    int count = 100000;
    int num = 0;
    int i;

    cleanupIntersectionList();

    if (merged == NULL)
        merged = (vec_t *)malloc(mergedSize * sizeof(*merged));

    ipos.d.x = ipos.d.y = 0;

    if (poly->u.poly.buffer != 1)
        return NULL;

    if (numIntersections == 0)
        return NULL;


    /* track the border of the polygon and all polygons it intersects.
     * Start with the bottom-most line of all these polygon to be
     * sure that we trace the outer border, not some inner one
     */
    l = endLine = getFirstIntersectPoint (poly);

    if (l == NULL)
        return NULL;

    newLineStart = l->u.line.start;
    startToEnd = 1;

    merged[num].d.x = SCREENC(newLineStart.d.x);
    merged[num].d.y = SCREENC(newLineStart.d.y);
    num++;

    CLEAR_MARKERS("+LOOP");
    do
    {

        if (num >= mergedSize)
        {
            mergedSize += 1000;
            merged = (vec_t *)realloc((void*)merged, mergedSize * sizeof(*merged));
        }

        /* check whether current line intersects with other line.
         */
        ipos = newLineStart;

        if ((iLine = findNextLineIntersection (l, &startToEnd, &ipos)) != NULL)
        {
            intersect = 1;

            DBG_MARKER_N (ipos.d.x, ipos.d.y, GRID/4, 0xff00ffff, "tmp");

            l = iLine;

            newLineStart = ipos;

            tmpPos.d.x = SCREENC(ipos.d.x);
            tmpPos.d.y = SCREENC(ipos.d.y);

            merged[num++] = tmpPos;
        }
        else
        {
            if (startToEnd)
            {
                l = l->next;
                newLineStart = l->u.line.start;
            }
            else
            {
                l = l->prev;
                newLineStart = l->u.line.end;
            }

            if (l->etype == e_point)
            {
                merged[num++] = l->u.point.pos;
            }

            if (l->flags & L_VISITED)
            {
                DBG_MARKER_N (newLineStart.d.x, newLineStart.d.y, GRID*2, 0xffffff00, "tmp");
                printf ("LOOP %d\n", num);

                for (i = 0; i < num-1; i++)
                {
                    DBG_LINE (INTERNC_M(merged[i].d.x), INTERNC_M(merged[i].d.y),
                                    INTERNC_M(merged[i+1].d.x), INTERNC_M(merged[i+1].d.y), 0xffff00FF, "+LOOP");

                }

                return NULL;
                break;
            }
            l->flags |= L_VISITED;

            intersect = 0;
            CLEAR_MARKERS("tmp");
        }

        if (count-- == 0)
            break;
    } while ((l != endLine) || intersect);

    newPoly = getElem(e_poly);

    newPoly->u.poly.buffer = 2;

    clearLineBuffer (2);

    for (i = 0; i < num; i++)
    {
        addPointToPoly (newPoly, (merged[i].d.x), (merged[i].d.y));
    }

    connectPoints (newPoly);
    
//XXX
//    if (!polyInvalid(newPoly))
    {
        return newPoly;
    }

    freePoly (newPoly);
    
/*
    if (po == NULL)
        po = polyNew ( 1000, POLY_BW1, COL_RED);

    polySetCoords (po, num, merged, NULL);
    polyShow (po);
*/
    return NULL;
}

#endif

void fillPoly (vecElement *poly)
{
    int x, y;
    vecElement *l;
    int inPoly;
    int buffer = poly->u.poly.buffer;

    for (y = poly->u.poly.b1.d.y+1; y <= poly->u.poly.b2.d.y; y++)
    {
        inPoly = 0;

        for (x = poly->u.poly.b1.d.x; x <= poly->u.poly.b2.d.x; x++)
        {
            l = S2VEC(buffer,x,y);

            if (l == NULL)
            {
                if (inPoly)
                {
                    vec_t pos;
                    S2VEC(buffer,x,y) = poly;

                    pos.d.x = x;
                    pos.d.y = y;
                    assignPointToElem (poly, &pos);
                }
            }
            else
            {
                /* Check the direction of the line's normal we just hit. If it's pointing to the left, we are entering
                 * the polygon, if it's pointing to the right we are exiting it.
                 */
                if (vecDir (&l->u.line.normal) & BLEFT)
                    inPoly = 1;
                else /* if (vecDir (&l->u.line.normal) & BRIGHT) */
                    inPoly = 0;
            }
        }
    }
}


int polyFinish ()
{
    vecElement *line;

    if (!curPoly)
        return 1;

    disconnectPoints (curPoly);
    mergePoints (curPoly);
    connectPoints (curPoly);
    if (polyInvalid (curPoly))
        return 0;

    recordLine (&playbackTape, 0, 0, p_finish);

    captureIntersections (1);
    setPoly (curPoly);
    captureIntersections (0);

    if (polyInvalid (curPoly))
    {
        drawCurrentPoly(1);
        drawCurrentPoly(0);
        return 0;
    }

#if 1
    if (polyOverlaps (curPoly))
    {
        vecElement *overlapPoly;

        overlapPoly = mergePolys (curPoly);

        if (overlapPoly)
        {
            setPoly (overlapPoly);
            fillPoly (overlapPoly);
            deletePoly (curPoly);
            curPoly = overlapPoly;
        }
        else
        {
            deletePoly (curPoly);
            curPoly = NULL;
            return 0;
        }
    }
#endif

    for (line = curPoly->u.poly.lines; line != NULL; line = GET_NEXT(line))
    {
        lineFinish (line);
    }

    setPoly (curPoly);

    /* fill the polygon in temporary array */
    fillPoly (curPoly);

    /* merge temporary array to main array */
    movePolyToBuffer (curPoly, 0);

    /* clear temporary array */
    clearLineBuffer (1);

    curPoly->next = polyList;
    polyList = curPoly;

    drawCurrentPoly(1);
    polyChangeFlags (curPoly->u.poly.se, POLY_FILLED, POLY_FILLED);
    polyShow (curPoly->u.poly.se);


    curPoly = NULL;
    return 1;
}

void drawCurrentPoly (int clear)
{
    if (!curPoly)
        return;

    polyUpdateScrObject (curPoly);
}

void doFillPoly (vecElement *poly)
{
    Sint16 *vx, *vy;
    int n = 0, max;
    vecElement *line;

    max = poly->u.poly.nLines + 1;

    vx = alloca(max * sizeof(*vx));
    vy = alloca(max * sizeof(*vy));

    for (line = poly->u.poly.lines; (line != NULL) && (n < max); line = GET_NEXT(line), n++)
    {
        vx[n] = IX_TO_SX(line->u.line.start.d.x);
        vy[n] = IY_TO_SY(line->u.line.start.d.y);

#if 0
        if (line->next == NULL)
        {
            n++;
            vx[n] = IX_TO_SX(line->u.line.end.d.x);
            vy[n] = IY_TO_SY(line->u.line.end.d.y);
        }
#endif
    }

    clipToDrawArea();
    myfilledPolygonColor (screen, vx, vy, n, COL_BRICK);
    mypolygonColor (screen, vx, vy, n, COL_ORANGE);
    clipDisable();
}

int polyDrawing()
{
    return (curPoly != NULL);

}

void removeCurrentPoly ()
{
    if (!curPoly)
        return;

    drawCurrentPoly(1);

    deletePoly (curPoly);

    curPoly = NULL;
}

void removeAllPolys()
{
    vecElement *poly, *next;

    for (poly = polyList; poly != NULL; poly = next)
    {
        next = poly->next;
        deletePoly (poly);
    }
}


static int linePixelIntersect (vecElement *l, int sx, int sy, vec_t *p1, vec_t *p2)
{
    int ix;
    int iy;
    int x1, y1;
    int r = 0;
    vec_t res[2];
    int checkonly = ((p1 == NULL) || (p2 == NULL));

    if (((uint)sx >= wid) || ((uint)sy >= hei))
        return 0;

    if (LDX(l) == 0) 
    {
        return (SCREENC(l->u.line.start.d.x) == sx);
    }
    if (LDY(l) == 0)
    {
        return (SCREENC(l->u.line.start.d.y) == sy);
    }

    ix = INTERNC(sx) - l->u.line.start.d.x; 
    iy = INTERNC(sy) - l->u.line.start.d.y;

    /* left border */
    y1 = l->u.line.start.d.y + ((float)ix * l->u.line.dyDx + 0.5);

    if (SCREENC(y1) == sy)
    {
        if (checkonly)
            return 1;

        x1 = ix + l->u.line.start.d.x;
        res[r].d.x = x1;
        res[r].d.y = y1;

        r++;
    }

    /* top */
    x1 = l->u.line.start.d.x + ((float)iy / l->u.line.dyDx + 0.5);

    if (SCREENC(x1) == sx)
    {
        if (checkonly)
            return 1;

        y1 = iy + l->u.line.start.d.y;

        res[r].d.x = x1;
        res[r].d.y = y1;

        r++;

        if (r == 2)
            goto out;
    }

    /* right */
    ix += MAX_SPEED;
    y1 = l->u.line.start.d.y + ((float)ix * l->u.line.dyDx + 0.5);


    if (SCREENC(y1) == sy)
    {
        if (checkonly)
            return 1;

        x1 = ix + l->u.line.start.d.x;
        res[r].d.x = x1;
        res[r].d.y = y1;

        r++;

        if (r == 2)
            goto out;
    }

    /* bottom */
    iy += MAX_SPEED;

    x1 = l->u.line.start.d.x + ((float)iy / l->u.line.dyDx + 0.5);

    if (SCREENC(x1) == sx)
    {
        if (checkonly)
            return 1;

        y1 = iy + l->u.line.start.d.y;

        res[r].d.x = x1;
        res[r].d.y = y1;

        r++;

        if (r == 2)
            goto out;
    }

out:
   if (!r)
       return 0;

    if (p1 && p2)
    {
        *p1 = res[0];
        *p2 = res[1];
    }
    return r;
}

void scrDrawLine (int sx, int sy, int all)
{
    vecElement *l;
    vec_t p1, p2, n;
    int x1, y1, x2, y2;

    if ((sx < zoomx) || (sx > zoomx + wid/zoom) 
        || (sy < zoomy) || (sy > zoomy + hei/zoom))
        return;

    if ((siArray[sx + sy*wid].flags & SI_LINE) == 0)
        return;

    if ((siArray[sx + sy*wid].flags & SI_IN_POLY) == 0)
    {
        l = S2VEC(0, sx, sy);

        if (!l)
            return;

        n = l->u.line.normal;

        if (linePixelIntersect (l, sx, sy, &p1, &p2) != 2)
        {
            return;
        }

        x1 = IX_TO_SX(p1.d.x);
        y1 = IY_TO_SY(p1.d.y);
        x2 = IX_TO_SX(p2.d.x);
        y2 = IY_TO_SY(p2.d.y);

        if (all)
        {
            drawLineF (x1, y1, x2, y2, lineCb, COL_LINE, 1, 0, 0);
        }
    }
    else
    {
        n.d.x = siArray[sx + sy*wid].normals[0];
        n.d.y = siArray[sx + sy*wid].normals[1];

        x1 = INTERNC(sx);
        x2 = x1 + MAX_SPEED;
        y1 = INTERNC(sy);
        y2 = y1 + MAX_SPEED;

        x1 = IX_TO_SX(x1);
        y1 = IY_TO_SY(y1);
        x2 = IX_TO_SX(x2);
        y2 = IY_TO_SY(y2);
    }

    if ((curPoly == NULL) && (zoom > 16))
    {
        x1 = (x1 + x2) / 2;
        y1 = (y1 + y2) / 2;

        x2 = x1 + n.d.x / 8;
        y2 = y1 + n.d.y / 8;

        drawArrowF (x1, y1, x2, y2, lineCb, COL_LINE, 1, 0, 0);
    }
}


void polyToScr (vecElement *poly)
{
#ifndef USE_OPENGL
    int sx, sy, ex, ey;

    sx = SCREENX(poly->u.poly.b1.d.x);
    sy = SCREENY(poly->u.poly.b1.d.y);
    ex = SCREENX(poly->u.poly.b2.d.x);
    ey = SCREENY(poly->u.poly.b2.d.y);

    if (((sx < 0) && (ex < 0)) || ((sx >= wid) && (ex >= wid)) ||
        ((sy < 0) && (ey < 0)) || ((sy >= hei) && (ey >= hei)))
    {
        return;
    }

#if 1
    doFillPoly (poly);

    if (zoom > 16)
        for (sx = poly->u.poly.b1.d.x; sx <= poly->u.poly.b2.d.x; sx++)
           for (sy = poly->u.poly.b1.d.y; sy <= poly->u.poly.b2.d.y; sy++)
               scrDrawLine (sx, sy, 0);
#else
    if (zoom == 1)
        doFillPoly (poly);
    else
        for (sx = poly->u.poly.b1.d.x; sx <= poly->u.poly.b2.d.x; sx++)
           for (sy = poly->u.poly.b1.d.y; sy <= poly->u.poly.b2.d.y; sy++)
               scrDrawLine (sx, sy, 1);
#endif
#endif
}

int approachesLine (int dx, int dy, int off)
{
    vecElement *line;

    /* make sure the current pixel has a line reference */
    line = O2VEC(0, off);
    if (line == NULL)
            return 0;

#if 0
    if (siArray[off].flags & SI_IN_POLY)
        return 0;
#endif

    /* check whether particles approaches the line */
    if (((-line->u.line.normal.d.y) * dy - line->u.line.normal.d.x * dx) <= 0)
    {
        return 1;
    }

    return 0;
}

#if 1
/* check for collission of particle with line. newVec is
 * - p's reflected vector if p is within the polygon (and moving further inside), or
 *   will be within the object at the next iteration
 * - unchanged if p is on the outside but does not (yet) hit the line. (rc=0)
 * - p's vector + plus an impuls in the direction of the normal vector of the line if
 *   p is within the polygon. This is to push out particles from the inner of the polygon (rc=1)
 *
 * Could use some heavy optimization ...
 */
int pHitLine (particle *p, vec_t *newVec, int off, threadData *td, screeninfo *pSi)
{
    float dyDx;
    float ix, iy;
    int64_t dx, dy;
    int64_t dq, vq;
    vec_t n;
    int cross;
    float pc0;
    float cxf, cyf, vpf, vec_len;
    fvec_t relPos;
    vecElement *line;


    /* make sure the current pixel has a line reference */
    line = O2VEC(0, off);
    if (line == NULL)
            return 0;

    if (pSi->flags & SI_IN_POLY)
    {
        n.d.x = pSi->normals[0];
        n.d.y = pSi->normals[1];
    }
    else
    {
        /* position of particle relative to start of line
         */
        relPos.d.x = p->pos.d.x - line->u.line.start.d.x;
        relPos.d.y = p->pos.d.y - line->u.line.start.d.y;

        /* particle is at a border */

        n = line->u.line.normal;

        /* check where the lines (border and particle vector) intersect
         */
        if (p->vec.d.x == 0)
        {
            if (n.d.y == 0)
            {
                /* particle moves vertical, line as well -> never intersect 
                 */
                return 0;
            }
            iy = relPos.d.x * line->u.line.dyDx;
            ix = relPos.d.x;
        }
        else 
        {
            /* line formula of particle */
            dyDx = (float)p->vec.d.y / (float)p->vec.d.x;

            pc0 = relPos.d.y - relPos.d.x * dyDx;

            /* x/y intersection point */
            ix = (pc0) / (line->u.line.dyDx - dyDx);
            iy = ix * line->u.line.dyDx;
        }

        /* vector particle -> intersection point */
        dx = ix - relPos.d.x;
        dy = iy - relPos.d.y;

#if 0
//        scrMarker (line->u.line.start.d.x + ix, line->u.line.start.d.y + iy, GRID/4, 0xff0000ff);

//        if ((abs(dx) > GRID) || (abs(dy) > GRID))
        {
            printf ("%f %f c0=%f d(p)=%f d(l)=%f\n", ix / MAX_SPEEDF, iy / MAX_SPEEDF, pc0, dyDx, line->u.line.dyDx );
            newVec->d.x = 0;
            newVec->d.y = 0;
            p->pos.d.x = GRID*10;
            p->pos.d.y = GRID*10;
            return 0;
        }
#endif

        cross = (-n.d.y) * dy - n.d.x * dx;

        if (cross >= 0)
        {
            /* appraching from correct side (i.e. not within object) */

            /* check whether particles approaches the line */
            cross = (-n.d.y) * p->vec.d.y - n.d.x * p->vec.d.x;

            if (cross <= 0)
            {
                /* no, nothing to do */
                return 0;
            }

            /* check whether particle will hit/cross the border in this itaration
             */
            dq = dx*dx + dy*dy;
            vq = (int64_t)p->vec.d.x * p->vec.d.x + (int64_t)p->vec.d.y * p->vec.d.y;

#ifdef HIT_WALL_WITH_SIZE
            if (dq > vq + PART_RADIUS(p)*PART_RADIUS(p))
#else
            if (dq > vq)
#endif
            {
                /* too far away */
                return 0;
            }

            /* reflect at border
             */
            vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

            cxf = (p->vec.d.x * NORM_LEN) / vec_len;
            cyf = (p->vec.d.y * NORM_LEN) / vec_len;

            if (FRICTION(pSi))
            {
                float vl;
                n.d.x = (n.d.x * (100 - FRICTION(pSi)) - cxf * FRICTION(pSi));
                n.d.y = (n.d.y * (100 - FRICTION(pSi)) - cyf * FRICTION(pSi));
                vl = VEC_LEN(n.d.x, n.d.y);
                n.d.x = (n.d.x * NORM_LEN) / vl;
                n.d.y = (n.d.y * NORM_LEN) / vl;
            }

            vpf = ((cxf * n.d.x) + (cyf * n.d.y));
            cxf = cxf - (2 * n.d.x * vpf) / (NORM_LEN*NORM_LEN);
            cyf = cyf - (2 * n.d.y * vpf) / (NORM_LEN*NORM_LEN);

            newVec->d.x = (int) (((vec_len) * (FLOAT)cxf * wallDamping) / (FLOAT)(NORM_LEN));
            newVec->d.y = (int) (((vec_len) * (FLOAT)cyf * wallDamping) / (FLOAT)(NORM_LEN));

#ifdef USE_OPENGL
            if (GETVAL32(particleTrails))
            {
                particle2 *p2 = &partArray2[PARTICLE_ID(p)];
                p2->ipos = p->pos;
                p2->ipos_flag = 1;
            }
#endif

            return 1;
        }

    }

    /* particle is already within the body */

    /* check whether particle moves further into the body */
    cross = (-n.d.y) * p->vec.d.y - n.d.x * p->vec.d.x;

    if (cross <= 0)
    {
        /* no, add impuls to further push it out */
        newVec->d.x = p->vec.d.x + n.d.x*1;
        newVec->d.y = p->vec.d.y + n.d.y*1;
        return 1;
    }

    /* yes, reflect at normal */

    vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

    cxf = (p->vec.d.x * NORM_LEN) / vec_len;
    cyf = (p->vec.d.y * NORM_LEN) / vec_len;
    vpf = ((cxf * n.d.x) + (cyf * n.d.y));
    cxf = cxf - (2 * n.d.x * vpf) / (NORM_LEN*NORM_LEN);
    cyf = cyf - (2 * n.d.y * vpf) / (NORM_LEN*NORM_LEN);

    newVec->d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
    newVec->d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

    return 1;
}
#else

int pHitLine (particle *p, vec_t *newVec, int off, screeninfo *pSi)
{
    float dyDx;
    float ix, iy;
    int64_t dx, dy;
    int64_t dq, vq;
    vec_t n;
    int cross, cross1;
    float pc0;
    float cxf, cyf, vpf, vec_len;
    fvec_t relPos;
    vecElement *line;
    float f;

    /* make sure the current pixel has a line reference */
    line = O2VEC(0, off);
    if (line == NULL)
            return 0;

    if (siArray[off].flags & SI_IN_POLY)
    {
        return 0;
        n.d.x = siArray[off].normals[0];
        n.d.y = siArray[off].normals[1];
    }
    else
    {
        /* position of particle relative to start of line
         */
        relPos.d.x = p->pos.d.x - line->u.line.start.d.x;
        relPos.d.y = p->pos.d.y - line->u.line.start.d.y;

        /* particle is at a border */

        n = line->u.line.normal;

        /* check where the lines (border and particle vector) intersect
         */
        if (p->vec.d.x == 0)
        {
            if (n.d.y == 0)
            {
                /* particle moves vertical, line as well -> never intersect 
                 */
                return 0;
            }
            iy = relPos.d.x * line->u.line.dyDx;
            ix = relPos.d.x;
        }
        else 
        {
            /* line formula of particle */
            dyDx = (float)p->vec.d.y / (float)p->vec.d.x;

            pc0 = relPos.d.y - relPos.d.x * dyDx;

            /* x/y intersection point */
            ix = (pc0) / (line->u.line.dyDx - dyDx);
            iy = ix * line->u.line.dyDx;
        }

        /* distance from intersection point */
        dx = ix - relPos.d.x;
        dy = iy - relPos.d.y;

#if 0
//        scrMarker (line->u.line.start.d.x + ix, line->u.line.start.d.y + iy, GRID/4, 0xff0000ff);

//        if ((abs(dx) > GRID) || (abs(dy) > GRID))
        {
            printf ("%f %f c0=%f d(p)=%f d(l)=%f\n", ix / MAX_SPEEDF, iy / MAX_SPEEDF, pc0, dyDx, line->u.line.dyDx );
            newVec->d.x = 0;
            newVec->d.y = 0;
            p->pos.d.x = GRID*10;
            p->pos.d.y = GRID*10;
            return 0;
        }
#endif

        cross1 = (-n.d.y) * dy - n.d.x * dx;
        
//        if (cross1 >= 0)
        {
            /* appraching from correct side (i.e. not within object) */

#if 1
            /* check whether particles approaches the line */
            cross = (-n.d.y) * p->vec.d.y - n.d.x * p->vec.d.x;

            if (cross1 < 0)
                cross = - cross;

            if (cross <= 0)
            {
                /* no, nothing to do */
                return 0;
            }
#endif

            /* check whether particle will hit/cross the border in this itaration
             */
            dq = dx*dx + dy*dy;
            vq = (int64_t)p->vec.d.x * p->vec.d.x + (int64_t)p->vec.d.y * p->vec.d.y;

#ifdef HIT_WALL_WITH_SIZE
            if (dq > vq + PART_RADIUS(p)*PART_RADIUS(p))
#else
            if (dq > vq)
#endif
            {
                /* too far away */
                return 0;
            }

            /* reflect at border
             */
            vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

            cxf = (p->vec.d.x * NORM_LEN) / vec_len;
            cyf = (p->vec.d.y * NORM_LEN) / vec_len;
            vpf = ((cxf * n.d.x) + (cyf * n.d.y));
            cxf = cxf - (2 * n.d.x * vpf) / (NORM_LEN*NORM_LEN);
            cyf = cyf - (2 * n.d.y * vpf) / (NORM_LEN*NORM_LEN);

            newVec->d.x = (int) (((vec_len) * (FLOAT)cxf * wallDamping) / (FLOAT)(NORM_LEN));
            newVec->d.y = (int) (((vec_len) * (FLOAT)cyf * wallDamping) / (FLOAT)(NORM_LEN));


            /* XXX friction is missing */

            return 1;
        }

    }

#if 0
    /* particle is already within the body */

    /* check whether particle moves further into the body */
    cross = (-n.d.y) * p->vec.d.y - n.d.x * p->vec.d.x;

    if (cross <= 0)
    {
        /* no, add impuls to further push it out */
        newVec->d.x = p->vec.d.x + n.d.x*1;
        newVec->d.y = p->vec.d.y + n.d.y*1;
        return 1;
    }

    /* yes, reflect at normal */

    vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

    cxf = (p->vec.d.x * NORM_LEN) / vec_len;
    cyf = (p->vec.d.y * NORM_LEN) / vec_len;
    vpf = ((cxf * n.d.x) + (cyf * n.d.y));
    cxf = cxf - (2 * n.d.x * vpf) / (NORM_LEN*NORM_LEN);
    cyf = cyf - (2 * n.d.y * vpf) / (NORM_LEN*NORM_LEN);

    newVec->d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
    newVec->d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

    return 1;
#endif
}
#endif
