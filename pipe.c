/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

int pipeHasHitWall;
char pipeStencil[32*32];

static vec_t minpos, maxpos;
static int startX, startY;
static int lastX, lastY;
static void *calloutConnect = NULL;
static void *calloutLock = NULL;
static int pipeEndLocked = 0;
static int lockTime = 250;

extern screeninfo *checkFree (screeninfo *pSi);

#define SI_TMP_MARK        SI_VISITED

int numPipePixels = 0;
screeninfo *pipePixels[1000];

void setPipe (int x, int y)
{
    screeninfo *pSi;
    int            ax, ay;

    if ((x < 0) || (x > wid - 1))
        return;

    if ((y < 0) || (y > hei - 1))
        return;

    pSi = &siArray[x + y * wid];

    /* save accelleration vector. setEmpty clears it, but we may
     * need it again later when averaging with new 
     * acceleration vector 
     */
    ax = XACCEL(pSi);
    ay = YACCEL(pSi);

    setEmpty (x, y, 1);

    /* store the pixel on the pipePixels list. This list is later used to calculate
     * the accelleration vectors in the pipe
     */
    if ((getDrawFct() == DO_PIPE_ACCEL) && ((pSi->flags & SI_TMP_MARK) == 0))
    {
        XACCEL(pSi) = ax;
        YACCEL(pSi) = ay;

        if (numPipePixels < NELEMENTS(pipePixels))
            pipePixels[numPipePixels++] = pSi;
    }

    pSi->flags |= SI_IS_PIPE | SI_MARKER | SI_TMP_MARK;
}

void clearPipeAccel ()
{
    int i;

    for (i = 0; i < numPipePixels; i++ )
    {
        pipePixels[i]->flags &= ~SI_TMP_MARK;
    }

    numPipePixels = 0;
}

/* look for pipe pixels (marked with SI_TMP_MARK) which are more distant than
 * <radius> relative to x/y.
 * If found, set an accelleration vactor pointing to x/y.
 */
void updatePipeAccel (int x, int y, int radius)
{
    int i;
    vec_t accel;
    float vl;
    screeninfo *pSi; 
    int six, siy;

    for (i = 0; i < numPipePixels; )
    {
        /* get pixel to check
         */
        pSi = pipePixels[i];

        six = SI_OFFSET(pSi) % wid;
        siy = SI_OFFSET(pSi) / wid;

        /* temp. set color, will be overdrawn later in setPump()
         */
        setPixel(six, siy, COL_RED);

        accel.d.x = x - six;
        accel.d.y = y - siy;

        /* calculate and check radius
         */
        vl = sqrt(accel.d.x * accel.d.x + accel.d.y * accel.d.y);

        if (vl <= radius )
        {
            i++;
            continue;
        }

        /* outside requested radius, set accelleration vector
         */
        if (vl)
        {
            accel.d.x = (accel.d.x * 10) / vl;
            accel.d.y = (accel.d.y * 10) / vl;
        }
        else
        {
            accel.d.x = accel.d.y = 0;
        }

        /* if this is an old pipe, calculate the averade accel vector */
//        if (pSi->flags & (SI_IS_PIPE | SI_IS_PUMP) == (SI_IS_PIPE | SI_IS_PUMP))
        {
            accel.d.x += XACCEL(pSi);
            accel.d.y += YACCEL(pSi);

            accel.d.x /= 2;
            accel.d.y /= 2;
        }


        setPump (six, siy, &accel);

        pSi->flags &= ~SI_TMP_MARK;

        pipePixels[i] = pipePixels[numPipePixels-1];
        numPipePixels--;
    }
}



int
doDrawPipe (int x, int y, int dx, int dy, vec_t *pipeLimitTgt, unsigned int pipeLimitLen)
{
    int xx, yy;
    int px, py;
    unsigned int l;
    int lx, ly;
    screeninfo *pSi, *newSi;
    int pass;

    /* pass 1 just draws inner (free) part of the pipe. Pass 2 draws the wall
     * after trying to warp out particles on the wall pixels.
     */
    for (pass = 0; pass < 2; pass++)
    {
        for (xx = 0; xx < 32; xx++)
        {
            for (yy = 0; yy < 32; yy++)
            {
                px = x + xx - 16 + CORR_X;
                py = y + yy - 16 + CORR_Y;

                if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei))
                    continue;


                if (pipeStencil[xx + yy*32] == 0)
                    continue;

                pSi = &siArray[px + py*wid];

                /* when drawing a "pipe line" limit the pipe to a certain radius around the
                 * start of the line.
                 * This is required when connecting pipes through a surface.
                 */
                if (pipeLimitTgt != NULL)
                {
                    lx = (px - pipeLimitTgt->d.x);
                    ly = (py - pipeLimitTgt->d.y);
                    l = lx*lx + ly*ly;

                    if (l >= pipeLimitLen)
                        continue;
                }

                if (pass == 0)
                {
                    if (pipeStencil[xx + yy*32] == 1)
                    {
                        setPipe (px, py);
                    }
                    else if ((pSi->flags & SI_IS_PIPE) == 0)
                    {
                        pSi->flags |= SI_PUSHBORDER;
                    }
                }
                else if (pipeStencil[xx + yy*32] > 1)
                {
                    if ((pSi->flags & SI_IS_PIPE) == 0)
                    {
                        if (pSi->particleList > SCR_BRICK)
                        {
                            /* relocate particles on pixel */
                            newSi = find_adjacent (px, py, x-12, y-12, 24, 24, (FUNCPTR)checkFree);

                            if (newSi)
                                warpParticles (pSi, newSi);
                        }

                        setBrick (px, py);
                        pSi->flags &= ~SI_PUSHBORDER;
                        pSi->flags |= SI_MARKER;
                    }
                }
            }
        }
    }

    makeNormals (x - 17, y - 17, x + 17, y + 17, NORM_SIMPLE_SAVE);

    if (getDrawFct() == DO_PIPE_ACCEL)
    {
        /* accellerate all pipe pixels which are more distant than radius 16 to the
         * current pixel. This makes sure that the direction is correct and also
         * pushes particles away from the wall a little bit
         */
        updatePipeAccel (x, y, 16);
    }

    return 1;
}

screeninfo *checkPipe (screeninfo *pSi)
{
    if ((pSi->flags & (SI_IS_PIPE|SI_MARKER)) == SI_IS_PIPE)
        return pSi;

    return NULL;
}


int
doCheckPipeEnd (int x, int y, int dx, int dy, vec_t *pipeEnd)
{
    if ((unsigned int)x >= wid)
        return 0;

    if ((unsigned int)y >= hei)
        return 0;

    if ((siArray[x + y*wid].flags & (SI_IS_PIPE|SI_MARKER)) != SI_IS_PIPE)
    {
        pipeEnd->d.x = x;
        pipeEnd->d.y = y;
        return 0;
    }

    return 1;
}

/* find center of nearest pipe
 */
int findNearestPipe (int x, int y, int *px, int *py)
{
    screeninfo *pSi;
    vec_t pipeStart;
    vec_t pipeEnd;
    vec_t searchTgt;

    /* find adjacent pipe */
    pSi = find_adjacent (x, y, x-40, y-40, 80, 80, (FUNCPTR)checkPipe);

    if (pSi == NULL)
        return 0;

    pipeStart.d.x = SI2X(pSi);
    pipeStart.d.y = SI2Y(pSi);
    pipeEnd = pipeStart;

    searchTgt.d.x = pipeStart.d.x + (pipeStart.d.x - x) * 2;
    searchTgt.d.y = pipeStart.d.y + (pipeStart.d.y - y) * 2;

    /* find center of pipe by continuing search in this direction
     * until we hit a non-pipe pixel
     */
     drawLineF (pipeStart.d.x, pipeStart.d.y,
                searchTgt.d.x, searchTgt.d.y,
                (FUNCPTR)doCheckPipeEnd, (intptr_t)&pipeEnd, 2, 3, 4);

     *px = (pipeStart.d.x + pipeEnd.d.x) / 2;
     *py = (pipeStart.d.y + pipeEnd.d.y) / 2;

     return 1;
}


screeninfo *checkWall (screeninfo *pSi)
{
    if ((pSi->particleList == SCR_BRICK)  && ((pSi->flags & SI_MARKER) == 0))
        return pSi;

    return NULL;
}

int
doCheckWallEnd (int x, int y, int dx, int dy, vec_t *wallEnd)
{
    if ((unsigned int)x >= wid)
        return 0;

    if ((unsigned int)y >= hei)
        return 0;

    if (siArray[x + y*wid].particleList != SCR_BRICK)
    {
        wallEnd->d.x = x;
        wallEnd->d.y = y;
        return 0;
    }

    return 1;
}


/* find nearest free->wall and wall->free transition
 */
int findNearestWall (int x, int y, int *px, int *py)
{
    screeninfo *pSi;
    vec_t wallStart;
    vec_t wallEnd;
    vec_t searchTgt;

    /* find adjacent wall */
    pSi = find_adjacent (x, y, x-40, y-40, 80, 80, (FUNCPTR)checkWall);

    if (pSi == NULL)
        return 0;

    wallStart.d.x = SI2X(pSi);
    wallStart.d.y = SI2Y(pSi);
    wallEnd = wallStart;

    searchTgt.d.x = wallStart.d.x + (wallStart.d.x - x) * 32;
    searchTgt.d.y = wallStart.d.y + (wallStart.d.y - y) * 32;

    /* find other side of wall by continuing search in this direction
     * until we hit a free pixel
     */
     drawLineF (wallStart.d.x, wallStart.d.y,
                searchTgt.d.x, searchTgt.d.y,
                (FUNCPTR)doCheckWallEnd, (intptr_t)&wallEnd, 2, 3, 4);

     *px = wallEnd.d.x;
     *py = wallEnd.d.y;

     return 1;
}

static scrPoly *pipeSeek;

static void initPipePoly()
{
    static int initialized = 0;

    if (initialized)
        return;

#ifndef USE_OPENGL
    pipeSeek = polyNew (10, 0, COL_LINE);
#else
    pipeSeek = polyNew (10, POLY_FILLED, COL_LINE);
#endif

    initialized = 1;
}

#define CANGLE 20
#define STEPS 6
#define SDEG        ((360 - CANGLE*2) / STEPS)
static void pipeSeekSet (int x1, int y1, int x2, int y2)
{
    static int ox1 = -1, oy1 = -1, ox2 = -1, oy2 = -1;
    float dx, dy;
    float radius  = MAX_SPEEDF * 10.0;
    float angle, rad;
    int deg;

    if ((ox1 == x1) && (ox2 == x2) && (oy1 = y1) && (oy2 == y2))
        return;

    ox1 = x1;
    ox2 = x2;
    oy1 = y1;
    oy2 = y2;

    polyClear (pipeSeek);

    dx = x2 - x1;
    dy = y2 - y1;

    angle = rads (dx, dy);

    for (deg = CANGLE; deg <= 360 - CANGLE; deg += SDEG)
    {
        rad = (deg * 2 * M_PI) / 360.0 + angle;

        polyAddCoord (pipeSeek, x1 + cos(rad) * radius, y1 - sin(rad) * radius, 0);
    }

    for (deg = 180 + CANGLE; deg <= 360 + 180 - CANGLE; deg += SDEG)
    {
        rad = (deg * 2 * M_PI) / 360.0 + angle;

        polyAddCoord (pipeSeek, x2 + cos(rad) * radius, y2 - sin(rad) * radius, 0);
    }
}

void seekPipe (int x, int y, int color)
{
    int ox, oy, px, py;


    initPipePoly();

    if (((getDrawFct() != DO_PIPE) &&
         (getDrawFct() != DO_PIPE_ACCEL)) ||
        (currentDrawMode() == DR_LINE))
    {
        polyHide (pipeSeek);
        return;
    }

    if (!shiftPressed() && findNearestPipe (x, y, &px, &py))
    {
        ox = INTERNC(x) + GRID / 2;
        oy = INTERNC(y) + GRID / 2;
        px = INTERNC(px) + GRID / 2;
        py = INTERNC(py) + GRID / 2;

        pipeSeekSet (ox, oy, px, py);

        polyShow (pipeSeek);
    }
    else if (shiftPressed() && findNearestWall (x, y, &px, &py))
    {
        ox = INTERNC(x) + GRID / 2;
        oy = INTERNC(y) + GRID / 2;
        px = INTERNC(px) + GRID / 2;
        py = INTERNC(py) + GRID / 2;

        pipeSeekSet (ox, oy, px, py);

        polyShow (pipeSeek);
    }
    else
    {
        polyHide (pipeSeek);
    }
}

void initPipeStencil ()
{
    char **wallStencil;
    char **clearStencil;
    int xx, yy;

    wallStencil = stencil;

    if (wallStencil == stencilPixmap(ST_PIXEL))
    {
        wallStencil = NULL;
        clearStencil = stencilPixmap(ST_BALL);
    }
    else if (wallStencil == stencilPixmap(ST_VSMALLBALL))
    {
        wallStencil = verysmallball2_pm;
        clearStencil = stencilPixmap(ST_PIXEL);
    }
    else if (wallStencil == stencilPixmap(ST_BALL))
    {
        clearStencil = stencilPixmap (ST_SMALLBALL);
    }
    else if (wallStencil == stencilPixmap(ST_SMALLBALL))
    {
        clearStencil = stencilPixmap (ST_VSMALLBALL);
    }
    else
    {
        return;
    }

    memset ((char*)pipeStencil, 0, sizeof(pipeStencil));

    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (clearStencil && (clearStencil[3 + yy][xx] != ' '))
            {
                pipeStencil[xx + yy*32] = 1;
            }
            else if (wallStencil && (wallStencil[3 + yy][xx] != ' '))
            {
                pipeStencil[xx + yy*32] =  2;
            }
        }
    }
}

void pipeConnectCallout (void *id, void *arg)
{
    int x = startX;
    int y = startY;
    int tx, ty;


    if (findNearestPipe (x, y, &tx, &ty))
    {
        motionVec.d.x = x - tx;
        motionVec.d.y = y - ty;

        drawLineF (tx, ty, x, y, (FUNCPTR)doDrawPipe, 0,0,0,0);

        minpos.d.x = MIN(minpos.d.x, tx);
        maxpos.d.x = MAX(maxpos.d.x, tx);
        minpos.d.y = MIN(minpos.d.y, ty);
        maxpos.d.y = MAX(maxpos.d.y, ty);
    }
}

void pipeLockCallout (void *id, void *arg)
{
    pipeEndLocked = 1;

    polyChangeColor (pipeSeek, -1, COL_RED);
}

void drawFctPipe (int x, int y, int act, int mode)
{
    int tx, ty;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    vec_t pipeLimitTgt;
    unsigned int pipeLimitLen;

    CHECK_DRAWING

    if (calloutConnect == NULL)
        calloutConnect = calloutNew();

    if (calloutLock == NULL)
        calloutLock = calloutNew();


    if (mouseButton != 1)
        return;

    switch (event)
    {
        case BUTTON_PRESS:

            pipeEndLocked = 0;

            initPipeStencil();
            startX = lastX = minpos.d.x = maxpos.d.x = x;
            startY = lastX =  minpos.d.y = maxpos.d.y = y;


            if (!shiftPressed())
            {
                /* normal mode. Wait 500ms to connect to nearest pipe 
                 */
                calloutSched (calloutConnect,  lockTime, (FUNCPTR)pipeConnectCallout, NULL);
                calloutSched (calloutLock, lockTime, (FUNCPTR)pipeLockCallout, NULL);
            }
            else if (shiftPressed() && findNearestWall (x, y, &tx, &ty))
            {
                /* shift pressed. Immedeately connect to nearest wall
                 */
                motionVec.d.x = x - tx;
                motionVec.d.y = y - ty;

                pipeLimitTgt.d.x = x;
                pipeLimitTgt.d.y = y;
                pipeLimitLen = (x-tx)*(x-tx) + (y-ty)*(y-ty);

                drawLineF (tx, ty, x, y, (FUNCPTR)doDrawPipe, (intptr_t)&pipeLimitTgt, pipeLimitLen, 3, 4);

                minpos.d.x = MIN(minpos.d.x, tx);
                maxpos.d.x = MAX(maxpos.d.x, tx);
                minpos.d.y = MIN(minpos.d.y, ty);
                maxpos.d.y = MAX(maxpos.d.y, ty);
            }
            else
            {
                doDrawPipe (x, y, 0, 0, NULL, 0);
            }

            break;

        case BUTTON_RELEASE:
            calloutUnsched (calloutConnect);
            calloutUnsched (calloutLock);

            minpos.d.x = MIN(minpos.d.x, x);
            maxpos.d.x = MAX(maxpos.d.x, x);
            minpos.d.y = MIN(minpos.d.y, y);
            maxpos.d.y = MAX(maxpos.d.y, y);

            if ((lastX != x) || (lastY != y))
            {
                drawLineF (lastX, lastY, x, y, (FUNCPTR)doDrawPipe, 0,0,0,0);
            }

            if (!shiftPressed() && pipeEndLocked && findNearestPipe (x, y, &tx, &ty))
            {
                drawLineF (x, y, tx, ty, (FUNCPTR)doDrawPipe,0,0,0,0);

                x = tx;
                y = ty;

                minpos.d.x = MIN(minpos.d.x, tx);
                maxpos.d.x = MAX(maxpos.d.x, tx);
                minpos.d.y = MIN(minpos.d.y, ty);
                maxpos.d.y = MAX(maxpos.d.y, ty);
            }
            else if (shiftPressed() && pipeEndLocked && findNearestWall (x, y, &tx, &ty))
            {
                pipeLimitTgt.d.x = x;
                pipeLimitTgt.d.y = y;
                pipeLimitLen = (x-tx)*(x-tx) + (y-ty)*(y-ty);

                drawLineF (x, y, tx, ty, (FUNCPTR)doDrawPipe,(intptr_t)&pipeLimitTgt, pipeLimitLen,0,0);

                x = tx;
                y = ty;

                minpos.d.x = MIN(minpos.d.x, tx);
                maxpos.d.x = MAX(maxpos.d.x, tx);
                minpos.d.y = MIN(minpos.d.y, ty);
                maxpos.d.y = MAX(maxpos.d.y, ty);
            }
            else
            {
                doDrawPipe (x, y, 0, 0, NULL, 0);
            }
            pipeEndLocked = 0;
            break;

        case BUTTON_MOVE:
            calloutUnsched (calloutConnect);
            calloutSched (calloutLock, lockTime, (FUNCPTR)pipeLockCallout, NULL);
            pipeEndLocked = 0;


            minpos.d.x = MIN(minpos.d.x, x);
            maxpos.d.x = MAX(maxpos.d.x, x);
            minpos.d.y = MIN(minpos.d.y, y);
            maxpos.d.y = MAX(maxpos.d.y, y);

            if ((lastX == x) && (lastY == y))
                break;

            drawLineF (lastX, lastY, x, y, (FUNCPTR)doDrawPipe, 0,0,0,0);

            break;
    }

    if (event == BUTTON_RELEASE)
        pushClear (x, y, 0);

    lastX = x;
    lastY = y;

    if (event == BUTTON_RELEASE)
        {
        /* clear all pipe markers inside the srceen rectange of the whole pipe
         */
        clearMarker (minpos.d.x-17, minpos.d.y-17, maxpos.d.x+17, maxpos.d.y+17);

        /* finish the accelleration vectors of the remaining pipe pixels. We accelerate
         * into the direction of the last pipe center we have drawn.
         */
        updatePipeAccel (x, y, 0);
        clearPipeAccel();
        }
}
