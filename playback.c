/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

tape playbackTape =
{
    .tapePos = 0,
    .tapeLen = 0,
    .recordStart = 0,
    .buffer = NULL,
    .recording = 0
};

int doRecord = 0;

static int doPlayback = 0;
static int playbackPos = 0;
static uint32 startIteration = 0;

int playbackIter (int relative)
{
    record *r;
    tape *t = &playbackTape;
    int rc = 0;

    if (!doPlayback)
        return 0;

    t->newBricks1.d.x = t->newBricks1.d.y = 1000000;
    t->newBricks2.d.x = t->newBricks2.d.y = 0;

    while (playbackPos < playbackTape.tapePos)
    {
        r = &playbackTape.buffer[playbackPos];

        if (systemTime() - startIteration < r->iteration)
        {
            rc = 1;
            break;
        }

        playbackItem (&playbackTape, playbackPos, NULL, NULL, 0, 0);

        playbackPos++;
    }

    if ((t->newBricks1.d.x <= t->newBricks2.d.x) &&
        (t->newBricks1.d.y <= t->newBricks2.d.y))
    {
        makeNormals (t->newBricks1.d.x - 17, t->newBricks1.d.y - 17,
                     t->newBricks2.d.x + 17, t->newBricks2.d.y + 17, NORM_SIMPLE_SAVE);
    }

    if (rc == 0)
    {
        if (doPlayback == 2)
        {
            /* restart */
            startPlayback (doPlayback);
            rc = 1;
        }
        else
        {
            selectButton (playButton, 0);
            doPlayback = 0;
        }
    }
    return rc;
}

void recordIndicator (void *calloutEntry, void *arg)
{
    static int toggle = 0;
    if (playbackTape.recording)
    {
        buttonSelectColor (recordButton, toggle ? COL_GRAY : COL_RED);
        toggle = 1 - toggle;
        calloutSched (calloutEntry, 500, (FUNCPTR)recordIndicator, 0);
    }
    else
    {
        buttonSelectColor (recordButton, COL_RED);
    }
}

void startRecord (int start)
{
    static void *calloutEntry = NULL;

    if (calloutEntry == NULL)
    {
        calloutEntry = calloutNew();
    }

    playbackTape.recording = start;

    if (start)
    {
        calloutSched (calloutEntry, 500, (FUNCPTR)recordIndicator, 0);
    }
    else
    {
        calloutUnsched (calloutEntry);
        buttonSelectColor (recordButton, COL_RED);
    }
}

void startPlayback (int start)
{
    doPlayback = start;
    startIteration = systemTime();
    playbackPos = 0;
}

int playbackInfo (int *pos, int *max)
{
    if (playbackTape.tapePos == 0)
        return 0;

    if (pos)
        *pos = playbackPos;

    if (max)
        *max = playbackTape.tapePos;

    return 1;
}

int playbackComplete()
{
    if ((playbackTape.tapePos > 0) && (playbackPos < playbackTape.tapePos))
        return 0;

    return 1;
}


void saveTape()
{
    char fname[SAVE_PATH_LEN];
    int findex;

    findex = getSaveFile (fname, sizeof(fname), &tapeRep);

    if (findex == -1)
        return;

    tapeSave (&playbackTape, fname, 1);
}

void tapeLoadCb (fileRep *r, int index)
{
    char fname[SAVE_PATH_LEN];

    mkFileName (fname, r, NULL, ".tape", index);

    tapeLoad (&playbackTape, fname);

    fileSelection (0, NULL);
}

void loadTape ()
{
    tapeRep.loadFunc = (FUNCPTR)tapeLoadCb;

    fileSelection (2, &tapeRep);
}
