/*
Copyright (C) 2008-2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"
#include <assert.h>

#define TRACK_STACK_SIZE    10000

static int        trackCount;
static vec_t        trackStart;
static int        trackStack[TRACK_STACK_SIZE];
static int        trackLowest;

#if 0
#define DBG(x,y,c) setPixel(x,y,c)
#else
#define DBG(x,y,c)
#endif

#define MAX_VEC_LEN 8

static int trackCb (int x, int y)
{
    screeninfo *pSi = &siArray[x + y*wid];

    if (pSi->particleList == SCR_BRICK)
    {
        if (pSi->flags & SI_TRACK_MARKER)
            return 0;

        pSi->flags |= SI_TRACK_MARKER;

        /* screen edges are always borders ..
         */
        if ((x == 0) || (y == 0) || 
            (x == wid-1) || (y == hei-1))
        {
            pSi->flags |= SI_MARKER;
            trackStart.d.x = x;
            trackStart.d.y = y;
            trackCount++;
        }

        return 1;
    }

    pSi->flags |= SI_MARKER;

    if (trackStart.d.y > trackLowest)
    {
        trackStart.d.x = x;
        trackStart.d.y = y;

        trackLowest = y;
    }
    trackCount++;


    DBG (x, y, COL_ORANGE);

    return 0;
}

void trackFinish( int wipe)
{
    int x, y;
    screeninfo *pSi;

    for (x = 0; x < wid; x++)
    {
        for (y = 0; y < hei; y++)
        {
            pSi = &siArray[x + y*wid];

            if (pSi->flags & (SI_MARKER|SI_TRACK_MARKER))
            {
                /* clear artifact wall pixels which have not been erased when
                 * drawing the polygon over the wall
                 */
                if (wipe && (pSi->flags & SI_TRACK_MARKER))
                {
                    setEmpty (x, y, 1);
                }
                pSi->flags &= ~(SI_MARKER|SI_TRACK_MARKER);
            }
        }
    }
}

#define DELPOINT(o)    {points[o].d.x = points[o].d.y = -1;}
#define ISDEL(o)    ((points[o].d.x == -1) && (points[o].d.y == -1))

#define _PREV(o,p)  {int _i; for (_i = o-1; _i >= 0; _i--) { if (!ISDEL(_i % num)) {p = _i % num; break;}}}
#define _NEXT(o,n)  {int _i; for (_i = o+1; _i < o+num; _i++) { if (!ISDEL(_i % num)) {n = _i % num; break;}}}

int smoothit (vec_t *points, int num)
{
    vec_t prevVec;
    vec_t curVec;
    vec_t nextVec;
    int i, j, k;
    int removed = 1;
    int pi, ni, nni;

    for (j = 0; (j < 30) && removed; j++)
    {
        removed = 0;
        for (k = num; k < num*2; k++)
        {
            i = k % num;

            if (ISDEL(k))
                continue;

            pi = ni = nni = -1;
            _PREV(k, pi);
            _NEXT(k, ni);
            _NEXT(ni, nni);

            assert (pi != -1);
#if 0
            assert (ni != -1); /* XXX failed once */
#else
            if (ni == -1)
            {
                printf ("%s%d: smoothit: assertion (ni != -1) failed\n", __FILE__, __LINE__);
                return 0;
            }
#endif
            assert (nni != -1);

            prevVec.d.x = points[i].d.x - points[pi].d.x;
            prevVec.d.y = points[i].d.y - points[pi].d.y;

            curVec.d.x = points[ni].d.x - points[i].d.x;
            curVec.d.y = points[ni].d.y - points[i].d.y;

            nextVec.d.x = points[nni].d.x - points[ni].d.x;
            nextVec.d.y = points[nni].d.y - points[ni].d.y;

            if (sameSlope (&prevVec, &curVec, 100))
            {
                DELPOINT(i);
                removed = 1;
            }
            else if (sameSlope (&prevVec, &nextVec, 10) && sameSlope (&prevVec, &curVec, 1))
            {
                DELPOINT(i);
                removed = 1;
            }
            else if ((abs(points[ni].d.x - points[pi].d.x) + abs(points[ni].d.y - points[pi].d.y)) <= 2)
            {
                DELPOINT(i);
                removed = 1;
            }
                     
        }
    }

    i = 0;
    for (j = 0; j < num; j++)
    {
        if (ISDEL(j))
            continue;

        points[i++] = points[j];
    }
    return i;
}

int trackSurface(screeninfo *pStartSi, vec_t **ap, int all)
{
    int                 x, y;
    vec_t         dirs[4];
    vec_t        *borderPixels;
    int                 bpIndex;
    int                 trackStackPos;
    int                 i, j;
    int                 atStart;
    int                 tx, ty;
    int                 nx, ny;
    int                 rc = 0;

//    all = 1;

    if (pStartSi->particleList != SCR_BRICK)
    {
        return 0;
    }


    x = SI2X(pStartSi);
    y = SI2Y(pStartSi);

    /* "fill" the interior of the area, mark all emtpy pixels at the outside edges
     */
    trackCount = 0;
    trackLowest = -1;
    flood (x, y, (FUNCPTR)trackCb, 0, 1);

    if (!trackCount)
        return 0;

    /* now follow the border pixels and put them into the borderPixels array
     */
    borderPixels = (vec_t*)alloca (trackCount * sizeof(vec_t));

    DBG (trackStart.d.x, trackStart.d.y, COL_RED);

    dirs[0].d.x = 1;
    dirs[0].d.y = 0;

    dirs[1].d.x = 0;
    dirs[1].d.y = 1;

    dirs[2].d.x = -1;
    dirs[2].d.y = 0;

    dirs[3].d.x = 0;
    dirs[3].d.y = -1;

    x = trackStart.d.x;
    y = trackStart.d.y;

    trackStackPos = 0;
    bpIndex = 0;

    nx = ny = 0;

    /* follow the border until we reach the start pixel. If there are multiple directions
     * to continue at a pixel, put the position to a stack and retry from there on if
     * a dead end is reached.
     */
    while (1)
    {

        /* mark the pixel as processed and put it on the list */
        siArray[x + y*wid].flags &= ~SI_MARKER;

        borderPixels[bpIndex].d.x = x;
        borderPixels[bpIndex].d.y = y;
        bpIndex++;

        atStart = 0;
        for (i = j = 0; i < 4; i++)
        {
            tx = x + dirs[i].d.x;
            ty = y + dirs[i].d.y;

            if ((tx == trackStart.d.x) && (ty == trackStart.d.y))
            {
                /* done */
                atStart = 1;
            }

            if (siArray[tx + ty*wid].flags & SI_MARKER)
            {
                if (j == 0)
                {
                    nx = tx;
                    ny = ty;
                }
                j++;
            }
        }

        if (j == 0)
        {
            /* no adjacent border pixel */

            if (atStart)
            {
                /* reached start pixel, stop
                 */
                break;
            }

            if (trackStackPos > 0)
            {
                /* pop most recent fork and continue
                 */
                trackStackPos--;
                bpIndex = trackStack[trackStackPos];

                nx = borderPixels[bpIndex].d.x;
                ny = borderPixels[bpIndex].d.y;
            }
            else
            {
                /* can not reach start pixel ...
                 */
                bpIndex = 0;
                break;
            }
        }
        else if (j > 1)
        {
            /* multiple ways to proceed here, push to stack */
            DBG (x, y, COL_ORANGE);
            if (trackStackPos < TRACK_STACK_SIZE)
            {
                trackStack[trackStackPos] = bpIndex-1;
                trackStackPos++;
            }
        }
        else
        {
            DBG (x, y, COL_BLUE);
        }

        x = nx;
        y = ny;

    }

    if (bpIndex)
    {
        vec_t *pTbl = (vec_t*)malloc (bpIndex * sizeof(vec_t));
        int pixelCount = 0;
        uint l = 0;
        uint ltmp = 0;
        int sx = 0, sy = 0;
        int dx, dy;


        for (i = rc = 0; i < bpIndex; i++)
        {
            DBG (borderPixels[i].d.x, borderPixels[i].d.y, COL_RED);

            x = INTERNC_M(borderPixels[i].d.x);
            y = INTERNC_M(borderPixels[i].d.y);

            if (rc > 0)
            {
                pixelCount++;

                /* check the distance of the next pixel
                 * If its closer than the current one we will stop here.
                 */
                if ((i+1) < bpIndex)
                {
                    dx = borderPixels[i+1].d.x - sx;
                    dy = borderPixels[i+1].d.y - sy;
                    ltmp = dx*dx + dy*dy;
                }
            }


            if (all || (i == 0) || (pixelCount == MAX_VEC_LEN) || (ltmp <= l))
            {
                pTbl[rc].d.x = x;
                pTbl[rc].d.y = y;

                rc++;

                pixelCount = 0;
                sx = SCREENC(x);
                sy = SCREENC(y);
                l = 0;
            }
            else
            {
                l = ltmp;
            }
        }

        rc = smoothit (pTbl, rc);

        *ap = pTbl;
    }


    return rc;
}



int vectorizePixels (int x, int y)
{
    vec_t *tArray;
    int i;
    int rc = 0;
    int num = trackSurface (&siArray[x + y*wid], &tArray, 0);
    float dist;

    if (num <= 0)
        return 0;

    for (i = 0; i < num; i++)
    {
        if (i == 0)
        {
            polyStart (SCREENC(tArray[i].d.x), SCREENC(tArray[i].d.y), 0);
//            polyMoveEdge (SCREENC(tArray[i].d.x), SCREENC(tArray[i].d.y), 0);
        }
        else
        {
#if 0
            dist = VEC_LEN(tArray[i].d.x - tArray[i-1].d.x, tArray[i].d.y - tArray[i-1].d.y);

            if (dist < 5 * MAX_SPEEDF)
                continue;
#endif

            if (i == num-1)
            {
                dist = VEC_LEN(tArray[i].d.x - tArray[0].d.x, tArray[i].d.y - tArray[0].d.y);

                if (dist < 5 * MAX_SPEEDF)
                    continue;
            }


            polyMoveEdge (SCREENC(tArray[i].d.x), SCREENC(tArray[i].d.y), 0);
            polyFix();
        }
    }

    trackFinish(1);
    
    if (!polyFinish())
    {
//        trackFinish (0);
        removeCurrentPoly();
    }
    else
    {
//        trackFinish (1);
        rc = 1;
    }

    redrawScreen();

    return rc;
}
