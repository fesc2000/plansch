
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"

int doProbe = 0;
vec_t probePos;

#define MIN_VAL        0x80000000
#define MAX_VAL 0x7fffffff

#define HIST_SIZE   1000

typedef struct
{
    int count;
    int min;
    int max;
    int total;
    int average;
    FLOAT average_f;
    int history[HIST_SIZE];
    int histHead;
    int histTail;
} probeValueInt;

probeValueInt *hv;


probeValueInt dens;
probeValueInt field;
probeValueInt dField;
probeValueInt mass;
probeValueInt charge;
probeValueInt speed;

void clearValue (probeValueInt *v)
{
    v->count = 0;
    v->min = 0x7fffffff;
    v->max = 0x80000000;
    v->total = 0;
    v->average = 0;
//    memset ((char*)v->history, 0, sizeof(v->history));
//    v->histHead = 0;
//    v->histTail = 0;
}

void addValue (probeValueInt *v, int value)
{
    v->count++;
    v->total += value;
    if (value > v->max)
        v->max = value;
    if (value < v->min)
        v->min = value;

    v->history[v->histHead] = value;
    v->histHead = (v->histHead + 1) % HIST_SIZE;

    if (v->histHead == v->histTail)
        v->histTail = (v->histHead + 1) % HIST_SIZE;
}

void updateValue (probeValueInt *v)
{
    if (v->count == 0)
        return;

    v->average = v->total / v->count;
    v->average_f = (FLOAT)v->total / (FLOAT)v->count;

}

void probeClear (void)
{
    clearValue (&dens);
    clearValue (&field);
    clearValue (&dField);
    clearValue (&mass);
    clearValue (&charge);
    clearValue (&speed);
}

void probeAdd (int x, int y)
{
    screeninfo *pSi = &siArray[x + y * wid];
    particle *p;

    addValue (&dens, densArray[x + y * wid]);

    if (!options->useGpu)
        addValue (&field, FIELD(pSi));

    addValue (&dField, diffArray[x + y * wid]);

    for (p = pSi->particleList; p > SCR_BRICK; p = p->next)
    {
        addValue (&mass, p->mass);
        addValue (&charge, CHARGE_SIGN(p));
        addValue (&speed, (int)VEC_LEN(p->vec.d.x, p->vec.d.y));
    }
}

void probeStart (int enable)
{
    doProbe = enable;

    if (enable)
    {
        clearValue (&dens);
        clearValue (&field);
        clearValue (&dField);
        clearValue (&mass);
        clearValue (&charge);
        clearValue (&speed);

        hv = &dField;
    }
}

void probe( int x, int y)
{
    int xx, yy;
    int px, py;

    if (!doProbe)
        return;

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei))
                continue;

            probeAdd (px, py);
        }
    
}

void showValue (char *name, probeValueInt *v, int line)
{
    char    m[128];

    if (v->count)
    {
        sprintf (m, "%s: %.2f %d/%d", name, v->average_f, v->min, v->max);
    }
    else
    {
        sprintf (m, "%s: n/a                      ", name);
    }
    statText (line, m);
}


void probeHistory (int sx, int sy, int w, int h)
{
    SDL_Rect rect;
    int head;
    int val;
    int scale;
    int i;
    int vx;

    if (!doProbe)
        return;

    head = hv->histHead + HIST_SIZE;

    scale = hv->max - hv->min;

    if (abs(hv->max) > abs(hv->min))
        scale = abs(hv->max);
    else
        scale = abs(hv->min);

    if (scale == 0)
    {
        scale = 1;
    }

    head += HIST_SIZE;

    rect.x = sx;
    rect.y = sy;
    rect.h = h;
    rect.w = w;

    SDL_FillRect(screen, &rect, COL_BLUE);    

    scale=16;
    
    for (i = 0; i < h; i++)
    {
        val = hv->history[(head - i) % HIST_SIZE];

        vx = sx + w/2 + val/scale;

        if (vx < sx)
            vx = sx;
        else if (vx >= sx + w)
            vx = sx + w-1;

        SCR_SET  (vx, sy + h - 1 - i, COL_RED);
        SCR_SET  (sx + w/2, sy + h - 1 - i, COL_ORANGE);
    }
}

void probeShow ()
{
    int            line;

    if (!doProbe)
        return;

    updateValue (&dens);
    updateValue (&field);
    updateValue (&dField);
    updateValue (&mass);
    updateValue (&charge);
    updateValue (&speed);

    line = 6;

    showValue ("Dens ", &dens, line++);
    showValue ("Fld  ", &field, line++);
    showValue ("dF/dT", &dField, line++);
    showValue ("Mass ", &mass, line++);
    showValue ("Chrg ", &charge, line++);
    showValue ("Spd  ", &speed, line++);
}
