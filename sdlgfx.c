/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/*
 SDL_gfxPrimitives - Graphics primitives for SDL surfaces
 LGPL (c) A. Schiffler
*/

#include "plansch.h"

/* following code is copied from the SDL_gfx library. Problem is that these
 * functions do not store the alpha channel as we need it. 
 */

/* ----- Defines for pixel clipping tests */
#define clip_xmin(surface) surface->clip_rect.x
#define clip_xmax(surface) surface->clip_rect.x+surface->clip_rect.w-1
#define clip_ymin(surface) surface->clip_rect.y
#define clip_ymax(surface) surface->clip_rect.y+surface->clip_rect.h-1

/* ----- Pixel - fast, no blending, no locking, clipping */
int myPixelColor(SDL_Surface * dst, Sint16 x, Sint16 y, Uint32 color)
{
    int bpp;
    Uint8 *p;

    /*
     * Honor clipping setup at pixel level
     */
    if ((x >= clip_xmin(dst)) && (x <= clip_xmax(dst)) && (y >= clip_ymin(dst)) && (y <= clip_ymax(dst))) {
        /*
         * Get destination format
         */
        bpp = dst->format->BytesPerPixel;
        p = (Uint8 *) dst->pixels + y * dst->pitch + x * bpp;
        switch (bpp) {
        case 1:
            *p = color;
            break;
        case 2:
            *(Uint16 *) p = color;
            break;
        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                p[0] = (color >> 16) & 0xff;
                p[1] = (color >> 8) & 0xff;
                p[2] = color & 0xff;
            } else {
                p[0] = color & 0xff;
                p[1] = (color >> 8) & 0xff;
                p[2] = (color >> 16) & 0xff;
            }
            break;
        case 4:
            *(Uint32 *) p = color;
            break;
        }                       /* switch */

    }
    return (0);
}

int myhlineColorStore(SDL_Surface * dst, Sint16 x1, Sint16 x2, Sint16 y, Uint32 color)
{
    Sint16 left, right, top, bottom;
    Uint8 *pixel, *pixellast;
    int dx;
    int pixx, pixy;
    Sint16 w;
    Sint16 xtmp;
    int result = -1;

    /*
     * Get clipping boundary
     */
    left = dst->clip_rect.x;
    right = dst->clip_rect.x + dst->clip_rect.w - 1;
    top = dst->clip_rect.y;
    bottom = dst->clip_rect.y + dst->clip_rect.h - 1;

    /*
     * Check visibility of hline
     */
    if ((x1<left) && (x2<left)) {
     return(0);
    }
    if ((x1>right) && (x2>right)) {
     return(0);
    }
    if ((y<top) || (y>bottom)) {
     return (0);
    }
    /*
     * Clip x
     */
    if (x1 < left) {
        x1 = left;
    }
    if (x2 > right) {
        x2 = right;
    }
    /*
     * Swap x1, x2 if required
     */
    if (x1 > x2) {
        xtmp = x1;
        x1 = x2;
        x2 = xtmp;
    }
    /*
     * Calculate width
     */
    w = x2 - x1;
    /*
     * Sanity check on width
     */
    if (w < 0) {
        return (0);
    }
#if 0
    /*
     * Lock surface
     */
    SDL_LockSurface(dst);
#endif
    /*
     * More variable setup
     */
    dx = w;
    pixx = dst->format->BytesPerPixel;
        pixy = dst->pitch;
        pixel = ((Uint8 *) dst->pixels) + pixx * (int) x1 + pixy * (int) y;
        /*
         * Draw
         */
        switch (dst->format->BytesPerPixel) {
        case 1:
            memset(pixel, color, dx);
            break;
        case 2:
            pixellast = pixel + dx + dx;
            for (; pixel <= pixellast; pixel += pixx) {
                *(Uint16 *) pixel = color;
            }
            break;
        case 3:
            pixellast = pixel + dx + dx + dx;
            for (; pixel <= pixellast; pixel += pixx) {
                if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                    pixel[0] = (color >> 16) & 0xff;
                    pixel[1] = (color >> 8) & 0xff;
                    pixel[2] = color & 0xff;
                } else {
                    pixel[0] = color & 0xff;
                    pixel[1] = (color >> 8) & 0xff;
                    pixel[2] = (color >> 16) & 0xff;
                }
            }
            break;
        default:                /* case 4 */
            dx = dx + dx;
            pixellast = pixel + dx + dx;
            for (; pixel <= pixellast; pixel += pixx) {
                *(Uint32 *) pixel = color;
            }
            break;
        }
        /*
         * Unlock surface
         */
        SDL_UnlockSurface(dst);
        /*
         * Set result code
         */
        result = 0;
    return (result);
}

/* ----- Vertical line */
int myvlineColor(SDL_Surface * dst, Sint16 x, Sint16 y1, Sint16 y2, Uint32 color)
{
    Sint16 left, right, top, bottom;
    Uint8 *pixel, *pixellast;
    int dy;
    int pixx, pixy;
    Sint16 h;
    Sint16 ytmp;
    int result = -1;
    /*
     * Get clipping boundary
     */
    left = dst->clip_rect.x;
    right = dst->clip_rect.x + dst->clip_rect.w - 1;
    top = dst->clip_rect.y;
    bottom = dst->clip_rect.y + dst->clip_rect.h - 1;

    /*
     * Check visibility of vline
     */
    if ((x<left) || (x>right)) {
     return (0);
    }
    if ((y1<top) && (y2<top)) {
     return(0);
    }
    if ((y1>bottom) && (y2>bottom)) {
     return(0);
    }
    /*
     * Clip y
     */
    if (y1 < top) {
        y1 = top;
    }
    if (y2 > bottom) {
        y2 = bottom;
    }
    /*
     * Swap y1, y2 if required
     */
    if (y1 > y2) {
        ytmp = y1;
        y1 = y2;
        y2 = ytmp;
    }
    /*
     * Calculate height
     */
    h = y2 - y1;
    /*
     * Sanity check on height
     */
    if (h < 0) {
        return (0);
    }

#if 0
    /*
     * No alpha-blending required
     */
    /*
     * Setup color
     */
    colorptr = (Uint8 *) & color;
    if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
        color = SDL_MapRGBA(dst->format, colorptr[0], colorptr[1], colorptr[2], colorptr[3]);
    } else {
        color = SDL_MapRGBA(dst->format, colorptr[3], colorptr[2], colorptr[1], colorptr[0]);
    }
#endif

#if 0
    /*
     * Lock surface
     */
    SDL_LockSurface(dst);
#endif
    /*
     * More variable setup
     */
    dy = h;
    pixx = dst->format->BytesPerPixel;
    pixy = dst->pitch;
    pixel = ((Uint8 *) dst->pixels) + pixx * (int) x + pixy * (int) y1;
    pixellast = pixel + pixy * dy;
    /*
     * Draw
     */
    switch (dst->format->BytesPerPixel) {
    case 1:
        for (; pixel <= pixellast; pixel += pixy) {
            *(Uint8 *) pixel = color;
        }
        break;
    case 2:
        for (; pixel <= pixellast; pixel += pixy) {
            *(Uint16 *) pixel = color;
        }
        break;
    case 3:
        for (; pixel <= pixellast; pixel += pixy) {
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                pixel[0] = (color >> 16) & 0xff;
                pixel[1] = (color >> 8) & 0xff;
                pixel[2] = color & 0xff;
            } else {
                pixel[0] = color & 0xff;
                pixel[1] = (color >> 8) & 0xff;
                pixel[2] = (color >> 16) & 0xff;
            }
        }
        break;
    default:                /* case 4 */
        for (; pixel <= pixellast; pixel += pixy) {
            *(Uint32 *) pixel = color;
        }
        break;
    }
    /*
     * Unlock surface
     */
    SDL_UnlockSurface(dst);
    /*
     * Set result code
     */
    result = 0;

    return (result);
}

int gfxPrimitivesCompareInt(const void *a, const void *b);
static int *gfxPrimitivesPolyInts = NULL;
static int gfxPrimitivesPolyAllocated = 0;

int myfilledPolygonColor(SDL_Surface * dst, const Sint16 * vx, const Sint16 * vy, int n, Uint32 color)
{
    int result;
    int i;
    int y, xa, xb;
    int miny, maxy;
    int x1, y1;
    int x2, y2;
    int ind1, ind2;
    int ints;
    /*
     * Sanity check
     */
    if (n < 3) {
        return -1;
    }
    /*
     * Allocate temp array, only grow array
     */
    if (!gfxPrimitivesPolyAllocated) {
        gfxPrimitivesPolyInts = (int *) malloc(sizeof(int) * n);
        gfxPrimitivesPolyAllocated = n;
    } else {
        if (gfxPrimitivesPolyAllocated < n) {
            gfxPrimitivesPolyInts = (int *) realloc(gfxPrimitivesPolyInts, sizeof(int) * n);
            gfxPrimitivesPolyAllocated = n;
        }
    }
    /*
     * Determine Y maxima
     */
    miny = vy[0];
    maxy = vy[0];
    for (i = 1; (i < n); i++) {
        if (vy[i] < miny) {
            miny = vy[i];
        } else if (vy[i] > maxy) {
            maxy = vy[i];
        }
    }
    /*
     * Draw, scanning y
     */
    result = 0;
    for (y = miny; (y <= maxy); y++) {
        ints = 0;
        for (i = 0; (i < n); i++) {
            if (!i) {
                ind1 = n - 1;
                ind2 = 0;
            } else {
                ind1 = i - 1;
                ind2 = i;
            }
            y1 = vy[ind1];
            y2 = vy[ind2];
            if (y1 < y2) {
                x1 = vx[ind1];
                x2 = vx[ind2];
            } else if (y1 > y2) {
                y2 = vy[ind1];
                y1 = vy[ind2];
                x2 = vx[ind1];
                x1 = vx[ind2];
            } else {
                continue;
            }
            if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
                gfxPrimitivesPolyInts[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
            }
        }
        qsort(gfxPrimitivesPolyInts, ints, sizeof(int), gfxPrimitivesCompareInt);
        for (i = 0; (i < ints); i += 2) {
            xa = gfxPrimitivesPolyInts[i] + 1;
            xa = (xa >> 16) + ((xa & 32768) >> 15);
            xb = gfxPrimitivesPolyInts[i+1] - 1;
            xb = (xb >> 16) + ((xb & 32768) >> 15);
            /* XXX changed: using our own hline function */
            result |= myhlineColorStore(dst, xa, xb, y, color);
        }
    }
    return (result);
}

/* --------- Clipping routines for line */
/* Clipping based heavily on code from                       */
/* http://www.ncsa.uiuc.edu/Vis/Graphics/src/clipCohSuth.c   */
#define CLIP_LEFT_EDGE   0x1
#define CLIP_RIGHT_EDGE  0x2
#define CLIP_BOTTOM_EDGE 0x4
#define CLIP_TOP_EDGE    0x8
#define CLIP_INSIDE(a)   (!a)
#define CLIP_REJECT(a,b) (a&b)
#define CLIP_ACCEPT(a,b) (!(a|b))
static int clipEncode(Sint16 x, Sint16 y, Sint16 left, Sint16 top, Sint16 right, Sint16 bottom)
{
    int code = 0;
    if (x < left) {
        code |= CLIP_LEFT_EDGE;
    } else if (x > right) {
        code |= CLIP_RIGHT_EDGE;
    }
    if (y < top) {
        code |= CLIP_TOP_EDGE;
    } else if (y > bottom) {
        code |= CLIP_BOTTOM_EDGE;
    }
    return code;
}
static int clipLine(SDL_Surface * dst, Sint16 * x1, Sint16 * y1, Sint16 * x2, Sint16 * y2)
{
    Sint16 left, right, top, bottom;
    int code1, code2;
    int draw = 0;
    Sint16 swaptmp;
    float m;
    /*
     * Get clipping boundary
     */
    left = dst->clip_rect.x;
    right = dst->clip_rect.x + dst->clip_rect.w - 1;
    top = dst->clip_rect.y;
    bottom = dst->clip_rect.y + dst->clip_rect.h - 1;
    while (1) {
        code1 = clipEncode(*x1, *y1, left, top, right, bottom);
        code2 = clipEncode(*x2, *y2, left, top, right, bottom);
        if (CLIP_ACCEPT(code1, code2)) {
            draw = 1;
            break;
        } else if (CLIP_REJECT(code1, code2))
            break;
        else {
            if (CLIP_INSIDE(code1)) {
                swaptmp = *x2;
                *x2 = *x1;
                *x1 = swaptmp;
                swaptmp = *y2;
                *y2 = *y1;
                *y1 = swaptmp;
                swaptmp = code2;
                code2 = code1;
                code1 = swaptmp;
            }
            if (*x2 != *x1) {
                m = (*y2 - *y1) / (float) (*x2 - *x1);
            } else {
                m = 1.0f;
            }
            if (code1 & CLIP_LEFT_EDGE) {
                *y1 += (Sint16) ((left - *x1) * m);
                *x1 = left;
            } else if (code1 & CLIP_RIGHT_EDGE) {
                *y1 += (Sint16) ((right - *x1) * m);
                *x1 = right;
            } else if (code1 & CLIP_BOTTOM_EDGE) {
                if (*x2 != *x1) {
                    *x1 += (Sint16) ((bottom - *y1) / m);
                }
                *y1 = bottom;
            } else if (code1 & CLIP_TOP_EDGE) {
                if (*x2 != *x1) {
                    *x1 += (Sint16) ((top - *y1) / m);
                }
                *y1 = top;
            }
        }
    }
    return draw;
}
/* ----- Line */
/* Non-alpha line drawing code adapted from routine          */
/* by Pete Shinners, pete@shinners.org                       */
/* Originally from pygame, http://pygame.seul.org            */
#define ABS(a) (((a)<0) ? -(a) : (a))
int mylineColor(SDL_Surface * dst, Sint16 x1, Sint16 y1, Sint16 x2, Sint16 y2, Uint32 color)
{
    int pixx, pixy;
    int x, y;
    int dx, dy;
    int sx, sy;
    int swaptmp;
    Uint8 *pixel;

    /*
     * Clip line and test if we have to draw
     */
    if (!(clipLine(dst, &x1, &y1, &x2, &y2))) {
        return (0);
    }

    /*
     * Test for special cases of straight lines or single point
     */
    if (x1 == x2) {
        if (y1 < y2) {
            return (myvlineColor(dst, x1, y1, y2, color));
        } else if (y1 > y2) {
            return (myvlineColor(dst, x1, y2, y1, color));
        } else {
            return (myPixelColor(dst, x1, y1, color));
        }
    }
    if (y1 == y2) {
        if (x1 < x2) {
            return (myhlineColorStore(dst, x1, x2, y1, color));
        } else if (x1 > x2) {
            return (myhlineColorStore(dst, x2, x1, y1, color));
        }
    }
    /*
     * Variable setup
     */
    dx = x2 - x1;
    dy = y2 - y1;
    sx = (dx >= 0) ? 1 : -1;
    sy = (dy >= 0) ? 1 : -1;
    /* Lock surface */
#if 0
    if (SDL_MUSTLOCK(dst)) {
        if (SDL_LockSurface(dst) < 0) {
            return (-1);
        }
    }
#endif

#if 0
    /*
     * No alpha blending - use fast pixel routines
     */
    /*
     * Setup color
     */
    colorptr = (Uint8 *) & color;
    if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
        color = SDL_MapRGBA(dst->format, colorptr[0], colorptr[1], colorptr[2], colorptr[3]);
    } else {
        color = SDL_MapRGBA(dst->format, colorptr[3], colorptr[2], colorptr[1], colorptr[0]);
    }
#endif

    /*
     * More variable setup
     */
    dx = sx * dx + 1;
    dy = sy * dy + 1;
    pixx = dst->format->BytesPerPixel;
    pixy = dst->pitch;
    pixel = ((Uint8 *) dst->pixels) + pixx * (int) x1 + pixy * (int) y1;
    pixx *= sx;
    pixy *= sy;
    if (dx < dy) {
        swaptmp = dx;
        dx = dy;
        dy = swaptmp;
        swaptmp = pixx;
        pixx = pixy;
        pixy = swaptmp;
    }
    /*
     * Draw
     */
    x = 0;
    y = 0;
    switch (dst->format->BytesPerPixel) {
    case 1:
        for (; x < dx; x++, pixel += pixx) {
            *pixel = color;
            y += dy;
            if (y >= dx) {
                y -= dx;
                pixel += pixy;
            }
        }
        break;
    case 2:
        for (; x < dx; x++, pixel += pixx) {
            *(Uint16 *) pixel = color;
            y += dy;
            if (y >= dx) {
                y -= dx;
                pixel += pixy;
            }
        }
        break;
    case 3:
        for (; x < dx; x++, pixel += pixx) {
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                pixel[0] = (color >> 16) & 0xff;
                pixel[1] = (color >> 8) & 0xff;
                pixel[2] = color & 0xff;
            } else {
                pixel[0] = color & 0xff;
                pixel[1] = (color >> 8) & 0xff;
                pixel[2] = (color >> 16) & 0xff;
            }
            y += dy;
            if (y >= dx) {
                y -= dx;
                pixel += pixy;
            }
        }
        break;
    default:                /* case 4 */
        for (; x < dx; x++, pixel += pixx) {
            *(Uint32 *) pixel = color;
            y += dy;
            if (y >= dx) {
                y -= dx;
                pixel += pixy;
            }
        }
        break;
    }

    /* Unlock surface */
    if (SDL_MUSTLOCK(dst)) {
        SDL_UnlockSurface(dst);
    }
    return (0);
}

/* ---- Polygon */
int mypolygonColor(SDL_Surface * dst, const Sint16 * vx, const Sint16 * vy, int n, Uint32 color)
{
    int result;
    int i;
    const Sint16 *x1, *y1, *x2, *y2;
    /*
     * Sanity check
     */
    if (n < 3) {
        return (-1);
    }
    /*
     * Pointer setup
     */
    x1 = x2 = vx;
    y1 = y2 = vy;
    x2++;
    y2++;
    /*
     * Draw
     */
    result = 0;
    for (i = 1; i < n; i++) {
        result |= mylineColor(dst, *x1, *y1, *x2, *y2, color);
        x1 = x2;
        y1 = y2;
        x2++;
        y2++;
    }
    result |= mylineColor(dst, *x1, *y1, *vx, *vy, color);
    return (result);
}

int gfxPrimitivesCompareInt(const void *a, const void *b)
{
    return (*(const int *) a) - (*(const int *) b);
}

#undef WIN32
#include "sdl_gfx/SDL_rotozoom.c"
