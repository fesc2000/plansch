#define DID_IGNORE  -1

/* touching pixel. dir = 0..7 (NW, W, SW, N, S, NE, E, SE) */
#define DID_TOUCH(dir)        (dir)



struct
{
    int diff1,
    int diff2,
    int src,
    int dst
} cTable [] =
{
    {        BLEFT,                DID_IGNORE,                BLEFT_I,    BRIGHT_I   },
    {        BRIGHT,                DID_IGNORE,                BRIGHT_I,   BBLEFT_I   },
    {        BTOP,                DID_IGNORE,                BTOP_I,            BBOT_I   },
    {        BBOT,                DID_IGNORE,                BBOT_I,            BTOP_I   },
    {        BRIGHTTOP,        BNW_ALL,                BLEFT_I,    BBOT_I  },


    }
















};








int pointDiff (vecElement *e1, vecElement *e2, tuple *vec)
{
    tuple v;

    v.d.x = e2->u.point.pos.d.x - e1->u.point.d.x;
    v.d.y = e2->u.point.pos.d.y - e1->u.point.d.y;

    if (vec)
        *vec = v;

    return vecId (&v) & ~e1->u.point.connected;
}


int touch_hv (vecElement *e, vecElement *np, vecElement *nnp, int *src, int *dst)
{
    tuple vec;
    int srcp, int *dstp;

    srcp = pointDiff (p, np, &vec);

    if ((abs)vec.d.x > 1) || (abs(vec.d.y) > 1)
        return 0;

    dstp = pointDiff (np, p, NULL);

    if ((srcp == BLEFT)

}




void connectWithNext (vecElement *e, int *src, int *dst)
{
    nectElement *np, *nnp;
    tuple vec;
    int srcMask, nMask, nnMask;

    np = nextPoint (p);
    nnp = nextPoint (np);

    vec.d.x = np->u.point.pos.d.x - p->u.point.pos.d.x;
    vec.d.y = np->u.point.pos.d.y - p->u.point.pos.d.y;
    srcMask = vecId (&vec) & ~p->u.point.connected;

    vec.d.x = -vec.d.x;
    vec.d.y = -vec.d.y;
    nMask = vecId (&vec) & ~np->u.point.connected;

    vec.d.x = nnp->u.point.pos.d.x - np->u.point.pos.d.x;
    vec.d.y = nnp->u.point.pos.d.y - np->u.point.pos.d.y;
    nnMask = vecId (&vec);
    

    if (touch_hv (e, srcMask, np, nMask, nnp, nnMask, src, dst))
        return;
}

