/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

static char helpLang[100] = "en";

struct
{
    char *lang;
    char *helpId;
    char *tooltip;
    char *longHelp;
} helpStrings[] =
{
    {
    "en",
            "clOscillator", "Oscillating particle at fixed position",
            "Create GPU particles at a fixed position with an oscillating charge.\n"
            "The amplitude is defined my the mass slider, the frequency by the length of the\n"
            "specified accelleration vector.\n" },
    {
    "en",
            "actPotential", "Action potential threshold",
            "If the color difference between linked particles B and A is greater than this\n"
            "threshold value (col(B) - col(A) > t) then A inherits the color information from B.\n"
            "This check is done independently for all three color channels.\n"
            "Particle a will try to restore it's original color based on the configured color restore rate.\n" },
    {
    "en",
            "Select screen area", "Select screen area",
            "Marks a screen area. All draw operations are limited to this area, and\n"
            "the area's contents can be cut or copied into a stencil.\n"
    },
    {
    "en",
            "Copy selected area", "Copy selected area",
            "Copies the current selection into a stencil."
    },
    {
    "en",
            "Cut selected area", "Cut selected area",
            "Cuts the current selection into a stencil."
    },
    {
    "en",
            "Save selected area", "Save selected area",
            "Saves the current selection."
    },
    {
    "en",
            "particleTrails", "Particle Trails",
            "If enabled particles are painted with a trail."
    },
    {
    "en",
            "colMergeSpeed", "Color averaging rate",
            "When two particles collide their color values are re-calculated using the formula\n"
            " \n"
            "    c = c1 + (c2 - c1) * n \n"
            " \n"
            "This setting adjusts the parameter <n>. Particles will attempt to retain their\n"
            "original color.\n"
    },
    {
    "en",
            "colRestoreSpeed", "Color restore rate",
            "The setting adjust how fast a particle's original color is restored."
    },
    {
    "en",
            "particleBlur", "Color Blur",
            "Lower values increase the color blur (a pixel's color is averaged with the color\n"
            "of neighbour pixels."
    },
    {
    "en",
            "particleFade", "Color Fading",
            "Higher values increase the tail created by particles."
    },
    {
    "en",
            "Mark", "Mark Particles",
            "Left button marks particles, right button unmarks particles (when shift is pressed\n"
            "all particles are unmarked). If CTRL is pressed, all connected particles are also marked.\n"
            "The followion operations can be performed on marked particles:\n"
            "- Changing particle properties: by selecting a property button/slider with the middle mouse\n"
            "  button (border will become red) the current setting of this property and future modifications\n"
            "  are applied to all marked particles.\n"
            "- Dragging particles: all marked particles are dragged (instead of those under the current stencil).\n"
            "- Erasing particles (\"x\" key or particle-erase button): all marked particles are erased.\n"
    },
    {
    "en",
            "Mark All", "Mark All Particles",
            "Marks all particles on the screen (or those inside the current selection).\n"
    },
    {
    "en",
            "wallFriction", "Wall Friction",
            "Lowest value means there is no wall friction (particles are reflected at the wall's\n"
            "normal vector), maximum value means maximum friction (particle's moevement is effectively\n"
            "reversed when hitting a wall).\n"
    },
    {
    "en",
            "particleLifetime", "Particle Lifetime",
            "If set to a value below the maximum, a particle's lifetime is limited to a random number\n"
            "of iterations between 1 and the square of this setting's value."
    },
    {
    "en",
            "GPUWIMP", "GPU Calculated Particles",
            "[GPU] Enables particles calculated by the GPU. They do not interace with \"CPU particles\"\n"
            "except via the force field.\n"
            },
    {
    "en",
            "GravityVector", "Gravity Vector",
            "Gravity Vector affecting all particles"
    },
    {
    "en",
            "preassure", "Spring Factor [CPU]",
            "Defines the additional impuls which is applied to very close or\n"
            "densly packed particles.\n"
            "High values cause explosion effects."
    },
    {
    "en",
            "wallDampingI", "Wall Damping [CPU]",
            "Low values cause particles to lose speed when colliding with a wall.\n"
            "When wall heating is enabled, high (low) values mean that much \n"
            "(fewer) energy is exchanged between particle and wall."
    },
    {
    "en",
            "decelLog", "Particle Deceleration",
            "Low values will decelerate particles."
    },
    {
    "en",
            "Gravity", "Enable/disable force field",
            "When enabled, each particle generates a force field and is itself affected\n"
            "by the force field. The strength of the generated force field depends on the\n"
            "particle's charge and it's weight. Particles with a positive charge generate\n"
            "a positive force field and are themselves attracted by a positive field (and\n"
            "repelled by a negative force field). Particles with a negative charge behave\n"
            "the other way round, and particles with zero charge neither generate a field\n"
            "nor are they affected by it.\n"
            "The force field does not propagate immedeately, heavy paticles or rapid density\n"
            "changes on a pixel might cause force waves (e.g. when particles fusion).\n"
            "Wall elements also generated a force field based on the current charge/mass\n"
            "setting."
    },
    {
    "en",
            "particleForceInv", "Invert Force",
            "Reverses the effect of the force field to particles. Particles with positive\n"
            "charge are attracted (repelled) by a negative (positive) field and vice versa."
    },
    {
    "en",
            "GShow", "Show Force Field",
            "Visualizes the force field and force waves."
    },
    {
    "en",
            "potDivider", "Divider for force field visualization",
            "When visualizing the force field, high values show weak fields, low values\n"
            "show strong fields."
    },
    {
    "en",
            "GFDivider", "Divider for force field visualization",
            "When visualizing the force field, high values show weak fields, low values\n"
            "show strong fields."
    },
    {
    "en",
            "rippleDivider",
            "Divider for force change rate visualization",
            "Divider for force change rate visualization\n"
            "When visualizing the force field, high values show low change rates, low values\n"
            "show strong change rates."
    },
    {
    "en",
            "CRDivider", "Divider for force change rate visualization",
            "Divider for force change rate visualization\n"
            "When visualizing the force field, high values show low change rates, low values\n"
            "show strong change rates."
    },
    {
    "en",
            "gpuGravDamping", "Force field Wave Frequency",
            "Low values cause a low frequency, high values a high frequency."
    },
    {
    "en",
            "gpuGravStrength", "Force field strength",
            "High values cause a high force field range.\n"
            "This setting determines the default value of the field behavior at the screen border.\n"
    },
    {
    "en",
            "gpuGravWaveDamping", "Force field wave damping",
            "Low values cause a high damping of force field waves."
    },
    {
    "en",
            "gpuGravWaveAbsorp", "TBD",
            "TBD"
    },
    {
    "en",
            "wallTempr", "Wall Heating",
            "When on, walls and particles exchange energy. Walls can be explictly\n"
            "heated by drawing with the wall draw operation and the right mouse button.\n"
            "Temperature is conducted by the wall depending on the wall mass."
    },
    {
    "en",
            "ShowLinks", "Visualize Links",
            "When on, connections between particles are shown.\n"
    },
    {
    "en", "Exit", "Exit program", "Exit program (double click)."
    },
    {
    "en",
            "applyPSettingToAll", "Apply selections to all particles",
            "All particle settings which are marked are immedeately applied to all\n"
            "particles.\n"
            "A particle setting can be marked by pressing the middle mouse button or\n"
            "by selecting the \"mark\" sub-item. Marked settings are outlined in red.\n"
    },
    {
    "en",
            "particleWeight", "Particle Mass (marker)",
            "The mass of a particle. Affects collissions and the generated force field."
    },
    {
    "en",
            "particleAttractionForce", "Particle charge (marker)",
            "Affects generation of and interaction with the force field."
    },
    {
    "en",
            "particleAttraction", "Enable Force Field",
            "Each Particle or wall generates a force field, unless it's charge is zero.\n"
            "The strength of the generated field depends on the particle's mass.\n"
            "All particles are repelled/attracted by the force field depending on their\n"
            "charge and the current strength/polarity of the force field.\n"
    },
    {
    "en",
            "particleSize", "Particle size (marker)",
            "Affects particle collissions."
    },
    {
    "en",
            "water", "Water [CPU]",
            "[CPU] Relatively fast algorithm looking a little bit like water. Good for\n"
            "densly packed particles. Does not interact well with other particle\n"
            "types since particles tend to \"tunnel\" through others."
    },
    {
    "en",
            "Fusion", "Fusion Density",
            "The particle density required for two particles to merge (fusion)\n"
            "A lower setting decreases the probabilty of fusion."
    },
    {
    "en",
            "peFusionGenEnergy", "Fusion To Energy",
            "Number of energy particles generated in a fusion event.\n" },
    {
    "en",
            "wFusionGenEnergy", "Fusion To Energy",
            "Number of energy particles generated in a fusion event.\n" },
    {
    "en",
            "wFusion2SliderSetting", "Fusion Density",
            "The particle density required for two particles to merge (fusion)\n"
            "A lower setting decreases the probabilty of fusion."
    },
    {
    "en",
            "gas", "Gas",
            "[CPU] More or less exact collission detection. Relatively slow compared to\n"
            "water particles, especially when particles are small and densly packed.\n"
    },
    {
    "en",
            "peFusionRad", "Fusion Radius",
            "The disatance between two particles so that they merge to one (heavier)\n"
            "particle. A value of 0 means a distance of 0 is required (does not happen),\n"
            "the max. value means they only need to collide for fusion."
    },
    {
    "en",
            "freeze", "Wall",
            "Particles never leave their pixel, but move within and transfer impuls to\n"
            "neighbouring particles."
    },
    {
    "en",
            "blob", "Multi-particle Object [CPU]",
            "[CPU] Particles form a bigger object where each particle tries to maintain it's\n"
            "relative position. An impuls to one particle affects speed and rotation of\n"
            "the whole object.\n"
    },
    {
    "en",
            "rotSpeed", "Rotation (marker)",
            "Object will rotate (counter)clockwise."
    },
    {
    "en",
            "blobContinueMode", "Continue Drawing Object",
            "When releasing the mouse button after having drawn particles, the object\n"
            "is activated and a new draw operation starts a new object.\n"
            "This setting causes the current object to be extended until the button is\n"
            "deselected."
    },
    {
    "en",
            "dla", "Sticky / Trees [CPU]",
            "[CPU] Particles can connect to one other particle when colliding with another\n"
            "tree particle which is also connected.\n"
            "The particle remembers it's relative position (angle) to the other\n"
            "particle. This causes growth of tree-like structures.\n"
            "Particles may also connect to walls forming the root of a tree.\n"
            "Or particles may connect to \"blob\" objects. The rotation of these objects\n"
            "is propagated to all connected tree particles.\n"
    },
    {
    "en",
            "dlaRotation", "Change tree rotation (mark)",
            "Rotates the tree the selected particle belongs to."
    },
    {
    "en",
            "sConnectToPrevious", "Connect To Previous",
            "Connects new particles to previously drawn particles."
    },
    {
    "en",
            "connectToAll", "Connect to Blobs",
            "Connect particles to \"blob\" type particles. Rotation of\n"
            "the blob object is maintained."
    },
    {
    "en",
            "connectToWall", "Connect to wall",
            "Connect particles to walls, forming an anchor for a new tree\n"
    },
    {
    "en",
            "wall", "Draw a solid wall.",
            "  Left Button:   Draw wall.\n"
            "  Middle Button: -\n"
            "  Right Button:  Heat up wall.\n"
    },
    {
    "en",
            "vectorizedWall", "Vectorized Wall (Experimental)",
            "EXPERIMENTAL: Draws vector based walls (polygons). Double-click to\n"
            "  finish polygon."
    },
    {
    "en",
            "particles", "Draw Particles.",
            "  Left Button:       Draw particles.\n"
            "    +CTRL:           Create particle fountain at this point (press longer for more particles).\n"
            "  Middle Button:     -\n"
            "  Right Button:      Define particle's initial speed/direction while pressed and moving mouse.\n"
            "  Shift+Mouse Wheel: Increase/decrease angle range of particle's initial (random) direction.\n"
    },
    {
    "en",
            "stencil", "Draw with Stencil.",
            "Draws walls and particles using stencil loaded from file or copied from screen selection.\n"
            "  Left Button:           Draw Data.\n"
            "  Middle Button:         -\n"
            "  Right Button:          Moving the mouse rotates and zooms the draw data.\n"
            "  Wheel (shift pressed): Rotate the stencil left/right.\n"
            "  Wheel (ctrl pressed):  Zoom the stencil in/out.\n"
            " \n"
            "Pressing the right mouse button on this control opens a sub-menu:\n"
            "  Option 1: Toggle whether to draw wall elements from stencil.\n"
            "  Option 2: Toggle whether to draw particles in stencil.\n"
            "  Option 3: Toggle whether to convert wall elements in stencil to particles.\n"
            "  Option 4: Toggle whether to convert particles to wall elements."
    },
    {
    "en",
            "erase", "Erase Elements",
            "Depending on the selected sub-item, erases\n"
            " - Walls (and accelleration fields),\n"
            " - Particles,\n" " - All walls, or\n" " - All particles.\n"
    },
    {
    "en",
            "pipe", "Draw Pipe",
            "A green line indicates the most adjacent pipe. Not moving the mouse\n"
            "for 1/2 second will cause the new pipe to be connected to this nearby pipe.\n"
            "Pressing shift causes this auto-connection to look for a nearby surface\n"
            "(drawn by a pipe or by a normal draw operation).\n"
            "Selecting the \"accellerated pipe\" sub-button causes a field\n"
            "to be created in the pipe which accellerates particles away from the pipe walls\n"
            "and into the direction of movement."
    },
    {
    "en",
            "push", "Push Particles",
            "Push Particles using the current stencil.\n"
            "When using the \"Box\" stencil, particles can be captured and moved somewhere else.\n"
            "Pressing the right mouse button while pushing causes a wall to be drawn.\n"
            "Alternatively to push particles, they can be simply accellerated. The\n"
            "accelleration vector can be defined by moving the mouse with the right button pressed."
    },
    {
    "en",
            "move", "Move Particles",
            "All particles below the stencil are captured, their speed is set to zero and their\n"
            "position immedeately follows the mouse.\n"
            "Pressing shift causes the particles to follow the mouse with the highest speed possible,\n"
            "and other particles being hit get accellerated.\n"
    },
    {
    "en",
            "props", "Change Particle Properties",
            "The marked particle properties (i.e. those buttons which have a red border by pressing\n"
            "the right mouse button over them) are applied to particles using the current stencil.\n"
            "The marked settings can also be applied to all particles immedeately by marking this\n"
            "button as well.\n"
    },
    {
    "en",
            "pump", "Draw Acceleration Field",
            "Draws an accelleration field. Dragging the mouse with the right button pressed\n"
            "defines the accelleration vector."
    },
    {
    "en",
            "Stamp", "Draw Stamp",
            "Perform the selected draw operation once when the left mouse botton is pressed."
    },
    {
    "en",
            "Draw", "Draw Continuously",
            "Perform the selected draw operation continuosly while the left mouse button\n"
            "is pressed.\n"
    },
    {
    "en",
            "Line", "Draw using lines or circles",
            "[LINE] The currently selected draw operation follows a line which is started when\n"
            " pressing the mouse button once and stopped when pressing it a second time.\n"
            " Pressing shift before starting the line will start a polygon. The polygon\n"
            " is terminated by a double click on the same position.\n"
            " \n"
            "[CIRCLE] The currently selected draw operation follows a oval or a circle (if\n"
            " shoft is pressed).\n"
    },
    {
    "en",
            "Fill", "Fill connected area",
            "The current draw operation is performed using a flood fill operation.\n"
            "The boundary used depends on the draw operation:\n"
            " - Particles: Fill empty pixels until a wall or other particles are reached.\n"
            " - Wall:      Fill empty pixels until a wall or other particles are reached.\n"
            " - Erase:     Erase until a pixel is reached which does not contain the \n"
            "              selected erase item.\n"
            " - Acc. Fld.: Fill until a wall is hit.\n"
    },
    {
    "en",
            "mask", "Draw Stencil",
            "Defines the stencil used for the current draw operation (wall, particles, etc.)\n"
            "and the current draw mode (stamp, line, etc)."
    },
    {
    "en",
            "chain", "Chained Particles [CPU]",
            "[CPU] Like gas particles, but each particle has up to six connections over which it can connect to other particles.\n"
            "When connected, the particle is attracted to the other particle(s) with a force equal to the distance between\n"
            "the two particles, multiplied with an adjustable constant (attraction force slider).\n"
            "A link may be open (not connected), or closed (particle is connected). Open links can be active or passive.\n"
            "Two particles with at least one active link each will connect to each other when they collide.\n"
            "Depending on the \"Activate\" button the links of new particles are active or passive.\n"
            "If the distance between two linked particles exceeds a configurable distance (Max. distance slider) the\n"
            "particle's link is destroyed and each of the two particles have an open link.\n"
    },
    {
    "en",
            "Energy", "Energy particles",
            "[CPU] These particles interact with other particles by transferring their impuls. They vanish\n"
            "when they hit other particles (except WIPMS) or walls.\n"
            "Energy particles are not affected by force fields or gravity, and they do not generate\n"
            "a force field.\n"
            "Energy particles always have the maximum speed. When created with a zero speed they\n"
            "obtain a random direction.\n"
    },
    {
    "en",
            "WIMP", "Weakly Interacting Massive Particles [CPU]",
            "[CPU] These particles do not collide with other particles. They interact with other particles\n"
            "- Via the force field they create\n"
            "- By linking to other particles. Wimps can connect with as much as 6 other particles, but\n"
            "  any amount of wimps can connect to the same non-wimp.\n"
            "WIMPS are reflected by walls.\n" },
    {
    "en",
            "chainConnectToPassive", "Connect To Passive Links",
            "Normally two particles with at least one active link each will connect.\n"
            "This option causes a particle with an active linke to be connected with a particle\n"
            "having at least one passive link as well.",},
    {
    "en",
            "chainActivate", "Activate/deactivate links",
            "Makes unconnected links active or passive.\n"
            "Passive links will not connect to other particles.\n"
            "Two particles with at least one active link each will connect to each other.",},
    {
    "en",
            "chainMaxDist", "Maximum particle distance",
            "The maximum distance between two particles until the link tears apart.\n"
            "1 unit == pixel/10 + pixel\n",},
    {
    "en",
            "wimpMaxDist", "Maximum particle distance",
            "The maximum distance between two particles until the link tears apart.\n"
            "1 unit == pixel/10 + pixel\n",},
    {
    "en",
            "chainConnectToPrevious", "Connect particles while drawing",
            "Connect particle to previously drawn particle using a double-link.",},
    {
    "en",
            "chainReconnectToFirst",
            "Connect particle while drawing (ring)",
            "Connect particle to previously drawn particle using a double-link.\n"
            "The last particle drawn is connected back to the first one.",},
    {
    "en",
            "chainConnectToNeighbour", "Connect Particle to neighbours",
            "When drawing, new particles are immedeately connected to adjacent new\n"
            "particles, forming a more or less solid object.\n",},
    {
    "en",
            "chainAttraction", "Chain Attraction",
            "The strength of attraction between connected particles."
    },
    {
    "en",
            "wimpAttraction", "WIMP Attraction",
            "The strength of attraction between connected particles."
    },
    {
    "en",
            "Replay", "Replay recorded scene",
            "Replays the scene currently stored in the memory buffer.\n"
            "Shift - Replay in endless loop.\n"
    },
    {
    "en",
            "Fast replay", "Instant replay of recorded scene",
            "Immedeately replays all elements of the scene currently stored in the\n"
            "memory buffer."
    },
    {
    "en",
            "Start/Pause Recorder", "Start/Pause Recorder",
            "Starts/stops/resumes recording of all draw operations and property\n"
            "modifications into a memory buffer, including the delays between the operations.\n"
            "The memory buffer can be replayed, saved to a file or cleared."
    },
    {
    "en",
            "ClearTape", "Clear Tape (Double-Click)",
            "Clears the scene currently stored in the memory buffer."
    },
    {
    "en",
            "Load", "Load scene from file",
            "Loads a scene from a file into the memory buffer for playback."
    },
    {
    "en",
            "Save", "Save scene to file",
            "Saved the scene stored in the mory buffer to a file."
    },
    {
    "en",
            "numLinks", "Maximum number of links",
            "The maximum number of links a particle can have."
    },
    {
    "en",
            "numWimpLinks", "Maximum number of links",
            "The maximum number of links a wimp particle can have."
    },
    {
    "en",
            "energyInteraction", "Energy interaction mode",
            "How energy particles interact with other particles:\n"
            " (1) Other particle is always repulsed\n"
            " (2) Other particle is always attracted\n"
            " (3) Other particle is repulsed if it has the same charge, attracted if it has a different charge\n"
            "Pressing SHIFT reverses the effect"
    },
    {
    "en",
            "energyNotAbsorped", "Energy particle absortion",
            "Energy particles transfer their impuls to other particles they hit. If this\n"
            "button is not selected, energy particles vanish if they have transferred all of their\n"
            "impuls. Otherwise they fly on ..\n"
    },
    {
    "en",
            "energyColormerge", "Merge color of energy particles",
            "Color of energy particles is merged with the color of particle(s) they hit."
    },
    {
    "en",
            "energyColorSat", "Saturation of energy particles",
            "The color saturation of energy particles\n"
    },
    {
    "en",
            "showLinks", "Show particle links",
            "Draws lines between connected particles.\n"
    },
    {
    "en",
            "gvX", "TBD",
            "TBD.\n"
    },
    {
    "en",
            "gvY", "TBD",
            "TBD.\n"
    },
    {
    "en",
            "preassure", "TBD",
            "TBD.\n"
    },
    {
    "en",
            "ptype", "Particle Types",
            "Particle types.\n"
    },
    {
    "en",
            "fieldLines", "Show Field Lines",
            "Shows field lines instead of absolute field values.\n"
    },
    {
    "en",
            "particleLifetimeLambda", "TBD",
            "TBD.\n"
    },
    {
    "en",
            "sReconnectToFirst", "TBD",
            "TBD.\n"
    },
    {
    "en",
            "hardness", "Hardness of object",
            "Specifies how fast particles try to re-gain their position. Low values cause soft objects,\n"
            "high values result in hard objects.\n"
    },
    {
    "en",
            "hardness2", "TBD",
            "TBD.\n"
    },
    {
    "en",
            "wimpChain", "Chain WIPMS to other objects",
            "When enabled, a wimp will connect to a massive paticle it hits.\n"
            "Any amount of WIMPs can connect to the same particle, but a WIMP can only\n"
            "connect to a limitied amount of particles.\n" },
    {
    "en",
            "wimpBounce", "Particles bounce at screen border",
            "Particles bounce at screen border instead of vanishing.\n"
    },
    {
    "en",
            "wimpPair", "Pair massive particle with wimp",
            "When selected, a new WIMP particle is created and linked together with a new massive particle.\n"
    },
    {
    "en",
            "hideParticles", "Hide Particles",
            "Do not draw particles to screen.\n"
    },
    {
    "en",
            "particleVisSize", "Visual particle size",
            "Defines the size of the particle dots.\n"
            "The actual size is usually relative to the zoom factor, unless the minum size of 1 (pixel) is selected.\n" },
    {
    "en",
            "repulseChained", "Attract or repulse two connected particles",
            "Attract or repulse two connected particles.\n"
    },
    {
    "en",
            "connectTwo", "Connect two particles",
            "Select two particles and \n"
            "  - connect them (if CTRL is NOT pressed)\n"
            "  - disconnect them (if CTRL is pressed)."
    },
    {
    "en",
            "particleSmooth", "Smooth particles",
            "Draw particles smoothed/anti-aliased or as rectangle..\n"
    },
    {
    "en",
            "updateInterval", "Screen Update Interval",
            "Minimum Screen Update Interval.\n"
    },
    {
    "en",
            "maxFps", "Max. Particle engine interations per second",
            "Can be used to limit the speed of the particle engine.\n"
    },
    {
    "en",
            "colLum", "Luminosity",
            "Color Luminosity.\n"
    },
    {
    "en",
            "energyColorVal", "Color of energy particles",
            "Color of energy particles.\n"
    },
    {
    "en",
            "Paint", "Change Particle Colors",
            " - left button:   Temporarily paint particles with the selected color\n"
            " - right button:  Permanently paint particles with the selected color\n"
            " - Middle button: Pick average color of particles under the stencil\n"
            " When shift is pressed (in combination with LMB/RMB), the particle's\n"
            "   default color is applied.\n"
    },
    {
    "en",
            "keys", "Keyboard Assignment",
            "space - Stop/resume particle movement         f    - Save screen                 \n"
            "v     - Hide/show particles                   l    - Redraw Screen               \n"
            "a/s   - Zoom in/out (or mouse wheel)          r    - Toggle random particle placement in pixel \n"
            "t     - Limit particle engine rate            b    - Toggle border               \n"
            ",.89  - De/Increase part. enging rate limit   TAB  - (or RMB) Toggle panels      \n"
            "                                                     (shift hides panel)         \n"
            "                                              F1   - Button help                 \n"
            "x     - Remove all particles (press twice)    Cursor keys - Move zoom area (or   \n"
            "q     - Exit (press twice)                     press mid. button and move mouse) \n"
            },
    {
        "en",
        "workOrderSetting",
        "Particle processing work order",
        "Sweep forward, backward or alternating"
    },
    {
        "en",
        "gpuSpeedLimit",
        "Speed limit",
        "Number of pixels a GPU particle can propagate per iteration."
    },

    {
        "en",
        "clAvgMode",
        "Averaging mode for force field",
        "Enhanced (but slower) method for applying a particle's mass to\n"
        "the force field\n"
    },
    {
        "en",
        "gpuCollission",
        "Enable GPU Particle Collission",
        "Enables collission handling of GPU particles.\n"
        "Requires a fast GPU\n"
    },

    {
        "en",
        "gpuCollAttraction",
        "Attraction factor",
        "Attraction Factor for GPU particle collission"
    },

    {
        "en",
        "gpuCollSpring",
        "Spring Factor",
        "Spring Factor for GPU particle collission"
    },

    {
        "en",
        "gpuCollShear",
        "Collission Shear parameter",
        "Shear factor for GPU particle collission"
    },

    {
        "en",
        "gpuCollDamping",
        "Collission Damping",
        "Damping value when colliding GPU particles"
    },

    {
        "en",
        "partAlpha",
        "Alpha channel for particle visulatization",
        "Alpha channel for particle visulatization"
    },

    {
        "en",
        "gravAlpha",
        "Alpha channel for field visulatization",
        "Alpha channel for field visulatization"
    },

    {
        "en",
        "gravIterations",
        "Number of field iterations per loop",
        "Defines the number of field iterations done for each GPU work pass."
    },

    {
        "en",
        "gpuSync",
        "Synchronize GPU Field Engine and CPU Particle Engine",
        "Synchronize GPU Field Engine and CPU Particle Engine"
    },
    {
        "en",
        "paintColor",
        "Color Selector",
        "Color Selector"
    },
    {
        "en",
        "colAlpha",
        "Color Transparency",
        "Color Transparency"
    },

    {
        "en",
        "clNumLinks",
        "Number of Links per particle",
        "The number of links a particle can establish."
    },

    {
        "en",
        "clLinkInterval",
        "Link Check Interval",
        "Every n-th iteration the GPU code will check whether to link together\n"
        "two colliding particles."
    },

    {
        "en",
        "clDoLink",
        "Link colliding particles together",
        "Particles created with this option may link to other particles on collission.\n"
        "Whether this actually happens depends on the link check interval and if there is at least\n"
        "one unused link on both particles.\n"
        "Only particles with the same weight can link\n"
    },

    {
        "en",
        "clParticleTrailLen",
        "Length of particle trails",
        "Length of particle trails.\n"
        "Only if global trail button is set, and for particles without links\n"
    },

    {
        "en",
        "gpuTimeFac",
        "GPU Particle Engine Time Factor",
        "The lower the value, the more exact particle collission detection is,\n"
        "but also slower."
    },

    {
        "en",
        "clMaxLinkLength",
        "Maximum link length",
        "The maximum length of a particle link before it snaps."
    },

    {
        "en",
        "clLinkLength",
        "Target distance of linked particles",
        "The distance two linked particles try to keep (in units of 1/100th of a pixel)"
    },

    {
        "en",
        "clLinkStrength",
        "Link Strength",
        "The attraction of two linked particles (0 = weak)"
    },

    {
        "en",
        "lumSpringFac",
        "Color difference affects particle collission",
        "When two particles collide, a difference in their luminance increases the\n"
        "collission spring factor (repulsion).\n"
        "The effect is that particles with different colors do not mix as good."
    },

    {
        "en",
        "Color Picker",
        "Color Picker",
        "Selects the Color Value\n"
        "A randomized color area can be defined using the mouse wheel."
    },

    {
        "en",
        "Exit Color Picker",
        "Exit Color Picker",
        "Exit Color Picker\n"
        "Use right mouse button to get a history of previous color values."
    },

    {
        "en",
        "Luminosity",
        "Luminosity",
        "Select Luminosity of color"
    },

    {
        "en",
        "Alpha",
        "Alpha",
        "Select alpha value (transparency)"
    },

    {
        "en",
        "ModifyAlpha",
        "Modify Alpha only",
        "If selected, only the alpha value is applied to existing particles."
    },

    {
        "en",
        "lumSpringFac",
        "Let luminosty difference affect collission parameters",
        "If enabled, luminosity difference increases spring factor,\n"
        " decreases attraction.\n"
    },
    {
        "en",
        "clMakeBlob",
        "Make solid object",
        "If enabled, all draw operations are grouped to form a solid object (more or less).\n"
        "Still experimental."
    },
    {
        "en",
        "clColExchThreshR",
        "Color exchange threshold",
        "The threshold where a color channel (RGB) difference of two colliding particles\n"
        "causes the higher value to be forwarded to the particle with the lower value.\n"
    },
    {
        "en",
        "clColExchThreshG",
        "Color exchange threshold",
        "The threshold where a color channel (RGB) difference of two colliding particles\n"
        "causes the higher value to be forwarded to the particle with the lower value.\n"
    },
    {
        "en",
        "clColExchThreshB",
        "Color exchange threshold",
        "The threshold where a color channel (RGB) difference of two colliding particles\n"
        "causes the higher value to be forwarded to the particle with the lower value.\n"
    },
    {
        "en",
        "clLinkAttractionMode",
        "Linked particles attract/repel each other",
        "If selected, linked particles attract each other."
    },
    {
        "en",
        "clCollissionCol",
        "Target Color when partticles collide",
        "If enabled, a colliding particles color will move towards the selected color,\n"
        "depending on the force it has experienced.\n"
    },
    {
        "en",
        "einsteinFactor",
        "Time slowdown under gravity",
        "A setting which controls the time factor applied to a particle\n"
        "based on the absolute value of the force field potential at its position.\n"
        "0 = off\n"
    },
    {
        "en",
        "clSticky",
        "Particle sticks to wall",
        "Particles will stick to walls with maximum friction."
    },
    {
        "en",
        "collTargetColor",
        "Target color of colliding particle",
        "Target color of colliding particle"
    },
    {
        "en",
        "gpuCollDist",
        "Collision distance of particles",
        "Collision distance of particles"
    },
    {
        "en",
        "fieldBorder",
        "Field Border behaviour",
        "If set, the screen border forces the field potential to zero.\n"
        "If clear, the screen border does not limit the field (inaccurate)."
    },
    {
        "en",
        "drawWithScreen",
        "Draw particles only if screen is updated",
        "Faster if set, nicer but slower if not set."
    },
    {
        "en",
        "clLinkMode",
        "Types of particles that can link with each other",
        "0 (upper)  : Only colliding particles can link.\n"
        "1 (middle) : Colliding particles will link only with non-colliding particles.\n"
        "2 (lower)  : Colliding particles will link with colliding or non-colliding particles."
    },
    {
        "en",
        "gpuBorderFieldCutoff",
        "Field behavior at screen border",
        "At the screen border the field can be either\n"
        " - Forced to zero (value = 0), or\n"
        " - Set to a fraction (0 < value < max) of the adjacent (inwards) field value.\n\n"
        "The default setting depends on the field strength setting.\n"
    },
    {
        "en",
        "XXX",
        "XXX",
        "XXX"
    },
    {
        "en",
        "XXX",
        "XXX",
        "XXX"
    },


};

void setHelpLang (char *s)
{
    strncpy (helpLang, s, sizeof(helpLang));
}

char *
getHelpString (char *ident)
{
    int i;

    if (ident == NULL)
        return NULL;

    for (i = 0; i < NELEMENTS (helpStrings); i++)
    {
        if (!strcmp (ident, helpStrings[i].helpId) && !strcmp (helpLang, helpStrings[i].lang))
        {
            return helpStrings[i].longHelp;
        }
    }

    return NULL;
}

char *
getTooltipString (char *ident)
{
    int i;

    if ((ident == NULL) || !strcmp (ident, "anon"))
        return NULL;

    for (i = 0; i < NELEMENTS (helpStrings); i++)
    {
        if (!strcmp (ident, helpStrings[i].helpId) && !strcmp (helpLang, helpStrings[i].lang))
            return helpStrings[i].tooltip;
    }

    return ident;
}
