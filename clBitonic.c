/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

// standard utilities and systems includes
#include "plansch.h"
#include <stdio.h>
#include <fcntl.h>
#include <CL/cl.h>
#include <CL/cl_gl.h>
#include <CL/cl_ext.h>

#include "opencl.h"

#define oclCheckError(cond,val)        if ((cond) != (val)) { printf ("ERROR: %s line %d\n", __FILE__, __LINE__); exit (1);}

////////////////////////////////////////////////////////////////////////////////
// OpenCL launcher for bitonic sort kernel
////////////////////////////////////////////////////////////////////////////////
//OpenCL bitonic sort program

//OpenCL bitonic sort kernels
extern cl_kernel
    bitonicSortAll,
    bitonicSortLocal,
    bitonicSortLocal1,
    bitonicMergeGlobal,
    bitonicMergeGlobalLoop,
    bitonicMergeLocal;

void bitonicSort(
    cl_command_queue cqCommandQueue,
    cl_mem d_DstKey,
    cl_mem d_DstVal,
    cl_mem d_SrcKey,
    cl_mem d_SrcVal,
    unsigned int batch,
    unsigned int arrayLength,
    unsigned int dir
){
    unsigned int size, stride;
    if(arrayLength < 2)
        return;

    //Only power-of-two array lengths are supported so far

    dir = (dir != 0);

    cl_int ciErrNum = CL_SUCCESS;
    size_t localWorkSize, globalWorkSize;

        
    if (haveOcl2 && 0)
    {
        /* does not yet work (slow) */
        ciErrNum  = clSetKernelArg(bitonicSortAll, 0,  sizeof(cl_mem), (void *)&d_DstKey);
        ciErrNum |= clSetKernelArg(bitonicSortAll, 1,  sizeof(cl_mem), (void *)&d_DstVal);
        ciErrNum |= clSetKernelArg(bitonicSortAll, 2,  sizeof(cl_mem), (void *)&d_SrcKey);
        ciErrNum |= clSetKernelArg(bitonicSortAll, 3,  sizeof(cl_mem), (void *)&d_SrcVal);
        ciErrNum |= clSetKernelArg(bitonicSortAll, 4,  sizeof(cl_uint), (void *)&batch);
        ciErrNum |= clSetKernelArg(bitonicSortAll, 5,  sizeof(cl_uint), (void *)&arrayLength);
        ciErrNum |= clSetKernelArg(bitonicSortAll, 6,  sizeof(cl_uint), (void *)&dir);
        oclCheckError(ciErrNum, CL_SUCCESS);

        globalWorkSize = 1;
        ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, bitonicSortAll, 1, NULL, &globalWorkSize, NULL, 0, NULL, NULL);
        oclCheckError(ciErrNum, CL_SUCCESS);
    }
    else
    {
        if(arrayLength <= LOCAL_SIZE_LIMIT)
        {
            oclCheckError( (batch * arrayLength) % LOCAL_SIZE_LIMIT == 0, 1 );
            //Launch bitonicSortLocal
            ciErrNum  = clSetKernelArg(bitonicSortLocal, 0,   sizeof(cl_mem), (void *)&d_DstKey);
            ciErrNum |= clSetKernelArg(bitonicSortLocal, 1,   sizeof(cl_mem), (void *)&d_DstVal);
            ciErrNum |= clSetKernelArg(bitonicSortLocal, 2,   sizeof(cl_mem), (void *)&d_SrcKey);
            ciErrNum |= clSetKernelArg(bitonicSortLocal, 3,   sizeof(cl_mem), (void *)&d_SrcVal);
            ciErrNum |= clSetKernelArg(bitonicSortLocal, 4,  sizeof(cl_uint), (void *)&arrayLength);
            ciErrNum |= clSetKernelArg(bitonicSortLocal, 5,  sizeof(cl_uint), (void *)&dir);
            oclCheckError(ciErrNum, CL_SUCCESS);

            localWorkSize  = LOCAL_SIZE_LIMIT / 2;
            globalWorkSize = batch * arrayLength / 2;
            ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, bitonicSortLocal, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
            oclCheckError(ciErrNum, CL_SUCCESS);
        }
        else
        {
            //Launch bitonicSortLocal1
            ciErrNum  = clSetKernelArg(bitonicSortLocal1, 0,  sizeof(cl_mem), (void *)&d_DstKey);
            ciErrNum |= clSetKernelArg(bitonicSortLocal1, 1,  sizeof(cl_mem), (void *)&d_DstVal);
            ciErrNum |= clSetKernelArg(bitonicSortLocal1, 2,  sizeof(cl_mem), (void *)&d_SrcKey);
            ciErrNum |= clSetKernelArg(bitonicSortLocal1, 3,  sizeof(cl_mem), (void *)&d_SrcVal);
            oclCheckError(ciErrNum, CL_SUCCESS);

            localWorkSize  = LOCAL_SIZE_LIMIT / 2;
            globalWorkSize = batch * arrayLength / 2;
            ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, bitonicSortLocal1, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
            oclCheckError(ciErrNum, CL_SUCCESS);


            for(size = 2 * LOCAL_SIZE_LIMIT; size <= arrayLength; size <<= 1)
            {
                if (haveOcl2 && 0)
                {
                    /* does not yet work (slow) */
                    ciErrNum  = clSetKernelArg(bitonicMergeGlobalLoop, 0,  sizeof(cl_mem), (void *)&d_DstKey);
                    ciErrNum |= clSetKernelArg(bitonicMergeGlobalLoop, 1,  sizeof(cl_mem), (void *)&d_DstVal);
                    ciErrNum |= clSetKernelArg(bitonicMergeGlobalLoop, 2,  sizeof(cl_mem), (void *)&d_DstKey);
                    ciErrNum |= clSetKernelArg(bitonicMergeGlobalLoop, 3,  sizeof(cl_mem), (void *)&d_DstVal);
                    ciErrNum |= clSetKernelArg(bitonicMergeGlobalLoop, 4, sizeof(cl_uint), (void *)&arrayLength);
                    ciErrNum |= clSetKernelArg(bitonicMergeGlobalLoop, 5, sizeof(cl_uint), (void *)&size);
                    ciErrNum |= clSetKernelArg(bitonicMergeGlobalLoop, 6, sizeof(cl_uint), (void *)&dir);
                    oclCheckError(ciErrNum, CL_SUCCESS);

                    localWorkSize  = 1;
                    globalWorkSize = 1;

                    ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, bitonicMergeGlobalLoop, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
                    oclCheckError(ciErrNum, CL_SUCCESS);
                }
                else
                {
                    for(stride = size / 2; stride >= LOCAL_SIZE_LIMIT; stride >>= 1)
                    {
                        //Launch bitonicMergeGlobal
                        ciErrNum  = clSetKernelArg(bitonicMergeGlobal, 0,  sizeof(cl_mem), (void *)&d_DstKey);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 1,  sizeof(cl_mem), (void *)&d_DstVal);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 2,  sizeof(cl_mem), (void *)&d_DstKey);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 3,  sizeof(cl_mem), (void *)&d_DstVal);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 4, sizeof(cl_uint), (void *)&arrayLength);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 5, sizeof(cl_uint), (void *)&size);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 6, sizeof(cl_uint), (void *)&stride);
                        ciErrNum |= clSetKernelArg(bitonicMergeGlobal, 7, sizeof(cl_uint), (void *)&dir);
                        oclCheckError(ciErrNum, CL_SUCCESS);

                        localWorkSize  = LOCAL_SIZE_LIMIT / 2;/*XXX*/
                        globalWorkSize = batch * arrayLength / 2;

                        ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, bitonicMergeGlobal, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
                        oclCheckError(ciErrNum, CL_SUCCESS);
                    }
                }

                //Launch bitonicMergeLocal
                ciErrNum  = clSetKernelArg(bitonicMergeLocal, 0,  sizeof(cl_mem), (void *)&d_DstKey);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 1,  sizeof(cl_mem), (void *)&d_DstVal);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 2,  sizeof(cl_mem), (void *)&d_DstKey);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 3,  sizeof(cl_mem), (void *)&d_DstVal);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 4, sizeof(cl_uint), (void *)&arrayLength);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 5, sizeof(cl_uint), (void *)&size);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 6, sizeof(cl_uint), (void *)&stride);
                ciErrNum |= clSetKernelArg(bitonicMergeLocal, 7, sizeof(cl_uint), (void *)&dir);
                oclCheckError(ciErrNum, CL_SUCCESS);

                localWorkSize  = LOCAL_SIZE_LIMIT / 2;
                globalWorkSize = batch * arrayLength / 2;

                ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, bitonicMergeLocal, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
                oclCheckError(ciErrNum, CL_SUCCESS);
            }
        }
    }
}
