/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"

SDL_Surface *screen;

static SDL_Rect drawArea;
static SDL_Rect buttonArea;

int linesize;
int wid, hei;
unsigned int maxX, maxY;
int noScreenOutput = 0;
int m128Align;

int scrChangeFlag = 1;

screeninfo **siList = NULL;
int siListSize;
int wimpsMarked;

extern SDL_Cursor *emptyCursor;

/* screen color cycling, responsible for generating particle trails. This is the SSE2 version,
 * a GPU version also exists.
 */
void cycle (int thread)
{
    __m128i *pp, *ppEnd;
    __m128i scr;
    __m128i thresh;
    __m128i iscol, add;
//    __m128i decr = _mm_set1_epi8 (0xff/4);
    __m128i gray = _mm_set1_epi32(COL_ACCEL);
    __m128i grayThresh = _mm_set1_epi32 (COL_ACCEL & 0xff000000);
    __m128i tgt, tmp;

    int perThread;

    perThread = (hei * (wid/4)) / numCores;
    thresh = _mm_set1_epi32 (0x01000000);

    pp = (__m128i*)((uintptr_t)screen->pixels + 0x10 - m128Align);
    pp += perThread * thread;
    ppEnd = pp + perThread;
    _mm_prefetch(pp, _MM_HINT_T0);

    for (; pp < ppEnd; pp++)
    {
        scr = *pp;

        /* Color cycling for true-color visual.
         * All pixels with alpha channel 0 (particles) are averaged towards zero (black).
         * This causes the particle trails.
         * Pixels with alpha channel >= alpha(COL_ACCEL) are averaged towards COL_ACCEL (gray).
         * This causes the accelleration arrows (e.g. when drawing an accelleration field or
         * a pipe) to stay visible.
         * Everything else (walls, lines, gravity, ..) with an alpha channel > 0 and < alpha(COL_ACCEL)
         * stays untouched.
         */
        tmp = _mm_cmpgt_epi32 (scr, grayThresh);        /* is >= alpha(COL_ACCEL) ? */
        iscol = _mm_or_si128 (tmp, _mm_cmpgt_epi32 (thresh, scr)); /* is 0 or >= alpha(COL_ACCEL) ? */
        tgt = _mm_and_si128 (tmp, gray); /* average towards 0 or gray */
        add = _mm_and_si128 (iscol, _mm_avg_epu8 (scr, tgt)); /* average screen color */
        *pp = _mm_or_si128 (add, _mm_andnot_si128 (iscol, scr)); /* merge to screen */
    }
}

void cycleColors()
{
    int thread;
    OMPVARS

    if ((options->drawMode & D_CYCLING) && !showGravity)
    {
#pragma omp parallel for schedule(static,1) 
        for (thread = 0; thread < numParallel; thread++)
        {
            OMP_THREAD_NAME("cycleColors", thread)
            cycle (thread);
        }
        OMPDONE
    }
}

void
doUpdateScreen (void)
{
    if (noScreenOutput)
    {
        return;
    }

#pragma omp critical
    {
#ifndef USE_OPENGL
        SDL_UpdateRect (screen, 0, 0, options->scrWid, options->scrHei);
#else
        if (!options->useGpu)
            openglScreenUpdate ();
#endif
    }
}

void requestScreenUpdate()
{
#ifdef USE_OPENGL
    if (options->useGpu)
    {
        return;
    }
#endif
    doUpdateScreen();
}


void
doUpdateScreenNoMp (void)
{

    if (noScreenOutput)
    {
        return;

    }

#ifndef USE_OPENGL
    SDL_UpdateRect (screen, 0, 0, options->scrWid, options->scrHei);

    if (options->drawMode & D_CYCLING)
    {
        cycle (0);
    }
#else
    if (!options->useGpu)
        openglScreenUpdate ();
#endif

}

void siMarkInit()
{
    if (siList == NULL)
    {
        siList = malloc(wid*hei*sizeof(*siList));
        siListSize = 0;
        wimpsMarked = 0;
    }
}

void siMark (screeninfo *pSi)
{
    if (pSi->flags & SI_MARKER)
    {
        return;
    }

    if (siListSize >= wid*hei)
    {
        printf ("ERROR: mark list too long\n");
        markClear();
    }

    pSi->flags |= SI_MARKER;
    siList[siListSize++] = pSi;

    pSi->flags |= SI_MARKER;
    if (pSi->wimps)
        wimpsMarked = 1;
}

void siMarkClear()
{
    int i;

    for (i = 0; i < siListSize; i++)
    {
        siList[i]->flags &= ~SI_MARKER;
    }
    siListSize = 0;
    wimpsMarked = 0;    
}

void siWalkMarked (FUNCPTR fct, intptr_t arg)
{
    int i;

    for (i = 0; i < siListSize; i++)
    {
        if (fct (siList[i], arg) == 0)
            return;
    }
}

int siMarkedHasWimps()
{
    return wimpsMarked;
}

static void sdlInfo()
{
    SDL_Rect **modes;
    int i;
   
    /* Get available fullscreen/hardware modes */
    modes=SDL_ListModes(NULL, SDL_FULLSCREEN|SDL_SWSURFACE);

    /* Check is there are any modes available */
    if(modes == (SDL_Rect **)0){
      printf("No modes available!\n");
      exit(-1);
    }

    /* Check if our resolution is restricted */
    if(modes == (SDL_Rect **)-1){
      printf("All resolutions available.\n");
    }
    else{
      /* Print valid modes */
      printf("Available Modes\n");
      for(i=0;modes[i];++i)
        printf("  %d x %d\n", modes[i]->w, modes[i]->h);
    }
}



static int maxRes (int *w, int *h)
{
    SDL_Rect **modes;
   
    /* Get available fullscreen/hardware modes */
    modes=SDL_ListModes(NULL, SDL_FULLSCREEN|SDL_SWSURFACE);

    /* Check is there are any modes available */
    if(modes == (SDL_Rect **)0){
        return 0;
    }

    /* Check if our resolution is restricted */
    if(modes != (SDL_Rect **)-1)
    {
        *w = modes[0]->w;
        *h = modes[0]->h;
        return 1;
    }
    return 0;
}

/* init main screen with a menu/panel height of
 * at least menuHei
 */
void initScreen (int menuHei, int fullscreen)
{
    if (SDL_Init (SDL_INIT_VIDEO) < 0)
    {
        fprintf (stderr, "SDL_init\n");
        exit (1);
    }

    sdlInfo();

    if (options->fullscreen)
    {
        maxRes(&options->scrWid, &options->scrHei);
    }
    
    /* align wid/height of draw area down to 16 
     */
    wid = options->scrWid & 0xfffffff0;
    hei = (options->scrHei - menuHei) & 0xfffffff0;

    memInit();

    drawArea.x = 0;
    drawArea.y = 0;
    drawArea.w = wid;
    drawArea.h = hei;

    buttonArea.x = 0;
    buttonArea.y = hei;
    buttonArea.w = wid;
    buttonArea.h = options->scrHei - hei;

    initColors (NULL);

    SDL_WM_SetCaption ("Plansch", "Plansch");

#ifndef USE_OPENGL
    screen =
        SDL_SetVideoMode (options->scrWid, options->scrHei, BITS_PER_PIXEL,
                            SDL_HWPALETTE 
                          | SDL_SWSURFACE 
                          | (fullscreen ? SDL_FULLSCREEN : 0));
    
    if (screen == NULL)
    {
        if (fullscreen)
        {
          screen =
            SDL_SetVideoMode (options->scrWid, options->scrHei, BITS_PER_PIXEL,
                                SDL_HWPALETTE
                              | SDL_SWSURFACE
                              | SDL_NOFRAME);


        }
        printf ("SDL_SetVideoMode failed\n");
        exit(1);
    }


    linesize = screen->pitch;
    m128Align = 0x10 - ((uintptr_t) (screen->pixels) & 0xf);

#else

    screen = SDL_CreateRGBSurface (SDL_SWSURFACE, options->scrWid, options->scrHei, 32, 0xff,0xff00,0xff0000,0xff000000);
    SDL_SetAlpha (screen, 0, 0xff);

    openglInitScreen (fullscreen);

#ifdef OPENGL_NOGRAVITY
    options->useGpu = 0;
#endif

    if (options->useGpu)
    {
        /* Leave one core for the GPU */
        if (oglDedicatedThread() && (options->numThreads > 1) && (options->numThreads == numCores))
        {
            options->numThreads--;
            printf ("numThreads reduced to %d\n", options->numThreads);
        }

        if (!grav_gpu_init (fullscreen))
            options->useGpu = 0;
    }
#endif


    maxX = INTERNC(wid-1);
    maxY = INTERNC(hei-1);

    siMarkInit();

}

void clipToDrawArea ()
{
    SDL_SetClipRect (screen,  &drawArea);
}

void clipToButtonArea ()
{
    SDL_SetClipRect (screen,  &buttonArea);
}

void clipDisable()
{
    SDL_SetClipRect (screen,  NULL);
}


int siColor (screeninfo *pSi)
{
    int rc = COL_EMPTY;

    if (pSi->particleList > SCR_BRICK)
        return COL_EMPTY;

    if (pSi->particleList == SCR_BRICK)
    {
        if (!getValue(&wallTempr) && pSi->flags & SI_BOUNDARY)
        {
            rc = BOUNDARY_COL(pSi);
        }
        else
        {
#ifndef USE_OPENCL
            rc = WALL_COL(TEMPR(pSi), WALL_MASS_GET(pSi));
#else
            rc = WALL_COL(0, WALL_MASS_GET(pSi));
#endif
        }
//        rc = CH_ADD_A (rc, 50);
    }
    else
    {
        if (pSi->flags & SI_MARKER)
            rc = COL_BLUE;
    }

    return rc;
}

int averagedParticleColor (screeninfo *pSi)
{
    particle *p;
    int r, g, b;
    uint32_t pc;
    r = g = b = 0;
    int div = 0;

    for (p = pSi->particleList; p > SCR_BRICK; p = p->next)
    {
        pc = getParticleColorARGB (p);
        r += CH_GET_R(pc);
        g += CH_GET_G(pc);
        b += CH_GET_B(pc);

        div++;
    }

    if (!div)
        return 0;

    r /= div;
    g /= div;
    b /= div;

    return MAKE_ARGB(0xff,r,g,b);
}

int pixelColorGet (int x, int y)
{
    screeninfo *pSi;

    x = zoomx + x / zoom;
    y = zoomy + y / zoom;
    pSi = &siArray[x + y*wid];

    if (pSi->particleList > SCR_BRICK)
    {
        return averagedParticleColor (pSi);
    }
    else
    {
        return siColor (pSi);
    }
}

int siColorIdx (screeninfo *pSi)
{
    if (pSi->particleList > SCR_BRICK)
        return COL_EMPTY_I;

    if (pSi->particleList == SCR_BRICK)
    {
#ifndef USE_OPENCL
        if (!getValue (&wallTempr) && pSi->flags & SI_BOUNDARY)
            return COL_ORANGE_I;
        else
            return TEMPR_COL_I(TEMPR(pSi));
#else
        return COL_ORANGE_I;
#endif
    }
    if (pSi->flags & SI_MARKER)
        return COL_BLUE_I;

    return COL_EMPTY_I;
}

int pixelColorIdx (int x, int y)
{
    screeninfo *pSi;

    x = zoomx + x / zoom;
    y = zoomy + y / zoom;
    pSi = &siArray[x + y*wid];

    return siColorIdx (pSi);

}


#ifndef USE_OPENGL
void redrawScreen (void)
{
    int x, y;
    screeninfo *pSi;
    SDL_Rect rect;


    for (rect.x = 0; rect.x < wid; rect.x += zoom)
    {
        for (rect.y = 0; rect.y < hei; rect.y += zoom)
        {
            x = zoomx + rect.x / zoom;
            y = zoomy + rect.y / zoom;
            rect.w = rect.h = zoom;

            if ((rect.x + rect.w) >= wid)
                rect.w = wid - rect.x;

            if ((rect.y + rect.h) >= hei)
                rect.h = hei - rect.y;

            if ((x >= wid) || (y >= hei))
            {
                SDL_FillRect (screen, &rect, COL_ICONDRAW);
                continue;
            }


            pSi = &siArray[x + y*wid];

            if (pSi->particleList == SCR_BRICK)
            {
                if (!getValue (&wallTempr) && pSi->flags & SI_BOUNDARY)
                    SDL_FillRect (screen, &rect, BOUNDARY_COL(pSi));
                else
                    SDL_FillRect (screen, &rect, WALL_COL(TEMPR(pSi), WALL_MASS_GET(pSi)));

                continue;
            }

            SDL_FillRect (screen, &rect, (pSi->flags & SI_MARKER) ? COL_BLUE : COL_EMPTY);
#if 0
            if (zoom >= 99999)
            {
                int sx, sy;
                int gx,gy;
                FLOAT v;
                int col = COL_LINE;

                sx = rect.x + rect.w/2;
                sy = rect.y + rect.h/2;

#if 1
                gx = GRAVX(pSi);
                gy = GRAVY(pSi);
                v = VEC_LEN(gx,gy);

                if (v > zoom/2)
                {
                    col = COL_RED;

                    gx = (gx * zoom/2) / v;
                    gy = (gy * zoom/2) / v;
                }

                if (gx || gy)
                {
                    drawLineF (sx, sy, sx + gx, sy + gy,
                           lineCbVis, col, 0, 0, 0);
                    SCR_SET(sx, sy, COL_RED);
                }
//    printf ("%d %d %d %d\n", sx, sy, (int)pSi->gravity[gravIdx].d.x, (int)pSi->gravity[gravIdx].d.y);
#else
                for (i = 0; i < 8; i++)
                {

                    nx = pSi->normals[i*2] / 4;
                    ny = pSi->normals[i*2+1] / 4;

                    if ((nx == 0) && (ny == 0))
                        continue;

                    switch (i)
                    {
                        case 0: sx = rect.x;
                                sy = rect.y;
                                break;
                        case 1: sx = rect.x;
                                sy = rect.y + rect.h/2;
                                break;
                        case 2: sx = rect.x;
                                sy = rect.y + rect.h;
                                break;
                        case 3: sx = rect.x + rect.w/2;
                                sy = rect.y;
                                break;
                        case 4: sx = rect.x + rect.w/2;
                                sy = rect.y + rect.h;
                                break;
                        case 5: sx = rect.x + rect.w;
                                sy = rect.y;
                                break;
                        case 6: sx = rect.x + rect.w;
                                sy = rect.y + rect.h/2;
                                break;
                        case 7: sx = rect.x + rect.w;
                                sy = rect.y + rect.h;
                                break;
                    }

                    drawLineF (sx, sy, sx + nx , sy + ny, lineCb, COL_RED, 0, 0, 0);
                }
#endif
            }
#endif

            if (pSi->flags & SI_IS_PUMP)
            {
                for (x = rect.x; x <= rect.x + rect.w; x++)
                {
                    for (y = rect.y; y <= rect.y + rect.h; y++)
                    {
                        if (!dirPixel (x, y, XACCEL(pSi), YACCEL(pSi)))
                        {
                            SCR_SET (x, y, COL_ACCEL);
                        }
                        else
                        {
                            SCR_SET (x, y, COL_EMPTY);
                        }
                    }
                }
            }

            if (zoom >= 16)
            {
                SCR_SET (rect.x, rect.y, COL_RED);
            }

        }
    }

    clearBackStore();
    stencilCenter (NULL, &x, &y);
    drawStencilSprite (NULL, x, y, 0);
    redrawSelectionBox();
    redrawScreenObjects();
}
#else
void redrawScreen (void)
{
    int x, y;

    oglClearScreen();

    clearBackStore();
    stencilCenter (NULL, &x, &y);
    drawStencilSprite (NULL, x, y, 0);
    redrawSelectionBox();
    redrawScreenObjects();
}
#endif

void gravShow (void)
{
    if (options->useGpu)
        return;

    int x, y;
    screeninfo *pSi;
    SDL_Rect rect;
    int c;

    for (rect.x = 0; rect.x < wid; rect.x += zoom)
    {
        for (rect.y = 0; rect.y < hei; rect.y += zoom)
        {
            x = zoomx + rect.x / zoom;
            y = zoomy + rect.y / zoom;
            rect.w = rect.h = zoom;

            pSi = &siArray[x + y*wid];

            if (pSi->particleList == SCR_BRICK)
                continue;

            if ((rect.x + rect.w) >= wid)
                rect.w = wid - rect.x;

            if ((rect.y + rect.h) >= hei)
                rect.h = hei - rect.y;

            if ((x >= wid) || (y >= hei))
            {
                continue;
            }

            c = FIELD(pSi) / (DENSITY_TYPE)(1<<GETVAL32(potDivider)) + diffArray[x + y*wid] / (DENSITY_TYPE)(1<<GETVAL32(rippleDivider));

            if (c > 127)
                c = 127;
            else if (c < -127)
                c = -127;

            if (c < 0)
                SDL_FillRect (screen, &rect, ((-c<<8)/* | (-c >> 1)*/) | 0x01000000);
            else
                SDL_FillRect (screen, &rect, (((c)<<16)) | 0x01000000);

        }
    }
    stencilCenter (NULL, &x, &y);
    drawStencilSprite (NULL, x, y, 0);
    redrawSelectionBox();
}

void clearProgressBar()
{
}

void progress (char *what, int min, int max, int current)
{
    static int progressStarted = 0;

    if (what == NULL)
    {
        progressStarted = 0;
        clearProgressBar();
        return;
    }

    if (!progressStarted)
    {
        progressStarted = 1;
    }
}

static surface_t *cursorSurface = NULL;

void setCursor (void *pixmap)
{
    if (cursor)
        SDL_FreeCursor (cursor);

#ifdef USE_OPENGL
    if (pixmap == (void *) pixel_cur_xpm)
        pixmap = (void *) pixel_xpm;

    if (!options->useGlCursor)
    {
        cursor = makeCursor ((void*)pixel_cur_xpm);
        SDL_SetCursor (cursor);
    }
    else
    {
        SDL_SetCursor (emptyCursor);
    }
#else
    cursor = makeCursor (pixmap);

    SDL_SetCursor (cursor);
#endif


    cursorSurface = pmToSurface (pixmap);

    if (cursorSurface == NULL)
    {
        printf ("Unknown pixmap for cursor\n");
        return;
    }

#ifdef USE_OPENGL
    if (!cursorSurface->texture)
    {
        oglInitSurface (cursorSurface);
    }
    if (cursorSurface->texture)
        cursorSurface->texture->needsUpdate = 1;
#endif

}

surface_t *currentCursor()
{
    return cursorSurface;
}

/* walk all cursor pixel coordinates
 * int cb(int ix, int iy, void *arg)
 *      Return code != 0 terminats search.
 *
 */
void walkCursor(int ix, int iy, FUNCPTR cb, void *arg)
{
    int xx, yy, px, py;
    int sx;
    int sy;
    
    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = ix + INTERNC(xx - 16 + CORR_X);
            py = iy + INTERNC(yy - 16 + CORR_Y);
            sx = SCREENC(px);
            sy = SCREENC(py);
            
            if (!inSelection(sx, sy))
                continue;

            if (cb (px, py, arg) == 1)
                return;
        }
    }
}

void updateNeighbours (screeninfo *pSi, int diff)
{
    int dir;

    for (dir = 0; dir < 8; dir++)
    {
        SI_DIR(pSi, dir)->neighbours += diff;
    }
}

