/*
Copyright (C) 2014 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/
#include "plansch.h"

static pWell_t *wells = NULL;

/* Callout queue entry for a well
 */
void executeWell (void *id, pWell_t *well)
{
    int xx, yy, px, py, vx, vy;
    int pt;
    particle *p;
    vec_t a[400];
    int n;

    if (enginesStopped())
        goto out;

    /* increase probablility of particle creation as long as the mouse button is pressed
     * (well->starting == 1)
     *
     * Increase the probability indicator
     */
    if (well->starting && (well->prob < 100))
    {
        well->prob = MIN(well->prob + 5, 100);

        n = makeAnglePoly (a, well->pos.x, well->pos.y, -well->vector.x, -well->vector.y, (int)((float)well->prob * 3.6f), INTERNC(8));
        polySetCoords (well->poly, n, a, NULL);
        polyShow (well->poly);
    }

    /* make subsequent getValue() calls to provide the frozen per-particle settings for this well.
     */
    useFrozenSet ((void*)well);
    
    pt = getValue(&ptype);

    /* draw particles based on saved stencil
     */
    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (well->stencil[3 + yy][xx] == ' ')
                continue;

            /* random creation based on configured probability
             */
            if (RND(100) > well->prob)
                    continue;

            px = well->pos.x + INTERNC(xx - 16 + CORR_X);
            py = well->pos.y + INTERNC(yy - 16 + CORR_Y);

            if ((px < 0) || (px >= INTERNC(wid)) || (py < 0) || (py >= INTERNC(hei)))
                continue;

            makeParticleVec (&well->vector, well->angle, &vx, &vy);

            p = newParticle (px, py, vx, vy);

            if (!p)
                    continue;

#ifdef USE_OPENCL
            clCommitJobs();
#endif

            p->size = getValue (&particleSize[pt]);
        }
    }

    /* revert to current set of particle attributes
     */
    useFrozenSet (NULL);

    /* schedule next invocation
     */
out:
    calloutSched (well->cbId, well->interval, (FUNCPTR)executeWell, (void*)well);
}

void createWell (int ix, int iy)
{
    pWell_t *well = calloc (sizeof(*well), 1);


    well->interval = 50;

    /* define position and vector of particles
     */
    well->pos.x = ix;
    well->pos.y = iy;
    getArrowVec (&well->vector.x, &well->vector.y);
    well->angle = getArrowAngle();

    /* if no current vector, make particles move outward in all directions
     */
    if ((well->vector.x == 0) && (well->vector.y == 0))
    {
        well->vector.x = 10;
        well->angle = 360;
    }

    /* use current stencil
     */
    well->stencil = stencil;

    well->cbId = calloutNew();

    /* start a half-circle which fills up the longer the button is pressed
     * showing th probability of particle creation
     */
    uint32_t col = ARGB(0xffff0000);
#ifdef USE_OPENGL
    well->poly = polyNew (360, POLY_FILLED, col);
#else
    well->poly = polyNew (360, 0, col);
#endif

    /* start with a low probability 
     */
    well->prob = 1;
    well->starting = 1;

    /* freeze current particle settings using the well pointer as key
     */
    freezeParticleSettings ((void*)well);

    well->next = (void*)wells;
    wells = well;

    /* schedule callout queue entry for this well
     */
    calloutSched (well->cbId, well->interval, (FUNCPTR)executeWell, (void*)well);
}

void removeWell (int sx, int sy)
{
    pWell_t *well, *prev, *next;

    prev = NULL;
    for (well = wells; well != NULL; well = next)
    {
        next = well->next;

        if ((SCREENC(well->pos.x) == sx) && (SCREENC(well->pos.y) == sy))
        {
            calloutUnsched (well->cbId);
            removeFrozenSet ((void*)well);
            polyDelete (well->poly);

            if (!prev)
            {
                wells = next;
            }
            else
            {
                prev->next = next;
            }
            free (well);
        }
        else
        {
            prev = well;
        }
    }
}

void completeWell(void)
{
    pWell_t *well;
    for (well = wells; well != NULL; well = well->next)
    {
        if (well->starting)
        {
            well->starting = 0;
            polyChangeColor (well->poly, -1, ARGB(0x800000ff));
        }
    }
}
