/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

static int backStoreCleared;
static uint32_t *backStore = NULL;
static uint8_t *backStoreFlg = NULL;

typedef struct
{
    int dx, dy;
    FUNCPTR cb;
    intptr_t arg1, arg2, arg3, arg4;
} dlf;



/* the back store is used when drawing temporary screen elements, like arrows, lines, ...
 * Whenever a pixel is drawn, the previous pixel is saved to the back store, unless it hasn't 
 * already been saved. The saved marker is kept in a seperate array.
 * When the original value of the pixel shall be restored, it's checked whether it has been saved
 * and the pixel value is restored if this is the case
 */
void initBackStore ()
{
    if (backStore)
        return;

    backStore = calloc(wid * hei * sizeof(*backStore), 1);
    backStoreFlg = calloc(wid * hei * sizeof(*backStoreFlg), 1);

    backStoreCleared = 0;

    clearBackStore();
}

void clearBackStore()
{
    if (backStoreCleared)
        return;

    memset ((char*)backStore, 0, wid * hei * sizeof(*backStore));
    memset ((char*)backStoreFlg, 0, wid * hei * sizeof(*backStoreFlg));
    backStoreCleared = 1;
}

/* restore x/y pixel from back store if it has been saved
 */
void restoreFromBackStore (int x, int y)
{
    if (((uint)x >= wid) || ((uint)y >= hei))
        return;

    if (backStoreFlg[x + y*wid])
    {
        SCR_SET (x, y, backStore[x + y*wid]);
        backStoreFlg[x + y*wid] = 0;
    }

    backStore[x + y*wid] = 0;
}


/* save x/y pixel to back store unless it has already been saved */
void saveToBackStore (int x, int y)
{
    if (((uint)x >= wid) || ((uint)y >= hei))
        return;

    if (backStoreFlg[x + y*wid])
        return;

    backStore[x + y*wid] = SCR_GET(x,y);
    backStoreFlg[x + y*wid] = 1;

    backStoreCleared = 0;
}

int escapePressed(void)
{
    SDL_Event event;

    while (SDL_PollEvent (&event))
        if (event.type == SDL_KEYDOWN)
            if (event.key.keysym.sym == SDLK_ESCAPE)
                return 1;
    
    return 0;
}

intptr_t lineCb (int x, int y, int dx, int dy, intptr_t col, intptr_t bg, intptr_t remove, intptr_t force)
{

    if ((x < 0) || (x >= wid) || (y < 0) || (y >= hei))
    {
        if (force)
            return 1;

        return 0;
    }

    if (remove)
    {
        restoreFromBackStore (x, y);
    }
    else
    {
        saveToBackStore (x, y);

        SCR_SET (x, y, col);
    }

    return 1;
}

int circleCb (int x, int y, int col, int bg, int remove)
{
    x = SCREENC(x);
    y = SCREENC(y);


    if ((x < 0) || (x >= wid) || (y < 0) || (y >= hei))
        return 1;

    if (remove)
    {
        restoreFromBackStore (x, y);
    }
    else if (backStoreFlg[x + y*wid] == 0)
    {
        saveToBackStore (x, y);

        SCR_SET (x, y, col);
    }

    return 1;
}

void drawBox (int x1, int y1, int x2, int y2, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    int x, y;
    if ((x1 == x2) || (y1 == y2))
        return;

    if (x1 > x2)
    {
        x = x1;
        x1 = x2;
        x2 = x;
    }
        
    if (y1 > y2)
    {
        y = y1;
        y1 = y2;
        y2 = y;
    }

    for (x = x1, y = y1; x <= x2; x++)
    {
        if (!cb (x, y, 1, 0, arg1, arg2, arg3, arg4))
        {
            return;
        }
    }
    for (y++; y <= y2; y++)
    {
        if (!cb (x, y, 0, 1, arg1, arg2, arg3, arg4))
        {
            return;
        }
    }
    for (x--; x >= x1; x--)
    {
        if (!cb (x, y, -1, 0, arg1, arg2, arg3, arg4))
        {
            return;
        }
    }
    for (y--; y > y1; y--)
    {
        if (!cb (x, y, 0, -1, arg1, arg2, arg3, arg4))
        {
            return;
        }
    }
}



void drawPolyLineF (vec_t *cords, int nCords, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    int                x1, y1, x2, y2;
    int                xx1, xx2, yy1, yy2;
    int                dx, dy;
    FLOAT        vl;
    int                i;
    int                first;

    if (nCords < 2)
        return;

    first = 0;
    for (i = 0; i < nCords-1; i++)
    {
        x1 = cords[i].d.x;
        y1 = cords[i].d.y;
        x2 = cords[i+1].d.x;
        y2 = cords[i+1].d.y;

        vl = VEC_LEN(x2-x1, y2-y1);

        xx1 = INTERNC(x1) + GRID/2;
        xx2 = INTERNC(x2) + GRID/2;
        yy1 = INTERNC(y1) + GRID/2;
        yy2 = INTERNC(y2) + GRID/2;

        if ((int)vl == 0)
            return;

        dx = (xx2 - xx1) / (int)vl;
        dy = (yy2 - yy1) / (int)vl;

        if ((dx == 0) && (dy == 0))
            continue;

        while ((((dx > 0) && (x1 <= x2)) || ((dx < 0) && (x1 >= x2)) || (dx == 0)) &&
               (((dy > 0) && (y1 <= y2)) || ((dy < 0) && (y1 >= y2)) || (dy == 0)))
        {
            /* at the start of a new line, do not draw the 1st pixel. It has already been
             * drawn at the end of the previous line.
             */
            if (!first)
            {
                if (!cb (x1, y1, dx, dy, arg1, arg2, arg3, arg4))
                {
//                    return;
                }
            }

            xx1 += dx;
            yy1 += dy;

            x1 = SCREENC(xx1);
            y1 = SCREENC(yy1);

            first = 0;
        }

        first = 1;
    }
}

static int lcb (uint offset, dlf *a)
{
    return a->cb(offset % wid, offset / wid, a->dx, a->dy, a->arg1, a->arg2, a->arg3, a->arg4);
}

/* line from x1/y1 to x2/y2, calling function
 *
 * int cb (x,y,dx,dy,arg1,arg2,arg3,arg4)
 */
void drawLineF (int x1, int y1, int x2, int y2, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    dlf lineArgs;
    int xx1, xx2, yy1, yy2;
    FLOAT        vl = VEC_LEN(x2-x1, y2-y1);

    xx1 = INTERNC(x1) + GRID/2;
    xx2 = INTERNC(x2) + GRID/2;
    yy1 = INTERNC(y1) + GRID/2;
    yy2 = INTERNC(y2) + GRID/2;


    if ((int)vl == 0)
    {
        lineArgs.dx = lineArgs.dy = 0;
    }
    else
    {
        lineArgs.dx = (xx2 - xx1) / (int)vl;
        lineArgs.dy = (yy2 - yy1) / (int)vl;
    }

    lineArgs.arg1 = arg1;
    lineArgs.arg2 = arg2;
    lineArgs.arg3 = arg3;
    lineArgs.arg4 = arg4;
    lineArgs.cb = cb;

    rasLine (x1, y1, x2, y2, wid, hei, (FUNCPTR)lcb, (intptr_t)&lineArgs);
}
/* line from x1/y1 to x2/y2, calling function
 *
 * int cb (x,y,dx,dy,arg1,arg2,arg3,arg4)
 */
void drawLineIF (int x1, int y1, int x2, int y2, int step, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    FLOAT        xx1, xx2, yy1, yy2;
    FLOAT        vl = VEC_LEN(x2-x1, y2-y1);
    FLOAT        fstep = step;
    FLOAT        fdx, fdy;
    FLOAT        o;

    xx1 = x1;
    xx2 = x2;
    yy1 = y1;
    yy2 = y2;

    if ((int)vl == 0)
    {
        fdx = fdy = 0.f;
    }
    else
    {
        fdx = (xx2 - xx1) / vl;
        fdy = (yy2 - yy1) / vl;
    }

    for (o = 0.f; o <= vl; o += fstep)
    {
        cb ((int)(xx1 + o*fdx), (int)(yy1 + o*fdy), (int)fdx, (int)fdy, arg1, arg2, arg3, arg4);
    }
}


void textPlotC (int x, int y, char *text, int col, int pico)
{
    SDL_Surface        *textSurface;
    int xx, yy;
    SDL_Color tcCol;
    tcCol.r = (col >> 16) & 0xff;
    tcCol.g = (col >> 8) & 0xff;
    tcCol.b = (col >> 0) & 0xff;

#ifdef TTF_SUPPORT
    if (!pico)
    {
        textSurface = TTF_RenderText_Blended(sdlFont, text, tcCol);
    }
    else
#endif
    {
        textSurface = FNT_Render(text, tcCol);
    }

    if (textSurface == NULL)
    {
        printf ("TTF_RenderText_Solid failed \n");
        return;
    }

    for (xx = 0; xx < textSurface->w; xx++)
    {
        if (x + xx >= wid)
            break;
        
        for (yy = 0; yy < textSurface->h; yy++)
        {
            if (yy >= options->scrHei)
                break;

            if (SURFACE_GET8 (textSurface, xx, yy) > 0)
            {
                SCR_SET_SAFE (x+xx, y+yy, col);
            }
        }
    }
    SDL_FreeSurface (textSurface);
}

void textPlot (int x, int y, char *text)
{
    SDL_Surface        *textSurface;
    SDL_Color textCol = {255,255,255,0};
    SDL_Rect src;
    SDL_Rect dst;

#ifdef TTF_SUPPORT
    textSurface = TTF_RenderText_Blended(sdlFont, text, textCol);
#else
    textSurface = FNT_Render(text, textCol);
#endif
    if (textSurface == NULL)
    {
        printf ("TTF_RenderText_Solid failed \n");
        return;
    }

    dst.x = x;
    dst.y = y;
    dst.w = textSurface->w;
    dst.h = textSurface->h;

    src.x = src.y = 0;
    src.w = textSurface->w;
    src.h = textSurface->h;

    if (SDL_BlitSurface(textSurface, &src,
                        screen, &dst) == -1)
    {
        printf ("textPlot: SDL_BlitSurface failed (src=%d/%d/%d/%d dst=%d/%d/%d/%d\n:%s\n",
                src.x, src.y, src.w, src.h,
                dst.x, dst.y, dst.w, dst.h,
                SDL_GetError());
        SDL_UnlockSurface (screen);
    }
    SDL_FreeSurface (textSurface);
}

void drawArrowF (int x1, int y1, int x2, int y2, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    vec_t arrowPoly[8];
    int n;

    n = makeArrowPoly (arrowPoly, x1, y1, x2-x1, y2-y1);
    drawPolyLineF (arrowPoly, n, cb, arg1, arg2, arg3, arg4);
}

void renderTextBox (int x, int y, char *banner, char *text, SDL_Surface **back, SDL_Rect *backPos)
{
    SDL_Surface *lines[20];
    int maxx, maxy;
    char tmpStr[1000];
    int textPos, linePos;
    int numLines;
    int i;
    SDL_Color textCol = {255,255,255,0};
    SDL_Rect r, s;
    int textLen = strlen(text);
    int spacing = 1;

    maxx = maxy = 0;
    textPos = 0;
    for (i = 0; i < NELEMENTS(lines) && (textPos < textLen); i++)
    {
        if ((i == 0) && banner)
        {
            strcpy (tmpStr, banner);
            spacing = 4;
        }
        else
        {
#ifdef TTF_SUPPORT
            spacing = -3;
#else
            spacing = 1;
#endif
            for (linePos = 0; (text[textPos] != '\n') && (text[textPos] != 0); textPos++, linePos++)
            {
                tmpStr[linePos] = text[textPos];
            }
            textPos++;

            tmpStr[linePos] = 0;
        }

#ifdef TTF_SUPPORT
        lines[i] = TTF_RenderText_Blended(sdlFont, tmpStr, textCol);
#else
        lines[i] = FNT_Render(tmpStr, textCol);
#endif

        if (lines[i] == NULL)
        {
            printf ("failed to render text line: <%s>\n", tmpStr);
            break;
        }

        maxx = MAX(lines[i]->w, maxx);
        maxy += lines[i]->h + spacing;
    }

    numLines = i;

    if (numLines == 0)
    {
        return;
    }

    if ((x == -1) && (y == -1))
    {
        x = wid / 2 - maxx/2;
        if (x < 10)
            x = 10;

        y = (hei + options->scrHei) / 2 - maxy / 2;
        if (y < (hei + 10))
            y = hei + 10;

        r.x = x-5;
        r.y = y-5;
        r.w = maxx+10;
        r.h = maxy+10;
    }
    else
    {
        r.x = x;
        r.y = y;
        r.w = maxx + 10;
        r.h = maxy + 10;

        if (r.x + r.w >= options->scrWid)
            r.x -= r.w;
        if (r.y + r.h >= options->scrHei)
            r.y -= r.h;

        x = r.x + 5;
        y = r.y + 5;
    }


    if (back)
    {
        SDL_Surface *b;

        b = SDL_CreateRGBSurface (SDL_SWSURFACE, r.w, r.h, 32, 0xff,0xff00,0xff0000,0xff000000);
        SDL_SetAlpha (b, 0, 0xff);

        SDL_BlitSurface (screen, &r, b, &b->clip_rect);

        *back = b;
        *backPos = r;
    }

    SDL_FillRect(screen, &r, COL_ORANGE);
    
    r.x = x-1;
    r.y = y-1;
    r.w = maxx+2;
    r.h = maxy+2;
    SDL_FillRect(screen, &r, COL_BLUE);

    r.y = y;
    r.w = maxx;
    r.h = maxy;
    for (i = 0; i < numLines; i++)
    {
        if ((i == 0) && banner)
        {
            SDL_Rect r2;
            r.x = x + maxx/2 - lines[i]->w/2;

            r2.x = r.x;
            r2.y = r.y;
            r2.w = lines[i]->w;
            r2.h = lines[i]->h;

            SDL_FillRect(screen, &r2, COL_RED);
            spacing = 4;
        }
        else
        {
            r.x = x;
#ifdef TTF_SUPPORT
            spacing = -3;
#else
            spacing = 1;
#endif
        }
        s.x = s.y = 0;
        s.w = lines[i]->w;
        s.h = lines[i]->h;
        SDL_BlitSurface (lines[i], &s, screen, &r);
        r.y += lines[i]->h + spacing;
        r.h -= lines[i]->h;

        SDL_FreeSurface (lines[i]);
    }
    SCR_CHANGED;
}

void drawFilledCircle (int cx, int cy, int rx, int ry, int step, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    float rad, radStep;
    int x, y;
    int flag;

    if ((rx == 0) && (ry == 0))
        return;

    flag = DRELEM_FIRST;

    while ((rx > 0) && (ry > 0))
    {
        if (abs(rx) > abs(ry))
        {
            radStep = atan2 ((float)step, (float)(abs(rx)));
            if (radStep < 0)
                radStep += 2 * M_PI;
        }
        else
        {
            radStep = atan2 ((float)step, (float)(abs(ry)));
            if (radStep < 0)
                radStep += 2 * M_PI;
        }

        for (rad = 0; rad < 2 * M_PI; rad += radStep)
        {
            x = cx + cos(rad) * (float)rx;
            y = cy - sin(rad) * (float)ry;

            if (cb (x, y, flag, arg1, arg2, arg3, arg4) == 0)
                return;

            flag &= ~DRELEM_FIRST;
        }

        rx -= step;
        ry -= step;
    }
    cb (cx, cy, flag|DRELEM_LAST, arg1, arg2, arg3, arg4);
}

void drawCircle (int cx, int cy, int rx, int ry, int step, int off, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4)
{
    float rad, radStep, radOff;
    int x, y;
    int sx, sy;
    int flag;

    if ((rx == 0) && (ry == 0))
        return;

    if (abs(rx) > abs(ry))
    {
        radStep = atan2 ((float)step, (float)(abs(rx)));
        if (radStep < 0)
            radStep += 2 * M_PI;
    }
    else
    {
        radStep = atan2 ((float)step, (float)(abs(ry)));
        if (radStep < 0)
            radStep += 2 * M_PI;
    }

    radOff = radStep / off; 

    flag = DRELEM_FIRST;

    sx = sy = 0;
    for (rad = 0; rad < 2 * M_PI; rad += radStep)
    {
        x = cx + cos(rad + radOff) * (float)rx;
        y = cy - sin(rad + radOff) * (float)ry;

        if (flag & DRELEM_FIRST)
        {
            sx = x;
            sy = y;
        }

        if (cb (x, y, flag, arg1, arg2, arg3, arg4) == 0)
            return;

        flag &= ~DRELEM_FIRST;
    }
    cb (sx, sy, flag|DRELEM_LAST, arg1, arg2, arg3, arg4);
}


void initDrPrimitives()
{
    initBackStore();
}


/* code base for line drawing taken from SDL_gfx ..
*/

#define CLIP_LEFT_EDGE   0x1
#define CLIP_RIGHT_EDGE  0x2
#define CLIP_BOTTOM_EDGE 0x4
#define CLIP_TOP_EDGE    0x8
#define CLIP_INSIDE(a)   (!a)
#define CLIP_REJECT(a,b) (a&b)
#define CLIP_ACCEPT(a,b) (!(a|b))
static int clip_encode(int x, int y, int left, int top, int right, int bottom)
{
    int code = 0;
    if (x < left) {
        code |= CLIP_LEFT_EDGE;
    } else if (x > right) {
        code |= CLIP_RIGHT_EDGE;
    }
    if (y < top) {
        code |= CLIP_TOP_EDGE;
    } else if (y > bottom) {
        code |= CLIP_BOTTOM_EDGE;
    }
    return code;
}

int clip(int w, int h, int * x1, int * y1, int * x2, int * y2)
{
    int left, right, top, bottom;
    int code1, code2;
    int draw = 0;
    int swaptmp;
    float m;

    /*
     * Get clipping boundary
     */
    left = 0;
    right = w - 1;
    top = 0;
    bottom = h - 1;
    while (1) {
        code1 = clip_encode(*x1, *y1, left, top, right, bottom);
        code2 = clip_encode(*x2, *y2, left, top, right, bottom);
        if (CLIP_ACCEPT(code1, code2)) {
            draw = 1;
            break;
        } else if (CLIP_REJECT(code1, code2))
            break;
        else {
            if (CLIP_INSIDE(code1)) {
                swaptmp = *x2;
                *x2 = *x1;
                *x1 = swaptmp;
                swaptmp = *y2;
                *y2 = *y1;
                *y1 = swaptmp;
                swaptmp = code2;
                code2 = code1;
                code1 = swaptmp;
            }
            if (*x2 != *x1) {
                m = (*y2 - *y1) / (float) (*x2 - *x1);
            } else {
                m = 1.0f;
            }
            if (code1 & CLIP_LEFT_EDGE) {
                *y1 += (Sint16) ((left - *x1) * m);
                *x1 = left;
            } else if (code1 & CLIP_RIGHT_EDGE) {
                *y1 += (Sint16) ((right - *x1) * m);
                *x1 = right;
            } else if (code1 & CLIP_BOTTOM_EDGE) {
                if (*x2 != *x1) {
                    *x1 += (Sint16) ((bottom - *y1) / m);
                }
                *y1 = bottom;
            } else if (code1 & CLIP_TOP_EDGE) {
                if (*x2 != *x1) {
                    *x1 += (Sint16) ((top - *y1) / m);
                }
                *y1 = top;
            }
        }
    }
    return draw;
}

void rasLine (int x1, int y1, int x2, int y2, int w, int h, FUNCPTR cb, intptr_t arg1 )
{
    int x, y;
    int dx, dy;
    int sx, sy;
    int swaptmp;
    uint pixel;
    int pixx, pixy;

    if (!(clip(w, h, &x1, &y1, &x2, &y2))) {
        return;
    }

    /*
     * Variable setup
     */
    dx = x2 - x1;
    dy = y2 - y1;
    sx = (dx >= 0) ? 1 : -1;
    sy = (dy >= 0) ? 1 : -1;

    /*
     * More variable setup
     */
    dx = sx * dx + 1;
    dy = sy * dy + 1;
    pixx = 1;
    pixy = wid;
    pixel = pixx * x1 + pixy * y1;
    pixx *= sx;
    pixy *= sy;
    if (dx < dy) {
        swaptmp = dx;
        dx = dy;
        dy = swaptmp;
        swaptmp = pixx;
        pixx = pixy;
        pixy = swaptmp;
    }
    /*
     * Draw
     */
    x = 0;
    y = 0;

    for (; x < dx; x++, pixel += pixx) {

        if (!cb (pixel, arg1))
            return;

        y += dy;
        if (y >= dx) {
            pixel += pixy;
            if (x < dx-1)
                if (!cb (pixel, arg1))
                    return;
            y -= dx;
        }
    }
}

static int CompareInt(const void *a, const void *b)
{
    return (*(const int *) a) - (*(const int *) b);
}

void rasFill(int * vx, int* vy, int n, int w, int h, FUNCPTR cb, intptr_t arg)
{
    int i;
    int y, xa, xb;
    int miny, maxy;
    int x1, y1;
    int x2, y2;
    int ind1, ind2;
    int ints;
    int *polyInts;
    /*
     * Sanity check
     */
    if (n < 3) {
        return;
    }
    /*
     * Allocate temp array, only grow array
     */
    polyInts = (int *) alloca(sizeof(int) * n);
    /*
     * Determine Y maxima
     */
    miny = vy[0];
    maxy = vy[0];
    for (i = 1; (i < n); i++) {
        if (vy[i] < miny) {
            miny = vy[i];
        } else if (vy[i] > maxy) {
            maxy = vy[i];
        }
    }
    /*
     * Draw, scanning y
     */
    for (y = miny; (y <= maxy); y++) {
        ints = 0;
        for (i = 0; (i < n); i++) {
            if (!i) {
                ind1 = n - 1;
                ind2 = 0;
            } else {
                ind1 = i - 1;
                ind2 = i;
            }
            y1 = vy[ind1];
            y2 = vy[ind2];
            if (y1 < y2) {
                x1 = vx[ind1];
                x2 = vx[ind2];
            } else if (y1 > y2) {
                y2 = vy[ind1];
                y1 = vy[ind2];
                x2 = vx[ind1];
                x1 = vx[ind2];
            } else {
                continue;
            }
            if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
                polyInts[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
            }
        }
        qsort(polyInts, ints, sizeof(int), CompareInt);
        for (i = 0; (i < ints); i += 2) {
            xa = polyInts[i] + 1;
            xa = (xa >> 16) + ((xa & 32768) >> 15);
            xb = polyInts[i+1] - 1;
            xb = (xb >> 16) + ((xb & 32768) >> 15);
            /* XXX changed: using our own hline function */
            rasLine (xa, y, xb, y, w, h, cb, arg);
        }
    }
}


