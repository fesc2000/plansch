/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#ifdef USE_OPENCL
#include "opencl.h"
#endif

static int myType = 0;

void clParameterCb (setting *s);

SETTING(gpuBorderFieldCutoff) =
{
    .minValue = 0,
    .maxValue = 1000,
    .defaultValue = 950,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Field bundary",
    .cb = (FUNCPTR)clParameterCb
};


SETTING(clNumLinks) =
{
    .minValue = 1,
    .maxValue = MAX_LINKS,
    .defaultValue = 6,
    .minDiff = 1,
    .canBeRandom = 1,
    .discrete = 1,
    .numDiscrete = 6,
    .partAttr = 1,
    .valueArray = { 1, 2, 3, 4, 5, 6 },

    .name = "NumberOfClLinks"
};

SETTING (clLinkInterval) =
{
    .minValue = 1,
    .maxValue = 20,
    .defaultValue = 2,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Interval for GPU Particle Link Check",
#ifdef USE_OPENCL
    .cb = (FUNCPTR)clSetLinkInterval
#endif
};

SETTING (clMaxLinkLength) =
{
    .minValue = 1,
    .maxValue = 1000,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Maximum link length",
    .cb = (FUNCPTR)clParameterCb
};

SETTING (clLinkLength) =
{
    .minValue = 0,
    .maxValue = 500,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Target distance of linked particles",
    .cb = (FUNCPTR)clParameterCb
};

SETTING (clLinkStrength) =
{
    .minValue =    1,
    .maxValue = 500,
    .defaultValue = 500,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Link Strength",
    .cb = (FUNCPTR)clParameterCb
};


SETTING (clDoLink) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 1,
    .name = "Link on Collission",
    .cb = (FUNCPTR)clParameterCb
};

SETTING (clParticleTrailLen) =
{
    .minValue = 0,
    .maxValue = CL_MAX_TRAIL_LENGTH,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Cl Particle Trail Length",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuSpeedLimit) =
{
    .minValue =         1,
    .maxValue =       100,
    .defaultValue =    100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Speed Limit",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuTimeFac) =
{
    .minValue =         1,
    .maxValue =       100,
    .defaultValue =    100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Time Factor",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuCollDamping) =
{
    .minValue =         0,
    .maxValue =       100,
    .defaultValue =    50,
    .floatDivi =     1000,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Damping Factor",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuCollShear) =
{
    .minValue =       0,
    .maxValue =     100,
    .defaultValue = 50,
    .floatDivi =   1000,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Shear Factor",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuCollSpring) =
{
    .minValue =       0,
    .maxValue =    1000,
    .defaultValue = 300,
    .floatDivi =   1000,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Spring Factor",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuCollAttraction) =
{
    .minValue =       0,
    .maxValue =     100,
    .defaultValue =   0,
    .floatDivi =   1000,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Attraction Factor",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuCollDist) =
{
    .minValue =       0,
    .maxValue =     100,
    .defaultValue = 100,
    .floatDivi =    100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Collission Distance",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(gpuCollission) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 1,
    .minDiff = 1,
    .partAttr = 1,
    .name = "GPU Particle Collission",
};

SETTING(clOscillator) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Oscillating Particle",
};

SETTING(clAvgMode) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 1,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Density Averaging",
};

SETTING(lumSpringFac) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Luminosity difference affects spring factor",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(clLinkAttractionMode) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 1,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Link attraction mode",
    .cb = (FUNCPTR)clParameterCb
};
SETTING(clMakeBlob) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Make a blob with next drawop"
};

SETTING(clSticky) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 1,
    .name = "Stick particle to wall"
};

SETTING (clColExchThreshR) =
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Color Exchange threshold (Red)",
    .cb = (FUNCPTR)clParameterCb
};

SETTING (clColExchThreshG)=
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Color Exchange threshold (Green)",
    .cb = (FUNCPTR)clParameterCb
};

SETTING (clColExchThreshB)=
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Color Exchange threshold (Blue)",
    .cb = (FUNCPTR)clParameterCb
};


static void collTargetColorCb (setting *s);

SETTING(clCollissionCol) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Target Color on collission",
    .cb = (FUNCPTR)clParameterCb
};

SETTING(collTargetColor) =
{
    .minValue = 0,
    .maxValue = 0,
    .defaultValue = 0,
    .name = "collisionTargetColor",
    .colorSelection = 1,
    .cb=(FUNCPTR)collTargetColorCb
};

SETTING(einsteinFactor) =
{
    .minValue = 0,
    .maxValue = 1000,
    .defaultValue = 0,
    .name = "einsteinFactor",
    .cb=(FUNCPTR)clParameterCb
};

SETTING(clLinkMode) =
{
    .minValue = 0,
    .maxValue = 2,
    .defaultValue = 0,
    .name = "Link Mode",
    .cb=(FUNCPTR)clParameterCb
};


/* a reasonable value for the field border behavior, depending on 
 * gravity strength.
 */
void setFieldCutoffDfl(int val)
{
    float x = (float)val / 10.0;
    float rc;

    if (x <= 104.0)
    {
        rc = -7.598957124395922e-7*x*x*x-5.87707998689903e-62*x*x+0.14283441664113125*x+600.0;
    }
    else if (x < 800.0)
    {
        rc = +4.4074146983265964e-7*x*x*x-0.0003745988008689426*x*x+0.18179269193150127*x+598.6494464566005;
    }
    else if (x < 950.0)
    {
        rc = +0.00001846805889054893*x*x*x-0.043640160610587994*x*x+34.794242139706746*x-8631.337072950131;
    }
    else
    {
        rc = -0.00005995871484984307*x*x*x+0.1798761445495292*x*x-177.5462477624046*x+58609.818062718456;
    }
    gpuBorderFieldCutoff.defaultValue = (int)rc;
}

static void collTargetColorCb (setting *s)
{
#ifdef USE_OPENCL
    clParameters_t *p = clParametersGet();
    buttonBgColor (clCollissionCol.pButton, ARGB(s->current.val.cv.argb | 0xff000000));

    p->collTargetColor.s0 = (float)CH_GET_R(ARGB(s->current.val.cv.argb)) / 255.0f;
    p->collTargetColor.s1 = (float)CH_GET_G(ARGB(s->current.val.cv.argb)) / 255.0f;
    p->collTargetColor.s2 = (float)CH_GET_B(ARGB(s->current.val.cv.argb)) / 255.0f;
    p->collTargetColor.s3 = (float)CH_GET_A(ARGB(s->current.val.cv.argb)) / 255.0f;

    clParametersNotifyChange();
#endif
}


#ifdef USE_OPENCL
static int canOverlap()
{
    if (currentDrawMode() == DR_DRAW)
        return 0;

    return 1;
}
#endif

static int clDrawInterval()
{
    if (currentDrawMode() == DR_DRAW)
        return GRID;

    if (currentDrawMode() == DR_LINE)
        return (int)((float)GRID * ((float)GETVAL32(particleSize[myType]) / 255.0f));

    return GRID-1;
}

static particle *clNewParticle (int x, int y, int vx, int vy, 
                          int size, int charge, int mass, int frcInv, uint32_t col)
{
#ifdef USE_OPENCL
    vec_t pos, v;
    uint32_t flags;

    flags = canOverlap() ? 0 : CL_MFLG_NEW;

    pos.d.x = x;
    pos.d.y = y;
    v.d.x = vx;
    v.d.y = vy;
    int links = 0;

    if (playbackTape.recording)
    {
        particle p;

        memset (&p, 0, sizeof(p));
        p.pos = pos;
        p.vec = v;
        p.mass = mass = mass;
        p.charge = charge;
        p.size = size;
        p.type = myType;
        p.color.icolor.col32 = col;

        recordNewParticle(&playbackTape, &p); 
    }
        
    if (GETVAL32(clDoLink))
        links = GETVAL32(clNumLinks);

    if (GETVAL32(gpuCollission))
        flags |= CL_MFLG_COLLIDE; 
    else
        flags |= CL_MFLG_WIMP; 

    if (frcInv)
        flags |= CL_MFLG_INVERT_FRC;

    if (GETVAL32(clMakeBlob))
        flags |= CL_MFLG_BLOB_NEW;

    if (GETVAL32(clSticky))
        flags |= CL_MFLG_STICKY;

    if (clSchedNewParticle (&pos, &v, charge, mass, col, links, flags, size))
    {
        return dummyPart();
    }
#endif

    return NULL;
}

static void handleSetting (setting *s, void *arg)
{
}

static int clParticleInit (int ptype)
{
    myType = ptype;

    registerSettingCallback ((FUNCPTR)handleSetting, NULL);

    encodeARGB(&collTargetColor.current.val.cv, 1.0, 0.0, 0.0, 0.0);
    collTargetColor.defaultValue = collTargetColor.current.val.va64;

#ifdef USE_OPENCL
    return 1;
#endif

    return 0;
}

void clParameterCb (setting *s)
{
#ifdef USE_OPENCL
    clParameters_t *p = clParametersGet();
    float val = s->current.normFloat;

    if (s == &clParticleTrailLen)
    {
        clSchedClearTrails ();
        p->trails = GETVAL32(*s);
    }
    else if (s == &gpuCollDamping)
    {
        p->damping = val;
    }
    else if (s == &gpuCollShear)
    {
        p->shear = val;
    }
    else if (s == &gpuCollSpring)
    {
        p->spring = val;
    }
    else if (s == &gpuCollAttraction)
    {
        p->attraction = val;
    }
    else if (s == &gpuCollDist)
    {
        p->collideDist = val;
        p->collideDistQ = p->collideDist * p->collideDist;
    }
    else if (s == &gpuSpeedLimit)
    {
        p->speedLimit = GETVAL32(*s);
    }
    else if (s == &gpuTimeFac)
    {
        p->timeFactor = getValueNorm (s);
    }
    else if (s == &clMaxLinkLength)
    {
        p->maxLinkLength = getValueFlt (s);
    }
    else if (s == &clLinkLength)
    {
        p->linkLength = getValueNorm (s) * 5.0f;
        p->linkLengthNe0 = (GETVAL32(*s) != 0);
    }
    else if (s == &clLinkStrength)
    {
        p->linkStrength = getValueNorm (s) / 1000.0f;
    }
    else if (s == &lumSpringFac)
    {
        p->flags &= ~CL_EFLAG_LUMDIFF;
        if (GETVAL32(*s))
            p->flags |= CL_EFLAG_LUMDIFF;
    }
    else if (s == &clLinkAttractionMode)
    {
        p->linkForceAscends = GETVAL32(*s);
    }
    else if (s == &wallTempr)
    {
        p->flags &= CL_EFLAG_WALLHEAT;
        if (GETVAL32(*s))
            p->flags |= CL_EFLAG_WALLHEAT;
    }
    else if (s == &clColExchThreshR)
    {
        p->colorDiffThresh.s0 =
        p->colorDiffThresh.s1 =
        p->colorDiffThresh.s2 =
        p->colorDiffThresh.s3 = getValueNorm (s);
        
        if (GETVAL32(*s) == 100)
            p->flags |= CL_EFLAG_COLTHRESH;
        else
            p->flags &= ~CL_EFLAG_COLTHRESH;
        
    }
    else if (s == &clCollissionCol)
    {
        s->pButton->useSelMarker = 1;
        p->flags &= ~CL_EFLAG_COLTARGET;
        if (GETVAL32(*s))
        {
            p->flags |= CL_EFLAG_COLTARGET;
            pickColor (&collTargetColor);
        }
        else
        {
            exitPicker();
        }
    }
    else if (s == &einsteinFactor)
    {
        p->einsteinFactor = getValueNorm(s);
        p->einsteinFactor *= p->einsteinFactor;
        p->einsteinFactor *= p->einsteinFactor;
        p->einsteinFactor *= p->einsteinFactor;
    }
    else if (s == &clLinkMode)
    {
        switch (GETVAL32(*s))
        {
            case 0:
                p->ccMask = CL_MASK_CCOLL;
                p->ccVal  = CL_VAL_CCOLL;
                break;
            case 1:
                p->ccMask = CL_MASK_CDIFF;
                p->ccVal  = CL_VAL_CDIFF;
                break;
            case 2:
                p->ccMask = CL_MASK_CCOLLDIFF;
                p->ccVal  = CL_VAL_CCOLLDIFF;
                break;
        }
    }
    else if (s == &gpuGravWaveDamping)
    {
        p->gravWaveDamping = (float) getValueNorm (s);
        p->gravWaveDampingHalf = floatToHalf (p->gravWaveDamping);
    }
    else if (s == &gpuGravDamping)
    {
        p->gravDamping = 1.0f / ((float)(100 -  getValue (s)) / 10.f);
        p->gravDampingHalf = floatToHalf (p->gravDamping);
    }
    else if (s == &gpuGravStrength)
    {
        p->gravStrength = 1.0f / (6.0 + (float)(10000 -  getValue (s)) * (0.5f / 10000.0f));
        p->gravStrengthHalf = floatToHalf (p->gravStrength);
        setFieldCutoffDfl(GETVAL32(*s));
    }
    else if (s == &gpuBorderFieldCutoff)
    {
        p->fieldBorderCutoff = (float)getValueNorm (s);
    }
    else
    {
        return;
    }

    clParametersNotifyChange();
#endif
}

void clParticleMark (int add, int ix, int iy)
{
#ifdef USE_OPENCL
    vec_t pos;

    pos.x = ix;
    pos.y = iy;

    clTransferSiMarker ();
    clSchedMarkParticles (add ? CL_MARK_ADD : CL_MARK_CLEAR, &pos);
#endif
}

void clParticleFollow (int stop, int ix, int iy)
{
#ifdef USE_OPENCL
    vec_t pos;

    if (stop)
    {
        clFollowStop();
    }
    else
    {
        pos.x = ix;
        pos.y = iy;

        clTransferSiMarker ();
        clSchedMarkParticles (CL_FOLLOW, &pos);
    }
#endif
}

/* Schedule a CL job to 
 * - transfer pixel marker data to GPU
 * - change the particle attributes of 
 *   all cl particles on marked pixels.
 * Only attributes which are marked by user. If no relevant attributes
 * are marked, do nothing.
 */
void clParticleChattrSelected (int ix, int iy)
{
#ifdef USE_OPENCL
    int flags = 0;
    int links = 0;
    vec_t pos;
    unsigned char size = 0;
    
    if (particleWeight[myType].marked)
        flags |= CL_CHATTR_MASS;
        
    if (particleAttractionForce[myType].marked)
        flags |= CL_CHATTR_CHARGE;
    
    if (particleForceInv[myType].marked)
        flags |= CL_CHATTR_INVFRC;
    
    if (gpuCollission.marked)
        flags |= CL_CHATTR_COLLIDE;
    
    if (clSticky.marked)
    {
        flags |= CL_CHATTR_STICKY;
        flags |= GETVAL32(clSticky) ? SO_CHATTR_STVAL : 0;
    }

    /* XXX distinct between "enable/disable linking" and 
     * "change amount of links".
     */
    if (clDoLink.marked)
    {
        if (GETVAL32(clDoLink) == 0)
        {
            flags |= CL_CHATTR_UNLINK;
        }
        else
        {
            flags |= CL_CHATTR_LINK;
            links = GETVAL32(clNumLinks);
        }
    }
    
    if (clNumLinks.marked)
    {
        flags |= CL_CHATTR_CHLINKS;
        links = GETVAL32(clNumLinks);
    }

    if (particleSize[myType].marked)
    {
        flags |= CL_CHATTR_SIZE;
        size = GETVAL32(particleSize[myType]);
    }

    
    if (flags)
    {
        /* transfer the markers from the CPU screeninfo array to the GPU markers
         */
        clTransferSiMarker ();

        /* change attributes of particles on marked pixels
         */
        pos.x = ix;
        pos.y = iy;
        
        flags |= GETVAL32(particleForceInv[myType]) ? CL_CHATTR_INVVAL : 0;
        flags |= GETVAL32(gpuCollission) ? CL_CHATTR_COLLVAL : 0;
        flags |= CL_CHATTR_ONCE;
        
        clSchedChattrMarked (0, 
                        GETVAL32(particleAttractionForce[myType]),
                        GETVAL32(particleWeight[myType]),
                        links,
                        &pos,
                        size,
                        flags
                        );
    }
#endif
}

void clApplyParticleSetting (setting *s, SDL_Rect *r)
{
#ifdef USE_OPENCL
    int flags = 0;
    int links = 0;
    unsigned char size = 0;

    if (s == &particleWeight[myType])
    {
        flags |= CL_CHATTR_MASS;
    }
    else if (s == &particleAttractionForce[myType])
    {
        flags |= CL_CHATTR_CHARGE;
    }
    else if (s == &particleForceInv[myType])
    {
        flags |= CL_CHATTR_INVFRC;
    }
    else if (s == &gpuCollission)
    {
        flags |= CL_CHATTR_COLLIDE;
    }
    else if (s == &clSticky)
    {
        flags |= CL_CHATTR_STICKY;
        flags |= GETVAL32(clSticky) ? SO_CHATTR_STVAL : 0;
    }
    else if (s == &clDoLink)
    {
        if (GETVAL32(clDoLink) == 0)
        {
            flags |= CL_CHATTR_UNLINK;
        }
        else
        {
            flags |= CL_CHATTR_LINK;
            links = GETVAL32(clNumLinks);
        }   
    }
    else if (s == &clNumLinks)
    {
        flags |= CL_CHATTR_CHLINKS;
        links = GETVAL32(clNumLinks);
    }
    else if (s == &clNumLinks)
    {
        flags |= CL_CHATTR_SIZE;
        size = GETVAL32(particleSize[myType]);
    }
    else
    {
        return;
    }

    /* transfer the markers from the CPU screeninfo array to the GPU markers
     */
    clTransferSiMarker ();

    flags |= GETVAL32(particleForceInv[myType]) ? CL_CHATTR_INVVAL : 0;
    flags |= GETVAL32(gpuCollission) ? CL_CHATTR_COLLVAL : 0;
    flags |= CL_CHATTR_ONCE;

    clSchedChattrAll (0,
                 GETVAL32(particleAttractionForce[myType]),
                 GETVAL32(particleWeight[myType]),
                 links,
                 r,
                 size,
                 flags                        
                 );
#endif
}


static void wimpApplySetting (particle *p, setting *s)
{
}

void clParticleColorGet (int charge, unsigned char *col4)
{
    uint32_t        col = 0xffffffff;

    if (col4 == NULL)
        return;
    
    if ((charge >= -1) && (charge <= 1))
    {
        col = pdescs[myType]->crangeColors[charge+1]->current.val.cv.argb;
        col = ARGB(col);
    }
    
    col4[3] = col >> 24;
    col4[2] = col >> 16;
    col4[1] = col >> 8;
    col4[0] = col >> 0;
}

static int placementHint(void)
{
    int rx, ry;

    currentStencilRadius (&rx, &ry);

//    if ((rx <= 1) || (ry <= 1) || (currentDrawMode() == DR_LINE) || (currentDrawMode() == DR_LINE) || (currentDrawMode() == DR_CIRCLE_FILL))
        return PHINT_RANDOM;

    return PHINT_FIXED1;
}

static int clCanCreate (int ix, int iy)
{
#ifdef USE_OPENCL
    return clCanCreateNew (ix, iy);
#else
    return 0;
#endif
}

static void clStopDraw(int stage)
{

#ifdef USE_OPENCL

    clCommitJobs();

    if ((stage == 1) && GETVAL32(clMakeBlob))
    {
        clSchedMkblob();

//        modifySetting (&clMakeBlob, 0, 0);
    }
    clSchedClearNewFlags();
    clCommitJobs();
#endif
}

int oclPartType()
{
    return myType;
}

extern void clSchedFill(void);

pdesc MOD = {
    .init = (FUNCPTR)clParticleInit,
    .newParticle = clNewParticle,
    .stopDraw = (FUNCPTR)clStopDraw,
    .name = "GPU Particles",
    .applySetting = (FUNCPTR)wimpApplySetting,
    .placementHint = (FUNCPTR)placementHint,
    .canCreate = (FUNCPTR)clCanCreate,
    .descr = "",
    .pm = wimp_pm,
    .help = "GPUWIMP",
    .isWimp = 1,
    .isGPUParticle = 1,
    .noSize = 0,
    .mpObject = 1,
    .drawInterval = (FUNCPTR)clDrawInterval,
#ifndef USE_OPENCL
    .skip = 1,
#endif
    .widgets = {
        {(FUNCPTR)slider,        32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)onOffGroupS,   32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS,   32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,   32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS,  32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
        {(FUNCPTR)slider,        34, 67, psize_pm, "Particle Size", 0, &particleSize[0], B_NEXTRC },
        {(FUNCPTR)onOffButtonS,  32, 32, average_xpm, "Density averaging mode", 0, &clAvgMode, B_NEXTRC },
        {(FUNCPTR)onOffButtonS,  32, 32, connect_wall_xpm, "Connect to wall", 0, &clSticky, B_NEXT },
        {(FUNCPTR)slider,        32, 32, trails_xpm, "Trail Length", 0, &clParticleTrailLen, B_NEXTRC },
#if 0
        {(FUNCPTR)onOffButtonS, 32, 32, oscillate_xpm, "Oscillating particle", 0, &clOscillator, B_NEXTRC },
#endif
        {(FUNCPTR)onOffButtonS,  32, 32, clcollide_xpm, "Particle Collission", 0, &gpuCollission, B_NEXTRC|B_SPC(3) },
        {(FUNCPTR)onOffButtonS,  32, 32, clblob_pm, "Make Blob (incomplete)", 0, &clMakeBlob, B_NEXT },
        {(FUNCPTR)slider,        16, 67, damp_xpm, "Damping", 0, &gpuCollDamping, B_NEXTRC },
        {(FUNCPTR)slider,        16, 67, shear_xpm, "Shear", 0, &gpuCollShear, B_NEXTRC },
        {(FUNCPTR)slider,        16, 67, spring_xpm, "Spring", 0, &gpuCollSpring, B_NEXTRC },
        {(FUNCPTR)slider,        16, 67, attract_xpm, "Attraction", 0, &gpuCollAttraction, B_NEXTRC },
//        {(FUNCPTR)slider,        16, 67, size_xpm, "Collission Distance", 0, &gpuCollDist, B_NEXTRC },
        {(FUNCPTR)slider,        16, 67, speedlimit_xpm, "Speed Limit", 0, &gpuSpeedLimit, B_NEXTRC },
        {(FUNCPTR)slider,        32, 67, lifetime_pm, "Time Factor", 0, &gpuTimeFac, B_NEXTRC|B_SPC(3) },
        {(FUNCPTR)slider,        32, 67, einstein_xpm, "Einstein Factor", 0, &einsteinFactor, B_NEXTRC|B_SPC(3) },
//        {(FUNCPTR)slider,        16, 67, NULL, "Link Check Interval", 0, &clLinkInterval, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 32, chain_activate_pm, "Link on collission", 0, &clDoLink, B_NEXTRC|B_SPC(2) },
        {(FUNCPTR)onOffGroupS,  32, 10, NULL, "Link only colliding particles", 0, &clLinkMode, B_NEXT },
        {(FUNCPTR)onOffGroupS,  32, 10, NULL, "Link colliding with non-colliding particles", 1, &clLinkMode, B_NEXT|B_SPC(0) },
        {(FUNCPTR)onOffGroupS,  32, 10, NULL, "Link colliding with colliding or non-colliding particles", 2, &clLinkMode, B_NEXT|B_SPC(0) },
        {(FUNCPTR)slider,        16, 67, chain_mdist_pm, "Max. link length", 0, &clMaxLinkLength, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)slider,        16, 67, chain_llength_pm, "Link length", 0, &clLinkLength, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)slider,        16, 67, chain_lstrength_pm, "Link strength", 0, &clLinkStrength, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,        22, 21, l1_xpm, "1 Link", 1, &clNumLinks, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,        22, 21, l3_xpm, "3 Links", 3, &clNumLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,        22, 21, l5_xpm, "5 Links", 5, &clNumLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,        22, 21, l2_xpm, "2 Links", 2, &clNumLinks, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,        22, 21, l4_xpm, "4 Links", 4, &clNumLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS,        22, 21, l6_xpm, "6 Links", 6, &clNumLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)slider,        16, 67, NULL, NULL, 0, &clColExchThreshR, B_NEXTRC },
//        {(FUNCPTR)slider,        16, 67, NULL, NULL, 0, &clColExchThreshG, B_NEXTRC },
//        {(FUNCPTR)slider,        16, 67, NULL, NULL, 0, &clColExchThreshB, B_NEXTRC },
#ifdef USE_OPENCL
        {(FUNCPTR)onOffButtonS,        32, 32, lum_spring_xpm, "LumSpring", 0, &lumSpringFac, B_NEXTRC },
        {(FUNCPTR)onOffButtonS,        32, 32, coll_color_xpm, "Color particle on collission", 0, &clCollissionCol, B_NEXT },
        {(FUNCPTR)pushButton,        16, 32, NULL, "Create all particles (debug)", (intptr_t)&clSchedFill, NULL, B_NEXTRC },
//        {(FUNCPTR)onOffButtonS,        32, 32, NULL, "Link attr. mode (debug)", 0, &clLinkAttractionMode, B_NEXT },
#endif
}
};
