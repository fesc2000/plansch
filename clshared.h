
/* Kernel/buffer boundary check code ..
 */
#undef CL_BOUNDARY_CHECK

/* Whether to keep global_work_size/id as variables or not 
 * Might save some registers
 */
#undef GBL_COORDS_AS_VAR

/* bits in cl wallFlags buffer (uchar) */
#define SI_BRICK        (1<<0)
#define SI_IS_PUMP        (1<<1)
#define SI_PUSHBORDER        (1<<2)
#define SI_LINE                (1<<3)
#define SI_IN_POLY        (1<<4)
#define SI_OUTER_LINE        (1<<5)
#define SI_CL_MARK        (1<<6)
#define SI_MARKER        (1<<7)

/* Additional bits used by CPU engine
 */
#define SI_BOUNDARY        (1<<8)
#define SI_STENCIL        (1<<9)
#define SI_IS_PIPE        (1<<10)
#define SI_VISITED        (1<<11)
#define SI_TRACK_MARKER        (1<<12)
#define SI_PIPE_MARKER        (1<<13)

#define LOCAL_SIZE_LIMIT 512U

#define CL_MAX_LINKS                6
#define CL_MAX_TRAIL_LENGTH        (CL_MAX_LINKS)
#define CL_TRAIL_SIZE   0x100000
