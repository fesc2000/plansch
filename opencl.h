#undef NEW_CREATE

#include <CL/cl_platform.h>
#include <CL/cl.h>
#include "clshared.h"

#include "plansch.clh"

#define MAX_CREATE_JOBS        10000

#define TO_ALL        3
#define TO_GPU        1
#define TO_HOST        2

#define CL_CLEAR_MARKERS    (1<<0)
#define CL_CHATTR_STOP            (1<<1)
#define CL_CHATTR_ONCE            (1<<2)
#define CL_CHATTR_INVVAL    (1<<3)
#define CL_CHATTR_COLLVAL   (1<<4)

/* Opencl SearchOp flags are identical, start at 1<<8 */
#define CL_CHATTR_MASS      SO_CHATTR_MASS        
#define CL_CHATTR_CHARGE    SO_CHATTR_CHARGE
#define CL_CHATTR_COLOR     SO_CHATTR_COLOR
#define CL_CHATTR_ALPHA     SO_CHATTR_ALPHA
#define CL_CHATTR_BASECOL   SO_CHATTR_BASECOL
#define CL_CHATTR_LINK      SO_CHATTR_LINK
#define CL_CHATTR_UNLINK    SO_CHATTR_UNLINK
#define CL_CHATTR_CHLINKS   SO_CHATTR_CHLINKS
#define CL_CHATTR_INVFRC    SO_CHATTR_INVFRC
#define CL_CHATTR_COLLIDE   SO_CHATTR_COLLIDE
#define CL_CHATTR_STICKY    SO_CHATTR_STICKY
#define CL_CHATTR_STVAL     SO_CHATTR_STVAL
#define CL_CHATTR_SIZE      SO_CHATTR_SIZE

#define CL_MARK_ADD         SO_MARK_ADD
#define CL_MARK_CLEAR       SO_MARK_CLEAR
#define CL_FOLLOW           SO_FOLLOW


#define CL_MODE_ELASTIC        0
#define CL_MODE_NONELASTIC     1

#if defined(_MINGW)
extern void *dc;
extern void *ctx;
#else
extern Window sdl_win;
extern Display *sdl_display;
extern GLXContext sdl_gl_context;
#endif

/* types */

typedef struct 
{
    cl_mem         clBuffer;            /* OpenCl buffer ID */
    void          *hostBuffer;          /* host buffer pointer, if available */
    size_t         size;                /* size in bytes */
    char          *name;                /* name */
    cl_event       events[2];           /* event IDs for pending transfer */
    int            pending;             /* if pending transfers (TO_.. flags) */
    GLuint         vbo;                 /* OpenGL VBO, if shared */
    size_t         elements;            /* # of elements */
    size_t         elem_size;           /* single element size */
} clBuffer_t;

typedef struct
{
    unsigned char       what;
    signed char         charge;
    unsigned char       mass;
    unsigned char       links;
    uint32_t            flags;
    unsigned char       size;
    vec_t               pos;
    vec_t               vec;
    uint32_t            color;
    mPartTpl_t          *tpl;
} clJob_t;

/* state structure for a set of GPU particles
 */
typedef struct
{
    /* GPU buffers for position, vector and attributes (ping-pong)
     */
    clBuffer_t           *particlePos[2];
    clBuffer_t           *particleVec[2];
    clBuffer_t           *particleAttr[2];
    clBuffer_t           *particleCol[2];
    clBuffer_t           *particleBaseCol[2];

    /* Trail and link lines
     */
    clBuffer_t           *particleTrailLines;
    clBuffer_t           *particleLinkLines;
    clBuffer_t           *particleLinkLineColors;

    /* Mouse relative positions of grabbed particles
     */
    clBuffer_t           *grabRelPos;

    /* holds GPU connection attributes
     */
    clBuffer_t           *particleConnections;
    clBuffer_t           *tmpParticleConnections;
    
    /* current search operation buffer/host data
     */
    clBuffer_t           *searchOp;
    clSearchOp_t         *search;

    /* ID remap when compressing arrays
     */
    clBuffer_t           *idRemap;

    /* GPU buffers required for collission handling
     */
    clBuffer_t           *particleSortedOff[2];
    clBuffer_t           *particleSortedIdx[2];
    clBuffer_t           *cellStart, *cellEnd;

    /* Blob structures
     */
    clBuffer_t           *blobs;
    clBuffer_t           *blobParticles[2];
    int                  currentBlobBuf;
    int                  numBlobs;
    blob_t               *blobsHost;

    /* Free list
     */
    clBuffer_t           *freelist;
    int                  *freelistHost;
    int                  numFree;
    int                  nextFree;
    int                  maxPid;

    /* particle creation queue
     */
    clBuffer_t           *createJobs;
    particleJob_t        *createJobsHost;
    int                  numCreateJobs;

    /* host buffers
     */
    unsigned int         *particleSortedOffHost;
    unsigned int         *particleSortedIdxHost;
    unsigned int         *cellStartHost;
    unsigned int         *cellEndHost;

    /* maximum nuber of particles
     */
    int                  maxGpuParticles;

    /* current highest index, aligned up to 4K
     */
    int                  particleCountAlgn;

    /* Current highest index, unaligned
     */
    int                  particleCount;

    /* highest index rounded up to next power of two 
    * (required by collission code which does a bitonic sort)
     */
    int                  particleCountP2;

    /* position, vector and attributes are held in ping-pong
     * buffers. These are the current indices (0/1).
     */
    int                  curParticlePos;
    int                  curParticleVec;
    int                  curParticleAttr;
    int                  curParticleCol;
    int                  curBaseCol;

    /* host buffers
     */
    float                *particleStartPos;
    float                *particleStartVec;
    signed char          *particleAttrHost;
    cl_float4            *particleColors;
    cl_uchar4            *baseColors;

    /* whether particle collission is supported for this 
     * particle set
     */
    int                  canCollide;

    size_t               collLocalMem;
    size_t               *collLocalThreadLimit;
    size_t               collLocalThreads[1];

    int                  iteration;

    /* status data reported back by some kernels
     */
    clStatus_t           *clStatus;
    clStatus_t           clStatusOld;
    clBuffer_t           *statusBuffer;


    /* for job processing
     */
#ifndef NEW_CREATE
    int                  pdataRead;
    int                  lastId, startId;
#endif
    int                  dropCreateJobs;

    /* Indicates potential holes in the tables if particles have been deleted
     */
    int                  haveDeleted;
    
    /* Clear trails
     */
    int                  clearTrails;
    
    /* Particles marked "new" are flying around 
     */
    int                  haveNewParticles;

    int                  recalcBlobs;
    int                  following;

} gpuParticleSet_t;


typedef void (*jobDispatch_t) (clJob_t *);

/* externs */
extern int haveOcl2;
extern clParameters_t *clParametersGet(void);
extern void clParametersNotifyChange(void);
extern void clUseLocalmem(int);
extern void clUseHalfField(int);
extern int clParticleLoops;
extern void doClFinish(void);
extern int clInit1(void);
extern int clInit2(int wid, int hei, DENSITY_TYPE **dens, FDTYPE **fixDens, GRAV_TYPE **dx, GRAV_TYPE **dy, unsigned int tex, GL_CONTEXT_T glCtx);
extern void clParticleLinkVboRelease(int set);
extern int clParticleLinkVboGet(int set, GLuint *pVbo, GLuint *pColorVbo);
extern void clSetCollissionMode (int mode);
extern int clCanCreateNew(int ix, int iy);
extern void clJobsInit(void);
void clBufferToHost (clBuffer_t *b, int async);
void clBufferToHostPart (clBuffer_t *b, int async, size_t offset, size_t count);
void clBufferToGPUPart (clBuffer_t *b, int async, size_t offset, size_t count);
void clBufferToGPU (clBuffer_t *b, int async);
int clCompressTables(gpuParticleSet_t *pSet, int force);
void clClearGrabFlags (gpuParticleSet_t*);
void clClearPixelMarkers (void);
void clParticleColorGet (int charge, unsigned char *col3);
void particleCollide (gpuParticleSet_t *pSet);
void clRestoreColors(gpuParticleSet_t *pSet, float rate);
void clearParticleBuffers (gpuParticleSet_t *pSet);
void clearParticleTrails (gpuParticleSet_t *pSet);
void setNGpuParticles (gpuParticleSet_t *pSet, int max);
void clearNewMarkers (void);
size_t kernelMaxWg (cl_kernel *k);
cl_uint kernelBufferArg (cl_kernel k, int arg, clBuffer_t *b);
cl_uint kernelIntArg (cl_kernel k, int arg, int *i);
void runParticleToDens (gpuParticleSet_t *pSet);
void clMakeLinkLines (gpuParticleSet_t *pSet);
clBuffer_t *makeBufferOgl (cl_mem_flags f, void *hostData, size_t elem_size, size_t elements, char *name);
clBuffer_t *makeBuffer (int hostMem, cl_mem_flags f, void *hostData, size_t elem_size, size_t elements, char *name);
void runFieldToTex();
void iterateField(void);
void iterateTempr(void);
void runCalcDiff (int toHost);
int gpuParticlesInit(int wh);
void *aAlloc (int size);
void clearStatusBuffer (gpuParticleSet_t *pSet);
void bitonicSort(
    cl_command_queue cqCommandQueue,
    cl_mem d_DstKey,
    cl_mem d_DstVal,
    cl_mem d_SrcKey,
    cl_mem d_SrcVal,
    unsigned int batch,
    unsigned int arrayLength,
    unsigned int dir);
void getClStatistics (int show);

void *clParticlePosGet (int set);
void *clParticleColGet (int set);
int clParticleLinkLinesGet(int set, void **lines, void **colors);

void clMakeBlobFromNew ( gpuParticleSet_t *pSet);

int clUpdateFreelist (gpuParticleSet_t *pSet);
int clGetFreeParticle (gpuParticleSet_t *pSet);
void clCreateParticles (gpuParticleSet_t *pSet);
particleJob_t *clGetCreateSlot (gpuParticleSet_t *pSet);

int oclPartType(void);
void clRecordScreenSection (tape *Tape, SDL_Rect *pRect);

extern FT_TYPE                         *gpuScrFlagsHost;
extern clBuffer_t                 *gpuScrFlags;
extern FT_TYPE                         *cpuScrFlags;
extern int                        cpuScrFlagsDirty;
extern clBuffer_t                 *accelBuffer;

extern clBuffer_t               *clWallTempr[];

extern uint64_t gpuTransToGpu;
extern uint64_t gpuTransToGpuCnt;
extern uint64_t gpuTransToGpuLat;
extern uint64_t gpuTransToHost;

extern cl_command_queue         commandQueue;
extern cl_command_queue         deviceQueue;
extern gpuParticleSet_t particleSet[];
extern int                movingPartSet;
extern int                oscPartSet;

extern int clJobTypes;
extern clSearchOp_t     currentChattrOp;
extern int              particleMarkOp;
extern int clDoGrab;
extern cl_float2 clPointerPos;
extern cl_float2 clGrabCenter;

extern cl_int4 *jobBufferHost;
extern int jobBufferEntries;

extern clBuffer_t                 *jobBuffer;
extern int                totalGpuParticles;
extern wall_t        *clWallsHost;
extern int         clWallUpdateFlag;
extern int useLocalMem;
extern int useHalfField;
extern size_t        ftypeLen;
extern int localSizeDim2;
extern cl_ulong localMemSize;
extern float colorRestore;
extern int scrFlagFieldElements;
extern clBuffer_t *clParamBuffer;

/* field.c */
extern int                          curField;
extern clBuffer_t                 *fieldBuffer[];
extern clBuffer_t                  *densBuffer;
extern clBuffer_t                  *fixDensBuffer;
extern int                          fixDensBufferUpdate;
extern clBuffer_t                 *diffBuffer;
extern clBuffer_t                 *diffBufferHost;
extern clBuffer_t                  gravBufferShared;
extern clBuffer_t                 *gravBuffer;
extern clBuffer_t                 *particleDens;
extern int                          curTempr;
extern size_t                         maxWorkGroupSize;
extern int                         densityUpdate;

extern clBuffer_t                 *clWalls;
extern clBuffer_t                 *clWallFlags;
extern int                             gpuLoops;
extern int                             tryPinned;
extern clParameters_t *clParameters;

/* defines */
#define CURRENT_POS(pSet)        (pSet)->particlePos[(pSet)->curParticlePos]
#define CURRENT_VEC(pSet)        (pSet)->particleVec[(pSet)->curParticleVec]
#define CURRENT_ATTR(pSet)        (pSet)->particleAttr[(pSet)->curParticleAttr]
#define CURRENT_COL(pSet)        (pSet)->particleCol[(pSet)->curParticleCol]
#define CURRENT_BASE_COL(pSet)        (pSet)->particleBaseCol[(pSet)->curBaseCol]
#define CURRENT_BLOBP(p)        (p)->blobParticles[pSet->currentBlobBuf]

#if 1
#define ALT_POS(pSet)                (pSet)->particlePos[1-(pSet)->curParticlePos]
#define ALT_VEC(pSet)                (pSet)->particleVec[1-(pSet)->curParticleVec]
#define ALT_ATTR(pSet)                (pSet)->particleAttr[1-(pSet)->curParticleAttr]
#define ALT_COL(pSet)                (pSet)->particleCol[1-(pSet)->curParticleCol]
#define ALT_BASE_COL(pSet)        (pSet)->particleBaseCol[1-(pSet)->curBaseCol]
#define SWITCH_POS(pSet)        (pSet)->curParticlePos = 1-(pSet)->curParticlePos
#define SWITCH_VEC(pSet)        (pSet)->curParticleVec = 1-(pSet)->curParticleVec
#define SWITCH_ATTR(pSet)        (pSet)->curParticleAttr = 1-(pSet)->curParticleAttr
#define SWITCH_COL(pSet)        (pSet)->curParticleCol = 1-(pSet)->curParticleCol
#define SWITCH_BASE_COL(pSet)        (pSet)->curBaseCol = 1-(pSet)->curBaseCol
#define ALT_BLOBP(p)                (p)->blobParticles[1-pSet->currentBlobBuf]
#define SWITCH_BLOBP(p)          (p)->currentBlobBuf = 1 - (p)->currentBlobBuf
#else
#define ALT_POS(pSet)                (pSet)->particlePos[(pSet)->curParticlePos]
#define ALT_VEC(pSet)                (pSet)->particleVec[(pSet)->curParticleVec]
#define ALT_ATTR(pSet)                (pSet)->particleAttr[(pSet)->curParticleAttr]
#define ALT_COL(pSet)                (pSet)->particleCol[(pSet)->curParticleCol]
#define SWITCH_POS(pSet)        
#define SWITCH_VEC(pSet)        
#define SWITCH_ATTR(pSet)        
#define SWITCH_COL(pSet)        
#endif

/* Make all the paranoia clFinish calls nops by default
 */
#if 0
#define CL_FINISH {clFinish (commandQueue); glFinish();}
#else
#define CL_FINISH
#endif

#define FILL_BUFFER(buf, data)                            \
    {clEnqueueFillBuffer (commandQueue,                    \
                         (buf)->clBuffer,            \
                         (void*)&data,                     \
                         sizeof(data),                    \
                         0,                            \
                         (buf)->size,                    \
                         0, NULL, NULL); CL_FINISH;}

#define FILL_BUFFER_PART(buf, data, off, cnt)            \
    {clEnqueueFillBuffer (commandQueue,                    \
                         (buf)->clBuffer,            \
                         (void*)&data,                     \
                         sizeof(data),                    \
                         off*sizeof(data),            \
                         cnt*sizeof(data),            \
                         0, NULL, NULL); CL_FINISH;}

#define INIT_BUFFER(buf, data)                            \
    {clEnqueueFillBuffer (commandQueue,                    \
                         (buf)->clBuffer,            \
                         (void*)data,                     \
                         (buf)->size,                    \
                         0,                            \
                         (buf)->size,                    \
                         0, NULL, NULL); CL_FINISH;}

#define COPY_BUFFER(dst, src)                            \
    {clEnqueueCopyBuffer (commandQueue,                    \
                         (src)->clBuffer,            \
                         (dst)->clBuffer,            \
                         0,                            \
                         0,                            \
                         (dst)->size,                    \
                         0, NULL, NULL); CL_FINISH;}

#define PENDING_JOB(job)        (clJobTypes & (1<<(job)))
#define JOB_ACTIVATE(job)        clJobTypes |= (1<<(job))
#define PENDING_JOB_DONE(job)        clJobTypes &= ~(1<<(job))

/* max. number of entries in the job buffer
 */
#define JOB_FIELD_SIZE                (16*1024)

#define CL_FLAG_MASK        (SI_BRICK | SI_LINE | SI_IN_POLY | SI_IS_PUMP)

#define CUR_TEMPR_BUF                  clWallTempr[curTempr]
#define ALT_TEMPR_BUF                  clWallTempr[1-curTempr]
#define SWITCH_TEMPR_BUF          curTempr = 1-curTempr

#define ROUNDUP_4K(v) (((v) + 0x1fff) & ~0x1fff)
