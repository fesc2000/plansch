/* Copyright (C) 2008-2016 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include <stdio.h>
#include <fcntl.h>
#include <CL/cl.h>
#include <CL/cl_ext.h>
#include <CL/cl_gl.h>

#include "opencl.h"
#include "kernels.h"

#define CONN_SIZE        (sizeof(cl_int8))
#define CONN_INDEX(i)        ((i) * sizeof(cl_int8))

static cl_kernel    doCollide;
static float            zeroVec[2] = { 0.f, 0.f };
static int            empty = 0;
static int            maxkey = MAXKEY;

int                  clMaxParticlesDyn = 1;
cl_kernel            kParticleVecUpdate;
cl_kernel            kParticleToDens;

void runBlobs ( gpuParticleSet_t *pSet);

static void getNewParticleFlags (gpuParticleSet_t *pSet)
{
    cl_int status = 0;
    int arg;
    size_t globalThreads[1];

    FILL_BUFFER (gpuScrFlags, empty);

    arg = 0;
    globalThreads[0] = pSet->particleCountAlgn;
    kernelBufferArg (getNewFlags, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (getNewFlags, arg++, CURRENT_POS(pSet));
    kernelBufferArg (getNewFlags, arg++, gpuScrFlags);
    kernelBufferArg (getNewFlags, arg++, clParamBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 getNewFlags,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "getNewFlags");
        return;
    }

    clBufferToHost (gpuScrFlags, 0);
}

/* Report highest known valid GPU particle index.
 * Sets the aligned number (particleCountAlgn, for kernel execution) 
 * and the next power of two (particleCountP2, for bitonic sort).
 */
void setNGpuParticles (gpuParticleSet_t *pSet, int max)
{
    if (max > pSet->maxGpuParticles)
    {
        printf ("Attempt to set maxGpuParticles=%d, limit=%d\n", 
                    max, pSet->maxGpuParticles);
        max = pSet->maxGpuParticles;
    }

    pSet->particleCount = max;
    pSet->particleCountAlgn        = ROUNDUP_4K(max);
    pSet->particleCountP2        = pSet->maxGpuParticles;

    if (clMaxParticlesDyn)
    {
        if (pSet->canCollide)
        {
            int i;

            /* the next higher 2^n number
             */
            for (i = pSet->maxGpuParticles; i != 0; i = i >> 1)
            {
                if (i & pSet->particleCountAlgn)
                {
                    if (pSet->particleCountAlgn > i)
                        pSet->particleCountP2 = i << 1;
                    else
                        pSet->particleCountP2 = i;

                    break;
                }
            }
        }
    }
}

void checkParticleConn (void)
{
    int i;

    clBufferToHost (particleSet[0].particleConnections, 0);

    for (i = 0; i < particleSet[0].particleConnections->size; i+= 4)
    {
        if (*(int*)(particleSet[0].particleConnections->hostBuffer + i) != 0)
        {
            printf ("connBuffer[%x] = %d = %f\n",
                i,
                *(int*)(particleSet[0].particleConnections->hostBuffer + i),
                *(float*)(particleSet[0].particleConnections->hostBuffer + i));
            break;
        }
    }
}

GLuint clParticleVecVboGet(int set)
{
    clFinish (commandQueue);
    clEnqueueReleaseGLObjects (commandQueue, 1, &particleSet[set].particleTrailLines->clBuffer, 0, NULL, NULL);

    return particleSet[set].particleTrailLines->vbo;
}

int clParticleLinkVboGet(int set, GLuint *pVbo, GLuint *pColorVbo)
{
    if (particleSet[set].clStatus->linkLineCount == 0)
        return 0;

    clFinish(commandQueue);
    clEnqueueReleaseGLObjects (commandQueue, 1, 
        &particleSet[set].particleLinkLineColors->clBuffer, 0, NULL, NULL);

    clEnqueueReleaseGLObjects (commandQueue, 1, 
        &particleSet[set].particleLinkLines->clBuffer, 0, NULL, NULL);

    *pVbo = particleSet[set].particleLinkLines->vbo;
    *pColorVbo = particleSet[set].particleLinkLineColors->vbo;


    return particleSet[set].clStatus->linkLineCount;
}

int clParticleVboGet(int set, GLuint *pVbo, GLuint *pColorVbo)
{
    clFinish(commandQueue);
    clEnqueueReleaseGLObjects (commandQueue, 1, 
        &CURRENT_COL(&particleSet[set])->clBuffer, 0, NULL, NULL);

    clEnqueueReleaseGLObjects (commandQueue, 1, 
        &CURRENT_POS(&particleSet[set])->clBuffer, 0, NULL, NULL);

    *pVbo = CURRENT_POS(&particleSet[set])->vbo;
    *pColorVbo = CURRENT_COL(&particleSet[set])->vbo;

    return 1;
}

void *clParticlePosGet (int set)
{
    clBufferToHost (CURRENT_POS(&particleSet[set]), 0);

    return (void*)CURRENT_POS(&particleSet[set])->hostBuffer;
}

void *clParticleColGet (int set)
{
    clBufferToHost (CURRENT_COL(&particleSet[set]), 0);

    return (void*)CURRENT_COL(&particleSet[set])->hostBuffer;
}

void clearParticleTrails (gpuParticleSet_t *pSet)
{
    static int zero = 0;
    FILL_BUFFER(pSet->particleTrailLines, zero);
}

int clParticleLinkLinesGet(int set, void **lines, void **colors)
{
    gpuParticleSet_t *pSet = &particleSet[set];
        
    if (pSet->clStatus->linkLineCount == 0)
        return 0;

    clBufferToHostPart (pSet->particleLinkLineColors, 0, 0, pSet->clStatus->linkLineCount*2);
    clBufferToHostPart (pSet->particleLinkLines, 0, 0, pSet->clStatus->linkLineCount*2);

    *lines  = pSet->particleLinkLines->hostBuffer;
    *colors = pSet->particleLinkLineColors->hostBuffer;

    return pSet->clStatus->linkLineCount;
}

void clearParticleBuffers (gpuParticleSet_t *pSet)
{
    int i;

    FILL_BUFFER(CURRENT_ATTR(pSet), empty);
    FILL_BUFFER(CURRENT_VEC(pSet), zeroVec);
    FILL_BUFFER(CURRENT_POS(pSet), zeroVec);
    FILL_BUFFER(ALT_ATTR(pSet), empty);
    FILL_BUFFER(ALT_VEC(pSet), zeroVec);
    FILL_BUFFER(ALT_POS(pSet), zeroVec);
    FILL_BUFFER(pSet->grabRelPos, zeroVec);

    FILL_BUFFER(pSet->particleConnections, empty);
    FILL_BUFFER(pSet->particleTrailLines, empty);

    FILL_BUFFER(pSet->particleSortedOff[0], maxkey);
    FILL_BUFFER(pSet->particleSortedOff[1], maxkey);
    FILL_BUFFER(pSet->particleSortedIdx[0], empty);
    FILL_BUFFER(pSet->particleSortedIdx[1], empty);
    FILL_BUFFER(pSet->blobs, empty);
    FILL_BUFFER(pSet->blobParticles[0], empty);
    FILL_BUFFER(pSet->blobParticles[1], empty);

    for (i = 0; i < pSet->maxGpuParticles-1; i++)
        pSet->freelistHost[i] = i+1;

    pSet->numFree = pSet->maxGpuParticles-1;
    pSet->nextFree = 0;

    CL_FINISH

    pSet->particleCountAlgn = pSet->particleCountP2 = pSet->particleCount = 0;
}

int gpuParticleSetInit(int set, int wh)
{
    unsigned int j;
    int maxParticles;
    gpuParticleSet_t *pSet = &particleSet[set];
    int canCollide = 0;

    memset ((void*)pSet, 0, sizeof(*pSet));

    if (set == 0)
        canCollide = 1;

    pSet->canCollide = canCollide;

    if ((options->maxGpuParticles[set] & (options->maxGpuParticles[set]-1)) != 0)
    {
        printf ("max. GPU wimps must be a power of two\n");
        for (j = 0x80000000; (j > 0) && ((j & options->maxGpuParticles[set]) == 0); j = j >> 1)
            ;
        
        printf ("  rounded down to %d\n", j);
        options->maxGpuParticles[set] = j;
    }

    if (options->maxGpuParticles[set] == 0)
        return 1;

    maxParticles = pSet->maxGpuParticles = options->maxGpuParticles[set];

    pSet->particleCountAlgn = pSet->particleCountP2 = pSet->particleCount =  0;

    pSet->particleStartPos = (float*)aAlloc (sizeof(cl_float2) * maxParticles);
    if (!pSet->particleStartPos)
        return 0;

    pSet->particleStartVec = (float*)aAlloc (sizeof(cl_float2) * maxParticles);
    if (!pSet->particleStartVec)
        return 0;
        
    pSet->particleAttrHost = (signed char*)aAlloc (sizeof(cl_uint2) * maxParticles);
    if (!pSet->particleAttrHost)
        return 0;
        
    pSet->particleColors = (cl_float4*)aAlloc (sizeof(cl_float4) * maxParticles);
    if (!pSet->particleColors)
        return 0;
        
    pSet->baseColors = (cl_uchar4*)aAlloc (sizeof(cl_uchar4) * maxParticles);
    if (!pSet->baseColors)
        return 0;
        
    if (pSet->canCollide)
    {
        pSet->particleSortedOffHost = (unsigned int *)aAlloc (sizeof(cl_uint) * maxParticles);
        if (!pSet->particleSortedOffHost)
            return 0;
        
        pSet->particleSortedIdxHost = (unsigned int *)aAlloc (sizeof(cl_uint) * maxParticles);
        if (!pSet->particleSortedIdxHost)
            return 0;
        
        pSet->cellStartHost = (unsigned int *)aAlloc (sizeof(cl_uint) * wh);
        if (!pSet->cellStartHost)
            return 0;
        
        pSet->cellEndHost = (unsigned int *)aAlloc (sizeof(cl_uint) * wh);
        if (!pSet->cellEndHost)
            return 0;
    }

    size_t vs = sizeof(cl_int) * maxParticles + CL_MAX_TRAIL_LENGTH * sizeof(cl_float2) * 2 * maxParticles;

    if (options->clGlSharing)
    {
        pSet->particlePos[0] = makeBufferOgl (CL_MEM_READ_WRITE,  pSet->particleStartPos, sizeof(cl_float2), maxParticles, "particlePos0");
        if (!pSet->particlePos[0])
            return 0;

        pSet->particlePos[1] = makeBufferOgl (CL_MEM_READ_WRITE,  pSet->particleStartPos, sizeof(cl_float2), maxParticles, "particlePos1");
        if (!pSet->particlePos[1])
            return 0;

        pSet->particleCol[0] = makeBufferOgl (CL_MEM_READ_WRITE,  pSet->particleColors, sizeof(cl_float4), maxParticles, "particleCol0");
        if (!pSet->particleCol[0])
            return 0;

        pSet->particleCol[1] = makeBufferOgl (CL_MEM_READ_WRITE,  pSet->particleColors, sizeof(cl_float4), maxParticles, "particleCol1");
        if (!pSet->particleCol[1])
            return 0;


        pSet->particleTrailLines = makeBufferOgl (CL_MEM_READ_WRITE,  NULL, 1, vs, "particleTrailLines");
        if (!pSet->particleTrailLines)
            return 0;

        pSet->particleLinkLines = makeBufferOgl (CL_MEM_READ_WRITE,  NULL, sizeof(cl_float2), 2 * maxParticles * 6, "particleLinkLines");
        if (!pSet->particleLinkLines)
            return 0;

        pSet->particleLinkLineColors = makeBufferOgl (CL_MEM_READ_WRITE,  NULL, sizeof(cl_char4), 2 * maxParticles * 6, "particleLinkLineColors");
        if (!pSet->particleLinkLineColors)
            return 0;
    }
    else
    {
        pSet->particlePos[0] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleStartPos, sizeof(cl_float2), maxParticles, "particlePos0");
        if (!pSet->particlePos[0])
            return 0;

        pSet->particlePos[1] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleStartPos, sizeof(cl_float2), maxParticles, "particlePos1");
        if (!pSet->particlePos[1])
            return 0;

        pSet->particleCol[0] = makeBuffer (1, CL_MEM_READ_WRITE,  pSet->particleColors, sizeof(cl_float4), maxParticles, "particleCol0");
        if (!pSet->particleCol[0])
            return 0;

        pSet->particleCol[1] = makeBuffer (1, CL_MEM_READ_WRITE,  pSet->particleColors, sizeof(cl_float4), maxParticles, "particleCol1");
        if (!pSet->particleCol[1])
            return 0;


        pSet->particleTrailLines = makeBuffer (1, CL_MEM_READ_WRITE,  NULL, 1, vs, "particleTrailLines");
        if (!pSet->particleTrailLines)
            return 0;

        pSet->particleLinkLines = makeBuffer (1, CL_MEM_READ_WRITE,  NULL, sizeof(cl_float2), 2 * maxParticles * 6, "particleLinkLines");
        if (!pSet->particleLinkLines)
            return 0;

        pSet->particleLinkLineColors = makeBuffer (1, CL_MEM_READ_WRITE,  NULL, sizeof(cl_float4), 2 * maxParticles * 6, "particleLinkLineColors");
        if (!pSet->particleLinkLineColors)
            return 0;
    }

    pSet->particleBaseCol[0] = makeBuffer (0, CL_MEM_READ_ONLY,  pSet->baseColors, sizeof(cl_uchar4), maxParticles, "baseColors0");
    if (!pSet->particleBaseCol[0])
        return 0;

    pSet->particleBaseCol[1] = makeBuffer (0, CL_MEM_READ_ONLY,  pSet->baseColors, sizeof(cl_uchar4), maxParticles, "baseColors1");
    if (!pSet->particleBaseCol[1])
        return 0;

    pSet->grabRelPos = makeBuffer (0, CL_MEM_READ_WRITE,  NULL, sizeof(cl_float2), maxParticles, "grabRelPos");
    if (!pSet->grabRelPos)
        return 0;

    pSet->particleVec[0] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleStartVec, sizeof(cl_float2), maxParticles, "particleVec0");
    if (!pSet->particleVec[0])
        return 0;

    pSet->particleVec[1] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleStartVec, sizeof(cl_float2), maxParticles, "particleVec1");
    if (!pSet->particleVec[1])
        return 0;

    pSet->particleAttr[0] = makeBuffer (0, CL_MEM_READ_WRITE, pSet->particleAttrHost, sizeof(cl_uint2), maxParticles, "particleAttr0");
    if (!pSet->particleAttr[0])
        return 0;

    pSet->particleAttr[1] = makeBuffer (0, CL_MEM_READ_WRITE, pSet->particleAttrHost, sizeof(cl_uint2), maxParticles, "particleAttr1");
    if (!pSet->particleAttr[1])
        return 0;


    pSet->particleConnections = makeBuffer (1, CL_MEM_READ_WRITE,  NULL, CONN_SIZE, maxParticles, "particleConnections");
    if (!pSet->particleConnections)
        return 0;

    pSet->tmpParticleConnections = makeBuffer (1, CL_MEM_READ_WRITE,  NULL, CONN_SIZE, maxParticles, "tmpParticleConnections");
    if (!pSet->tmpParticleConnections)
        return 0;


    pSet->idRemap = makeBuffer (1, CL_MEM_READ_WRITE,  NULL, sizeof(int), maxParticles, "idRemap");
    if (!pSet->idRemap)
        return 0;

    pSet->blobsHost = (blob_t*)aAlloc(sizeof(blob_t) * MAX_CL_BLOBS);
    if (!pSet->blobsHost)
        return 0;

    pSet->blobs = makeBuffer (1, CL_MEM_READ_WRITE, pSet->blobsHost, sizeof(blob_t), MAX_CL_BLOBS, "blobs");
    if (!pSet->blobs)
        return 0;

    pSet->blobParticles[0] = makeBuffer (1, CL_MEM_READ_WRITE, NULL, sizeof(blobPart_t), maxParticles, "blobParticles0");
    pSet->blobParticles[1] = makeBuffer (1, CL_MEM_READ_WRITE, NULL, sizeof(blobPart_t), maxParticles, "blobParticles1");
    if (!pSet->blobParticles[0] || !pSet->blobParticles[1])
        return 0;
    pSet->currentBlobBuf = 0;


    pSet->freelist = makeBuffer (1, CL_MEM_READ_WRITE, NULL, sizeof(int), maxParticles, "freelist");
    if (!pSet->freelist)
        return 0;
    pSet->numFree = 0;
    pSet->freelistHost = (int*)pSet->freelist->hostBuffer;
    if (!pSet->freelistHost)
        return 0;

    pSet->createJobsHost = (particleJob_t*)aAlloc(sizeof(particleJob_t) * MAX_CREATE_JOBS);
    if (!pSet->createJobsHost)
        return 0;

    pSet->createJobs = makeBuffer (1, CL_MEM_READ_WRITE, pSet->createJobsHost, sizeof(particleJob_t), MAX_CREATE_JOBS, "createJobs");
    if (!pSet->createJobs)
        return 0;
    pSet->numCreateJobs = 0;

    pSet->clStatus = aAlloc (sizeof(clStatus_t));
    pSet->statusBuffer = makeBuffer (0, CL_MEM_READ_WRITE,
        (void*)pSet->clStatus, sizeof(clStatus_t), 1, "status data");
    if (!pSet->statusBuffer)
        return 0;


    pSet->searchOp = makeBuffer (1, CL_MEM_READ_ONLY, NULL, sizeof(clSearchOp_t), 1, "search");
    if (!pSet->searchOp)
        return 0;

    pSet->search = (clSearchOp_t*)pSet->searchOp->hostBuffer;
    if (!pSet->search)
        return 0;

    if (pSet->canCollide)
    {
        pSet->particleSortedOff[0] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleSortedOffHost, sizeof(cl_uint), maxParticles, "particleSortedOffset0");
        if (!pSet->particleSortedOff[0])
            return 0;

        pSet->particleSortedOff[1] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleSortedOffHost, sizeof(cl_uint), maxParticles, "particleSortedOffset1");
        if (!pSet->particleSortedOff[1])
            return 0;

        pSet->particleSortedIdx[0] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleSortedIdxHost, sizeof(cl_uint), maxParticles, "particleSortedId0");
        if (!pSet->particleSortedIdx[0])
            return 0;

        pSet->particleSortedIdx[1] = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->particleSortedIdxHost, sizeof(cl_uint), maxParticles, "particleSortedId1");
        if (!pSet->particleSortedIdx[1])
            return 0;

        pSet->cellStart = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->cellStartHost, sizeof(cl_uint), wh, "cellStart0");
        if (!pSet->cellStart)
            return 0;

        pSet->cellEnd = makeBuffer (0, CL_MEM_READ_WRITE,  pSet->cellEndHost, sizeof(cl_uint), wh, "cellStart1");
        if (!pSet->cellEnd)
            return 0;


        /* The collission detection needs a int8 for each local ID. Make sure we have 
         * enough local memory on this platform, otherwise limit the local size for 
         * the doCollide() kernel
         */
        if ((sizeof(cl_int8) * maxWorkGroupSize) > localMemSize)
        {
            pSet->collLocalMem = localMemSize;
            pSet->collLocalThreads[0] = localMemSize / sizeof(cl_int8);
            pSet->collLocalThreadLimit = &pSet->collLocalThreads[0];

            printf ("local size limited to %d for collission detection\n", (int)pSet->collLocalThreads[0]);
        }
        else
        {
            pSet->collLocalMem = sizeof(cl_int8) * maxWorkGroupSize;
            pSet->collLocalThreadLimit = NULL;
        }

    }

    clearParticleBuffers (pSet);

    return 1;
}

int gpuParticlesInit(int wh)
{
    int set;
    char cz[1] = {0};
    float fz[1] = {0.0f};

    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        switch (set)
        {
            case 0:
                movingPartSet = set;
                oscPartSet = set;
                break;
            case 1:
                oscPartSet = set;
                break;
        }
        if (!gpuParticleSetInit (set, wh))
            return 0;
    }

    particleDens = makeBuffer (0, CL_MEM_READ_WRITE,  NULL, sizeof(cl_int), wh, "particleDens");
    if (!particleDens)
        return 0;

    clWallsHost = (wall_t *)aAlloc (sizeof(wall_t) * wh);
    if (!clWallsHost)
        return 0;

    clWalls = makeBuffer (0, CL_MEM_READ_ONLY, clWallsHost, sizeof(wall_t), wh, "clWalls");
    if (!clWalls)
        return 0;

    clWallFlags = makeBuffer (0, CL_MEM_READ_WRITE, NULL, sizeof(cl_uchar), wh, "clWallFlags");
    if (!clWallFlags)
        return 0;

    clWallTempr[0] = makeBuffer (0, CL_MEM_READ_WRITE, NULL, sizeof(cl_float), wh, "clWallTempr[0]");
    if (!clWallTempr[0])
        return 0;
    FILL_BUFFER(clWallTempr[0], fz);

    clWallTempr[1] = makeBuffer (0, CL_MEM_READ_WRITE, NULL, sizeof(cl_float), wh, "clWallTempr[1]");
    if (!clWallTempr[1])
        return 0;
    FILL_BUFFER(clWallTempr[1], fz);
        
    FILL_BUFFER(clWallFlags, cz);

    cpuScrFlags = scrFlagFieldAlloc();
    gpuScrFlagsHost = scrFlagFieldAlloc();
    gpuScrFlags = makeBuffer (0, CL_MEM_READ_WRITE, gpuScrFlagsHost, sizeof(cl_int), scrFlagFieldElements, "gpuScrFlags");
    if (!gpuScrFlags || !gpuScrFlagsHost)
        return 0;
        
    void *cp = clParametersGet();
    if (!cp)
        return 0;
        
    clParamBuffer = makeBuffer (0, CL_MEM_READ_ONLY, cp, sizeof(clParameters_t), 1, "parameters");
    if (!clParamBuffer)
        return 0;


    clParameters->collideDist = 1.0f;
    clParameters->wid = wid;
    clParameters->hei = hei;
    clParameters->maxOffset = wh;
    clParameters->flags = 0;
    clParameters->maxParticles = options->maxGpuParticles[0];
    clParametersNotifyChange();

    clSchedClearAllParticles();
    
    return 1;
}


void clParticleConnect (gpuParticleSet_t *pSet, int src, int dst)
{
    int sourceConn[8], dstConn[8];
    int srcCid, dstCid;

    clEnqueueReadBuffer (commandQueue, pSet->particleConnections->clBuffer, 1, CONN_INDEX(src), CONN_SIZE, (void*)sourceConn, 0, NULL, NULL);
    clEnqueueReadBuffer (commandQueue, pSet->particleConnections->clBuffer, 1, CONN_INDEX(dst), CONN_SIZE, (void*)dstConn, 0, NULL, NULL);

    srcCid = sourceConn[0];
    dstCid = dstConn[0];

    if ((srcCid >= 6) || (dstCid >= 6))
        return;

    sourceConn[srcCid+1] = dst;
    dstConn[dstCid+1] = src;
    sourceConn[0]++;
    dstConn[0]++;

    clEnqueueWriteBuffer (commandQueue, pSet->particleConnections->clBuffer, 1, CONN_INDEX(src), CONN_SIZE, (void*)sourceConn, 0, NULL, NULL);
    clEnqueueWriteBuffer (commandQueue, pSet->particleConnections->clBuffer, 1, CONN_INDEX(dst), CONN_SIZE, (void*)dstConn, 0, NULL, NULL);
}


void clParticleVboRelease(int set)
{
    glFinish();
    clEnqueueAcquireGLObjects (commandQueue, 1, &CURRENT_POS(&particleSet[set])->clBuffer, 0, NULL, NULL);
    clEnqueueAcquireGLObjects (commandQueue, 1, &CURRENT_COL(&particleSet[set])->clBuffer, 0, NULL, NULL);
}

void clParticleVecVboRelease(int set)
{
    glFinish();
    clEnqueueAcquireGLObjects (commandQueue, 1, &particleSet[set].particleTrailLines->clBuffer, 0, NULL, NULL);
}

void clParticleLinkVboRelease(int set)
{
    glFinish();
    clEnqueueAcquireGLObjects (commandQueue, 1,         
        &particleSet[set].particleLinkLines->clBuffer, 0, NULL, NULL);
    clEnqueueAcquireGLObjects (commandQueue, 1, 
        &particleSet[set].particleLinkLineColors->clBuffer, 0, NULL, NULL);
}

int clNumParticles(int set)
{
    if (set == -1)
    {
        int rc = 0;
        int i;

        for (i = 0; i < CL_NUM_PARTICLE_SETS; i++)
            rc += clNumParticles (i);
        
        return rc;
    }

    return particleSet[set].particleCount;
}

void runParticleToDens (gpuParticleSet_t *pSet)
{
    cl_int   status;
    size_t globalThreads[1];
    int max = wid * hei;

    globalThreads[0] = pSet->particleCountAlgn;

    kParticleToDens = GETVAL32(clAvgMode) ? particleToDensDist : particleToDens;

    kernelBufferArg (kParticleToDens, 0, particleDens);
    kernelBufferArg (kParticleToDens, 1, CURRENT_POS(pSet));
    kernelBufferArg (kParticleToDens, 2, CURRENT_ATTR(pSet));
    status = clSetKernelArg( kParticleToDens, 3, sizeof(cl_int), (void *)&wid);
    status = clSetKernelArg( kParticleToDens, 4, sizeof(cl_int), (void *)&max);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 kParticleToDens,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);

    CL_FINISH;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "particleToDens");
        return;
    }
}

void clClearPixelMarkers (void)
{
    int cz = 0;
    FILL_BUFFER(clWallFlags, cz);
}

void clClearGrabFlags(gpuParticleSet_t *pSet)
{
    int arg = 0;
    size_t globalThreads[1];
    cl_int   status;

    if (pSet == NULL)
    {
        for (arg = 0; arg < CL_NUM_PARTICLE_SETS; arg++)
            clClearGrabFlags (&particleSet[arg]);
        return;
    }

    clDoGrab = 0;

    globalThreads[0] = pSet->particleCountAlgn;
    kernelBufferArg (stopGrab, arg++, CURRENT_ATTR(pSet));
    status = clEnqueueNDRangeKernel( commandQueue, stopGrab, 1, NULL, globalThreads, NULL, 0, NULL, NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "stopGrab");
        return;
    }
    
    CL_FINISH;
}

/* Search particles on marked pixels (+/- an offet from the pixel's center)
 * and apply the current search operation (delete, paint, grab, ...)
 */
void clSearchParticles (gpuParticleSet_t *pSet)
{
    int arg = 0;
    size_t globalThreads[1];
    cl_int   status;
    SDL_Rect r;
    getSelection (&r);

    pSet->search->rect.s[0] = r.x;
    pSet->search->rect.s[1] = r.y;
    pSet->search->rect.s[2] = r.w;
    pSet->search->rect.s[3] = r.h;

    clBufferToGPU (pSet->searchOp, 1);

    globalThreads[0] = pSet->particleCountAlgn;
    kernelBufferArg (searchParticles, arg++, CURRENT_POS(pSet));
    kernelBufferArg (searchParticles, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (searchParticles, arg++, pSet->grabRelPos);
    kernelBufferArg (searchParticles, arg++, clWallFlags);
    kernelBufferArg (searchParticles, arg++, CURRENT_COL(pSet));
    kernelBufferArg (searchParticles, arg++, CURRENT_BASE_COL(pSet));
    kernelBufferArg (searchParticles, arg++, pSet->particleConnections);
    kernelBufferArg (searchParticles, arg++, pSet->particleLinkLines);
    kernelBufferArg (searchParticles, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (searchParticles, arg++, pSet->statusBuffer);
    kernelBufferArg (searchParticles, arg++, clParamBuffer);
    kernelBufferArg (searchParticles, arg++, pSet->searchOp);

    status = clEnqueueNDRangeKernel( commandQueue, searchParticles, 1, NULL, globalThreads, NULL, 0, NULL, NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "searchParticles");
        return;
    }
    
    CL_FINISH;
}

void clMakeLinkLines (gpuParticleSet_t *pSet)
{
    cl_int   status;
    size_t globalThreads[1];
    int arg;

    if (clParameters->flags & CL_EFLAG_SHOWLINK)
    {
        /* Transfer link lines to a vertex/color buffer
         */
        globalThreads[0] = pSet->particleCountAlgn;
        arg = 0;
        kernelBufferArg (mkLinkLines, arg++, CURRENT_POS(pSet));
        kernelBufferArg (mkLinkLines, arg++, CURRENT_COL(pSet));
        kernelBufferArg (mkLinkLines, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (mkLinkLines, arg++, pSet->particleConnections);
        kernelBufferArg (mkLinkLines, arg++, pSet->particleLinkLines);
        kernelBufferArg (mkLinkLines, arg++, pSet->particleLinkLineColors);
        kernelBufferArg (mkLinkLines, arg++, pSet->statusBuffer);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     mkLinkLines,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
        CL_FINISH;

        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "mkLinkLines");
            return;
        }

        clBufferToHost (pSet->statusBuffer, 0);
    }
}

static void updateVectors (gpuParticleSet_t *pSet)
{
    cl_int   status;
    size_t globalThreads[1];
    int arg;

    /* restore colors towards original color
     */
    if (GETVAL32(colRestoreSpeed) > 0)
    {
//        if ((pSet->iteration % 5) == 0)
        {
            clRestoreColors (pSet, colorRestore);
        }
    }

    /* update particle vectors
     */
    globalThreads[0] = pSet->particleCountAlgn;

    if (getValue (&particleAttraction))
    {
        kParticleVecUpdate = GETVAL32(clAvgMode) ? 
            particleVecUpdateDist : particleVecUpdate;

        arg = 0;
        kernelBufferArg (kParticleVecUpdate, arg++, ALT_VEC(pSet));
        kernelBufferArg (kParticleVecUpdate, arg++, accelBuffer);
        kernelBufferArg (kParticleVecUpdate, arg++, diffBuffer);
        kernelBufferArg (kParticleVecUpdate, arg++, CURRENT_POS(pSet));
        kernelBufferArg (kParticleVecUpdate, arg++, CURRENT_VEC(pSet));
        kernelBufferArg (kParticleVecUpdate, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (kParticleVecUpdate, arg++, fieldBuffer[curField]);
        kernelBufferArg (kParticleVecUpdate, arg++, pSet->particleConnections);
        kernelBufferArg (kParticleVecUpdate, arg++, pSet->statusBuffer);
        kernelBufferArg (kParticleVecUpdate, arg++, clParamBuffer);

        SWITCH_VEC(pSet);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     kParticleVecUpdate,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "particleVecUpdate");
            return;
        }
    }
    else
    {
        arg = 0;
        kernelBufferArg (particleVecUpdateNg, arg++, ALT_VEC(pSet));
        kernelBufferArg (particleVecUpdateNg, arg++, accelBuffer);
        kernelBufferArg (particleVecUpdateNg, arg++, CURRENT_POS(pSet));
        kernelBufferArg (particleVecUpdateNg, arg++, CURRENT_VEC(pSet));
        kernelBufferArg (particleVecUpdateNg, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (particleVecUpdateNg, arg++, pSet->particleConnections);
        kernelBufferArg (particleVecUpdateNg, arg++, pSet->statusBuffer);
        kernelBufferArg (particleVecUpdateNg, arg++, clParamBuffer);

        SWITCH_VEC(pSet);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     particleVecUpdateNg,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "particleVecUpdateNg");
            return;
        }
    }
    CL_FINISH;

    /* do collissions
     */
    if (pSet->canCollide)
    {
        particleCollide(pSet);
    }

    /* operate blobs 
     */
    if (pSet->numBlobs)
    {
        runBlobs (pSet);
    }

}

static void follow (gpuParticleSet_t *pSet)
{
    int arg = 0;
    cl_int status;
    size_t globalThreads[1];
    int ax;
    int ay;

    globalThreads[0] = pSet->particleCountAlgn;
    kernelBufferArg (getPosAverage, arg++, CURRENT_POS(pSet));
    kernelBufferArg (getPosAverage, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (getPosAverage, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 getPosAverage,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "getPosAverage");
        return;
    }
    
    clBufferToHost (pSet->statusBuffer, 0);

    pSet->following = pSet->clStatus->count;


    if (!pSet->following)
        return;

    ax = (int)(pSet->clStatus->avgx / (cl_long)pSet->following);
    ay = (int)(pSet->clStatus->avgy / (cl_long)pSet->following);

    setPan(ax - INTERNC((wid/2)/zoom), ay - INTERNC((hei/2)/zoom), 0);
}

void runParticleMove (gpuParticleSet_t *pSet, int scrUpdate, int stopped)
{
    cl_int   status;
    size_t globalThreads[1];
    int arg;
    clBuffer_t *cvec, *nvec;
    cl_kernel kernel;

    pSet->iteration++;


    if (!pSet->particleCountAlgn)
        return;

    if (pSet->clearTrails)
    {
        clearParticleTrails (pSet);
        pSet->clearTrails = 0;
    }

    if (PENDING_JOB(CL_JOB_MARK_PARTICLES))
    {
        pSet->search->op   = particleMarkOp;
        clSearchParticles (pSet);
        
        /* update number of marked particles and transfer it back to GPU */
        clBufferToHost (pSet->statusBuffer, 0);
        switch (particleMarkOp & SO_MASK)
        {
            case CL_MARK_ADD:
            case CL_MARK_CLEAR:
                clParameters->markedParticles += pSet->clStatus->count;
                break;
            case CL_FOLLOW:
                pSet->following = pSet->clStatus->count;
                break;
        }
        
        clBufferToGPU (clParamBuffer, 1);

        PENDING_JOB_DONE(CL_JOB_MARK_PARTICLES);
    }

    if (PENDING_JOB(CL_JOB_GRAB_MARKED))
    {
        pSet->search->op = SO_GRAB;
        pSet->search->centerPos = clGrabCenter; 

        /* if it's known that there are marked particles, grab them 
         * instead of those below the cursor
         */
        if (clParameters->markedParticles)
            pSet->search->op |= SO_SEARCH_MARKED;

        clSearchParticles (pSet);
        clBufferToHost (pSet->statusBuffer, 0);
        clDoGrab = (pSet->clStatus->count != 0);

        PENDING_JOB_DONE(CL_JOB_GRAB_MARKED);
    }
    
    if (PENDING_JOB(CL_JOB_CHATTR_MARKED))
    {
        *(pSet->search) = currentChattrOp;

        if (clParameters->markedParticles)
            pSet->search->op |= SO_SEARCH_MARKED;

        clSearchParticles (pSet);

        /* Stop painting if markers are to be deleted afterwards
         */
        if (PENDING_JOB(CL_JOB_UNMARK_AFTER))
            PENDING_JOB_DONE(CL_JOB_CHATTR_MARKED);
    }

    if (!stopped)
    {
        updateVectors (pSet);
        cvec = CURRENT_VEC(pSet);
        nvec = ALT_VEC(pSet);
    }
    else
    {
        /* if the engine is stopped, fake a zero vector for all particles
         * that may get adjusted if particles are grabbed (which should
         * also work).
         * The original vector is preserved in CURRENT_VEC
         */
        FILL_BUFFER(ALT_VEC(pSet), zeroVec);
        cvec = nvec = ALT_VEC(pSet);
    }
        
    /* handle grabbed particles (resets vector)
     */
    if (clDoGrab)
    {
        /* Overwrite accelleration vector for all "grabbed particles". Always directly move
         * to the remembered position relative to the cursor.
         */
        globalThreads[0] = pSet->particleCountAlgn;

        arg = 0;
        kernelBufferArg (moveMarked, arg++, CURRENT_POS(pSet));
        kernelBufferArg (moveMarked, arg++, cvec);
        kernelBufferArg (moveMarked, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (moveMarked, arg++, pSet->grabRelPos);
        kernelBufferArg (moveMarked, arg++, clParamBuffer);
        clSetKernelArg  (moveMarked, arg++, sizeof(clPointerPos), (void *)&clPointerPos);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     moveMarked,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);

        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "moveMarked");
            return;
        }
        CL_FINISH;
    }

    if (GETVAL32(particleAttraction) && GETVAL32(einsteinFactor))
        kernel = particleMoveE;
    else
        kernel = particleMove;

    /* move particles, incl. wall collission
     */
    globalThreads[0] = pSet->particleCountP2;
    arg = 0;
    kernelBufferArg (kernel, arg++, ALT_POS(pSet));
    kernelBufferArg (kernel, arg++, nvec);
    kernelBufferArg (kernel, arg++, CURRENT_POS(pSet));
    kernelBufferArg (kernel, arg++, cvec);
    kernelBufferArg (kernel, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (kernel, arg++, clWalls);
    kernelBufferArg (kernel, arg++, CUR_TEMPR_BUF);
    if (kernel == particleMoveE)
        kernelBufferArg (kernel, arg++, fieldBuffer[curField]);
    kernelBufferArg (kernel, arg++, clParamBuffer);
    kernelBufferArg (kernel, arg++, pSet->particleTrailLines);
    kernelBufferArg (kernel, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 kernel,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);

    CL_FINISH;
    SWITCH_POS(pSet);
    
    if (!stopped)
        SWITCH_VEC(pSet);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", (kernel == particleMove) ? "particleMove" : "particleMoveE");
exit(0);
        return;
    }

    if (PENDING_JOB(CL_JOB_DEL_MARKED))
    {
        /* delete particles on marked pixels
         */
        pSet->search->op = SO_DELETE;

        clSearchParticles (pSet);
        PENDING_JOB_DONE(CL_JOB_DEL_MARKED);
    }

    clBufferToHost (pSet->statusBuffer, 0);

    /* handle deleted particles
     */
    if (pSet->clStatus->needToDelete)
    {
        arg = 0;
        kernelBufferArg (cleanupDeletedParticles, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (cleanupDeletedParticles, arg++, pSet->particleConnections);
        kernelBufferArg (cleanupDeletedParticles, arg++, CURRENT_POS(pSet));
        kernelBufferArg (cleanupDeletedParticles, arg++, CURRENT_VEC(pSet));
        kernelBufferArg (cleanupDeletedParticles, arg++, CURRENT_COL(pSet));
        kernelBufferArg (cleanupDeletedParticles, arg++, pSet->particleSortedOff[0]);
        kernelBufferArg (cleanupDeletedParticles, arg++, CURRENT_BLOBP(pSet));
        kernelBufferArg (cleanupDeletedParticles, arg++, pSet->blobs);
        kernelBufferArg (cleanupDeletedParticles, arg++, clParamBuffer);
        kernelBufferArg (cleanupDeletedParticles, arg++, pSet->statusBuffer);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     cleanupDeletedParticles,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
        CL_FINISH;

        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "deleteParticles");
            return;
        }

        clBufferToHost (pSet->statusBuffer, 0);

        /* clear all particle trails before next iteration to remove artifacts
         */
        pSet->clearTrails = 1;
        pSet->haveDeleted = 1;

        pSet->recalcBlobs = pSet->clStatus->recalcBlobs;
    }

    /* handle new/broken particle connections
     */
    if (pSet->clStatus->updateLinks)
    {
        globalThreads[0] = pSet->particleCountAlgn;
        arg = 0;
        kernelBufferArg (cleanupConnections, arg++, pSet->particleConnections);
        kernelBufferArg (cleanupConnections, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (cleanupConnections, arg++, pSet->statusBuffer);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     cleanupConnections,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
        CL_FINISH;

        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "cleanupConnections");
            return;
        }
    }

    if (clParameters->markedParticles)
    {
        /* if there are or might be marked particles, count them
         */
        clearStatusBuffer (pSet);

        globalThreads[0] = pSet->particleCountAlgn;
        arg = 0;
        kernelBufferArg (countAndColorMarked, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (countAndColorMarked, arg++, CURRENT_COL(pSet));
        kernelBufferArg (countAndColorMarked, arg++, CURRENT_POS(pSet));
        kernelBufferArg (countAndColorMarked, arg++, pSet->statusBuffer);
        kernelBufferArg (countAndColorMarked, arg++, clParamBuffer);
        
        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     countAndColorMarked,
                     1,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
        CL_FINISH;

        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "countAndColorMarked");
            return;
        }
        
        clBufferToHost (pSet->statusBuffer, 0);
        
        if (clParameters->markedParticles != pSet->clStatus->count)
        {
            clParameters->markedParticles = pSet->clStatus->count;
            clParametersNotifyChange();
        }
    }

    if (pSet->haveNewParticles)
    {
        /* transfer new particle markers to a host buffer to avoid creating new
         * ones on the corresponding screen pixels
         */
        getNewParticleFlags (pSet);
    }

    if (pSet->following)
    {
        follow (pSet);
    }

    /* compress the particle table from time to time if there are deleted particles
     */
    //INTERVAL_RUN(500, clCompressTables (pSet, 0));
    //clUpdateFreelist(pSet);
}

void printSE(gpuParticleSet_t *pSet)
{
    int x, y, i;
    int s, e;

    clBufferToHost (pSet->cellStart, 0);
    clBufferToHost (pSet->cellEnd, 0);
    clBufferToHost (pSet->particleSortedIdx[1], 0);

    i = 0;
    for (y = 0; y < hei; y++)
    {
        for (x = 0; x < wid; x++)
        {
            int off = y * wid + x;

            s = pSet->cellStartHost[off];
            e = pSet->cellEndHost[off] - 1;

            if (s != -1)
            {
                printf ("%d/%d:%d(%d)..%d(%d)\n",
                    x, y,
                    s, pSet->particleSortedIdxHost[s],
                    e, pSet->particleSortedIdxHost[e]);
            }
        }
        i++;
    }
}

void checkSE(gpuParticleSet_t *pSet)
{
    int x, y;
    int count = 0;
    int poff, pid;
    int i;
    int n = 10;

    clBufferToHost (pSet->cellStart, 0);
    clBufferToHost (pSet->cellEnd, 0);
    clBufferToHost (CURRENT_POS(pSet), 0);
    clBufferToHost (CURRENT_ATTR(pSet), 0);
    clBufferToHost (pSet->particleSortedIdx[1], 0);

    for (y = 0; y < hei; y++)
    {
        for (x = 0; x < wid; x++)
        {
            int off = y * wid + x;

            if ((pSet->cellStartHost[off] == MAXKEY) && (pSet->cellEndHost[off] == MINKEY))
                continue;

            if (((pSet->cellStartHost[off] == MAXKEY) && 
                 (pSet->cellEndHost[off] != MINKEY)) ||
                ((pSet->cellEndHost[off] == MAXKEY) &&
                 (pSet->cellStartHost[off] != MINKEY)))
            {
                printf ("%d/%d: start=%d end=%d\n", x, y,
                    pSet->cellStartHost[off], pSet->cellEndHost[off]);
                return;
            }

            if (pSet->cellStartHost[off] > pSet->cellEndHost[off])
            {
                printf ("Start %d > END %d!\n", pSet->cellStartHost[off], pSet->cellEndHost[off]);
                return;
            }

            for (i = pSet->cellStartHost[off]; i < pSet->cellEndHost[off]; i++)
            {
                pid = pSet->particleSortedIdxHost[i];

                poff = (int)pSet->particleStartPos[pid*2] + (int)pSet->particleStartPos[pid*2+1] * wid;

                if (poff != off)
                {
                    printf ("checkSE: bad offset: pos=%u/%u, is=%u/%u, pid=%u(%d) start=%u, end=%u, i=%u\n",
                            x, y, poff%wid, poff/wid, pid,
                            pSet->particleAttrHost[pid], pSet->cellStartHost[off],
                            pSet->cellEndHost[off], i);

                    if (n-- == 0)
                    return;
                }
            }

            count += pSet->cellEndHost[off] - pSet->cellStartHost[off];

//            printf ("%d/%d: %d\n", x, y, pSet->cellEndHost[off] - pSet->cellStartHost[off]);
        }
    }


//    printf ("checkSE: good (%d particles)\n", count);
}

void particleCollide (gpuParticleSet_t *pSet)
{
    cl_int   status;
    size_t globalThreads[1];
    static int maxk[1] = { MAXKEY };
    static int mink[1] = { MINKEY };
    int arg;

    /* clear per-pixel start/end markers */

    FILL_BUFFER(pSet->cellStart, maxk);
    FILL_BUFFER(pSet->cellEnd, mink);

    /* generate the key (=screen offset) / value (=particle index)
     * table for sorting
     */
    arg = 0;
    globalThreads[0] = pSet->particleCountAlgn;

    kernelBufferArg (getKeys, arg++, CURRENT_POS(pSet));
    kernelBufferArg (getKeys, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (getKeys, arg++, pSet->particleSortedOff[0]);
    kernelBufferArg (getKeys, arg++, pSet->particleSortedIdx[0]);
    kernelBufferArg (getKeys, arg++, clParamBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 getKeys,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "getKeys");
        return;
    }
    CL_FINISH

    /* sort particle IDs (=index into position/vector/attributes/color tables) using
     * the particle's screen pixel offset as key
     */
    bitonicSort (commandQueue,
                pSet->particleSortedOff[1]->clBuffer,
                pSet->particleSortedIdx[1]->clBuffer,
                pSet->particleSortedOff[0]->clBuffer,
                pSet->particleSortedIdx[0]->clBuffer, 
                1,
                pSet->particleCountP2,
                1);

    CL_FINISH


    /* for each pixel, create a start/end pointer into the list of sorted IDs,
     * If there is no particle on the pixel, set start > end pointer
     */
    globalThreads[0] = pSet->particleCountP2;

    arg = 0;
    kernelBufferArg (getPixelBoundaries, arg++, pSet->cellStart);
    kernelBufferArg (getPixelBoundaries, arg++, pSet->cellEnd);
    kernelBufferArg (getPixelBoundaries, arg++, pSet->particleSortedOff[1]);
    kernelBufferArg (getPixelBoundaries, arg++, clParamBuffer);
    kernelBufferArg (getPixelBoundaries, arg++, pSet->statusBuffer);
    
    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 getPixelBoundaries,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "getPixelBoundaries");
        return;
    }
    CL_FINISH

    globalThreads[0] = pSet->particleCountP2;

    if (globalThreads[0])
    {
        doCollide = doCollideElasticLink;

        /* calculate particle collissions (updates vectors and colors)
         */
        arg = 0;
        kernelBufferArg (doCollide, arg++, pSet->cellStart);
        kernelBufferArg (doCollide, arg++, pSet->cellEnd);
        kernelBufferArg (doCollide, arg++, pSet->particleSortedIdx[1]);
        kernelBufferArg (doCollide, arg++, CURRENT_POS(pSet));
        kernelBufferArg (doCollide, arg++, CURRENT_VEC(pSet));
        kernelBufferArg (doCollide, arg++, ALT_VEC(pSet));
        kernelBufferArg (doCollide, arg++, CURRENT_ATTR(pSet));
        kernelBufferArg (doCollide, arg++, CURRENT_COL(pSet));
        kernelBufferArg (doCollide, arg++, ALT_COL(pSet));
        kernelBufferArg (doCollide, arg++, pSet->particleConnections);
        kernelBufferArg (doCollide, arg++, clParamBuffer);
        kernelBufferArg (doCollide, arg++, pSet->statusBuffer);

        SWITCH_VEC(pSet);
        SWITCH_COL(pSet);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     doCollide,
                     1,
                     NULL,
                     globalThreads,
                     pSet->collLocalThreadLimit,
                     0,
                     NULL,
                     NULL);
        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "doCollide");
            return;
        }

        CL_FINISH
    }
}

void clParticles(int scrUpdate, int stopped)
{
    int i;
    int wallsCopied = 0;
    int work = 0;

    for (i = 0; i < CL_NUM_PARTICLE_SETS; i++)
    {
        if (particleSet[i].particleCountAlgn == 0)
            continue;

        if (!wallsCopied && clWallUpdateFlag)
        {
            clBufferToGPU (clWalls, 1);
            clBufferToGPU (accelBuffer, 1);
            clWallUpdateFlag = 0;
        }
        wallsCopied = 1;

        runParticleMove(&particleSet[i], scrUpdate, stopped);
        work = 1;
    }

    if (work)
    {
        CL_FINISH

        clParticleLoops++;
    }
}

int clCanCreateNew (int ix, int iy)
{
    uint off = SCREENC(ix)+SCREENC(iy)*wid;

    /*XXX*/
#if 1
    if (SIFLAG_ISSET(cpuScrFlags, off))
        return 0;

    if (SIFLAG_ISSET(gpuScrFlagsHost, off))
        return 0;
#endif

    return 1;
}

void clearNewMarkers (void)
{
    cl_int status = 0;
    int arg = 0;
    size_t globalThreads[1];
    gpuParticleSet_t *pSet = &particleSet[movingPartSet];

    /* clear "new particle" markers in all particles
     */
    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;
    kernelBufferArg (clearNewFlag, arg++, CURRENT_ATTR(pSet));

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 clearNewFlag,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    CL_FINISH;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "clearNewFlag");
        return;
    }
    
    pSet->haveNewParticles = 0;

    SIFLAG_CLRALL(gpuScrFlagsHost);
}


static void addPart (int pid, attr_t *attr, float *pos, float *vec, cl_float4 *col, cl_uchar4 *base, cl_uint8 *conn, int x, int y)
{
    attr[pid].x = ENCODE_MASS(10) | ENCODE_CHARGE(1) | CL_TYPE_MOVING | CL_MFLG_COLLIDE;
    attr[pid].y = 0.5f;

    pos[pid*2 + 0] = ((float)x) + ((y & 1) ? 0.25f : 0.75f);
    pos[pid*2 + 1] = ((float)y) + 0.5f;
    vec[pid*2 + 0] = 0.0f;
    vec[pid*2 + 1] = 0.0f;

    col[pid].s[0] = 1.0f;
    col[pid].s[1] = (float)abs(x - wid/2) / (float)(wid/2);
    col[pid].s[2] = (float)abs(y - hei/2) / (float)(hei/2);
    col[pid].s[3] = 1.0f;
    base[pid].s[0] = (uint8_t)(col[pid].s[0] * 255.0f);
    base[pid].s[1] = (uint8_t)(col[pid].s[1] * 255.0f);
    base[pid].s[2] = (uint8_t)(col[pid].s[2] * 255.0f);
    base[pid].s[3] = (uint8_t)(col[pid].s[3] * 255.0f);

    memset (&conn[pid], 0, sizeof(cl_int8));

    if (getValue (&clDoLink))
    {
        conn[pid].s[7] = getValue (&clNumLinks);
    }
    else
    {
        attr[pid].x |= CL_MFLG_LINKFULL;
    }

    memset ((char*)&conn[pid], 0, sizeof(*conn));
}

void clFillAll (gpuParticleSet_t *pSet, int mode)
{
    int                 pid;
    attr_t        *attr = (attr_t*)CURRENT_ATTR(pSet)->hostBuffer;
    float        *pos = (float*)CURRENT_POS(pSet)->hostBuffer;
    float        *vec = (float*)CURRENT_VEC(pSet)->hostBuffer;
    cl_float4        *col = (cl_float4*)CURRENT_COL(pSet)->hostBuffer;
    cl_uchar4        *base = (cl_uchar4*)CURRENT_BASE_COL(pSet)->hostBuffer;
    cl_uint8        *conn = (cl_uint8*)(pSet->particleConnections->hostBuffer);
    int max = pSet->maxGpuParticles;
    int i, j;


    clBufferToHost (CURRENT_POS(pSet), 0);
    clBufferToHost (CURRENT_VEC(pSet), 0);
    clBufferToHost (CURRENT_ATTR(pSet), 0);
    clBufferToHost (CURRENT_COL(pSet), 0);
    clBufferToHost (CURRENT_BASE_COL(pSet), 0);
    clBufferToHost (pSet->particleConnections, 0);
    clFinish (commandQueue);

    pid = 1;
    for (i = 1; i < hei/2; i++)
    {
        if (pid >= max)
            break;

        for (j = -i; j <= i; j++)
        {
            addPart (pid++, attr, pos, vec, col, base, conn, wid/2+j, hei/2-i);
            if (pid >= max)
                break;

            addPart (pid++, attr, pos, vec, col, base, conn, wid/2+j, hei/2+i);
            if (pid >= max)
                break;

            if (j != abs(i))
            {
                addPart (pid++, attr, pos, vec, col, base, conn, wid/2-i, hei/2+j);
                if (pid >= max)
                    break;

                addPart (pid++, attr, pos, vec, col, base, conn, wid/2+i, hei/2+j);
                if (pid >= max)
                    break;
            }
        }
    }

    clBufferToGPU (CURRENT_POS(pSet), 0);
    clBufferToGPU (CURRENT_VEC(pSet), 0);
    clBufferToGPU (CURRENT_ATTR(pSet), 0);
    clBufferToGPU (CURRENT_COL(pSet), 0);
    clBufferToGPU (CURRENT_BASE_COL(pSet), 0);
    clBufferToGPU (pSet->particleConnections, 0);
    clFinish (commandQueue);

    setNGpuParticles (pSet, pid);
    totalGpuParticles = pSet->particleCountAlgn;
}

/* compress particle tables by eliminating holes.
 * Updates particle counts (pSet->particleCount).
 */ 
int clCompressTables (
    gpuParticleSet_t *pSet,
    int force
    )
{
    cl_int status = 0;
    int arg = 0;
    size_t globalThreads[1];

    /* The particle count is the maximum of active particles. Nothing to do if
     * zero, or if there were no deletions since the last time tables were
     * compressed
     */
    if (!force && (!pSet->particleCountAlgn || !pSet->haveDeleted))
    {
        return 0;
    }

    pSet->haveDeleted = 0;

    clBufferToHost (pSet->statusBuffer, 0);
    debug ("particles before compress: %x/%x/%x\n", pSet->particleCountAlgn, pSet->clStatus->count, pSet->particleCountP2);

    /* zero out the attributes in the new attributes buffer
     */
    FILL_BUFFER(ALT_ATTR(pSet), empty);

    globalThreads[0] = pSet->particleCountAlgn;

    /* Destination buffers 
     */
    kernelBufferArg (compressParticleTable, arg++, ALT_POS(pSet));
    kernelBufferArg (compressParticleTable, arg++, ALT_VEC(pSet));
    kernelBufferArg (compressParticleTable, arg++, ALT_ATTR(pSet));
    kernelBufferArg (compressParticleTable, arg++, ALT_COL(pSet));
    kernelBufferArg (compressParticleTable, arg++, pSet->tmpParticleConnections);
    kernelBufferArg (compressParticleTable, arg++, ALT_BASE_COL(pSet));
    kernelBufferArg (compressParticleTable, arg++, ALT_BLOBP(pSet));

    /* Source buffers
     */
    kernelBufferArg (compressParticleTable, arg++, CURRENT_POS(pSet));
    kernelBufferArg (compressParticleTable, arg++, CURRENT_VEC(pSet));
    kernelBufferArg (compressParticleTable, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (compressParticleTable, arg++, CURRENT_COL(pSet));
    kernelBufferArg (compressParticleTable, arg++, pSet->particleConnections);
    kernelBufferArg (compressParticleTable, arg++, CURRENT_BASE_COL(pSet));
    kernelBufferArg (compressParticleTable, arg++, CURRENT_BLOBP(pSet));

    /* Remap table (oldId->newID)
     */
    kernelBufferArg (compressParticleTable, arg++, pSet->idRemap);

    /* count element will hold new highest index
     */
    kernelBufferArg (compressParticleTable, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 compressParticleTable,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        debug( "Error: Enqueueing kernel onto command queue. (%s)\n", "compressParticleTable");
        return 0;
    }
    CL_FINISH

    /* switch buffers
     */
    SWITCH_POS(pSet);
    SWITCH_VEC(pSet);
    SWITCH_ATTR(pSet);
    SWITCH_COL(pSet);
    SWITCH_BASE_COL(pSet);
    SWITCH_BLOBP(pSet);


    /* get new amount of particles
     */
    clBufferToHost (pSet->statusBuffer, 0);

    setNGpuParticles (pSet, pSet->clStatus->count ? pSet->clStatus->count + 1 : 0);
    debug ("particles after compress: %x/%x/%x\n", pSet->particleCountAlgn, pSet->clStatus->count, pSet->particleCountP2);

    if (pSet->particleCountAlgn == 0)
    {
        FILL_BUFFER(pSet->particleConnections, empty);
        return 0;
    }

    /* If there are grabbed particles, also re-organize the 
     * buffer that stores the relative positions
     * - use the old, non-compressed attributes
     * - reorganize relative positions to unused position buffer
     *   and copy back the position buffer afterwards
     */
    if (clDoGrab)
    {
        arg = 0;
        kernelBufferArg (compressGrabbed, arg++, ALT_ATTR(pSet));
        kernelBufferArg (compressGrabbed, arg++, pSet->grabRelPos);
        kernelBufferArg (compressGrabbed, arg++, ALT_POS(pSet));
        kernelBufferArg (compressGrabbed, arg++, pSet->idRemap);
        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     compressGrabbed,
                     1,
                     NULL,
                     globalThreads,
                     0,
                     0,
                     NULL,
                     NULL);

        if(status != CL_SUCCESS) 
        { 
            printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "compressGrabbed");
            return 0;
        }
        CL_FINISH

        /* copy back the reorganized data
         */
        COPY_BUFFER (pSet->grabRelPos, ALT_POS(pSet));
    }

    /* Fix the link tables (they contain the old indices) by using the id remap
     * buffer.
     * Also transfer the link table back from the temporary to the
     * working buffer.
     */
    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;
    kernelBufferArg (fixCompressedLinks, arg++, pSet->particleConnections);
    kernelBufferArg (fixCompressedLinks, arg++, pSet->tmpParticleConnections);
    kernelBufferArg (fixCompressedLinks, arg++, pSet->idRemap);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 fixCompressedLinks,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "fixCompressedLinks");
        return 0;
    }
    CL_FINISH

    return 1;
}

void clRestoreColors (
    gpuParticleSet_t *pSet,
    float restoreRate
    )
{
    cl_int status = 0;
    int arg = 0;
    size_t globalThreads[1];

    globalThreads[0] = pSet->particleCountAlgn;

    kernelBufferArg (restoreColors, arg++, CURRENT_BASE_COL(pSet));
    kernelBufferArg (restoreColors, arg++, CURRENT_COL(pSet));
    kernelBufferArg (restoreColors, arg++, ALT_COL(pSet));
    clSetKernelArg  (restoreColors, arg++, sizeof(cl_float), (void *)&restoreRate);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 restoreColors,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    SWITCH_COL(pSet);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "restoreColors");
        return;
    }
}

/* Create a blob from particles marked as new
 */
void clMakeBlobFromNew (
    gpuParticleSet_t *pSet
    )
{
    size_t globalThreads[1];
    int blob;
    int arg;
    int count;
    cl_int status = 0;

    clBufferToHost (pSet->blobs, 0);

    for (blob = 1; blob < MAX_CL_BLOBS; blob++)
    {
        if (pSet->blobsHost[blob].count == 0)
        {
            memset (&pSet->blobsHost[blob], 0, sizeof(blob_t));
            break;
        }
    }

    if (blob >= MAX_CL_BLOBS)
    {
        printf ("No free blob\n");
        return;
    }

    printf ("New blob %d\n", blob);

    clBufferToGPU (pSet->blobs, 1);

    /* find new particles and assign to blob
     */
    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;

    kernelBufferArg (makeBlobFromNew, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (makeBlobFromNew, arg++, CURRENT_POS(pSet));
    kernelBufferArg (makeBlobFromNew, arg++, CURRENT_VEC(pSet));
    kernelBufferArg (makeBlobFromNew, arg++, pSet->idRemap);
    kernelIntArg    (makeBlobFromNew, arg++, &blob);
    kernelBufferArg (makeBlobFromNew, arg++, pSet->blobs);
    kernelBufferArg (makeBlobFromNew, arg++, CURRENT_BLOBP(pSet));

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 makeBlobFromNew,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "makeBlobFromNew");
        return;
    }

    clBufferToHost (pSet->blobs, 0);

    pSet->blobsHost[blob].aSpeed = 0;
    count = pSet->blobsHost[blob].count;

    /* none found, abort
     */
    if (count <= 0)
    {
        printf ("No particles found %d\n", (int)globalThreads[0]);
        pSet->blobsHost[blob].count = 0;
        return;
    }

    /******************** stage 2: update center 
     */
    pSet->blobsHost[blob].cx = pSet->blobsHost[blob].cx / (float)count;
    pSet->blobsHost[blob].cy = pSet->blobsHost[blob].cy / (float)count;
#if 0
    printf ("Adding %d/%d particles to blob %d, center %f/%f\n",
        count, (int)globalThreads[0], blob, pSet->blobsHost[blob].cx, pSet->blobsHost[blob].cy);
#endif

    clBufferToGPU (pSet->blobs, 1);

    /******************** stage 3: Generate particle data
     */

    globalThreads[0] = ROUNDUP_4K(count);
    arg = 0;

    kernelBufferArg (makeBlobStage3, arg++, pSet->blobs);
    kernelBufferArg (makeBlobStage3, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (makeBlobStage3, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (makeBlobStage3, arg++, CURRENT_POS(pSet));
    kernelBufferArg (makeBlobStage3, arg++, pSet->idRemap);
    kernelIntArg    (makeBlobStage3, arg++, &blob);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 makeBlobStage3,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "makeBlobStage3");
        return;
    }

    /******************** stage 4: Finalize static particle data
     */

    globalThreads[0] = ROUNDUP_4K(count);
    arg = 0;

    kernelBufferArg (makeBlobStage4, arg++, pSet->blobs);
    kernelBufferArg (makeBlobStage4, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (makeBlobStage4, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (makeBlobStage4, arg++, CURRENT_POS(pSet));
    kernelBufferArg (makeBlobStage4, arg++, pSet->idRemap);
    kernelIntArg    (makeBlobStage4, arg++, &blob);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 makeBlobStage4,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "makeBlobStage4");
        return;
    }

    pSet->numBlobs++;
}

void recalBlobs (
    gpuParticleSet_t *pSet
    )
{
    size_t globalThreads[1];
    cl_uint status;
    int arg;
    int e = 0;
    uint numRecal;

    /* zero out counter */
    FILL_BUFFER_PART(pSet->idRemap, e, 0, 1);

    /* Stage 1: find particles of modified blobs 
     */
    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;

    kernelBufferArg (recalBlobStage1, arg++, pSet->blobs);
    kernelBufferArg (recalBlobStage1, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (recalBlobStage1, arg++, pSet->idRemap);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 recalBlobStage1,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "recalBlobStage1");
        return;
    }

    /* copy back id list counter size 
     */
    clBufferToHostPart (pSet->idRemap, 0, 0, 1);

    numRecal = ((uint*)pSet->idRemap->hostBuffer)[0];

    if ((numRecal <= 0) || (numRecal > pSet->particleCountAlgn))
        return;

    /* stage 2: update/prepare blob structures
     */
    globalThreads[0] = MAX_CL_BLOBS;
    arg = 0;

    kernelBufferArg (recalBlobStage2, arg++, pSet->blobs);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 recalBlobStage2,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "recalBlobStage2");
        return;
    }

    /* stage 3: Calculate new relative positions/angles, sum up blob MOI
     */
    globalThreads[0] = numRecal;
    arg = 0;

    kernelBufferArg (recalBlobStage3, arg++, pSet->blobs);
    kernelBufferArg (recalBlobStage3, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (recalBlobStage3, arg++, pSet->idRemap);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 recalBlobStage3,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "recalBlobStage3");
        return;
    }

    /* stage 4: Calculate particle impuls
     */
    globalThreads[0] = numRecal;
    arg = 0;

    kernelBufferArg (recalBlobStage4, arg++, pSet->blobs);
    kernelBufferArg (recalBlobStage4, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (recalBlobStage4, arg++, pSet->idRemap);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 recalBlobStage4,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "recalBlobStage4");
        return;
    }

}


void runBlobs (
    gpuParticleSet_t *pSet
    )
{
    size_t globalThreads[1];
    int arg;
    cl_int status = 0;

    if (pSet->recalcBlobs)
    {
        pSet->recalcBlobs = 0;

        recalBlobs (pSet);
    }

    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;

    kernelBufferArg (blobUpdateStage1, arg++, pSet->blobs);
    kernelBufferArg (blobUpdateStage1, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (blobUpdateStage1, arg++, CURRENT_POS(pSet));
    kernelBufferArg (blobUpdateStage1, arg++, CURRENT_VEC(pSet));
    kernelBufferArg (blobUpdateStage1, arg++, pSet->particleLinkLines);
    kernelBufferArg (blobUpdateStage1, arg++, pSet->particleLinkLineColors);
    kernelBufferArg (blobUpdateStage1, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 blobUpdateStage1,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "blobUpdateStage1");
        return;
    }

    globalThreads[0] = MAX_CL_BLOBS;
    arg = 0;
    kernelBufferArg (blobUpdateStage2, arg++, pSet->blobs);
    kernelBufferArg (blobUpdateStage2, arg++, pSet->particleLinkLines);
    kernelBufferArg (blobUpdateStage2, arg++, pSet->particleLinkLineColors);
    kernelBufferArg (blobUpdateStage2, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 blobUpdateStage2,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "blobUpdateStage2");
        return;
    }

    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;

    kernelBufferArg (blobUpdateStage3, arg++, pSet->blobs);
    kernelBufferArg (blobUpdateStage3, arg++, CURRENT_BLOBP(pSet));

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 blobUpdateStage3,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "blobUpdateStage3");
        return;
    }
}

int clUpdateFreelist (gpuParticleSet_t *pSet)
{
    size_t globalThreads[1];
    int arg;
    cl_int status = 0;

    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;

    kernelBufferArg (getParticleFreeList, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (getParticleFreeList, arg++, pSet->freelist);
    kernelBufferArg (getParticleFreeList, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 getParticleFreeList,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS)
    {
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "getParticleFreeList");
        return 0;
    }

    clBufferToHost (pSet->statusBuffer, 0);
    pSet->nextFree = 0;

    if ((pSet->numFree = pSet->clStatus->count) == 0)
    {
        printf ("No free particles)\n");
        return 0;
    }
    else
        printf ("%d free particles\n", pSet->numFree);

    clBufferToHost (pSet->freelist, 0);

    return pSet->numFree;
}

/* get a free particle from the freelist
 * Returns -1 if none available
 */
int clGetFreeParticle (gpuParticleSet_t *pSet)
{
    int rc;

    if (pSet->nextFree >= pSet->numFree)
    {
        if (clUpdateFreelist (pSet) == 0)
            return -1;
    }

    rc = pSet->freelistHost[pSet->nextFree];
    pSet->nextFree++;

    return rc;
}

/* create particles in particle job buffer
 */
void clCreateParticles (gpuParticleSet_t *pSet)
{
    size_t globalThreads[1];
    int arg;
    cl_int status = 0;

    if (!pSet->numCreateJobs)
        return;

    clBufferToGPU (pSet->createJobs, 1);

    globalThreads[0] = ROUNDUP_4K(pSet->numCreateJobs);
    arg = 0;

    kernelBufferArg (addParticles, arg++, CURRENT_POS(pSet));
    kernelBufferArg (addParticles, arg++, CURRENT_VEC(pSet));
    kernelBufferArg (addParticles, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (addParticles, arg++, CURRENT_COL(pSet));
    kernelBufferArg (addParticles, arg++, CURRENT_BASE_COL(pSet));
    kernelBufferArg (addParticles, arg++, pSet->particleConnections);
    kernelBufferArg (addParticles, arg++, CURRENT_BLOBP(pSet));
    kernelBufferArg (addParticles, arg++, pSet->createJobs);
    kernelIntArg    (addParticles, arg++, &pSet->numCreateJobs);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 addParticles,
                 1,
                 NULL,
                 globalThreads,
                 0,
                 0,
                 NULL,
                 NULL);

    pSet->numCreateJobs = 0;

    if(status != CL_SUCCESS)
    {
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "addParticles");
        return;
    }
}

/* create particles in particle job buffer
 */
particleJob_t *clGetCreateSlot (gpuParticleSet_t *pSet)
{
    particleJob_t *rc;

    if (pSet->numCreateJobs >= MAX_CREATE_JOBS)
    {
        clCreateParticles (pSet);
    }

    rc = &pSet->createJobsHost[pSet->numCreateJobs];
    pSet->numCreateJobs++;

    memset (rc, 0, sizeof(*rc));

    return rc;
}

