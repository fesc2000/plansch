
/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define CANSHARE
#undef COLL_LOSS

#ifdef COLL_LOSS
static FLOAT collLoss = 1.0;
#define CLOS(x)        ((x)*collLoss)
#else
#define CLOS(x)        (x)
#endif

static int ldToggle = 0;

#define LINK_MARKER                PFLAG1_VAL
#define LINKS_DRAWN(P)                (PFLAG1(P) == ldToggle)
#define LINK_MARK(P)                if (ldToggle) {PFLAG1_SET(P);} else {PFLAG1_CLR(P);}

#define IS_MARKED(P)                PFLAG2(P)
#define SET_MARKED(P)                PFLAG2_SET(P)
#define CLEAR_MARKED(P)                PFLAG2_CLR(P)

#define IS_STICKED(P)                PFLAG3(P)
#define SET_STICKED(P)                PFLAG3_SET(P)
#define CLEAR_STICKED(P)        PFLAG3_CLR(P)

#define NO_LINK_BREAK        (MAX_SPEEDQ * 10000)

#define MAX_DIST(data)                (int)((data) & 0xffff)
#define SET_MAX_DIST(data,d)        data = ((data) & 0xffff0000) | ((d) & 0xffff)
#define CHAIN_ATTRACT(data)        (int)((data) >> 16)
#define SET_CHAIN_ATTRACT(data,d)        data = (data & 0x0000ffff) | ((d) << 16)

#define CURRENT_MAX_DIST  ((getValue(&carMaxDist) == carMaxDist.maxValue) ? 0 : getValue(&carMaxDist))

#define MK_LINK_DATA(maxdist,attract)        (((maxdist) & 0xffff) | ((attract) << 16))
#define DFL_LINK_DATA        MK_LINK_DATA(CURRENT_MAX_DIST, getValue(&carAttraction))


#define MAX_FOOD    10000
#define        HUNGRY(p2)  ((p2)->food < (MAX_FOOD/4))
#define        SATURATED(p2)  ((p2)->food > ((MAX_FOOD/4)*3)
#define PMASS_TO_FOOD(m)    ((MAX_FOOD / 0x100) * (m))
#define FOOD_TRANSFER_THRESH        10
#define FOOD_TRANSFER                5
#define TRANSFER_LOSS                2

static int connectToPrevious = 0;
static int connectToNeighbour = 1;
static int connectToWall = 0;
static int connectToAll = 0;
static int drawing = 0;
static int drawingFirst = 0;
static particle *firstParticle = NULL;
static particle *lastParticle = NULL;

static void connectHandler (setting *s);

static int myPtype = 0;
static int wimpType = 0;

SETTING(carNumLinks) =
{
    .minValue = 1,
    .maxValue = MAX_LINKS,
    .defaultValue = 6,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "NumberOfLinks"
};

SETTING(carConnectToPassive) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect with closed link",
};


SETTING(carActivate) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 1,
    .name = "Activate/deactivate links",
};

SETTING(carMaxDist) =
{
    .minValue = 1,
    .maxValue = 500,
    .minDiff = 1,
    .defaultValue = 500,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Maximum part. dist.",
    .cb = (FUNCPTR)NULL,
};

SETTING(carConnectToPrevious) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect particles while drawing",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(carReconnectToFirst) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect particle while drawing (ring)",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(carConnectToNeighbour) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 1,
    .partAttr = 0,
    .name = "Connect Particle to neighbours",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(carAttraction) =
{
    .minValue = 1,
    .maxValue = GRID/30,
    .minDiff = 1,
    .defaultValue = 100,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Chain Attraction",
    .cb = (FUNCPTR)NULL,
};

SETTING(carDflFood) =
{
    .minValue = 1,
    .maxValue = MAX_FOOD,
    .minDiff = 1,
    .defaultValue = MAX_FOOD/2,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Food",
    .cb = (FUNCPTR)NULL,
};


static void connectHandler (setting *s)
{
    int val = getValue (s);
    connectToPrevious = 0;
    connectToNeighbour = 0;

    if ((s == &carConnectToPrevious) && val)
    {
        modifySetting (&carReconnectToFirst, 0);
        modifySetting (&carConnectToNeighbour, 0);
        connectToPrevious = 1;
    }
    else if ((s == &carReconnectToFirst) && val)
    {
        modifySetting (&carConnectToNeighbour, 0);
        modifySetting (&carConnectToPrevious, 0);
        connectToPrevious = 1;

    }
    else if ((s == &carConnectToNeighbour) && val)
    {

        modifySetting (&carReconnectToFirst, 0);
        modifySetting (&carConnectToPrevious, 0);
        connectToNeighbour = 1;
    }

    pdescs[myPtype]->noSize = connectToNeighbour;


}

static void chainUnsetType (particle *p)
{
    unlinkAll (p);

    CLEAR_MARKED(p);
    CLEAR_STICKED(p);
}

static void chainInit (int ptype)
{
    myPtype = ptype;

    wimpType = pdescFind ("Wimp");

    linkInit();
}

/* use the "gas" move function */
#define CHAIN
#define CARNIVORE
#include "genmove.c"


#ifdef COLL_LOSS
void
collLossButtonSelect (int b, int action, int ox, int oy)
{
    int x, y;

    if (IS_RESET(action))
    {
        collLoss = 1.0;
        oy = ((FLOAT)collLoss * BUTTON_HEI(b));
    }
    else if (EVENT_CODE(action) == SCROLLDOWN)
    {
        collLoss += (FLOAT)0.05;

        if (collLoss > 1.0)
            collLoss = 1.0;

        oy = (collLoss * (FLOAT)BUTTON_HEI(b));

    }
    else if (EVENT_CODE(action) == SCROLLUP)
    {
        collLoss -= (FLOAT)0.05;

        if (collLoss < 0.0)
            collLoss = 0.0;

        oy = ((FLOAT)collLoss * (FLOAT)BUTTON_HEI(b));
    }
    else
    {
//        if (BUTTON_ID(action) != 1)
//            return;

        if (oy < 0)
            oy = 0;
        if (oy > BUTTON_HEI(b))
            oy = BUTTON_HEI(b);

        collLoss = (FLOAT)(oy - 1) / (FLOAT)BUTTON_HEI(b);
    }
        
    if (buttons[b].visible)
    {
        for (x = 1; x < BUTTON_WID(b); x++)
            for (y = 1; y <= BUTTON_HEI(b); y++)
            {
                if (SCR_GET (buttons[b].x1 + x, buttons[b].y1 + y) == COL_ICONFG)
                    continue;

                if (y > oy)
                {
                    SCR_SET (buttons[b].x1 + x, buttons[b].y1 + y, COL_EMPTY);
                }
                else 
                {
                    SCR_SET (buttons[b].x1 + x, buttons[b].y1 + y, COL_ICONDRAW);
                }
            }
    }


out:
    sprintf (tmpStr, "collLoss=%f", collLoss);
    statText (4, tmpStr);

}
#endif

static void chainDrawStart()
{
    drawing = 1;
    drawingFirst = 1;
}

static void chainDrawStop()
{
    int i, t;
    int pi;
    particle *p;

    if (!tapeOperation())
    {
        if (connectToPrevious && getValue (&carReconnectToFirst) && firstParticle && lastParticle && (lastParticle != firstParticle))
        {
            /* install double-link to first drawn particle */
            linkParticles (firstParticle, lastParticle, 0, DFL_LINK_DATA);
            linkParticles (firstParticle, lastParticle, 0, DFL_LINK_DATA);
        }

        /* close all open links of marked particles
         */
        for (t = 0; t < numThreads; t++)
        {
            for (i = 0; i < tData[t]->numParticles; i++)
            {
                pi = tData[t]->particleList[i];

                p = &partArray[pi];

                if (PTYPE(p) != myPtype)
                    continue;

                if (!getValue (&carActivate) && IS_MARKED(p))
                {
                    closeOpenLinks (p, 0);
                }
                CLEAR_MARKED(p);
            }
        }
    }

    drawing = 0;
    drawingFirst = 0;
    firstParticle = lastParticle = NULL;
}

int connectedWith (particle *p, particle *pp)
{
    return isLinked(p, pp);
}

particle *findNearestPart (screeninfo *startSi, particle *p)
{
    int i;
    particle *pp;
    particle *rp = NULL;
    float minDist = MAX_SPEEDF * 2.0;
    float dist;
    screeninfo *pSi = startSi;

    for (i = -1; i < 8; i++)
    {
        for (pp = pSi->particleList; pp > SCR_BRICK; pp = pp->next)
        {
            if ((pp == p) || connectedWith(p, pp) || (openLinks(pp, getValue (&carConnectToPassive)) == 0))
                continue;

            if ((PTYPE(p) == PTYPE(pp)) || (connectToAll && (pdescs[PTYPE(pp)]->getRot)))
            {
                dist = VEC_LEN((p->pos.d.x - pp->pos.d.x), (p->pos.d.y - pp->pos.d.y));

                if (dist < minDist)
                {
                    minDist = dist;
                    rp = pp;
                }
            }
        }
        pSi = SI_DIR(startSi, i);
    }

    return rp;


}

void findBestPart (screeninfo *pSi, particle *p, int *minLinks, particle **lp)
{
    particle *pp;

    if (pSi->particleList <= SCR_BRICK)
    {
        if ((pSi->particleList == SCR_BRICK) && connectToWall)
            SET_STICKED(p);

        return;
    }

    for (pp = pSi->particleList; pp != NULL; pp = pp->next)
    {
        if ((pp == p) || connectedWith(p, pp) || (openLinks(pp, getValue (&carConnectToPassive)) == 0))
            continue;

        if ((PTYPE(p) == PTYPE(pp)) || (connectToAll && (pdescs[PTYPE(pp)]->getRot)))
        {
            *lp = pp;
            return;
        }
    }
}

static int chainSetType(particle *p)
{
    if (drawingFirst)
    {
        firstParticle = p;
    }

    p->pPtr = (void*)linkPartners (p);

    p->food = getValue(&carDflFood);

    drawingFirst = 0;

    unlinkAll (p);

    /* Link operations are not doen when loading from tape so that we don not interact
     * with the link records in the tape
     */
    if (!tapeOperation())
    {
        CLEAR_STICKED(p);

#if 0
        if (wimpType != -1)
        {
            vec_t pp = p->pos;
            pp.d.x += GRID;
            createParticleAsync (&pp, &p->vec, 0, p->mass, wimpType, p->size, p);
        }
#endif


        if (connectToPrevious && drawing)
        {
            if (lastParticle)
            {
                /* establish double link to previous particle */
                linkParticles (p, lastParticle, 0, DFL_LINK_DATA);
                linkParticles (p, lastParticle, 0, DFL_LINK_DATA);
            }
        }
        else if (connectToNeighbour && drawing)
        {
            screeninfo *pSi = GET_SCREENINFO(p);
            particle *pp = NULL;
            int minLinks = 8;
            int i;
            int sy = SCREENC(p->pos.d.y);

#if (MAX_LINKS == 6)
#if 0
            p->pos.d.y = INTERNC(sy) + GRID/2;
            p->pos.d.x = INTERNC(SCREENC(p->pos.d.x));

            if (sy & 1)
            {
                p->pos.d.x = (p->pos.d.x & ~MAX_SPEED) + GRID/3;
            }
            else
            {
                p->pos.d.x = (p->pos.d.x & ~MAX_SPEED) + ((GRID*2)/3);
            }
#endif

            for (i = 0; i < MAX_LINKS; i++)
            {
                if (openLinks(p, 0) == 0)
                    break;

                pp = NULL;
                minLinks = 3;

                if (sy & 1)
                {
                    findBestPart (SI_DIR(pSi, 0), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 1), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 2), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 3), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 4), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 6), p, &minLinks, &pp);
                }
                else
                {
                    findBestPart (SI_DIR(pSi, 1), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 3), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 4), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 5), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 6), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 7), p, &minLinks, &pp);
                }

                if (pp != NULL)
                {
                    linkParticles (p, pp, getValue (&carConnectToPassive), DFL_LINK_DATA);

                    /* mark particle so that we later can  close all remaining links */
                    SET_MARKED(p);
                    SET_MARKED(pp);
                }

            }
#else
            for (i = 0; i < MAX_LINKS; i++)
            {
                if (openLinks(p, 0) == 0)
                    break;

                pp = NULL;
                minLinks = 3;
                findBestPart (SI_DIR(pSi, 1), p, &minLinks, &pp);
                findBestPart (SI_DIR(pSi, 3), p, &minLinks, &pp);
                findBestPart (SI_DIR(pSi, 4), p, &minLinks, &pp);
                findBestPart (SI_DIR(pSi, 6), p, &minLinks, &pp);

                if (pp != NULL)
                {
                    linkParticles (p, pp, getValue (&carConnectToPassive), DFL_LINK_DATA);

                    /* mark particle so that we later can  close all remaining links */
                    SET_MARKED(p);
                    SET_MARKED(pp);
                }

            }
#endif
        }
        else
        {
            if (!getValue (&carActivate))
            {
                closeOpenLinks (p, 0);
            }
        }

        lastParticle = p;
    }

    return 1;
}

static void chainApplySetting (particle *p, setting *s)
{
    pLinks *pl, *pl2;
    int i,j;
    int val = getValue (s);

    if (s == &carActivate)
    {
        if (val)
            activateClosedLinks (p);
        else
            closeOpenLinks (p, 0);
    }
    else if (s == &carAttraction)
    {
        pl = linkPartners(p);

        for (i = 0; i < pl->num; i++)
        {
            SET_CHAIN_ATTRACT(pl->linkData[i], val);

            pl2 = linkPartners(pl->links[i]);

            for (j = 0; j < pl2->num; j++)
            {
                if (pl2->links[j] == p)
                {
                    SET_CHAIN_ATTRACT(pl2->linkData[j], val);
                }
            }
        }
    }
    else if (s == &carMaxDist)
    {
        pl = linkPartners(p);

        for (i = 0; i < pl->num; i++)
        {
            SET_MAX_DIST(pl->linkData[i], CURRENT_MAX_DIST);

            pl2 = linkPartners(pl->links[i]);

            for (j = 0; j < pl2->num; j++)
            {
                if (pl2->links[j] == p)
                {
                    SET_MAX_DIST(pl2->linkData[j], CURRENT_MAX_DIST);
                }
            }
        }
    }
    else if (s == &carNumLinks)
    {
        linkSetCount (p, getValue(s));
    }
}

static int chainColor (particle *p)
{
    if (openLinks(p, 0))
    {
        return 3;
    }
    return -1;
}

static int chainCanCreate (int x, int y)
{
    screeninfo *pSi;
    particle *p;

    /* if we are in "connect to other particles" mode, do not create chain particles
     * on pixels with other particles
     */
    if (!connectToAll)
        return 1;

    pSi = &siArray[x + y*wid];

    for (p = pSi->particleList; p > SCR_BRICK; p = p->next)
        if (PTYPE(p) !=  myPtype)
            return 0;
    
    return 1;
}

int chainPlacementHint(void)
{
    if (connectToNeighbour)
        return PHINT_FIXED1;
    else
        return PHINT_RANDOM;
}

static void chainWalk (particle *p, FUNCPTR cb, void *arg)
{
    walkLinks (p, cb, arg);
}

static void chainIter(int scrUpdate)
{
    if (scrUpdate)
    {
        ldToggle = ldToggle ^ LINK_MARKER;
    }
}

static screeninfo *chainRedraw (particle *p, screeninfo *oldSi, threadData *td, int off )
{
    pLinks *pl = (pLinks*)p->pPtr;
    particle *pp;
    int i;

    td->v.partCount++;

    if (!showLinks.val)
        return oldSi;

    for (i = 0; i < pl->num; i++)
    {
        pp = pl->links[i];

#ifndef USE_OPENGL
        mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y),
                             (IX_TO_SX(pp->pos.d.x) + IX_TO_SX(p->pos.d.x)) / 2, 
                             (IY_TO_SY(pp->pos.d.y) + IY_TO_SY(p->pos.d.y)) / 2, PCOLOR(p));
#else
        if (!LINKS_DRAWN(pp))
        {
            ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv);
        }
#endif
    }
#ifdef USE_OPENGL
    LINK_MARK(p);
#endif

    return oldSi;
}

static int getNumLinks (particle *p)
{
    return getValue(&carNumLinks);
}

pdesc MOD = {
    .moveParticle = moveExactRad,
    .name = "Carnivore",
    .descr = "",
    .init = (FUNCPTR)chainInit,
    .unsetType = (FUNCPTR)chainUnsetType,
    .startDraw = (FUNCPTR)chainDrawStart,
    .stopDraw = (FUNCPTR)chainDrawStop,
    .setType = (FUNCPTR)chainSetType,
    .applySetting = (FUNCPTR)chainApplySetting,
    .getColor = (FUNCPTR)chainColor,
    .redraw = (FUNCPTR)chainRedraw,
    .help = "chain",
    .pm = chain_pm,
    .canCreate = (FUNCPTR)chainCanCreate,
    .placementHint = (FUNCPTR)chainPlacementHint,
    .walkSiblings = (FUNCPTR)chainWalk,
    .iteration = (FUNCPTR)chainIter,
    .iterationFrozen = (FUNCPTR)chainIter,
    .numLinks = (FUNCPTR)getNumLinks,
    .isWimp = 0,
    .noSize = 1,

    .widgets = {
        {(FUNCPTR)slider, 32, 64, chain_attract2_pm , "Chain Attraction", 0, &carAttraction },
        {(FUNCPTR)slider, 32, 64, chain_maxdist_pm, "Max. Distance", 0, &carMaxDist },
        {(FUNCPTR)slider, 32, 64, NULL, "Start Food", 0, &carDflFood },
        {SAVE_MARKER},
//        {(FUNCPTR)onOffButtonS, 32, 32, connect_pm, "Draw connected", 0, &carConnectToPrevious },
//        {(FUNCPTR)onOffButtonS, 32, 32, connect_ring_pm, "Reconnect to first", 0, &carReconnectToFirst },
        {(FUNCPTR)onOffButtonS, 32, 32, connect_solid_pm, "Solid", 0, &carConnectToNeighbour },
        {(FUNCPTR)onOffButton, 32, 32, connect_blob_xpm, "Connect to blob", (intptr_t)&connectToAll, NULL },
        {(FUNCPTR)onOffButton, 32, 32, connect_wall_xpm, "Connect to wall", (intptr_t)&connectToWall, NULL },
#if (MAX_LINKS == 8)
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 17, l1_xpm, "1 Link", 1, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 17, l2_xpm, "2 Links", 2, &carNumLinks },
        {RESTORE_MARKER},
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 17, l3_xpm, "3 Links", 3, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 17, l4_xpm, "4 Links", 4, &carNumLinks },
        {RESTORE_MARKER},
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 17, l5_xpm, "5 Links", 5, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 17, l6_xpm, "6 Links", 6, &carNumLinks },
        {RESTORE_MARKER},
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 17, NULL, "7 Links", 7, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 17, NULL, "8 Links", 8, &carNumLinks },
        {RESTORE_MARKER},
#else
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 21, l1_xpm, "1 Link", 1, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 21, l2_xpm, "2 Links", 2, &carNumLinks },
        {RESTORE_MARKER},
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 21, l3_xpm, "3 Links", 3, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 21, l4_xpm, "4 Links", 4, &carNumLinks },
        {RESTORE_MARKER},
        {SAVE_MARKER},
        {(FUNCPTR)onOffGroupS, 22, 21, l5_xpm, "5 Links", 5, &carNumLinks },
        {(FUNCPTR)onOffGroupS, 22, 21, l6_xpm, "6 Links", 6, &carNumLinks },
        {RESTORE_MARKER},
#endif
        {RESTORE_MARKER},
        {(FUNCPTR)onOffButtonS, 32, 32, chain_activate_pm, "Activate/deactivate links", 0, &carActivate },
#ifdef CONNECT_PASSVE_LINKS
        {(FUNCPTR)onOffButtonS, 32, 32, chain_fconnect_pm, "Connect to inactive link", 0, &carConnectToPassive },
#endif
#ifdef COLL_LOSS
        {(FUNCPTR)collLossButtonSelect, 32, 64, NULL, "Collossion Loss" },
#endif
        }
};
