extern int ldToggle;

#define LINK_MARKER                PFLAG1_VAL
#define LINKS_DRAWN(P)                (PFLAG1(P) == ldToggle)
#define LINK_MARK(P)                if (ldToggle) {PFLAG1_SET(P);} else {PFLAG1_CLR(P);}

#define IS_MARKED(P)                PFLAG2(P)
#define SET_MARKED(P)                PFLAG2_SET(P)
#define CLEAR_MARKED(P)                PFLAG2_CLR(P)

#define IS_STICKED(P)                PFLAG3(P)
#define SET_STICKED(P)                PFLAG3_SET(P)
#define CLEAR_STICKED(P)        PFLAG3_CLR(P)

#define HAS_ACTPOT(P)                PFLAG4(P)
#define SET_ACTPOT(P)                PFLAG4_SET(P)
#define CLEAR_ACTPOT(P)         PFLAG4_CLR(P)

#define NO_LINK_BREAK        (MAX_SPEEDQ * 10000)

#define MAX_DIST(data)                (int)((data) & 0x7fff)
#define SET_MAX_DIST(data,d)        data = ((data) & 0xffff8000) | ((d) & 0x7fff)
#define CHAIN_ATTRACT(data)        (int)((data) >> 16)
#define SET_CHAIN_ATTRACT(data,d)        data = (data & 0x0000ffff) | ((d) << 16)
#define REPEL(data)                (data & 0x8000)
#define SET_REPEL(data,r)        data = ((data) & ~0x8000) | ((r) << 15)

#define CURRENT_MAX_DIST  ((getValue(&chainMaxDist) == chainMaxDist.maxValue) ? 0 : getValue(&chainMaxDist))

#define MK_LINK_DATA(maxdist,attract,ra)        (((maxdist) & 0x7fff) | ((attract) << 16) | ((ra << 15) & 0x8000))
#define DFL_LINK_DATA        MK_LINK_DATA(CURRENT_MAX_DIST, getValue(&chainAttraction), getValue(&repulseChained[chainPtype]))

extern int chConnectToPrevious;
extern int chConnectToNeighbour;
extern int chConnectToWall;
extern int chConnectToAll;
extern int chainPtype;

