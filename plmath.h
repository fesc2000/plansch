
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/* whether two vectors point to the same direction */
#define SAME_DIR(v1,v2)        ((((float)-(v1).d.y) * (float)(v2).d.y - (float)(v1).d.x * (float)(v2).d.x) < 0)
#define SAME_DIRD(v1x, v1y, v2x, v2y) ((-(v1y) * (v2y) - (v1x) * (v2x)) <= 0)
#define VEC_DOT_LL(v1,v2) ((int64_t)(-(v1).d.y) * (int64_t)(v2).d.y - (int64_t)(v1).d.x * (int64_t)(v2).d.x)

#define SIGN(v)        (((v) == 0) ? 0 : (((v) < 0) ? -1 : 1))

#define DEG2RAD(deg)        ((FLOAT)(deg) * M_PI / 180.0)
#define NORM_LEN        0x7f
#define NORM_LENQ        (NORM_LEN*NORM_LEN)
#define NORM_X(deg)        (cos(DEG2RAD(deg)) * (FLOAT)NORM_LEN)
#define NORM_Y(deg)        (-sin(DEG2RAD(deg)) * (FLOAT)NORM_LEN)

#define VEC_LEN(vx,vy) (sqrt((FLOAT)(vx)*(FLOAT)(vx)+(FLOAT)(vy)*(FLOAT)(vy)))

#define VDX(p1,p2)        ((p2).d.x -(p1).d.x)
#define VDY(p1,p2)        ((p2).d.y -(p1).d.y)
#define VDXQL(p1,p2)        (int64_t)(VDX(p1,p2) * VDX(p1,p2))
#define VDYQL(p1,p2)        (int64_t)(VDY(p1,p2) * VDY(p1,p2))

#define DISTQL(p1,p2)        (VDXQL(p1,p2) + VDYQL(p1,p2))
#define DIST(p1,p2)        (int)(VEC_LEN(VDX(p1,p2), VDY(p1,p2)))

#define LENQ(x,y)        ((x)*(x)+(y)*(y))
#define LENQ_64(x,y)        ((int64_t)(x)*(int64_t)(x)+(int64_t)(y)*(int64_t)(y))



#define RND(range)  (1 + (int) ((FLOAT)(range) * (rand() / (RAND_MAX + 1.0))))
#define FRND(range)  ((FLOAT)(range) * ((FLOAT)rand() / (FLOAT)(RAND_MAX)))

#if (RAND_MAX >= 0x7fffffff)
#define RND_MASK(bits) ((rand() & (0xffffffff << (31-bits))) >> (31-bits))
#else
#define RND_MASK(bits) ((rand() & (0xffffffff << (15-bits))) >> (15-bits))
#endif

/* Macro to perform preliminary check whether speed vector is below GRID (before checking
 * vector length).
 * No speed improve ...
 */
#if 0
#define SPEED_OK(v)        (((v).d.x < maxrad) && ((v).d.y < maxrad) && ((v).d.x > minrad) && ((v).d.y > minrad))
#else
#define SPEED_OK(v)        1
#endif

#define SCT_MUL                ((float)SIN_COS_TABLE_ENTRIES / (2.0*M_PI))
#define RAD2SCT(rad)        ((int)((rad) * SCT_MUL) + SIN_COS_TABLE_ENTRIES) % SIN_COS_TABLE_ENTRIES
#define TBL_NSIN(i)        scTable[j].ns
#define TBL_COS(i)        scTable[j].c
extern scTable_t scTable[SIN_COS_TABLE_ENTRIES];

extern uint16_t floatToHalf(float i);
