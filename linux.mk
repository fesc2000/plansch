##### Config options

SRCDIR =	$(shell pwd)


##### Optimization settings ######


## Default compiler flags (SSE2 is required)
#
CFLAGS = -O3 

ifneq ($(DEBUG),)
ifeq ($(DEBUG),1)
CFLAGS = -g
else
CFLAGS += -g
endif
endif

## CPU optimization
#CPU=core2
#CFLAGS += -march=$(CPU) -mtune=$(CPU)

# Depends on whether 32- or 64-bit is used. Required (e.g.) by OpenCL
#
#LIBARCH=x86
LIBARCH=$(shell uname -m)

###### Features ###########

# Use SSE2 code and FP math?
USE_SSE2=y

# use SSE4.1 code if CPU supports it?
USE_SSE41=y

# use SSE4.1 for FP math
USE_SSE41_MATH=n

# experimental
USE_AVX=n


## Variants to build
#
# default and opengl stuff should build on most systems.
# OpenCL requires OpenCl libs and headers (tested with ATI only)
#
#VARIANTS = sopengl
VARIANTS =	default opengl
VARIANTS +=	opencl

## MP support 
# Defining BIND_THREADS associates threads with specific CPUs
#
OMPFLAGS += -DOPENMP -fopenmp
OMPFLAGS += -DBIND_THREADS

## Display some statistics
#
# CFLAGS += -DSTATS 

## 
# multi-threading by dividing the draw area
CFLAGS += -DMT_BORDERS

# Multi-threading by using a dedicated thread for wimps and non-wimps (buggy)
# CFLAGS += -DMT_WIMPS

#
ifneq ($(CCSH_DIR),)
CFLAGS += -DINCLUDE_CCSH -I$(CCSH_DIR)/../..
LIBDIRS += -L$(CCSH_DIR)
LIBS += -lccsh -ldl -lreadline -lhistory
ifneq ($(DYNCALL_DIR),)
LIBS += -L$(DYNCALL_DIR) -ldyncall_s 
endif
endif


#CFLAGS += -DTTF_SUPPORT -DTTF_FONT=\"plansch.ttf\"
#LIBS += -lSDL_ttf


################## System specific settings ##################

## Compiler to use
#
ifeq ($(CC),)
CC = gcc
endif

ifeq ($(CPP),)
CPP = g++ -fpermissive
endif

# LLVM requires explicit link of pthread library and does not support
# OpenMP yet
#
ifeq ($(CC),/usr/bin/clang)
LIBS += -lpthread
OMPFLAGS =
CFLAGS += --analyze

LLVM=1
endif

ifeq ($(CC),clang)
LIBS += -lpthread
#OMPFLAGS =
#CFLAGS += --analyze

LLVM=1
endif

CFLAGS += $(OMPFLAGS)

## GLEW available?
#
#GLEW_LIBS=$(shell ls /usr/lib/libGLEW* 2>/dev/null)

ifeq ($(GLEW_LIBS),)
    OPENGL_LIBS = -lGL
else
    CFLAGS += -DUSE_GLEW -DOPENGL_GLEW
    OPENGL_LIBS = -lGLEW
endif

RM = rm -f

## Mingw support
#
#CFLAGS += -D_MINGW
#LIBS = -lmingw32 -lSDLmain -lSDL
#OPENGL_LIBS = -lopengl32

## OpenGL extension wrangler (glew)
# This might be required on some systems, esp. Windows. 
# You need to compile libglew32.a from scratch under mingw.
#
#CFLAGS += -DOPENGL_GLEW
#OPENGL_LIBS = -lopengl32 -lglew32

## Root path for SDL headers/libs (default is /usr)
#
#SDLROOT=c:\dev\SDL-1.2.14

## Additional Library search paths
#
#LIBDIRS += -L/usr/local/lib

## Include directories
#
#INCDIRS += -I/usr/local/include

exesuffix =

KERNEL	    = $(shell uname -s)
MACH	    = $(shell uname -m)

