
/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

char tmpStr[256];


#define CANSHARE


#define LINKED_PARTICLE(P)        (particle*)((P)->pPtr)
#define LINKED_PARTICLE_IDX(P)        PARTICLE_ID((particle*)((P)->pPtr))

#define ROT_DIR(P)                (P)->rot
#define ROT_DIR_SET(P,R)        (P)->rot = (R)

#define LINK_DIR(P)                (P)->dir
#define LINK_DIR_SET(P,R)        (P)->dir = (R)

#define IS_ANCHOR(P)                PFLAG1(P)
#define SET_ANCHOR(P)                PFLAG1_SET(P)
#define CLEAR_ANCHOR(P)                PFLAGS_CLR(P)

#define IS_LINKED(P)                PFLAG2(P)
#define SET_LINKED(P)                PFLAG2_SET(P)
#define CLEAR_LINKED(P)                PFLAG2_CLR(P)

#define ROT_STEP_BITS   8
#define ROT_STEPS           (1<<ROT_STEP_BITS)
#define ROT_STEPSF           256.0

#define DIR_TO_DEG(D) (((D) * 360.0)/ROT_STEPSF)
#define DEG_TO_DIR(D) (((D) * ROT_STEPSF)/360.0)
#define ROTDIR_TO_DEG(R) (((float)(R)*360.0)/(float)ROT_STEPS)
#define DEG_TO_ROTDIR(D) (((float)(D)*(float)ROT_STEPS)/360.0)

#define ROT_DIR_NEQUAL(P,PP)        ((P)->rot != (PP)->rot)
#define ROT_DIR_EQUAL(P,PP)        ((P)->rot == (PP)->rot)
#define ROT_DIR_COPY(P,PP)        (P)->rot = (PP)->rot;


static vec_t cp[ROT_STEPS];

int connectToPrevious = 0;
int connectToWall = 1;
int connectToSameConnected = 1;
int connectToAll = 0;
int drawing = 0;
int drawingFirst = 0;
int dflDir = 0;;
particle *firstParticle = NULL;
particle *lastParticle = NULL;

static int thisType;

void connectHandler (setting *s);

SETTING(sConnectToPrevious) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect DLA particles while drawing",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(sReconnectToFirst) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect DLA particle while drawing (ring)",
    .cb = (FUNCPTR)connectHandler,
};


SETTING(dlaRotation) =
{
    .minValue = -10,
    .maxValue = 10,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 1,
    .name = "Rotate object"
};

void connectHandler (setting *s)
{
    connectToPrevious = 0;

    if ((s == &sConnectToPrevious) && getValue(s))
    {
        connectToPrevious = 1;

        modifySetting (&sReconnectToFirst, 0, 0);
    }
}

void dlaUnlinkFrom (particle *p)
{
    int ppIndex;
    int pt = PTYPE(p);
    int i, t;
    particle *pp;

    /* search all particles which may link to this one and remove the link
     */
    for (t = 0; t < options->numThreads; t++)
    {
        for (i = 0; i < tData[t]->numParticles; i++)
        {
            ppIndex = tData[t]->particleList[i];
            pp = &partArray[ppIndex];

            if (PTYPE(pp) != pt)
                continue;

            if (IS_LINKED(pp) && (LINKED_PARTICLE(pp) == p))
            {
                CLEAR_LINKED(pp);
                CLEAR_ANCHOR(p);
                p->pPtr = NULL;
            }
        }
    }
}

void dlaUnsetType (particle *p)
{
    dlaUnlinkFrom (p);
    p->pPtr = NULL;
    CLEAR_LINKED(p);
    CLEAR_ANCHOR(p);
}

void dlaInit (int t)
{
    float a;
    int i;

    thisType = t;

    for (i = 0; i < ROT_STEPS; i++)
    {
        a = (((float)i * (360.0 / ROT_STEPSF)) / 360.0 ) * 2 * M_PI;

        cp[i].d.x = (int)(cos(a) * (float)(GRID));
        cp[i].d.y = (int)(-sin(a) * (float)(GRID));
    }
}

int dlaDir (int dx, int dy)
{
    float r;

    r = atan2 (-dy, dx);
    if (r < 0)
        r += 2 * M_PI;

    return (r / (2 * M_PI)) * (ROT_STEPSF) ;


}

static const int dlaRotDir (const particle * p)
{
    return ROTDIR_TO_DEG(ROT_DIR(p));
}

void dlaLink (particle *p, particle *pp)
{
    int absd, dir;
    float r;
    int ppRot = 0;

    if (PTYPE(p) == PTYPE(pp))
        ppRot = ROT_DIR(pp);
    else if (pdescs[PTYPE(pp)]->getRot)
        ppRot = DEG_TO_ROTDIR (pdescs[PTYPE(pp)]->getRot (pp));

    r = atan2 (-(p->pos.d.y - pp->pos.d.y), (p->pos.d.x - pp->pos.d.x));
    if (r < 0)
        r += 2 * M_PI;

    absd = ((r / (2 * M_PI))) * (ROT_STEPSF);

    dir = ((absd + ROT_STEPS) - ppRot) & (ROT_STEPS-1);

    LINK_DIR_SET(p, dir);
    ROT_DIR_SET(p, ppRot);
    p->pPtr = pp;
    SET_LINKED(p);

//    if (pp->mass > 1)
        p->mass = pp->mass-1;
//    p->vec.d.x = p->vec.d.y = 0;
}

screeninfo *
moveDla (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 )
{
    vec_t screenPos;
    vec_t screenPosOld;
    vec_t screenPosDiff;
    vec_t newPos;
    vec_t normVec;
    particle *pp, *tpp;
    screeninfo *newSi;
    int cx, cy;
    int i;
    int dir;
    FLOAT vec_len;
    vec_t ttvec, tvec;
    uint64_t spq;
    uint64_t minSpq;
#if 0
    uint64_t minPpDistQ;
#endif
    screeninfo *tmpSi;
    unsigned int pRad, ppRad;
    int        px, py;
    vec_t linkDst;

    PART_GRAV(p,oldSi);
    V_TO_SCREEN(screenPosOld, p->pos);

    if (oldSi->flags & SI_LINE)
    {
        pHitLine (p, &p->vec, off, td, oldSi);

        if (!IS_LINKED(p) && connectToWall)
        {
            SET_ANCHOR(p);
            SET_LINKED(p);
            ROT_DIR_SET(p, RND_MASK(ROT_STEP_BITS));
    
            p->vec.d.x = p->vec.d.y = 0;

            return oldSi;
        }
    }


    if (IS_LINKED(p))
    {
        if (IS_ANCHOR(p))
        {
            screeninfo *pSi;
            for (i = 0; i < 8; i++)
            {
                pSi = SI_DIR(oldSi, i);

                if ((pSi->particleList == SCR_BRICK) || 
                    (pSi->flags & SI_LINE))
                {
                    p->vec.d.x = p->vec.d.y = 0;
                    return oldSi;
                }
            }
            CLEAR_ANCHOR(p);
        }

        pp = LINKED_PARTICLE(p);

#ifndef USE_OPENGL
        if (GETVAL32(showLinks) && (zoom > 2))
        {
            mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y),
                        IX_TO_SX(pp->pos.d.x), IY_TO_SY(pp->pos.d.y), ICOLOR(p).col32);
        }
#else
        if (GETVAL32(showLinks))
        {
            ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv );
        }
#endif

        if (PTYPE(p) == PTYPE(pp))
        {
            if (ROT_DIR_NEQUAL(p, pp))
            {
                ROT_DIR_COPY(p, pp)
            }
        }
        else if (pdescs[PTYPE(pp)]->getRot)
        {
            int ppRot = DEG_TO_DIR (pdescs[PTYPE(pp)]->getRot (pp));
            
            ROT_DIR_SET(p, ppRot);
        }

        dir = (LINK_DIR(p) + ROT_DIR(p)) & (ROT_STEPS-1);

        linkDst.d.x = pp->pos.d.x + cp[dir].d.x;
        linkDst.d.y = pp->pos.d.y + cp[dir].d.y;

        ttvec.d.x = (linkDst.d.x - p->pos.d.x)/20;
        ttvec.d.y = (linkDst.d.y - p->pos.d.y)/20;

        spq = (int64_t)ttvec.d.x * (int64_t)ttvec.d.x + (int64_t)ttvec.d.y * (int64_t)ttvec.d.y;
#if 1
        if (spq >= MAX_SPEEDQ)
        {
            vec_len = sqrt(spq);
            ttvec.d.x = (((FLOAT)ttvec.d.x * MAX_SPEEDF)) / vec_len;
            ttvec.d.y = (((FLOAT)ttvec.d.y * MAX_SPEEDF)) / vec_len;
        }
        else
        {
            mergeParticleColors (p, pp);
        }
#endif

//        p->vec.d.x = (p->vec.d.x * 95) / 100 + ttvec.d.x;
//        p->vec.d.y = (p->vec.d.y * 95) / 100 + ttvec.d.y;
        p->vec.d.x += ttvec.d.x;
        p->vec.d.y += ttvec.d.y;

        pdescs[PTYPE(pp)]->hit (pp, (int)-ttvec.d.x, (int)-ttvec.d.y);
    }

    {
        tvec = p->vec;

        spq = (int64_t)tvec.d.x * (int64_t)tvec.d.x + (int64_t)tvec.d.y * (int64_t)tvec.d.y;
        if (spq >= MAX_SPEEDQ)
        {
            vec_len = sqrt(spq);
            tvec.d.x = (((FLOAT)tvec.d.x * MAX_SPEEDF)) / vec_len;
            tvec.d.y = (((FLOAT)tvec.d.y * MAX_SPEEDF)) / vec_len;

            lostImpuls += vec_len - MAX_SPEEDF;

        }
    }


    V_ADD(newPos, p->pos, tvec);

    V_TO_SCREEN(screenPos, newPos);
    V_SUB(screenPosDiff, screenPos, screenPosOld);

    dir = QUAD2DIR((screenPosDiff.d.x + 1) * 3 + screenPosDiff.d.y + 1);

    td->v.partCount++;
    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

    if (newSi->particleList == SCR_BRICK)
    {
        FLOAT cxf, cyf, vpf;

#ifdef FREEZE
        if (freeze)
        {
            if (spq < freeze)
            {
                p->vec.d.x = p->vec.d.y = 0;
                return oldSi;
            }
        }
#endif

        td->v.wallHit++;
        
        /* wall hit */

        if (!IS_LINKED(p) && connectToWall)
        {
            SET_ANCHOR(p);
            SET_LINKED(p);
            ROT_DIR_SET(p, RND_MASK(ROT_STEP_BITS));
    
            p->vec.d.x = p->vec.d.y = 0;

            return oldSi;
        }

                
        /* get normal vector of current screen position (direction based) 
         *
         * This is fixed-point arithmetic, except vector length calculation.
         * Normals are stored in NORM_LEN units (+-127).
         *
         */
        /* normal vector in normVec */
        normVec.d.x = oldSi->normals[dir * 2];
        normVec.d.y = oldSi->normals[dir * 2 + 1];
        cxf = tvec.d.x;
        cyf = tvec.d.y;

        /* normalize */
        vec_len = VEC_LEN (cxf, cyf);

        cxf = (cxf * NORM_LEN) / vec_len;
        cyf = (cyf * NORM_LEN) / vec_len;

        /* reflect at normal vector */
        /*        reflect (&cx, &cy, normVec.d.x, normVec.d.y);*/
        vpf = ((cxf * normVec.d.x) + (cyf * normVec.d.y));
        cxf = cxf - (2 * normVec.d.x * vpf) / (NORM_LEN*NORM_LEN);
        cyf = cyf - (2 * normVec.d.y * vpf) / (NORM_LEN*NORM_LEN);

        /* handle energy transfer between particle and wall.  */
#if 0
        {
        FLOAT e = ((vec_len / GRID) + ((FLOAT)TEMPR(newSi) / MAX_TEMP)) / 2.0;
        unsigned int t;
        vec_len = (e * GRID) * wallDamping;

        t = (e / wallDamping) * MAX_TEMP;
        if (t < 255) 
            TEMPR(newSi) = t;
        else
            TEMPR(newSi) = 255;
//        SCR_SET(screenPos.d.x, screenPos.d.y, TEMPR_COL(TEMPR(newSi)));

        }
#endif

        if (GETVAL32(wallTempr))
        {
            int t = (FLOAT)((vec_len - TEMPR(newSi))/2) * wallDamping;

            TEMPR(newSi) += t;

            if (t > 0.0)
            {
                vec_len -= t;

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));
            }
            else
            {
                vec_t tmpvec;

                frandomize (cxf, cyf, -t, &tmpvec);

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

                p->vec.d.x += tmpvec.d.x;
                p->vec.d.y += tmpvec.d.y;

            }
            /* Make sure perticle does not get too hot */
            spq = (QUAD_TYPE)p->vec.d.x * (QUAD_TYPE)p->vec.d.x + (QUAD_TYPE)p->vec.d.y * (QUAD_TYPE)p->vec.d.y;
            if (spq >= MAX_SPEEDQ)
            {
                vec_len = sqrt(spq);
                p->vec.d.x = (((FLOAT)p->vec.d.x * MAX_SPEEDF)) / vec_len;
                p->vec.d.y = (((FLOAT)p->vec.d.y * MAX_SPEEDF)) / vec_len;
            }
        }
        else
        {

            /* apply original impuls to reflection vector */

            p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf * wallDamping) / (FLOAT)(NORM_LEN));
            p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf * wallDamping) / (FLOAT)(NORM_LEN));
        }
        


        /* Check pixel in new direction. */

        newPos.d.x = p->pos.d.x + p->vec.d.x;
        newPos.d.y = p->pos.d.y + p->vec.d.y;
        V_TO_SCREEN(screenPos, newPos);
        if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
        {
#if 1
            /* pixel in new direction is blocked. May happen if hitting
             * a wall pixel at a slope.
             * Set direction equal to normal vector, retaining the particle
             * speed. There shouldn't be a pixel in this direction.
             * This is not ideal for slope collisions, but prevents
             * paricle warping.
             */
            vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

            p->vec.d.x = (normVec.d.x * vec_len) / NORM_LEN;
            p->vec.d.y = (normVec.d.y * vec_len) / NORM_LEN;
            newPos = p->pos;
            V_TO_SCREEN(screenPos, newPos);
#else
            /* warp particle to adjacent free pixel or stay put. Makes collisions
             * at slopes more realistic, but warping creates unrealistic motion effects
             * if there is a particle at the destination pixel.
             */
            newPos.d.x += (normVec.d.x * GRID) / (NORM_LEN);
            newPos.d.y += (normVec.d.y * GRID) / (NORM_LEN);
            V_TO_SCREEN(screenPos, newPos);

            if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
            {
                p->vec.d.x = -p->vec.d.x;
                p->vec.d.y = -p->vec.d.y;
                newPos = p->pos;
                V_TO_SCREEN(screenPos, newPos);
            }
#endif
        }

        newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];
        goto move;
        
    }

    pp = NULL;
    minSpq = 0xffffffffffffffffULL;
    px = py = 0;
    pRad = PART_RADIUS(p);

    tmpSi = oldSi;
    for (i = -1; i < 8;)
    {
        if (tmpSi->particleList <= SCR_BRICK)
        {
            i++;
            tmpSi = SI_DIR(oldSi, i);
            continue;
        }
        
        for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
        {
            uint64_t ppDistQ;

            _mm_prefetch((void*)tpp->next, _MM_HINT_T0);

            if (tpp == p)
                continue;

#if 0
            if ((IS_LINKED(p) && (LINKED_PARTICLE(p) == tpp)))
                continue;

            if (PTYPE(tpp) == PTYPE(p))
            {
                if (IS_LINKED(p) && IS_LINKED(tpp) && ROT_DIR_EQUAL(p,tpp))
                {
                    continue;
                }
            }
#endif

            ppRad = PART_RADIUS(tpp);

            ppDistQ = (pRad + ppRad) * (pRad + ppRad);

            cx = tpp->pos.d.x - newPos.d.x;
            cy = tpp->pos.d.y - newPos.d.y;

            spq = (uint64_t)cx * (uint64_t)cx + (uint64_t )cy * (uint64_t)cy;

            if (spq < ppDistQ)
            {
                if (spq < minSpq)
                {
                    minSpq = spq;
#if 0
                    minPpDistQ = ppDistQ;
#endif
                    pp = tpp;
                }
            }
#if 0
            else if (particleAttraction)
            {
                px += cx;
                py += cy;
            }
#endif
        }
        i++;
        tmpSi = SI_DIR(oldSi, i);
    }

    p->vec.d.x += px/5000;
    p->vec.d.y += py/5000;

    if (pp == NULL)
    {
        if (newSi != oldSi)
            goto move;
        
        p->pos = newPos;

        return oldSi;
    }

    if (!IS_LINKED(p)) 
    {
        if (!connectToAll)
        {
            if ((PTYPE(p) == PTYPE(pp)) && IS_LINKED(pp))
                {
                dlaLink (p, pp);
            }
        }
        else
        {
            /* connect to dla particle if linked or other particle if it provides
             * a rotation
             */
            if (((PTYPE(p) == PTYPE(pp)) && IS_LINKED(pp)) ||
                ((PTYPE(p) != PTYPE(pp)) && (pdescs[PTYPE(pp)]->getRot)))
            {
                dlaLink (p, pp);
            }
        }
    }

    ppRad = PART_RADIUS(pp);

#if 0
    if ((minSpq < (minPpDistQ * 50)/100) && (PTYPE(p) == PTYPE(pp)))
    {
        dlaLink (pp, p);
    }
#endif

    if (minSpq > 0.0)
    {
        FLOAT dlen;
        FLOAT dx, dy;

        FLOAT diffy = pp->pos.d.y - newPos.d.y;
        FLOAT diffx = pp->pos.d.x - newPos.d.x;
//        FLOAT impx1, impy1, impx2, impy2;
//        FLOAT frc;
        int off;

        dlen = minSpq;

        /* We need the relative mass difference of the two involved particles.
         * wp is in the range of 0..1, 0.5 for the case that the particles are of same
         * mass.
         */
        FLOAT wp = (FLOAT)(pp->mass) / (FLOAT)(p->mass + pp->mass);

        dlen = sqrt(dlen) + 0.5;

        dx = (FLOAT)diffx / dlen;
        dy = (FLOAT)diffy / dlen;

        td->v.collissions++;

        cx = pp->pos.d.x - p->pos.d.x;
        cy = pp->pos.d.y - p->pos.d.y;

        /* check whether the two particles move away from each other or approach eath other
         */
        if ((-cy)*(tvec.d.y - pp->vec.d.y) - (cx)*(tvec.d.x - pp->vec.d.x) < 0)
        {
            /* particle are approaching each other: do a collisison
             * In the next iteration we will probably do the "else" path (move particles further
             * away from each other if they are still within their collission area).
            FLOAT wf;
             */

//            if ((IS_LINKED(p) && (LINKED_PARTICLE(p) == pp)))
            {
                p->vec.d.x = (p->vec.d.x * p->mass + pp->vec.d.x * pp->mass) / ((int)p->mass + (int)pp->mass);
                p->vec.d.y = (p->vec.d.y * p->mass + pp->vec.d.y * pp->mass) / ((int)p->mass + (int)pp->mass);

                ACCEL_TO(pp, p->vec.d.x, p->vec.d.y);
            }
            
#if 0
            if (GETVAL32(preassure))
            {
                wf = ((FLOAT)(pRad + ppRad) / (dlen)) * ((float)GETVAL32(preassure)/1000.0);
            }
            else
            {
                wf = 0.0;
            }
#endif
                

            /* XXX immedeately do the "squeeze-out", 
             */
            off = (((dlen - (pRad + ppRad))) * wp ) / 8;

            newPos.d.x += (dx) * off;
            newPos.d.y += (dy) * off;

            screenPos.d.x = SCREENC (newPos.d.x);
            screenPos.d.y = SCREENC (newPos.d.y);

#if 0
            frc = (tvec.d.x * dx + tvec.d.y * dy);
            impx1 = frc * dx;
            impy1 = frc * dy;

            dx = -dx;
            dy = -dy;

            frc = (pp->vec.d.x * dx + pp->vec.d.y * dy);
            impx2 = frc * dx;
            impy2 = frc * dy;

            /* adjust both particle's vectors, based on
             * - The impuls resulting from the collission, adjusted by the relative weight
             *   of the two particles
             * - Constant collLoss, which can be adjusted so that particles lose energy
             *   when colliding.
             * - "push-out" direction times 1/(relative weight) times perticle preassure:
             *   Heavy particle gets less impuls from light particle and vice versa.
             */
            wp *= 2;

            p->vec.d.x = (FLOAT)(p->vec.d.x + ((impx2 - impx1)*wp)) + dx * wf * wp;
            p->vec.d.y = (FLOAT)(p->vec.d.y + ((impy2 - impy1)*wp)) + dy * wf * wp;

            wp = 2 - wp;

            pdescs[PTYPE(pp)]->hit (pp, (int)(((impx1 - impx2)*wp) - dx * wf * wp), (int)(((impy1 - impy2)*wp) - dy * wf * wp));
#endif
        }
#if 0
        else if (IS_LINKED(p))
        {
            newPos.d.x += (linkDst.d.x - newPos.d.x)/2;
            newPos.d.y += (linkDst.d.y - newPos.d.y)/2; 

            screenPos.d.x = SCREENC (newPos.d.x);
            screenPos.d.y = SCREENC (newPos.d.y);

        }
#endif
        else
        {
            /* particles move away from each other:
             * move particles in the direction of the collisison point, but dont actually do a collission
             */
            FLOAT wf;
            if (GETVAL32(preassure))
            {
                wf = ((FLOAT)(pRad + ppRad) / (dlen)) * ((float)GETVAL32(preassure)/1000.0) * wp;
            }
            else
            {
                wf = 0.0;
            }

            /* when two particles collide, the distance will temporarily be smaller than allowed (i.e. one
             * particle is inside the other. off is the distance we need to move the particle
             * "out" so that it is exatly at the other particle's border.
             *
             * Divide by 8 so that the particle is "pushed out" gradually.
             * Multiply by wp so that (relativly) heavy particles don't get pushed out as fast.
             */
            off = (((dlen - (pRad + ppRad))) * wp ) ;

            /* push out the particle without changing the impuls */
            newPos.d.x += dx * off;
            newPos.d.y += dy * off;

            screenPos.d.x = SCREENC (newPos.d.x);
            screenPos.d.y = SCREENC (newPos.d.y);

            wp *= 2;
            p->vec.d.x += dx * wf * wp;
            p->vec.d.y += dy * wf * wp;

            wp = 2 - wp;

            pdescs[PTYPE(pp)]->hit (pp, (int)(-dx * wf * wp), (int)(-dy * wf * wp));
        }
    }

    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

    if (newSi->particleList == SCR_BRICK)
    {
        return oldSi;
    }
    
move:
#include "do_move.c"
}


void dlaDrawStart()
{
    drawing = 1;
    drawingFirst = 1;
    dflDir++;
}

void dlaDrawStop()
{
    if (connectToPrevious && getValue(&sReconnectToFirst) && firstParticle && lastParticle && (lastParticle != firstParticle))
    {
        dlaLink (firstParticle, lastParticle);
    }
    drawing = 0;
    drawingFirst = 0;
    firstParticle = lastParticle = NULL;
}

int dlaSetType(particle *p)
{
    if (drawingFirst)
    {
        firstParticle = p;
        ROT_DIR_SET (p, dflDir);
    }

    drawingFirst = 0;

    CLEAR_LINKED(p);

    if (connectToPrevious && drawing)
    {
        if (lastParticle)
        {
            dlaLink (p, lastParticle);
        }
    }

    lastParticle = p;

    return 1;
}

void dlaApplySetting (particle *p, setting *s)
{
    int r;
    particle *sp, *pp;
    int c = 0;;

    if (s != &dlaRotation)
        return;

    sp = p;

    /* marker for circular dependency 
     */
    PFLAG3_SET(sp);

    /* find root particle. We set the new rotation there since it will be propagated to
     * all children
     */
    while (!IS_ANCHOR(p) && IS_LINKED(p))
    {
        pp = LINKED_PARTICLE(p);

        if (PFLAG3(pp))
        {
            printf ("loop\n");
            break;
        }

        p = pp;

        if (c++ > numParticles)
        {
            printf ("dlaConvert: ENDLESS LOOP\n");
            break;
        }
    }

    PFLAG3_CLR(sp);

    r = ROT_DIR(p);

    r = (r + getValue(&dlaRotation) + ROT_STEPS) & (ROT_STEPS-1);

    ROT_DIR_SET(p, r);
}

static FT_TYPE *isMarked = NULL;

static int walkToRoot (particle *p, int doMark, FUNCPTR cb, void *arg)
{
    int pType = PTYPE(p);
    particle *sp = p;

    while (1)
    {
        if (doMark)
        {
            /* mark this particle and invoke callback
            */
            FLAG_SET(isMarked, p);
            cb (p, arg);
        }

        /* reached the root of the tree without finding a marked particle
         */
        if (!IS_LINKED(p) || IS_ANCHOR(p))
            return 0;

        p = LINKED_PARTICLE(p);

        /* circular dependency: like reaching the root
        */
        if (p == sp)
            return 0;

        /* particle is not a dla type (e.g. a blob): like reaching root
        */
        if (PTYPE(p) != pType)
            return 0;

        /* reached marked particle? If yes, we can stop here, the remaining
         * particles up to the root are already marked.
         */
        if (FLAG_ISSET(isMarked, p))
            return 1;

    }
}

/* Invoke cb() for all particles of the tree where p is part of. Since a particle
 * does not know it's branches, we need to check all particles whether
 * they are part of the tree. For this, we walk up to the root
 * for the particle to be checked and mark these particles.
 * Then we check all particles whether they are dla types, and for
 * each one also walk up to the root, checking whether we hit a marked
 * particle. If yes, we do the walk again and also mark each particle.
 * For each particle we mark, we also invloke the provided callback function.
 *
 * This approach requires only a single pass through all particles and
 * should therefore be fairly fast.
 */
static void dlaWalk (particle *p, FUNCPTR cb, void *arg)
{
    int thread;
    int i;

    if (isMarked == NULL)
        isMarked = flagFieldAlloc();

    if (!isMarked)
    {
        fprintf (stderr, "dlaWalk: failed to allocate flag field\n");
        return;
    }

    FLAG_CLRALL(isMarked);

    walkToRoot (p, 1, cb, arg);

    for (thread = 0; thread < options->numThreads; thread++)
    {
        for (i = 0; i < tData[thread]->numParticles; i++)
        {
            p = &partArray[tData[thread]->particleList[i]];

            if (PTYPE(p) != thisType)
                continue;
        
            if (FLAG_ISSET(isMarked, p))
                continue;
                
            if (walkToRoot(p, 0, NULL, NULL))
            {
                walkToRoot (p, 1, cb, arg);
            }
        }
    }
}

static screeninfo *dlaRedraw (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *pa2 )
{
    particle *pp;

    td->v.partCount++;

    if (!GETVAL32(showLinks))
        return oldSi;

    if (!IS_LINKED(p) || IS_ANCHOR(p))
        return oldSi;

    pp = LINKED_PARTICLE(p);

    if (pp)
            ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv );

    return oldSi;
}

static int getNumLinks (particle *p)
{
    return MAX_LINKS;
}

pdesc MOD = {
    .moveParticle = moveDla,
    .name = "Sticky",
    .descr = "",
    .init = (FUNCPTR)dlaInit,
    .unsetType = (FUNCPTR)dlaUnsetType,
    .startDraw = (FUNCPTR)dlaDrawStart,
    .stopDraw = (FUNCPTR)dlaDrawStop,
    .setType = (FUNCPTR)dlaSetType,
    .getRot = (FUNCPTR)dlaRotDir,
    .applySetting = (FUNCPTR)dlaApplySetting,
    .walkSiblings = (FUNCPTR)dlaWalk,
    .redraw = (FUNCPTR)dlaRedraw,
    .numLinks = (FUNCPTR)getNumLinks,
    .help = "dla",
    .isWimp = 0,

    .pm = dla_pm,

    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, psize_pm, "Particle Size", 0, &particleSize[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},

            {(FUNCPTR)onOffButtonS, 32, 32, connect_wimp_xpm, "Pair_with_WIMP", (intptr_t)0, &wimpPair[0], B_NEXTRC },
        {(FUNCPTR)onOffButtonS, 32, 32, connect_pm, "Draw connected", (intptr_t)0, &sConnectToPrevious, B_NEXTRC },
//        {(FUNCPTR)onOffButtonS, 32, 32, connect_ring_pm, "Reconnect to first", 0, &sReconnectToFirst, B_NEXTRC },
        {(FUNCPTR)onOffButton, 32, 32, connect_blob_pm, "connectToAll", (intptr_t)&connectToAll, NULL, B_NEXTRC },
        {(FUNCPTR)onOffButton, 32, 32, connect_wall_pm, "connectToWall", (intptr_t)&connectToWall, NULL, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, rotate_pm, "Rotate Selection", 0, &dlaRotation, B_NEXTRC },
        }
};
