/*
Copyright (C) 2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "clprivate.clh"

/* collide two particles, return the force vector
 */
float2 collide (float2 posA,
                float2 velA,
                float2 posB,
                float2 velB,
                float lDiff,
                constant clParameters_t *params,
                int *collided
                )
{
    float2        relPos1, relPos2;
    float        dist;
    float        springFactor;
    float2        norm, relVel, tanVel;

    relPos1 = posB - posA;
    relPos2 = (posB + velB) - (posA + velA);
    relVel = velB - velA;

    dist  = relPos1.x * relPos1.x + relPos1.y * relPos1.y;

    if ((dist < params->collideDist) && (dist > MINDIST))
    {
        *collided = 1;
        dist = SQRT (dist);
        norm = relPos1 / dist;

        springFactor = -params->spring * (params->collideDist - dist);

#if 1
        if ((relPos1.y*relVel.y + relPos1.x*relVel.x) < 0.0f)
        {
            float frc1 = dot(velA, norm);
            float2 i1 = frc1 * norm;
            float frc2 = dot(velB, (-norm));
            float2 i2 = frc2 * (-norm);

            return (i2-i1) + params->damping * relVel + params->attraction * relPos1;
        }
#endif

        return springFactor * norm + params->attraction * relPos1;
    }

    *collided = 0;
    return (float2)(0.0f, 0.0f);

    dist = relPos2.x * relPos2.x + relPos2.y * relPos2.y;

    if ((dist < params->collideDist) && (dist > MINDIST))
    {
        *collided = 1;

        /* Particles will collide in next iteration
        dist = SQRT (dist);
        norm = relPos2 / dist;
         */
        dist = length (relPos1);
        norm = relPos1 / dist;

#if 1
//        if ((relPos1.y*relVel.y + relPos1.x*relVel.x) < 0.0f)
        {
            float frc1 = dot(velA, norm);
            float2 i1 = frc1 * norm;
            float frc2 = dot(velB, (-norm));
            float2 i2 = frc2 * (-norm);

            return (i2-i1) + params->damping * relVel + params->attraction * relPos1;
        }
#endif

        tanVel = relVel - (norm * dot(relVel, norm));
        springFactor = -params->spring * (params->collideDist - dist);

        return springFactor * norm + params->damping * relVel + params->shear * tanVel + params->attraction * relPos1;
    }

    *collided = 0;
    return (float2)(0.0f, 0.0f);
}

/*
 * To make makefile.opencl find the right kernel name:

KERNEL_F2(doCollideNonElastic) ()

*/


#define KERNEL_NAME doCollideNonElastic
#include "collide.cl"
