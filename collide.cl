/*
Copyright (C) 2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#define LUMINOSITY(c)        (c.x * 0.2126f + c.y * 0.7152f + c.z * 0.0722f)

#define NLINK

/* check if two particles will collide, and calculate dist^2 and relative position
 * Check position for next iteration first, then the current positions
 */
int willCollide (float2 posA,
                global float2 *velA,
                global float2 *posB,
                global float2 *velB,
                float collideDistQ,
                float2 *relPos,
                float *distq)
{
    *relPos = (*posB + *velB) - (posA + *velA);
    *distq = MAD((*relPos).x, (*relPos).x, (*relPos).y * (*relPos).y);

    if ((*distq <= collideDistQ) && (*distq > MINDIST))
    {
        /* will collide in next iteration
         */
        return 1;
    }


    *relPos = *posB - posA;
    *distq = MAD((*relPos).x, (*relPos).x, (*relPos).y * (*relPos).y);

    if ((*distq <= collideDistQ) && (*distq > MINDIST))
    {
        /* will collide in current iteration
         */
        return 1;
    }

    /* will not collide
     */
    return 0;
}

/* perform particle collission.
 *
 * Apply collission with all particles on particle's pixel and the 8 sorrounding pixels
 */
__kernel void KERNEL_NAME (global int *start, 
        global int *end,
        global uint *sortedId,
        global float2 *Pos,
        global float2 *Vec,
        global float2 *newVec,
        global attr_t *attr,
        global float4 *colors,
        global float4 *newCol,
        global uint8  *conn,
        constant clParameters_t *params,
        global clStatus_t *status
        )
{
    uint    off;
    float2  frc = (float2)(0.f, 0.f);
    float2  pos, vec;
    uint2   ipos;
    uint    pid;
    int            o1, o2, o3, i, i2;
    uint    nCollided;
    float4  cCol;
    attr_t  attributes;
    float   luminosity;
#ifdef PARTICLE_LINK
#ifdef NLINK
    int            newLinks, maxLinks;
    global uint *cp;
#else
    int oldLinks;
    uint8 cp;
#endif
#endif
    float2  relPos;
    float   distq;

    attr_t  oattr;
    float   wa;
    float4  maxcol;
    float   lumDiff;
    
#if 1
    pid = sortedId[GID0];
    attributes = attr[pid];
    if (pid == 0)
    {
        return;
    }
#else
    pid = GID0;
    attributes = attr[pid];
    if ((attributes.x & CL_TYPE_MOVING) == 0)
        return;
#endif


#ifdef CL_BOUNDARY_CHECK
    if (!(pid<get_global_size(0)))
    {
#ifndef BD_SILENT
        printf ("pid=%d gs=%d",pid, get_global_size(0));
#endif
        return;
    }
#endif

    pos = Pos[pid];
    vec = Vec[pid];

    ipos = convert_uint2_rtz (pos);

    off = IMAD(ipos.y, (uint)SCR_WID, ipos.x);

#ifdef PARTICLE_LINK
#ifdef NLINK
    cp = (global uint*)&conn[pid];
    newLinks = cp[6];
    maxLinks = cp[7];
#else
    cp = conn[pid];
    oldLinks = cp.s6;
#endif
#endif
    
    
    /* remember luminosity of particle in case the luminosity difference
     * shall influence collission attributes
     */
    if ((params->flags & CL_EFLAG_LUMDIFF) != 0)
        luminosity = LUMINOSITY(colors[pid]);

#ifdef CL_BOUNDARY_CHECK
    if (off >= MAX_SCR_OFFSET)
    {
            status->boundaryCheckErrors++;
            newVec[pid] = v;
            return;
    }
#endif


    nCollided = 0;
    cCol = maxcol = (float4)(0.0f, 0.0f, 0.0f, 0.0f);

    /* Process particles on three pixels in upper row */
    if (ipos.y > 0)
    {
        /* pixel offsets of upper, upper-left and upper-right pixel
         */
        o2 = off - SCR_WID;
        o1 = max(0,o2-1);
        o3 = min(o2+1, (int)MAX_SCR_OFFSET - 2) ;

        /* minimum and maximum index into sorted particle array
         * for these three pixels.
         * An empty pixel has start = impossible large number, 
         * end = -1. 
         */
        i = min(min(start[o1], start[o2]), start[o3]);
        i2 = max(max(end[o1], end[o2]), end[o3]);
#include "collideAll.clh"
    }

    /* Process particles on three pixels in current row */
    o2 = off;
    o1 = max(0,o2-1);
    o3 = min(o2+1, (int)MAX_SCR_OFFSET - 2) ;

    i = min(min(start[o1], start[o2]), start[o3]);
    i2 = max(max(end[o1], end[o2]), end[o3]);

    /* if there is only one particle involved, it must be the current one
     *  -> nothing to do
     */
    if ((i2-1) > 1)
    {
#include "collideAll.clh"
    }
    
    /* Process particles on three pixels in lower row */
    if (ipos.y < SCR_HEI-1)
    {
        /* pixel offsets of lower, lower-left and lower-right pixel
         */
        o2 = off + SCR_WID;
        o1 = max(0,o2-1);
        o3 = min(o2+1, (int)MAX_SCR_OFFSET - 2) ;

        i = min(min(start[o1], start[o2]), start[o3]);
        i2 = max(max(end[o1], end[o2]), end[o3]);
#include "collideAll.clh"
    }

#ifdef PARTICLE_LINK
#ifdef NLINK
    if (attributes.x & CL_MFLG_LINKED)
    {
        attr[pid] = attributes; 

        cp[6] = newLinks;
        status->updateLinks = 1;
    }
#else
    if (cp.s6 != oldLinks)
    {
        attr[pid].w |= CL_MFLG_LINKED;
        conn[pid] = cp;
    }
#endif
#endif

    if (nCollided)
    {
        int4 cmp;
        float nl;

        /* limit the impuls from particle collissions
         */
        nl = LENQ(frc);
        if (nl > 9.0f)
            frc = (frc / SQRT(nl)) * 3.0f;

        /* add sum of collission vectors to particle vector
         */
        newVec[pid] = vec + frc;

        /* Color merging
         * move current color towards weighted average color of all collided 
         * particles.
         */
        nCollided += PART_MASS(attributes);
        
        cCol = (cCol + colors[pid]*(float)PART_MASS(attributes)) / (float)(nCollided);

        cCol = colors[pid] + (cCol - colors[pid]) * params->colorMerge; 

        /* if enabled, move towards collisson target color, depending of accelleration force */
        if ((params->flags & CL_EFLAG_COLTARGET) != 0)
                cCol +=  (params->collTargetColor - cCol) * (0.125f - 1.0f/(nl + 8.0f));
        
//        if ((params->flags & CL_EFLAG_COLTHRESH) != 0)
        {
            cmp = ((maxcol - cCol) > params->colorDiffThresh);
            cCol = select(cCol, maxcol, cmp);
        }
        
        newCol[pid] = cCol;        
    }
    else
    {
        newCol[pid] = colors[pid];
        newVec[pid] = vec;
    }
}
