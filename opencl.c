/* Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/* OpenCL code to calculate a field which affects and is affected by particles, and a GPU
 * based particle engine.
 *
 * Field Calculation
 * =================
 * 
 * The principle of the field calculation is that particles, having a "mass" (1..255) and a
 * "charge" (-1, 0, +1), add their mass*charge to the corresponding location of a 2D
 * "density buffer" (all the field buffers are organized as array in the same resolution as 
 * the screen area).
 * The field itself is represented by a 2D buffer where each element consists of two scalars:
 * - The current potential
 * - The current change rate (1st derivation).
 *
 * With each iteration of the field, the change rate of a field value is adjusted based
 * on the difference of the current field potential to the weighted mean value of
 * - The corresponding value of the density buffer
 * - The current potential of the surrounding 8 field elements.
 * The new field value is calculated from the old field value and the new change rate.
 *
 * So when a new particle "appears" at a pixel, the change in the density buffer at this
 * location pulls the field at this location (and the 8 sorrunding values) to a new potential.
 * Consecutive iterations propagate this change further on, so that field waves appear.
 * If nothing moves on the screen, the field will eventually reach a static state.
 *
 * Constant parameters affect the frequency and damping of field waves, as well as the 
 * range of the field itself. The field formula
 * 
 * F''(x,y) = (D(x,y) + Fhv(x,y) + Fd(x,y) * 0.5) * strength - Fc(x,y)
 * Fn'(x,y)  = Fc'(x,y) + F''(x,y) * damping
 * Fn(x,y)   = (Fc(x,y) + Fn'(x,y)) * waveDamping
 *
 * where
 *  Fn        -   new field value
 *  Fc  -   current field value
 *  D   -   density
 *  Fhv -   Sum of four field values in horizontal/vertical direction
 *  Fd  -   Sum of four field values in diagonal direction
 *
 * Constants:
 *  strength    - The weight of neighbour pixels on the formula. 
 *  damping     - high values near 1 cause a high wave frequency
 *  waveDamping - high values cause waves to get propagated endlessly
 *
 * Effect of walls
 * ---------------
 * Walls also affect the field in two ways:
 * 1. They add their mass*density value to the density buffer just like particles
 * 2. In addition, their mass determines how much the field can oscillate at this location.
 *    If the mass is low (zero), the wall has few (no) influence to the field oscillation, 
 *    i.e. the wall is fully transparent to the field.
 *    The higher the mass, the smaller the amplitude of the field oscillation. The
 *    maximum mass causes the field to have a fixed potential at this point, which in 
 *    turn causes field waves to be fully reflected.
 *
 * The wall mass is kept in a separate buffer ("fixDens").
 *
 * Effect to particles
 * -------------------
 * For each pixel the "slope" of the field at this point is calculated, taking the potential
 * of the 8 surropunding pixels into account.
 * For all particles on this pixel, the slope multiplied with the particles' charge (-1, 0, 1)
 * is taken as accelleration vector for the particles. This means that particles with a charge
 * of 0 do not affect and aren't affected by the field.
 * The effect of the field on particles can be reversed depending on particle attributes. This
 * way particles can be configured to get repelled by particles with the same charge instead
 * of being attracted.
 * 
 *
 * Implementation
 * --------------
 * The CPU particle engine updates the density buffer whenever particles move or walls are
 * added/removed (the latter also updates the "fixDens" buffer). It also sets a change flag
 * when these buffers are modified.
 * The OpenCl thread (running asynchronously)
 * - checks the change flags and transfers density/fixDens buffers into GPU memory
 * - Performs n field iterations (n is configurable)
 * - Calculates the accelleration vector (slope) for each pixel
 * - Transfers the accelleration vector array to host memory (if there are any CPU 
 *   particles that would be affected)
 * - If a screen update is scheduled, transfers the current field potential/change rate 
 *   to a OpenGL screen texture for visualization.
 *
 * GPU Particle Engine
 * ===================
 * 
 * The GPU particle engine implements a subset of the CPU engine:
 * - Particle mass/charge
 * - Colored particles
 * - Wall collission handling (brick and vectored walls)
 * - Particle-to-particle collission
 * - Field interaction
 * - Linked particles
 * - Grabbing and moving particles
 * - re-color particles, color merging
 *
 * Not implemented (yet):
 * - Different particle sizes (radius is limited to maximum of 1/2 pixel)
 * - Manually pushing/accellerating particles
 * - Selecting particles
 * - Different particle types (energy, blob, dla)
 * - Select/cut/copy/paste
 *
 * Notes:
 * - CPU and GPU particle movement are totally decoupled. Speeds 
 *   don't match, the engines run is separate threads, so it totally
 *   depends on GPU and GPU speeds how they behave with respect to each
 *   other. There is an option/button to synchronize the two engines though.
 * - CPU/GPU particles don't interact, except via the force field (see below).
 *
 * Movement (particles.cl)
 * -----------------------
 * Unlike CPU particles, GPU particles are not speed limited by principle.
 * To avoid tunneling through walls, the "particleMove" kernel will run in a loop where
 * a particle is advanced max. one pixel at a time. This has the drawback that the kernel
 * might take a long time for very fast particles, so there is a speed limit slider which
 * simply stops the movement after the defined number of iterations.
 *
 * Collission detection between particles is not affected by this (see below).
 *
 * Particle attributes
 * -------------------
 * Particle attributes are stored in several buffers:
 * 
 * Position (float2), double buffer, shared with OpenGL
 * Vector (float2), double buffer
 * Attributes (uint), double buffer
 * Links (uint8)
 *    .s0 .. s5    Particle ID of linked particle, or 0 if not linked
 *    .s6          Number (n) of links (s[0] .. s[n-1] encodes a valid link)
 *    .s7          Max. number of links this particle can have
 * Color (float4), double buffer, shared with OpenGL:
 *                 The current color of the particle
 * base color (uchar4), double buffer
 *                 The original color of the particle
 *
 * Reasons for not using a single buffer:
 * - Work is done in several kernels, not every one needs all data
 * - Some work needs to be done by using ping-pong buffers. Having a single buffer for all
 *   data would result in copying a lot of useless data.
 *
 * Collission detection (collide.cl, sort.cl).
 * -------------------------------------------
 * The CPU engine uses a linked list of particles for each pixels to limit the amount
 * of searches for collission detection. This is not possible for a GPU, so a different
 * approach is used (based on the "particles" sample from Nvidia's SDK):
 * - In each iteration, all particles are sorted based on their location (using a bitonic
 *   sort algorithm). 
 * - Afterwards, an width*height sized index buffer is filled where each pixel holds a
 *   start/end pointer into the sorted list of particles so that it's known which
 *   particles are on a given screen pixel.
 * - The actual collission detection for a particle looks at the index buffer entry for
 *   the particle's location, and the surrounding 8 pixels, to search for collission
 *   candidates. The collission formula (collide() in collideElastic.cl) is applied for
 *   all particles nearer than 1.0, summed up and added to the particle's vector.
 * - For each detected collission, 
 *   - The collission vector is calculated based on the relative position, vectors and masses
 *     of the two involved particles (taking into account global parameters shear, damping,
 *     spring factor and attraction).
 *   - The color of the two particles is merged based on their relative mass and a merge
 *     factor.
 *     A separate kernel will restore a particle's color back to it's original color based
 *     on a color restore factor.
 *   - Particles may get permanently linked together (see below)
 *
 * Notes:
 * - The major bottleneck is sorting the particles
 * - Unlike wall collission (and the CPU engine), particle-to-particle collission does not
 *   take fast particles (>> 1 pixel/iteration) into account, i.e. those particles might fly
 *   through each other although they would hit.
 *   The collission detection does only two checks:
 *   1. Check if two particles collide based on their current position.
 *   2. If not, check if the two particles will collide at the next iteration (i.e. after
 *      adding their current speed vector).
 * - Depending on their speed particles more-or-less move into each other before a collission
 *   is detected.
 *   The deeper they move into each other, the more they get repelled, leading to a problematic
 *   dynamic behaviour of compressed particles.
 * 
 *   To enhance this, and to reduce the probability of two particles missing each other,
 *   a time factor can be adjusted, leading to a more realistic behaviour (but also to 
 *   slower movement).
 *
 * Linked particles
 * ----------------
 * This works similar to chained CPU particles. Each particle can link to up to six other
 * particles. Linked particles attract each other with a force proportional to their distance.
 * If the distance gets too big the link is broken again.
 *
 * The check for link is done each time two particles collide. Each particle has associated an
 * array of 8 integers (cl_uint8). The first six ("link table") keep the indices to the linked
 * particles (0 means no link). The 7th is the number of linked particles, the 8th is the
 * maximum number of links a particle can have.
 *
 * When particle A detects that it collides with B it checks
 *  - if it's own and B's link count is lower than the maximum,
 *  - if it is not already linked with B.
 *  - If the mass is identical (the attraction/repulsion forces caused by link/collission are
 *    not symmetrical for particles with different mass ...) 
 * If all checks pass, B's index is inserted into a free slot of A's link table
 *
 * It is most likely (but not 100% sure!) that the collision code of B also detects a collision
 * with A and adjusts it's own link table accordingly by adding A.
 *
 * A link is only valid if A refers to B _AND_ B refers to A. Since this can not be guaranteed
 * due to race conditions caused by parallel kernels/work items an additional check-and-repair
 * stage is needed:
 * - Whenever a link is created or broken, the flag "updateLinks" is set in the status buffer.
 * - The CPU invokes the cleanupConnections() kernel. This kernel goes though all particle
 *   link tables and makes sure that each link between two particles is bi-directional. If not,
 *   the link is removed again.
 *   This makes sure that uni-directional links are elimitated before they are taken into
 *   account in the next iteration.
 *
 *   This kernel also eliminates "holes" in the link table, which may exist after a link has 
 *   broken or a particle has been removed
 *   For example, if a particle has 3 links, the three references are stored
 *   in the first three entries of it's link table. If the first entry points to a deleted
 *   particle, the 2nd and 3rd entry have to be moved.
 *
 * Displaying particles
 * --------------------
 * The position and color of particles are kept in buffers that are shared with OpenGL
 * (if OpenCl/OpenGL interoperability is supported, otherwise data is copied to host memory).
 * This data is drawn as array into a texture when screen is updated.
 *
 * Particle links are generated in the mkLinkLines thread by adding start end point/color into
 * link vector/color buffers for each link line, and are again drawn as OpenGL array.
 *
 * Movement lines (trails) are generated using a large buffer that has consists of 
 * - a bucket of n lines (2 x float2) for each particle, containg the "movement history" of the 
 *   particle's last n iterations
 * - an index for each particle that points to the next line element in the bucket (increased
 *   at each iteration).
 *
 * OpenGL uses all buckets as PBO/array to draw all the lines for m particles (n*m line elements). 
 * Buckets of new particles are preset with zero. When a particle is deleted, it's bucket is 
 * cleared.
 * So there might be a lot of "zero lines" drawn.
 */

#define DEBUG 0

int allowAsyncBuff = 1;
int alwaysAsync = 0;
int roundToPot = 0;
int haveOcl2 = 0;

#include "plansch.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <CL/cl.h>
#include <CL/cl_ext.h>
#include <CL/cl_gl.h>

#include "opencl.h"

#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)

#define CL_MP_COUNT_MAX 100

typedef struct 
{
    char *source;
    char *fct;
    cl_kernel *kernel;
    int maxWg;
} clKernel_t;

#define _DECL_KERNELS
#include "kernels.h"

float                     colorRestore = 0.0f;
int                       scrFlagFieldElements = 0;
clBuffer_t               *jobBuffer;
clBuffer_t               *clWalls;
clBuffer_t               *clWallFlags;
FT_TYPE                  *gpuScrFlagsHost;
clBuffer_t               *gpuScrFlags;
FT_TYPE                  *cpuScrFlags;
int                       cpuScrFlagsDirty = 0;
clBuffer_t               *accelBuffer;
size_t                    maxWorkGroupSize;
cl_command_queue          commandQueue;
cl_command_queue          deviceQueue;
int                       clDoGrab = 0;
cl_float2                 clPointerPos;
cl_float2                 clGrabCenter;
int                       clJobTypes = 0;
int                       gpuLoops = 0;
uint64_t                  gpuMemUsed = 0;
clBuffer_t               *clParamBuffer = NULL;
clParameters_t           *clParameters = NULL;
int                       densityUpdate;
int                       clWallUpdateFlag = 0;
wall_t                   *clWallsHost = NULL;
int                       totalGpuParticles = 0;
gpuParticleSet_t          particleSet[CL_NUM_PARTICLE_SETS];
int                       movingPartSet = 0;
int                       oscPartSet = 0;
clSearchOp_t              currentChattrOp;
int                       particleMarkOp;
SDL_Rect                  particleMarkRect;
int                       localSizeDim2 = 8;
int                       tryPinned = 0;
int                       useLocalMem = -1;
int                       useHalfField = 0;
size_t                    ftypeLen;
cl_ulong                  localMemSize;
cl_ulong                  atomicCounters;
int                       clParticleLoops = 0;

static float              zeroFloat = 0.f;
static uint16_t           zeroHalf;
static clStats_t          oldStats, currentStats;
static clFieldStats_t     oldFieldStats, currentFieldStats;
static cl_context         context;
static int                contextValid = 0;
static cl_device_id      *devices;
static int                clParametersChanged = 0;
static int                initialized = 0;
static float             *gravBufferHost = NULL;
static int                clMkLinkInterval = 10;

static void runJobBuffer(void);

/**********************************************************************************/


#define SHOWCHANGED(p) if (pSet->clStatus->p != pSet->clStatusOld.p) printf ("%s: %d\n", #p, pSet->clStatus->p)
void clStatusShow (gpuParticleSet_t *pSet, int changed)
{
    clBufferToHost (pSet->statusBuffer, 0);

    if (changed)
    {
        SHOWCHANGED(nParticlesAfterSort);
        SHOWCHANGED(boundaryCheckErrors);
    }
    else
    {
        printf ("nParticlesAfterSort: %d\n", pSet->clStatus->nParticlesAfterSort);
        printf ("boundaryCheckErrors: %d\n", pSet->clStatus->boundaryCheckErrors);
    }
    pSet->clStatusOld = *pSet->clStatus;
}


void clUseHalfField (int value)
{
    useHalfField = value;
}


void clUseLocalmem (int value)
{
    useLocalMem = value;
}

void clSetLocalSizeDim2 (int value)
{
    localSizeDim2 = value;
}

void clTryPinned (int p)
{
    tryPinned = p;
}

void clFixDensArrayUpdate(void)
{
    fixDensBufferUpdate = 1;
    densityUpdate = 1;
}

static void abortHandler()
{
    if (options->clGlSharing)
    {
        printf ("\n    Error during OpenCL initialization."
                "\n    Try using the \"-C\" switch!\n");
    }
}



void *clGravBufferGet()
{
    return (void*)gravBufferHost;
}

const char *fileToString (char *path, char *file)
{
    int fd;
    int len;
    char *rc;
    char fname[MAXPATHLEN];

    sprintf (fname, "%s/%s", path, file);

    fd = open (fname, O_RDONLY);

    if (fd == -1)
    {
        perror (fname);
        return NULL;
    }
    len = lseek (fd, 0, SEEK_END);

    if (len <= 0)
    {
        perror (fname);
        close (fd);
        return NULL;
    }

    rc = malloc(len+1);
    if (rc == NULL)
    {
        close (fd);
        return NULL;
    }

    lseek (fd, 0, SEEK_SET);

    if (read (fd, rc, len) == -1)
    {
        perror (fname);
        close (fd);
        free (rc);
        return NULL;
    }
    rc[len] = 0;

    close (fd);

    return rc;
}

int openclPresent()
{
    cl_int status = 0;
    cl_uint numPlatforms;

    status = clGetPlatformIDs(0, NULL, &numPlatforms);
    if(status != CL_SUCCESS)
    {
        printf("Error: Getting Platforms. (clGetPlatformsIDs)\n");
        return 0;
    }

    return numPlatforms;
}

clBuffer_t *makeBufferOgl (cl_mem_flags f, void *hostData, size_t elem_size, size_t elements, char *name)
{
    cl_int status;
    clBuffer_t *b = calloc(1, sizeof(*b));

    size_t hostDataSize = elem_size * elements;

    b->hostBuffer = hostData;
    b->size = hostDataSize;
    b->name = name;
    b->elem_size = elem_size;
    b->elements = elements;
    b->pending = 0;

    glGenBuffers(1, &b->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, b->vbo);
    glBufferData(GL_ARRAY_BUFFER, hostDataSize, hostData, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    if (!b->vbo)
    {
        printf ("Error: Failed to create fbo for <%s>\n", name);
        free (b);
        return NULL;
    }

    b->clBuffer = clCreateFromGLBuffer (context, CL_MEM_READ_WRITE, b->vbo, &status);

    if(status != CL_SUCCESS) 
    { 
        printf("Error: clCreateFromGLBuffer (%s) : %d\n", name, status);
        free (b);
        return NULL;
    }

    clEnqueueAcquireGLObjects (commandQueue, 1, &b->clBuffer, 0, NULL, NULL);

    if (hostData)
        clBufferToGPU (b, 0);

    debug ("OGL buffer %s initialized: clBuffer=%p vbo=%d size=%ldMB\n", name, b->clBuffer, b->vbo, (hostDataSize+0xffffff)/0x100000);

    gpuMemUsed += hostDataSize;

    return b;
}

int roundup (int size)
{
    int allocSize = size;
    int mask;

    for (mask = 0x40000000; mask > 1; mask = mask >> 1)
    {
        if (size & mask)
        {
            allocSize = (mask == size) ? size : (mask << 1);
            break;
        }
    }

    return allocSize;
}

void *aAlloc (int size)
{
    int allocSize = size;
    void *rc;

    if (roundToPot)
        allocSize = roundup(size);

    rc = memalign (0x2000, allocSize);

    if (!rc)
    {
        printf ("aAlloc: falied to alloc %x bytes\n", size);
        BACKTRACE;
        return NULL;
    }

    memset (rc, 0, allocSize);

    return rc;
}

clBuffer_t *makeBuffer (int hostMem, cl_mem_flags f, void *hostData, size_t elem_size, size_t elements, char *name)
{
    cl_int status;
    clBuffer_t *b = malloc(sizeof(*b));
    memset ((char*)b, 0, sizeof(*b));
    size_t hostDataSize = elem_size * elements;
    int allocSize = hostDataSize;
    int pin = (tryPinned && (hostMem == 2));

    if (f == 0)
        f = CL_MEM_READ_WRITE;

    if (hostMem)
    {
        if (hostData == NULL)
        {
            if (pin)
                f |= CL_MEM_ALLOC_HOST_PTR;
//            allocSize = roundup(hostDataSize);
        }
        else
        {
            f |= CL_MEM_USE_HOST_PTR;
        }
    }
    else if (hostData)
    {
        f |= CL_MEM_COPY_HOST_PTR;
    }

    b->clBuffer = clCreateBuffer( context, f,
                      allocSize,
                      hostData, 
                      &status);

    if(status != CL_SUCCESS) 
    { 
        printf("Error: clCreateBuffer (%p, %s)\n", hostData, name);
        free (b);
        return NULL;
    }

    contextValid = 1;

    if (hostMem && (hostData == NULL))
    {
        if (pin)
        {
            cl_map_flags mf;

            if (f & CL_MEM_READ_ONLY)
            {
                mf = CL_MAP_WRITE;
            }
            else if (f & CL_MEM_WRITE_ONLY)
            {
                mf = CL_MAP_READ;
            }
            else
            {
                mf = CL_MAP_READ|CL_MAP_WRITE;
            }

            b->hostBuffer = (void*)clEnqueueMapBuffer(commandQueue,
                                b->clBuffer,
                                CL_TRUE,
                                mf, 0, allocSize, 0,
                                NULL, NULL, NULL);

            if (b->hostBuffer == NULL)
            {
                printf ("Failed to map host buffer %s!\n", name);
                exit (1);
            }
            memset (b->hostBuffer, 0, allocSize);
        }
        else
        {
            b->hostBuffer = aAlloc (allocSize);
        }
    }
    else if (hostData)
    {
        b->hostBuffer = hostData;
    }

    b->size = hostDataSize;
    b->name = name;
    b->pending = 0;
    b->elem_size = elem_size;
    b->elements = elements;

    debug ("OCL buffer %s initialized: clBuffer=%p size=%dMB\n", name, b->clBuffer, (allocSize+0xffffff)/0x100000);

    gpuMemUsed += allocSize;

    return b;
}

static cl_program compile (char *file, char *clc_options)
{
    size_t sourceSize[1];
    const char *sources[1];
    cl_int status = 0;
    cl_program program;


    sources[0]    = fileToString (dataDir, file);
    sourceSize[0] = strlen(sources[0]);
    printf ("Compiling %s ..\n", file);

    program = clCreateProgramWithSource(
                  context, 
                  1, 
                  sources,
                  sourceSize,
                  &status);
    if(status != CL_SUCCESS) 
    { 
        printf("Error: Loading Binary into cl_program \
                       (clCreateProgramWithBinary)\n");
        return 0;
    }

    /* create a cl program executable for all the devices specified */
    status = clBuildProgram (program, 1, &devices[options->usedClDevice], clc_options, NULL, NULL);
    if(status != CL_SUCCESS) 
    { 
        char buildLog[50000];
        size_t s;
        clGetProgramBuildInfo (program, devices[options->usedClDevice], CL_PROGRAM_BUILD_LOG, sizeof(buildLog), buildLog, &s);
        printf("Error: Building Program (clBuildProgram) : %s\n", buildLog);
        return 0; 
    }

    return program;
}

static int initKernels ()
{
    int i, j;
    cl_int status = 0;
    int rc = 1;
    char *s;
    char *clc_options;
    char *clOptions = getenv ("clc_options");
    cl_program                program;
    char *sources[] =
    {
        "field.cl",
        "sort.cl",
        "particles.cl",
        "particleMove1.cl",
        "particleMove2.cl",
        "collideElastic.cl",
        "collideElasticLink.cl",
        "blob.cl",
        NULL
    };


    s = clc_options = alloca(0x10000);
    strcpy (clc_options, "");

    if (clOptions == NULL)
    {
        s += sprintf (s, " -cl-mad-enable ");
        printf ("OCL Compiler options: <%s> (use clc_options env.var to override)\n", clc_options);
    }
    else
    {
        s += sprintf (s, "%s ", clOptions);
        printf ("OCL Compiler options: <%s> (from clc_options env.var)\n", clc_options);
    }

    if (haveOcl2)
    {
        s += sprintf (s, "-cl-std=CL2 -DHAVE_OCL2 ");
    }
    s += sprintf (s, "-I%s ", dataDir);
    s += sprintf (s, "-DDENSITY_TYPE=%s ", STR(DENSITY_TYPE));
    s += sprintf (s, "-DMAX_SCR_OFFSET=%d ", wid*hei);
    s += sprintf (s, "-DSCR_WID=%d ", wid);
    s += sprintf (s, "-DSCR_HEI=%d ", hei);
    if (!strcmp ("int", STR(DENSITY_TYPE)))
        s += sprintf (s, "-DADDFCT=atom_add ");
    else
        s += sprintf (s, "-DADDFCT=atomic_add_%s ", STR(DENSITY_TYPE));

    if (atomicCounters)
        s += sprintf (s, "-DATOMIC_CNT ");

    if (useHalfField)
        s += sprintf(s, "-DFIELD_HALF ");

#ifdef FIX_DENS_FLOAT
    s += sprintf(s, "-DFIX_DENS_FLOAT ");
#endif

    printf ("%s\n", clc_options);

    /* compile all sources and find the kernels we need
     */
    for (j = 0; sources[j] != NULL; j++)
    {
        program = compile (sources[j], clc_options);

        if (program == 0)
        {
            rc = 0;
            break;
        }

        for (i = 0; i < NELEMENTS(kernels); i++)
        {
            if (strcmp (kernels[i].source, sources[j]))
                continue;

            *kernels[i].kernel = clCreateKernel(program, kernels[i].fct, &status);

            if (status != CL_SUCCESS)
            {  
                printf("Error: No kernel <%s> in program <%s>\n", kernels[i].fct, kernels[i].source);
                rc = 0;
            }
            else
            {
                clGetKernelWorkGroupInfo(*kernels[i].kernel,
                    devices[options->usedClDevice],
                    CL_KERNEL_WORK_GROUP_SIZE,
                    sizeof(uint),
                    (void*)&kernels[i].maxWg, NULL);

                debug ("%s: %d\n", kernels[i].fct, kernels[i].maxWg);
            }
        }
    }

    if (rc)
    {
        printf ("Successfully compiled %d kernels\n", i);
    }
    else
    {
        printf ("ERROR: Failed to build kernels\n");
    }

    return rc;
}


/* first stage OpenCL initialization
 */
int
clInit1(void)
{
    int i;
    cl_int status = 0;
    size_t deviceListSize;
    char deviceName[1024] = "Unknown";
    char deviceVersion[1024] = "Unknown";
    cl_context_properties * cprops = NULL;
    cl_platform_id* platforms = NULL;

    clJobsInit();

    openglRestoreContext();

    /*
     * Have a look at the available platforms and pick either
     * the AMD one if available or a reasonable default.
     */

    cl_uint numPlatforms;
    cl_platform_id platform = NULL;
    status = clGetPlatformIDs(0, NULL, &numPlatforms);
    if(status != CL_SUCCESS)
    {
        printf("Error: Getting Platforms. (clGetPlatformsIDs)\n");
        return 0;
    }

    if (options->usedClPlatform >= numPlatforms)
        options->usedClPlatform = 0;
    
    if(numPlatforms > 0)
    {
        platforms = (cl_platform_id *)malloc(numPlatforms*sizeof(cl_platform_id));

        status = clGetPlatformIDs(numPlatforms, platforms, NULL);
        if(status != CL_SUCCESS)
        {
            printf("Error: Getting Platform Ids. (clGetPlatformsIDs)\n");
            return 0;
        }
        for(i=0; i < numPlatforms; ++i)
        {
            char pbuff[100];
            status = clGetPlatformInfo(
                        platforms[i],
                        CL_PLATFORM_VENDOR,
                        sizeof(pbuff),
                        pbuff,
                        NULL);
            platform = platforms[i];
            printf ("OpenCL Platform: %s %s\n",
                pbuff,
                (i == options->usedClPlatform) ? "[used]" : "");

            if (i == options->usedClPlatform)
                break;
        }
        free(platforms);
    }

    /* 
     * If we could find our platform, use it. Otherwise pass a NULL and get whatever the
     * implementation thinks we should be using.
     */
    cl_context_properties cps_noshare[] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 
        0 };


    cl_context_properties cps_glshare[] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 
#if defined(_MINGW)
        CL_GL_CONTEXT_KHR, (cl_context_properties)ctx, 
	CL_WGL_HDC_KHR, (cl_context_properties)dc, 
#else
        CL_GL_CONTEXT_KHR, (cl_context_properties)sdl_gl_context, 
        CL_GLX_DISPLAY_KHR, (cl_context_properties)sdl_display,
#endif
        0 };


    if (platform)
    {
#if 0
	char platExtensions[5000];
        status = clGetPlatformInfo (platform, CL_PLATFORM_EXTENSIONS,
                                    sizeof(platExtensions), platExtensions,
                                    NULL);

        if (status == CL_SUCCESS)
        {
            options->clGlSharing = (strstr(platExtensions, "_gl_sharing") != NULL);
            printf ("cl_gl_sharing is %ssupported by platform\n%s\n",
                    options->clGlSharing ? "" : "not ",
                platExtensions);
        }
#endif

        cprops = options->clGlSharing ? cps_glshare : cps_noshare;
    }

    /////////////////////////////////////////////////////////////////
    // Create an OpenCL context
    /////////////////////////////////////////////////////////////////
    context = clCreateContextFromType(cprops, 
                                      CL_DEVICE_TYPE_ALL,
                                      NULL, 
                                      NULL, 
                                      &status);
    if(status != CL_SUCCESS) 
    {  
        printf("Error: Creating Context. (clCreateContextFromType): %d\n", status);
        
        if (cprops == cps_glshare)
        {
            printf("Trying without OpenGL interop\n");
            context = clCreateContextFromType(cps_noshare, 
                                              CL_DEVICE_TYPE_ALL,
                                              NULL, 
                                              NULL, 
                                              &status);
            if(status != CL_SUCCESS) 
            {  
                printf("Error: Creating Context. (clCreateContextFromType): %d\n", status);
                return 0;
            }
        }
    }

    /* First, get the size of device list data */
    status = clGetContextInfo(context, 
                              CL_CONTEXT_DEVICES, 
                              0, 
                              NULL, 
                              &deviceListSize);
    if(status != CL_SUCCESS) 
    {  
        printf(
                "Error: Getting Context Info \
            (device list size, clGetContextInfo)\n");
        return 0;
    }

    /////////////////////////////////////////////////////////////////
    // Detect OpenCL devices
    /////////////////////////////////////////////////////////////////
    devices = (cl_device_id *)malloc(deviceListSize);
    if(devices == 0)
    {
            printf("Error: No devices found.\n");
            return 0;
    }

    /* Now, get the device list data */
    status = clGetContextInfo(
                 context, 
                 CL_CONTEXT_DEVICES, 
                 deviceListSize, 
                 devices, 
                 NULL);

    if(status != CL_SUCCESS) 
    { 
            printf("Error: Getting Context Info \
                (device list, clGetContextInfo)\n");
            return 0;
    }

    printf ("Available devices: \n");

    if (options->usedClDevice >= deviceListSize)
        options->usedClDevice = 0;

    for (i = 0; i < deviceListSize / sizeof(*devices); i++)
    {
        clGetDeviceInfo (devices[i], CL_DEVICE_NAME, sizeof(deviceName), deviceName, NULL);
        clGetDeviceInfo (devices[i], CL_DEVICE_VERSION, sizeof(deviceVersion), deviceVersion, NULL);

        if (i == options->usedClDevice)
        {
            if (useLocalMem == -1)
            {
                useLocalMem = 0;

                if (strstr(deviceName, "Juniper"))
                    useLocalMem = 0;
                if (strstr(deviceName, "CPU"))
                    useLocalMem = 0;
            }

            if (haveOcl2 == -1)
            {
                haveOcl2 = (strstr(deviceVersion, "OpenCL 2") != NULL);
            }
        }

        printf ("%d: %s %s %s\n", i, deviceName, deviceVersion, (i == options->usedClDevice) ? "(used)":"");
    }


    /////////////////////////////////////////////////////////////////
    // Create an OpenCL command queue
    /////////////////////////////////////////////////////////////////
    if (haveOcl2)
    {
        cl_queue_properties qprop[] =
        {
            CL_QUEUE_PROPERTIES,
                    (cl_command_queue_properties)(CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | 
                            CL_QUEUE_ON_DEVICE | CL_QUEUE_ON_DEVICE_DEFAULT),
            CL_QUEUE_SIZE, 
                    1024, 
            0, 0
        };

        deviceQueue = clCreateCommandQueueWithProperties(
                           context, 
                           devices[options->usedClDevice], 
                           qprop, 
                           &status);

        if(status != CL_SUCCESS) 
        { 
            printf("Error creating Device Queue. (clCreateCommandQueue) : %d\n", status);
            printf("OpenCL 2 DISABLED\n");
            haveOcl2 = 0;
        }
    }

    commandQueue = clCreateCommandQueueWithProperties(
                       context, 
                       devices[options->usedClDevice], 
                       NULL, 
                       &status);
    if(status != CL_SUCCESS) 
    { 
        printf("Creating Command Queue. (clCreateCommandQueue) : %d\n", status);
        return 0;
    }

    signal (SIGABRT, abortHandler);

    clGetDeviceInfo(devices[options->usedClDevice], CL_DEVICE_LOCAL_MEM_SIZE, 
                sizeof(localMemSize), &localMemSize, NULL);
    clGetDeviceInfo(devices[options->usedClDevice], CL_DEVICE_MAX_WORK_GROUP_SIZE, 
                sizeof(maxWorkGroupSize), &maxWorkGroupSize, NULL);
#ifdef CL_DEVICE_MAX_ATOMIC_COUNTERS_EXT
    clGetDeviceInfo(devices[options->usedClDevice], CL_DEVICE_MAX_ATOMIC_COUNTERS_EXT, 
                sizeof(atomicCounters), &atomicCounters, NULL);
#else
    atomicCounters = 0;
#endif

    printf ("local mem size : 0x%x\n", (int)localMemSize);
    printf ("work groups    : 0x%x\n", (int)maxWorkGroupSize);
    printf ("counters       : %d\n",   (int)atomicCounters);
    atomicCounters = 0;

    if (!initKernels ())
        return 0;
    
    initialized = 1;

    return 1;
}


/*
 * \brief OpenCL related initialization 
 *        Create Context, Device list, Command Queue
 *        Create OpenCL memory buffer objects
 *        Load CL file, compile, link CL source 
 *                  Build program and kernel objects
 */
int
clInit2 (int wid, int hei, DENSITY_TYPE **dens, FDTYPE **fixDens, GRAV_TYPE **dx, GRAV_TYPE **dy, GLuint stex, GL_CONTEXT_T glCtx)
{
    cl_int status = 0;
    int wh = wid * hei;
    DENSITY_TYPE dz[] = {0};
    GRAV_TYPE gz[] = {0};
    int x, y;
    FDTYPE fdzero = FDZERO;
    void *field;
    cl_float2 fval;
    cl_float2 *fieldFloat;
    struct
    { 
        int16_t x; 
        int16_t y; 
    } *fieldShort;

    if (!initialized)
        return 0;

    if (useHalfField)
    {
        ftypeLen = 2;
    }
    else
    {
        ftypeLen = 4;
    }

    field = (cl_float2*)malloc(wh * ftypeLen * 2);
    fieldFloat = field; 
    fieldShort = field; 

    for (x = 0; x < wid; x++)
    {
        for (y = 0; y < hei; y++)
        {
            if (x == 0)
            {
                fval.y = ((y == 0) || (y == hei-1)) ? (float)0 : (float)1;
            }
            else if (x == wid-1)
            {
                fval.y = ((y == 0) || (y == hei-1)) ? (float)0 : (float)-1;
            }
            else if (y == 0)
            {
                fval.y = (float)wid;
            }
            else if (y == hei-1)
            {
                fval.y = (float)-wid;
            }
            else
            {
                fval.y = (float)0;
            }
            fval.x = (float)0;

            if (!useHalfField)
            {
                fieldFloat[x + y*wid] = fval;
            }
            else
            {
                fieldShort[x + y*wid].x = floatToHalf(fval.x);
                fieldShort[x + y*wid].y = floatToHalf(fval.y);
            }
        }
    }
    zeroHalf = floatToHalf(0.0f);

    /* # of integer elements in a screen flag field
     */
    scrFlagFieldElements = (wid*hei + 31) / 32;
    
    initialized = 0;

    densBuffer = makeBuffer (1, CL_MEM_READ_ONLY, NULL, sizeof(**dens), wh, "dens");
    if (!densBuffer)
        return 0;

    *dens = (DENSITY_TYPE*)densBuffer->hostBuffer;
    
    FILL_BUFFER(densBuffer, dz);

    fixDensBuffer = makeBuffer (1, CL_MEM_READ_ONLY, NULL, sizeof(**fixDens), wh, "fixDens");
    if (!fixDensBuffer)
        return 0;

    *fixDens = (FDTYPE*)fixDensBuffer->hostBuffer;
    FILL_BUFFER(fixDensBuffer, fdzero);

    diffBufferHost = makeBuffer (2, CL_MEM_WRITE_ONLY, NULL, sizeof(**dx), 2 * wh, "dy");
    if (!diffBufferHost)
        return 0;

    *dx = (GRAV_TYPE*)diffBufferHost->hostBuffer;
    FILL_BUFFER(diffBufferHost, gz);

    diffBuffer = makeBuffer (0, CL_MEM_READ_WRITE, NULL, ftypeLen*2, wh, "diffBuffer");
    if (!diffBuffer)
        return 0;


    fieldBuffer[0] = makeBuffer (0, CL_MEM_READ_WRITE,  field, ftypeLen*2, wh, "field");
    if (!fieldBuffer[0])
        return 0;

    fieldBuffer[1] = makeBuffer (0, CL_MEM_READ_WRITE,  field, ftypeLen*2, wh, "field");
    if (!fieldBuffer[1])
        return 0;

    jobBufferHost = (cl_int4*)aAlloc (sizeof(cl_int4) * JOB_FIELD_SIZE);
    if (!jobBufferHost)
        return 0;

    jobBuffer = makeBuffer (0, CL_MEM_READ_ONLY, jobBufferHost, sizeof(cl_int4), JOB_FIELD_SIZE, "jobBuffer");
    if (!jobBuffer)
        return 0;


    if (options->clGlSharing)
    {
        /* when we are directly sharing with OpenGl we render into a openGl texture
         */

        memset (&gravBufferShared, 0, sizeof(gravBufferShared));
        gravBufferShared.name = "gravBuffer";

        gravBufferShared.clBuffer =
                clCreateFromGLTexture (context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, stex, &status);

        if (gravBufferShared.clBuffer == NULL)
        {
            printf ("clCreateFromGLTexture(2D) failed: %d\n", status);
            return 0;
        }
        gravBuffer = &gravBufferShared;

        clEnqueueAcquireGLObjects (commandQueue, 1, &gravBufferShared.clBuffer, 0, NULL, NULL);
    }
    else
    {
        /* .. otherwise take a detour via host memory :-(
         */
        gravBufferHost = aAlloc (wh*sizeof(cl_uchar4));
        if (!gravBufferHost)
            return 0;

        gravBuffer = makeBuffer (0, CL_MEM_READ_WRITE, gravBufferHost, sizeof(cl_uchar4), wh, "gravBuffer");
        if (!gravBuffer)
            return 0;
    }

    signal (SIGABRT, SIG_IGN);

    if (!options->clGlSharing)
    {
        fieldToTex = fieldToTexBuf;
        fieldToTexLines = fieldToTexLinesBuf;
    }

    accelBuffer = makeBuffer (1, CL_MEM_READ_ONLY, NULL, ftypeLen*2, wh, "accelBuffer");
    if (!accelBuffer)
        return 0;

    if (useHalfField)
    {
        FILL_BUFFER(accelBuffer, zeroHalf);
    }
    else
    {
        FILL_BUFFER(accelBuffer, zeroFloat);
    }

    if (!gpuParticlesInit(wh))
    {
        printf ("OpenCL init failed!\n");
        return 0;
    }

    printf ("OpenCL init done. Used %d MB for buffers.\n",
            (int)(gpuMemUsed / (1024*1024)));
    initialized = 1;
    return 1;
}

void clearStatusBuffer (gpuParticleSet_t *pSet)
{
    int e = 0;
    FILL_BUFFER(pSet->statusBuffer, e);
    memset (pSet->clStatus, 0, sizeof(*pSet->clStatus));
}


void clWork (int scrUpdate)
{
    int set;

    if (!initialized)
        return;

    clPointerPos.x = SCREENC_F(iPositionLast.x);
    clPointerPos.y = SCREENC_F(iPositionLast.y);

    glFinish();

    /* clear GPU status data
     */
    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        clearStatusBuffer (&particleSet[set]);
    }

    /* run the CPU job queue 
     */
    processClJobQueue();
//    clUpdateFreelist(&particleSet[0]);

    /* transfer modified global parameters to constant GPU
     * buffer
     */
    if (clParametersChanged)
    {
        clBufferToGPU (clParamBuffer, 1);
        clParametersChanged = 0;
    }

    /* Run the GPU job queue
     */
    if (jobBufferEntries)
    {
        runJobBuffer();
    }
    
    /* Field iteration, unless engine is stopped and no particles
     * are grabbed
     */
    if (!enginesStopped() || clDoGrab)
    {
        int toScr = 0;

        if (fixDensBufferUpdate)
        {
            clBufferToGPU (fixDensBuffer, 1);
            fixDensBufferUpdate = 0;
        }

        if (getValue (&particleAttraction))
        {
            clDoGrav(scrUpdate);
            toScr = scrUpdate;
        }

        if (GETVAL32(wallTempr))
        {
            iterateTempr();
            toScr = scrUpdate;

            clParameters->seed = 214013*clParameters->seed+2531011;
            clParametersChanged = 1;
        }

        if (toScr)
        {
            runFieldToTex();
        }
    }
    
    /* Run particle engine
     */
    clParticles (scrUpdate, enginesStopped());

    /* draw particle data
     */
    if (!GETVAL32(drawWithScreen))
    {
        /* render particles independently from screen update
         */
        if (showParticles)
        {
            for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
            {
                clMakeLinkLines (&particleSet[set]);
                renderClParticles(set);
            }
        }
    }
    else
    {
        if (scrUpdate && showParticles)
        {
            for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
            {
                clMakeLinkLines (&particleSet[set]);
                renderClParticles(set);
            }
        }
    }

    /* clear pixel markers if necessary
     */
    if (PENDING_JOB(CL_JOB_UNMARK_AFTER))
    {
        clClearPixelMarkers ();
        PENDING_JOB_DONE(CL_JOB_UNMARK_AFTER);
    }

//    INTERVAL_RUN(1000, getClStatistics (1));

    clFinish (commandQueue);
}

int clFinishBufferTransfer (clBuffer_t *b, int direction)
{
    cl_int   status;

    direction &= b->pending;

    if (direction == 0)
        return 0;

    if (direction & TO_HOST)
        gpuTransToHost += b->size;

    if (direction & TO_GPU)
        gpuTransToGpu += b->size;

    /* Wait for the buffer to finish execution
     */
    status = clWaitForEvents(1, &b->events[1]);
    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Waiting for buffer call to finish: %d. (%s)\n", status, b->name);
        return 0;
    }
    
    clReleaseEvent(b->events[1]);
    b->pending &= ~direction;

    return 1;
}

void clBufferToHostPart (clBuffer_t *b, int async, size_t offset, size_t count)
{
    cl_int   status;

    offset *= b->elem_size;
    count *= b->elem_size;

    if (!allowAsyncBuff)
        async = 0;
    else if (alwaysAsync)
        async = 1;

    clFinishBufferTransfer (b, TO_ALL);

    /* Enqueue readBuffer*/
    status = clEnqueueReadBuffer(
                commandQueue,
                b->clBuffer,
                async ? CL_FALSE : CL_TRUE,
                offset,
                count,
                b->hostBuffer,
                0,
                NULL,
                &b->events[1]);
    
    if(status != CL_SUCCESS) 
    {
        printf( "Error: clEnqueueReadBuffer failed, status %d. (%s)\n", status, b->name);
    }


    b->pending = TO_HOST;
    if (!async)
        clFinishBufferTransfer (b, TO_HOST);


    CL_FINISH;
}

void clBufferToHost (clBuffer_t *b, int async)
{
    cl_int   status;

    if (!allowAsyncBuff)
        async = 0;
    else if (alwaysAsync)
        async = 1;

    clFinishBufferTransfer (b, TO_ALL);

    /* Enqueue readBuffer*/
    status = clEnqueueReadBuffer(
                commandQueue,
                b->clBuffer,
                async ? CL_FALSE : CL_TRUE,
                0,
                b->size,
                b->hostBuffer,
                0,
                NULL,
                &b->events[1]);
    
    if(status != CL_SUCCESS) 
    {
        printf( "Error: clEnqueueReadBuffer failed, status %d. (%s)\n", status, b->name);
    }


    b->pending = TO_HOST;
    if (!async)
        clFinishBufferTransfer (b, TO_HOST);


    CL_FINISH;
}

void clBufferToGPUPart (clBuffer_t *b, int async, size_t offset, size_t count)
{
    cl_int   status;

    offset *= b->elem_size;
    count *= b->elem_size;

    if (!allowAsyncBuff)
        async = 0;
    else if (alwaysAsync)
        async = 1;

    clFinishBufferTransfer (b, TO_ALL);

    uint64_t        start = ullTime();

    status = clEnqueueWriteBuffer(
                commandQueue,
                b->clBuffer,
                CL_TRUE,
                offset,
                count,
                b->hostBuffer + offset,
                0,
                NULL,
                &b->events[1]);
    
    if(status != CL_SUCCESS) 
    {
        printf( "Error: clEnqueueWriteBuffer failed. (%s)\n", b->name);
    }
    
    b->pending = TO_GPU;
    if (!async)
    {
        if (clFinishBufferTransfer (b, TO_GPU))
        {
            gpuTransToGpuLat += ullTime() - start;
            gpuTransToGpuCnt ++;
        }
    }

    CL_FINISH;
}


void clBufferToGPU (clBuffer_t *b, int async)
{
    cl_int   status;

    if (!allowAsyncBuff)
        async = 0;
    else if (alwaysAsync)
        async = 1;

    clFinishBufferTransfer (b, TO_ALL);

    uint64_t start = ullTime();

    /* Enqueue readBuffer*/
    status = clEnqueueWriteBuffer(
                commandQueue,
                b->clBuffer,
                CL_TRUE,
                0,
                b->size,
                b->hostBuffer,
                0,
                NULL,
                &b->events[1]);
    
    if(status != CL_SUCCESS) 
    {
        printf( "Error: clEnqueueWriteBuffer failed, status %d. (%s)\n", status, b->name);
    }
    
    b->pending = TO_GPU;
    if (!async)
    {
        if (clFinishBufferTransfer (b, TO_GPU))
        {
            gpuTransToGpuLat += ullTime() - start;
            gpuTransToGpuCnt ++;
        }
    }

    CL_FINISH;
}

cl_uint kernelBufferArg (cl_kernel k, int arg, clBuffer_t *b)
{
    cl_uint status;

    clFinishBufferTransfer (b, TO_GPU);
        
    status = clSetKernelArg(
                k, 
                arg, 
                sizeof(cl_mem), 
                (void *)&b->clBuffer);
    if(status != CL_SUCCESS) 
    { 
        printf("Error: Setting kernel argument. (buffer %s)\n", b->name);
    }
    return status;
}

cl_uint kernelIntArg (cl_kernel k, int arg, int *i)
{
    cl_uint status;

    status = clSetKernelArg(
                k, 
                arg, 
                sizeof(cl_int), 
                (void *)i);

    if(status != CL_SUCCESS) 
    { 
        printf("Error: Setting kernel argument %d to %d.\n", arg, *i);
    }
    return status;
}

size_t kernelMaxWg (cl_kernel *k)
{
    size_t rc = 0;
    int i;

    for (i = 0; i < sizeof(kernels) / sizeof(kernels[0]); i++)
    {
        if (kernels[i].kernel == k)
        {
            rc = kernels[i].maxWg;
            break;
        }
    }

    if (rc == 0)
        rc = maxWorkGroupSize;

    return rc;
}


static void runJobBuffer(void)
{
    cl_int   status;
    size_t globalThreads[1];

    globalThreads[0] = jobBufferEntries;
    
    kernelBufferArg (clRunJobs, 0, fieldBuffer[curField]);
    kernelBufferArg (clRunJobs, 1, CUR_TEMPR_BUF);
    kernelBufferArg (clRunJobs, 2, clWallFlags);
    kernelBufferArg (clRunJobs, 3, jobBuffer);

    jobBufferEntries = 0;

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 clRunJobs,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "clRunJobs");
        return;
    }
    CL_FINISH

}


void printWB(gpuParticleSet_t *pSet, int b)
{
    int i;

    clBufferToHost (pSet->particleSortedOff[b], 0);
    clBufferToHost (pSet->particleSortedIdx[b], 0);
    printf ("\n");
    for (i = 0; i < 100; i++)
    {
        printf ("%4d:%3d/%3d ", pSet->particleSortedIdxHost[i],
                pSet->particleSortedOffHost[i]%wid, pSet->particleSortedOffHost[i]/wid);
        if ((i+1)%8 == 0)
            printf ("\n");
    }
}

void compWb (gpuParticleSet_t *pSet, int b)
{
    int i;

    clBufferToHost (pSet->particleSortedOff[b], 0);
    clBufferToHost (CURRENT_POS(pSet), 0);
    printf ("\n");
    for (i = 0; i < pSet->particleCountAlgn; i++)
    {
        printf ("%3d/%3d:%3d/%3d ", pSet->particleSortedOffHost[i]%wid, pSet->particleSortedOffHost[i]/wid,
                (int)pSet->particleStartPos[i*2], (int)pSet->particleStartPos[i*2+1]);
        if ((i+1)%8 == 0)
            printf ("\n");
    }
}

void checkWB (gpuParticleSet_t *pSet, int b)
{
    int i;
    unsigned int idx;
    int last = -1;
    int haveEmpty = 0;
    int pixels = 0;
//    int top = 0;

    clBufferToHost (pSet->particleSortedOff[b], 0);
    clBufferToHost (pSet->particleSortedIdx[b], 0);
    clBufferToHost (CURRENT_POS(pSet), 0);
    for (i = 0; i < pSet->particleCountP2; i++)
    {
        idx = pSet->particleSortedIdxHost[i];

//        if (i < 10)
//            printf ("%d %d\n", i, idx);

        if (idx == MAXKEY)
        {        
            haveEmpty++;
            continue;
        }

        if (idx >= pSet->particleCountP2)
        {
            printf ("bad ID %d at pos %d\n", idx, i);
            continue;
        }


//        top = i;

        if (haveEmpty)
            printf ("%u: empty particle before good\n", i);

        if (last != -1)
        {
            if (pSet->particleSortedOffHost[i] < last)
            {
                printf ("%d:%d(%d)  last=%d\n",
                    idx,
                    pSet->particleSortedOffHost[i],
                    (int)pSet->particleStartPos[idx*2] + (int)pSet->particleStartPos[idx*2+1]*wid,
                    last
                    );
            }
            else if (pSet->particleSortedOffHost[i] > last)
            {
                pixels++;
            }
        }
        
        last = pSet->particleSortedOffHost[i];
    }
//        printf ("E: %d of %d, %d pixels, %d particles\n", haveEmpty, pSet->particleCountAlgn, pixels, top);
}




/* transfer openCl Wall buffer to GPU 
 */
void clWallUpdate (void)
{
    clWallUpdateFlag = 1;
}

void clColorMergeSet (float v)
{
    clParametersGet()->colorMerge = v;
    clParametersNotifyChange();
}

void clColorRestoreSet (float v)
{
    colorRestore = v;
}

void clGravVecSet (vec_t *v)
{
    clParameters_t *cp = clParametersGet();

    cp->grav.x = (float)(gravVec.x) / 1000.f;
    cp->grav.y = (float)(gravVec.y) / 1000.f;

    clParametersNotifyChange();
}

void clDecelSet (int decel)
{
    clParameters_t *cp = clParametersGet();

    cp->decel = 1.0 - (0.01 / (float)decel);

    clParametersNotifyChange();
}

clParameters_t *clParametersGet (void)
{
    if (clParameters == NULL)
    {
        clParameters = (clParameters_t*)aAlloc(sizeof(clParameters_t));
    }

    return clParameters;
}

void clParametersNotifyChange (void)
{
    clParametersChanged = 1;
}

void clUnInit (void)
{
    if (contextValid)
        clReleaseContext (context);

    contextValid = 0;
}

void doClFinish(void)
{
    clFinish (commandQueue);
}

void clSetLinkInterval (setting *s)
{
    clMkLinkInterval = GETVAL32(*s);
}

void clShowLinks (int on)
{
    if (on)
        clParametersGet()->flags |= CL_EFLAG_SHOWLINK;
    else
        clParametersGet()->flags &= ~CL_EFLAG_SHOWLINK;
    clParametersNotifyChange();
}

void getClStatistics (int show)
{
    cl_int status = 0;
    int arg = 0;
    size_t globalThreads[1];
    gpuParticleSet_t *pSet = &particleSet[movingPartSet];
    
    clearStatusBuffer (pSet);
    
    globalThreads[0] = pSet->particleCountAlgn;
    arg = 0;
    kernelBufferArg (particleStats, arg++, CURRENT_ATTR(pSet));
    kernelBufferArg (particleStats, arg++, CURRENT_POS(pSet));
    kernelBufferArg (particleStats, arg++, CURRENT_VEC(pSet));
    kernelBufferArg (particleStats, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 particleStats,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    CL_FINISH;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "particleStats");
        return;
    }

    globalThreads[0] = wid*hei;
    arg = 0;
    kernelBufferArg (getFieldStats, arg++, fieldBuffer[curField]);
    kernelBufferArg (getFieldStats, arg++, CUR_TEMPR_BUF);
    kernelBufferArg (getFieldStats, arg++, pSet->statusBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 getFieldStats,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    CL_FINISH;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", "getFieldStats");
        return;
    }

    clBufferToHost (pSet->statusBuffer, 0);
    
    pSet->clStatus->stats.speed /= (float)pSet->clStatus->stats.count;
    pSet->clStatus->stats.impuls /= (float)pSet->clStatus->stats.count;
    
    oldStats = currentStats;
    currentStats = pSet->clStatus->stats;
    oldFieldStats = currentFieldStats;
    currentFieldStats = pSet->clStatus->fieldStats;
    
    if (show)
    {
        if ((oldStats.count != 0) && (currentStats.count != 0))
        {
            printf ("Particles:\n"
                    " Count        : %-10d (%d)\n"
                    " Speed        : %f (%f)\n"
                    " Impuls       : %f (%f)\n"
                    ,
                    currentStats.count,
                    currentStats.count - oldStats.count,
                    currentStats.speed,
                    currentStats.speed - oldStats.speed,
                    currentStats.impuls,
                    currentStats.impuls - oldStats.impuls
                    );
        }
        
        if (getValue (&particleAttraction))
        {
            printf (
                    "Field:\n"
                    " Potential    : %f (%f)\n"
                    " Energy       : %f (%f)\n"
                    " Sum          : %f (%f)\n"
                    ,
                    currentFieldStats.potential,
                    currentFieldStats.potential - oldFieldStats.potential,
                    currentFieldStats.energy,
                    currentFieldStats.energy - oldFieldStats.energy,
                    currentFieldStats.potential + currentFieldStats.energy,
                    (currentFieldStats.potential + currentFieldStats.energy) -
                    (oldFieldStats.potential + oldFieldStats.energy)
                    );
        }
        if (getValue (&wallTempr))
        {
            printf (
                    "Wall:\n"
                    " Temperature    : %f (%f)\n"
                    ,
                    currentFieldStats.temperature,
                    currentFieldStats.temperature - oldFieldStats.temperature
                    );
        }
    }
}

char *clStatString(void)
{
    static char str[100];

    sprintf (str, "T:%.2f I:%.2f    ",
            currentFieldStats.temperature,
            currentStats.impuls);

    return str;
}


