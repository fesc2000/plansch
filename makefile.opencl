ifeq ($(OPENCLROOT),)
    OPENCLROOT=/usr
endif


CFLAGS	    += -DUSE_OPENCL -DUSE_OPENGL
INCDIRS     += -I$(OPENCLROOT)/include
#LIBDIRS	    += -L$(OPENCLROOT)/lib/$(LIBARCH)
LIBS	    += -lOpenCL
#LIBS	    += -ldl
#LIBDIRS    += -L/usr/lib
#LIBS	    += -lnvidia-opencl

# Intel ...
#LIBS += -lintelocl -lcl_logger -ltask_executor -ltbb -ltbbmalloc 
#LIBDIRS    += -L$(OPENCLROOT)/lib64

CLSRC	    = field.cl particles.cl collideElastic.cl collideElasticLink.cl sort.cl collide.cl blob.cl particleMove.cl particleMove1.cl particleMove2.cl
CLHDR       = opencl.h plansch.clh clshared.h clprivate.clh collideAll.clh
CLFILES	    = $(CLSRC) $(CLHDR)

ifneq ($(CLCC),)
CLBIN	    = $(CLSRC:.cl=.out)
ifeq ($(clc_options),)
clopt	    = -cl-mad-enable
else
clopt	    = $(clc_options)
endif
else
CLBIN	    =
endif

LIBS	    += $(OPENGL_LIBS)

GPU_PARTICLE_TYPES  = clwimp

## options
#
## Job processing done by GPU instead of CPU (experimental)
#
#CFLAGS      += GPU_JOB_PROCESSING

MOD_OBJECTS = opencl.o opencl_if.o opencl_field.o opencl_particles.o opengl.o oglSync.o clBitonic.o

kernels.h:  kernels.h.tmp
	    @diff -q kernels.h kernels.h.tmp || cp kernels.h.tmp kernels.h

kernels.h.tmp: $(CLSRC)
	    @echo '#ifdef _DECL_KERNELS' > kernels.h.tmp
	    @echo '#define _SCOPE' >> kernels.h.tmp
	    @echo '#else' >> kernels.h.tmp
	    @echo '#define _SCOPE extern' >> kernels.h.tmp
	    @echo '#endif' >> kernels.h.tmp
	    @grep -h '^KERNEL' $(CLSRC) | sed -e 's/KERNEL[^(]*(\([a-zA-Z0-9]*\).*/_SCOPE cl_kernel \1;/' >> kernels.h.tmp
	    @echo '#ifdef _DECL_KERNELS' >> kernels.h.tmp
	    @echo 'static clKernel_t kernels[] = {' >> kernels.h.tmp
	    @grep '^KERNEL' $(CLSRC) | sed -e 's/\(.*\):KERNEL[^(]*(\([a-zA-Z0-9]*\).*/ { "\1", "\2", \&\2 },/' >> kernels.h.tmp
	    @echo '};' >> kernels.h.tmp
	    @echo '#endif' >> kernels.h.tmp


$(MOD_OBJECTS:%.o=%.c):	$(CLHDR) kernels.h
	    @touch $@

include rules.mk

$(OBJDIR)/opencl.o:   kernels.h $(CLBIN)

$(CLBIN): %.out : %.cl
	    $(CLCC) --device_type GPU --cloptions "-I. -DDENSITY_TYPE=int -DADDFCT=atom_add $(clopt)" $< -o $@
