/*
Copyright (C) 2010 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


void markPixels (int x, int y);
void setPan(int nx, int ny, int end);
int needScreenUpdate(uint64_t tick);
int handleInput(void);
void clearBorderOn(void);
int bordersLocked(void);
void setThrottle (int);
int isThrottled(void);
void startParticleMovement (int start);
void doPan (vec_t *pStartPos, int end);
void setPartsPerPixel (int parts);
int expired (uint32_t *tick, int timeout, uint32_t current);


void makeNormals (int x1, int y1, int x2, int y2, int mode);
void updateNormals (int count);
int drawOperation (int x, int y, int action);
void setDrawFct (int f);
int getDrawFct ();
int doDrawWall (int x, int y, int dx, int dy);
int doDrawClear (int x, int y, int dx, int dy);

intptr_t lineCb (int x, int y, int dx, int dy, intptr_t col, intptr_t bg, intptr_t remove, intptr_t force);
intptr_t lineCbVis (int x, int y, int dx, int dy, intptr_t col, intptr_t bg, intptr_t remove);
void drawLineF (int x1, int y1, int x2, int y2, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4);
void drawPolyLineF (vec_t *cords, int nCords, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4);
void drawArrowF (int x1, int y1, int x2, int y2, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4);

void convertParticleType (particle *p, int t);
particle * newParticle (uint32 x, uint32 y, int vx, int vy);
particle * newParticle2 (uint32 x, uint32 y, int vx, int vy, int type, int charge, int mass, int size, uint32_t color);
void setBrick (int x, int y);

void setEmpty (int x, int y, int force);
void setPump (int x, int y, vec_t *pVec);
void selectButton (button *b, int select);
void selectPanelButtons (panel *pPanel, int sel);
SDL_Cursor * makeCursor (const char *image[]);
void frandomize (FLOAT dx, FLOAT dy, FLOAT len, vec_t *out);
void wipe();
void removeAllParticles();
void remgrav();
void slider (button *b, int action, int x, int y);
void sliderS (button *b, int action, int x, int y);
void expSlider (button * b, int action, int ox, int oy);
void asySlider (button * b, int action, int ox, int oy);
void onOffButton (button *b, int action, int x, int y);
void onOffButtonS (button *b, int action, int x, int y);
void onOffButtonDouble (button *b, int action, int x, int y);
void invertButton (button * b, int action, int x, int y);
void actionButtonHandler (button *b, int action, int x, int y);
button * makeButton (panel *pPanel, VOIDFUNCPTR handler, int wx, int wy, char **pixmap, char *help, intptr_t userData, setting *s, int connection);
button * addSubButton (button *parent, int wx, int wy, char **pixmap, char *help, intptr_t userData, int connection);

void modifySetting (setting *pSetting, int64_t newValue, int toArray);
int markSetting (setting *pSetting, int glob);

void drawStencilSprite (stencil_t *st, int mouseX, int mouseY, int clear);
void saveSelectedArea (button *b);
SDL_Surface *stencilBitmap(stencil_t *st);
SDL_Surface *stencilData(stencil_t *st);
void rotateStencil (stencil_t *st,int deg, int zoom, int rel);
screeninfo * find_adjacent (int px, int py, int x1, int y1, int w, int h, FUNCPTR checkFP);
extern void doUpdateScreen (void);

void *calloutNew (void);
void calloutSched (void *id, int time, FUNCPTR fct, void *arg);
void calloutUnsched (void *id);
void processCalloutQueue (void);
void processCalloutQueueInt (void);

void drawFctStencilDrawsParticles (int paintParticles);

int oglDedicatedThread(void);
int oglDedicatedThreadSet (int val);
void gpuSignalScreenUpdateDone();
void gpuWaitScreenUpdateDone ();
void gpuSignalDone();
void textPlotC (int x, int y, char *text, int col, int pico);
void textPlot (int x, int y, char *text);
void ioModeButton (button *b);
int handleButton (int action, int x, int y);

char **stencilPixmap (int id);

void fileSelection (int on, fileRep *r);
int inSelection (int x, int y);
void drawSelectionBox (int clear);
void redrawSelectionBox ();

void selectStencil (int sten);
void redrawButton (button *pButton);

void addRecord (tape *t, record *r, recordInfo *i);
void tapeStart(tape *t);
void tapeStop(tape *t);
void tapeClear(tape *t);
SDL_Surface *tapeToThumbnail (tape *t, int rct);
void tapeSave (tape *t, char *fname, int withThumbnail);
int tapeLoad (tape *t, char *fname);
void recordNewParticle (tape *t, particle *p);
void recordSetting (tape *t, setting *s, int glob);
void recordBrick (tape *t, int x, int y, int dens, int heat, int friction, int charge);
void recordErase (tape *t, int x, int y);
void recordRemoveParticle (tape *t, particle *p);
void recordStartDraw (tape *t, int start, int ptype);
void playbackItem (tape *t, int tapeIndex, vec_t *pOffset, vec_t *pCenter, int zoom, int rot);
void playbackAll (tape *t, vec_t *pOffset, vec_t *pCenter, int zoom, int rot, unsigned int mask);
void eraseTape (tape *t);
int tapeEmpty (tape *t);
void saveSelection (SDL_Rect *pRect, int saveWall, int saveParticles);
int loadFile (fileRep *r, int index, SDL_Surface **stencil, SDL_Surface **data, tape *pTape);
int tapeOperation (void);
int playbackComplete();

void removeParticleFromTape (tape *t, particle *p);


void stencilOrgSize (stencil_t *st, int *w, int *h);
void warpParticles (screeninfo *, screeninfo *);
int warpParticle (screeninfo *, screeninfo *, particle *);
int shiftPressed();
int ctrlPressed();
void pushClear (int, int, int);
void clearMarker (int, int, int, int);

void spreadSS1(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last);
void spreadSS1Scr(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last);
void spreadSS1_s3(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last);
void spreadSS1Scr_s3(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last);

void gravShow();
void updateAllBlobs();
void probeShow();
void probeClear();
void probeStart(int);
void clearArrow();
void showArrow (int, int);
int drawingSelection();
void seekPipe (int, int, int);
void redrawScreen();
void initColors (SDL_Color *colors);
void makeButtons();
int playbackIter (int relative);
void doDrawParticles (int, int, int, int, int, int);
int getSelection (SDL_Rect*);
void normalize (int*, int*);
void probe(int, int);
void buttonColor (button * b, int col, int overwrite);
void probeRestoreActionButton();
int floodMode();
int pushDraw(int,int, int);
#ifdef THREAD_DIST_HORIZ
int threadGetX(int);
#else
int threadGetY(int);
#endif
int removePFromThread(int, particle *);
void addPToThread(int, particle *);
void stencilPos (stencil_t *st, int*, int*);
void stencilSize (stencil_t *st, int*, int*);
void stencilEdge (stencil_t *st, int*, int*);
void stencilCenter (stencil_t *st, int*, int*);
void lockStencilCenter (stencil_t *st, int);
void stencilWalk (stencil_t *st, FUNCPTR callout, void *arg1, void *arg2);
int stencilZoom(stencil_t *st );
int stencilRotation(stencil_t *st);
int dirPixel (int, int, int, int);
void enablePanel (panel *pPanel, int enable);
void setCursor (void *pixmap);
void setButtonPixmap (button *b, char **pixmap);
void setButtonSurface (button *b, SDL_Surface *s);
int drawingBigStencil();
void updateSelPanels();
void clearStencil(stencil_t *st);
void enableButton (button*, int);
void endSelectionMode(int);
void setArrowVec(int,int);
void enableArrow(int);
int playbackInfo (int*, int*);
void loadTape();
void saveTape();
void startPlayback(int);
void startRecord(int);
void init_widgets();
void initSelectPanel (panel*);
void initFileSelectorPanel (panel*);
void buttonBorderColor (button*, int);
void drawButton (button*, int);
void removeSaveFile (fileRep *r, int);
void mkFileName (char *fname, fileRep *r, char *prefix, char *suffix, int index);
void newStencil (stencil_t *st, SDL_Surface *bitmap, SDL_Surface *data, tape *);
void showPanel (panel*, int);
int hidePanelSection ();
void currentStencilToCursor();
void buttonSelectColor (button*, int);
void stencil_swap(stencil_t *st, int);
void initFsButtons(int);
void makeStencil (stencil_t *st, SDL_Rect*);
int pixelColorGet(int,int);
int siColor(screeninfo*);
int pixelColorIdx(int,int);
int siColorIdx(screeninfo*);
void drawBox(int,int,int,int,FUNCPTR,intptr_t,intptr_t,intptr_t,intptr_t);
void makeSettingPanel (button *, setting*);
int handleSettingEvent (button*, int);
int getSaveFile(char*, int l, fileRep *r);
void recordScreenSection (tape *Tape, SDL_Rect *pRect);
int siEraseParticles (screeninfo *pSi, unsigned int flags);
int64_t getValue (setting*);
value_u *getValue_u (setting *pSetting);
float getValueNorm (setting *pSetting);
float getValueFlt (setting *pSetting);
void autoFilePanel (int ena);
void updateNeighbours (screeninfo *pSi, int diff);

void removeParticle (particle *p, uint flags );
void removeParticleDelayed (particle *p);
void probeHistory (int sx, int sy, int w, int h);

int currentDrawMode();

void setDrawMode (int mode);
int floodOngoing();
void flood (int x, int y, FUNCPTR action, int p1, int adj8);
SDL_Rect *getFloodRect ();

void createAsyncParticles();
void cloneParticleAsync (particle *p, int mass, int vx, int vy);
void createParticleAsync (vec_t *pos, 
                vec_t *vec,        
                int charge, 
                int mass, 
                int type, 
                int size,
                int lifetime,
                uint32_t color,
                      particle *linkto,
                int count);

void drawOpSelect (int mode);
void enableDrPanels(int ena);
 
int grav_cal_8(int w, int h, int iterations, short *dens,  char *fb, short *dx, short *dy);
int grav_cal_32(int w, int h, int iterations, short *dens,  char *fb, short *dx, short *dy);
void gpuSignalDone();
int gpuWaitForStart();
void gpuWait(void);
void gpuStart(int);
int grav_gpu_init(intptr_t arg);
int gpuPresent();

SDL_Rect *statBox (void);

void linkInit();
int openLinks (particle *p, int passive);
int openLinksActive (particle *p);
void linkParticles(particle *p1, particle *p2, int passive, uint32 data);
void linkPartReset (particle *p);
int _isLinked (particle *p1, particle *p2);
pLinks *linkPartners (particle *p);
void unlinkAll (particle *p);
void unlinkTwo (particle *p1, particle *p2);
void closeOpenLinks (particle *p, int num);
void activateClosedLinks (particle *p);
int motionCapture (int button);

void cbApplyToAll (setting *s);

void applySettingsToPixel (screeninfo *);
void applySettingToParticle (particle *,setting *s);



void moveStart(int x, int y);
void moveEnd ();
int addMoveElement (int type, void *object);
void handleMove (int x, int y);

void updateDrButtons (int stencilMask, int dflStencil, int drawModeMask, int dflMode);
void removeElementFromMoveList (void *e);

void showHelp (int,int);
void setButtonHelp (button *, char*);
char *getHelpString (char*);
char *getTooltipString (char*);
void renderTextBox (int, int, char*, char*, SDL_Surface **back, SDL_Rect *r);
void escConnect (FUNCPTR);
void initSettings();

void updateParticleColors();
uint32_t currentParticleColor (int type, int charge);


unsigned int planschTime();
unsigned int systemLoops();
void freezeTime();
void unfreezeTime();
unsigned int systemTime();
void systemTimeTick();

extern float ran_gaussian (float sigma);

void initParticleCq();
void unschedParticle (particle *p);
void unschedAllParticles();
void schedParticle (particle *p, uint32 lifetime, FUNCPTR action, intptr_t arg);
void processParticleCq();

void toBorder (vec_t*, vec_t*);
void toNextBorder (vec_t*, vec_t*);

int tcBoundaryColor (screeninfo *);

FT_TYPE *flagFieldAlloc();
FT_TYPE *scrFlagFieldAlloc();

void setNewParticle (particle *p);
void setNewParticle (particle *p);
int hasNewParticle (screeninfo *pSi);
void clearNewParticleList (void);

void initParticles();
void mathInit (void);

int circleCb (int x, int y, int col, int bg, int remove);
void drawCircle (int cx, int cy, int rx, int ry, int step, int off, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4);
void drawFilledCircle (int cx, int cy, int rx, int ry, int step, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4);
void currentStencilRadius(int*, int*);
void buttonSetRandom (button *b, int random);

void removeCurrentPoly ();
void removeAllPolys ();
void drawCurrentPoly (int clear);
void drawAllPolys();
void linesInit();
void polyStart (int sx, int sy, int redraw);
void polyMoveEdge (int sx, int sy, int autoMode);
int polyFix ();
int polyFinish ();
int pHitLine (particle *p, vec_t *newVec, int off, threadData *td, screeninfo *pSi);
int polyDrawing();

void initBackStore ();
void clearBackStore();

int myfilledPolygonColor(SDL_Surface * dst, const Sint16 * vx, const Sint16 * vy, int n, Uint32 color);
int myPixelColor(SDL_Surface * dst, Sint16 x, Sint16 y, Uint32 color);
int myhlineColorStore(SDL_Surface * dst, Sint16 x1, Sint16 x2, Sint16 y, Uint32 color);
int myvlineColor(SDL_Surface * dst, Sint16 x, Sint16 y1, Sint16 y2, Uint32 color);
int mylineColor(SDL_Surface * dst, Sint16 x1, Sint16 y1, Sint16 x2, Sint16 y2, Uint32 color);
int mypolygonColor(SDL_Surface * dst, const Sint16 * vx, const Sint16 * vy, int n, Uint32 color);


void initScreen (int menuHei, int fullscreen);
void clipToDrawArea ();
void clipToButtonArea ();
void clipDisable();

int notInSelectionGroup (particle *p);
void drawFctMark (int x, int y, int act, int mode);
void markClear ();
void markParticle (particle *p, markAction what, int withSiblings);
void markInit();
void cycleColors();
int trackSurface(screeninfo *pStartSi, vec_t **ta, int all);
void markRemoveCb (particle *p);
int particlesMarked();
int pIsMarked (particle *p);
void walkLinks (particle *p, FUNCPTR cb, void *arg);
void walkMarkedParticles (FUNCPTR cb, void *arg1);
void walkAllParticles (FUNCPTR cb, intptr_t arg);
void markAll();
int haveSelection();
void toolTipMove (int x, int y);

void initPolys();
scrPoly *polyNew (int count, int coordType, uint32 dflColor);
void polyAddCoord (scrPoly *po, int x, int y, uint32 col);
void polyChangeCoord (scrPoly *po, int num, int x, int y, uint32 col);
void polyClear (scrPoly *po);
void polySetCoords (scrPoly *po, int num, vec_t *coords, uint32 *colors);
void polyMove (scrPoly *po, int x, int y);
void polyShow (scrPoly *po);
void polyHide (scrPoly *po);
void polyChangeFillColor (scrPoly *, uint32_t);
void walkWorldObjects (FUNCPTR cb, void *arg);
void walkScreenObjects (FUNCPTR cb, void *arg);
void walkCursor(int ix, int iy, FUNCPTR cb, void *arg);

int to_poly( int n, fvec_t *v, triangle **pList );
void openglInitScreen (intptr_t fullscreen);
void oglWork (int scrUpdate);
void oglWorkThread (void);

int doExit();
void polyDelete (scrPoly *po);
void polyChangeFlags (scrPoly *po, int flags, int mask);
int makeArrowPoly (vec_t *poly, int sx, int sy, int dx, int dy);
void resetArrow (int x, int y);
void updateArrow (int x, int y);
void fixArrow ();
void getArrowVec (int *px, int *py);
void openglScreenUpdate() ;
void oglClearScreen();

void redrawScreenObjects ();
void memInit();
int oglInitDone();
const float rads (float dx, float dy);
void polyChangeColor (scrPoly *po, int o, uint32 color);
void onOffGroupS (button * b, int action, int x, int y);
const int degs (float dx, float dy);
const int deg_sub(int d1, int d2);
void rasLine (int x1, int y1, int x2, int y2, int w, int h, FUNCPTR cb, intptr_t arg1 );
int clip(int w, int h, int * x1, int * y1, int * x2, int * y2);
scrPoly *scrMarker (int x, int y, int rad, uint col);
scrPoly *scrSimpleArrow (int x, int y, int dx, int dy, uint col);
void linkSetCount (particle *p, int count);
void seekBorder (int x, int y, int dx, int dy);
void applyDefaultSettings();
void vecCutMode (int m);
int vectorizePixels (int x, int y);
void groupBorderColor (button * b, int col);
void clTask(int wid, int hei, int iter, DENSITY_TYPE **dens, char *fb, GRAV_TYPE **dx, GRAV_TYPE **dy);
int linkGetCount (particle *p);
int compareParticle (particle *p1, particle *p2);
int makeAnglePoly (vec_t *poly, int sx, int sy, int dx, int dy, int angle, int radius);
Uint32 rgb32ToSurfaceColor (SDL_Surface *s, Uint32 col);
void initStencil(void);
void hideArrow (int);
void setArrowAngle (int angle);
int getArrowAngle (void);
void recordAccel (tape *t, int x, int y, int dx, int dy);
int tapeToStencil (stencil_t *st, tape *t, int bx, int by);
void recordLine (tape *t, int x, int y, recordLine_t what);
int approachesLine (int dx, int dy, int off);
void openglRestoreContext (void);
void openglReleaseContext (void);
void registerRemoveCallback (FUNCPTR fct, void *arg);
void registerCreateCallback (FUNCPTR fct, void *arg);
void broadcastRemove (particle *p);
int pdescFind (char *name);
int isDeleted (particle *p);
void linkParticlesWeak(particle *p1, particle *p2, uint32 data);
void setLinkPartners (particle *p, pLinks *pl);
void siMarkClear();
void siWalkMarked (FUNCPTR fct, intptr_t arg);
int siMarkedHasWimps();
void siMark (screeninfo *pSi);
void applySettingsToMarked (int x, int y);
int threadGet (int type, int x, int y);
void doClFinish(void);
void clUnInit (void);
void clDoGrav(int);
void registerSettingCallback (FUNCPTR fct, void *arg);
scrPoly *dbgMarker (int x, int y, int rad, uint col, char *name);
scrPoly *dbgMarkerBox (int x, int y, int rad, uint col, char *name);
scrPoly *dbgLine (int x1, int y1, int x2, int y2, uint col, char *name);
void clearMarkers (char *wildcard);
char *handleStr(void *h);
scrPoly *scrLine (int x1, int y1, int x2, int y2, uint col);
scrPoly *scrBox (int x, int y, int len, uint col);
int semTake (sem_t *sem, int wait);
int semGive (sem_t * sem);
int semFlush (sem_t * sem);
sem_t *  semMCreate(int flags);
sem_t *  semBCreate(int flags, int state);
sem_t *  semCCreate(int flags, int state);
const int bitsSet (uint val, int bsize);
const int highestBit (uint val, int bsize);
const int lowestBit (uint val, int bsize);
int clNumParticles();
const int sameSlope (const vec_t *v1, const vec_t *v2, int f);
void drawLineIF (int x1, int y1, int x2, int y2, int step, FUNCPTR cb, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4);
particle *nearestNewParticle (int ix, int iy, int rad);
panel * makePanel (char *name, int gid, int startx, int starty, int raster, int spacing);
panel * makeSubPanel (button *parent, int raster, int spacing, int flags);
void panelRCContinue (panel *p, int wx, int wy, int attr);
void panelRCNext(panel *p, int spacing);
void connectPanel (panel *p, panel *to, int side);
void buttonDrawPixel (button *b, int x, int y, unsigned int color);
void buttonDrawPixelB (button *b, int x, int y, unsigned int color);
unsigned int buttonGetPixel (button *b, int x, int y);
unsigned int buttonGetRGB (button *b, int x, int y);
void movePanel (panel *p, int x, int y);
void clearPosCache(void);
int dflNumLinks (particle *p);
void showCircle (int ix, int iy);
int mouseButtonPressed(int b);
int circleStencilStarted(void);
void stopCircleStencil (void);
void fillButton (button *b, int miny, int maxy, int color, int array);
void buttonDependsOn (button *b, setting *s, int disable);
void setPanelMax (panel *p, int maxx, int maxy);
void doButtonDependencies (setting *s);
void clearCurrentButtonSelection (void);
void requestScreenUpdate();
int escapePressed();
int worldObjectsChanged();
void clParticles(int, int);
setting *hashToSetting (uint32 hash);
uint32 namesToHash (char *str, char *str2);
void maskRandomValue (setting *s, int val, int mask, int all);
int randomSetting (setting *pSetting, int random, int toArray);
int valueIsMasked (setting *s, int val);
void rndRange (setting *pSetting, int ena, int rndMin, int rndMax, int toArray);
void cloneSettingToArray (setting *pSetting);
scrPoly *scrRect (int x, int y, int w, int h, uint col);
void *clParticlePosGet ();
void *clGravBufferGet();
void showWallControls (int mode);
void processClJobQueue();
void processClJobQueueInt();
void clSchedKickFieldAt (vec_t *pos, int value);
void clSchedHeatAt (uint off, int value);
void clFixDensArrayUpdate(void);
surface_t * pmToSurface (char **pm);
int oglInitSurface (surface_t *s);
surface_t *currentCursor();
void clFollowStop(void);
void clParticleFollow (int stop, int ix, int iy);

SDL_Surface *IMG_ReadXPMFromArray(char **xpm);
particle *nearestParticle (int ix, int iy, int rad, FUNCPTR filter);
void leaveParticle (particle *p, FUNCPTR followFct, void *fctArg);
void followParticle (particle *p, FUNCPTR followFct, void *fctArg);
void followParticles (void);
void activeNClosedLinks (particle *p, int n);
int drawOpInternc(void);
void clWallSet (int x, int y);
int availLinks (particle *p);
particle *nearestParticleSt (int ix, int iy, FUNCPTR filter);
void activatePtype (int pt, int activate);
void setParticleColor (particle *p, uint32_t argb);
void walkStencilParticles(int ix, int iy, FUNCPTR cb, void *arg);
int linkedTogether (particle *p1, particle *p2);
int checkButtonPixel (button *b, int sx, int sy);
int checkButtonPixelB (button *b, int sx, int sy);
int hasLink (particle *p);
void moveInit(void);
void togglePanelGid(void);
void setCurrentPanelGid(int);
int getCurrentPanelGid(void);
void pushButton (button * b, int action, int x, int y);
void reactivateAll ();
void tmpDeactivateAllExcept (panel *p);
void buttonBgColor (button *b, uint32_t argb);
void nullButton (button * b, int action, int ox, int oy);
void defaultParticleColor(particle *p);
void setParticleColor (particle *p, uint32_t argb);
uint32_t getColorSetting (int type, int charge);

void setApplyToAll (int ena);
void pickColor (setting *sl);
int pickerIsActive (button *b);
void initColorPicker (int gid);
void buttonColorBg (setting *s);
float cvToHue (int cv);
void encodeHSLA (colorVal_t *cv, float hue, float sat, float lum, float alpha);
uint32_t colorSettingToARGB (int val);
uint32_t getParticleColorARGB (particle *p);
void markButton (button *b, int mark, int array);
void encodeARGB (colorVal_t *cv, float a, float r, float g, float b);
int RGB2HSL (float r, float g, float b, float*h, float *s, float *l);

uint32_t pickerMask(void);
void pickerSetARGB (float a, float r, float g, float b);
button *buttonFindByName (char *name);
void highlightParticle (particle *p);
void deleteParticles (void);
void exitPicker (void);
void keyCapture (FUNCPTR cb, void *cbArg);
void keyDownCapture (char key, int ctrl, int shift);
void reorderParticles (void);
void setParticleColorARGB (particle *p, uint32_t argb, int permanent, uint32_t mask);
void randomizeColorVal(colorVal_t *cv, int);
void clTryPinned(int);
int keyCaptureActive();
void randomizeParticles (void);
int getLine (int off, line_t *line);
int enginesStopped();
void walkStencilCoordinates(int ix, int iy, FUNCPTR cb, void *arg);

particle *dummyPart(void);
int isDummy(particle *p);

mPartTpl_t *mptFind (char *name);
void initMpSelector(button *b1);
mPartTpl_t *getMpTemplate(void);
void initMpTemplates (void);
void setMpTemplate (int index);
mPartTpl_t *getMpTemplate(void);
void showMpObjectButton (int show);


void makeParticleVec (vec_t *vec, int angle, int *dx, int *dy);
void useFrozenSet (void *useKey);
void removeFrozenSet (void *key);
void freezeParticleSettings (void *key);

void createWell (int, int);
void removeWell (int, int);
void completeWell(void);

void clParticleChattrSelected (int ix, int iy);
void clParticleMark (int add, int ix, int iy);

#ifdef USE_OPENCL

void clParameterCb (setting *s);
void clSync(void);
void renderClParticles(int set);
void clWork (int);
int clParticleVboGet(int set, GLuint *pVbo, GLuint *pColorVbo);
GLuint clParticleVecVboGet(int set);
void *clParticlePosGet (int set);
void *clGravBufferGet();
void clParticleVboRelease(int set);
void clParticleVecVboRelease(int set);
void clParticleColorVboRelease(int set);
int clNumParticles(int set);
void clSchedClearTrails (void);
void clSchedStartGrab (int ix, int iy);
void clMoveGrab (int x, int y);
void clSchedStopGrab(void);
void clMarkCursorPixels (int x, int y, int mark);
void clSetLinkInterval (setting *s);
int clSchedMarkAt (int ix, int iy, int mark);
void clSchedMarkAll(int mark);
void clSchedDeleteStart(void);
void clTransferSiMarker (void);
void clSchedChattrMarked (uint32_t argb, 
                     signed char charge, 
                     unsigned char mass, 
                     unsigned char links,
                     vec_t *pos,
                     unsigned char size,
                     int flags);
void clSchedChattrAll (uint32_t argb, 
                     signed char charge, 
                     unsigned char mass, 
                     unsigned char links,
                     SDL_Rect *r,
                     unsigned char size,
                     int flags);
void clParticleChattrSelected (int ix, int iy);
void clApplyParticleSetting (setting *s, SDL_Rect *r);
void clSchedClearAllParticles (void);
void clDecelSet (int decel);
void clGravVecSet (vec_t *v);
void clShowLinks(int on);
int clSchedNewParticle (vec_t *pos, vec_t *vec, int charge, int mass, uint32_t col, int links, uint32_t flags, unsigned char size);
void clColorMergeSet (float v);
void clColorRestoreSet (float v);
void clCommitJobs(void);
void clSchedMarkParticles (int flags, vec_t *ipos);
void clSchedClearNewFlags (void);
void clSchedMkblob (void);

#endif
#ifdef USE_OPENGL
int oglStop (void);
#endif


void setAlpha (colorVal_t *cv, float a);
float getAlpha (colorVal_t *cv);
void getColorWheelValue(float dx, float dy, float *outH, float *outS);
void randomizeColorVal (colorVal_t *cv, int box);
void encodeHSLA (colorVal_t *cv, float hue, float sat, float lum, float alpha);
void encodeARGB (colorVal_t *cv, float a, float r, float g, float b);
void encodeColorVec (colorVal_t *cv, float dx, float dy, float lum, float alpha);
void decodeColorVec (colorVal_t *cv, float *dx, float *dy, float *lum,
                     float *alpha);
void cvRGBUpdate (colorVal_t *cv);

void showGlCursor (int x, int y);
char *clStatString(void);
