/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define SEL_START   0
#define SEL_CLEAR   1
#define SEL_STOP    2

button *buttonSelector;
button *buttonFlipH;
button *buttonFlipV;
button *buttonCopy;
button *buttonCut;
button *buttonSave;

static vec_t selectStart;
static vec_t lastSelection;
static int selectionStarted = 0;
static int selectionVisible = 0;
static uint32 lCount = 0;
static int savedDrawFct = -1;
static int dflBoxColor = COL_RED;
#ifndef USE_OPENGL
static int boxColorA = 3;
#else
static int boxColorA = 0x60;
#endif
int selectionFixed = 1;

static scrPoly *selbox;

SDL_Rect selection;

void *calloutSelect = NULL;

void updateSelPanels (void)
{
    int        showResize = 0;
    int showCutCopySave = 0;
    SDL_Rect r;

    if (getDrawFct() == DO_STENCIL)
    {
        showResize = 1;
    }
    if ((getDrawFct() == DO_SELECTOR) &&
             getSelection(&r))
    {
        showCutCopySave = 1;
    }

    enableButton (buttonFlipH, showResize);
    enableButton (buttonFlipV, showResize);
    enableButton (buttonCopy, showCutCopySave);
    enableButton (buttonCut, showCutCopySave);
    enableButton (buttonSave, showCutCopySave);
}

void saveDrawMode ()
{
    savedDrawFct = getDrawFct();

}
void restoreDrawMode ()
{
    if (savedDrawFct != -1)
        setDrawFct (savedDrawFct);

    savedDrawFct = -1;
        
    currentStencilToCursor();

    updateSelPanels();
}


void startSelectionBox (int start)
{
    static int started = -1;

    if (start == started)
        return;

    if (calloutSelect == NULL)
        calloutSelect = calloutNew();

    started = start;

    switch (start)
    {
        case SEL_START:
            saveDrawMode ();

            enableDrPanels (0);
            setDrawFct (DO_SELECTOR);
            setCursor ((void *) pixel_cur_xpm);

            selectButton (buttonSelector, 1);
            break;

        case SEL_STOP:
            drawSelectionBox (1);
  
            dflBoxColor = COL_LINE;
            CH_SET_A(dflBoxColor, boxColorA);


            selectionVisible = 1;
            redrawSelectionBox();

            enableDrPanels (1);
            restoreDrawMode();
            selectionFixed = 1;

            selectButton (buttonSelector, 1);
            break;
        
        case SEL_CLEAR:
            drawSelectionBox (1);
            calloutUnsched (calloutSelect);
            enableDrPanels (1);
            restoreDrawMode();

            selectButton (buttonSelector, 0);
            break;
    }
}

void flipH (button * b)
{
    stencil_swap (NULL, 1);
    drawStencilSprite (NULL, wid/2, hei/2, 0);
}

void flipV (button * b)
{
    stencil_swap (NULL, 0);
    drawStencilSprite (NULL, wid/2, hei/2, 0);
}

void saveSelectedArea (button * b)
{
    SDL_Rect r;

    if (!getSelection(&r))
    {
        return;
    }

    saveSelection (&r, 1, 1);

    initFsButtons (0);
}

void copySelectedArea (button * b)
{
    SDL_Rect r;

    if (!getSelection(&r))
    {
        return;
    }

    startSelectionBox (SEL_CLEAR);

    makeStencil (NULL, &r);

    drawOpSelect (DO_STENCIL_NOFS);

    updateSelPanels();
}

void cutSelectedArea (button * b)
{
    int x, y;
    SDL_Rect r;

    if (!getSelection(&r))
    {
        return;
    }

    startSelectionBox (SEL_CLEAR);
    makeStencil (NULL, &r);

    for (x = r.x; x < r.x + r.w; x++)
    {
        for (y = r.y; y < r.y + r.h; y++)
        {
            setEmpty (x, y, 1);
            siEraseParticles (&siArray[x + y*wid], ERASE_IMM);
        }
    }
    makeNormals (r.x - 16, r.y - 16, r.x + r.w + 16, r.y + r.h + 16, NORM_SIMPLE_SAVE);

    updateSelPanels();
    drawOpSelect (DO_STENCIL_NOFS);

}

int getSelection (SDL_Rect *r)
{
    if (!selectionVisible)
    {
        r->x = 0;
        r->y = 0;
        r->w = wid;
        r->h = hei;
        
        return 0;
    }

    *r = selection;

    return 1;
}

int haveSelection()
{
    return selectionVisible;
}

void getSelectionOrAll (SDL_Rect *r)
{
    if (!selectionVisible)
    {
        r->x = r->y = 0;
        r->w = wid;
        r->h = hei;

        return;
    }

    *r = selection;
}

int inSelection (int x, int y)
{
    if (!selectionVisible)
    {
        if ((x < 0) || (x >= wid) || (y < 0) || (y >= hei))
            return 0;
        return 1;
    }

    if ((x < selection.x) || (x >= selection.x + selection.w) ||
        (y < selection.y) || (y >= selection.y + selection.h))
    {
        return 0;
    }

    return 1;
}

intptr_t selCb (int x, int y, int dx, int dy, int col, int bg, int remove)
{
    lCount++;

    if ((x < 0) || (x >= options->scrWid) || (y < 0) || (y >= options->scrHei))
        return 1;

    if (remove)
    {
        SCR_SET (x,y,pixelColorGet(x,y));
    }
    else
    {
        if ((lCount & 0xf) < 8)
            SCR_SET (x, y, col);
    }

    return 1;
}


void drawSelectionBox (int clear)
{
    vec_t cords[4];
    uint32 cols[4];

    if (clear)
    {
        polyHide (selbox);
        selectionVisible = 0;
        return;
    }

    cords[0].d.x = selectStart.d.x;
    cords[0].d.y = selectStart.d.y;
    cords[1].d.x = lastSelection.d.x;
    cords[1].d.y = selectStart.d.y;
    cords[2].d.x = lastSelection.d.x;
    cords[2].d.y = lastSelection.d.y;
    cords[3].d.x = selectStart.d.x;
    cords[3].d.y = lastSelection.d.y;
    cols[0] = cols[1] = cols[2] = cols[3] = dflBoxColor;

    polySetCoords (selbox, 4, cords, cols);
    polyShow (selbox);

    selectionVisible = 1;
}

void redrawSelectionBox ()
{
    if (!selectionVisible)
        return;

    drawSelectionBox (1);
    drawSelectionBox (0);
    calloutSched (calloutSelect, 500, (FUNCPTR)redrawSelectionBox, (void*)0);
}

int drawingSelection()
{
    return (getDrawFct() == DO_SELECTOR);
}


void drawFctSelector (int x, int y, int act)
{
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);

    CHECK_DRAWING

    if (mouseButton != 1)
        return;

    if (event == BUTTON_PRESS)
    {
        dflBoxColor = COL_RED;
        CH_SET_A(dflBoxColor, boxColorA);

        selectStart.d.x = x;
        selectStart.d.y = y;

        lastSelection = selectStart;

        selectionStarted = 1;
        selectionFixed = 0;

        drawSelectionBox (0);

        goto out;
    }

    if (selectionStarted && (event == BUTTON_MOVE))
    {
        lastSelection.d.x = x;
        lastSelection.d.y = y;

        drawSelectionBox (0);
    }

    if (event == BUTTON_RELEASE)
    {
        selectionStarted = 0;

        if ((selectStart.d.x == lastSelection.d.x) &&
            (selectStart.d.y == lastSelection.d.y))
        {
            selectStart.d.x = INTERNC(selection.x);
            selectStart.d.y = INTERNC(selection.y);
            lastSelection.d.x = selectStart.d.x + INTERNC(selection.w) - 1;
            lastSelection.d.y = selectStart.d.y + INTERNC(selection.h) - 1;

            startSelectionBox (SEL_STOP);
        }
        redrawSelectionBox();
    }

    selection.x = MIN(S_SCREENC(selectStart.d.x), S_SCREENC(lastSelection.d.x));
    selection.y = MIN(S_SCREENC(selectStart.d.y), S_SCREENC(lastSelection.d.y));
    selection.w = abs(S_SCREENC(selectStart.d.x) - S_SCREENC(lastSelection.d.x))+1;
    selection.h = abs(S_SCREENC(selectStart.d.y) - S_SCREENC(lastSelection.d.y))+1;

out:
    updateSelPanels();
}

void selectorButtonHandler (button * b)
{
    if (b->selected)
    {
        startSelectionBox (SEL_CLEAR);
    }
    else
    {
        startSelectionBox (SEL_START);
    }
}


void initSelectPanel (panel *pPanel)
{
#ifndef USE_OPENGL
    selbox = polyNew (4, 0, COL_LINE);
#else
    selbox = polyNew (4, POLY_CONVEX|POLY_FILLED, COL_LINE);
//    selbox = polyNew (4, 0, COL_LINE);
#endif

    buttonSave = makeButton (pPanel, actionButtonHandler, 32, 32, saveSelection_pm, "Save selected area", (intptr_t)saveSelectedArea, NULL, B_NEXT);
    buttonCut = makeButton (pPanel, actionButtonHandler, 32, 32, cut_pm, "Cut selected area", (intptr_t)cutSelectedArea, NULL, B_NEXT);
    buttonCopy = makeButton (pPanel, actionButtonHandler, 32, 32, copy_pm, "Copy selected area", (intptr_t)copySelectedArea, NULL, B_NEXT);
    buttonFlipV = makeButton (pPanel, actionButtonHandler, 32, 32, flipV_pm, "Flip vertically", (intptr_t)flipV, NULL, B_NEXT);
    buttonFlipH = makeButton (pPanel, actionButtonHandler, 32, 32, flipH_pm, "Flip horizontally", (intptr_t)flipH, NULL, B_NEXT);
    buttonSelector = makeButton (pPanel, actionButtonHandler, 32, 32, select_pm, "Select screen area", (intptr_t)selectorButtonHandler, NULL, B_NEXT);
}
