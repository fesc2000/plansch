/*
Copyright (C) 2010 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"


static int             numMarked;
static particle             **marked;

FT_TYPE                     *isMarked;        /* flag field */

static int             groupSize[9];
static particle             **groups[9];

static void             *markShowCallout;

void markShow ()
{
    static unsigned int count = 0;
    int sx, sy;
    particle *p;
    int i;

    if (numMarked == 0)
        return;

    count++;

    for (i = 0; i < numMarked; i++)
    {
        p = marked[i];

        sx = SCREENC(p->pos.d.x);
        sy = SCREENC(p->pos.d.y);

#ifndef USE_OPENGL
        if (((sx + sy + count) & 0xf) > 7)
        {
            SET_PARTICLE_COLOR(p,ARGB(0x00ff00ff));
        }
        else
        {
            SET_PARTICLE_COLOR(p,ARGB(0x0000ffff));
        }
#else
        if (((sx + sy + count) & 0xf) > 7)
        {
            SET_PARTICLE_COLOR_IMM(p, 1.0, 1.0, 0.0, 1.0);
        }
        else
        {
            SET_PARTICLE_COLOR_IMM(p, 1.0, 0.0, 1.0, 1.0);
        }
#endif
    }   

    calloutSched (markShowCallout, MARK_SHOW_INTERVAL, (FUNCPTR)markShow, NULL);
}

void markInit()
{
    int i;

    numMarked = 0;
    marked = malloc (sizeof(particle *) * options->maxParticles);
    if (marked == NULL)
    {
        fprintf (stderr, "Failed to allocate memory for marker array\n");
    }

    for (i = 0; i < 9; i++)
    {
        groupSize[i] = 0;
        groups[i] = malloc (sizeof(particle*) * options->maxParticles);

        if (groups[i] == NULL)
        {
            fprintf (stderr, "Failed to allocate memory for group %d\n", i);
        }
    }

    isMarked = flagFieldAlloc();

    markShowCallout = calloutNew ();
}

int unsetMarked (particle *p)
{
    int i;

    if ((numMarked == 0) || !FLAG_ISSET(isMarked, p))
        return 1;

    for (i = 0; i < numMarked; i++)
    {
        if (p == marked[i])
        {
            marked[i] = marked[numMarked-1];
            numMarked--;

            break;
        }
    }   
    FLAG_CLR(isMarked, p);

    RESTORE_PARTICLE_COLOR (p);

    return 1;
}

int setMarked (particle *p)
{
    if (FLAG_ISSET(isMarked, p))
    {
        return 1;
    }

    marked[numMarked++] = p;

    FLAG_SET(isMarked, p);

    return 1;
}


int sbWalkCb (particle *p, void *arg)
{
    markAction what = (markAction)arg;

    switch (what)
    {
        case mark:
//            if (FLAG_ISSET(isMarked, p))
//                return 0;
            setMarked (p);
            break;

        case unmark:
//            if (!FLAG_ISSET(isMarked, p))
//                return 0;
            unsetMarked (p);
            break;
    }

    return 1;
}


void markParticle (particle *p, markAction what, int withSiblings)
{
    switch (what)
    {
        case mark: 
            setMarked (p);
            break;

        case unmark:
            unsetMarked (p);
            break;
    }

    if (withSiblings && (pdescs[PTYPE(p)]->walkSiblings))
    {
        pdescs[PTYPE(p)]->walkSiblings (p, (FUNCPTR)sbWalkCb, (void*)what);
    }
}

void markRemoveCb (particle *p)
{
    int i;
    int g;

    unsetMarked (p);

    for (g = 0; g < 9; g++)
    {
        for (i = 0; i < groupSize[g] ; i++)
        {
            if (p == groups[g][i])
            {
                groups[g][i] = groups[g][groupSize[g]-1];
                groupSize[g]--;
            }
        }
    }
}

void markClear ()
{
    int i;

    for (i = 0; i < numMarked; i++)
    {
        RESTORE_PARTICLE_COLOR (marked[i]);
    }


    FLAG_CLRALL(isMarked);

#ifdef USE_OPENCL
    clSchedMarkAll(0);
#endif

    numMarked = 0;
}

/* mark all particles on this pixel. Return 1 if there are WIMPs affected,
 * 0 otherwise
 */
int markAllOnPixel (int x, int y, markAction what, int withSiblings)
{
    screeninfo *pSi;
    particle *p;

    if (!inSelection(x,y))
        return 0;

    pSi = &siArray[x + y*wid];

    if (pSi->particleList <= SCR_BRICK)
        return pSi->wimps ? 1 : 0;

    for (p = pSi->particleList; p != NULL; p = p->next)
    {
        markParticle (p, what, withSiblings);
    }

    if (pSi->wimps)
        return 1;

    return 0;
}

void markWimps (particle *p, intptr_t arg)
{
    int t = PTYPE(p);
    int x, y;
    markAction what = (markAction)arg;

    if (!pdescs[t]->isWimp)
        return;

    x = SCREENC(p->pos.d.x);
    y = SCREENC(p->pos.d.y);

    if (siArray[x + y*wid].flags & SI_MARKER)
    {
        if (what == mark)
            setMarked (p);
        else
            unsetMarked (p);
    }
}

int
doDrawMark (int x, int y, int dx, int dy, markAction what, int withSiblings)
{
    int xx, yy;
    int px, py;
    int wimps = 0;
    screeninfo *pSi;

    /* mark all particles under the current stencil.
     * WIMPs are hard to catch, as usual. We mark all pixel where a 
     * WIMP is supposed to be. Later on, all particles are checked
     * whether they are a WIMP on a marked pixel.
     */
    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei))
                continue;

            pSi = &siArray[px + py*wid];

            if (markAllOnPixel (px, py, what, withSiblings))
            {
                wimps = 1;
            }
            siMark (pSi);
        }
    }

    if (!wimps)
        return 1;

    /* mark all WIMPs ..*/
    walkAllParticles ((FUNCPTR)markWimps, (intptr_t)what);

    return 1;
}

void markAll()
{
    SDL_Rect r;
    int x, y;
    screeninfo *pSi;
    particle *p;

    if (!getSelection (&r))
    {
        walkAllParticles ((FUNCPTR)setMarked, 0);
    }
    else
    {
        for (y = r.y; y < r.y + r.h; y++)
        {
            pSi = &siArray[r.x + y*wid];

            for (x = r.x; x < r.x + r.w; x++, pSi++)
            {
                if (pSi->particleList <= SCR_BRICK)
                    continue;

                for (p = pSi->particleList; p != NULL; p = p->next)
                {
                    markParticle (p, mark, 0);
                }
            }
        }
    }

#ifdef USE_OPENCL
    clSchedMarkAll(1);
#endif

    markShow();
}

void drawFctMark (int x, int y, int act, int mode)
{
    static int lastX, lastY;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    markAction what = mark;
    int clearAll = 0;
    int withSiblings = ctrlPressed();

    CHECK_DRAWING

    if (mouseButton == 3)
    {
        what = unmark;

        clearAll = shiftPressed();
    }
    else
    {
        if (mouseButton != 1)
            return;
    }


    switch (event)
    {
        case BUTTON_PRESS:

            if (clearAll)
            {
                markClear();
            }

            doDrawMark (x, y, 0, 0, what, withSiblings);
            markShow ();

#ifdef USE_OPENCL
            clParticleMark ((what == mark), x, y);
#endif

            siMarkClear();
            break;

        case BUTTON_MOVE:
            /* fall through */

        case BUTTON_RELEASE:

            if ((mode == DR_DRAW) ||
                (mode == DR_LINE))
            {
                drawLineF (lastX, lastY, x, y, (FUNCPTR)doDrawMark, (int)what, withSiblings, 3, 4);
            }
            markShow ();

#ifdef USE_OPENCL
            clParticleMark ((what == mark), x, y);
#endif

            siMarkClear();
            break;
    }

    lastX = x;
    lastY = y;
}

/* returns 1 if marking is active but the particle is not marked
 */
int notInSelectionGroup (particle *p)
{
    if (numMarked == 0)
        return 0;

    if (FLAG_ISSET(isMarked, p))
        return 0;

    return 1;
}

int pIsMarked (particle *p)
{
    return (FLAG_ISSET(isMarked, p) != 0);
}

int particlesMarked()
{
    return (numMarked > 0);
}

void walkMarkedParticles (FUNCPTR cb, void *arg1)
{
    int i;

    for (i = 0; i < numMarked; i++)
    {
        cb (marked[i], arg1);
    }
}

/* mark pixels under current stencil */
void markPixels (int x, int y)
{
    int xx, yy;
    int px, py;
    screeninfo *pSi;

    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= INTERNC(wid)) || (py < 0) || (py >= INTERNC(hei)))
                continue;

            pSi = &siArray[px + py*wid];

            siMark (pSi);
        }
    }
}

