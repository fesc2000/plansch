/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/
#include <emmintrin.h>

extern unsigned int particleColors[256][8];
extern unsigned int baseColors[];

#ifdef USE_OPENGL
#define COLOR_TYPE fcolor_t
#else
#define COLOR_TYPE uint32_t
#endif

extern COLOR_TYPE *tgtCol;
extern COLOR_TYPE *dflCol;

#define CV_RED  0
#define CV_GREEN 1
#define CV_BLUE 2
#define CV_CYAN 3
#define CV_MAGENTA 4
#define CV_YELLOW 5


/* options */
#undef COLOR_DENS
#undef COLOR_WITH_COL
#undef COLOR_SPEED
#undef COLOR_FIX
#define COLOR_MASS

#ifndef USE_OPENGL
#define ARGB(C)        (C)

#define MAKE_ARGB(a,r,g,b)  ((((a)&0xff) << 24) | (((r)&0xff) << 16) | (((g)&0xff) << 8) | ((b)&0xff))

#define CH_SET_R(x,r)        x = (x & 0xff00ffff) | (((r) & 0xff) << 16)
#define CH_SET_G(x,r)        x = (x & 0xffff00ff) | (((r) & 0xff) <<  8)
#define CH_SET_B(x,r)        x = (x & 0xffffff00) | (((r) & 0xff) <<  0)
#define CH_SET_A(x,r)        x = (x & 0x00ffffff) | (((r) & 0xff) << 24)

#define CH_GET_R(x)        (((x) >> 16) & 0xff)
#define CH_GET_G(x)        (((x) >>  8) & 0xff)
#define CH_GET_B(x)        (((x) >>  0) & 0xff)
#define CH_GET_A(x)        (((x) >> 24) & 0xff)
#define CH_GET_RGB(x)        ((x) & 0xffffff)

#define CH_ADD_R(x,r)        ((x & 0xff00ffff) | (((r) & 0xff) << 16))
#define CH_ADD_G(x,r)        ((x & 0xffff00ff) | (((r) & 0xff) <<  8))
#define CH_ADD_B(x,r)        ((x & 0xffffff00) | (((r) & 0xff) <<  0))
#define CH_ADD_A(x,r)        ((x & 0x00ffffff) | (((r) & 0xff) << 24))

#define CH_ADD_RA(x,r,a)        ((x & 0x0000ffff) | (((a) & 0xff) << 24) | (((r) & 0xff) << 16))


#else

/* ARGB -> ABGR */

#define CH_SET_R(x,r)        x = (x & 0xffffff00) | (((r) & 0xff) <<  0)
#define CH_SET_G(x,r)        x = (x & 0xffff00ff) | (((r) & 0xff) <<  8)
#define CH_SET_B(x,r)        x = (x & 0xff00ffff) | (((r) & 0xff) << 16)
#define CH_SET_A(x,r)        x = (x & 0x00ffffff) | (((r) & 0xff) << 24)

#define CH_GET_R(x)        (((x) >>  0) & 0xff)
#define CH_GET_G(x)        (((x) >>  8) & 0xff)
#define CH_GET_B(x)        (((x) >> 16) & 0xff)
#define CH_GET_A(x)        (((x) >> 24) & 0xff)
#define CH_GET_RGB(x)        ((x) & 0xffffff)

#define CH_ADD_R(x,r)        ((x & 0xffffff00) | (((r) & 0xff) <<  0))
#define CH_ADD_G(x,r)        ((x & 0xffff00ff) | (((r) & 0xff) <<  8))
#define CH_ADD_B(x,r)        ((x & 0xff00ffff) | (((r) & 0xff) << 16))
#define CH_ADD_A(x,r)        ((x & 0x00ffffff) | (((r) & 0xff) << 24))

#define CH_ADD_RA(x,r,a) ((x & 0x00ffff00) | (((a) & 0xff) << 24) | (((r) & 0xff) <<  0))

#define ARGB(C)        (((C) & 0xff00ff00) | (((C) & 0xff) << 16) | (((C) & 0x00ff0000) >> 16))

#define MAKE_ARGB(a,r,g,b)  ((((a)&0xff) << 24) | (((b)&0xff) << 16) | (((g)&0xff) << 8) | ((r)&0xff))

#endif

#define SCR_IS_RGB(x,y,v) (CH_GET_RGB(SCR_GET (x,y)) == CH_GET_RGB(v))
#define BUT_IS_RGB(b,x,y,v)        (buttonGetRGB(b,x,y) == CH_GET_RGB(v))

/* color values */


#define COL_EMPTY_V                ARGB(0x00000000)
#define BRICK_COL_COLD_V        ARGB(0x00323232)
#define BRICK_COL_HOT_V                ARGB(0x00ff3232)
#define COL_BRICK_V                BRICK_COL_COLD
#define COL_STAT_V                ARGB(0x00ffffff)
#define COL_GRAY_V                ARGB(0x00323232)
#define COL_RED_V                ARGB(0x00ff0000)
#define COL_ICONDRAW_V                ARGB(0x00009600)
#define COL_BLUE_V                ARGB(0x000000ff)
#define COL_ICONFG_V                ARGB(0x00c8c8c8)
#define COL_ORANGE_V                ARGB(0x00ff8000)

#define COL_SELECT_V                ARGB(0x00ff0000)
#define COL_ACTIVATED_V                ARGB(0x00c05000)
#define COL_LINE_V                ARGB(0x00329b00)

#define COL_WPBORDER_V                ARGB(0x0000ffff)
#define COL_WPBORDERI_V                ARGB(0x00ff0000)

#define COL_ACCEL_V                ARGB(0x00303030)


/* palette indices */
#define COL_MIN_I                -128
#define COL_MAX_I                127


/* colors for gravity: 0..63 */
#define GRAV_COL_MIN_I        COL_MIN_I
#define GRAV_COL_SPREAD        32
#define GRAV_COL_0_I        (GRAV_COL_MIN_I + GRAV_COL_SPREAD)
#define GRAV_COL_MAX_I        (GRAV_COL_MIN_I + 2*GRAV_COL_SPREAD - 1)

/* particle colors: 96 ..  */
#define PART_COL_MIN_I        (COL_MIN_I + 64)
#define PART_COL_MAX_I        (COL_MIN_I + 223)


/* fix colors: 224.. */
#define FIX_COL_MIN_I        (COL_MIN_I + 224)
#define FIX_COL_MAX_I        (COL_MAX_I)

#define BRICK_COL_COLD_I        FIX_COL_MIN_I
#define BRICK_COL_HOT_I        (FIX_COL_MIN_I + 7)
#define COL_STAT_I        (FIX_COL_MIN_I + 8)
#define COL_GRAY_I        BRICK_COL_COLD_I
#define COL_RED_I        (FIX_COL_MIN_I + 9)
#define COL_ICONDRAW_I        (FIX_COL_MIN_I + 10)
#define COL_BLUE_I        (FIX_COL_MIN_I + 11)
#define COL_ICONFG_I        (FIX_COL_MIN_I + 12)
#define COL_ORANGE_I        (FIX_COL_MIN_I + 13)

#define COL_SELECT_I        (FIX_COL_MIN_I + 14)
#define COL_LINE_I        (FIX_COL_MIN_I + 15)

#define COL_EMPTY_I        GRAV_COL_0_I
#define COL_ALTEMPTY_I        (PART_COL_MIN_I + PART_RANGE)
#define COL_BRICK_I        BRICK_COL_COLD_I

#define BRICK_RANGE        (BRICK_COL_HOT_I - BRICK_COL_COLD_I + 2)
#define TEMPR_COL_I(T)        (BRICK_COL_COLD_I + ((T) * BRICK_RANGE) / MAX_TEMP)

#define PART_RANGE        16
#define TRAIL_LEN_MIN        3
#define PART_COL_START_I(p) (PART_COL_MIN_I + PART_RANGE*2*(p))
#define PART_COL_START_ALT_I(p) (PART_COL_MIN_I + PART_RANGE*2*(p) + PART_RANGE)

#define STEN_IS_BRICK(c)        ((signed char)(c) >= FIX_COL_MIN_I)
#define STEN_IS_NOBRICK(c)        ((signed char)(c) < FIX_COL_MIN_I)

#define COL_CONTROL_BG        COL_GRAY


/************************ True color defines *************************/

#define AVG_TO(C,T,D) CH_SET_R(C, (CH_GET_R(C) + CH_GET_R(T))/(D)); \
                      CH_SET_G(C, (CH_GET_G(C) + CH_GET_G(T))/(D)); \
                      CH_SET_B(C, (CH_GET_B(C) + CH_GET_B(T))/(D));

#define AVG_TO2(C,T,TM,D) CH_SET_R(C, (CH_GET_R(C) + (int)CH_GET_R(T)*(TM))/(D)); \
                         CH_SET_G(C, (CH_GET_G(C) + (int)CH_GET_G(T)*(TM))/(D)); \
                         CH_SET_B(C, (CH_GET_B(C) + (int)CH_GET_B(T)*(TM))/(D));



#ifndef USE_OPENGL
#define ALPHA1                0x01
#define ALPHA2                0x02
#define COL_ACCEL_ALPHA        0x30
#else
#define ALPHA1                0xff
#define ALPHA2                0xff
#define COL_ACCEL_ALPHA        0x80
#endif

#define COL_EMPTY        CH_ADD_A(COL_EMPTY_V, 0)
#define BRICK_COL_COLD        CH_ADD_A(BRICK_COL_COLD_V, ALPHA1)
#define BRICK_COL_HOT        CH_ADD_A(BRICK_COL_HOT_V, ALPHA1)
#define COL_BRICK        BRICK_COL_COLD
#define COL_STAT        CH_ADD_A(COL_STAT_V, ALPHA2)
#define COL_GRAY        CH_ADD_A(COL_GRAY_V, ALPHA2)
#define COL_RED                CH_ADD_A(COL_RED_V, ALPHA2)
#define COL_ICONDRAW        CH_ADD_A(COL_ICONDRAW_V, ALPHA2)
#define COL_BLUE        CH_ADD_A(COL_BLUE_V, ALPHA2)
#define COL_ICONFG        CH_ADD_A(COL_ICONFG_V, ALPHA2)
#define COL_ORANGE        CH_ADD_A(COL_ORANGE_V, ALPHA2)
#define COL_SELECT        CH_ADD_A(COL_SELECT_V, ALPHA2)
#define COL_ACTIVATED        CH_ADD_A(COL_ACTIVATED_V, ALPHA2)
#define COL_LINE        CH_ADD_A(COL_LINE_V, ALPHA2)
#define COL_WPBORDER        CH_ADD_A(COL_WPBORDER_V, ALPHA2)
#define COL_WPBORDERI        CH_ADD_A(COL_WPBORDERI_V, ALPHA2)
#define COL_ACCEL        CH_ADD_A(COL_ACCEL_V, COL_ACCEL_ALPHA)

// #define TEMPR_COL(T)        (BRICK_COL_COLD +  ((((T) * (0xff - 0x32)) / MAX_TEMP) << 16))
#define TEMPR_COL(T)        CH_ADD_R(BRICK_COL_COLD, (((T) * (0xff - 0x32)) / MAX_TEMP))
#define WALL_COL(T,D)        CH_ADD_RA(BRICK_COL_COLD, (((T) * (0xff - 0x32)) / MAX_TEMP), D)

#define SCR_IS_EMPTY(x,y)        (SCR_GET(x,y) == 0)

/*XXX TC*/
#define COL_ALTEMPTY        COL_EMPTY

#define SCR_ADRS(X,Y)        (volatile unsigned int*)&screen->pixels[(X) + (Y)*wid]
#define SCR_SET(X, Y, COL)  {((volatile unsigned int*)screen->pixels)[(X) + (Y)*wid] = COL; SCR_CHANGED;}
#define SCR_GET(X, Y)  ((volatile unsigned int*)screen->pixels)[(X) + (Y)*wid]

#define COORD_OK(X,Y)        (((unsigned int)(X) < options->scrWid) && (unsigned int)(Y) < options->scrHei)
#define SCR_SET_SAFE(X,Y,COL)   if (COORD_OK(X,Y)) SCR_SET (X,Y,COL)
#define SCR_GET_SAFE(X,Y,COL)   COORD_OK(X,Y ? SCR_GET (X,Y) : 0

#define SCR_SETC(X, Y, C, COL)  ((volatile unsigned char*)screen->pixels)[(X)*4+(C) + (Y)*wid*4] = COL;
#define SCR_GETC(X, Y, C)  ((volatile unsigned char*)screen->pixels)[(X)*4+(C) + (Y)*wid*4]

#define COLOR_DFL(X,Y,P,SI)        {SCR_SETC(X, Y, CHARGE_CODE(P), MIN(((int)SCR_GETC(X,Y,CHARGE_CODE(P)) + 2*zoom),0xff)); SCR_SETC(X, Y, 2-CHARGE_CODE(P), 0xff);}

#define BITS_PER_PIXEL 32
#define BYTES_PER_PIXEL        4

#define color_add(OFF,COL)                                                                        \
{                                                                                                \
    __m128i *src = (__m128i*)(((uintptr_t)screen->pixels + ((OFF)<<2)));                                \
    _mm_storeu_si128 (src, _mm_adds_epu8 (_mm_loadu_si128 (src), _mm_cvtsi32_si128(COL)));        \
}

#define COLOR(X,Y,P,SI) color_add((X)+(Y)*wid, ICOLOR(P).col32)

#define BOUNDARY_COL(SI)    tcBoundaryColor(SI)

#ifdef USE_OPENGL
extern v4sf *colorMergeSpeed;
extern v4sf *colorRestoreSpeed;
#endif

#ifdef USE_OPENGL

#define RESTORE_PARTICLE_COLOR(p)   FCOLOR(p).cv = tgtCol[PARTICLE_ID(p)].col = dflCol[PARTICLE_ID(p)].col
#define SET_PARTICLE_COLOR(p,a,r,g,b)        {tgtCol[PARTICLE_ID(p)].c[0] = r; tgtCol[PARTICLE_ID(p)].c[1] = g; tgtCol[PARTICLE_ID(p)].c[2] = b; tgtCol[PARTICLE_ID(p)].c[3] = a;}
#define SET_PARTICLE_COLOR_IMM(p,a,r,g,b)        {SET_PARTICLE_COLOR(p,a,r,g,b); FCOLOR(p).cv = tgtCol[PARTICLE_ID(p)].col;}

static inline void mergeParticleColors (particle *p1, particle *p2)
{
    v4sf avg, m1, m2, ms;

    m1 = _mm_set1_ps (p1->mass);
    m2 = _mm_set1_ps (p2->mass);
    ms = _mm_add_ps (m1, m2);

    /* average color based on relative mass */
    avg = _mm_div_ps (_mm_add_ps (_mm_mul_ps (p1->color.cv, m1), _mm_mul_ps (p2->color.cv, m2)), ms);

    /* slowly move particle color to average color */
    p1->color.cv = _mm_add_ps (p1->color.cv, _mm_mul_ps( _mm_sub_ps (avg, p1->color.cv), *colorMergeSpeed));
    p2->color.cv = _mm_add_ps (p2->color.cv, _mm_mul_ps( _mm_sub_ps (avg, p2->color.cv), *colorMergeSpeed));
}

static inline void restoreParticleColor (particle *p, particle2 *p2)
{
    p2->cdiff.cv = _mm_sub_ps (tgtCol[PARTICLE_ID(p)].col, p->color.cv);
    p->color.cv = _mm_add_ps (p->color.cv, _mm_mul_ps( p2->cdiff.cv, *colorRestoreSpeed));
}

static inline void adjustParticleColor (particle *p1, float *col, float mul)
{
    /* slowly move particle color to average color */
    p1->color.cv = _mm_add_ps (p1->color.cv, _mm_mul_ps(_mm_mul_ps( _mm_sub_ps (*(v4sf*)col, p1->color.cv), *colorMergeSpeed), _mm_set1_ps (mul)));
}
#else
#define RESTORE_PARTICLE_COLOR(p)   ICOLOR(p).col32 = dflCol[PARTICLE_ID(p)]
#define SET_PARTICLE_COLOR(p,col)         ICOLOR(p).col32 = col
#define SET_PARTICLE_COLOR_IMM(p,col)         ICOLOR(p).col32 = col

#define mergeParticleColors(...)
#define restoreParticleColor(...)
#define adjustParticleColor(...)
#endif

#define GET_WALL_COLOR(offset)        _mm_add_ps (wallColors[offset], wallTemprF[offset])

#define COL_CODE_NEGCHARGE  0
#define COL_CODE_NOCHARGE   1
#define COL_CODE_POSCHARGE  2
#define COL_CODE_ACTLINK    3
#define COL_CODE_WIMP       4
#define COL_CODE_ENERGY     5
#define COL_CODE_6          6
#define COL_CODE_7          7

void HSL2RGB(float h, float s, float l, float* outR, float* outG, float* outB);


/* x/y color value is encoded as integer within +-XY_INT_MAX (corresponds to -1.0f/1.0f)
 */
#define XY_INT_MAX   2000
#define XY_INT_MAXF  2000.0f

