/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/* each particle has a pLinks structure associated with it, containing an array of MAX_LINKS
 * pointers to other particles.
 * The pLinks structure also counts the mount of valid links (num) and the maximum number 
 * of links the particle can have (max). max is <= MAX_LINKS, the default is MAX_LINKS.
 * The first <num> elements in the array must point to valid particles.
 * The number of a particle's open links is (max - num). When a open link is made inactive, max is
 * decresed by one. 
 * With each link, a link data element is maintained which can be used by the particle type code.
 */

#include "plansch.h"

pLinks *linkTbl;

/* allocate link array */
void linkInit()
{
    linkTbl = memalign (128, sizeof(pLinks) * options->maxParticles);
    memset ((char*)linkTbl, 0, sizeof(pLinks) * options->maxParticles);
}

/* reset a particle's link structure */
void linkPartReset (particle *p)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

    memset ((char*)pl, 0, sizeof(*pl));

    pl->max = pl->count = pdescs[PTYPE(p)]->numLinks(p);
}

void linkSetCount (particle *p, int count)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

    while (pl->num > count)
    {
        unlinkTwo (p, pl->links[pl->num-1]);
    }
    pl->count = pl->max = count;
}

int linkGetCount (particle *p)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

    return pl->count;
}

/* return number of open links (optionally including the passive ones) */
int openLinks (particle *p, int passive)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

#ifndef CONNECT_PASSVE_LINKS
    return pl->max - pl->num;
#else
    if (!passive)
        return pl->max - pl->num;

    return pl->count - pl->num;
#endif
}

int openLinksActive (particle *p)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

    return pl->max - pl->num;
}

int availLinks (particle *p)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

    return pl->count - pl->num;
}

/* 0 if the particle has no closed link, >0 otherwise */
int hasLink (particle *p)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p)];

    return pl->num;
}

/* link two particles. p1 must have an open link, p2 an open or optionally
 * a passive link. Data is the link data element associated to both particle's
 * link structure
 */
void linkParticlesWeak(particle *p1, particle *p2, uint32 data)
{
    pLinks *pl1 = &linkTbl[PARTICLE_ID(p1)];
    int i;

    if (openLinks(p1, 0) == 0)
        return;

    i = pl1->num;
    pl1->links[i] = p2;
    pl1->linkData[i] = data;
    pl1->num++;
    RESTORE_PARTICLE_COLOR (p1);
}

/* link two particles. p1 must have an open link, p2 an open or optionally
 * a passive link. Data is the link data element associated to both particle's
 * link structure
 */
void linkParticles(particle *p1, particle *p2, int passive, uint32 data)
{
    pLinks *pl1 = &linkTbl[PARTICLE_ID(p1)];
    pLinks *pl2 = &linkTbl[PARTICLE_ID(p2)];
    int srcIndex, tgtIndex;;

    if ((openLinks(p1, 0) == 0) || (openLinks(p2, passive) == 0))
    {
        return;
    }

    srcIndex = pl1->num;
    tgtIndex = pl2->num;
    pl1->links[srcIndex] = p2;
    pl1->linkData[srcIndex] = data;
    pl1->num++;
#ifdef LINK_HAVE_OTHER_IDX
    pl1->otherIdx[srcIndex] = tgtIndex;
#endif
    RESTORE_PARTICLE_COLOR (p1);

#ifdef CONNECT_PASSIVE_LINKS
    if (pl2->num < pl2->max)
#endif
    {
        pl2->links[tgtIndex] = p1;
        pl2->linkData[tgtIndex] = data;
#ifdef LINK_HAVE_OTHER_IDX
        pl2->otherIdx[tgtIndex] = srcIndex;
#endif
        pl2->num++;
        RESTORE_PARTICLE_COLOR (p2);
        return;
    }

#ifdef CONNECT_PASSIVE_LINKS
    if (!passive || pl2->max >= pl2->count)
        return;

    tgtIndex = pl2->max;

    pl2->links[tgtIndex] = p1;
    pl2->linkData[tgtIndex] = data;
    pl2->max++;
    pl2->num++;
    RESTORE_PARTICLE_COLOR (p2);
#endif
}

/* provide a particle's link structure */
pLinks *linkPartners (particle *p)
{
    return &linkTbl[PARTICLE_ID(p)];
}

void setLinkPartners (particle *p, pLinks *pl)
{
        linkTbl[PARTICLE_ID(p)] = *pl;
}

/* undo all particle's links */
void unlinkAll (particle *p)
{
    pLinks *pl = linkPartners (p);
    pLinks *plp;
    particle *pp;
    int i, j;

    for (i = pl->num; i > 0;)
    {
        i--;
        pp = pl->links[i];
        plp = linkPartners (pp);

        for (j = plp->num; j > 0;)
        {
            j--;
            if (plp->links[j] == p)
            {
                plp->num--;
                plp->links[j] = plp->links[plp->num];
                plp->linkData[j] = plp->linkData[plp->num];
                plp->links[plp->num] = NULL;
            }
        }
        RESTORE_PARTICLE_COLOR (pp);
        pl->links[i] = NULL;
        pl->linkData[i] = 0;
    }
    pl->num = 0;
    RESTORE_PARTICLE_COLOR (p);
}

/* unlink two particles */
void unlinkTwo (particle *p1, particle *p2)
{
    pLinks *pl1 = linkPartners (p1);
    pLinks *pl2 = linkPartners (p2);
    int i;

    for (i = MAX(pl1->num, pl2->num); i > 0;)
    {
        i--;
        if (pl1->links[i] == p2)
        {
            pl1->num--;
            pl1->links[i] = pl1->links[pl1->num];
            pl1->linkData[i] = pl1->linkData[pl1->num];
            pl1->links[pl1->num] = NULL;
        }

        if (pl2->links[i] == p1)
        {
            pl2->num--;
            pl2->links[i] = pl2->links[pl2->num];
            pl2->linkData[i] = pl2->linkData[pl2->num];
            pl2->links[pl2->num] = NULL;
        }
    }
    RESTORE_PARTICLE_COLOR (p1);
    RESTORE_PARTICLE_COLOR (p2);
}

/* convert <num> open links to closed links. If <num> is 0
 * all open links are closed
 */
void closeOpenLinks (particle *p, int num)
{
    pLinks *pl = linkPartners (p);

    if (num == 0)
    {
        pl->max = pl->num;
    }
    else
    {
        num = MIN((pl->max - pl->num), num);
        pl->max -= num;
    }

    RESTORE_PARTICLE_COLOR (p);
}

/* convert all inactive links to active links */
void activateClosedLinks (particle *p)
{
    pLinks *pl = linkPartners (p);

    pl->max = pl->count;
    RESTORE_PARTICLE_COLOR (p);
}

void activeNClosedLinks (particle *p, int n)
{
    pLinks *pl = linkPartners (p);

    pl->max = MIN(pl->max + n, pl->count);
    RESTORE_PARTICLE_COLOR (p);
}

#define PPUSH(P)        {                \
            walkStack[walkStackPos] = P;        \
            walkStackPos++;                        \
        }

#define PPOP(P) {                        \
        walkStackPos--;                        \
        P = walkStack[walkStackPos];        \
        }

static particle **walkStack = NULL;
static int walkStackPos;
static FT_TYPE *visited = NULL;        /* flag field */
#define WALK_STACK_SIZE        (options->maxParticles * 2)
        
static void doWalk (particle *p, FUNCPTR cb, void *arg)
{
    int j;
    int rc;
    pLinks *pl;
    particle *pp;
    int startType = PTYPE(p);

    walkStackPos = 0;

    PPUSH (p)

    while ((walkStackPos < WALK_STACK_SIZE) && (walkStackPos > 0))
    {
        PPOP (p)

        if (FLAG_ISSET(visited, p))
        {
            continue;
        }

        if (PTYPE(p) != startType)
        {
            continue;
        }

    
        rc = cb (p, arg);

        if (rc == 0)
            continue;
        else if (rc == -1)
            break;

        FLAG_SET(visited, p);

        pl = linkPartners(p);

        if (!pl)
            continue;
    
        for (j = 0; j < pl->num; j++)
        {
            pp = pl->links[j];

            if (FLAG_ISSET(visited, pp) || (PTYPE(pp) != startType))
                continue;

            PPUSH(pp);
        }
    }
}

void walkLinks (particle *p, FUNCPTR cb, void *arg)
{
    if (walkStack == NULL)
        walkStack = malloc (WALK_STACK_SIZE * sizeof(*walkStack));

    if (walkStack == NULL)
    {
        fprintf (stderr, "walkLinks: failed to malloc walkStack\n");
        return;
    }

    if (visited == NULL)
        visited = flagFieldAlloc();

    FLAG_CLRALL(visited);

    doWalk (p, cb, arg);
}

int dflNumLinks (particle *p)
{
    return 0;
}

int linkedTogether (particle *p1, particle *p2)
{
    pLinks *pl = linkPartners (p1);
    int i;
    
    for (i = 0; i < pl->num; i++)
    {
        if (pl->links[i] == p2)
            return 1;
    }
    
    return 0;
}
