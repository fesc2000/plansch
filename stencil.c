/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include "stencil_list.h"

#include "SDL_rotozoom.h"


static stencil_t dflStencil;

void stencilOrgSize (stencil_t *st, int *w, int *h)
{
    if (st == NULL)
        st = &dflStencil;

    if (!st->orgBitmap)
    {
        *w = *h = 0;
            return;
    }
    *w = st->orgBitmap->w;
    *h = st->orgBitmap->h;

}

void stencilPos (stencil_t *st, int *px, int *py)
{
    if (st == NULL)
        st = &dflStencil;

    *px = st->lastPos.d.x;
    *py = st->lastPos.d.y;
}

void stencilSize (stencil_t *st, int *w, int *h)
{
    if (st == NULL)
        st = &dflStencil;

    *w = st->lastSize.d.x;
    *h = st->lastSize.d.y;
}

void stencilCenter (stencil_t *st, int *x, int *y)
{
    if (st == NULL)
        st = &dflStencil;

    *x = st->lastPos.d.x + st->lastSize.d.x/2;
    *y = st->lastPos.d.y + st->lastSize.d.y/2;
}

void stencilEdge (stencil_t *st, int *ex, int *ey)
{
    double rad;
    double z;

    if (st == NULL)
        st = &dflStencil;

    rad = (((double)st->currentRotation) * 2.0 * M_PI) / 360.0;
    z = ((double)st->currentZoom) / 100.0;

    *ex = (st->lastPos.d.x + (st->lastSize.d.x/2)) + cos(rad) * z * (st->orgBitmap->w/2);
    *ey = (st->lastPos.d.y + (st->lastSize.d.y/2)) - sin(rad) * z * (st->orgBitmap->w/2);
}

int visible = 0;

void lockStencilCenter (stencil_t *st, int lock)
{
    if (st == NULL)
        st = &dflStencil;

    if (st->stencilLocked == lock)
        return;

    st->stencilLocked = lock;

    if (lock)
    {
        st->lockedPos.d.x = st->lastPos.d.x + st->lastSize.d.x/2;
        st->lockedPos.d.y = st->lastPos.d.y + st->lastSize.d.y/2;
    }

}

void drawStencilSprite (stencil_t *st, int mouseX, int mouseY, int clear)
{
    vec_t pos;
    screeninfo *pSi;
    int sx, sy;
    int x,y;
    SDL_Surface *s;
    char type;
    int col;

    if (st == NULL)
        st = &dflStencil;

    s = stencilBitmap(st);

    /* clear old stencil
    */
    if (visible)
    {
        visible = 0;

        for (y = st->lastPos.d.y; y < st->lastPos.d.y + st->lastSize.d.y; y++)
        {
            if ((unsigned int)y > hei)
                continue;;

            pSi = &siArray[st->lastPos.d.x + y * wid];

            for (x = st->lastPos.d.x; x < st->lastPos.d.x + st->lastSize.d.x; x++, pSi++)
            {
                if ((unsigned int)x > wid)
                    continue;

                if ((pSi->flags & SI_STENCIL) == 0)
                    continue;

                pSi->flags &= ~SI_STENCIL;

                col = siColor (pSi);

                if (pSi->flags & SI_IS_PUMP)
                {
                    if (!dirPixel (x, y, XACCEL(pSi), YACCEL(pSi)))
                    {
                        col = COL_ACCEL;
                    }
                    else
                    {
                        col = 0;
                    }
                }
                
//                restoreFromBackStore (x, y);
                setPixel (x, y, col);
            }
        }
    }

    if (clear)
        return;

    if (!s)
        return;

    visible = 1;

    if (!st->stencilLocked)
    {
        pos.d.x = mouseX - s->w/2;
        pos.d.y = mouseY - s->h/2;
    }
    else
    {
        /* the given mouse position defines the rotation and zoom information
         */
        vec_t mouseVec;
        double origLen = st->orgBitmap->w/2;
        double newLen;
        double r;
        int newZoom, newRot;

        mouseVec.d.x = (mouseX - st->lockedPos.d.x);
        mouseVec.d.y = (mouseY - st->lockedPos.d.y);
        newLen = VEC_LEN(mouseVec.d.x, mouseVec.d.y);
        newZoom = (newLen / origLen) * 100.0;
        r = atan2 (-mouseVec.d.y, mouseVec.d.x);
        if (r < 0)
            r += 2 * M_PI;
        
        newRot = (r * 360.0) / (2*M_PI);
        newRot %= 360;

        rotateStencil (st, newRot, newZoom, 0);
        s = stencilBitmap(st);
        pos.d.x = st->lockedPos.d.x - s->w/2;
        pos.d.y = st->lockedPos.d.y - s->h/2;
    }

    /* paint new one */
    for (y = pos.d.y; y < pos.d.y + s->h; y++)
    {
        if ((unsigned int)y >= hei)
            continue;

        pSi = &siArray[pos.d.x + y * wid];

        sy = y - pos.d.y;

        for (x = pos.d.x; x < pos.d.x + s->w; x++, pSi++)
        {
            if (((unsigned int)x >= wid) || !inSelection(x,y))
                continue;

            sx = x - pos.d.x;

            type = SURFACE_GET8(s, sx, sy);

            if (type == 0)
                continue;

            pSi->flags |= SI_STENCIL;

            {
                unsigned int col = SCR_GET(x,y);
                unsigned int dst = 0;

                switch (type)
                {
                    case R_POLYBRICK:
                            dst = 0xff5050;
                        break;
                    case R_BRICK:
                            dst = 0xffffff;
                        break;
                    case R_PART:
                            dst = 0xff;
                        break;
                    case R_ACCEL:
                            dst = 0xff00;
                        break;
                }

                AVG_TO(col, dst, 3);
#ifndef USE_OPENGL
                setPixel(x,y,col | 0x01000000);
#else
                CH_SET_A (col,0x80);
                setPixel(x,y,col);
#endif
            }
        }
    }

    st->lastPos = pos;
    st->lastSize.d.x = s->w;
    st->lastSize.d.y = s->h;
}

void clearStencil (stencil_t *st)
{
    if (st == NULL)
        st = &dflStencil;

    if (st->currentBitmap)
        SDL_FreeSurface (st->currentBitmap);
    
    if (st->currentData)
        SDL_FreeSurface (st->currentData);

    if (st->orgBitmap)
        SDL_FreeSurface (st->orgBitmap);
    
    if (st->orgData)
        SDL_FreeSurface (st->orgData);


    memset ((char*)st, 0, sizeof(*st));

    st->currentZoom = 100;
}

void newStencil (stencil_t *st, SDL_Surface *bitmap, SDL_Surface *data, tape *t)
{
    if (st == NULL)
        st = &dflStencil;

    clearStencil(st);

    st->currentRotation = 0;
    st->currentZoom = 100;

#if 0
    st->orgBitmap = SDL_ConvertSurface (bitmap, screen->format, 0);
    SDL_FreeSurface (bitmap);
#else
    st->orgBitmap = bitmap;
#endif
    st->orgData = data;

    st->Tape = t;
}

int stencilRotation(stencil_t *st )
{
    if (st == NULL)
        st = &dflStencil;

    return st->currentRotation;
}

int stencilZoom(stencil_t *st )
{
    if (st == NULL)
        st = &dflStencil;

    return st->currentZoom;
}

void rotateStencil (stencil_t *st, int deg, int zoom, int rel)
{
    char msg[32];
    if (st == NULL)
        st = &dflStencil;

    if (st->orgBitmap == NULL)
        return;

    if (rel)
    {
        deg = st->currentRotation + deg;
        while (deg < 0)
            deg += 360;

        zoom = st->currentZoom + zoom;
    }

    deg = deg % 360;
    if (zoom <= 0)
        zoom = 1;

    if ((deg >= 0) && (deg <= 180))
        sprintf (msg, "Rotation/zoom: %d~/%d.%d\n", deg, zoom/100, zoom%100);
    else
        sprintf (msg, "Rotation/zoom: %d~/%d.%d\n", deg-360, zoom/100, zoom%100);

    statText (4, msg);


    if ((st->currentBitmap != NULL) && (deg == st->currentRotation) && (zoom == st->currentZoom))
        return;

    st->currentRotation = deg;
    st->currentZoom = zoom;

    if (st->currentBitmap)
    {
        SDL_FreeSurface (st->currentBitmap);
    }

    if (st->currentData) 
    {
        SDL_FreeSurface (st->currentData);
    }
    st->currentBitmap = st->currentData = NULL;

    if ((deg == 0) && (zoom == 100))
        return;

    st->currentBitmap = rotozoomSurface (st->orgBitmap, (double)deg, (double)zoom / 100.0, 0);
    if (st->currentBitmap == NULL)
    {
        printf ("rotozoom %d failed: %s\n", deg, SDL_GetError());
        st->currentBitmap = NULL;
        st->currentRotation = 0;
        return;
    }
}

SDL_Surface *stencilBitmap(stencil_t *st)
{
    if (st == NULL)
        st = &dflStencil;

    if ((st->currentRotation == 0) && (st->currentZoom == 100))
        return st->orgBitmap;

    if (st->currentBitmap == NULL)
    {
        rotateStencil (st, 0, 0, 1);
    }

    return st->currentBitmap;
}

SDL_Surface *stencilData(stencil_t *st)
{
    int x, y;
    double vec_len;
    double vec_rad;
    unsigned int data;
    signed short vx, vy;
    double rot_rad;

    if (st == NULL)
        st = &dflStencil;

    if (st->currentData)
        return st->currentData;

    if ((st->currentRotation == 0) && (st->currentZoom == 100))
        return st->orgData;

    rot_rad = ((double)st->currentRotation * 2 * M_PI) / 360.0;

    if (st->currentData)
        SDL_FreeSurface (st->currentData);

    st->currentData = rotozoomSurface (st->orgData, (double)st->currentRotation, (double)st->currentZoom / 100.0, 0);

    if (st->currentRotation != 0)
    {
        /* rotate accelleration vectors encoded in stencil data
         */
        for (y = 0; y < st->currentData->h; y++)
        {
            for (x = 0; x < st->currentData->w; x++)
            {
                if (SURFACE_GET8(st->currentBitmap, x, y) != R_ACCEL)
                    continue;

                data = SURFACE_GET32(st->currentData, x, y);

                if (data == 0)
                    continue;
                
                vx = data >> 16;
                vy = data & 0xffff;

                vec_len = VEC_LEN(vx, vy);
                vec_rad = atan2(-vy, vx);
                if (vec_rad < 0)
                    vec_rad += 2 * M_PI;

                vec_rad += rot_rad;

                vx = cos(vec_rad) * vec_len;
                vy = -sin(vec_rad) * vec_len;

                data = (vx << 16) | (unsigned short)vy;
                SURFACE_SET32(st->currentData, x, y, data);
            }
        }
    }

    return st->currentData;
}

#define INVERT_X(v)                                \
{                                                \
    signed short vx;                                \
    vx = (v) >> 16;                                \
    vx = -vx;                                        \
    (v) = ((v) & 0x0000ffff) | (vx << 16);        \
}
#define INVERT_Y(v)                                        \
{                                                        \
    signed short vy;                                        \
    vy = (v) & 0xffff;                                        \
    vy = -vy;                                                \
    (v) = (((v) & 0xffff0000) | (unsigned short)vy);        \
}


void stencil_swap (stencil_t *st, int horiz)
{
    int x, y;
    int x2, y2;
    unsigned int tmp, tmp2;

    if (st == NULL)
        st = &dflStencil;

    if (st->currentBitmap)
        SDL_FreeSurface (st->currentBitmap);

    if (st->currentData)
        SDL_FreeSurface (st->currentData);

    st->currentBitmap = st->currentData = NULL;

    if (st->orgBitmap == NULL)
        return;

    if (horiz)
    {
        for (y = 0; y < st->orgBitmap->h; y++)
        {
            for (x = 0; x < st->orgBitmap->w / 2; x++)
            {
                x2 = st->orgBitmap->w - 1 - x;

                tmp = SURFACE_GET8(st->orgBitmap, x, y);
                SURFACE_SET8(st->orgBitmap, x, y, SURFACE_GET8(st->orgBitmap, x2, y));
                SURFACE_SET8(st->orgBitmap, x2, y, tmp);

                tmp = SURFACE_GET32(st->orgData, x, y);
                tmp2 = SURFACE_GET32(st->orgData, x2, y);

                INVERT_X(tmp);
                INVERT_X(tmp2);

                SURFACE_SET32(st->orgData, x2, y, tmp);
                SURFACE_SET32(st->orgData, x, y, tmp2);
            }
        }
    }
    else
    {
        for (x = 0; x < st->orgBitmap->w; x++)
        {
            for (y = 0; y < st->orgBitmap->h / 2; y++)
            {
                y2 = st->orgBitmap->h - 1 - y;

                tmp = SURFACE_GET8(st->orgBitmap, x, y);
                SURFACE_SET8(st->orgBitmap, x, y, SURFACE_GET8(st->orgBitmap, x, y2));
                SURFACE_SET8(st->orgBitmap, x, y2, tmp);

                tmp = SURFACE_GET32(st->orgData, x, y);
                tmp2 = SURFACE_GET32(st->orgData, x, y2);

                INVERT_Y(tmp);
                INVERT_Y(tmp2);

                SURFACE_SET32(st->orgData, x, y2, tmp);
                SURFACE_SET32(st->orgData, x, y, tmp2);
            }
        }
    }

    rotateStencil (st, 0, 0, 1);
}

void makeStencil (stencil_t *st, SDL_Rect *pRect)
{
    if (st == NULL)
        st = &dflStencil;

    recordScreenSection (&stencilTape, pRect);
    tapeToStencil(st, &stencilTape, 0, 0);
}

void stencilWalk (stencil_t *st, FUNCPTR callout, void *arg1, void *arg2)
{
    int x, y;
    int rx, ry;
    double ox, oy;
    double l;
    double vec_rad;
    double rad;
    double z;

    if (st == NULL)
        st = &dflStencil;

    rad = (((double)st->currentRotation) * 2.0 * M_PI) / 360.0;
    z = ((double)st->currentZoom) / 100.0;

    if (st->orgBitmap == NULL)
        return;

    for (x = 0; x < st->orgBitmap->w; x++)
    {
        ox = x - st->orgBitmap->w/2;
        for (y = 0; y < st->orgBitmap->h; y++)
        {
            char c;

            oy = y - st->orgBitmap->h/2;

            c = SURFACE_GET8(st->orgBitmap, x, y);

            if ((c != R_BRICK) && (c != R_POLYBRICK))
                continue;

            l = VEC_LEN(ox, oy);

            vec_rad = atan2(-oy, ox);
            if (vec_rad < 0)
                vec_rad += 2 * M_PI;

            vec_rad += rad;

            rx = cos(vec_rad) * z * l * GRID;
            ry = -sin(vec_rad) * z * l * GRID;

            if (callout (rx, ry, arg1, arg2))
            {
                printf ("aborted\n");
                return;
            }
        }
    }
}

void initStencil(void)
{
    surface_t        *s;

    memset ((char*)&dflStencil, 0, sizeof(dflStencil));

    for (s = pixmap_list; s->pixmap != NULL; s++)
    {
        s->surface = IMG_ReadXPMFromArray (s->pixmap);

        if (!s->surface)
        {
            printf ("ERROR: Failed to create surface from pixmap %s\n", s->name);
            continue;
        }
        s->surface = SDL_ConvertSurface (s->surface, screen->format, 0);

        s->w = s->surface->w;
        s->h = s->surface->h;

    }
}

surface_t * pmToSurface (char **pm)
{
    surface_t        *s;
    for (s = pixmap_list; s->pixmap != NULL; s++)
    {
        if (s->pixmap == pm)
            return s;
    }
    return NULL;
}
