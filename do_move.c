/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NOTWARPED
#define NOTWARPED 1
#endif


/* this is common code used by the particle_XXX.c routines when moving a particle to a
 * new pixel 
 */

    /* check if particle has moved out of bounds */
    if (((unsigned int)newPos.d.x >= maxX) || ((unsigned int)newPos.d.y >= maxY))
    {
        td->drawLinkLine = 1;
        return NULL;
    }


    {
#ifndef NO_DYN_RELINK
        if (likely (oldSi != NULL))
        {
            particle *prev = PREV_GET(p);

//            *oldSi->dens[gravIdx] -= PCHARGE(p)/4;

            /* unlink particle from old pixel */
            if (prev)
            {
                prev->next = p->next;
                if (p->next)
                {
                    PREV_SET(p->next, prev);
                }
            }
            else
            {
                oldSi->particleList = p->next;
                if (p->next)
                {
                    PREV_SET(p->next, NULL);
                }
                else if (oldSi->wimps == 0)
                {
                    /* Old pixel is now empty. Set density to 0 to compensate for
                     * errors that might have happened while adding/subtracting masses for
                     * this pixel (e.g. due to satuaration).
                     * Set the value to this particle's mass. This will be set to 0 later.
                     */
                    SI_CHARGE(oldSi) = (DENSITY_TYPE)PCHARGE(p);

                }
            }

        }
#endif

        /* update particle data */
        p->pos = newPos;

        SET_SCREENINFO (p, newSi);

#ifndef NO_DYN_RELINK
        p->next = newSi->particleList;
        PREV_SET(p, NULL);
        if (p->next)
        {
            PREV_SET(p->next, p);
        }
        newSi->particleList = p;
#endif
    }
    
#if  (DEBUG == 1)
    for (pp = GET_SCREENINFO (p)->particleList; pp != NULL; pp = pp->next)
    {
        if (GET_SCREENINFO (pp) != GET_SCREENINFO (p))
        {
            printf ("bad particle list!\n");
            *(int *) 0 = 0;
        }
    }
#endif
    return newSi;
