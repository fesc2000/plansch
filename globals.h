extern struct cfg_options *options;
extern particle *partArray;
extern particle2 *partArray2;
extern screeninfo *siArray;
extern int hei;
extern int wid;
extern signed char adj2n[];
extern char **stencil;
extern int pumpMode;
extern int pipeMode;

extern int pushAddsImpuls;

extern int linesize;
extern button buttons[];
extern button *buttonGrav;

extern button *buttonPixel;
extern button *buttonBall;
extern button *buttonVerysmallball;
extern button *buttonSmallball;
extern button *buttonPush;
extern button *buttonBowl;
extern button *buttonCup;
extern button *buttonClosedbox;

extern button * weight1Button;
extern button * weight2Button;
extern button * attractButton;

extern button *buttonSelector;
extern button *buttonFlipH;
extern button *buttonFlipV;
extern button *buttonCopy;
extern button *buttonCut;
extern button *buttonSave;

extern svec_t *pumpVecArray;

extern int statAreaX1;
extern int statAreaY1;
extern int statAreaX2;
extern int statAreaY2;

extern int exitButton;

extern vec_t gravVec;
extern SDL_Cursor *cursor;
extern SDL_Cursor *dflCursor;
extern unsigned int numParallel;
extern int numParticles;
extern svec_t *gravUpdateArray;
extern int numGravPoints;
extern int currentWeight;
extern FLOAT wallDamping;
extern int deceleration;


extern int curStatFunc;
extern FUNCPTR statFunctions[];
extern FUNCPTR statFunc;

extern int simpleNormals;
extern uint32 partCount;
extern int dir2SiOff[];

extern threadData *tData[];

extern int temprIdx;

extern int moveDefinesImpuls;
extern int chainParticles;
extern uint64_t lostImpuls;
extern vec_t motionVec;
extern int zoom, minZoom;
extern int zoomx;
extern int zoomy;
extern int izoomx;
extern int izoomy;
extern vec_t mousePos;
extern vec_t iPositionLast;
extern int randomPlacement;
extern particle **newParticleList;
extern int overwrite;
extern int gravIdx;
extern int wallRemoves;
extern FUNCPTR gravSpreadTSS[];
extern particle *freeList;
#ifdef TTF_SUPPORT
extern TTF_Font *sdlFont;
#endif

extern GRAV_TYPE *grvArray[2];
extern DENSITY_TYPE *densArray;
extern FDTYPE *fixDensArray;
extern DENSITY_TYPE *diffArray;

extern int showGravity;
extern pdesc        *pdescs[];
extern unsigned int maxX, maxY;
extern int doProbe;
extern vec_t probePos;
extern int gSgn;
extern int probeButtonVal;
extern vec_t probePosition;
extern vec_t selectVector;
extern vec_t pumpVec;
extern int newParticleListSize;
extern globalData gd;
extern SDL_Color colors[256];
extern int doRecord;

extern button * playButton;
extern button * recordButton;

extern settingId settingsList [];

extern panel *viewPanel;
extern panel *actions;
extern panel *stencils;
extern panel *actions2;
extern panel *resetButtons;
extern panel *globalSettings;
extern panel *particleSettings;
extern panel *statusPanel;
extern panel *fileSelector;
extern panel *cutCopySavePanel;
extern panel *resizePanel;
extern panel *recorder;
extern panel *gravPanel;

extern int settingChanged;
extern int currentStencil;

extern tape stencilTape;
extern void statText (int line, char *msg);
extern void replayProgress();

extern int stencilDrawWall;
extern int stencilDrawParticles;
extern int stencilWallToParticle;
extern int stencilParticleToWall;
extern int gpuWimps;

extern button *statusBoxButton;

extern fileRep stencilRep;
extern fileRep tapeRep;

extern char *xflags;

#ifndef PID_IN_PSTRUCT
extern particle **prevTbl;
#endif
extern int useOpengl;

extern unsigned int numCores;
extern int stopUpdate;

extern tape playbackTape;

extern int planschInitialized;
extern int showParticles;
extern int updateLinkLines;
extern icon_t icons[];
extern int maxrad;
extern pLinks *linkTbl;
extern signed char oscArray[128][256];
extern char *dataDir;
extern int wallArrayIndex;
extern int applyToAll;
extern int have_sse2;
extern int have_ssse3;
extern int have_sse41;
extern int have_avx;
extern int scrChangeFlag;
extern FT_TYPE *alive;

