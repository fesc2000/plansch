/*

Copyright (C) 2008-2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

static int circleScreenCb (int x, int y, uintptr_t arg1, uintptr_t arg2)
{
    scrPoly *po = (scrPoly*)arg1;

    polyAddCoord (po, x, y, arg2);

    return 1;
}

int makeAnglePoly (vec_t *poly, int sx, int sy, int dx, int dy, int angle, int radius)
{
    float a = rads ((float)dx, (float)dy);
    float m = (2*M_PI) / 360.0;
    float ca;
    int i, j;

    angle /= 2;

    j = 0;
    poly[j].d.x = sx; 
    poly[j].d.y = sy; 
    j++;

    for (i = -angle; i < angle; i+=8)
    {
        ca = a + (float)i * m;
        poly[j].d.x = sx + cos (ca) * (float)radius;
        poly[j].d.y = sy - sin (ca) * (float)radius;
        j++;
    }

    ca = a + (float)angle * m;
    poly[j].d.x = sx + cos (ca) * (float)radius;
    poly[j].d.y = sy - sin (ca) * (float)radius;
    j++;

    return j;
}


int makeArrowPoly (vec_t *poly, int sx, int sy, int dx, int dy)
{
    float l = VEC_LEN(dx, dy);
    float nx, ny;
    float lx, ly, rx, ry;

    nx = (float)dx / l;
    ny = (float)dy / l;

    lx = ny;
    ly = -nx;
    rx = -ny;
    ry = nx;

    poly[0].d.x = sx + lx * 5; 
    poly[0].d.y = sy + ly * 5; 
    poly[1].d.x = sx + rx * 5; 
    poly[1].d.y = sy + ry * 5; 
    poly[2].d.x = poly[1].d.x + nx * (l-10);
    poly[2].d.y = poly[1].d.y + ny * (l-10);

    poly[3].d.x = poly[2].d.x + rx * 5;
    poly[3].d.y = poly[2].d.y + ry * 5;

    poly[4].d.x = sx + dx;
    poly[4].d.y = sy + dy;

    poly[5].d.x = poly[3].d.x + lx *20;
    poly[5].d.y = poly[3].d.y + ly *20;

    poly[6].d.x = poly[3].d.x + lx * 15;
    poly[6].d.y = poly[3].d.y + ly * 15;

    return 7;

    poly[7].d.x = poly[0].d.x;
    poly[7].d.y = poly[0].d.y;

    return 8;
}



scrPoly *scrMarker (int x, int y, int rad, uint col)
{
    scrPoly *m = polyNew (1000, 0, col);

    drawCircle (0, 0, rad, rad,
        GRID/2, 1,
        (FUNCPTR)circleScreenCb,
        (uintptr_t)m, (uintptr_t)col, (uintptr_t)0, (uintptr_t)0);

    polyMove (m, x, y);

    polyShow (m);

    return m;
}

scrPoly *scrBox (int x, int y, int len, uint col)
{
    scrPoly *m = polyNew (5, 0, col);

    x -= len/2;
    y -= len/2;

    polyAddCoord (m, x, y, col);
    polyAddCoord (m, x+len, y, col);
    polyAddCoord (m, x+len, y+len, col);
    polyAddCoord (m, x, y+len, col);
    polyAddCoord (m, x, y, col);

    polyShow (m);

    return m;
}

scrPoly *scrRect (int x, int y, int w, int h, uint col)
{
    scrPoly *m = polyNew (5, 0, col);

    polyAddCoord (m, x, y, col);
    polyAddCoord (m, x+w, y, col);
    polyAddCoord (m, x+w, y+h, col);
    polyAddCoord (m, x, y+h, col);
    polyAddCoord (m, x, y, col);

    polyShow (m);

    return m;
}


scrPoly *scrSimpleArrow (int x, int y, int dx, int dy, uint col)
{
    float l = VEC_LEN((float)dx, (float)dy);
    float ndx = (float)dx / l;
    float ndy = (float)dy / l;
    scrPoly *m = polyNew (8, POLY_OPEN, col);
    int xx, yy;

    polyAddCoord (m, x, y, col);
    x += dx;
    y += dy;
    polyAddCoord (m, x, y, col);

    xx = x - ndx * GRID * 5;
    yy = y - ndy * GRID * 5;
    polyAddCoord (m, xx - ndy * GRID * 5, yy - ndx * GRID * 5, col);

    polyAddCoord (m, x, y, col);
    polyAddCoord (m, xx - ndy * GRID * 5, yy + ndx * GRID * 5, col);

    polyShow (m);

    return m;
}

scrPoly *scrLine (int x1, int y1, int x2, int y2, uint col)
{
    scrPoly *m = polyNew (3, POLY_OPEN|POLY_BW1, col);

    polyAddCoord (m, 0, 0, col);
    polyAddCoord (m, x2-x1, y2-y1, col);

    polyMove (m, x1, y1);

    polyShow (m);

    return m;
}

int makeConnector (scrPoly *po, int x1, int y1, int x2, int y2, uint col)
{
    if (!po)
        return 0;

    polyClear (po);

    polyAddCoord (po, x1, y1, col);
    polyAddCoord (po, x2, y2, col);

    return 1;
}
