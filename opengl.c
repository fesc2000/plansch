/*
Copyright (C) 2010 Felix Schmidt.

Based on code from SDL_oglblit:
Fredrik Hultin, noname@ the domain two lines below, 2008

http://nurd.se/~noname/sdl_oglblit


This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#ifdef USE_OPENCL
#include "opencl.h"
#endif

#if !defined(_MINGW)
#include <SDL/SDL_syswm.h>
#include <GL/glx.h>
#endif


int gpuLoops;
extern float particleFadeF;
extern float particleBlurF;

static float currentFade = 0.0;

static uint clearRequest = 2;
static int ping = 0;

static int initDone = 0;
static int oglStopped = 1;

static int pointSize = 1;

static int glCursorEnabled = 0;
static int glCursorHidden = 0;
static void *glCursorObj = NULL;
static vec_t lastGlCursorPos = { .d.x = -1, .d.y = -1 };

int useGlCursor = 0;

GLenum oglFormat;
OGL_Texture *screenTexture;
OGL_Texture *gravTexture;
OGL_Texture *panelTexture;

SDL_Surface *oglScreen;

/* framebuffer objects where particles are rendered to
 */
GLuint particleFBO;
OGL_Texture *fbTexture[2];

OGL_Texture *densTex;
#if 0
OGL_Texture *dxTex;
OGL_Texture *dyTex;
#else
OGL_Texture *diffTex;
#endif
OGL_Texture *fieldTex[2];

GLuint toHostPboIds[2];

#define R_POINTS        1
#define R_CONNECTIONS        2
#define R_GPU                4

#define ZOOM_TIME   500
#define ZSQ (float)(ZOOM_TIME*ZOOM_TIME)

static float zx1, zy1, zx2, zy2;
static float szx1, szy1, szx2, szy2;
static float tzx1, tzy1, tzx2, tzy2;
static float curz, sz, tz;
static int lastZoom, lastZoomx_i, lastZoomy_i;
static int ztime = 0;

#define GL_USE_DISPLAYLIST

#ifdef GL_USE_DISPLAYLIST
GLuint        worldStaticDisplayList = 0;
#endif

int *hlParticleList = NULL;
int numHlParticles = 0;

#if defined(_MINGW)
void *dc;
void *ctx;
#else
SDL_SysWMinfo info;
Window sdl_win;
Display *sdl_display;
GLXContext sdl_gl_context;
#endif


void gpuInit (DENSITY_TYPE **dens, FDTYPE **fixDens, GRAV_TYPE **dx, GRAV_TYPE **dy);

GLenum OGL_GetFormat(SDL_Surface* surface);

void clearGlCursor();
void hideGlGursor (int hide);
void updateGlCursor (int x, int y);
void showGlCursor (int x, int y);
void enableGlCursor (int enable);

typedef struct
{
    char *name;

    const GLcharARB *vs;        /* vertex shader source */
    const GLcharARB *fs;        /* fragment shader source */

    GLhandleARB        program;

    GLhandleARB        vertexShader;
    GLhandleARB        fragmentShader;

    GLint  parameters[10];

} shader;

/********************  Shaders ************************/

const GLcharARB *dflVertexShader =
"#version 120\n"
" void main(void) "
" { "
"  gl_Position     = gl_ModelViewProjectionMatrix * gl_Vertex; "
"  gl_FrontColor   = gl_Color; "
"  gl_TexCoord[0]  = gl_MultiTexCoord0; "
" } ";


/* shader for generating particle trails.
 * oldImage is the texture of the current image, particleData
 * is the texture containing the current particle data.
 */
shader particleTrailShader =
{
    .name = "particleTrailShader",
    .vs = NULL,
    .fs =
"#version 120\n"
" uniform sampler2D Image; "
" uniform float Length; "
" uniform float wd; "
" uniform float wh; "
" uniform float blur; "
" "
" void main(void) "
" { "
"   vec2 coord = vec2(gl_TexCoord[0]);"
"   vec2 scoord;"
"   vec4 oldColor = texture2D(Image, coord); "
"   vec4 tmp; "
"   tmp = texture2D (Image, coord + vec2(wd, 0.0)); "
"   tmp += texture2D (Image, coord - vec2(wd, 0.0)); "
"   tmp += texture2D (Image, coord + vec2(0.0, wh)); "
"   tmp += texture2D (Image, coord - vec2(0.0, wh)); "
"   tmp /= vec4(blur, blur, blur, blur); "
"   tmp = (vec4(oldColor.r * Length, oldColor.g * Length, oldColor.b * Length, oldColor.a * Length) + tmp) * vec4(0.5, 0.5, 0.5, 0.5); "
"   gl_FragColor = tmp; "
" } ",
};

/* shader for smooth cutoff of particle data
 */
shader trailCutoffShader =
{
    .name = "trailCutoffShader",
    .vs = NULL,
    .fs =
"#version 120\n"
" uniform sampler2D Image; "
" "
" void main(void) "
" { "
"   vec2 coord = vec2(gl_TexCoord[0]);"
"   vec4 tmp = texture2D(Image, coord); "
//"   vec4 tmp2 = smoothstep(0.5, 0.6, tmp);"
//"   tmp2.a = tmp.a * 0.5;"
" tmp.a = smoothstep(0.5, 0.6, tmp.a) * 0.5;"
 "   gl_FragColor = tmp; "
" } ",
};

/* shader to reduce alpha channel
 */
shader reduceAlpha =
{
    .name = "reduceAlpha",
    .vs = NULL,
    .fs =
"#version 120\n"
" uniform sampler2D Image; "
" uniform float reduce; "
" "
" void main(void) "
" { "
"   vec2 coord = vec2(gl_TexCoord[0]);"
"   vec4 tmp = texture2D(Image, coord); "
"   tmp.a *= reduce; "
"   gl_FragColor = tmp; "
" } ",
};


shader cr8 =
{
    .name = "frcChangeRate8",
    .vs = NULL,
    .fs =
"  \n"
"#version 120\n"
" uniform sampler2D dens; \n"
" uniform sampler2D field; \n"
" uniform float gStrength; \n"
" uniform float gDamping; \n"
" uniform float gravWaveDamping; \n"
" uniform float wd; \n"
" uniform float wh; \n"
" \n"
" void main(void) \n"
" { \n"
"   vec2 coord = vec2(gl_TexCoord[0]);\n"
"   float tmp = texture2D (dens, coord).r; \n"
"   float tmp2; \n"
"   vec4 oldField = texture2D (field, coord); \n"
"   vec4 newField; \n"

"   tmp += texture2D (field, coord + vec2(wd, 0.0)).r; \n"
"   tmp += texture2D (field, coord - vec2(wd, 0.0)).r; \n"
"   tmp += texture2D (field, coord + vec2(0.0, wh)).r; \n"
"   tmp += texture2D (field, coord - vec2(0.0, wh)).r; \n"

"   tmp2 =  texture2D (field, coord + vec2(wd, wh)).r; \n"
"   tmp2 += texture2D (field, coord + vec2(wd, -wh)).r; \n"
"   tmp2 += texture2D (field, coord + vec2(-wd, wh)).r; \n"
"   tmp2 += texture2D (field, coord + vec2(-wd, -wh)).r; \n"

"   tmp = (tmp + tmp2 * 0.5) / gStrength; \n"

"   tmp = tmp - oldField.r; \n"
"   tmp = oldField.g + tmp / gDamping; \n"
"   newField.r = (oldField.r + tmp) * gravWaveDamping;\n"
"   newField.g = tmp;\n"
"   newField.a = 1.0;\n"
"   newField.b = 0.0;\n"
"   gl_FragColor = newField; \n"
" } \n"
};

shader display =
{
    .name = "gVisualize",
    .vs = NULL,
    .fs =
"#version 120\n"
" uniform sampler2D field; "
" uniform float gm;"
" uniform float cm;"
" "
" void main(void) "
" { "
"   vec4 F = texture2D (field, vec2(gl_TexCoord[0])); "
"   vec4 col; "
"   float r, g, b; "
"   r = clamp(F.r * gm, 0.0, 32000.0) / 32000.0; "
"   b = -clamp(F.r * gm, -32000.0, 0.0) / 32000.0; "
"   g = abs(F.g * cm) / 32000.0; "
"   col = vec4(r, g, b, 0.6); "
"   gl_FragColor = col; "
" } "
};


#if !defined(OPENGL_NOGRAVITY) && !defined(USE_OPENCL)
static shader calcDiff =
{
    .name = "frcDiff",
    .vs = NULL,
    .fs =
"#version 120\n"
" uniform sampler2D field; "
" uniform float wd; "
" uniform float wh; "
" "
" void main(void) "
" { "
" float dx, dy; "
" float ddx, ddy; "
"   dx = (texture2D (field, vec2(gl_TexCoord[0]) + vec2(wd, 0.0)).r - texture2D (field, vec2(gl_TexCoord[0]) - vec2(wd, 0.0)).r);"
"   ddx = (texture2D (field, vec2(gl_TexCoord[0]) + vec2(wd, wh)).r - texture2D (field, vec2(gl_TexCoord[0]) - vec2(wd, -wh)).r);"
"   ddx += (texture2D (field, vec2(gl_TexCoord[0]) + vec2(wd, -wh)).r - texture2D (field, vec2(gl_TexCoord[0]) - vec2(wd, wh)).r);"
"   dy = (texture2D (field, vec2(gl_TexCoord[0]) + vec2(0.0, wh)).r - texture2D (field, vec2(gl_TexCoord[0]) - vec2(0.0, wh)).r);"
"   ddy = (texture2D (field, vec2(gl_TexCoord[0]) + vec2(wd, wh)).r - texture2D (field, vec2(gl_TexCoord[0]) - vec2(-wd, wh)).r);"
"   ddy += (texture2D (field, vec2(gl_TexCoord[0]) + vec2(-wd, wh)).r - texture2D (field, vec2(gl_TexCoord[0]) - vec2(wd, wh)).r);"
"   gl_FragColor.rgb = vec3((dx + ddx / 2.0)/64000.0, (dy + ddy / 2.0)/64000.0, 0); "
//"   gl_FragColor.rgb = vec3(dx/32000.0, dy/32000.0, 0); "
" } "
};
#endif

void oglClearScreen()
{
    clearRequest = 2;
}

void zoomCountdownStart(void)
{
    ztime = planschTime();
}

void zoomCountdownStop(void)
{
    ztime = 0;
}

int zoomCountdownGet (void)
{
    int rc = ZOOM_TIME - (planschTime() - ztime);

    if (rc <= 0)
    {
        ztime = 0;
        rc = 0;
    }

    return rc;
}

int zoomCountdownZero (void)
{
    if (ztime == 0)
        return 1;

    return 0;

}


static int glCheckError(shader *s, GLhandleARB glObject, char *what)
{
    GLuint error = glGetError();
    int len;

    if (error != GL_NO_ERROR)
    {
        printf ("%s : %s : GL Error %d\n", s->name, what, error);
    }

    if (glObject)
    {
        glGetObjectParameterivARB(glObject, GL_OBJECT_INFO_LOG_LENGTH_ARB , &len);

        if (len > 1)
        {
            char *buffer = alloca (len+10);

            glGetInfoLogARB(glObject, len+10, &len, buffer);

            printf ("Shader Compile error (%s %s):\n%s\n", s->name, what, buffer);

            return 1;
        }
    }

    return 0;
}


static void initShader (shader *s)
{
    int len;
    const char *uni, *uniEnd;
    int i;
    char uniName[100];

    glGetError();

    if (s->vs == NULL)
        s->vs = dflVertexShader;

    s->program = glCreateProgramObjectARB();

    s->vertexShader = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
    glCheckError (s, 0, "glCreateShaderObjectARB");

    s->fragmentShader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
    glCheckError (s, 0, "glCreateShaderObjectARB");

    len = strlen(s->vs);
    glShaderSourceARB(s->vertexShader, 1, &s->vs, &len);
    glCheckError (s, s->vertexShader, "glShaderSourceARB");

    len = strlen(s->fs);
    glShaderSourceARB(s->fragmentShader, 1, &s->fs, &len);
    glCheckError (s, s->fragmentShader, "glShaderSourceARB");

    glCompileShaderARB (s->vertexShader);
    glCheckError (s, s->vertexShader, "glCompileShaderARB vs");
    glCompileShaderARB (s->fragmentShader);
    glCheckError (s, s->fragmentShader, "glCompileShaderARB fs");

    glAttachObjectARB(s->program, s->vertexShader);
    glAttachObjectARB(s->program, s->fragmentShader);
    glCheckError (s, 0, "glAttachObjectARB");

    glDeleteObjectARB(s->vertexShader);
    glDeleteObjectARB(s->fragmentShader);
    glCheckError (s, 0, "glDeleteObjectARB");

    glLinkProgramARB(s->program);
    glCheckError (s, s->program, "glLinkProgramARB");

    uni = s->fs;
    for (i = 0; i < 10; i++)
    {
        uni = strstr (uni, "uniform");
        
        if (uni == NULL)
            break;

        uni += strlen("uniform");

        /* skip spaces */
        for (;*uni == ' '; uni++)
            ;
        if ((*uni == ';') || (*uni == ';') || (*uni == 0))
            break;
        
        /* skip type */
        for (;*uni != ' '; uni++)
            ;
        if ((*uni == ';') || (*uni == ';') || (*uni == 0))
            break;
        
        /* skip spaces */
        for (;*uni == ' '; uni++)
            ;
        if ((*uni == ';') || (*uni == ';') || (*uni == 0))
            break;

        for (uniEnd = uni; (*uniEnd != ' ') && (*uniEnd != ';') && (*uniEnd != 0); uniEnd++)
            ;

        if (uni == uniEnd)
            break;
        
        strncpy (uniName, uni, (uniEnd-uni));
        uniName[uniEnd-uni] = 0;

        s->parameters[i] = glGetUniformLocationARB (s->program, uniName);

        uni = uniEnd;
    }
}

void oglDeleteTexture (OGL_Texture *t)
{
    if (!t)
        return;

    glDeleteTextures (1, &t->name);

    free (t);
}

/* (internal) Converts an SDL_Surface with a compatible color format to an OGL_Texture*/
OGL_Texture *oglMakeTexture (SDL_Surface* surface, GLenum format, int w, int h, int yOffset)
{
    OGL_Texture* texture = NULL;
        
    texture = (OGL_Texture*)malloc(sizeof(OGL_Texture));
        
    if (texture == NULL)
    {
        return NULL;
    }

    texture->w = w;
    texture->h = h;
    texture->yOffset = yOffset;

    texture->format = format;
    texture->data = surface->pixels;
    texture->BytesPerPixel = surface->format->BytesPerPixel;
    texture->dataType = GL_UNSIGNED_BYTE; 

    glGenTextures(1, &texture->name);

    return texture;
}

void oglDeleteSurface (surface_t *s)
{
    if (!s || !s->texture)
        return;

    oglDeleteTexture (s->texture);

    s->texture = NULL;
}


int oglInitSurface (surface_t *s)
{
    if (s->surface == NULL)
        return 0;

    if (OGL_GetFormat(s->surface) == OGL_UNKNOWN)
    {
        printf ("unknown format for %s %d\n", s->name, s->surface->format->BytesPerPixel);
    }

    s->texture = oglMakeTexture (s->surface, OGL_GetFormat(s->surface), s->w, s->h, 0);

    if (s->texture)
        return 1;


    printf ("ERROR: Failed to create OpenGL texture for %s\n", s->name);

    return 0;
}

OGL_Texture *oglMakeTexture2 (void *data, GLenum bytesPerPixel, GLenum internalFormat, GLenum dataType, int w, int h)
{
        OGL_Texture* texture = NULL;
        
        texture = (OGL_Texture*)malloc(sizeof(OGL_Texture));
        
        if (texture == NULL)
        {
            return NULL;
        }

        texture->w = w;
        texture->h = h;
        texture->yOffset = 0;

        texture->format = internalFormat;
        texture->dataType = dataType;
        texture->BytesPerPixel = bytesPerPixel;

        texture->data = data;

        glGenTextures(1, &texture->name);

        return texture;
}

int OGL_CompareFormat(SDL_PixelFormat* a, SDL_PixelFormat* b) 
{ 
        if(a->BytesPerPixel == b->BytesPerPixel){ 
                if(a->BytesPerPixel == 3){ 
                        if( 
                                a->Rmask == b->Rmask && 
                                a->Gmask == b->Gmask && 
                                a->Bmask == b->Bmask  
                        ){ 
                                return 1 ; 
                        } 
                }else{ 
                        if ( 
                                a->Rmask == b->Rmask && 
                                a->Gmask == b->Gmask && 
                                a->Bmask == b->Bmask && 
                                a->Amask == b->Amask  
                        ){ 
//                                printf("Yep."); 
                                return 1; 
                        } 
                } 
        } 
 
        return 0; 
}

/* Returns an SDL_PixelFormat from GL_RGBA or GL_RGBA for the current machine */
SDL_PixelFormat OGL_GetPixelFormat(GLenum glFormat)
{
        SDL_PixelFormat format;

        format.alpha = 0;
        format.colorkey = 0;
        format.BitsPerPixel = 0;
        format.palette = 0;

        if(glFormat == GL_RGBA){
                format.BytesPerPixel = 4;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                format.Rmask = 0xff000000;
                format.Rshift = 24;
                format.Gmask = 0x00ff0000;
                format.Gshift = 16;
                format.Bmask = 0x0000ff00;
                format.Bshift = 8;
                format.Amask = 0x000000ff;
                format.Ashift = 0;
#else
                format.Rmask = 0x000000ff;
                format.Rshift = 0;
                format.Gmask = 0x0000ff00;
                format.Gshift = 8;
                format.Bmask = 0x00ff0000;
                format.Bshift = 16;
                format.Amask = 0xff000000;
                format.Ashift = 24;
#endif
        }else{
                format.BytesPerPixel = 3;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                format.Rmask = 0xff0000;
                format.Rshift = 16;
                format.Gmask = 0x00ff00;
                format.Gshift = 8;
                format.Bmask = 0x0000ff;
                format.Bshift = 0;
#else
                format.Rmask = 0x0000ff;
                format.Rshift = 0;
                format.Gmask = 0x00ff00;
                format.Gshift = 8;
                format.Bmask = 0xff0000;
                format.Bshift = 16;
#endif
                format.Amask = 0;
                format.Ashift = 0;
        }

        format.Aloss = format.Bloss = format.Gloss = format.Rloss = 0;

        return format;
}


GLenum OGL_GetFormat(SDL_Surface* surface)
{
        if(surface->format->BytesPerPixel == 4){
                SDL_PixelFormat rgba = OGL_GetPixelFormat(GL_RGBA);
                if(OGL_CompareFormat(&rgba, surface->format)){
                        return GL_RGBA;
                }
        }else
        if(surface->format->BytesPerPixel == 3){
                SDL_PixelFormat rgb = OGL_GetPixelFormat(GL_RGB);
                if(OGL_CompareFormat(&rgb, surface->format)){
                        return GL_RGB;
                }
        }

        return OGL_UNKNOWN;
}


void oglUpdateTexture (OGL_Texture *texture)
{
    if (!texture)
        return;

    glBindTexture ( GL_TEXTURE_2D, texture->name ); 

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glTexImage2D (GL_TEXTURE_2D, 
                  0, 
                  texture->BytesPerPixel, 
                  texture->w, 
                  texture->h, 
                  0, 
                  texture->format, 
                  texture->dataType,
                  (char*)texture->data + texture->yOffset * texture->w * texture->BytesPerPixel
                  );
}
void oglUpdateTexture2 (OGL_Texture *texture)
{
    if (!texture)
        return;

    glBindTexture( GL_TEXTURE_2D, texture->name ); 

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glTexImage2D (GL_TEXTURE_2D, 
                  0, 
                  GL_RGBA, 
                  texture->w, 
                  texture->h, 
                  0, 
                  GL_LUMINANCE,
                  GL_UNSIGNED_BYTE,
                  0
                  );
}

void openglGetContext (void)
{
#if defined(_MINGW)
    dc = wglGetCurrentDC();
    ctx = wglGetCurrentContext();
#else
    SDL_SysWMinfo info;

    SDL_VERSION (&info.version);
    SDL_GetWMInfo (&info);
    sdl_display = info.info.x11.display;
    sdl_win = info.info.x11.window;
    sdl_gl_context = glXGetCurrentContext ();

    if ( !glXQueryExtension( sdl_display, 0, 0 ) )
        printf ( "X Server doesn't support GLX extension" );

#endif
}

void openglReleaseContext (void)
{
#if defined(_MINGW)
    wglMakeCurrent (dc, NULL);
#else
    if (!glXMakeCurrent (sdl_display, None, 0))
        printf ("Failed Release OpenGL context: %p/%p/%p\n", (void*)sdl_display, (void*)sdl_win, (void*)sdl_gl_context);
#endif

}

void openglRestoreContext (void)
{
#if defined(_MINGW)
    wglMakeCurrent (dc, ctx);
#else
    if (!glXMakeCurrent (sdl_display, sdl_win, sdl_gl_context))
        printf ("Failed to Restore OpenGL context: %p/%p/%p\n", (void*)sdl_display, (void*)sdl_win, (void*)sdl_gl_context);
#endif
}

void openglInitScreen (intptr_t fullscreen)
{
    int i;

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    oglScreen = SDL_SetVideoMode(options->scrWid, options->scrHei, BITS_PER_PIXEL,
                        SDL_OPENGL |
                        (fullscreen ? SDL_FULLSCREEN : 0));

    if (oglScreen == NULL)
    {
        printf ("SDL_SetVideoMode (OPENGL) failed\n");
        exit (1);
    }

#ifdef USE_GLEW
    glewInit();
#endif

    openglGetContext();

#ifdef USE_OPENCL
    clInit1 ();
#endif

    oglFormat = OGL_GetFormat(screen);

    if (oglFormat == OGL_UNKNOWN)
    {
        printf ("incompativle OGL format!\n");
        exit (1);
    }

    screenTexture = oglMakeTexture (screen, oglFormat, wid, hei, 0);

    if (screenTexture == NULL)
    {
        printf ("oglMakeTexture failed\n");
        exit (1);
    }

    gravTexture = oglMakeTexture (screen, oglFormat, wid, hei, 0);

    if (gravTexture == NULL)
    {
        printf ("oglMakeTexture failed\n");
        exit (1);
    }

    panelTexture = oglMakeTexture (screen, oglFormat, options->scrWid, options->scrHei - hei, hei);

    if (screenTexture == NULL)
    {
        printf ("oglMakeTexture failed\n");
        exit (1);
    }

    glEnable(GL_TEXTURE_2D);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glViewport(0, 0, options->scrWid, options->scrHei);

    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_ALPHA_TEST);
    glDisable(GL_DEPTH_TEST);


    glGenFramebuffers (1, &particleFBO);

    glLoadIdentity();

    for (i = 0; i < 2; i++)
    {
        fbTexture[i] = oglMakeTexture (screen, OGL_GetFormat(screen), options->scrWid, options->scrHei, 0);
        oglUpdateTexture (fbTexture[i]);
#if 0
        glBindFramebuffer (GL_FRAMEBUFFER, particleFBO);
        glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTexture[i]->name, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        glBindFramebuffer (GL_FRAMEBUFFER, 0);
#endif
    }


    initShader (&particleTrailShader);
    initShader (&trailCutoffShader);
    initShader (&reduceAlpha);
#if !defined(OPENGL_NOGRAVITY) && !defined(USE_OPENCL)
    initShader (&cr8);
    initShader (&display);
    initShader (&calcDiff);
#endif

    zx1 = tzx1 = izoomx;
    zx2 = tzx2 = izoomx + INTERNC(options->scrWid/zoom);
    zy1 = tzy1 = izoomy + INTERNC(options->scrHei/zoom);
    zy2 = tzy2 = izoomy;

    lastZoom = zoom;
    lastZoomx_i = izoomx;
    lastZoomy_i = izoomy;
    sz = zoom;
    zoomCountdownStop();

    if (useGlCursor)
    {
        enableGlCursor (1);
    }

    gpuInit (&densArray, &fixDensArray, &grvArray[0], &grvArray[1]);

    initDone = 1;
}

void highlightParticle (particle *p)
{
    if (hlParticleList == NULL)
    {
        hlParticleList = (int*)malloc(numParticles * sizeof(int));
        numHlParticles = 0;
    }
    if (numHlParticles >= numParticles)
        return;
    hlParticleList[numHlParticles++] = PARTICLE_ID(p);
}

int oglInitDone()
{
    return initDone;
}

void adjustZoom()
{
    int zstep;

    if ((zoom == lastZoom) && (izoomx == lastZoomx_i) && (izoomy == lastZoomy_i))
    {
        if (zoomCountdownZero())
        {
            zx1 = tzx1 = (FLOAT)(izoomx);
            zx2 = tzx2 = (FLOAT)(izoomx + INTERNC(options->scrWid/zoom));
            zy1 = tzy1 = (FLOAT)(izoomy + INTERNC(options->scrHei/zoom));
            zy2 = tzy2 = (FLOAT)(izoomy);
            curz = tz;

            currentFade = (particleFadeF + currentFade) / 2.0;
        }
        else
        {
            zstep = zoomCountdownGet();

            zx1 = szx1 + ((tzx1 - szx1) / ZSQ ) * (float)(ZSQ - zstep * zstep);
            zx2 = szx2 + ((tzx2 - szx2) / ZSQ ) * (float)(ZSQ - zstep * zstep);
            zy1 = szy1 + ((tzy1 - szy1) / ZSQ ) * (float)(ZSQ - zstep * zstep);
            zy2 = szy2 + ((tzy2 - szy2) / ZSQ ) * (float)(ZSQ - zstep * zstep);
            curz = sz + ((tz - sz) / ZSQ ) * (float)(ZSQ - zstep * zstep);
        }
    }
    else if (zoom != lastZoom)
    {
        tzx1 = izoomx;
        tzx2 = izoomx + INTERNC(options->scrWid/zoom);
        tzy1 = izoomy + INTERNC(options->scrHei/zoom);
        tzy2 = izoomy;

        tz = zoom;

        szx1 = zx1;
        szx2 = zx2;
        szy1 = zy1;
        szy2 = zy2;
        sz = curz;

        lastZoom = zoom;
        lastZoomx_i = izoomx;
        lastZoomy_i = izoomy;

        zoomCountdownStart();

        currentFade = 0.2;
    }
    else
    {
        zx1 = tzx1 = izoomx;
        zx2 = tzx2 = izoomx + INTERNC(options->scrWid/zoom);
        zy1 = tzy1 = izoomy + INTERNC(options->scrHei/zoom);
        zy2 = tzy2 = izoomy;

        lastZoomx_i = izoomx;
        lastZoomy_i = izoomy;
        zoomCountdownStop();

        currentFade = 0.2;
    }
    
    pointSize = GETVAL32(particleVisSize);

    if (pointSize > 1)
    {
        float sz;

        if (!GETVAL32(showLinks))
            sz = curz/2+1;
        else
            sz = curz/8+1;

        sz *= getValueNorm (&particleVisSize);

        pointSize = sz;

        if (pointSize == 0)
            pointSize = 1;
    }
}

void showScrQuad (int x1, int y1, int x2, int y2)
{
    GLfloat l, r, t, b; 
    GLuint ch, cw; 
    GLfloat hch, hcw; 

    glPushMatrix(); 

    cw = x2 - x1; 
    ch = y2 - y1;

    hcw = (((GLfloat)INTERNC(cw)) / 2.0f); 
    hch = (((GLfloat)INTERNC(ch)) / 2.0f); 

    l = t = 0; 
    r = b = 1; 

    glTranslatef(hcw, hch, 0); 

    glColor4f (1.0f, 1.0f, 1.0f, 1.0f);
    glBegin( GL_QUADS ); 

    /* Top-left */ 
    glTexCoord2f(l, t); 
    glVertex3f(-hcw, -hch, 0); 

    /* Top-right */ 
    glTexCoord2f(r, t); 
    glVertex3f(hcw, -hch, 0 ); 

    /* Bottom-right */ 
    glTexCoord2f(r, b); 
    glVertex3f(hcw, hch, 0); 

    /* Bottom-left */ 
    glTexCoord2f(l, b); 
    glVertex3f(-hcw, hch, 0); 

    glEnd(); 

    glPopMatrix();   
}

void showTextureAt (OGL_Texture *texture, int x, int y, GLfloat a)
{
    GLfloat l, r, t, b; 
    GLuint ch, cw; 
    GLfloat hch, hcw; 
    GLfloat yoffset = INTERNC(texture->yOffset);

    if (texture->needsUpdate)
    {
        texture->needsUpdate = 0;

        oglUpdateTexture (texture);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture( GL_TEXTURE_2D, texture->name ); 

    glPushMatrix(); 

    cw = texture->w; 
    ch = texture->h;         

    l = t = 0; 
    r = b = 1; 

    hcw = (((GLfloat)INTERNC(cw)) / 2.0f); 
    hch = (((GLfloat)INTERNC(ch)) / 2.0f); 

    glTranslatef(hcw, hch, 0); 
#if 0
    glRotatef(rotation, 0, 0, 1.0f); 
#endif

    glEnable(GL_TEXTURE_2D);
    glColor4f (1.0f, 1.0f, 1.0f, a);
    glBindTexture( GL_TEXTURE_2D, texture->name ); 

    glBegin( GL_QUADS ); 

    /* Top-left */ 
    glTexCoord2f(l, t); 
    glVertex3f(-hcw + (GLfloat)x, -hch + yoffset + (GLfloat)y, 0); 

    /* Top-right */ 
    glTexCoord2f(r, t); 
    glVertex3f(hcw + (GLfloat)x, -hch + yoffset + (GLfloat)y, 0 ); 

    /* Bottom-right */ 
    glTexCoord2f(r, b); 
    glVertex3f(hcw + (GLfloat)x, hch + yoffset + (GLfloat)y, 0); 

    /* Bottom-left */ 
    glTexCoord2f(l, b); 
    glVertex3f(-hcw + (GLfloat)x, hch + yoffset + (GLfloat)y, 0); 

    glEnd(); 

    glDisable(GL_TEXTURE_2D);

    glPopMatrix();   
}

void showTexture (OGL_Texture *texture)
{
    showTextureAt (texture, 0, 0, 1.0f);
}

/* draw polygons without using glVertexPointer (suitable for display lists)
*/
static void drawPolyNa (scrPoly *po, void *arg)
{
    int i;

    if (po->flags & POLY_BW1)
    {
        glLineWidth (4);
    }

    if (po->flags & POLY_OPEN)
    {
        glEnable (GL_LINE_SMOOTH);
        glBegin (GL_LINE_STRIP);
        for (i = 0; i < po->num; i++)
        {
            glColor4ubv ((GLubyte*)&po->colors[i]);
            glVertex2fv ((GLfloat*)&po->abs[i]);
        }
        glEnd();
        glDisable (GL_LINE_SMOOTH);
    }
    else  if ((po->flags & (POLY_CONVEX|POLY_FILLED)) == (POLY_CONVEX|POLY_FILLED))
    {
        glEnable (GL_LINE_SMOOTH);
        glBegin (GL_POLYGON);
        for (i = 0; i < po->num; i++)
        {
            glColor4ubv ((GLubyte*)&po->colors[i]);
            glVertex2fv ((GLfloat*)&po->abs[i]);
        }
        glEnd();
        glDisable (GL_LINE_SMOOTH);
    }
    else
    {
        /* complex polygon, only filled if triangle list is available
         */
        if (po->triangles)
        {
            glColor4ubv ((GLubyte*)&po->fillColor);

            glBegin (GL_TRIANGLES);
            for (i = 0; i < po->numTri; i++)
            {
                glVertex2iv ((GLint*)&po->triangles[i][0]);
                glVertex2iv ((GLint*)&po->triangles[i][1]);
                glVertex2iv ((GLint*)&po->triangles[i][2]);
            }
            glEnd();
        }

        /* actually a LINE_LOOP, but that doesn't seem to always work ...
         * (display list bug?)
         */
        glBegin (GL_LINE_STRIP);
        for (i = 0; i < po->num; i++)
        {
            glColor4ubv ((GLubyte*)&po->colors[i]);
            glVertex2fv ((GLfloat*)&po->abs[i]);
        }
        glColor4ubv ((GLubyte*)&po->colors[0]);
        glVertex2fv ((GLfloat*)&po->abs[0]);
        glEnd();
    }

    glLineWidth (1);
}

static void drawPoly (scrPoly *po, void *arg)
{
    if (po->flags & POLY_BW1)
    {
        glLineWidth (4);
    }

    if (po->flags & POLY_OPEN)
    {
        glEnable (GL_LINE_SMOOTH);
        glVertexPointer (2, GL_FLOAT, sizeof(vec_t), po->abs);
        glColorPointer (4, GL_UNSIGNED_BYTE, sizeof(uint32), po->colors);

        glDrawArrays(GL_LINE_STRIP, 0, po->num);
        glDisable (GL_LINE_SMOOTH);
    }
    else  if ((po->flags & (POLY_CONVEX|POLY_FILLED)) == (POLY_CONVEX|POLY_FILLED))
    {
        glEnable (GL_LINE_SMOOTH);
        glVertexPointer (2, GL_FLOAT, sizeof(vec_t), po->abs);
        glColorPointer (4, GL_UNSIGNED_BYTE, sizeof(uint32), po->colors);

        glDrawArrays(GL_POLYGON, 0, po->num);
        glDisable (GL_LINE_SMOOTH);
    }
    else
    {
        if (po->triangles)
        {
            glDisableClientState(GL_COLOR_ARRAY);
            glColor4ubv ((GLubyte*)&po->fillColor);

            glVertexPointer (2, GL_INT, sizeof(vec_t), po->triangles);
            glDrawArrays(GL_TRIANGLES, 0, po->numTri * 3);

            glEnableClientState(GL_COLOR_ARRAY);
        }

        glVertexPointer (2, GL_FLOAT, sizeof(vec_t), po->abs);
        glColorPointer (4, GL_UNSIGNED_BYTE, sizeof(uint32), po->colors);

        glDrawArrays(GL_LINE_LOOP, 0, po->num);
    }

    glLineWidth (1);
}

static void renderParticles(int what)
{
    int thread;

    /* use the current ping texture as target 
    */
    glBindFramebuffer (GL_FRAMEBUFFER, particleFBO);
    glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTexture[ping]->name, 0);

    glEnable (GL_BLEND);
    
    /* add particles. Adjust projection according to current zoom/pan settings
    */
    if (what & (R_POINTS|R_CONNECTIONS))
    {
        glPushMatrix ();

        /* coordinates are internal fixed-point 0..wid<<16/0..hei<<16
         */
        glOrtho (zx1, zx2, zy2, zy1, -1.0f, 1.0f);

        if (showParticles && (what & R_POINTS) && (pointSize != 0))
        {
            glPointSize (pointSize);
            
            if (GETVAL32(particleSmooth) && (pointSize > 1))
                    glEnable (GL_POINT_SMOOTH);
            
            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_COLOR_ARRAY);

            glVertexPointer( 2, GL_INT, sizeof(particle), &partArray[0].pos);

            glColorPointer(  4, GL_FLOAT, sizeof(particle), &partArray[0].color);

            for (thread = 0; thread < options->numThreads; thread++)
            {
                glDrawElements( GL_POINTS, tData[thread]->numParticles, GL_UNSIGNED_INT, tData[thread]->particleList);
            }
            glDisable (GL_POINT_SMOOTH);

            if (numHlParticles)
            {
                glEnableClientState(GL_VERTEX_ARRAY);
                glDisableClientState(GL_COLOR_ARRAY);
                glPointSize ((pointSize / 2)+1 );
                glVertexPointer( 2, GL_INT, sizeof(particle), &partArray[0].pos);
                glColor4f (1.f, 1.f, 1.f, .8f);
                glDrawElements( GL_POINTS, numHlParticles, GL_UNSIGNED_INT, hlParticleList);
                numHlParticles = 0;
            }
        }

        if ((what & R_CONNECTIONS))
        {
            glLineWidth (1);
            glEnableClientState(GL_COLOR_ARRAY);

            for (thread = 0; thread < options->numThreads; thread++)
            {
                if (tData[thread]->numLinkLines2)
                {
                    glVertexPointer( 2, GL_INT, sizeof(vec_t), tData[thread]->linkLines2);
                    glColorPointer( 3, GL_FLOAT, sizeof(v4sf), tData[thread]->linkColors2);
                    glDrawArrays(GL_LINES, 0, tData[thread]->numLinkLines2);
                }
            }
        }
        glPopMatrix();
    }
    glEnable (GL_BLEND);

    glBindFramebuffer (GL_FRAMEBUFFER, 0);
}

#ifdef USE_OPENCL
void renderClParticles(int set)
{
    int count = clNumParticles(set);

    if (!count)
        return;

    /* use the current ping texture as target 
    */
    glBindFramebuffer (GL_FRAMEBUFFER, particleFBO);
    glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTexture[ping]->name, 0);

    glEnable (GL_BLEND);

    if (GETVAL32(particleSmooth))
        glEnable (GL_POINT_SMOOTH);
    else
        glDisable (GL_POINT_SMOOTH);

    glPushMatrix ();
    {
        /* coordinates are 0..wid/0..hei (float)
         */
        glOrtho (SCREENC_F(zx1), SCREENC_F(zx2), SCREENC_F(zy2), SCREENC_F(zy1), -1.0f, 1.0f);

        if (options->clGlSharing)
        {
            int tl;

            /* draw trails 
             */
            tl = GETVAL32(particleTrails) * GETVAL32(clParticleTrailLen);

            if (tl)
            {
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, clParticleVecVboGet(set));
                glVertexPointer (2, GL_FLOAT, 0, 0);
                glDisableClientState (GL_COLOR_ARRAY);
                glColor4f (0.2f, 0.3f, 0.5f, 0.7f);
                glDrawArrays (GL_LINES, 0, count * tl * 2);
                clParticleVecVboRelease(set);
            }

            /* draw link lines
             */
            GLuint vbo, colorVbo;
            int linkCount;
            if (GETVAL32(showLinks) && 
                ((linkCount = clParticleLinkVboGet (set, &vbo, &colorVbo)) != 0))
            {
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vbo);
                glVertexPointer (2, GL_FLOAT, 0, 0);
                
                if (colorVbo)
                {
                    glEnableClientState (GL_COLOR_ARRAY);
                    glBindBufferARB(GL_ARRAY_BUFFER_ARB, colorVbo);
                    glColorPointer (4, GL_UNSIGNED_BYTE, 0, 0);
                }
                else
                {
                    glDisableClientState (GL_COLOR_ARRAY);
                    glColor4f (0.5f, 0.3f, 0.2f, 0.7f);
                }
                glDrawArrays (GL_LINES, 0, linkCount*2);
                clParticleLinkVboRelease(set);
            }

            /* reserve VBO where OpenCl rendered the particle positions to and
             * use it as pointer array
             */
            if (pointSize)
            {
                clParticleVboGet (set, &vbo, &colorVbo);
                glPointSize (pointSize);

                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vbo);
                glVertexPointer (2, GL_FLOAT, 0, 0);
                glEnableClientState(GL_VERTEX_ARRAY);

                glBindBufferARB(GL_ARRAY_BUFFER_ARB, colorVbo);
                glColorPointer (4, GL_FLOAT, 0, 0);
                glEnableClientState(GL_COLOR_ARRAY);

                glDrawArrays(GL_POINTS, 0, count);

                clParticleVboRelease(set);
            }

            glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
        }
        else
        {
            int linkCount;
            void *linkLines, *linkColors;

            if (GETVAL32(showLinks) && 
                ((linkCount = clParticleLinkLinesGet (set, &linkLines, &linkColors)) != 0))
            {
                glEnableClientState(GL_VERTEX_ARRAY);
                glVertexPointer (2, GL_FLOAT, 0, linkLines);
                glEnableClientState(GL_COLOR_ARRAY);
                glColorPointer (4, GL_UNSIGNED_BYTE, 0, linkColors);
                glDrawArrays (GL_LINES, 0, linkCount*2);
            }

            /* read coordinates into a array 
             */
            void *data = clParticlePosGet(set);
            void *col  = clParticleColGet(set);

            if (data)
            {
                glEnableClientState(GL_VERTEX_ARRAY);
                glVertexPointer( 2, GL_FLOAT, 2*sizeof(float), data);
                glEnableClientState(GL_COLOR_ARRAY);
                glColorPointer(  4, GL_FLOAT, 4*sizeof(float), col);

                glDrawArrays( GL_POINTS, 0, count);
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
            }
        }
    }
    glPopMatrix();

    glBindFramebuffer (GL_FRAMEBUFFER, 0);
}
#endif

static void pingPong()
{
    int pong = 1 - ping;

    /* render to different framebuffer
     */
    glBindFramebuffer (GL_FRAMEBUFFER, particleFBO);

    /* use the current pong texture as target 
    */
    glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTexture[pong]->name, 0);

    glDisable (GL_BLEND);
    glPushMatrix ();
    {
        glOrtho(0, INTERNC(options->scrWid), 0, INTERNC(options->scrHei), -1.0f, 1.0f);

        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, fbTexture[ping]->name);
            glUseProgramObjectARB (particleTrailShader.program);
            glUniform1iARB (particleTrailShader.parameters[0], 0);
            glUniform1fARB (particleTrailShader.parameters[1], currentFade);
            glUniform1fARB (particleTrailShader.parameters[2], 1.0 / (float)wid);
            glUniform1fARB (particleTrailShader.parameters[3], 1.0 / (float)hei);
            glUniform1fARB (particleTrailShader.parameters[4], particleBlurF);

            showScrQuad (0, 0, options->scrWid, options->scrHei);

            glUseProgramObjectARB (0);
        }
    }
    glPopMatrix ();
    glBindFramebuffer (GL_FRAMEBUFFER, 0);

    glEnable (GL_BLEND);

    ping = 1 - ping;
}



static void updScreen() 
{ 
    surface_t *s;

    glEnable(GL_BLEND);

    /* render particles into the particle texture
     */
    if (showParticles)
        renderParticles(R_POINTS|R_CONNECTIONS);

    /* show the walls etc.
    */
    glPushMatrix ();
    {
        glOrtho (zx1, zx2, zy1, zy2, -1.0f, 1.0f);

        if ((getValue (&particleAttraction) || getValue(&wallTempr)) && showGravity)
        {
#ifdef USE_OPENCL
            if (!options->clGlSharing)
            {
                /* need to load data from host buffer first ...
                 */
                gravTexture->needsUpdate = 1;
            }

            showTexture (gravTexture);
            glFlush();
#else
            float gm = getValueNorm (&potDivider);
            float cm = getValueNorm (&rippleDivider);

            gm *= 100.f;
            cm *= 1000.f;

            glUseProgramObjectARB (display.program);

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, fieldTex[0]->name);

            glUniform1iARB (display.parameters[0], 0);
            glUniform1fARB (display.parameters[1], gm);
            glUniform1fARB (display.parameters[2], cm);

            showScrQuad (0, 0, wid, hei);
            glUseProgramObjectARB (0);
#endif
        }

        showTexture (screenTexture);

#ifdef GL_USE_DISPLAYLIST
        if (worldObjectsChanged())
        {
            if (worldStaticDisplayList != 0)
            {
                glDeleteLists(worldStaticDisplayList, 1);
                worldStaticDisplayList = 0;
            }

            worldStaticDisplayList = glGenLists(1);
            glNewList(worldStaticDisplayList, GL_COMPILE);

            walkWorldObjects ((FUNCPTR)drawPolyNa, NULL);

            glEndList();

        }

        if (worldStaticDisplayList)
        {
            glCallList (worldStaticDisplayList);
        }
#else
        walkWorldObjects ((FUNCPTR)drawPoly, NULL);
#endif

        /* show the cursor as texture 
         */
        s = currentCursor();
        if (s && s->texture && (hidePanelSection() || (mousePos.d.y < hei)))
        {
            int cx = SX_TO_IX(mousePos.d.x - (((s->w)/2))*zoom) + INTERNC(CORR_X) + zoomOffX;
            int cy = SY_TO_IY(mousePos.d.y - (((s->h)/2))*zoom) + INTERNC(CORR_Y) + zoomOffY;

            if (!drawOpInternc())
            {
                cx &= ~MAX_SPEED;
                cy &= ~MAX_SPEED;
            }
            else
            {
                cx -= GRID/2;
                cy -= GRID/2;
            }

            showTextureAt (s->texture, cx, cy, 0.2f);
        }
    }
    glPopMatrix();
    

    glPushMatrix ();
    glOrtho(0, INTERNC(options->scrWid), INTERNC(options->scrHei), 0, -1.0f, 1.0f);

    /* show particle data 
     */
    if (showParticles)
    {
        if (GETVAL32(partAlpha) < 100)
        {
            glUseProgramObjectARB (reduceAlpha.program);
            glUniform1fARB (reduceAlpha.parameters[1], partAlpha.current.normFloat);
        }
        
        /* set the blend function so that the color value is taken from
         * particle plane, and the particle plane's alpha defines it's
         * transparency.
         * Background color is preserved
         */
        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, 
                GL_ONE_MINUS_DST_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        showTexture (fbTexture[ping]);

        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, 
                GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glUseProgramObjectARB (0);
    }

    /* show buttons and other stuff which is not zoomable 
     */
    if (!hidePanelSection())
        showTexture (panelTexture);

    walkScreenObjects ((FUNCPTR)drawPoly, NULL);

    glPopMatrix();

    SDL_GL_SwapBuffers();

    glClear(GL_COLOR_BUFFER_BIT);
    pingPong();

}

void glMakeFieldTextures(void)
{
    int i;
    char *empty = malloc(wid*hei*4*4);

    memset (empty, 0, wid*hei*4*4);
    for (i = 0; i < 2; i++)
    {
        glBindFramebuffer (GL_FRAMEBUFFER, particleFBO);
        fieldTex[i] = oglMakeTexture2 (empty, GL_RGBA32F, GL_RGBA, GL_FLOAT, wid, hei);
        oglUpdateTexture (fieldTex[i]);
        glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fieldTex[i]->name, 0);
        glClearColor (0,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    glBindFramebuffer (GL_FRAMEBUFFER, 0);
}

#if !defined( OPENGL_NOGRAVITY) && !defined(USE_OPENCL)
static void gravInit(DENSITY_TYPE *dens, short *dx, short *dy)
{

#define IFORMAT GL_RGB32F

    densTex = oglMakeTexture2 (dens, IFORMAT, GL_RED, GL_FLOAT, wid, hei);
    diffTex = oglMakeTexture2 (dx, IFORMAT, GL_RED, GL_SHORT, wid, hei);

    oglUpdateTexture (diffTex);
    glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, diffTex->name, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    glMakeFieldTextures();

    /* PBO to write back data */
    glGenBuffersARB(2, toHostPboIds);

    glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, toHostPboIds[0]);
    glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, wid*hei*6, 0, GL_STREAM_READ_ARB);

    glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, toHostPboIds[1]);
    glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, wid*hei*6, 0, GL_STREAM_READ_ARB);

    glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);

}

static void doGravity ()
{
    int i;
    float gravStrength = 6.0 + (float)(10000 -  getValue (&gpuGravStrength)) * (0.5f / 10000.0f);
    float gravDamping = (float)(100 -  getValue (&gpuGravDamping)) / 10.f;
    float gravWaveDamping = (float) getValueNorm (&gpuGravWaveDamping);

    glBindFramebuffer (GL_FRAMEBUFFER, particleFBO);

    glPushMatrix();
    {
        glOrtho(0, INTERNC(options->scrWid), 0, INTERNC(options->scrHei), -1.0f, 1.0f);

        oglUpdateTexture (densTex);
        glFlush();
        glDisable(GL_BLEND);

        for (i = 0; i < GETVAL32(gravIterations); i++)
        {
            gpuLoops++;

            {
                /* (dens,ch0,gr0) -> ch1 */
                glUseProgramObjectARB (cr8.program);

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, densTex->name);

                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, fieldTex[0]->name);

                glUniform1iARB (cr8.parameters[0], 0);
                glUniform1iARB (cr8.parameters[1], 1);
                glUniform1fARB (cr8.parameters[2], gravStrength);
                glUniform1fARB (cr8.parameters[3], gravDamping);
                glUniform1fARB (cr8.parameters[4], gravWaveDamping);
                glUniform1fARB (cr8.parameters[5], 1.0 / (float)wid);
                glUniform1fARB (cr8.parameters[6], 1.0 / (float)hei);
                
                glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fieldTex[1]->name, 0);
                showScrQuad (0, 0, wid, hei);

                glFlush();

                /* (dens,ch0,gr0) -> ch1 */
//                glUseProgramObjectARB (cr8.program);

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, densTex->name);

                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, fieldTex[1]->name);

                glUniform1iARB (cr8.parameters[0], 0);
                glUniform1iARB (cr8.parameters[1], 1);
                glUniform1fARB (cr8.parameters[2], gravStrength);
                glUniform1fARB (cr8.parameters[3], gravDamping);
                glUniform1fARB (cr8.parameters[4], gravWaveDamping);
                glUniform1fARB (cr8.parameters[5], 1.0 / (float)wid);
                glUniform1fARB (cr8.parameters[6], 1.0 / (float)hei);
                
                glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fieldTex[0]->name, 0);
                showScrQuad (0, 0, wid, hei);
                glFlush();
            }
        }

        /* this the major bottleneck ... transferring data from the GPU to host memory is awfully slow ...
         */
        if (1)
        {
            glUseProgramObjectARB (calcDiff.program);

            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, fieldTex[0]->name);

            glUniform1iARB (calcDiff.parameters[0], 2);
            glUniform1fARB (calcDiff.parameters[1], 1.0f / (float)wid);
            glUniform1fARB (calcDiff.parameters[2], 1.0f / (float)hei);

            glFramebufferTexture2DEXT (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, diffTex->name, 0);
            showScrQuad (0, 0, wid, hei);

            glBindTexture(GL_TEXTURE_2D, diffTex->name);
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_SHORT, diffTex->data);
            glFlush();

            glUseProgramObjectARB (0);
            glFlush();
        }
#if 0
        {
            static int index = 1;
            int nextIndex;
            GLubyte* src;

            index = 1 - index;
            nextIndex = 1 - index;

            glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, toHostPboIds[index]);
//            glReadPixels(0, 0, wid, hei, GL_RGB, GL_SHORT, 0);

            // map the PBO that contain framebuffer pixels before processing it

            glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, toHostPboIds[nextIndex]);

//            src = (GLubyte*)glMapBufferARB(GL_PIXEL_PACK_BUFFER_ARB, GL_READ_ONLY_ARB);

            if(src)
            {
//                memcpy (diffTex->data, src, wid*hei*6);

                glUnmapBufferARB(GL_PIXEL_PACK_BUFFER_ARB);     // release pointer to the mapped buffer
            }
            glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
        }
#endif
    }
    glBindFramebuffer (GL_FRAMEBUFFER, 0);

    glPopMatrix();

    glUseProgramObjectARB (0);
}
#endif

void gpuInit (DENSITY_TYPE **dens, FDTYPE **fixDens, GRAV_TYPE **dx, GRAV_TYPE **dy)
{
#ifndef OPENGL_NOGRAVITY
    oglStopped = 0;

    if (options->clGlSharing)
        oglUpdateTexture2 (gravTexture);

#ifdef USE_OPENCL
#if defined(_MINGW)
    clInit2 (wid, hei, dens, fixDens, dx, dy, gravTexture->name, ctx);
#else
    clInit2 (wid, hei, dens, fixDens, dx, dy, gravTexture->name, sdl_gl_context);
#endif

    if (!options->clGlSharing)
    {
        void *data = clGravBufferGet();

        /* the data where the gravity texture is placed comes from OpenCl 
         * for the case that data is not directly shared.
         * Otherwise, OpenCL renders directly into our texture.
         */
        if (data)
        {
            gravTexture->data = data;
        }
    }


#else
    gravInit(*dens, *dx, *dy);
#endif
#endif
}

void oglWork (int scrUpdate)
{
#ifdef USE_OPENCL
    clWork (scrUpdate);
    
#else
    if (getValue (&particleAttraction) && !enginesStopped())
    {
        doGravity();
    }
#endif

    if (scrUpdate)
    {
        adjustZoom();

        updScreen();
        gpuSignalScreenUpdateDone();
    }
}

void oglWorkThread (void)
{
    int scrUpdate;
#ifndef OPENGL_NOGRAVITY
    oglStopped = 0;

    while (!planschInitialized)
        sleep(1);

    printf ("Starting OGL work thread\n");

    /* restore OpenGl context to this thread
     */
    openglRestoreContext();

    while (!doExit())
    {
        scrUpdate = gpuWaitForStart();

        oglWork(scrUpdate);

        gpuSignalDone();

    }
    printf ("Stopping OGL work thread\n");

#ifdef USE_OPENCL
    clUnInit();
#endif

    oglStopped = 1;
#endif
}


void openglScreenUpdate()
{
    adjustZoom();
    renderParticles(R_POINTS|R_CONNECTIONS);
    updScreen();
}

int oglStop (void)
{
    int i;

    for (i = 3000; i > 0; i--)
    {
        if (oglStopped)
            return 1;
        
        usleep (100);
    }

    return 0;
}

void hideGlGursor (int hide)
{
    if (!glCursorEnabled)
        return;

    glCursorHidden = hide;

    if (hide)
        clearGlCursor();
    else
        showGlCursor(lastGlCursorPos.d.x, lastGlCursorPos.d.y);
}

void clearGlCursor()
{
    if (!glCursorEnabled)
        return;

    polyHide (glCursorObj);
}

void updateGlCursor (int x, int y)
{
    if (!glCursorEnabled)
        return;

    if (!glCursorHidden)
    {
        polyShow (glCursorObj);
        polyMove (glCursorObj, x, y);
    }
    else
    {
        polyHide (glCursorObj);
    }
}

void enableGlCursor (int enable)
{
    static vec_t a[] =
    {
         { .d.x=0,  .d.y=0 }
        ,{ .d.x=10, .d.y=0 }
        ,{ .d.x=6,  .d.y=4 } 
        ,{ .d.x=14, .d.y=12 }
        ,{ .d.x=12, .d.y=14 }
        ,{ .d.x=4,  .d.y=6 }
        ,{ .d.x=0,  .d.y=10 }
        ,{ .d.x=0,  .d.y=0 }

        ,{ .d.x=0,  .d.y=-1 }
        ,{ .d.x=11, .d.y=-1 } 
        ,{ .d.x=7, .d.y=4 }
        ,{ .d.x=15, .d.y=12 }
        ,{ .d.x=12,  .d.y=15 }
        ,{ .d.x=4,  .d.y=7 }
        ,{ .d.x=-1,  .d.y=11 }
        ,{ .d.x=-1,  .d.y=-1 }
    };
    uint32_t col[] =
    {
         ARGB(0xffffffff)
        ,ARGB(0xffffffff)
        ,ARGB(0xffff0000)
        ,ARGB(0xffffffff)
        ,ARGB(0xffffffff)
        ,ARGB(0xfff00000)
        ,ARGB(0xffffffff)
        ,ARGB(0xffffffff)

        ,ARGB(0x01000000)
        ,ARGB(0x01000000)
        ,ARGB(0xffffffff)
        ,ARGB(0xffffffff)
        ,ARGB(0xffffffff)
        ,ARGB(0xffffffff)
        ,ARGB(0x01000000)
        ,ARGB(0x01000000)
    };

    glCursorEnabled = enable;

    if (glCursorObj == NULL)
    {
        glCursorObj = polyNew (20, POLY_SCREEN|POLY_FILLED, ARGB(0xffe02000));

        if (!glCursorObj)
            return;

        polySetCoords (glCursorObj, 16, a, col);
        polyChangeFillColor (glCursorObj, ARGB(0xf0000000));
    }

    if (!glCursorObj)
    {
        glCursorEnabled = 0;
        return;
    }

    glCursorEnabled = enable;
}

void showGlCursor (int x, int y)
{
    if (!glCursorEnabled)
        return;

    if ((lastGlCursorPos.x == x) && (lastGlCursorPos.y == y))
        return;

    lastGlCursorPos.x = x;    
    lastGlCursorPos.y = y;    

    updateGlCursor (x,y);
}

