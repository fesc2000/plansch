/*
Copyright (C) 2016 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/* Experimental code to group a set of particles into a "blob".
 * A blob defines target positions for each particle relative to the mass center of the blob.
 * While attempting to retain it's target position each particle drags the blob at the
 * respective target position, thereby affecting the blobs rotation and center position.
 */

/* show some debug lines (using link lines button) */
#ifndef BLOB_SHOW_LINKS
#define BLOB_SHOW_LINKS 0
#endif

/*
 * 64 bit fix point is requied for some operations which required atomic add.
 * If the extension is not available, use floating point, which is dead slow in
 * comparision.
 */
#ifndef USE_FLOAT_FOR_ATOMS
#ifndef cl_khr_int64_base_atomics
#define USE_FLOAT_FOR_ATOMS 1
#else
#define USE_FLOAT_FOR_ATOMS 0
#endif
#endif

#if (USE_FLOAT_FOR_ATOMS==0)
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#endif



#include "clprivate.clh"

#ifndef M_PI
#define M_PI        3.141592654f
#endif


/* multiplication factors to convert floating point to fix point
 * This is done to be able to use atom_add, which is not available
 * for FP (or very time consuming).
 * The _ERR macros can be used to average out rounding errors towards
 * negative infinity.
 */
#define VEC_MUL        100000
#define VEC_MULF ((float)VEC_MUL)
#define VEC_MUL_ERR (1.0f / (VEC_MUL * 2.0f))

#define ROT_MUL 10000000
#define ROT_MULF ((float)ROT_MUL)
#define ROT_MUL_ERR (1.0f / (ROT_MUL * 2.0f))

/* Blob update stage 1:
 * - sum up new position of all particles (to get new center)
 * - calculate difference of current to target position
 * - apply effect of drag to angle speed of blob
 * - apply force to particle to regain target position
 *
 * Global ID: Particle index
 */
KERNEL_F2(blobUpdateStage1) (
          global blob_t            *blobs
        , global blobPart_t *blobPart
        , global float2            *pos
        , global float2            *vec
        , global float2 *linkLines          /* Out: Link Lines */
        , global uchar4 *linkLineColors     /* Out: LinkLine colors */
        , global clStatus_t *status          /* Out: Status buffer */
        )
{
    int                        blob;
    float2                fpos;
    float2                bpos;
    float2                cpos;
    float2                tgtDiff, n;
    float                tlen;
    
    blob = blobPart[GID0].blobId;

    if (blob == 0)
    {
        return;
    }

    if (blobs[blob].count == 0)
    {
        blobPart[GID0].blobId = 0;
        return;
    }

    fpos = pos[GID0];
    n = blobPart[GID0].blobPosN;
    bpos = n * blobPart[GID0].vecl;
    cpos = (float2)(blobs[blob].cx, blobs[blob].cy);
    tgtDiff  = fpos - (cpos + bpos);

    if (MAD(tgtDiff.x, tgtDiff.x, tgtDiff.y * tgtDiff.y) > 10000.0f)
    {
        /* too far from target position. Just disregard the drag at the moment
         */
//        atomic_add (&blobs[blob].deleted, 1);
//        blobPart[GID0].blobId = 0;
        vec[GID0] = vec[GID0] - tgtDiff * 0.1f;
    }
    else
    {
        /* right oriented normalized tangent
         */
        n = (float2)(n.y, -n.x);

        /* angular impuls */
        tlen = dot(tgtDiff, n) * blobPart[GID0].imp;

        /* Sum up difference from target positions.
         * To avoid atomic_add_global, use fix-point arithmetic so that we can 
         * use atomic_add on integers.
         */
#if (USE_FLOAT_FOR_ATOMS == 0)
        atom_add (&blobs[blob].aSpeed, (long)(tlen * ROT_MULF));
        atom_add (&blobs[blob].nx, (long)(tgtDiff.x * VEC_MULF));
        atom_add (&blobs[blob].ny, (long)(tgtDiff.y * VEC_MULF));
#else
        atomic_add_global (&blobs[blob].aSpeed, tlen);
        atomic_add_global (&blobs[blob].nx, tgtDiff.x);
        atomic_add_global (&blobs[blob].ny, tgtDiff.y);
#endif

        vec[GID0] = vec[GID0] - tgtDiff * 0.1f;
    }
    

#if (BLOB_SHOW_LINKS == 1)
    {
        int j = atomic_add(&status->linkLineCount, 3) * 2;
        linkLines[j]   = fpos;
        linkLines[j+1] = cpos + bpos;
        linkLineColors[j]   = (uchar4)(0x00, 0xff, 0x00, 0xff);
        linkLineColors[j+1] = (uchar4)(0x00, 0xff, 0x00, 0xff);

        j+=2;
        linkLines[j]   = cpos+bpos;
        linkLines[j+1] = (cpos+bpos) + n * tlen * 100000.0f;
        linkLineColors[j]   = (uchar4)(0xff, 0x00, 0x00, 0xff);
        linkLineColors[j+1] = (uchar4)(0xff, 0x00, 0x00, 0xff);

        j+=2;
        linkLines[j]   = cpos+bpos;
        linkLines[j+1] = cpos;
        linkLineColors[j]   = (uchar4)(0x30, 0x30, 0x30, 0x30);
        linkLineColors[j+1] = (uchar4)(0x80, 0x80, 0x80, 0x50);
    }
#endif
}

/* 2nd stage
 *
 * Global ID: Blob index
 */
KERNEL_I(blobUpdateStage2) (
        global blob_t           *blobs
        , global float2 *linkLines          /* Out: Link Lines */
        , global uchar4 *linkLineColors     /* Out: LinkLine colors */
        , global clStatus_t *status          /* Out: Status buffer */
        )
{
    int count = blobs[GID0].count - blobs[GID0].deleted;
#if (BLOB_SHOW_LINKS == 1)
    float2 pos;
    float2 npos;
    int j;
#endif
    float2 vec;
    float2 acc;
    float a;

    if (count <= 0)
    {
        blobs[GID0].count = blobs[GID0].deleted = 0;
        return;
    }

    /* convert npos back from fix to floating point
     */
#if (USE_FLOAT_FOR_ATOMS == 0)
    acc = (float2)((float)blobs[GID0].nx, (float)blobs[GID0].ny) / VEC_MULF;
#else
    acc = (float2)(blobs[GID0].nx, blobs[GID0].ny);
#endif

#define BLOB_ACC_DIV 100
    acc /= (float)(count * BLOB_ACC_DIV);

#if (BLOB_SHOW_LINKS == 1)
    pos = (float2)(blobs[GID0].cx, blobs[GID0].cy);
    npos = pos + acc * (float)BLOB_ACC_DIV;
#endif

    vec = blobs[GID0].vec + acc;

#if (USE_FLOAT_FOR_ATOMS == 0)
    a = blobs[GID0].angle + ((float)blobs[GID0].aSpeed / ROT_MULF);
#else
    a = blobs[GID0].angle + blobs[GID0].aSpeed;
#endif
    a += (float)(4*M_PI);
    a = fmod (a, (float)(2*M_PI));

    blobs[GID0].cx += vec.x;
    blobs[GID0].cy += vec.y;
    blobs[GID0].nx = 0;
    blobs[GID0].ny = 0;
    blobs[GID0].vec = vec;
    blobs[GID0].angle = a;

#if (BLOB_SHOW_LINKS == 1)
    j = atomic_add(&status->linkLineCount, 2) * 2;
    linkLines[j]   = pos;
    linkLines[j+1] = npos;
    linkLineColors[j]   = (uchar4)(0xff, 0x00, 0x00, 0xff);
    linkLineColors[j+1] = (uchar4)(0xff, 0xff, 0x00, 0xff);

    j += 2;
    linkLines[j]   = pos;
    linkLines[j+1] = pos + acc * (float)(BLOB_ACC_DIV * 10);
    linkLineColors[j]   = (uchar4)(0x00, 0x00, 0xff, 0xff);
    linkLineColors[j+1] = (uchar4)(0x00, 0x00, 0xff, 0xff);
#endif
}


/* Blob update stage 3:
 * - update angle of particles target position based on blob rotation
 * - calculate new target position based on new angle
 *
 * Global ID: Particle index
 */
KERNEL_F2(blobUpdateStage3)(
          global blob_t            *blobs
        , global blobPart_t *blobPart
        )
{
    int blob = blobPart[GID0].blobId;
    float angle;
    float nx, ny;

    if (blob == 0)
        return;

    /* apply blobs angle speed to particle angle 
     */
    angle = blobPart[GID0].angle + blobs[blob].angle;

    /* calculate new normalized target position
     */
    ny = -sincos (angle, &nx);
    blobPart[GID0].blobPosN = (float2)(nx, ny);
}


/**** Blob create / update *******/


/* Stage 1: Search all particles marked with CL_MFLG_BLOB_NEW and add them to empty blobId by
 * - summing up their position in blob.center
 * - counting them
 * - unassigning non-new particles from blobId
 * Create an ID list for particles part of blob
 *
 * Global ID: Particle index
 */
KERNEL_I(makeBlobFromNew) (
         global attr_t           *attr
        ,global float2           *pos
        ,global float2           *vec
        ,global int           *idList
        ,const int            blobId
        ,global blob_t           *blobs
        ,global blobPart_t *blobPart
        )
{
    int                    id;
    float2            fpos;
    attr_t            attributes;
    __local float   cx, cy;

    if (LID0 == 0)
    {
        cx = cy = 0.0f;
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    attributes = attr[GID0];

    if (attributes.x & CL_MFLG_BLOB_NEW)
    {
        id = atomic_add (&blobs[blobId].count, 1);
        idList[id] = GID0;
        fpos = pos[GID0];
    
        blobPart[GID0].blobId = blobId;
    
        atomic_add_local (&cx, fpos.x);
        atomic_add_local (&cy, fpos.y);

        attributes.x |= CL_MFLG_BLOB;
   
        attr[GID0].x = attributes.x & ~CL_MFLG_BLOB_NEW;
    }

    barrier(CLK_LOCAL_MEM_FENCE);
    if (LID0 == 0)
    {
        atomic_add_global (&blobs[blobId].cx, cx);
        atomic_add_global (&blobs[blobId].cy, cy);
    }
}

/* 2nd stage (center calculation) done by CPU */

/* 3rd stage of blob creation:
 * - calculate particles relative position to center
 * - distance from center
 * - angle
 * - sum up MOI for blob
 *
 * Global ID: ID-list index
 */
KERNEL_F2(makeBlobStage3) (
         global blob_t           *blobs
        ,global blobPart_t *blobPart
        ,global attr_t           *attr
        ,global float2           *pos
        ,global int           *idList
        ,const int            blobId
        )
{
    int id;
    float r;
    float2  blobPos;

    if (GID0 >= blobs[blobId].count)
        return;

    id = idList[GID0];
    blobPos = pos[id] - (float2)(blobs[blobId].cx, blobs[blobId].cy);
    r = atan2 (-blobPos.y, blobPos.x);
    if (r < 0.0f)
        r += 2 * M_PI;

    atomic_add_global (&blobs[blobId].moi, LENQ(blobPos));

    blobPart[id].angle = r;
    blobPart[id].vecl = length(blobPos);
    blobPart[id].blobPosN = normalize(blobPos);
}

/* 4th stage of blob creation:
 * - particles center offset / blobMOI
 *
 * Global ID: ID-list index
 */
KERNEL_F2(makeBlobStage4) (
         global blob_t           *blobs
        ,global blobPart_t *blobPart
        ,global attr_t           *attr
        ,global float2           *pos
        ,global int           *idList
        ,const int            blobId
        )
{
    int id;

    if (GID0 >= blobs[blobId].count)
        return;

    id = idList[GID0];

    blobPart[id].imp = (blobPart[id].vecl) / blobs[blobId].moi;
    SET_TYPE(attr[id], CL_TYPE_MOVING);
}

/************** recalculate blobs */

/* Stage 1: Sum up current target positions of remaining particles of modified blobs
 *                Also create an id list of affected particles.
 * Stage 2: Calculate new center and reset blob structures for subsequent steps
 * Stage 3: Calculate new relative positions/angles, sum up blob MOI 
 * Stage 4: Calculate particle impulse constants
 */
KERNEL_I(recalBlobStage1) (
          global blob_t     *blobs
         ,global blobPart_t *blobPart
         ,global uint            *idList
)
{
    int                     blobId = blobPart[GID0].blobId;
    global blob_t   *pBlob;
    int                     i;
    float2             tgtPos;

    if (!blobId)
        return;

    pBlob = &blobs[blobId];

    /* reset blob if it has deleted particles
     */
    if (pBlob->deleted)
    {
        /* first entry in id list is the counter
         */
        i = atomic_add (idList, 1) + 1;
        idList[i] = GID0;

        /* use the current target position as basis
         */
#if (USE_FLOAT_FOR_ATOMS == 0)
        tgtPos = blobPart[GID0].blobPosN * blobPart[GID0].vecl * VEC_MULF;
        atom_add (&pBlob->nx, (long)tgtPos.x);
        atom_add (&pBlob->ny, (long)tgtPos.y);
#else
        tgtPos = blobPart[GID0].blobPosN * blobPart[GID0].vecl;
        atomic_add_global (&pBlob->nx, tgtPos.x);
        atomic_add_global (&pBlob->ny, tgtPos.y);
#endif
        /* temporarily store the current target position in absolute coordinates
         * needed in stage 3
         */
        blobPart[GID0].blobPosN = (float2)(pBlob->cx, pBlob->cy) + blobPart[GID0].blobPosN * blobPart[GID0].vecl;
    }
}

KERNEL_I(recalBlobStage2) (
          global blob_t     *blobs
)
{
    global blob_t *blob = &blobs[GID0];
    int            count;
    float2  newCenter;

    if (!blob->count || !blob->deleted)
        return;

    count = blob->count - blob->deleted;
#if (USE_FLOAT_FOR_ATOMS == 0)
    newCenter = (float2)((float)blob->nx, (float)blob->ny) / ((float)count * VEC_MULF) + (float2)(blob->cx, blob->cy);
#else
    newCenter = (float2)(blob->nx, blob->ny) / (float)count + (float2)(blob->cx, blob->cy);
#endif

    /* reset blob structure, except the speed vectors
     */
    blob->count = count;
    blob->moi = 0.0f;
    blob->cx = newCenter.x;
    blob->cy = newCenter.y;
    blob->nx = blob->ny = 0;
    blob->angle = 0.0f;
    blob->deleted = 0;
}


KERNEL_I(recalBlobStage3) (
          global blob_t     *blobs
         ,global blobPart_t *blobPart
         ,global uint            *idList
)
{
    int                         id;
    float                 r;
    float2                 blobPos;
    global blob_t        *blob;
    global blobPart_t        *part;
    float                 vecl;

    if (GID0 >= *idList)
        return;

    id = idList[GID0+1];

    part = &blobPart[id];
    blob = &blobs[part->blobId];

    /* calculate the relative position from the new center to the current
     * absolute target position
     */
    blobPos = part->blobPosN - (float2)(blob->cx, blob->cy);
    r = atan2 (-blobPos.y, blobPos.x);
    if (r < 0.0f)
        r += 2 * M_PI;

    vecl = LENQ(blobPos);
    atomic_add_global (&blob->moi, vecl);

    vecl = sqrt(vecl);

    /* update position attributes relative to blob center
     */
    blobPart[id].angle = r;                    /* angle (static) */
    blobPart[id].vecl = vecl;                    /* distance from center (static) */
    blobPart[id].blobPosN = blobPos / vecl; /* normalized position (updated with rotation) */
}

KERNEL_F2(recalBlobStage4) (
         global blob_t           *blobs
        ,global blobPart_t *blobPart
        ,global int           *idList
        )
{
    int id;
    int blobId;

    if (GID0 >= *idList)
        return;

    id = idList[GID0+1];

    blobId = blobPart[id].blobId;

    blobPart[id].imp = (blobPart[id].vecl) / blobs[blobId].moi;
}
