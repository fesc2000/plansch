/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/* schedule future work for particles. Each particle can have only one scheduling entry since
 * the callout queue has maxParticles entries.
*/

#include "plansch.h"

#define MAX_CQ_ENTRIES        (options->maxParticles*2)

/* callout queue entry. The queue consists a linked list of "l1" entries, ordered by schedTime.
 * Each l1 entry points to a list of l2 entries, containing the actual work definition to be 
 * done at the time in the l1 entry.
 */
typedef struct sched_cqe
{
    struct sched_cqe *next;
    union
    {
        struct
        {
            uint32 schedTime;
            struct sched_cqe *cqList;
        } l1;

        struct
        {
            particle *p;
            FUNCPTR action;
            intptr_t arg;
        } l2;
    } u;
} schedCqe;


/* an bit array to remember which particle is already scheduled and which not.
 * index by PARTICLE_ID(p)
 */
FT_TYPE *sched;

/* buffer with scheduling entries (maxParticles*2 size) 
 */
schedCqe *cqBuffer;

/* callout queue (NULL if empty) */
schedCqe *cqHead;

/* free list of scheduing entries */
schedCqe *cqFreeList;

void initParticleCq()
{

    if (cqBuffer == NULL)
    {
        cqBuffer = malloc(MAX_CQ_ENTRIES * sizeof(schedCqe));
        sched = flagFieldAlloc();
    }

    if ((cqBuffer == NULL) || (sched == NULL))
    {
        printf ("failed to allocate particle callout queue\n");
        cqBuffer = NULL;
        return;
    }

    unschedAllParticles();
}

/* get callout queue entry from freelist */
static schedCqe *getFreeCqe()
{
    schedCqe *rc;

    if (cqFreeList == NULL)
        return NULL;

    rc = cqFreeList;
    cqFreeList = rc->next;
    rc->next = NULL;

    return rc;
}

static void toFreelist (schedCqe *cqe)
{
    cqe->next = cqFreeList;
    cqFreeList = cqe;
}

/* insert new entry into list, ordered by schedTime. Entries with same time
 * are processed  in LIFO order to speed up insertion.
 */
static void insertCqe (schedCqe *l2Cqe, uint32 time)
{
    schedCqe *l1Cqe;
    schedCqe *cqs, *cqp; 

    /* find an existing entry in the l1 list with the same time), or
     * create a new l1 entry and link it into the l1 list (time sorted).
     */
    if (cqHead == NULL)
    {
        l1Cqe = getFreeCqe();
        l1Cqe->u.l1.cqList = NULL;
        cqHead = l1Cqe;
    }
    else
    {
        cqs = cqHead; 
        cqp = NULL;

        while ((cqs != NULL) && (cqs->u.l1.schedTime < time))
        {
            cqp = cqs;
            cqs = cqs->next;
        }

        if (cqs == NULL)
        {
            /* after end of list */
            l1Cqe = getFreeCqe();
            l1Cqe->u.l1.schedTime = time;
            l1Cqe->u.l1.cqList = NULL;

            cqp->next = l1Cqe;
            l1Cqe->next = NULL;
        }
        else
        {
            if (cqs->u.l1.schedTime == time)
            {
                l1Cqe = cqs;
            }
            else
            {
                /* link before cqs */

                l1Cqe = getFreeCqe();
                l1Cqe->u.l1.schedTime = time;
                l1Cqe->u.l1.cqList = NULL;

                if (cqp == NULL)
                {
                    l1Cqe->next = cqHead;
                    cqHead = l1Cqe;
                }
                else
                {
                    l1Cqe->next = cqp->next;
                    cqp->next = l1Cqe;
                }
            }
        }
    }

    l2Cqe->next = l1Cqe->u.l1.cqList;
    l1Cqe->u.l1.cqList = l2Cqe;
}

/* remove particle from lifetime callout queue */
void unschedParticle (particle *p)
{
    schedCqe *l1Cqe, *l1Prev;
    schedCqe *cqs, *cqp;

    if (!FLAG_ISSET(sched, p))
        return;

    FLAG_CLR(sched,p);

    l1Prev = NULL;
    for (l1Cqe = cqHead; l1Cqe != NULL; l1Cqe = l1Cqe->next)
    {
        cqp = NULL;
        for (cqs = l1Cqe->u.l1.cqList; cqs != NULL; cqs = cqs->next)
        {
            if (cqs->u.l2.p == p)
            {
                goto found;
            }
            cqp = cqs;
        }
        l1Prev = l1Cqe;
    }

    printf ("ERROR: unschedParticle: marked as scheduled but not found!\n");

    return;

found:
    if (cqp)
    {
        cqp->next = cqs->next;
    }
    else
    {
        l1Cqe->u.l1.cqList = cqs->next;
    }
    toFreelist (cqs);

    if (l1Cqe->u.l1.cqList == NULL)
    {
        if (l1Prev)
        {
            l1Prev->next = l1Cqe->next;
        }
        else
        {
            cqHead = l1Cqe->next;
        }
        toFreelist (l1Cqe);
    }
}

#if 0
static schedCqe *particleInCq (particle *p, schedCqe **prev)
{
    schedCqe *cqs, *cqPrev;

    cqPrev = NULL;
    for (cqs = cqHead; cqs != NULL; cqs = cqs->next)
    {
        if (cqs->p == p)
        {
            if (prev)
                *prev = cqPrev;
            return cqs;
        }
        cqPrev = cqs;
    }
    return NULL;
}
void checkCq()
{
    uint32 t = systemTime();
    schedCqe *cqs;
    uint32 pt = t-1;
    int num1, num2;

    num1 = num2 = 0;
    for (cqs = cqHead; cqs != NULL; cqs = cqs->next)
    {
        if (cqs->schedTime < t)
        {
            printf ("entry time < current time!\n");
        }
        if (cqs->schedTime < pt)
        {
            printf ("entry time < prev time!\n");
        }
        pt = cqs->schedTime;
        num1++;
    }
    for (cqs = cqFreeList; cqs != NULL; cqs = cqs->next)
        num2++;

    if (num1 + num2 != maxParticles)
    {
        printf ("bad count: %d+%d\n", num1, num2);
    }

}
#endif

/* insert particle into callout queue */
void schedParticle (particle *p, uint32 delay, FUNCPTR action, intptr_t arg)
{
    schedCqe *cqe;
    static int error = 0;
    uint32 schedTime;

#if 0
    if (particleInCq (p, NULL))
    {
        return;
    }
#else
    if (FLAG_ISSET(sched, p))
        return;
#endif

    if (delay == 0)
        return;

    cqe = getFreeCqe();

    if (cqe == NULL)
    {
        if (!error)
            printf ("Error: lifetime callout queue full!\n");
        
        error = 1;
        return;
    }
    error = 0;

    schedTime = systemTime() + delay;
    cqe->u.l2.p = p;
    cqe->u.l2.action = (FUNCPTR)action;
    cqe->u.l2.arg = arg;

    insertCqe (cqe, schedTime);
    FLAG_SET(sched, p);
}

/* process the callout queue based on the current system time */
void processParticleCq()
{
    schedCqe *cqs1, *cqLast;
    schedCqe *cqs2, *cq2Last = NULL;
    uint32 t = systemTime();

    if ((cqHead == NULL) || (cqHead->u.l1.schedTime > t))
        return;

    for (cqs1 = cqLast = cqHead; (cqs1 != NULL) && (cqs1->u.l1.schedTime <= t); cqs1 = cqs1->next)
    {
        for (cqs2 = cqs1->u.l1.cqList; cqs2 != NULL; cqs2 = cqs2->next)
        {
            FLAG_CLR(sched, cqs2->u.l2.p);
            cqs2->u.l2.action (cqs2->u.l2.p, cqs2->u.l2.arg);
            cq2Last = cqs2;
        }

        cq2Last->next = cqFreeList;
        cqFreeList = cqs1->u.l1.cqList;

        cqLast = cqs1;
    }

    cqLast->next = cqFreeList;
    cqFreeList = cqHead;
    cqHead = cqs1; 

}

void unschedAllParticles()
{
    int i;
 
    memset ((char*)cqBuffer, 0, MAX_CQ_ENTRIES * sizeof(schedCqe));
    FLAG_CLRALL(sched);

    cqHead = NULL;

    for (i = 0; i < MAX_CQ_ENTRIES-1; i++)
    {
        cqBuffer[i].next = &cqBuffer[i+1];
    }
    cqBuffer[i].next = NULL;

    cqFreeList = &cqBuffer[0];
}

