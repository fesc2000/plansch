
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

extern int neighbours[];
extern int neighbours5[];

FLOAT collLoss = 1.0;

screeninfo *
moveFreeze (particle *p, screeninfo *oldSi, threadData *tData, int off )
{
    particle *pp, *tpp;
    int cx, cy;
    int dx, dy;
    uint64_t spq;
    uint64_t minSpq;
    int nx, ny;
    screeninfo *wSi = NULL;

    tData->v.partCount++;

    /* check whether we hit the pixel border */
    nx = (p->pos.d.x & MAX_SPEED) + p->vec.d.x;
    ny = (p->pos.d.y & MAX_SPEED) + p->vec.d.y;

    
    dx = p->vec.d.x;
    dy = p->vec.d.y;
    if (nx > MAX_SPEED)
    {
        dx = -p->vec.d.x;
        if (ny > MAX_SPEED)
        {
            dy = -p->vec.d.y;
            wSi = oldSi + wid + 1;
        }
        else if (ny < 0)
        {
            dy = -p->vec.d.y;
            wSi = oldSi - wid + 1;
        }
        else
        {
            wSi = oldSi + 1;
        }
    }
    else if (nx < 0)
    {
        dx = -p->vec.d.x;
        if (ny > MAX_SPEED)
        {
            dy = -p->vec.d.y;
            wSi = oldSi + wid - 1;
        }
        else if (ny < 0)
        {
            dy = -p->vec.d.y;
            wSi = oldSi - wid - 1;
        }
        else
        {
            wSi = oldSi - 1;
        }
    }
    else
    {
        if (ny > MAX_SPEED)
        {
            dy = -p->vec.d.y;
            wSi = oldSi + wid;
        }
        else if (ny < 0)
        {
            dy = -p->vec.d.y;
            wSi = oldSi - wid;
        }
    }

    if (wSi == NULL)
    {
        V_ADD(p->pos, p->pos, p->vec);
        return oldSi;
    }

    /* hit pixel border, check whether there's a particle on the neighbour pixel. If not, reverse
     * direction and out
     */
    if (wSi->particleList <= SCR_BRICK)
    {
        p->vec.d.x = dx;
        p->vec.d.y = dy;
//        V_ADD(p->pos, p->pos, p->vec);
        return oldSi;
    }

    /* hit pixel border. Interact with neares particle on neighbour pixel 
     */
    pp = NULL;
    minSpq = 0xffffffffffffffffULL;

    for (tpp = wSi->particleList; tpp != NULL; tpp = tpp->next)
    {
        cx = tpp->pos.d.x - p->pos.d.x;
        cy = tpp->pos.d.y - p->pos.d.y;

        spq = (uint64_t)cx * (uint64_t)cx + (uint64_t )cy * (uint64_t)cy;

//        if ((spq < (uint64_t)particleSize * (uint64_t)particleSize) && (spq < minSpq))
        if (spq < minSpq)
        {
            minSpq = spq;
            pp = tpp;
        }
    }

    if (pp == NULL)
    {
        p->vec.d.x = dx;
        p->vec.d.y = dy;

//        V_ADD(p->pos, p->pos, p->vec);
        return oldSi;
    }

    {
        FLOAT dlen, dlen2;
        FLOAT dx, dy;
        FLOAT frc;

#define REL_OLD
#ifdef REL_OLD 
        FLOAT diffy = pp->pos.d.y - p->pos.d.y;
        FLOAT diffx = pp->pos.d.x - p->pos.d.x;
#else
        FLOAT diffy = pp->pos.d.y - p->pos.d.y;
        FLOAT diffx = pp->pos.d.x - p->pos.d.x;
#endif
        FLOAT impx1, impy1, impx2, impy2;

        dlen = sqrt(diffx*diffx + diffy*diffy);

//        if (dlen > 0.0)
        {
//            off = (dlen - particleSize)/4;

            dx = (FLOAT)diffx / dlen;
            dy = (FLOAT)diffy / dlen;

            diffx = (pp->pos.d.x + pp->vec.d.x) - (p->pos.d.x + p->vec.d.x);
            diffy = (pp->pos.d.y + pp->vec.d.y) - (p->pos.d.y + p->vec.d.y);
            dlen2 = sqrt(diffx*diffx + diffy*diffy);

            tData->v.collissions++;

            if (dlen2 < dlen)
            {
                frc = (p->vec.d.x * dx + p->vec.d.y * dy) * ((FLOAT)WEIGHT(pp)/(FLOAT)WEIGHT(p));
                impx1 = frc * dx;
                impy1 = frc * dy;

                dx = -dx;
                dy = -dy;

                frc = (pp->vec.d.x * dx + pp->vec.d.y * dy) * ((FLOAT)WEIGHT(p)/(FLOAT)WEIGHT(pp));
                impx2 = frc * dx;
                impy2 = frc * dy;

                p->vec.d.x = (FLOAT)(p->vec.d.x + impx2 - impx1) * collLoss;
                p->vec.d.y = (FLOAT)(p->vec.d.y + impy2 - impy1) * collLoss;

                pp->vec.d.x = (FLOAT)(pp->vec.d.x + impx1 - impx2) * collLoss;
                pp->vec.d.y = (FLOAT)(pp->vec.d.y + impy1 - impy2) * collLoss;

                return oldSi;
            }
        }
    }

    p->vec.d.x = dx;
    p->vec.d.y = dy;
    return oldSi;
}

pdesc MOD = {
    .moveParticle = moveFreeze,

    .pm = freeze_pm,
    .isWimp = 0,

    .name = "Wall",
    .help = "freeze",
};
