/*
Copyright (C) 2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "clprivate.clh"

float2 collide (float2 relPos,
                float2 relVel,
                float dist,
                float collideDist,
                float lDiff,
                constant clParameters_t *params
                )
{
    float        springFactor;
    float2        norm, tanVel;

    dist = SQRT (dist);
    norm = relPos / dist;

    tanVel = relVel - (norm * dot(relVel, norm));
    springFactor = params->spring * (dist - collideDist);

    return springFactor * norm  * (1.0f + lDiff) + 
           params->damping * relVel + 
           params->shear * tanVel + 
           params->attraction * relPos * (1.0f - lDiff);
}

/*
 * To make makefile.opencl find the right kernel name:

KERNEL_F2(doCollideElasticLink) ()

*/

#define KERNEL_NAME doCollideElasticLink
#define PARTICLE_LINK
#include "collide.cl"
