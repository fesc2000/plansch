
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

SETTING(peFusionRad) =
{ 
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "FusionLimit"
    };

SETTING(peFusionGenEnergy) =
{ 
    .minValue = 0,
    .maxValue = 10,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "FusionToEnergy"
    };

static void gasInit (int ptype);

#ifdef USE_AVX
screeninfo * move_gas_avx(particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );
#endif
#ifdef USE_SSE41
screeninfo * move_gas_sse41(particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );
#endif
#ifdef USE_SSE2
screeninfo * move_gas_sse2 (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );
#endif
screeninfo * move_gas_c (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );

pdesc MOD = {
    .init = (FUNCPTR)gasInit,
    .name = "Gas",
    .descr = "",
    .help = "gas",
    .isWimp = 0,

    .pm = gas_pm,

    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, psize_pm, "Particle Size", 0, &particleSize[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},

            {(FUNCPTR)onOffButtonS, 32, 32, connect_wimp_xpm, "Pair_with_WIMP", (intptr_t)0, &wimpPair[0], B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, fusion_xpm, "Fusion Limit", (intptr_t)0, &peFusionRad, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, fusion_to_e_xpm, "Fusion To Energy", (intptr_t)0, &peFusionGenEnergy, B_NEXTRC },
        }
};

static void gasInit (int ptype)
{
    MOD.moveParticle = move_gas_c;
#ifdef USE_AVX
    if (have_avx)
    {
        printf ("GAS: using AVX\n");
        MOD.moveParticle = move_gas_avx;
    }
    else
#endif
#ifdef USE_SSE41
    if (have_sse41)
    {
        printf ("GAS: using SSE4.1\n");
        MOD.moveParticle = move_gas_sse41;
    }
    else
#endif
#ifdef USE_SSE2
    if (have_sse2)
    {
        printf ("GAS: using SSE2\n");
        MOD.moveParticle = move_gas_sse2;
    }
#endif
}


