/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#undef PROBE_BUTTON


/* this is really a mess ... */

#include "plansch.h"
#include "math.h"
#include "SDL_rotozoom.h"


SETTING(fieldLines) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .name = "Show field lines"
};

SETTING(fieldBorder) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .name = "Field Border",
};


SETTING(potDivider) =
{
    .defaultValue = 9,
    .minValue = 0,
    .maxValue = 10000, /* 100 for gpu */
    .minDiff = 1,
    .name = "PotDivider"
};
SETTING(rippleDivider) =
{
    .defaultValue = 10,
    .minValue = 0,
    .maxValue = 10000, /* 100 for GPU */
    .minDiff = 1,
    .name = "RippleDivider"
};

/* Whether nearby particles attract each other (BIG performance loss!).
 */
void cbPartAttraction (setting *s);
SETTING(particleAttraction) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "ParticleAttraction",
    .cb = (FUNCPTR)cbPartAttraction
};

SETTING (applyPSettingToAll) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Apply Particle settings to all",
    .cb = (FUNCPTR)cbApplyToAll
};

int curDrawMode = 0;

int stencilDrawWall = 1;
int stencilDrawParticles = 1;
int stencilWallToParticle = 0;
int stencilParticleToWall = 0;

int rightSideWid;

int wallArrayIndex;
int mpId = 0;

panel *actions = NULL;
panel *drmodes = NULL;
panel *stencils = NULL;
panel *globalSettings = NULL;
panel *particleSettings = NULL;
panel *wallSettings = NULL;
panel *statusPanel = NULL;
panel *fileSelector = NULL;
panel *cutCopySavePanel = NULL;
panel *recorder = NULL;
panel *gravPanel = NULL;
panel *viewPanel = NULL;

panel *settings = NULL;

button *doButtons[DO_MAX];

static int circleStencilState = 0;

struct
{
    int id;
    char *name;
    char **pixmap;
} stencilIDs[] =
{
    { ST_PIXEL, "Dot", pixel_xpm },
    { ST_VSMALLBALL, "Very Small Ball", verysmallball_xpm },
    { ST_SMALLBALL, "Small Ball", smallball_xpm },
    { ST_BALL, "Ball", ball_xpm },
    { ST_BRICK, "Brick", brick_xpm },
//    { ST_BOX, "Box", closedbox_xpm },
    { ST_FCIRCLE, "Filled Circle", drcirclef_xpm },
    { ST_SELECTOR, "Addition Stencils", NULL },

    { 10, "Ball 1", ball1_xpm },
    { 11, "Ball 2", ball2_xpm },
    { 12, "Ball 3", ball3_xpm },
    { 13, "Ball 4", ball4_xpm },
    { 14, "Ball 5", ball5_xpm },
    { 15, "Ball 6", ball6_xpm },
    { 16, "Ball 7", ball7_xpm },

    { 20, "Box 1", box1_xpm },
    { 21, "Box 2", box2_xpm },
    { 22, "Box 3", box3_xpm },
    { 23, "Box 4", box4_xpm },
    { 24, "Box 5", box5_xpm },
    { 25, "Box 6", box6_xpm },
    { 26, "Box 7", box7_xpm },

    { 30, "Brick 1", brick1_xpm },
    { 31, "Brick 2", brick2_xpm },
    { 32, "Brick 3", brick3_xpm },
    { 33, "Brick 4", brick4_xpm },
    { 34, "Brick 5", brick5_xpm },
    { 35, "Brick 6", brick6_xpm },
    { 36, "Brick 7", brick7_xpm },

    { 40, "HLine1", hline_xpm },
    { 41, "HLine2", thick_hline_xpm },
    { 42, "VLine1", vline_xpm },
    { 43, "VLine2", thick_vline_xpm },
    { 44, "Down", downhill_xpm },
    { 45, "Up", uphill_xpm },
    { 46, "Bowl", bowl2_xpm },
};

int currentStencil = 0;
int previousStencilId = 0;
button *stencilButtons[100];

button *buttonPush;
button *buttonMagic;
button *buttonBowl;
button *buttonFlood;

button *trashButton;
button *recordButton;
button *ffwdButton;
button *playButton;
button *replayProgressButton;
button *tapeSaveButton;
button *tapeLoadButton;

button *buttonSelector;


button *sgButton;

button *attractButton;
button *buttonPump;
button *statusBoxButton;
#ifdef STATS
button *statAreaButton;
#endif

button *dmButtons[DR_MAX];

SDL_Rect statusBox;

#ifdef STATS
int statAreaX1;
int statAreaY1;
int statAreaX2;
int statAreaY2;
#endif

int probeButtonVal = 0;

char tmpStr[128];
void showPanel (panel *pPanel, int doShow);
void buttonBorderColor (button * b, int col);
void ioModeButton (button * b);
void deselectCurrentStencilButton ();
void resumeSmallStencil();


button * oldStencil = NULL;
button *mpSelection = NULL;

char **stencil = NULL;

void actionButtonSelect (button * b);
void startSelectionBox (int start);
void drawButton (button *pButton, int doDraw);
void drawModeButtonSelect (button *b);

int numPtButtons;
button *_ptButtons[10];

void fileLeft (void);
void fileRight (void);

void cbPartAttraction (setting *s)
{
//    enablePanel (gravPanel, s->value);
}

void showMpObjectButton (int show)
{
    if (mpSelection)
        enableButton (mpSelection, show);
}

/* mode:
 * 0 : Show particle controls
 * 1 : show wall controls
 * 2 : hide all
 * 3 : show re-apply current mode
 */
void showWallControls (int mode)
{
    static int currentWallControl = -1;
    static int hidden = 0;

    if (mode < 2)
        currentWallControl = mode;
    else if (mode == 2)
        hidden = 1;
    else if (mode == 3)
        hidden = 0;

    if (hidden)
    {
        showPanel (wallSettings, 0);
        showPanel (particleSettings, 0);
        showPanel (pdescs[GETVAL32(ptype)]->pPanel, 0);
    }
    else if (currentWallControl == 0)
    {
        showPanel (wallSettings, 0);
        showPanel (particleSettings, 1);
        showPanel (pdescs[GETVAL32(ptype)]->pPanel, 1);
    }
    else if (currentWallControl == 1)
    {
        showPanel (particleSettings, 0);
        showPanel (pdescs[GETVAL32(ptype)]->pPanel, 0);
        showPanel (wallSettings, 1);
    }
}

int floodMode()
{
    return (currentDrawMode() == DR_FLOOD);
}

void currentStencilRadius(int *rx, int *ry)
{
    char **c = stencilPixmap (currentStencil);
    int x, y;
    int minx, maxx, miny, maxy;
    int found;

    minx = miny = 1000;
    maxx = maxy = 0;
    for (x = 0; x < 31; x++)
        for (y = 0; y < 31; y++)
        {
            if (c[3+y][x] == ' ')
                continue;

            minx = MIN(x, minx);
            miny = MIN(y, miny);
            maxx = MAX(x, maxx);
            maxy = MAX(y, maxy);
            found = 1;
        }

    if (!found)
    {
        *rx = *ry = 0;
    }

    minx = abs(minx - 16);
    miny = abs(miny - 16);
    maxx = abs(maxx - 16);
    maxy = abs(maxy - 16);

    *rx = MAX(minx, maxx);
    *ry = MAX(miny, maxy);
}

void currentStencilToCursor()
{
    char **c = stencilPixmap (currentStencil);

    if (curDrawMode == DR_FLOOD)
        c = pixel_cur_xpm;

    if (currentStencil == ST_PIXEL)
    {
        if (getDrawFct() == DO_PIPE_ACCEL)
            c = ball_outline_xpm;
        else
            c = pixel_cur_xpm;
    }
    else if (currentStencil == ST_FCIRCLE)
    {
        c = pixel_cur_xpm;
    }

    if (c)
        setCursor ((void*)c);
}

char **stencilPixmap(int id)
{
    if (stencilButtons[id])
        return stencilButtons[id]->pixmap;

    return NULL;
}

void startCircleStencil (void)
{
    circleStencilState = 1;
}

void stopCircleStencil (void)
{
    circleStencilState = 0;
}

int circleStencilStarted(void)
{
    return circleStencilState;
}

void selectStencil (int id)
{
    button *b = stencilButtons[id];

    if (b == NULL)
        return;

    clearPosCache();

    if (b->parentButton == stencilButtons[ST_SELECTOR])
        setButtonPixmap (b->parentButton, b->pixmap);

    if (id == ST_FCIRCLE)
    {
        stencil = stencilButtons[ST_PIXEL]->pixmap;

        startCircleStencil();
    }
    else
    {
        stencil = stencilButtons[id]->pixmap;
    }

    if (drawingBigStencil())
    {
        /* restore draw action settings for "small stencil" mode
         */
         /*XXX*/
    }

    currentStencil = id;

    currentStencilToCursor();

    if (b->parentButton == stencilButtons[ST_SELECTOR])
        redrawButton (b->parentButton);

    updateSelPanels();

    selectPanelButtons (b->pPanel, 0);
    selectButton (b, 1);
}

void
stencilButtonHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);

    if (IS_SUB_BUTTON(b))
    {
        if (event != SUBPANEL_SELECT)
            return;

        selectButton (b, 1 - b->selected);

        switch ((intptr_t)b->userData)
        {
            case 1:        stencilDrawWall = b->selected;
                            break;
            case 2:        stencilDrawParticles = b->selected;
                            break;
            case 3:        stencilWallToParticle = b->selected;
                            break;
            case 4:        stencilParticleToWall = b->selected;
                            break;
        }

        return;
    }

    actionButtonHandler (b, action, x, y);
}
        

void
stencilSelectorHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);
    int stencilId;

    if (action == REDRAW)
    {
        markButton (b, b->selected, 0);
        return;
    }

    if (IS_SUB_BUTTON(b))
    {
        if (event != SUBPANEL_SELECT)
            return;

        stencilId = (intptr_t)b->userData;

        b = b->parentButton;
    }
    else
    {
        if ((event == BUTTON_RELEASE) && (mouseButton == 1))
        {
            buttonBorderColor (b, COL_ICONDRAW);
            if (MOUSE_OVER_BUTTON(b,x,y))
            {
                stencilId = (intptr_t)b->userData;
            }
            else
            {
                return;
            }
        }
        else if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
        {
            if (MOUSE_OVER_BUTTON(b,x,y))
                buttonBorderColor (b, COL_ORANGE);
            else
                buttonBorderColor (b, COL_ICONDRAW);

            return;
        }
        else
        {
            return;
        }
    }

    selectPanelButtons (b->pPanel, 0);
    selectButton (b,1);

    selectStencil (stencilId);
}

void
mpSelectorHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);

    if (action == REDRAW)
    {
        markButton (b, b->selected, 0);
        return;
    }

    if (IS_SUB_BUTTON(b))
    {
        if (event != SUBPANEL_SELECT)
            return;

        setMpTemplate ((int)b->userData);

        b = b->parentButton;
    }
    else
    {
        if ((event == BUTTON_RELEASE) && (mouseButton == 1))
        {
            buttonBorderColor (b, COL_ICONDRAW);
#if 0
            if (MOUSE_OVER_BUTTON(b,x,y))
            {
                mpId = (intptr_t)b->userData;
            }
            else
            {
                return;
            }
#endif
        }
        else if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
        {
            if (MOUSE_OVER_BUTTON(b,x,y))
                buttonBorderColor (b, COL_ORANGE);
            else
                buttonBorderColor (b, COL_ICONDRAW);

            return;
        }
        else
        {
            return;
        }
    }
}

void updateDrButtons (int stencilMask, int dflStencil, int drawModeMask, int dflMode)
{
    int ena;
    int i;

    for (i = 0; i < NELEMENTS(dmButtons); i++)
    {
        if (dmButtons[i] == NULL)
            continue;

        ena = ((drawModeMask & (1<<i)) != 0);

        enableButton (dmButtons[i], ena);

        if (!ena && dmButtons[i]->selected)
            drawModeButtonSelect (dmButtons[dflMode]);
    }

//    if ((dmButtons[currentDrawMode()]->disabled))
//        drawModeButtonSelect (dmButtons[dflMode]);
        
    for (i = 0; i < NELEMENTS(stencilButtons); i++)
    {
        if ((stencilButtons[i] == NULL) || (stencilButtons[i]->parentButton))
            continue;

        ena = ((stencilMask & (1<<i)) != 0);

        enableButton (stencilButtons[i], ena);
        if (stencilButtons[i]->subPanel)
            enablePanel(stencilButtons[i]->subPanel, ena);
    }

    if (currentStencil && (stencilButtons[currentStencil]->disabled))
        selectStencil (dflStencil);

}

void
statAreaButtonSelect (button * b, int action)
{
    if (EVENT_CODE(action) != BUTTON_PRESS)
        return;

    if (statFunctions[curStatFunc+1] == NULL)
        curStatFunc = 0;
    else
        curStatFunc++;
        
    statFunc = statFunctions[curStatFunc];
    
    statFunc();

}

int drawingBigStencil()
{
    return (getDrawFct() == DO_STENCIL);
}


int currentDrawMode()
{
    return curDrawMode;
}

void drawModeButtonSelect (button *b)
{
    button *sb = b;
    int showStencils = 1;

    if (b->parentButton)
    {
        sb = b->parentButton;
        sb->pixmap = b->pixmap;
    }

    if (b == dmButtons[DR_STAMP])
    {
        curDrawMode = DR_STAMP;
    }
    else if (b == dmButtons[DR_DRAW])
    {
        curDrawMode = DR_DRAW;
    }
    else if (b == dmButtons[DR_LINE])
    {
        curDrawMode = DR_LINE;
        if (b->parentButton)
            setButtonPixmap (b->parentButton, b->pixmap);
    }
    else if (b == dmButtons[DR_CIRCLE])
    {
        curDrawMode = DR_CIRCLE;
        if (b->parentButton)
            setButtonPixmap (b->parentButton, b->pixmap);
    }
    else if (b == dmButtons[DR_CIRCLE_FILL])
    {
        curDrawMode = DR_CIRCLE_FILL;
        selectStencil (ST_PIXEL);
        if (b->parentButton)
            setButtonPixmap (b->parentButton, b->pixmap);
    }
    else if (b == dmButtons[DR_FLOOD])
    {
        curDrawMode = DR_FLOOD;
        showStencils = 0;
    }
    else
    {
        return;
    }

    currentStencilToCursor ();
    selectPanelButtons (sb->pPanel, 0);
    selectButton (sb, 1);
    enablePanel (stencils, showStencils);
}

void setDrawMode (int mode)
{
    button *b = NULL;

    switch (mode)
    {
        case DR_STAMP:
            b = dmButtons[DR_STAMP];
            break;

        case DR_DRAW:
            b = dmButtons[DR_DRAW];
            break;

        case DR_LINE:
            b = dmButtons[DR_LINE];
            break;

        case DR_FLOOD:
            b = dmButtons[DR_FLOOD];
            break;
    }

    if (b)
        drawModeButtonSelect (b);
}

void drawOpModeSelect (int op)
{
    switch (op)
    {
        case DO_PARTICLES:
        case DO_WALL:
        case DO_WALL_AUTOPOLY:
        case DO_ERASE_WALL:
        case DO_ERASE_PAR:
        case DO_PIPE:
        case DO_PIPE_ACCEL:
        case DO_PUMP:
        case DO_PUSH:
        case DO_MOVE:
        case DO_CONVERT:
        case DO_MARK:
            setDrawMode (DR_DRAW);
            break;
    }

}

void stencilFileLoadCb (fileRep *rep, int index)
{
    char imageName[SAVE_PATH_LEN];

    setDrawFct (DO_STENCIL);
    clearStencil(NULL);

    mkFileName (imageName, &stencilRep, NULL, ".tape", index);

    if (!tapeLoad (&stencilTape, imageName))
        return;

    if (!tapeToStencil (NULL, &stencilTape, 0,0))
        return;

    setCursor ((void *) pixel_cur_xpm);

    fileSelection (0, rep);
}

/* select draw operation together with selecting the corresponding button.
 * DO_STENCIL toggles the file selection panel, everything else disables it.
 * DO_STENCIL_NOFS is same as DO_STENCIL except that file-selection panel is
 * not shown.
 */
void
drawOpSelect (int mode)
{
    int nofs = 0;

    if (mode == DO_STENCIL_NOFS)
    {
        nofs = 1;
        mode = DO_STENCIL;
    }

    selectPanelButtons (actions, 0);

    if (doButtons[mode])
    {
        if (doButtons[mode]->parentButton)
            selectButton (doButtons[mode]->parentButton, 1);
        else
            selectButton (doButtons[mode], 1);
    }

    setDrawFct (mode);

    if (mode == DO_STENCIL)
    {
        stencilRep.loadFunc  = (FUNCPTR)stencilFileLoadCb;
        if (!nofs)
            fileSelection (2, &stencilRep);
    }
    else
    {
        fileSelection (0, &stencilRep);
        clearStencil(NULL);
        currentStencilToCursor();
    }

    if ((mode == DO_WALL) || (mode == DO_WALL_AUTOPOLY) ||
        (mode == DO_WALL_POLY) ||
        (mode == DO_PIPE) || (mode == DO_PIPE_ACCEL) ||
        (mode == DO_PUSH)
        )
    {
        showWallControls (1);
        showWallControls (3);
    }
    else if (mode == DO_PARTICLES)
    {
        showWallControls (0);
        showWallControls (3);
    }
    
    if (mode == DO_PAINT)
    {
        pickColor (&paintColor);
    }
    else
    {
        exitPicker();
    }
}

void
drOpButtonHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);
    int drOp = b->userData;

    if (IS_SUB_BUTTON(b))
    {
        if (event != SUBPANEL_SELECT)
            return;

        if ((intptr_t)b->parentButton->userData == DO_STENCIL)
        {
            selectButton (b, 1 - b->selected);

            switch ((intptr_t)b->userData)
            {
                case 1:        stencilDrawWall = b->selected;
                        break;
                case 2:        stencilDrawParticles = b->selected;
                        break;
                case 3:        stencilWallToParticle = b->selected;
                        break;
                case 4:        stencilParticleToWall = b->selected;
                        break;
            }
            return;
        }

        switch ((intptr_t)b->userData)
        {
            case DO_WIPE_ALL:
                wipe();
                return;

            case DO_REMPART_ALL:
                removeAllParticles();
                return;

            case DO_MARK_ALL:
                markAll();
                drawOpSelect (DO_MARK);
                return;

            case DO_UNMARK_ALL:
                markClear();
                drawOpSelect (DO_MARK);
                return;

            case 7:
                if (b->parentButton->bgColorSetting)
                {
                    pickColor (b->parentButton->bgColorSetting);
                }
                break;
                
        }


        setButtonPixmap (b->parentButton, b->pixmap);

        drawOpSelect ((intptr_t)b->userData);
        return;
    }

    if (action == REDRAW)
    {
        markButton (b, b->selected, 0);
        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, ((drOp == DO_CONVERT) && applyToAll) ? COL_RED : COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (drOp == DO_CONVERT)
            applyToAll = 0;

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            int op = (intptr_t)b->userData;

            drawOpSelect (op);
        }
    }
    else if ((event == BUTTON_RELEASE) && (mouseButton == 2))
    {
        if (drOp == DO_CONVERT)
        {
            applyToAll = 1 - applyToAll;

            buttonBorderColor (b, applyToAll ? COL_RED : COL_ICONDRAW);
        }
    }
}

void setApplyToAll (int ena)
{

    applyToAll = ena;

    buttonBorderColor (doButtons[DO_CONVERT], applyToAll ? COL_RED : COL_ICONDRAW);
}


#ifdef PROBE
void
probeButtonHandler (button * b, int action, int x, int y)
{
    onOffButton (b, action, x, y);

    if (EVENT_CODE(action) == BUTTON_RELEASE)
    {
        probeStart (0);

        if (probeButtonVal)
        {
            lastAction = unselectActionButtons();
            setDrawFct ((FUNCPTR)drawFctProbe);
        }
        else
        {
            if (lastAction != NULL)
                actionButtonSelect (lastAction);
            
            lastAction = NULL;
        }
    }
}
#endif



void
dropButtonSelect (button * b, int action)
{
    if (EVENT_CODE(action) != BUTTON_PRESS)
        return;

    chainParticles = 1 - chainParticles;

    selectButton (b, chainParticles);
}



void applyGravVec()
{
    screeninfo *pSi = siArray;
    int i;

    for (i = 0; i < (hei + 1) * wid; i++, pSi++)
    {
        if (pSi->flags & (SI_IS_PUMP | SI_BRICK))
            continue;

        XACCEL(pSi) = gravVec.d.x;
        YACCEL(pSi) = gravVec.d.y;
    }
}


void
gravityButtonSelect (button * b, int action, int ox, int oy)
{
    int x=0, y=0;
    FLOAT vec;
    FLOAT di;

    if (IS_RESET(action))
    {
        ox = BUTTON_WID(b) / 2;
        oy = BUTTON_HEI(b) / 2;
    }
    else if (action == REDRAW)
    {
        ox = gravVec.d.x;
        oy = gravVec.d.y;

        vec = VEC_LEN (ox, oy);

        for (x = 0; x < BUTTON_WID(b); x++)
            for (y = 0; y < BUTTON_HEI(b); y++)
                buttonDrawPixel (b, x, y, COL_EMPTY);;

        if ((ox != 0) || (oy != 0))
        {
            for (di = 0.0; di <= vec; di += 1.0)
            {
                x = (BUTTON_WID(b) / 2) + (int) ((FLOAT) ox / vec * di);
                y = (BUTTON_HEI(b) / 2) + (int) ((FLOAT) oy / vec * di);
                buttonDrawPixel (b, x, y, COL_ICONDRAW);
            }
            buttonDrawPixel (b, x - 1, y, COL_ICONDRAW);
            buttonDrawPixel (b, x + 1, y, COL_ICONDRAW);
            buttonDrawPixel (b, x, y - 1, COL_ICONDRAW);
            buttonDrawPixel (b, x, y + 1, COL_ICONDRAW);
        }

        buttonDrawPixel (b, BUTTON_WID(b) / 2, BUTTON_HEI(b) / 2, COL_ICONFG);

        return;
    }
    else if (EVENT_CODE(action) == SCROLLDOWN)
    {
        gravVec.d.y++;

        if (gravVec.d.y > BUTTON_HEI(b) / 2 )
            gravVec.d.y = BUTTON_HEI(b) / 2-1;

        oy = gravVec.d.y + BUTTON_HEI(b) / 2;
        ox = BUTTON_WID(b) / 2;
    }
    else if (EVENT_CODE(action) == SCROLLUP)
    {
        gravVec.d.y--;

        if (gravVec.d.y <= -(BUTTON_HEI(b) / 2) + 1 )
            gravVec.d.y = -(BUTTON_HEI(b) / 2) + 2;

        oy = gravVec.d.y + BUTTON_HEI(b) / 2;
        ox = BUTTON_WID(b) / 2;
    }
    else if ((BUTTON_ID(action) != 1) && (BUTTON_ID(action) != 2))
    {
        return;
    }

    if (ox < 2)
    {
        ox = 2;
    }
    if (ox >= BUTTON_WID(b))
    {
        ox = BUTTON_WID(b);
    }
    if (oy < 2)
    {
        oy = 2;
    }
    if (oy >= BUTTON_HEI(b))
    {
        oy = BUTTON_HEI(b);
    }

    ox -= BUTTON_WID(b) / 2;
    oy -= BUTTON_HEI(b) / 2;

#if 0
    if (BUTTON_ID(action) == 2)
    {
        if (abs(ox) > abs(oy))
            oy = 0;
        else
            ox = 0;
    }
#endif

    gravVec.d.x = ox;
    gravVec.d.y = oy;

    if (EVENT_CODE(action) == BUTTON_RELEASE)
    {
        applyGravVec();
    }
    else
    {
        INTERVAL_RUN (250, applyGravVec())

    }

    sprintf (tmpStr, "Gravity=%d/%d", ox, oy);
    statText (4, tmpStr);

    gvX.pButton = b;
    gvY.pButton = b;

    modifySetting (&gvX, ox, 0);
    modifySetting (&gvY, oy, 0);

#ifdef USE_OPENCL
    clGravVecSet (&gravVec);
#endif

}

void cbGrav (setting *s)
{
    gravVec.d.x = getValue (&gvX);
    gravVec.d.y = getValue (&gvY);

    if (s->pButton)
        gravityButtonSelect (s->pButton, REDRAW, 0, 0);
}



#ifndef _CYGWIN
extern double log2(double);
#endif

void selectRecordButton (int sel)
{
    if (tapeEmpty(&playbackTape))
    {
        buttonBorderColor (recordButton, COL_ICONDRAW);
    }
    else
    {
        buttonBorderColor (recordButton, COL_ORANGE);
    }

    selectButton (recordButton, sel);
}

void
showReplayProgress (button * b, int action, int x, int y)
{
    int col;
    static int lastValue = 0;
    int value;
    int pos, max;

    if (action != REDRAW)
        return;

    if (!playbackInfo (&pos, &max) || (max == 0))
        value = 0;
    else
        value = (pos * (BUTTON_WID(b)+1)) / max;

    if (value == lastValue)
        return;

    lastValue = value;
    
    for (x = 1; x <= BUTTON_WID(b); x++)
    {
        col = (x <= value) ? COL_RED : COL_EMPTY;

        for (y = 1; y <= BUTTON_HEI(b); y++)
        {
            buttonDrawPixel (b, x,  y, col);
        }
    }
}

void replayProgress (void)
{
    showReplayProgress (replayProgressButton, REDRAW, 0, 0);
}

void doReplay (button * b)
{
    if (b == tapeLoadButton)
    {
        loadTape ();

        selectRecordButton (0);
    }

    if (b == tapeSaveButton)
    {
        saveTape();
    }

    if (b == trashButton)
    {
        static int lastSelection = 0;

        if ((planschTime() - lastSelection) > 500)
        {
            lastSelection = planschTime();
        }
        else
        {
            eraseTape(&playbackTape);

            selectButton (playButton, 0);
            startPlayback(0);
            
            selectRecordButton (0);
            startRecord (0);
        }
    }

    if (b == playButton)
    {
        selectRecordButton (0);
        startRecord (0);

        startPlayback (shiftPressed() ? 2 : 1);
        selectButton (b,1);
    }

    if (b == ffwdButton)
    {
        selectRecordButton (0);
        startRecord (0);

        selectButton (playButton, 0);

        playbackAll (&playbackTape, NULL, NULL, 0,0,0xffffffff);
    }

    if (b == recordButton)
    {
        selectRecordButton (1 - b->selected);

        if (b->selected)
        {
            startRecord (1);

            startPlayback (0);
            selectButton (playButton, 0);
        }
        else
        {
            startRecord (0);
        }
    }
}

void callPicker (button *b)
{
    setting *colorSetting = b->pSetting;

    pickColor (colorSetting);
}

void
makeButtons (void)
{
    int i, j;
    button *b1;
    panel *p;

    memset ((char*)stencilButtons, 0, sizeof(stencilButtons));

    init_widgets();

    setCurrentPanelGid (1);
    
    actions = makePanel ("actions", 1, 0, hei+1, P_RIGHT_DOWN, 1);
    drmodes = makePanel ("drmodes", 1, 0, 0, P_RIGHT_DOWN, 1);
    connectPanel (drmodes, actions, RIGHT_SIDE);

    stencils = makePanel ("stencils", 1, 0, 0, P_RIGHT_DOWN, 1);
    connectPanel (stencils, drmodes, RIGHT_SIDE);

    cutCopySavePanel = makePanel ("cutCopySave", 1, wid-2, hei + 1, P_LEFT_DOWN, 1);
    recorder = makePanel ("recorder", 1, 0, 0, P_LEFT_DOWN, 1);
    connectPanel (recorder, cutCopySavePanel, BOT_SIDE);

    globalSettings = makePanel ("globalSettings", 1, 0, 0, P_DOWN_RIGHT, 1);
    connectPanel (globalSettings, actions, BOT_SIDE);

    particleSettings = makePanel ("particleSettings", 1, 0, 0, P_RIGHT_DOWN, 1);
    connectPanel (particleSettings, globalSettings, BOT_SIDE);

    wallSettings = makePanel ("wallSettings", 1, 0, 0, P_DOWN_RIGHT, 1);
    connectPanel (wallSettings, globalSettings, BOT_SIDE);

    statusPanel = makePanel ("status", 0, 0, 0, P_LEFT_DOWN, 1);
    connectPanel (statusPanel, recorder, BOT_SIDE);

    viewPanel = makePanel ("view", 1, 0, 0, P_DOWN_RIGHT, 1);
    connectPanel (viewPanel, globalSettings, RIGHT_SIDE);

    gravPanel = makePanel ("grav", 1, 0, 0, P_DOWN_RIGHT, 1);
    connectPanel (gravPanel, viewPanel, RIGHT_SIDE);

    b1 = makeButton (actions, drOpButtonHandler, 32, 32, wall_pm, "wall", DO_WALL, NULL, B_NEXT);
    makeSubPanel (b1, P_DOWN_RIGHT, 1, SP_BUTTON1 | SP_CENTER_CURRENT);
    doButtons[DO_WALL] = addSubButton (b1, 32, 32, wall_pm, "wall", DO_WALL, B_NEXT);
    doButtons[DO_WALL_POLY] = addSubButton (b1, 32, 32, wall_vec_pm, "vectorizedWall", DO_WALL_POLY, B_NEXT);
    doButtons[DO_WALL_AUTOPOLY] = addSubButton (b1, 32, 32, wall_vec2_pm, "autovectorize", DO_WALL_AUTOPOLY, B_NEXT);

    doButtons[DO_PARTICLES] = makeButton (actions, drOpButtonHandler, 32, 32, particles_pm, "particles", DO_PARTICLES, NULL, B_NEXT);

    encodeARGB(&paintColor.current.val.cv, 1.0, 0.5, 0.1, 0.3);
    paintColor.defaultValue = paintColor.current.val.va64;
    doButtons[DO_PAINT] = paintColor.pButton = 
                makeButton (actions, drOpButtonHandler, 32, 32, colors_xpm, "Paint", DO_PAINT, NULL, B_NEXT);
    doButtons[DO_PAINT]->pSetting = &paintColor;
    doButtons[DO_PAINT]->useSelMarker = 1;
    
    doButtons[DO_STENCIL] = makeButton (actions, drOpButtonHandler, 32, 32, duck_pm, "stencil", DO_STENCIL, NULL, B_NEXT);
    makeSubPanel (doButtons[DO_STENCIL], P_DOWN_RIGHT, 1, SP_BUTTON3 | SP_CENTER_CURRENT);
    selectButton (addSubButton (doButtons[DO_STENCIL], 60,32, drawWall_pm, "Draw Wall in Stencil", 1, B_NEXT), 1);
    selectButton (addSubButton (doButtons[DO_STENCIL], 60,32, drawParticles_pm, "Draw Particles in Stencil", 2, B_NEXT), 1);
    selectButton (addSubButton (doButtons[DO_STENCIL], 60,32, wallToParticle_pm, "Convert Wall to Particle", 3, B_NEXT), 0);
    selectButton (addSubButton (doButtons[DO_STENCIL], 60,32, particleToWall_pm, "Convert Particle to Wall", 4, B_NEXT), 0);

    b1 = makeButton (actions, drOpButtonHandler, 32, 32, erase_pm, "erase", DO_ERASE_WALL, NULL, B_NEXT);
    makeSubPanel (b1, P_DOWN_RIGHT, 1, SP_BUTTON1);
    doButtons[DO_ERASE_WALL] = addSubButton (b1, 32, 32, erase_pm, "EraseWall", DO_ERASE_WALL, B_NEXT);
    doButtons[DO_ERASE_PAR] = addSubButton (b1, 32, 32, particleErase_pm, "Erase Particle(s)", DO_ERASE_PAR, B_NEXT);
    addSubButton (b1, 32, 32, wipe_pm, "Remove All Walls", DO_WIPE_ALL, B_NEXT);
    addSubButton (b1, 32, 32, rempart_pm, "Remove All Particles", DO_REMPART_ALL, B_NEXT);

    b1 = makeButton (actions, drOpButtonHandler, 32, 32, pipe_pm, "pipe", DO_PIPE, NULL, B_NEXT);
    makeSubPanel (b1, P_DOWN_RIGHT, 1, SP_BUTTON1 | SP_CENTER_CURRENT);
    doButtons[DO_PIPE] = addSubButton (b1, 32, 32, pipe_pm, "Draw Pipe", DO_PIPE, B_NEXT);
    doButtons[DO_PIPE_ACCEL] = addSubButton (b1, 32, 32, pipe_accel_pm, "Draw Pipe with accelleration", DO_PIPE_ACCEL, B_NEXT);

    doButtons[DO_PUMP] = makeButton (actions, drOpButtonHandler, 32, 32, accel_pm, "pump", DO_PUMP, NULL, B_NEXT);
    doButtons[DO_PUSH] = makeButton (actions, drOpButtonHandler, 32, 32, push_pm, "push", DO_PUSH, NULL, B_NEXT);
    doButtons[DO_MOVE] = makeButton (actions, drOpButtonHandler, 32, 32, grab_pm, "move", DO_MOVE, NULL, B_NEXT);
    doButtons[DO_CONVERT] = makeButton (actions, drOpButtonHandler, 32, 32, psapply_pm, "props", DO_CONVERT, NULL, B_NEXT);

    b1 = makeButton (actions, drOpButtonHandler, 32, 32, mark_pm, "Mark", DO_MARK, NULL, B_NEXT);
    makeSubPanel (b1, P_DOWN_RIGHT, 1, SP_BUTTON1);
    doButtons[DO_MARK] = addSubButton (b1, 32, 32, mark_pm, "Mark", DO_MARK, B_NEXT);
    doButtons[DO_MARK_ALL] = addSubButton (b1, 32, 32, markall_pm, "Mark All", DO_MARK_ALL, B_NEXT);
    doButtons[DO_UNMARK_ALL] = addSubButton (b1, 32, 32, unmarkall_pm, "Unmark All", DO_UNMARK_ALL, B_NEXT);


#ifdef PROBE_BUTTON
    doButtons[DO_PROBE] = makeButton (actions, probeButtonHandler, 32, 32, measure_pm, "Probe", (intptr_t)&probeButtonVal, NULL, B_NEXT);
#endif

    showPanel (actions, 1);

    dmButtons[DR_STAMP] = makeButton (drmodes, actionButtonHandler, 32, 32, stamp_pm, "Stamp", (intptr_t)drawModeButtonSelect, NULL, B_NEXT);
    dmButtons[DR_DRAW] = makeButton (drmodes, actionButtonHandler, 32, 32, draw_pm, "Draw", (intptr_t)drawModeButtonSelect, NULL, B_NEXT);
    b1 = makeButton (drmodes, actionButtonHandler, 32, 32, line_pm, "Line", (intptr_t)drawModeButtonSelect, NULL, B_NEXT);
    makeSubPanel (b1, P_DOWN_RIGHT, 1, SP_BUTTON1 | SP_CENTER_CURRENT);
    dmButtons[DR_LINE] = addSubButton (b1, 32, 32, line_pm, "Line", (intptr_t)drawModeButtonSelect, B_NEXT);
    dmButtons[DR_CIRCLE] = addSubButton (b1, 32, 32, drcircle_pm, "Circle", (intptr_t)drawModeButtonSelect, B_NEXT);

    dmButtons[DR_FLOOD] = makeButton (drmodes, actionButtonHandler, 32, 32, fill_pm, "Fill", (intptr_t)drawModeButtonSelect, NULL, B_NEXT);
    showPanel (drmodes, 1);

    stencilButtons[ST_SELECTOR] = makeButton (stencils, stencilSelectorHandler, 32, 32, NULL, "mask", 0, NULL, B_NEXT);
    makeSubPanel (stencilButtons[ST_SELECTOR], P_RIGHT_DOWN, 1, SP_BUTTON1 | SP_CENTER_CURRENT);
    for (i = 0, j = 0; i < NELEMENTS(stencilIDs); i++)
    {
        int id = stencilIDs[i].id;

        if (id == ST_SELECTOR)
            continue;

        if (id >= NELEMENTS(stencilButtons))
        {
            printf ("stencilButtons array too small\n");
            continue;
        }

        if (id < ST_SELECTOR)
        {
            stencilButtons[id] = makeButton (stencils, stencilSelectorHandler, 32, 32, stencilIDs[i].pixmap, "mask", id, NULL, B_NEXT);
        }
        else
        {
            stencilButtons[id] =
                addSubButton (stencilButtons[ST_SELECTOR], 32, 32, stencilIDs[i].pixmap, stencilIDs[i].name, id,
                ((j % 7) == 0) ? B_NEXTRC : B_NEXT);

            j++;
        }
    }
    mpSelection = makeButton (stencils, mpSelectorHandler, 32, 32, NULL, "mpo", 0, NULL, B_NEXT);
    makeSubPanel (mpSelection, P_RIGHT_DOWN, 1, SP_BUTTON1 | SP_CENTER_CURRENT);
    initMpSelector (mpSelection);

    showPanel (stencils, 1);

    initSelectPanel (cutCopySavePanel);
    showPanel (cutCopySavePanel, 1);

    rightSideWid = abs(cutCopySavePanel->start.d.x - cutCopySavePanel->cur.d.x);
    rightSideWid = MAX(rightSideWid, abs(recorder->start.d.x - recorder->cur.d.x));

    /* ----------------- Recorder ---------------------------- */
    tapeSaveButton = makeButton (recorder, actionButtonHandler, 32, 32, saveTape_pm, "Save", (intptr_t)doReplay, NULL, B_NEXT);
    tapeLoadButton = makeButton (recorder, actionButtonHandler, 32, 32, loadTape_pm, "Load", (intptr_t)doReplay, NULL, B_NEXT);
    trashButton = makeButton (recorder, actionButtonHandler, 32, 32, trash_pm, "ClearTape", (intptr_t)doReplay, NULL, B_NEXT);
    recordButton = makeButton (recorder, actionButtonHandler, 32, 32, record_pm, "Start/Pause Recorder", (intptr_t)doReplay, NULL, B_NEXT);
    ffwdButton = makeButton (recorder, actionButtonHandler, 32, 32, ffwd_pm, "Fast replay", (intptr_t)doReplay, NULL, B_NEXT);
    playButton = makeButton (recorder, actionButtonHandler, 32, 32, playback_pm, "Replay", (intptr_t)doReplay, NULL, B_NEXT);

    replayProgressButton = makeButton (recorder, showReplayProgress, rightSideWid-2, 5, NULL, "Progress", 0, NULL, B_NEXTRC);

    showPanel (recorder, 1);

    /* ----------------- Global Settings -------------------- */
    b1 = makeButton (globalSettings, gravityButtonSelect, 68, 68, NULL, "GravityVector", 0, NULL, B_NEXT);
    gvX.pButton = gvY.pButton = b1;
    makeButton (globalSettings, expSlider, 32, 68, unshare_pm, "ParticleRepulsion", 0, &preassure, B_NEXTRC);
    makeButton (globalSettings, slider, 32, 68, damping_pm, "Wall Damping", 0, &wallDampingI, B_NEXTRC);
    makeButton (globalSettings, sliderS, 32, 68, speed_pm, "decel", 0, &decelLog, B_NEXTRC);
    makeButton (globalSettings, onOffButtonS, 32, 32, therm_pm, "Heating", 0, &wallTempr, B_NEXTRC);

    makeButton (viewPanel, onOffButtonS, 32, 32, pLinks_xpm, "ShowLinks", 0, &showLinks, B_NEXTRC);

#ifdef USE_OPENGL
    makeButton (viewPanel, onOffButtonS, 32, 32, trails_xpm, "particleTrails", 0, &particleTrails, B_NEXT | B_SPC(2));
    makeButton (viewPanel, slider, 32, 67, hide_xpm, "Particle Alpha", 0, &partAlpha, B_NEXTRC);
#else
    makeButton (viewPanel, onOffButtonS, 32, 32, hide_xpm, "Hide Particles", 0, &hideParticles, B_NEXTRC);
#endif

#ifdef USE_OPENGL
    makeButton (viewPanel, slider, 32, 53, psize_vis_xpm, particleVisSize.name, 0, &particleVisSize, B_NEXTRC);
    makeButton (viewPanel, onOffButtonS, 32, 13, psmooth_xpm, "Smooth GPU Particles", 0, &particleSmooth, B_NEXT|B_SPC(0));
    makeButton (viewPanel, slider, 32, 67, fade_xpm, particleFade.name, 0, &particleFade, B_NEXTRC);
    makeButton (viewPanel, slider, 32, 67, blur_xpm, particleBlur.name, 0, &particleBlur, B_NEXTRC);
    makeButton (viewPanel, slider, 32, 67, colormerge_xpm, colMergeSpeed.name, 0, &colMergeSpeed, B_NEXTRC);
    makeButton (viewPanel, slider, 32, 67, colorrestore_xpm, colRestoreSpeed.name, 0, &colRestoreSpeed, B_NEXTRC);
#endif

    if (options->useGpu == 0)
    {
#if !defined(USE_OPENGL) || !defined(OPENGL_NOGRAVITY)
        makeButton (gravPanel, onOffButtonS, 32, 32, grav_xpm, "Gravity", 0, &particleAttraction, B_NEXTRC);
        makeButton (gravPanel, onOffButton, 32, 32, grav_show_xpm, "GShow", (intptr_t)&showGravity, NULL, B_NEXTRC);
        makeButton (gravPanel, slider, 32, 32, grav_show_pot_xpm, "GFDivider", 0, &potDivider, B_NEXTRC);
        makeButton (gravPanel, slider, 32, 32, grav_show_ripple_xpm, "CRDivider", 0, &rippleDivider, B_NEXTRC);
#endif
    }
    else
    {
#ifdef USE_OPENGL
#ifdef USE_OPENCL
        makeButton (gravPanel, onOffButtonS, 32, 32, grav_xpm, "Gravity", 0, &particleAttraction, B_NEXTRC);
        makeButton (gravPanel, onOffButtonS, 32, 32, gravlines_xpm, "FieldLines", 0, &fieldLines, B_NEXT);
//        makeButton (gravPanel, onOffButtonS, 32, 32, fieldborder_xpm, "FieldBorder", 0, &fieldBorder, B_NEXTRC);
#else
        makeButton (gravPanel, onOffButtonS, 32, 32, grav_xpm, "Gravity", 0, &particleAttraction, B_NEXTRC);
#endif
        makeButton (gravPanel, slider, 32, 67, NULL, "Graph Alpha", 0, &gravAlpha, B_NEXTRC);

        makeButton (gravPanel, slider, 32, 67, grav_show_pot_xpm, "GFDivider", 0, &potDivider, B_NEXTRC);
        makeButton (gravPanel, slider, 32, 67, grav_show_ripple_xpm, "CRDivider", 0, &rippleDivider, B_NEXTRC);

        makeButton (gravPanel, slider, 32, 67, grav_damping_pm, gpuGravDamping.name, 0, &gpuGravDamping, B_NEXTRC | B_SPC(2));
        makeButton (gravPanel, slider, 32, 67, grav_strength_pm, gpuGravStrength.name, 0, &gpuGravStrength, B_NEXTRC);
        makeButton (gravPanel, asySlider, 32, 67, grav_wave_damping_pm, gpuGravWaveDamping.name, 0, &gpuGravWaveDamping, B_NEXTRC);
#ifdef USE_OPENCL
        makeButton (gravPanel, asySlider, 32, 67, fieldborder_xpm, "Field Cutoff", 0, &gpuBorderFieldCutoff, B_NEXTRC);
#endif
#endif
    }

    showPanel (gravPanel, 1);
    showPanel (viewPanel, 1);
    showPanel (globalSettings, 1);

    modifySetting (&particleAttraction, 0, 0);

    
    /* ----------------- Particle Settings -------------------- */

    {
        for (i = j = 0; pdescs[i] != NULL; i++)
        {
            int r = ((j % 2) == 0) ? B_NEXTRC : B_NEXT;

            if (pdescs[i]->skip)
                continue;

            r |= B_SPC(0);

            b1 = makeButton (particleSettings, onOffGroupS, 90, 16, pdescs[i]->pm, pdescs[i]->name, i, &ptype, r);
            setButtonHelp (b1, pdescs[i]->help);
            b1->text = pdescs[i]->name;

            j++;
        }

        numPtButtons = i;
    }

    /* particle type specific panels */
    for (i = 0; pdescs[i] != NULL; i++)
    {
        pWidget        *w;

        if (pdescs[i]->skip)
        {
            pdescs[i]->hasPanel = 0;
            continue;
        }

        if (pdescs[i]->widgets[0].handler == NULL)
        {
            pdescs[i]->hasPanel = 0;
            continue;
        }

        p = pdescs[i]->pPanel = makePanel ("pdesc", 1, 0, 0, P_DOWN_RIGHT, 2);

        connectPanel (p, particleSettings, RIGHT_SIDE);

        pdescs[i]->hasPanel = 1;

        b1 = NULL;
        for (w = pdescs[i]->widgets; w->handler != NULL; w++)
        {
            setting *si;

            si = w->pSetting;
            
            if (!w->help)
            {
                if (si)
                    w->help = si->name;
                else
                    w->help = "?";
            }
            
            if (si && si->isArray)
            {
                si = &si->arrayFirst[i];

                /* reassign hash value. Calculate it based on the particle type name, not in the 
                 * index (which may change in future).
                 * The hash is used in save files to identify a setting, so using the particle type
                 * name enhances future compatibility.
                 */
                si->hash = namesToHash (si->id->name, pdescs[i]->name);
            }

            b1 = makeButton (p, (VOIDFUNCPTR)w->handler, w->wid, w->hei, w->icon, w->help, w->userData, si, w->attr);

            if (strstr (b1->name, "Charge"))
            {
                value_u col;

                b1->useSelMarker = 1;
                switch (w->userData)
                {
                    case -1:        
                        pdescs[i]->crangeColors[0] = b1->bgColorSetting; 
                        encodeHSLA (&col.cv, cvToHue(CV_RED), 1.0, 0.5f, 1.0f);
                        modifySetting (b1->bgColorSetting, col.va64, 0);
                        break;
                    case  0:        
                        pdescs[i]->crangeColors[1] = b1->bgColorSetting; 
                        encodeHSLA (&col.cv, cvToHue(CV_GREEN), 1.0, 0.5f, 1.0f);
                        modifySetting (b1->bgColorSetting, col.va64, 0);
                        break;
                    case  1:        
                        pdescs[i]->crangeColors[2] = b1->bgColorSetting; 
                        encodeHSLA (&col.cv, cvToHue(CV_BLUE), 1.0, 0.5f, 1.0f);
                        modifySetting (b1->bgColorSetting, col.va64, 0);
                        break;
                }
            }

            w->handler (b1, REDRAW, 0,0);
        }
    }

    showPanel (particleSettings, 1);

    /* ----------------- Wall Settings -------------------- */
    p = wallSettings;
    for (i = 0; pdescs[i] != NULL; i++)
    {
        /* find index of the first unused particle desc. This is the settings array index used for the
         * wall pixels (mass, charge)
         */
    }
    wallArrayIndex = i;
    makeButton (p, slider, 32, 67, friction_pm, "Wall Friction", 0, &wallFriction, B_NEXTRC);
    makeButton (p, slider, 34, 67, heavy_pm, "Wall Mass", 0, &particleWeight[i], B_NEXTRC);
    particleWeight[i].hash = namesToHash (particleWeight[i].id->name, "Wall");
    particleWeight[i].wallSetting = 1;

    makeButton (p, onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[i], B_NEXTRC);
    makeButton (p, onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[i], B_NEXT|B_SPC(1));
    makeButton (p, onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[i], B_NEXT|B_SPC(1));
    particleAttractionForce[i].hash = namesToHash (particleAttractionForce[i].id->name, "Wall");
    particleAttractionForce[i].wallSetting = 1;

    /* ---------------- Status and statistic box -------------------- */
    statusBoxButton = makeButton (statusPanel, NULL, rightSideWid-2, 
        options->scrHei - statusPanel->start.d.y - 2, NULL, NULL, 0, NULL, B_NEXT);
#ifdef STATS
    statAreaButton = makeButton (statusPanel, statAreaButtonSelect, 
        64, 1000, NULL, NULL, 0, NULL, B_NEXT);
#endif

    showPanel (statusPanel, 1);

    fileSelector = makePanel ("fileSelector", 1, 0, 0, P_RIGHT_DOWN, 1);
    connectPanel (fileSelector, actions, BOT_SIDE);

    setPanelMax (fileSelector, options->scrWid - rightSideWid - 2, 100);
    initFileSelectorPanel (fileSelector);
    showPanel (fileSelector, 0);

#ifdef STATS
    statAreaX1 = statAreaButton->x1 + 1;;
    statAreaY1 = statAreaButton->y1 + 1;;
    statAreaX2 = statAreaButton->x2 - 1;
    statAreaY2 = statAreaButton->y2 - 1;
#endif
    
     /* ---------------- Geneal settings on 2nd panel set ------------------ */

    settings = makePanel ("settings", 2, 0, hei+1, P_RIGHT_DOWN, 1);
    
    b1 = makeButton (settings, slider, 150, 20, NULL, "Min. Update Interval", 0, &updateInterval, B_NEXTRC);
    b1->text = "Upate Interval (ms)";
    b1 = makeButton (settings, onOffButtonS, 20, 20, NULL, "Draw particles only during screen update", 0, &drawWithScreen, B_NEXT);
    b1->text = "";
    
    b1 = makeButton (settings, slider, 150, 20, NULL, "Max. Iterations/sec", 0, &maxFps, B_NEXTRC);
    b1->text = "Iterations/sec";
    b1 = makeButton (settings, slider, 150, 20, NULL, "GPU Iterations/CPU loop", 0, &gravIterations, B_NEXTRC);
    b1->text = "GPU Iterations/loop";
#ifdef USE_OPENGL
    if (oglDedicatedThread())
    {
        b1 = makeButton (settings, onOffButtonS, 150, 20, NULL, "GPU Sync", 0, &gpuSync, B_NEXTRC);
        b1->text = "CPU/GPU Sync";
    }
#endif

    makeButton (settings, onOffGroupS, 32, 21, po_forward_xpm, "Forward", 0, &workOrderSetting, B_NEXTRC);
    makeButton (settings, onOffGroupS, 32, 21, po_backward_xpm, "Reverse", 1, &workOrderSetting, B_NEXT);
    makeButton (settings, onOffGroupS, 32, 21, po_alt_xpm, "Toggle", 2, &workOrderSetting, B_NEXT);
    
    showPanel (settings, 1);

    initColorPicker (1);    

    
}

SDL_Rect *statBox (void)
{
    static SDL_Rect r;

    if ((statusBoxButton == NULL) || !statusBoxButton->visible || statusBoxButton->disabled)
        return NULL;

    r.x = statusBoxButton->x1 + 1;
    r.y = statusBoxButton->y1 + 1;
    r.w = statusBoxButton->x2 - statusBoxButton->x1 - 3;
    r.h = statusBoxButton->y2 - statusBoxButton->y1 - 3;

    return &r;
}


void _handleButtonMove (button * b, int x, int y)
{
    if (b->handler)
    {
        b->handler (b, x - b->x1, y - b->y1);
    }
}

void enableDrPanels(int ena)
{
    enablePanel (actions, ena);
    enablePanel (stencils, ena);
    enablePanel (drmodes, ena);
}


void togglePanelGid (void)
{
    int gid = getCurrentPanelGid() + 1;
    
    if (gid > 2)
        gid = 1;
    
    setCurrentPanelGid (gid);
}
