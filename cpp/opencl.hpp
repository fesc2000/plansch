
#include <CL/cl_platform.h>
#include <CL/cl.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <SDL/SDL_syswm.h>
#include <GL/glx.h>

#include <vector>

using namespace std;


#define TO_ALL  3
#define TO_GPU  1
#define TO_HOST 2

class ocl_world;
class ocl_particle_set;
class ocl_buffer;
class ocl_kernel_invoke_inst;


typedef enum {
    undefd,
    ocl_arg_buffer,	    /*! ocl_buffer class (working) */
    ocl_arg_buffer_alt,	    /*! ocl_buffer class (shadow) */
    ocl_arg_buffer_pingpong, /*! ocl_buffer class (working+shadow, swap after use) */
    ocl_arg_int,	    /*! integer argument */
    ocl_arg_float,	    /*! float argument */
    ocl_arg_int_ref,	    /*! integer reference argument */  
    ocl_arg_float_ref	    /*! float reference argument */
} ocl_arg_type;

typedef struct {
    cl_kernel_arg_address_qualifier address;
    cl_kernel_arg_access_qualifier  access;
    char			    typeName[100];
    cl_kernel_arg_type_qualifier    type;
    char			    name[100];
} ocl_arg_info;

typedef struct {
    ocl_arg_type	argType;

    union
    {
	ocl_buffer	*buffer;
	int		 iarg;
	int		*pIarg;
	float		 farg;
	float		*pFarg;
    } u;
} ocl_arg_val;

/*@ An OpenCL kernel
 */
class ocl_kernel
{
    private:
	/*! Source file of kernel
	 */
	char			*srcFile;

	/*! Name of kernel
	 */
	char		        *name;

	/*! Number of arguments
	 */
	vector<ocl_arg_info>	argInfo;
	cl_kernel		kernel;
	int			maxWg;

    public:
	ocl_kernel (cl_program program, const char *name);
	int compile(void);
	int argCount(void)
	{
	    return argInfo.size();
	}
	char *argName (int index)
	{
	    return argInfo.at(index).name;
	}
	char *argTypeName (int index)
	{
	    return argInfo.at(index).typeName; 
	}

    friend ocl_kernel_invoke_inst;
};

/*@ A specific set of parameters (dimension, arguments) bound to a kernel
 */
class ocl_kernel_invoke_inst 
{
    private:
	vector<ocl_arg_val> args;
	int	      dimensions;
	size_t	      globalThreads[3];
	ocl_kernel   *kernel;
	cl_int	      last_status;

    public:
	template <typename T> int addArg (ocl_arg_type argtype, T arg);
	int execute (void);
	ocl_kernel_invoke_inst(ocl_kernel *k) { kernel = k; };

};


class ocl_buffer
{
    private:
	int	     numBuffers;
	cl_mem	     buffer[2];
	size_t	     size;
	char	    *name;
	cl_event     events[2];
	int	     pending[2];
	GLuint	     vbo;
	size_t	     elements;
	size_t	     elemSize;
	int	     currentIndex;
	void	    *hostBuffer;

    public:
	ocl_buffer (void);
	ocl_buffer (ocl_arg_type type, int dimensions, int size[3]);

	/*! Clear the whole buffer */
	int clear (void);

	/*! Fill with char constant */
	int fill (char data);

	/*! Fill with integer constant */
	int fill (int data);

	/*! Fill with float constant */
	int fill (float data);

	/*! host data to GPU */
	int toGpu (int async = 0);

	/*! GPU data to host */
	int toHost (int async = 0);

	/*! Get active CL mem handle */
	cl_mem active (void);

	/*! Get shadow CL mem handle (=active if single buffer) */
	cl_mem shadow (void);

	/*! switch active/shadow */
	void switchBuffer(void);

	/*! finish a buffer transfer (bit mask of TO_GPU/TO_HOST, TO_ALL is both) */
	int finish (int flags = TO_ALL);

	/*! Get VBO shared with OpenGL (for exclusing OpenGL use) */
	GLuint	vboGet (void);

	/*! Release VBO (for exclusive OpenCL use) */
	void	vboRelease (void);
};

class particle_engine
{


};

class ocl_field
{
    private:
	ocl_buffer               field;
	ocl_buffer               dens;
	ocl_buffer               fixDens;
	ocl_buffer               diff;
	ocl_buffer               diffHost;
	ocl_buffer               gravBuffer;
	ocl_buffer               grav;
	ocl_buffer               particleDens;
	int                      fixDensBufferUpdate = 0;

	ocl_world		*world = NULL;

    public:
	ocl_field (int wid, int hei);

};

class ocl_device
{
    private:
	cl_context		  context;
	cl_command_queue          commandQueue;

    public:
	int initDevice (GLXContext sdl_gl_context, Display *sdl_display);
	int initKernels (char **sources, void *kernels);
	cl_context		  cx(void) { return context; }
	cl_command_queue	  cq(void) { return commandQueue; }
};

class ocl_particle_set
{
    private:
	/*! GPU buffers for position, vector and attributes (ping-pong)
	 */
	ocl_buffer   pos, vec, attr, col, baseCol;

	/* Trail and link lines
	 */
	ocl_buffer   trailLines, linkLines, linkLineColors;

	/* Mouse relative positions of grabbed particles
	 */
	ocl_buffer   grabPos;

	/* holds GPU connection attributes
	 */
	ocl_buffer   connections;

	/* current search operation 
	 */
	ocl_buffer   searchOp;

	/* ID remap when compressing arrays
	 */
	ocl_buffer   idRemap;

	/* GPU buffers required for collission handling
	 */
	ocl_buffer   SortedOff, SortedIdx, cellStart, cellEnd;

	/* Blob structures
	 */
	ocl_buffer   blobs, blobParticles;

	ocl_buffer   freelist;

	ocl_buffer   createJobs;

	ocl_buffer   statusBuffer;

	ocl_field   *field = NULL;
	ocl_world   *world = NULL;

    public:
	ocl_particle_set (class ocl_world *world, class ocl_field *field, int maxParticles, int coll=1);
};

class ocl_world
{
    private:
	ocl_buffer               wallTempr;

	ocl_buffer		 jobBuffer;
	ocl_buffer		 walls;
	ocl_buffer               wallFlags;

	ocl_buffer               scrFlags;

	ocl_buffer               accel;

	ocl_buffer		 params;

	int			 wid, hei, wh;

	ocl_device		*device = NULL;
	ocl_field		 field;
	ocl_particle_set	 collParticles;

    public:
	ocl_world (ocl_device *device, int wid, int hei, void *dens, void *fixDens, void *dx, void *dy, GLuint stex);
	cl_context		 cx(void) { return device->cx(); }
	cl_command_queue	 cq(void) { return device->cq(); }


};


