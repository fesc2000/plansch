


#include "opencl.hpp"

template <typename T> int ocl_kernel_invoke_inst::addArg (ocl_arg_type argType, T arg)
{
    int index = args.size();
    int maxIndex = kernel->argInfo.size();
    ocl_arg_val	argVal;

    if (index >= maxIndex)
	return -1;

    switch (argType)
    {
	case ocl_arg_buffer:
	case ocl_arg_buffer_alt:
	case ocl_arg_buffer_pingpong:
	    argVal.u.buffer = arg;
	    break;
	case ocl_arg_int:
	    argVal.u.iarg = arg;
	    break;
	case ocl_arg_float:
	    argVal.u.farg = arg;
	    break;
	case ocl_arg_int_ref:
	    argVal.u.pIarg = arg;
	    break;
	case ocl_arg_float_ref:
	    argVal.u.pFarg = arg;
	    break;
    }

    argVal.argType = argType;

    args.push_back (argVal);



    return 0;
}

void makeSampleKernel()
{
    cl_program program = 0;
    int wh[3] = { 1024, 768, 0 };
    int arg1 = 2;
    ocl_buffer buffer1 (ocl_arg_buffer_pingpong, 2, wh);


    ocl_kernel kernel (program, "test");
    ocl_kernel_invoke_inst kexec1 (&kernel);
    kexec1.addArg (ocl_arg_buffer_pingpong, &buffer1);
    kexec1.addArg (ocl_arg_int, arg1);
    kexec1.addArg (ocl_arg_int_ref, &arg1);

    ocl_kernel_invoke_inst kexec2 (&kernel);


}
