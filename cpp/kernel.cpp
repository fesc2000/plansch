

int ocl_kernel::compile(void)
{
    return 0;
}

int ocl_kernel::addArgType (ocl_arg_type argType)
{
    argTypes.push_back (argType);

    return 0;
}


int ocl_kernel_invoke_inst::addArg (ocl_buffer &buffer)
{
    int index = args.size;
    int maxIndex = kernel.argTypes.size;

    if (index >= maxIndex)
	return -1;

    switch (kernel.argTypes[index])
    {
	case ocl_arg_buffer:
	case ocl_arg_buffer_alt:
	case ocl_arg_buffer_pingpong:
	    break;
	default:
	    return 1;
    }

    args.push_back (new ocl_kernel_arg (kernel.argTypes &buffer, kernel.argTypes[index]));
}

int ocl_kernel_invoke_inst::addArg (int arg)
{
    int index = args.size;

    if (kernel.argTypes[index] != ocl_arg_int)
	return 1;

    args.push_back (new ocl_kernel_arg (arg));
}



int ocl_kernel_invoke_inst::execute (void)
{
    int i;

    for (i = 0; i < kernel.args.size; i++)
    {
	switch (kernel.argTypes[i])
	{
	    case ocl_arg_buffer:
	    case ocl_arg_buffer_alt:
	    case ocl_arg_buffer_pingpong:
	    case ocl_arg_int:
	    case ocl_arg_float:
	}
    }
}
