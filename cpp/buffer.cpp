
#include "opencl.hpp"


/*! Clear the whole buffer */
int ocl_buffer::clear (void)
{
    return 0;
}

/*! Fill with char constant */
int ocl_buffer::fill (char data)
{
    return 0;
}

/*! Fill with integer constant */
int ocl_buffer::fill (int data)
{
    return 0;
}

/*! Fill with float constant */
int ocl_buffer::fill (float data)
{
    return 0;
}

/*! host data to GPU */
int ocl_buffer::toGpu (int async = 0)
{
    return 0;
}

/*! GPU data to host */
int ocl_buffer::toHost (int async = 0)
{
    return 0;
}

/*! Get active CL mem handle */
cl_mem ocl_buffer::active (void)
{
    return 0;
}

/*! Get shadow CL mem handle (=active if single buffer) */
cl_mem ocl_buffer::shadow (void)
{
    return 0;
}

/*! switch active/shadow */
void ocl_buffer::switchBuffer(void)
{
    return 0;
}

/*! finish a buffer transfer (bit mask of TO_GPU/TO_HOST, TO_ALL is both) */
int ocl_buffer::finish (int flags = TO_ALL)
{
    return 0;
}

/*! Get VBO shared with OpenGL (for exclusing OpenGL use) */
GLuint	ocl_buffer::vboGet (void)
{
    return 0;
}

/*! Release VBO (for exclusive OpenCL use) */
void	ocl_buffer::vboRelease (void)
{
    return 0;
}
