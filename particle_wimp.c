/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"


SETTING(wimpAttraction) =
{
    .minValue = 1,
    .maxValue = GRID/30,
    .minDiff = 1,
    .defaultValue = (GRID/30)/2,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Wimp Attraction",
    .cb = (FUNCPTR)NULL,
};

SETTING(wimpMaxDist) =
{
    .minValue = 1,
    .maxValue = 500,
    .minDiff = 1,
    .defaultValue = 500,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Maximum part. dist.",
    .cb = (FUNCPTR)NULL,
};

SETTING(wimpChain) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect Wimps to real particles",
};

SETTING(numWimpLinks) =
{
    .minValue = 1,
    .maxValue = MAX_LINKS,
    .defaultValue = 6,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .discrete = 1,
    .numDiscrete = 6,
    .valueArray = { 1, 2, 3, 4, 5, 6 },
    .name = "NumberOfLinksWimp"
};


extern int ldToggle;
static const float _ps_red[4] __attribute__((aligned(16))) = { 1.0, 0, 0, 0 };

static int redraw = 0;
static int myType;
static int wimpSavedCharge, wimpSavedWeight;


#define CHAIN

#define MAX_DIST(data)                (int)((data) & 0xffff)
#define SET_MAX_DIST(data,d)        data = ((data) & 0xffff0000) | ((d) & 0xffff)
#define CHAIN_ATTRACT(data)        (int)((data) >> 16)
#define SET_CHAIN_ATTRACT(data,d)        data = (data & 0x0000ffff) | ((d) << 16)

#define CURRENT_MAX_DIST  ((getValue(&wimpMaxDist) == wimpMaxDist.maxValue) ? 0 : getValue(&wimpMaxDist))

#define MK_LINK_DATA(maxdist,attract)        (((maxdist) & 0xffff) | ((attract) << 16))
#define DFL_LINK_DATA        MK_LINK_DATA(CURRENT_MAX_DIST, getValue(&wimpAttraction))


static char *pLinkCount;

screeninfo *
moveWimp (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *pa2 )
{
    vec_t screenPos;
    vec_t screenPosOld;
    vec_t screenPosDiff;
    vec_t newPos;
    vec_t normVec;
    screeninfo *newSi;
    FLOAT vec_len;
    vec_t tvec;
#if 1
    QUAD_TYPE spq;
#endif

    int dir;

    td->v.partCount++;

    PART_GRAV(p,oldSi);
    V_TO_SCREEN(screenPosOld, p->pos);

#ifdef CHAIN
    if (p->pPtr)
    {   
        int i;
        pLinks *pl = (pLinks*)p->pPtr;
        particle *pp;
        vec_t ttvec;

        for (i = 0; i < pl->num; i++)
        {
            uint32 linkData = pl->linkData[i];
            int maxDist = MAX_DIST(linkData);

            pp = pl->links[i];

            if (isDeleted(pp))
            {
                pLinkCount[PARTICLE_ID(pp)] = 0;
                p->pPtr = NULL;
            }


            ttvec.d.x = (pp->pos.d.x - p->pos.d.x);
            ttvec.d.y = (pp->pos.d.y - p->pos.d.y);

            spq = (int64_t)ttvec.d.x * (int64_t)ttvec.d.x + (int64_t)ttvec.d.y * (int64_t)ttvec.d.y;

            if (maxDist && (spq > ((int64_t)maxDist * (MAX_SPEEDQ / 10))))
            {
                /* distance too large, unlink particles
                 */
                unlinkTwo (p, pp);

                pLinkCount[PARTICLE_ID(pp)] = 0;
                p->pPtr = NULL;

#ifndef USE_OPENGL
                if (GETVAL32(showLinks) && (zoom > 2))
                {
                    mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y), IX_TO_SX(pp->pos.d.x), IY_TO_SY(pp->pos.d.y), 0x00ff0000);
                }
#else
                if (GETVAL32(showLinks) && redraw)
                {
                    /* temporary color particles red */
                    p->color.cv = *(v4sf*)_ps_red;
                    pp->color.cv = *(v4sf*)_ps_red;

                    ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv);
                }
#endif

            }
            else
            {

#if 0
                if (spq < MAX_SPEEDQ * 2)
                {
                        ttvec.d.x = -ttvec.d.x;
                        ttvec.d.y = -ttvec.d.y;
                }
#endif

                /* add the distance of the two particles to the speed vector.
                 */
                ttvec.d.x /= CHAIN_ATTRACT(linkData);
                ttvec.d.y /= CHAIN_ATTRACT(linkData);

                p->vec.d.x = p->vec.d.x + ttvec.d.x;
                p->vec.d.y = p->vec.d.y + ttvec.d.y;

                pdescs[PTYPE(pp)]->hit (pp, (int)-ttvec.d.x, (int)-ttvec.d.y);

#ifndef USE_OPENGL
                if (GETVAL32(showLinks) && ((zoom > 2) || (spq >= MAX_SPEEDQ*8)))
                {
                    mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y),
                                         (IX_TO_SX(pp->pos.d.x) + IX_TO_SX(p->pos.d.x)) / 2, 
                                         (IY_TO_SY(pp->pos.d.y) +
                                         IY_TO_SY(p->pos.d.y)) / 2,
                                         ICOLOR(p).col32);
                }
#else
                if (GETVAL32(showLinks) && redraw)
                {
                    ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv);
                }
#endif
            }
        }

#if 0
        if (oldSi->flags & SI_LINE)
        {
            lhit = pHitLine (p, &p->vec, off);
        }
        else
        {
            lhit = 0;
        }


        if (IS_STICKED(p))
        {
            int d;

            /* particle is sticked to wall. Dont move it
             * XXX transfer speed to wall temperature ...
             */
            p->vec.d.x = 0;
            p->vec.d.y = 0;

            /* check if there is still an adjacent wall. If not, unstick it
            */
            for (d = 0; d < 8; d++)
            {
                if (((SI_DIR(oldSi, d)->particleList == SCR_BRICK) ||
                    (SI_DIR(oldSi, d)->flags & SI_LINE)))
                    break;
            }

            if (d >= 8)
            {
                CLEAR_STICKED(p);
            }

            tvec = p->vec;

        }
#endif
    }
#endif /* CHAIN */

    if (oldSi->flags & SI_LINE)
    {
        pHitLine (p, &p->vec, off, td, oldSi);
    }


    tvec = p->vec;

#if 1
    spq = (QUAD_TYPE)tvec.d.x * (QUAD_TYPE)tvec.d.x + (QUAD_TYPE)tvec.d.y * (QUAD_TYPE)tvec.d.y;
    if (spq >= MAX_SPEEDQ)
    {
        vec_len = sqrt(spq);
        tvec.d.x = (((FLOAT)p->vec.d.x * MAX_SPEEDF)) / vec_len;
        tvec.d.y = (((FLOAT)p->vec.d.y * MAX_SPEEDF)) / vec_len;

        p->vec = tvec;
    }
#endif

    V_ADD(newPos, p->pos, tvec);
    V_TO_SCREEN(screenPos, newPos);

    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];


    if (oldSi == newSi)
    {
        /* no screen movement */
        p->pos = newPos;

        return oldSi;
    }


    if (unlikely (newSi->particleList == SCR_BRICK))
    {
        FLOAT cxf, cyf, vpf;

        V_SUB(screenPosDiff, screenPos, screenPosOld);
        V_SUB(screenPosDiff, screenPos, screenPosOld);

        dir = ((screenPosDiff.d.x + 1) * 3 + screenPosDiff.d.y + 1);
        if (dir > 3) dir--;

        td->v.wallHit++;

        td->drawLinkLine = 1;
        
        /* wall hit */

        /* get normal vector of current screen position (direction based) 
         *
         * This is fixed-point arithmetic, except vector length calculation.
         * Normals are stored in NORM_LEN units (+-127).
         *
         */
        /* normal vector in normVec */
        normVec.d.x = oldSi->normals[dir * 2];
        normVec.d.y = oldSi->normals[dir * 2 + 1];
        cxf = tvec.d.x;
        cyf = tvec.d.y;

        /* normalize */
        vec_len = VEC_LEN (cxf, cyf);

        cxf = (cxf * NORM_LEN) / vec_len;
        cyf = (cyf * NORM_LEN) / vec_len;

        if (FRICTION(newSi))
        {
            float vl;
            normVec.d.x = (normVec.d.x * (100 - FRICTION(newSi)) - cxf * FRICTION(newSi));
            normVec.d.y = (normVec.d.y * (100 - FRICTION(newSi)) - cyf * FRICTION(newSi));
            vl = VEC_LEN(normVec.d.x, normVec.d.y);
            normVec.d.x = (normVec.d.x * NORM_LEN) / vl;
            normVec.d.y = (normVec.d.y * NORM_LEN) / vl;
        }

        /* reflect at normal vector */
        vpf = ((cxf * normVec.d.x) + (cyf * normVec.d.y));
        cxf = cxf - (2 * normVec.d.x * vpf) / (NORM_LENQ);
        cyf = cyf - (2 * normVec.d.y * vpf) / (NORM_LENQ);

        /* handle energy transfer between particle and wall.  */

        if (GETVAL32(wallTempr))
        {
            int t = (FLOAT)((vec_len - TEMPR(newSi))/2) * wallDamping;

            TEMPR(newSi) += t;

            if (t > 0.0)
            {
                vec_len -= t;

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));
            }
            else
            {
                vec_t tvec;

                frandomize (cxf, cyf, -t, &tvec);

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

                p->vec.d.x += tvec.d.x;
                p->vec.d.y += tvec.d.y;

            }
        }
        else
        {
            /* apply original impuls to reflection vector */

            p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf * wallDamping) / (FLOAT)(NORM_LEN));
            p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf * wallDamping) / (FLOAT)(NORM_LEN));
        }

        /* move away only if it seems necessary */
        newPos.d.x = p->pos.d.x + p->vec.d.x;
        newPos.d.y = p->pos.d.y + p->vec.d.y;
        V_TO_SCREEN(screenPos, newPos);
        if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
        {
            newPos.d.x += (normVec.d.x * GRID) / (NORM_LEN);
            newPos.d.y += (normVec.d.y * GRID) / (NORM_LEN);
            V_TO_SCREEN(screenPos, newPos);

            if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
            {
                newPos = p->pos;
                V_TO_SCREEN(screenPos, newPos);
            }
        }

        newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];
    }
//    else if (GETVAL32(wimpChain) && (p->pPtr == NULL) && (newSi->particleList > SCR_BRICK))
    else if (GETVAL32(wimpChain) && (newSi->particleList > SCR_BRICK))
    {
        particle *pp;
        for (pp = newSi->particleList; pp >= SCR_BRICK; pp = pp->next)
        {
            int pid = PARTICLE_ID(pp);

            if ((pLinkCount[pid] < 6) && !isLinked (p, pp))
            {
                p->pPtr = (void*)linkPartners (p);

                linkParticlesWeak (p, pp, DFL_LINK_DATA);
                pLinkCount[pid]++;
                break;
            }
        }
    }

    oldSi->wimps--;

    if ((oldSi->wimps <= 0) && (oldSi->particleList == NULL))
    {
        SI_CHARGE(oldSi) = PCHARGE(p);
        oldSi->wimps = 0;
    }

    if (((uint)screenPos.d.x >= wid) || ((uint)screenPos.d.y >= hei))
    {
        td->drawLinkLine = 1;
        return NULL;
    }

    newSi->wimps++;

    p->pos = newPos;

    return newSi;
}

static int wimpSetType(particle *p)
{
    p->pPtr = NULL;

    return 1;
}

static int wimpUnsetType(particle *p)
{
    if (p->pPtr == NULL)
        return 1;

    unlinkAll (p);

    return 1;
}

static void handleRemove (particle *p, void *arg)
{
    pLinkCount[PARTICLE_ID(p)] = 0;
}

static void handleCreate (particle *p, void *arg)
{
    particle *pp;
    int pt = PTYPE(p);

    if (!getValue(&wimpPair[pt]))
        return;

    if (pdescs[pt]->isWimp)
        return;

    pp = newParticle2 (p->pos.d.x, p->pos.d.y, p->vec.d.x, p->vec.d.y, myType, wimpSavedCharge, wimpSavedWeight, p->size, getColorSetting (myType, wimpSavedCharge));

    if (pp)
    {
        pp->pPtr = (void*)linkPartners (pp);

        linkParticlesWeak (pp, p, DFL_LINK_DATA);
        pLinkCount[PARTICLE_ID(p)]++;
    }

}

static void handleSetting (setting *s, void *arg)
{
    /* capture particle weight/charge when it changes and when 
     * our ptype is selected
     */
    if (getValue (&ptype) != myType)
        return;

    if (s == &particleWeight[myType])
    {
        wimpSavedWeight = getValue (s);
    }
    else if (s == &particleAttractionForce[myType])
    {
        wimpSavedCharge = getValue (s);
    }
    else if (s == &wimpChain)
    {
        if (getValue (&wimpChain))
        {
            wimpSavedWeight = getValue (&particleWeight[myType]);
            wimpSavedCharge = getValue (&particleAttractionForce[myType]);
        }
    }
}

static void wimpInit (int ptype)
{
    myType = ptype;
    value_u col;

    pLinkCount = (char*)malloc(options->maxParticles);

    memset (pLinkCount, 0, options->maxParticles);

    registerRemoveCallback ((FUNCPTR)handleRemove, NULL);

    registerCreateCallback ((FUNCPTR)handleCreate, NULL);

    registerSettingCallback ((FUNCPTR)handleSetting, NULL);

    wimpSavedWeight = getValue (&particleWeight[myType]);
    wimpSavedCharge = getValue (&particleAttractionForce[myType]);

    encodeHSLA (&col.cv, cvToHue(CV_RED), 1.f, 0.8f, 1.0f);
    modifySetting (pdescs[myType]->crangeColors[0], col.va64, 0);

    encodeHSLA (&col.cv, cvToHue(CV_GREEN), 1.f, 0.8f, 1.0f);
    modifySetting (pdescs[myType]->crangeColors[1], col.va64, 0);

    encodeHSLA (&col.cv, cvToHue(CV_BLUE), 1.f, 0.8f, 1.0f);
    modifySetting (pdescs[myType]->crangeColors[2], col.va64, 0);
}

static void wimpApplySetting (particle *p, setting *s)
{
    pLinks *pl;
    int i;
    int val = getValue (s);
    pl = p->pPtr;

    if (pl == NULL)
        return;

    if (s == &wimpAttraction)
    {
        for (i = 0; i < pl->num; i++)
        {
            SET_CHAIN_ATTRACT(pl->linkData[i], val);
        }
    }
    else if (s == &wimpMaxDist) 
    {
        for (i = 0; i < pl->num; i++)
        {
            SET_MAX_DIST(pl->linkData[i], CURRENT_MAX_DIST);
        }
    }
    else if (s == &numWimpLinks)
    {
        linkSetCount (p, getValue(s));
    }
}

static screeninfo *wimpRedraw (particle *p, screeninfo *oldSi, threadData *td, int off )
{
    pLinks *pl = (pLinks*)p->pPtr;
    particle *pp;
    int i;

    td->v.partCount++;

    if (!GETVAL32(showLinks) || !redraw || pl == NULL)
        return oldSi;

    for (i = 0; i < pl->num; i++)
    {
        pp = pl->links[i];

        ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv);
    }

    return oldSi;
}

static void wimpIter(int scrUpdate)
{
    redraw = scrUpdate;
}

static void wimpLink (particle *p, particle *pp)
{
    pLinks *pl = (pLinks*)p->pPtr;
    particle *current;

    if (pl)
    {
        current = pl->links[0];
        pLinkCount[PARTICLE_ID(current)] = 0;

        unlinkTwo (p, current);
    }
    else
    {
        p->pPtr = linkPartners (p);

        linkParticlesWeak (p, pp, DFL_LINK_DATA);
        pLinkCount[PARTICLE_ID(pp)]++;
    }
}

static int wimpNumLinks(void)
{
    return getValue(&numWimpLinks);
}

pdesc MOD = {
    .moveParticle = moveWimp,
    .init = (FUNCPTR)wimpInit,
    .setType = (FUNCPTR)wimpSetType,
    .unsetType = (FUNCPTR)wimpUnsetType,
    .name = "Wimp",
    .applySetting = (FUNCPTR)wimpApplySetting,
    .iteration = (FUNCPTR)wimpIter,
    .iterationFrozen = (FUNCPTR)wimpIter,
    .redraw = (FUNCPTR)wimpRedraw,
    .link = (FUNCPTR)wimpLink,
    .numLinks = (FUNCPTR)wimpNumLinks,
    .descr = "",
    .pm = wimp_pm,
    .help = "WIMP",
    .isWimp = 1,
    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},
            {(FUNCPTR)onOffButtonS, 32, 32, connect_wimp_xpm, "Connect", 0, &wimpChain, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, chain_attract2_pm , "Chain Attraction", 0, &wimpAttraction, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, chain_maxdist_pm, "Max. Distance", 0, &wimpMaxDist, B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 22, 21, l1_xpm, "1 Link", 1, &numLinks, B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 22, 21, l3_xpm, "3 Links", 3, &numLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l5_xpm, "5 Links", 5, &numLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l2_xpm, "2 Links", 2, &numLinks, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l4_xpm, "4 Links", 4, &numLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l6_xpm, "6 Links", 6, &numLinks, B_NEXT|B_SPC(1) },
}
};
