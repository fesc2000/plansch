/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define T_PIXEL        1
#define T_PARTICLE 2

typedef enum
{
        M_WARP,
        M_ACCEL
} moveMode_t;

typedef struct 
{
    int type;
    void *oPtr;
    vec_t off;
    int old;
} moveRecord;

static int maxMoveElements;
static int numMoveElements = 0;
static moveRecord *records;
static vec_t movePos;

static int moving = 0;
static int absMove = 0;
static void *showCallout;

void moveInit()
{
    static int initialized = 0;

    if (initialized)
        return;

    /* in the worst case we capture all particles and all pixels
     */
    maxMoveElements = options->maxParticles + wid*hei; 

    records = malloc(maxMoveElements * sizeof(*records));

    numMoveElements = 0;

    showCallout = calloutNew();
    initialized = 1;
}

/* initialize movement (when botton is pressed):
 * Clear move list, initialize current move pointer, set the move mode
 * (immedeate or accellerate if shift is pressed).
 */
void moveStart(int x, int y)
{
    numMoveElements = 0;

    movePos.d.x = x;
    movePos.d.y = y;
    
    moving = 1;
    absMove = 0;

#ifdef USE_OPENCL
    clMarkCursorPixels (x, y, 1);
    clSchedStartGrab (x, y);
#endif
}

void moveEnd ()
{
    moving = 0;
#ifndef USE_OPENGL
    currentStencilToCursor();
#endif
#ifdef USE_OPENCL
    clSchedStopGrab();
#endif
}

/* add object (particle or screen pixel) to move list
 */
int addMoveElement (int type, void *object)
{
    int x, y;
    screeninfo *pSi;
    particle *p;
    int i;
    
    for (i = 0; i < numMoveElements; i++)
        if (records[i].oPtr == object)
        {
            records[i].old = 0;
            return 1;
        }
    
    
    if (numMoveElements >= maxMoveElements)
        return 0;

    switch (type)
    {
#if 1
        case T_PIXEL:
            pSi = (screeninfo*)object;
            x = INTERNC(SI2X(pSi));
            y = INTERNC(SI2Y(pSi));
            break;
#endif

        case T_PARTICLE:
            p = (particle*)object;
            x = p->pos.d.x;
            y = p->pos.d.y;
            break;

        default:
            return 2;
    }

    /* remember relative position */
    records[numMoveElements].type = type;
    records[numMoveElements].oPtr = object;
    records[numMoveElements].off.d.x = x - movePos.d.x;
    records[numMoveElements].off.d.y = y - movePos.d.y;
    records[numMoveElements].old = 0;
    
    numMoveElements++;

#ifndef USE_OPENGL
    if (numMoveElements == 1)
        setCursor ((void*)pixel_cur_xpm);
#endif

    return 1;
}

/* set all captured particle's speed to 0 */
void freezeMoveElements()
{
    int i;
    particle *p;

    for (i = 0; i < numMoveElements; i++)
    {
        if (records[i].type == T_PARTICLE)
        {
            p = (particle*)records[i].oPtr;
            p->vec.d.x = p->vec.d.y = 0;
        }
    }
}

/* relocate/accellerate all captured elements to new position */
void relocateMoveElements(int loopDiff)
{
    int i;
    particle *p;
    screeninfo *oldSi, *newSi;
    int nx, ny, sx, sy, vx, vy;
    int warp;

    if (loopDiff == 0)
        loopDiff = 1;

    warp = !shiftPressed();

    for (i = 0; i < numMoveElements; i++)
    {
        if (records[i].type == T_PARTICLE)
        {
            p = (particle*)records[i].oPtr;

            oldSi = GET_SCREENINFO(p);

            if (warp)
            {
                nx = MIN(INTERNC(wid), movePos.d.x + records[i].off.d.x);
                ny = MIN(INTERNC(hei), movePos.d.y + records[i].off.d.y);

                vx = vy = 0;
            }
            else
            {
                int dx = movePos.d.x + records[i].off.d.x - p->pos.d.x;
                int dy = movePos.d.y + records[i].off.d.y - p->pos.d.y;
                float dl = VEC_LEN(dx, dy);

                if (dl > GRID * 1)
                {

                    nx = p->pos.d.x + (dx / (dl / GRID));
                    ny = p->pos.d.y + (dy / (dl / GRID));

                    /* loopDiff is the number of iterations since the last movement.
                     * If it's large (slow mouse movement) the impuls created by the particle is 
                     * small
                     */
                    dl *= loopDiff*2;

                    vx = (dx / (dl / GRID));
                    vy = (dy / (dl / GRID));
                }
                else
                {
                    nx = movePos.d.x + records[i].off.d.x;
                    ny = movePos.d.y + records[i].off.d.y;

                    vx = vy = 0;
                }
            }

            if (nx < 0)
                continue;
            else if (SCREENC(nx) >= wid)
                continue;

            if (ny < 0)
                continue;
            else if (SCREENC(ny) >= hei)
                continue;

            sx = SCREENC(nx);
            sy = SCREENC(ny);

            newSi = &siArray[sx + sy*wid];

            if (oldSi == newSi)
            {
                p->pos.d.x = nx;
                p->pos.d.y = ny;
            }
            else if (newSi->particleList != SCR_BRICK) 
            {
                if (warpParticle (oldSi, newSi, p))
                {
                    p->pos.d.x = nx;
                    p->pos.d.y = ny;

                    if (newSi != GET_SCREENINFO(p))
                        printf ("XXX %d %d\n", nx, ny);
                }
            }

            if (vx || vy)
            {
                ACCEL_TO(p, vx, vy);
            }
            else
            {
                ACCEL_TO(p, 0,0);
            }
        }
    }
}

static int markCb (particle *p, void *arg)
{
    addMoveElement (T_PARTICLE, (void*)p);
    return 0;
}

void markCandidates(int ix, int iy)
{
    walkStencilParticles (ix, iy, (FUNCPTR)markCb, NULL);
}    

void showUnderStencil (void *show)
{
    int i;

    if (moving || (getDrawFct() != DO_MOVE) || particlesMarked())
        return;

    if (show)
    {
        for (i = 0; i < numMoveElements; i++)
            records[i].old = 1;

        markCandidates (iPositionLast.d.x, iPositionLast.d.y);
        
        for (i = 0; i < numMoveElements; i++)
        {
            if (records[i].old)
            {
#if 0
                RESTORE_PARTICLE_COLOR ((particle*)records[i].oPtr);
#endif
                records[i] = records[numMoveElements-1];
                i--;
                numMoveElements--;
            }
            else
            {
#ifdef USE_OPENGL
                highlightParticle ((particle*)records[i].oPtr);
#else
                SET_PARTICLE_COLOR_IMM ((particle*)records[i].oPtr, ARGB(0xffffffff));
#endif
            }
        }
        calloutSched (showCallout, 100, (FUNCPTR)showUnderStencil, show);
    }
    else
    {
#ifndef USE_OPENGL_XX
        for (i = 0; i < numMoveElements; i++)
        {
            RESTORE_PARTICLE_COLOR ((particle*)records[i].oPtr);
        }
#endif
        numMoveElements = 0;
    }
}

void handleMove (int x, int y)
{
    static unsigned int lastIter = 0;
    unsigned int loopDiff;

    if (!moving)
    {
        showUnderStencil ((void*)1);
        return;
    }
    
    if ((x == movePos.d.x) && (y == movePos.d.y))
    {
        loopDiff = 100000;
    }
    else
    {
        loopDiff = systemTime() - lastIter;
        lastIter = systemTime();
    }

    if (x < 0)
        x = 0;
    else if (x >= INTERNC(wid))
        x = INTERNC(wid)-1;

    if (y < 0)
        y = 0;
    else if (y >= INTERNC(hei))
        y = INTERNC(hei)-1;

    movePos.d.x = x;
    movePos.d.y = y;
    
    relocateMoveElements (loopDiff);

}

void moveAddCb (particle *p, void *arg)
{
    addMoveElement (T_PARTICLE, p);
}

void
drawFctMove (int ix, int iy, int act, int mode)
{
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);

    CHECK_DRAWING

    if (mouseButton != 1)
        return;

    switch (event)
    {
        case BUTTON_PRESS:
            showUnderStencil (0);
            
            moveStart (ix, iy);

            if (particlesMarked())
            {
                walkMarkedParticles ((FUNCPTR)moveAddCb, NULL ); 
            }
            else
            {
                markCandidates (ix, iy);
            }
            
            break;

        case BUTTON_RELEASE:
            if (currentDrawMode() == DR_LINE)
            {
                /* Warp particles to end of line */
                absMove = 1;
                handleMove (ix, iy);
            }
            moveEnd();
            break;
    }
}

void removeElementFromMoveList (void *e)
{
    int i;

    if (!moving)
        return;

    for (i = 0; i < numMoveElements; i++)
    {
        if (records[i].oPtr == e)
        {
            records[i] = records[numMoveElements-1];
            numMoveElements--;
            return;
        }
    }
}



