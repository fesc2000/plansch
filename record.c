/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include <unistd.h>

#ifdef USE_OPENCL
#include "opencl.h"
#endif

#include <fcntl.h>
#include "SDL/SDL_rwops.h"

#define RECORD_REALLOC        100000
#define MAX_RECORD        10000000

#define TAPE_FILE_HEADER    "pl_TAPE_V1"

#define DECR_OPEN        -1
#define DECR_MAX        -2
#define LID_FIRST        -2
#define ID_NOT_FOUND        -10

static int tapeActive = 0;

int drawableRecordType (int type)
{
    if ((type == R_BRICK) || (type == R_ACCEL) || (type == R_PART) || (type == R_POLYBRICK) || (type == R_POLYPOINT))
        return 1;

    return 0;
}

int tapeOperation()
{
    return tapeActive;
}

void updateTapeRect (tape *t, int x, int y, int first)
{
    if (t->tapePos == 0)
    {
        t->rect.x = x;
        t->rect.y = y;
        t->rect.w = 1;
        t->rect.h = 1;
    }
    else
    {
        if (x < t->rect.x)
        {
            t->rect.w += t->rect.x - x;
            t->rect.x = x;
        }
        else if (x > (t->rect.x + t->rect.w))
        {
            t->rect.w += x - (t->rect.x + t->rect.w);
        }

        if (y < t->rect.y)
        {
            t->rect.h += t->rect.y - y;
            t->rect.y = y;
        }
        else if (y > (t->rect.y + t->rect.h))
        {
            t->rect.h += y - (t->rect.y + t->rect.h);
        }
    }
}

void addRecord (tape *t, record *r, recordInfo *i)
{
    if (!t->recording)
        return;

    if (t->buffer == NULL)
    {
        t->buffer = malloc(sizeof(record) * RECORD_REALLOC);

        if (t->buffer == NULL)
        {
            printf ("Failed to malloc tape\n");
            return;
        }
        t->tapeLen = RECORD_REALLOC;
        t->tapePos = 0;
    }

    if (t->info == NULL)
    {
        t->info = malloc(sizeof(recordInfo) * t->tapeLen);

        if (t->info == NULL)
        {
            printf ("Failed to malloc tape info\n");

            free (t->buffer);
            t->buffer = NULL;
            return;
        }
        memset ((void*)t->info, 0, sizeof(recordInfo) * RECORD_REALLOC);
    }

    if (t->tapePos >= t->tapeLen)
    {

        if (t->tapeLen >= MAX_RECORD)
        {
            printf ("Tape too long\n");
            return;
        }

        t->tapeLen += RECORD_REALLOC;
        t->buffer = realloc (t->buffer, t->tapeLen * sizeof(record));

        if (t->buffer == NULL)
        {
            printf ("Failed to increase tape\n");
            return;
        }

        t->info = realloc (t->info, t->tapeLen * sizeof(recordInfo));

        if (t->info == NULL)
        {
            printf ("Failed to increase tape\n");
            return;
        }
        memset (&t->info[t->tapePos], 0, sizeof(recordInfo) * RECORD_REALLOC);
    }

    if (t->tapePos == 0)
        t->recordStart = systemTime();

    t->buffer[t->tapePos] = *r;
    t->buffer[t->tapePos].iteration = systemTime() - t->recordStart;

    if (i)
    {
        t->info[t->tapePos] = *i;
    }
    else
    {
        memset ((char*)&t->info[t->tapePos], 0, sizeof(recordInfo));
    }

    if (drawableRecordType(r->type))
    {
        updateTapeRect (t, r->pos.d.x, r->pos.d.y, (t->drawables == 0));

        t->drawables++;
    }

    t->tapePos++;
}

void tapeStart(tape *t)
{
    t->recording = 1;
}

void tapeStop(tape *t)
{
    t->recording = 0;
}

void tapeClear(tape *t)
{
    t->tapePos = 0;
    t->drawables = 0;
    t->rect.x = 0;
    t->rect.y = 0;
    t->rect.w = 0;
    t->rect.h = 0;
}

#define TSIZE        64

SDL_Surface *tapeToThumbnail (tape *t, int rct)
{
    int minx, miny;
    float wx, wy, offx, offy;
    record *r;
    SDL_Surface *s;
    int i;
    int x,y;
    Uint32 c;
    float irct = INTERNC_M(rct);
    float aspect;

    if (t->tapePos == 0)
        return NULL;

    minx = t->rect.x;
    miny = t->rect.y;
    wx = (float)t->rect.w;
    wy = (float)t->rect.h;

    if (wx > wy)
    {
        aspect = irct / (wx * 1.2);
    }
    else
    {
        aspect = irct / (wy * 1.2);
    }

    offx = ((irct / 1.2) - wx * aspect) / 2.0;
    offy = ((irct / 1.2) - wy * aspect) / 2.0;

    s = SDL_CreateRGBSurface (SDL_SWSURFACE, rct, rct, 32, 0xff0000, 0x00ff00, 0x0000ff,0);

    if (s == NULL)
    {
        printf ("SDL_CreateRGBSurface failed: %s\n",  SDL_GetError());
        return NULL;
    }

    for (x = 0; x < rct; x++)
        for (y = 0; y < rct; y++)
            SURFACE_SET32(s,x,y,0);

    r = t->buffer;
    for (i = 0; i < t->tapePos; i++, r++)
    {
        switch (r->type)
        {
            case R_PART:
                    c =  r->u.part.color;
                break;
            case R_BRICK:
            case R_POLYBRICK:
                c = TEMPR_COL (r->u.brick.heat) * 2;
                break;
            case R_ACCEL:
                    c = COL_ACCEL_V;
                break;
            default:
                    continue;
        }

        x = SCREENC((int)(((float)r->pos.d.x - minx) * aspect + offx));
        y = SCREENC((int)(((float)r->pos.d.y - miny) * aspect + offy));

        if (((unsigned int)x >= rct) || ((unsigned int)y >= rct))
        {
            printf ("TapeToStencil: %d/%d > %d/%d!\n", x, y, rct, rct);
            continue;
        }

        SURFACE_SET32(s, x, y, rgb32ToSurfaceColor(s, c));
    }

    return s;
}

int tapeToStencil (stencil_t *st, tape *t, int bx, int by)
{
    int minx, miny;
    int wx, wy;
    record *r;
    SDL_Surface *s, *d;
    int i;
    int x,y;

    if (t->tapePos == 0)
        return 0;

    minx = t->rect.x;
    miny = t->rect.y;
    wx = SCREENC(t->rect.w);
    wy = SCREENC(t->rect.h);

    if ((wx == 0) && (wy == 0))
        return 0;

    wx += 1;
    wy += 1;

    s = SDL_CreateRGBSurface (SDL_SWSURFACE, wx, wy, 8, 0,0,0,0);

    if (s == NULL)
    {
        printf ("SDL_CreateRGBSurface failed: %s\n",  SDL_GetError());
        return 0;
    }

    d = SDL_CreateRGBSurface (SDL_SWSURFACE, wx, wy, 32, 0,0,0,0);

    if (d == NULL)
    {
        SDL_FreeSurface (s);
        printf ("SDL_CreateRGBSurface failed: %s\n",  SDL_GetError());
        return 0;
    }
//    SDL_SetPalette (s, SDL_LOGPAL, colors, 0, 255);

    for (x = 0; x < wx; x++)
        for (y = 0; y < wy; y++)
        {
            SURFACE_SET8(s,x,y,COL_EMPTY);
            SURFACE_SET32(d,x,y,0);
        }

    r = t->buffer;
    for (i = 0; i < t->tapePos; i++, r++)
    {
        x = SCREENC(r->pos.d.x - minx);
        y = SCREENC(r->pos.d.y - miny);

        if (((unsigned int)x >= wx) || ((unsigned int)y >= wy))
        {
            printf ("!%d/%d > %d/%d!\n", x, y, wx, wy);
            continue;
        }

        switch (r->type)
        {
            case R_PART:
                SURFACE_SET32(d, x, y, 0);
                break;

            case R_BRICK:
            case R_POLYBRICK:
                SURFACE_SET32(d, x, y, BRICK_DATA_ENCODE(r->u.brick.heat, r->u.brick.charge));
                break;

            case R_ACCEL:
                SURFACE_SET32(d, x, y, ACCEL_DATA_ENCODE(r->u.accel.dx, r->u.accel.dy));
                break;

            default:
                    continue;
        }
        SURFACE_SET8(s, x, y, r->type);
    }

    newStencil (st, s, d, t);

    return 1;
}


void tapeSave (tape *t, char *fname, int withThumbnail)
{
    SDL_Surface *s;
    SDL_RWops                    *rw, *f;
    char    fileName[200];

    if (withThumbnail)
    {
        s = tapeToThumbnail (t, TSIZE);

        if (!s)
            return;

        sprintf (fileName, "%s.bmp", fname);

        rw = SDL_RWFromFile (fileName, "w+");

        if (!rw)
            return;

        if (SDL_SaveBMP_RW (s, rw, 1) == -1)
        {
            printf ("SDL_SaveBMP_RW failed (%s): %s\n", fname,  SDL_GetError());
            SDL_RWclose (rw);
            SDL_FreeSurface (s);
            return;
        }

        SDL_FreeSurface (s);
    }

    /* save tape as raw data */
    sprintf (fileName, "%s.tape", fname);

    f = SDL_RWFromFile (fileName, "w+");

    if (f == NULL)
    {
        perror (fileName);
        return;
    }

    if (0 == SDL_RWwrite (f, TAPE_FILE_HEADER, sizeof(TAPE_FILE_HEADER), 1))
    {
        perror (fileName);
        SDL_RWclose (f);
        return;
    }

    if (0 == SDL_RWwrite (f, t->buffer, 1, sizeof(record) * t->tapePos))
    {
        perror (fileName);
        SDL_RWclose (f);
        return;
    }
    SDL_RWclose (f);

}

int tapeLoad (tape *t, char *fname)
{
    char header[32];
    int len;
    int i;
    SDL_RWops *rw = SDL_RWFromFile (fname, "r");

    if (rw == NULL)
    {
        perror (fname);
        return 0;
    }

    SDL_RWseek(rw, 0, SEEK_END);
    len = SDL_RWtell(rw);
    SDL_RWseek(rw, 0, SEEK_SET);

    SDL_RWread(rw, header, 1, sizeof(TAPE_FILE_HEADER));
    if ((len < sizeof(TAPE_FILE_HEADER)) || (strncmp(header, TAPE_FILE_HEADER, sizeof(TAPE_FILE_HEADER))))
    {
        printf ("bad tape file: %s\n", fname);
        free(t->buffer);
        SDL_RWclose(rw);

        return 0;
    }

    len -= sizeof(TAPE_FILE_HEADER);

    if (t->buffer)
        free (t->buffer);

    t->buffer = malloc(len);

    if (SDL_RWread(rw, t->buffer, 1, len) != len)
    {
        perror (fname);
        SDL_RWclose(rw);
        return 0;
    }
    SDL_RWclose(rw);

    if (t->info)
        free (t->info);

    t->info = malloc((len/sizeof(record) + 1) * sizeof(recordInfo));

    if ((t->buffer == NULL) || (t->info == NULL))
    {
        printf ("failed to allocate 0x%x bytes for tape load (%d entries)\n",
                (int)len, (int)(len / sizeof(record)));
        return 0;
    }

    t->tapeLen = len / sizeof(record);
    t->tapePos = len / sizeof(record);;

    t->drawables = 0;
    for (i = 0; i < t->tapeLen; i++)
    {
        if (!drawableRecordType (t->buffer[i].type))
            continue;
        
        updateTapeRect (t, t->buffer[i].pos.d.x, t->buffer[i].pos.d.y, (t->drawables == 0));

        t->drawables++;
    }

    printf ("Loaded %d(%d) entries from tape %s\n", t->tapeLen, t->drawables, fname);

    return 1;
}

/* searches for a particle p stored in the tape and returns the tape position if found,
 * ID_NOT_FOUND  (!) otherwise
 */
int particleToTapeId (tape *t, particle *p)
{
    int i;

    for (i = 0; i < t->tapePos; i++)
    {
        if (t->info[i].u.p == p)
            return i;
    }
    return ID_NOT_FOUND;
}

void removeParticleFromTape (tape *t, particle *p)
{
    int i;

    for (i = 0; i < t->tapePos; i++)
    {
        if (t->info[i].u.p == p)
        {
            t->info[i].u.p = NULL;
        }
    }
}

particle *tapeIdToParticle (tape *t, int id)
{
    if ((id < 0) || (id >= t->tapeLen))
        return NULL;

    return t->info[id].u.p;
}

void recordNewParticle (tape *t, particle *p)
{
    record         r;
    recordInfo         info;
    pLinks        *pl = linkPartners(p);
    int                 i;
    int                 pId;

    if (!t->recording)
        return;

    r.type = R_PART;
    r.pos = p->pos;
    r.pos.d.x -= INTERNC(t->offx);
    r.pos.d.y -= INTERNC(t->offy);

    r.u.part.vec = p->vec;
    r.u.part.charge = p->charge;
    r.u.part.mass = p->mass;
    r.u.part.type = p->type;
    r.u.part.size = p->size;
    r.u.part.color = getParticleColorARGB(p);

    /* store particle ID to info array. Referring to other particles inside the tape
     * is done by referring to the tape index, so we need a way to map the particle ID
     * to the tape index. This is done via the info array.
     */
    info.u.p = p; 

    addRecord (t, &r, &info);

    /* add entries for links */
    if (IS_CPU_PARTICLE(p) && pl && ((pId = particleToTapeId (t, p)) != ID_NOT_FOUND))
    {
        for (i = MAX_LINKS-1; i >= 0; i--)
        {
            r.type = R_LINK;
            r.u.link.p1 = pId;
            r.u.link.data = pl->linkData[i];

            if (i >= pl->count)
            {
                r.type = R_LINK;
                r.u.link.p2 = DECR_MAX;
            }
            else if (i >= pl->max)
            {
                r.type = R_LINK;
                r.u.link.p2 = DECR_OPEN;
            }
            else if (i < pl->num)
            {
                r.u.link.p2 = particleToTapeId (t, pl->links[i]);

                if (r.u.link.p2 == ID_NOT_FOUND)
                {
                    continue;
                }
            }
            else
            {
                continue;
            }


            addRecord (t, &r, NULL);
        }
    }
}

void recordSetting (tape *t, setting *s, int glob)
{
    record        r;

    if (!t->recording)
        return;

    r.type = R_SETTING;

    r.u.setting.hash = s->hash;
    r.u.setting.makeGlobal = glob;
    if (glob)
        r.u.setting.value = s->marked;
    else
        r.u.setting.value = getValue(s);

    addRecord (t, &r, NULL);
}

void recordBrick (tape *t, int x, int y, int charge, int heat, int friction, int mass)
{
    record        r;

    if (!t->recording)
        return;

    r.type = R_BRICK;

    r.pos.d.x = INTERNC_M(x - t->offx);
    r.pos.d.y = INTERNC_M(y - t->offy);

    r.u.brick.charge = charge;
    r.u.brick.heat = heat;
    r.u.brick.friction = friction;
    r.u.brick.mass = mass;

    addRecord (t, &r, NULL);

}

void recordPolyBrick (tape *t, int x, int y, int charge, int heat, int friction, int mass)
{
    record        r;

    if (!t->recording)
        return;

    r.type = R_POLYBRICK;

    r.pos.d.x = INTERNC_M(x - t->offx);
    r.pos.d.y = INTERNC_M(y - t->offy);

    r.u.brick.charge = charge;
    r.u.brick.heat = heat;
    r.u.brick.friction = friction;
    r.u.brick.mass = mass;

    addRecord (t, &r, NULL);

}

void recordAccel (tape *t, int x, int y, int dx, int dy)
{
    record        r;

    if (!t->recording)
        return;

    r.type = R_ACCEL;

    r.pos.d.x = INTERNC_M(x - t->offx);
    r.pos.d.y = INTERNC_M(y - t->offy);

    r.u.accel.dx = dx;
    r.u.accel.dy = dy;

    addRecord (t, &r, NULL);

}


void recordErase (tape *t, int x, int y)
{
    record r;

    if (!t->recording)
        return;


    r.type = R_EMPTY;

    r.pos.d.x = INTERNC_M(x - t->offx);
    r.pos.d.y = INTERNC_M(y - t->offy);

    addRecord (t, &r, NULL);

}

void recordRemoveParticle (tape *t, particle *p)
{
    record r;

    if (!t->recording)
        return;

    r.type = R_RMPART;
    r.u.rmPart.p = particleToTapeId (t, p);

    if (r.u.rmPart.p != ID_NOT_FOUND)
    {
        addRecord (t, &r, NULL);
    }
}

void recordStartDraw (tape *t, int start, int ptype)
{
    record r;

    if (!t->recording)
        return;

    r.type = R_DRSTART;

    r.u.start.startDraw = start;
    r.u.start.ptype = ptype;

    addRecord (t, &r, NULL);

}

void playbackItem (tape *t, int tapeIndex, vec_t *pOffset, vec_t *pCenter, int zoom, int rot)
{
    setting *s;
    float tx, ty, vl, ang;
    vec_t pos;
    particle *p, *pp;
    record *r;

    if ((tapeIndex < 0) || (tapeIndex >= t->tapePos))
        return;

    tapeActive = 1;

    r = &t->buffer[tapeIndex];

    pos.d.x = r->pos.d.x;
    pos.d.y = r->pos.d.y;

    if (drawableRecordType (r->type))
    {
        if (pCenter)
        {
            tx = pos.d.x - INTERNC(pCenter->d.x);
            ty = pos.d.y - INTERNC(pCenter->d.y);
            vl = VEC_LEN(tx, ty) * ((float)zoom / 100.0);

            ang = atan2 (-ty, tx);
            if (ang < 0)
                ang += 2 * M_PI;
            
            ang += ((float)rot / 360.0) * 2 * M_PI;

            pos.d.x = INTERNC(pCenter->d.x) + (int)(cos(ang) * vl);
            pos.d.y = INTERNC(pCenter->d.y) + (int)(-sin(ang) * vl);
        }

        if (pOffset)
        {
            pos.d.x += INTERNC(pOffset->d.x);
            pos.d.y += INTERNC(pOffset->d.y);
        }

        if (!inSelection (SCREENC(pos.d.x), SCREENC(pos.d.y)))
            goto out;
    }

    switch (r->type)
    {
        case R_PART:
            p = newParticle2 (pos.d.x, pos.d.y, r->u.part.vec.d.x, r->u.part.vec.d.y,
                          r->u.part.type, r->u.part.charge, r->u.part.mass, r->u.part.size, r->u.part.color);

            if (p == NULL)
            {
                t->info[tapeIndex].u.p = NULL;
                break;
            }

            if (IS_CPU_PARTICLE(p))
            {
                /* use max. number of links to be compatible 
                 */
                linkSetCount (p, MAX_LINKS);

                /* activate all links so that links can be generated later on 
                */
                activateClosedLinks (p);

                /* remember the particle ID so that we can reference to it when creating links */
                t->info[tapeIndex].u.p = p;
            }
#ifdef USE_OPENCL
            else
            {
                clCommitJobs();
            }
#endif

            break;

        case R_LINK:
            if ((r->u.link.p1 < 0) || (r->u.link.p1 >= t->tapePos))
            {
                // printf ("Link: bad particle id %d\n", r->u.link.p1);
                break;
            }
            if ((r->u.link.p2 < LID_FIRST) || (r->u.link.p2 >= t->tapePos))
            {
                // printf ("Link: bad particle id %d\n", r->u.link.p2);
                break;
            }
            p = t->info[r->u.link.p1].u.p;
            pp = t->info[r->u.link.p2].u.p;

//            printf ("load link: %d[%p] -> %d[%p]\n", r->u.link.p1, p, r->u.link.p2, pp);

            if (p == NULL)
            {
                // printf ("Link: Particle index 1 (%d) not in tape\n", r->u.link.p1);
                break;
            }

            if (r->u.link.p2 == DECR_MAX)
            {
                /* decrease max. number of links by one 
                 */
                linkSetCount (p, linkGetCount(p) - 1);
                break;
            }

            if (r->u.link.p2 == DECR_OPEN)
            {
                /* close one open link */
                closeOpenLinks (p, 1);
                break;
            }

            if (pp == NULL)
                break;

            linkParticles (p, pp, 0, r->u.link.data);
            break;

        case R_SETTING:
            s = hashToSetting (r->u.setting.hash);

            if (s)
            {
                if (r->u.setting.makeGlobal)
                {
                    markSetting (s, r->u.setting.value);
                }
                else
                {
                    modifySetting (s, r->u.setting.value, 0);
                }
            }
            else
            {
                printf ("playback: invalid setting hash %x\n", r->u.setting.hash);
            }
            break;

        case R_DRSTART:
            if (r->u.start.startDraw)
            {
                if (pdescs[r->u.start.ptype]->startDraw)
                {
                    pdescs[r->u.start.ptype]->startDraw();
                }
            }
            else
            {
                if (pdescs[r->u.start.ptype]->stopDraw)
                {
                    pdescs[r->u.start.ptype]->stopDraw(1);
                }
            }
            break;

        case R_RMPART:
        {
            particle *p = tapeIdToParticle (t, r->u.rmPart.p);
            screeninfo *pSi;

            if (p)
            {
                pSi = GET_SCREENINFO(p);

                removeParticle (p, 0);
                SI_CHARGE(pSi) -= PCHARGE(p);

                removeParticleFromTape (t, p);
            }
            break;
        }

        case R_BRICK:
        case R_POLYBRICK:
        {
            screeninfo *pSi;
            
            pos.d.x = SCREENC(pos.d.x);
            pos.d.y = SCREENC(pos.d.y);
            
            setBrick (pos.d.x, pos.d.y);

            pSi = &siArray[pos.d.x + pos.d.y * wid];

            if (pSi->particleList == SCR_BRICK)
            {
                SI_CHARGE(pSi) = r->u.brick.charge;
                WALL_MASS_SET(pSi,r->u.brick.mass);
            } 

            t->newBricks1.d.x = MIN(t->newBricks1.d.x, pos.d.x);
            t->newBricks1.d.y = MIN(t->newBricks1.d.y, pos.d.y);
            t->newBricks2.d.x = MAX(t->newBricks2.d.x, pos.d.x);
            t->newBricks2.d.y = MAX(t->newBricks2.d.y, pos.d.y);

            break;
        }

        case R_POLYPOINT:
            switch (r->u.line.what)
            {
                case p_start:
                    polyStart (SCREENC(pos.d.x), SCREENC(pos.d.y), 0);
                    polyMoveEdge (SCREENC(pos.d.x), SCREENC(pos.d.y), 0);
                    break;

                case p_add:
                    polyMoveEdge (SCREENC(pos.d.x), SCREENC(pos.d.y), 0);
                    polyFix();
                    break;

                case p_finish:
                    polyFinish();
                    break;
            }
            break;
    }

    replayProgress();

out:
    tapeActive = 0;
}

void playbackAll (tape *t, vec_t *pOffset, vec_t *pCenter, int zoom, int rot, unsigned int typeMask)
{
    int playbackPos;

    t->recording = 0;
    t->newBricks1.d.x = t->newBricks1.d.y = 1000000;
    t->newBricks2.d.x = t->newBricks2.d.y = 0;

    for (playbackPos = 0; playbackPos < t->tapePos; playbackPos++)
    {
        if ((typeMask & (1 << t->buffer[playbackPos].type)) == 0)
            continue;

        playbackItem (t, playbackPos, pOffset, pCenter, zoom, rot);
    }

    if ((t->newBricks1.d.x <= t->newBricks2.d.x) &&
        (t->newBricks1.d.y <= t->newBricks2.d.y))
    {
        makeNormals (t->newBricks1.d.x - 17, t->newBricks1.d.y - 17,
                     t->newBricks2.d.x + 17, t->newBricks2.d.y + 17, NORM_SIMPLE_SAVE);
    }
}
void eraseTape (tape *t)
{
    t->tapePos = 0;

    if (t->buffer)
        free (t->buffer);

    if (t->info)
        free (t->info);
    
    t->buffer = NULL;
    t->info = NULL;
}

int tapeEmpty (tape *t)
{
    return (t->tapePos == 0);
}

void recordScreenSection (tape *Tape, SDL_Rect *pRect)
{
    int                                 x, y;
    particle                        *p, *startParticle;
    screeninfo                        *pSi;
    int                                 done = 0;
    int                                 count, iters;
    static unsigned char        *captured = NULL;

    /* this array holds a flag for each particle whether it has already been saved
     */
    if (captured == NULL)
    {
        captured = malloc(sizeof(unsigned char) * options->maxParticles);
    }
    if (captured == NULL)
    {
        printf ("recordScreenSection: failed to alloc capture flag buffer \n");
        return;
    }
    memset (captured, 0, sizeof(unsigned char) * options->maxParticles);

    Tape->offx = pRect->x;
    Tape->offy = pRect->y;

    tapeClear (Tape);
    tapeStart (Tape);

    count = iters = 0;
    while (!done)
    {
        iters++;
        done = 1;
        startParticle = NULL;

        for (y = pRect->y; y < pRect->y + pRect->h; y++)
        {
            pSi = &siArray[pRect->x + y*wid];

            for (x = pRect->x; x < pRect->x + pRect->w; x++, pSi++)
            {
                if (iters == 1)
                {
                    /* capture bricks and accelleration vectors at first pass
                     */
                    if (pSi->particleList == SCR_BRICK)
                    {
                        recordBrick (Tape, x, y, SI_CHARGE(pSi), TEMPR(pSi), FRICTION(pSi), WALL_MASS_GET(pSi));
                    }
                    else if (pSi->flags & SI_IS_PUMP)
                    {
                        recordAccel (Tape, x, y, XACCEL(pSi), YACCEL(pSi));
                    }

                    if (pSi->flags & SI_IN_POLY)
                    {
                        recordPolyBrick (Tape, x, y, SI_CHARGE(pSi), TEMPR(pSi), FRICTION(pSi), WALL_MASS_GET(pSi));
                    }


                    done = 0;
                    continue;
                }
                
                if (pSi->particleList <= SCR_BRICK)
                    continue;
                
                for (p = pSi->particleList; p != NULL; p = p->next)
                {
                    if (captured[PARTICLE_ID(p)])
                        continue;

                    if (startParticle == NULL)
                    {
                        startParticle = p;

                        if (pdescs[PTYPE(p)]->startDraw)
                            recordStartDraw (Tape, 1, PTYPE(p));

                        recordNewParticle (Tape, p);

                        captured[PARTICLE_ID(p)] = 1;
                        count++;
                    }
                    else if (compareParticle (startParticle, p))
                    {
                        recordNewParticle (Tape, p);

                        captured[PARTICLE_ID(p)] = 1;
                        count++;
                    }
                    else
                    {
                        /* process this particle in the next iteration
                        */
                        done = 0;
                    }
                }
            }
        }

        if (startParticle)
        {
            if (pdescs[PTYPE(startParticle)]->stopDraw)
            {
                recordStartDraw (Tape, 0, PTYPE(startParticle));
            }
        }
    }

    printf ("recorded %d particles (%d iterations)\n", count, iters);
    
#ifdef USE_OPENCL
    clRecordScreenSection (Tape, pRect);
#endif
}

void recordLine (tape *t, int x, int y, recordLine_t what)
{
    record        r;

    if (!t->recording)
        return;

    r.type = R_POLYPOINT;

    r.pos.d.x = x - INTERNC(t->offx);
    r.pos.d.y = y - INTERNC(t->offy);

    r.u.line.what = what;

    addRecord (t, &r, NULL);
}
