## Defaults
#
OS = 		$(shell uname -o)
MOS =		$(shell uname -s)
LIBS =		-lSDL -lX11 
SDLROOT =	/usr

ALLVARIANTS =	default opengl opencl

GITVER  = $(shell git log | grep commit | head -1 | sed -e 's/.* //')

ifeq ($(OS),GNU/Linux)
 ifeq ($(CROSS),)
   include linux.mk
 else
   include mxe.mk
 endif
else
ifeq ($(MOS),MINGW32_NT-6.2)
include msys.mk
else
include mingw.mk
endif
endif

VPATH =		$(SRCDIR)

ARCHDIR=	obj/$(KERNEL)/$(MACH)

CFLAGS_SSE2_$(USE_SSE2)  = -DUSE_SSE2
CFLAGS_SSE41_$(USE_SSE41) = -DUSE_SSE41
CFLAGS_AVX_$(USE_AVX)	 = -DUSE_AVX

ifdef LLVM
SSEFLAGS=
else
SSEFLAGS=-mfpmath=sse
endif

ifndef prefix
prefix=/usr/local
endif

I_BINDIR=$(DESTDIR)$(prefix)/bin
I_LIBDIR=$(DESTDIR)$(prefix)/share/plansch
I_DOCDIR=$(DESTDIR)$(prefix)/share/doc/plansch

OFLAGS_$(USE_SSE2) = -msse2 $(SSEFLAGS)
OFLAGS_$(USE_SSE41_MATH) = -msse4.1 $(SSEFLAGS)
OFLAGS_$(USE_AVX) = -msse2avx $(SSEFLAGS)

CFLAGS += $(CFLAGS_SSE2_y)
CFLAGS += $(CFLAGS_SSE41_y)
CFLAGS += $(CFLAGS_AVX_y)
CFLAGS += $(OFLAGS_y)

CFLAGS += -Wall
CFLAGS += $(ECFLAGS)
#CFLAGS += -Werror
CFLAGS += -Wno-strict-overflow
CFLAGS += -Wno-unknown-pragmas

ifneq ($(GITVER),)
CFLAGS += -DGITVER=\"$(GITVER)\"
endif

ifeq ($(DEBUG),2)
CFLAGS += -DDEBUG=1
endif

LIBS +=		-lm 
INCDIRS += -I$(CURDIR) -I$(CURDIR)/pm -I$(CURDIR)/sdl_gfx

ifndef PARTICLE_TYPES
PARTICLE_TYPES	= clwimp water gas dla blob chain wimp energy 
GENMOVE_TYPES   = gas_c	chain_c
GENMOVE_TYPES_SSE2_$(USE_SSE2)  = gas_sse2  chain_sse2
GENMOVE_TYPES_SSE41_$(USE_SSE41) = gas_sse41 chain_sse41
GENMOVE_TYPES_AVX_$(USE_AVX)	= gas_avx   chain_avx
GENMOVE_TYPES += $(GENMOVE_TYPES_SSE2_y) $(GENMOVE_TYPES_SSE41_y) $(GENMOVE_TYPES_AVX_y)
endif

APPL_OBJECTS	= 	poly_tri.o arrow.o scrObjects.o record.o pipe.o main.o normals.o draw.o buttons.o spf.o font.o \
		probe.o colors.o fileio.o stencil.o time.o filesel.o \
		select.o widget.o  buttonHandlers.o values.o playback.o particle.o drprimitives.o \
		screen.o link.o move.o help.o psched.o math.o lines.o sdlgfx.o mark.o IMG_xpm.o track.o forms.o debug.o lock.o \
		flood.o colorPicker.o mpTemplates.o well.o input.o
		
CFILES =	$(APPL_OBJECTS:%.o=%.c) $(PARTICLE_TYPES:%=particle_%.c)

CLSRC  =        $(shell grep '^CLSRC' makefile.opencl | sed -e 's/.*=//')
CLHDR  =        $(shell grep '^CLHDR' makefile.opencl | sed -e 's/.*=//')

PIXMAPS =	$(shell ls pm/*.pm | sort)
PIXXMAPS =	$(shell ls pm/*.xpm | sort)

MKFILES =	Makefile rules.mk

DEPS =		init.stamp plansch.h globals.h inline.h protos.h types.h colors.h


all:		init.stamp $(VARIANTS)

frc:

clean:		
		rm -f $(VARIANTS:%=$(ARCHDIR)/%/*.o) init.stamp


$(VARIANTS):	$(DEPS)

init.stamp:
		make update
		touch init.stamp

# rule to update auto-generated files
# To be executed if settings or icons are added
#
update:		ptypes.h settings.h settings.c pixmaps.c pixmaps.h stencil_list.h 
		make clean

ptypes.h:	$(MKFILES)
		@echo Rebuilding $@ ..
		@rm -f ptypes.h
		@echo "#define PTYPES	$(PARTICLE_TYPES:%=&%,)" >> ptypes.h
		@echo "#define PDECLS	$(PARTICLE_TYPES:%=extern pdesc %;)" >> ptypes.h

settings.h:	$(CFILES) opengl.c opencl.c
		@echo Rebuilding $@ ..
		@rm -f $@
		@echo '/* Automagically generated! */' > settingsTmp.h
		@grep ^SETTING $(CFILES) opengl.c  | sed -e 's/[:()]/ /g' | awk '{print "extern setting " $$3 ";"}' >> settingsTmp.h
		@grep ^ASETTING $(CFILES) opengl.c | sed -e 's/[:(),]/ /g' | awk '{print "extern setting " $$3"["$$4"];"}' >> settingsTmp.h
		@if [ ! -f settings.h ]; then touch settings.h; fi
		@diff settings.h settingsTmp.h >/dev/null || mv settingsTmp.h settings.h

settings.c:	$(CFILES) $(MKFILES) plansch.h opengl.c
		@echo Rebuilding $@ ..
		@rm -f $@
		@echo '#include "plansch.h"' > settings.c
		@echo '/* Automagically generated! */' >> settings.c
		@echo 'settingId settingsList[] = {' >> settings.c
		grep ^SETTING $(CFILES) opengl.c | sed -e 's/[:()]/ /g' | awk '{print "{\"" $$3 "\", &"$$3 ", 1},"}' >> settings.c
		@grep ^ASETTING $(CFILES) opengl.c | sed -e 's/[:(),]/ /g' | awk '{print "{\"" $$3 "\", "$$3 ", "$$4"},"}' >> settings.c
		@echo '{NULL, NULL}' >> settings.c
		@echo '};' >> settings.c

pixmaps.c:	$(PIXMAPS) $(PIXXMAPS) $(MKFILES)
		@echo Rebuilding $@ ..
		@echo '/* Automagically generated */' > pixmaps.c
		@echo '#include "plansch.h"' >> pixmaps.c
		@cat $(PIXMAPS) | sed -e 's/static.*char/char/' >> pixmaps.c
		@cat $(PIXXMAPS) | sed -e 's/static.*char/char/' >> pixmaps.c

pixmaps.h:	pixmaps.c
		@echo Rebuilding $@ ..
		@echo '/* Automagically generated */' > pixmaps.h
		@grep '^char' pixmaps.c | sed -e 's/char/extern char/' -e 's/ =.*/;/' >> pixmaps.h

stencil_list.h:	pixmaps.h
		@echo Rebuilding $@ ..
		@echo '/* Automagically generated */' > stencil_list.h
		@echo 'surface_t pixmap_list[] = {' >> stencil_list.h
		@grep 'char' pixmaps.h | sed -e 's/.*\*[ ]*\(.*\)\[\].*/ { .pixmap=\1, .name="\1" },/' >> stencil_list.h
		@echo ' { .pixmap = NULL }};' >> stencil_list.h

VERSION = $(shell grep VERSION plansch.h | sed -e 's/.*\"\(.*\)\"/\1/')

REL = plansch $(ALLVARIANTS:%=plansch_%) README.md plansch.ttf $(CLSRC) $(CLHDR)
SREL = plansch README.md $(patsubst %, %/makefile, $(ALLVARIANTS)) *.c *.h pm/*.pm pm/*.xpm sdl_gfx/*.[ch] *.cpp Makefile *.mk CHANGES BUGS makefile.* $(CLSRC) $(CLHDR)

RELF = $(patsubst %,plansch-$(VERSION)/%,$(REL))
SRELF = $(patsubst %,plansch-$(VERSION)/%,$(SREL))
VER_REL = $(VERSION)$(VSUFFIX)

.PHONY:		plansch-$(VER_REL).tar.gz plansch-$(VER_REL)-win.zip plansch-$(VER_REL)-src.tar.gz

release:	plansch-$(VER_REL).tar.gz srelease

wrelease:	plansch-$(VER_REL)-win.zip

srelease:	plansch-$(VER_REL)-src.tar.gz

plansch-$(VER_REL).tar.gz:
		@rm -f plansch-$(VER_REL) $@
		@ln -s . plansch-$(VER_REL)
		@tar cfhz $@  `ls -d ${RELF} 2>/dev/null`
		@echo $@ | tee rel-file

plansch-$(VER_REL)-win.zip:
		@rm -rf plansch-$(VER_REL)-win $@
		@mkdir plansch-$(VER_REL)-win
		@cp $(VARIANTS:%=plansch_%.exe) $(CLSRC) $(CLHDR) *.bat README.md plansch-$(VER_REL)-win
		@zip -r $@ plansch-$(VER_REL)-win
		@echo $@ | tee wrel-file

plansch-$(VER_REL)-src.tar.gz:
		@rm -f plansch-$(VER_REL) $@
		@ln -s . plansch-$(VER_REL)
		@tar cfhz $@ `ls -d ${SRELF} 2>/dev/null | sort -u`; rm -f plansch-$(VER_REL)
		@echo $@ |tee srel-file

install:	all
		mkdir -p $(I_BINDIR)
		mkdir -p $(I_LIBDIR)
		mkdir -p $(I_DOCDIR)
		cp `ls -d $(ALLVARIANTS:%=plansch_%)` $(I_BINDIR)
		cp `ls -d $(CLSRC) $(CLHDR)` $(I_LIBDIR)
		cp README.md $(I_DOCDIR)

$(PARTICLE_OBJECTS): genmove.c do_move.c

dpkg:		
		rm -f plansch-$(VERSION)-src.tar.gz
		make plansch-$(VERSION)-src.tar.gz
		make -C deb clean
		make -C deb 
	    

export

.PHONY:		$(VARIANTS)

$(VARIANTS):
		$(MAKE) -f makefile.$@  VARIANT=$@ all

