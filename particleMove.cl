/*
Copyright (C) 2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#ifdef ATOMIC_CNT
#pragma OPENCL EXTENSION cl_ext_atomic_counters_32 : enable
#endif
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

#include "clprivate.clh"


/* check whether particle at p, vector v, will intersects with wall.
 * Return length^2 to intersection point or NO_INTERSECT (impossible large value)
 * if no intersection, or particle if moving away from wall.
 */
float intersect (float2 p, float2 np, float2 v, int off, global wall_t *wall, constant clParameters_t *params)
{
    float2 lineStart = wall->lineStart;
    float lineDyDx = wall->lineDyDx;
    float2 relPos = p - lineStart;
    float pDyDx, l;
    float2 rel_ip, ip;
    float2 n = wall->lineNormal;

    /* moving away from line ?
     */
    if (dot (n, v) >= 0.0f)
        return NO_INTERSECT;

    pDyDx = v.y / v.x;

    /* calculate intersection point of wall line and particle vector
     * relative to start coordinate of wall borderline for increased accuracy
     */
    if (isinf(pDyDx))
    {
        /* particle is moving vertically, simple formula
         * 
         * Assume that line.c code does not generate strict vertical lines!
         */

        rel_ip.y = relPos.x * lineDyDx;
        rel_ip.x = relPos.x;
    }
    else
    {
        /* no need to check for parallel lines. In the worst case we get
         * an infinite number if lineDyDx = pDyDx, which is the same as NO_INTERSECT
         * Other large values < INFINTE are dealt with by setting a distance
         * limit.
         */
        rel_ip.x = (relPos.y - relPos.x * pDyDx) / (lineDyDx - pDyDx);
        rel_ip.y = rel_ip.x * lineDyDx;
    }

    ip = rel_ip + lineStart;

#if 0
    /* the intersection point must be on the same pixel as the line.
     * Several lines in a single pixel are not possible, and the movement
     * is max. one pixel per loop.
     */
    int2 iip = convert_int2_rtz(ip);
    if (off != IMAD(iip.y, (int)SCR_WID, iip.x))
    {
        return NO_INTERSECT;
    }
#endif
   /* If particle will not be behind the wall's borderline at
     * the next iteration do nothing
     */
    if (dot (n, ip - np) < 0.0f)
    {
        return NO_INTERSECT;
    }

    /* if the particle is already on the other side of the border (which it isn't supposed to be)
     * report a zero length to the intersection point -> particle will be fixed at this position
     * and reflected at the normal
     * It will eventually/hopefully move out again.
     */
    if (dot (n, ip - p) >= 0.0f)
        return 0.0f;
 
    /* Return distance^2 to intersection point so that we can search for other lines
     * that might be nearer
     */
    rel_ip -= relPos;

    l = max(LEN(rel_ip) - 0.001f, 0.0f);

    return l;
}


/* Move GPU particles
 * 
 * newpos - new coordinate
 * newvec - new vector 
 * pos - current coordinate
 * vec - current vector 
 * attr - particle attributes
 * vecs - current vectors
 * idx/key - For particle collission. index array is sorted by key (screen offset) later.
 * iw/ih - screen wid/hei
 * 
 * attr.x = Particle type/flags (CL_FLG_XX/CL_TYPE_XX)
 */
#ifdef WITH_EINSTEIN
KERNEL_F2(particleMoveE) (
             global float2 *newpos
            ,global float2 *newvec
            ,global float2 *pos
            ,global float2 *vec
            ,global uint2 *attr
            ,global wall_t *walls
            ,global float *wallTemp
            ,global float2 *field
            ,constant clParameters_t *params
            ,global float2 *posVec
            ,global clStatus_t *status
            )
#else
KERNEL_F2(particleMove) (
             global float2 *newpos
            ,global float2 *newvec
            ,global float2 *pos
            ,global float2 *vec
            ,global uint2 *attr
            ,global wall_t *walls
            ,global float *wallTemp
            ,constant clParameters_t *params
            ,global float2 *posVec
            ,global clStatus_t *status
            )
#endif
{
    float2 p, v, np, nv;
    int2 pi, npi, pd;
    uint2 attributes;
    unsigned int so, nso;
    int dir;
    float2 n;
    float ll, lcount;
    float2 norm_vec;
    char2 cn;
    float d;
    float lmax;
    int iter = params->speedLimit;
    global int *pIndex = 0;
    int index;

    attributes = attr[GID0];
    float w = (float)SCR_WID;
    float h = (float)SCR_HEI;

    if (IS_MOVING_PART(attributes))
    {
        p = pos[GID0];
        v = vec[GID0];

        if (((v.x != 0.f) || (v.y != 0.f)) && !IS_STUCK(attributes))
        {
            /* particle is moving */
            ll = length (v);
            
#ifdef WITH_EINSTEIN
            lcount = ll * params->timeFactor * EINSTEIN(field[IMAD(p.y, (int)SCR_WID, p.x)], v);
#else
            lcount = ll * params->timeFactor;
#endif

            norm_vec = v / ll;

            if (params->trails)
            {
                /* the particle index counters are kept after the line vectors
                 */
                pIndex = (global int*)&posVec[params->maxParticles * CL_MAX_TRAIL_LENGTH * 2];
                index = pIndex[GID0];
            }

            while ((lcount > 0.f) && (iter > 0))
            {
                /* lmax is the maximum movement for this iteration
                 */
                lmax = fmin (lcount, 1.0f);

                np = MAD(norm_vec, lmax, p);

                pi = convert_int2_rtz(p);
                npi = convert_int2_rtz(np);
                pd = npi - pi;
                so = IMAD(pi.y, (int)SCR_WID, pi.x);
                nso = IMAD(npi.y, (int)SCR_WID, npi.x);

                if ((so >= MAX_SCR_OFFSET) || (nso >= MAX_SCR_OFFSET) || (npi.x >= SCR_WID) || (npi.x < 0))
                {
                    /* out of bounds, remove particle */
                    if (HAS_LINK(attributes))
                        status->updateLinks = 1;

                    attr[GID0] = (attributes.x & ~CL_TYPE_MASK) | CL_TYPE_DELETED;
                    status->needToDelete = 1;

                    newvec[GID0] = (float2)(0.0f, 0.0f);
                    newpos[GID0] = (float2)(0.0f, 0.0f);

                    return;
                }

                if ((pd.x || pd.y)
                    && (walls[nso].wallFlags & SI_BRICK)
                    && ((walls[so].wallFlags & SI_BRICK) == 0))
                {
                    /* transition from space to wall pixel
                     */

                    if (attributes.x & CL_MFLG_STICKY)
                    {
                        /* stick particle to this wall pixel
                         * until wall is removed
                         */
                        attr[GID0] = attributes.x | CL_MFLG_STUCK;
                        break;
                    }
                        
                    dir = ((pd.x + 1) * 3 + pd.y + 1);
                    dir -= (dir > 3);
                    dir = (dir & 7) * 2;

                    /* wall normal for this pixel direction
                     */
                    cn = (char2)(walls[so].brickNormals[dir], walls[so].brickNormals[dir+1]);

                    /* if there is a normal, and it points to the opposite direction, reflect
                     * the particle. Keep the position as it is.
                     */
                    if (cn.x || cn.y)
                    {
                        n = convert_float2(cn) / 127.f; 

                        /* The actual normal used for reflection is the wall normal (friction == 0)
                         * or the opposite of the movement vector (friction == 100) or the weighted,
                         * normalized sum of both
                         */
                        if (walls[nso].friction)
                        {
                            float frictionFactor = (float)walls[nso].friction / 100.0f;
                            n = normalize((n * (1.0f - frictionFactor)) - (norm_vec * frictionFactor));
                        }

                        d = dot(n, norm_vec);
                        if (d < 0.f)
                        {
                            /* reflect at mormal, don't move into the wall
                             */
                            norm_vec = MAD(-2 * d, n, norm_vec);
                            np = p;

                            if (params->flags & CL_EFLAG_WALLHEAT)
                            {
                                /* Heat transfer between particle and wall
                                 */
                                float e = ll * (float)PART_MASS(attributes);
                                float d = ((e - wallTemp[nso]) / 2.0f);

                                ll = (e - d) / (float)PART_MASS(attributes);
                                if (d < 0.0f)
                                {
                                    /* if particle receives energy, modify the direction towards
                                     * wall normal plus some random component perpendicular to the normal.
                                     */
                                    float2 rn = n + (float2)(n.y, n.x) * (frnd(params->seed) - 0.5f);
                                    norm_vec = normalize(norm_vec + rn*d);        
                                }

                                wallTemp[nso] += d;
                            }
                        }
                        else
                        {
                            /* collission while moving away from surface.
                             * This happens if the particle has been reflected at a very flat angle
                             * and hits another pixel outwards.
                             * Make sure it doesn't move into the wall by
                             * a) warping it one pixel in the direction of the normal (which should be free), or
                             * b) setting the direction equal to the wall normal
                             *
                             * a is better for non-interacting particles, b for colliding particles
                             *
                             */

                            // np = p + n;                /* warp */

                            np = p;        
                            norm_vec = n;         /* stay put, apply wall normal */
                        }
                    }
                }
                else
                {
                    /* Check for intersect with vectorized wall
                     */
                    float d1 = NO_INTERSECT, d2 = NO_INTERSECT;

                    if (walls[so].wallFlags & (SI_LINE|SI_IN_POLY))
                    {
                        d1 = intersect (p, np, norm_vec, so, &walls[so], params);
                    }

                    if ((nso != so)
                        && (walls[nso].lineDyDx != walls[so].lineDyDx)
                        && (walls[nso].wallFlags & (SI_LINE|SI_IN_POLY)))
                    {
                        /* Crossing pixel boundary, there is a borderline on the next pixel,
                         * and it's different to the one on the current pixel.
                         * Check intersection/distance to see if it's nearer.
                         */
                        d2 = intersect (p, np, norm_vec, nso, &walls[nso], params);
                    }

                    if ((d1 <= lmax) || (d2 <= lmax))
                    {
                        if (attributes.x & CL_MFLG_STICKY)
                        {
                            /* stick particle to this wall pixel
                             * until wall is removed
                             */
                            attr[GID0] = attributes.x | CL_MFLG_STUCK;
                            break;
                        }
                        
                        /* if there is a collision with the borderline on this or the next pixel,
                         * reflect the particle at the one with the nearer intersection point.
                         *
                         * Move particle onto the intersection point, reflect it and add the remainder
                         * to the remaining stride of the particle
                         */
                        float2 lineNormal;

                        if (d1 < d2)
                        {
                            np = MAD (norm_vec, d1, p);
                            lineNormal = walls[so].lineNormal;
                            lcount += 1.0f + (lmax - d1);
                        }
                        else
                        {
                            np = MAD(norm_vec, d2, p);
                            lineNormal = walls[nso].lineNormal;
                            lcount += 1.0f + (lmax - d2);
                        }

                        /* The actual normal used for reflection is the wall normal (friction == 0)
                         * or the opposite of the movement vector (friction == 100) or the weighted,
                         * normalized sum of both
                         */
                        if (walls[so].friction)
                        {
                            float frictionFactor = (float)walls[so].friction / 100.0f;
                            lineNormal = normalize(MAD(lineNormal, (1.0f - frictionFactor), -(norm_vec * frictionFactor)));
                        }

                        norm_vec -= 2.0f * lineNormal * dot (norm_vec, lineNormal);
                    }
                }

                if (pIndex)
                {
                    int indexPtr = (GID0 * params->trails + index) * 2;
                    posVec[indexPtr] = p;
                    posVec[indexPtr+1] = np;
                    index = (index + 1) % params->trails;
                }

                lcount -= 1.f;
                iter--;
                p = np;
            };

            if (pIndex)
                pIndex[GID0] = index;

            nv = norm_vec * ll;
        }
        else
        {
            nv = v;
            np = p;

            /* Unstick particle if wall no longer exists on this pixel
             * or its no longer sticky
             */
            if (attributes.x & CL_MFLG_STUCK)
            {
                pi = convert_int2_rtz(p);
                so = IMAD(pi.y, (int)SCR_WID, pi.x);
                if (((attributes.x & CL_MFLG_STICKY) == 0) ||
                    ((walls[so].wallFlags & (SI_BRICK|SI_LINE|SI_IN_POLY)) == 0))
                {
                    attr[GID0] = attributes.x & ~CL_MFLG_STUCK;
                }
            }
        }

        newvec[GID0] = nv;
        newpos[GID0] = np;
    }
}
