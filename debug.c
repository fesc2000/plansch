/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"

#define MAX_MARKERS 1000

static int numMarkers = 0;
static scrPoly *markers[MAX_MARKERS];
static char *mnames[MAX_MARKERS];

scrPoly *moveNamed (int x, int y, uint col, char *name)
{
    int i;

    if (!name)
        return NULL;

    if (name[0] == '+')
        return NULL;

    for (i = 0; i < numMarkers; i++)
    {
        if (mnames[i] && !strcmp (name, mnames[i]))
        {
            polyChangeColor (markers[i], -1, col);
            polyMove (markers[i], x, y);

            return markers[i];
        }
    }

    return NULL;
}

scrPoly *dbgMarker (int x, int y, int rad, uint col, char *name)
{
    scrPoly *p = moveNamed (x, y, col, name);

    if (p)
        return p;

    if (numMarkers >= MAX_MARKERS)
        return NULL;

    markers[numMarkers] = scrMarker (x, y, rad, col);
    mnames[numMarkers] = name;
    numMarkers++;

    return markers[numMarkers-1];
}

scrPoly *dbgMarkerBox (int x, int y, int len, uint col, char *name)
{
    scrPoly *p = moveNamed (x, y, col, name);

    if (p)
        return p;

    if (numMarkers >= MAX_MARKERS)
        return NULL;

    markers[numMarkers] = scrBox (x, y, len, col);
    mnames[numMarkers] = name;
    numMarkers++;

    return markers[numMarkers-1];
}

scrPoly *dbgLine (int x1, int y1, int x2, int y2, uint col, char *name)
{
    scrPoly *p = moveNamed (x1, y1, col, name);

    if (p)
        return p;

    if (numMarkers >= MAX_MARKERS)
        return NULL;

    markers[numMarkers] = scrLine (x1, y1, x2, y2, col);
    mnames[numMarkers] = name;
    numMarkers++;

    return markers[numMarkers-1];

}

void clearMarkers (char *wildcard)
{
    int i;

    for (i = 0; i < numMarkers; i++)
    {
        if (wildcard == NULL)
        {
            polyDelete (markers[i]);
        }
        else if (mnames[i] && !strncmp(mnames[i], wildcard, strlen(wildcard)))
        {
            polyDelete (markers[i]);
            markers[i] = markers[numMarkers-1];
            mnames[i] = mnames[numMarkers-1];
            numMarkers--;
            i--;
        }
    }
    if (wildcard == NULL)
        numMarkers = 0;
}

typedef struct strtab_s
{
    char str[32];
    struct strtab_s *next;
} strtab_t;

static strtab_t *strtab = NULL;

char *handleStr(void *h)
{
    char    val[32];
    strtab_t *s;

    sprintf (val, "%p", h);

    for (s = strtab; s != NULL; s = s->next)
    {
        if (!strcmp (s->str, val))
            return s->str;
    }

    s = (strtab_t*)malloc(sizeof(strtab_t));

    strcpy (s->str, val);

    s->next = strtab;
    strtab = s;

    return s->str;
}

