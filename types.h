/*
Copyright (C) 2010 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


typedef unsigned int uint32;
#ifdef _MINGW
typedef unsigned int uint;
#endif
typedef intptr_t (*FUNCPTR) ();
typedef void (*VOIDFUNCPTR) ();
typedef void* (*TFUNCPTR) (void*);
typedef int (*PFUNCPTR) (void *, void*);

typedef __m128 v4sf;  // vector of 4 float (sse1)
#ifdef USE_SSE2
# include <emmintrin.h>
typedef __m128i v4si; // vector of 4 int (sse2)
#endif


#ifdef _MSC_VER /* visual c++ */
# define ALIGN16_BEG __declspec(align(16))
# define ALIGN16_END 
#else /* gcc or icc */
# define ALIGN16_BEG
# define ALIGN16_END __attribute__((aligned(16)))
#endif

/* declare some SSE constants -- why can't I figure a better way to do that? */
#define _PS_CONST(Name, Val)                                            \
  static const ALIGN16_BEG float _ps_##Name[4] ALIGN16_END = { Val, Val, Val, Val }
#define _PI32_CONST(Name, Val)                                            \
  static const ALIGN16_BEG int _pi32_##Name[4] ALIGN16_END = { Val, Val, Val, Val }
#define _PS_CONST_TYPE(Name, Type, Val)                                 \
  static const ALIGN16_BEG Type _ps_##Name[4] ALIGN16_END = { Val, Val, Val, Val }


_PS_CONST(0p5, 0.5f);
_PS_CONST(0p1, 0.01f);
_PS_CONST(1  , 1.0f);


typedef union 
{
    struct
    {
        int x, y;
    } d;
    struct
    {
        int x, y;
    };
    int64_t ll;
    __m64 m;
} vec_t;

typedef union
{
    struct
    {
        int64_t x, y;
    };
    struct
    {
        int64_t x, y;
    } d;
    __m128 m;
} llTuple;

typedef union
{
    struct
    {
        signed short x, y;
    };
    struct
    {
        signed short x, y;
    } d;
    unsigned int t;
} svec_t;

 
typedef union
{
    struct
    {
        signed char x, y;
    };
    struct
    {
        signed char x, y;
    } d;
    unsigned short t;
} bTuple;

typedef union
{
    struct
    {
        float x, y;
    };
    struct
    {
        float x, y;
    } d;
} fvec_t;

typedef vec_t triangle[3];

typedef enum
{
    unmark,
    mark
} markAction;

typedef struct 
{
    int x, y, w, h;
} rect_i;

#define FCOLOR(p)   ((p)->color)
#define ICOLOR(p)   ((p)->color.icolor)

typedef struct  __attribute__((aligned(64))) __attribute__((packed)) particle
{
    union
    {
        /* float color value (OpenGL)
         */
        __m128        cv; 
        float        c[4];

        /* integer color value (non-OpenGL)
         */
        union
        {
            uint32_t col32;
            char         c[4];
        } icolor;
    } color;        /* 16 */

    struct particle *next;        /* 4/8 next in per-pixel chain */
    void *pPtr;                        /* 4/8 Private pointer*/
    vec_t pos;                        /* 8 position */
    vec_t vec;                        /* 8 vector */

    
    signed char                charge;        /* 1 Charge (-1, 0, 1) */
    unsigned char        mass;        /* 1 Mass */
    unsigned char        type;        /* 1 Particle type */
    unsigned char        size;        /* 1 Size */


    unsigned char pflags;        /* 1 Generic flags */
    signed char          frcInv;        /* 1 Invert force effect (-1, 1) */
    unsigned char rot;                /* 1 DLA: Rotation */
    unsigned char dir;                /* 1 DLA: Connection direction */

#ifdef PID_IN_PSTRUCT
    struct particle *prev;                /* 4/8 prev in per-pixel chain */   
#endif
} particle;


typedef struct __attribute__((aligned(128))) __attribute__ ((packed))  particle2
{
    union
    {
        __m128        cv; 
        float        c[4];
    } actPot;                        /* 16 */

    union
    {
        __m128        cv; 
        float        c[4];
    } cdiff;                        /* 16 */

    vec_t lpos;                        /* 8 */
    vec_t ipos;                        /* 8 */
    int ipos_flag;                /* 4 */
#if 0
    unsigned int food;
#endif
    unsigned int crange;        /* 4 */

    /* @56 */

    particle *p;                /* 4/8 */
    struct particle2 *del_next;        /* 4/8 */

} particle2;

typedef struct __attribute__ ((packed)) screeninfo
{
    struct particle *particleList;

    signed char normals[2 * 8];

    union
    {
        unsigned short        tempr[2];
        signed short    accel[2];
    } u;

    int                        wimps;
    unsigned short        flags;
    unsigned char        friction;
    unsigned char        neighbours;

} screeninfo;

typedef  struct
{
    unsigned int lastPartCount;
    unsigned int attractDirCheck;
    unsigned int tbo;
    int             rnd;
    int             drawLinkLine;

    struct
    {
        int partCount;
        unsigned int nomove;
        unsigned int unshareFree;
        unsigned int unshareSqueeze;
        unsigned int unshareStuck;
        unsigned int wallHit;
        unsigned int collissions;
        unsigned int move;
        unsigned int nLock;
        unsigned int impuls;
        unsigned int reorder;
        unsigned int locked;
        unsigned int gas_nb_total;
        unsigned int gas_nb_ccount;
        unsigned int unordered;

    } v;

    unsigned int wallPixels;
    uint64_t wallTempr;

    unsigned int             *particleList;
    int              numParticles;
    int              handoverStart;
    int              numHandover;

    int              cpuScore;

    int              numFusionEvents;
    int              maxFusionEvents;
    vec_t    *fusionEvents;

    vec_t    *linkLines;
    v4sf     *linkColors;
    int              numLinkLines;

    vec_t    *linkLines2;
    v4sf     *linkColors2;
    int              numLinkLines2;

#ifdef MT_BORDERS
#ifdef THREAD_DIST_HORIZ
    uint32 workLeft;
    uint32 workRight;
#else
    uint32 workTop;
    uint32 workBot;
#endif
    int fixedBorder;
#endif

    int *xa_arr;
    int *ya_arr;
    int *pa_arr;
    int *rada_arr;

    uint32  siModIdx;
    screeninfo **siModArray;

    void *rightMarker;
    
#ifdef OPENMP
#ifdef MT_BORDERS
    omp_lock_t        *lock[2];
#endif
#ifdef BIND_THREADS
    cpu_set_t        *set;
#endif
    int                 setsize;
#endif

    particle2 *deleteList;
} threadData;

typedef struct __attribute__ ((aligned (64)))
{
        int dirSi[8];
        int quadSi[9];
        int *quadSiNeg;
        int dirNextSi9[9+1];
        int dirNextSi5[5+1];
        int dirNextSi8[8+1];
} globalData;

#define MAX_LINKS  6
typedef struct
    {
        particle *links[MAX_LINKS];
        uint32          linkData[MAX_LINKS];
        unsigned char num;
        unsigned char max;
        unsigned char count;
#ifdef LINK_HAVE_OTHER_IDX
        // needed by CARNIVORE type
        unsigned char give[MAX_LINKS];
        unsigned char otherIdx[MAX_LINKS];
#endif
    } pLinks;

#define PLINE_FPTYPE        float

/* line formula: y = y0 + x * dyDx
 */
typedef struct
{
    PLINE_FPTYPE y0;            /* y0 */
    PLINE_FPTYPE dyDx;    /* asc. */
} pLine;

/* line with normal vector (right-oriented) */
typedef  struct
{
    pLine l;
    vec_t n;
} pLineN;

#define MAX_PANEL_LINK        10

typedef struct _panel
{
    char        *name;
    vec_t        start, cur, max, dir;
    vec_t        size;
    int         spacing;
    SDL_Rect    box;

    int                primaryIsX;
    vec_t        nextRC, startRC;

    int         visible;
    int         disabled;
    int         restore;
    uint32        flags;

    int         gid;
    
    struct _panel *left[MAX_PANEL_LINK], *right[MAX_PANEL_LINK], *above[MAX_PANEL_LINK], *below[MAX_PANEL_LINK];

    struct _button *lastSel;

    struct _button *buttons;
    struct _button *lastButton;
    
    struct _panel *next;
} panel;

typedef struct _button
{
    int visible;
    int disabled;
    int selected;
    int borderCol;
    int selectCol;
    VOIDFUNCPTR handler;
    int x1, y1, x2, y2;
    char **pixmap;
    SDL_Surface *s;
    SDL_Rect r;
    int pmx, pmy;
    int pmoffx, pmoffy;
    char *help;
    char *name;
    int savedDisable;
    int drawing;
    int random;
    int randomRangeStart;

    intptr_t userData;

    int useSelMarker;
    uint32_t bgColor;
    struct setting *bgColorSetting;

    struct _button *panelNext;
    struct _button *parentButton;

    struct setting *pSetting;

    /* array of settings */
    int *applyToArray;
    int settingInstances;
    struct setting *settingArray;

    panel *pPanel;

    panel *subPanel;

    char *text;

    SDL_Rect groupRect;
    int groupCol;
    struct _button *groupFirst, *groupNext;

    struct _button *next;

} button;

typedef struct
{
    int           x     : 12;
    int           y     : 12;
    unsigned int  lum   :  8;
    unsigned int  argb  : 32;
} colorVal_t;

/* 32-bit value for little endian */
#define GETVAL32(s)    ((s).current.val.v32[0])

/* overlay for value field of struct setting
 */
typedef union
{
    int                v32[2];
    int64_t        va64;
    colorVal_t        cv;
} value_u;

typedef struct settingStore
{
    void *key;

    value_u val;        /* actual setting */

    float normFloat;        /* if floatDivi != 0: val/floatDivi, else val/(max((abs)maxValue, abs(minValue)) */
    float floatVal;        /* val/(max((abs)maxValue, abs(minValue)) */

    int isRandom;

    int rndRange;                /* random range enabled? */
    int64_t rndMin, rndMax;

    uint64_t rndMask;
    int numUnmasked;                /* current number of unmasked values */

    struct settingStore *next;
} settingStore_t;

typedef struct setting
{
    settingStore_t        current;

    int64_t prevValue;        /* previous setting */
    int partAttr;        /* particle specific attribute */
    int canBeRandom;
    int        floatDivi;        

    int marked;                /* marked for "apply" operation */

    int64_t minValue;
    int64_t maxValue;
    int64_t defaultValue;
    int64_t minDiff;

    int wallSetting;                /* whether it affects a wall */

    int discrete;                /* whether the setting has discrete values */
    int numDiscrete;                /* number of discrete values */
    int64_t valueArray[64];        /* array of discrete values */
    int rndMapArray[64];        /* current array of selectable randum numbers (numUnmasked entries) */

    int colorSelection;
    
    char *name;
    char *help;

    FUNCPTR cb;                /* void cb (&setting, cbArg) */

    button *pButton;        /* associated button or NULL */

    int isArray;
    int index;                /* index in array */
    struct setting *arrayFirst;

    struct settingId *id;
    uint32 hash;

    int valid;

    settingStore_t *store;

    struct setting *next;
} setting;

typedef struct settingId
{
    char *name;
    setting *pSetting;
    int instances;
    int isGrouped;
} settingId;


/* record->type values
 */
#define R_PART                1
#define R_RMPART        2
#define R_BRICK                3
#define R_EMPTY                4
#define R_SETTING        5
#define R_DRSTART        6
#define R_LINK                7
#define R_ACCEL                8
#define R_POLYBRICK        9
#define R_POLYPOINT     10

typedef enum
{
    p_start = 1,
    p_add = 2,
    p_finish = 3
} recordLine_t;

typedef struct
{
    unsigned char type;
    unsigned int iteration;
    vec_t pos;

    union
    {
        struct
        {
            vec_t vec;
            signed char          charge;
            unsigned char mass;
            unsigned char type;
            unsigned char size;
            uint32_t          color;
        } part;

        struct 
        {
            int p;
        } rmPart;

        struct
        {
            int            p1, p2;
            uint32 data;
        } link;

        struct
        {
            int charge;
            int heat;
            int friction;
            int mass;
        } brick;

        struct 
        {
            svec_t accel;
            unsigned int flags;
        } empty;

        struct
        {
            uint32 hash;
            uint64_t value;
            int makeGlobal;
        } setting;

        struct
        {
            int startDraw;
            int ptype;
        } start;
        
        struct
        {
            int dx, dy;
        } accel;

        struct
        {
            recordLine_t what;
        } line;

    } u;
} record;

typedef struct
{
    union 
    {
        struct particle *p;
    } u;
} recordInfo;

typedef struct
{
    int tapePos;
    int tapeLen;
    int drawables;
    unsigned int recordStart;
    record *buffer;
    recordInfo *info;
    int recording;
    int offx, offy;
    rect_i rect;
    vec_t newBricks1, newBricks2;
} tape;

#ifdef USE_OPENGL

/* The OGL_Texture structure */
typedef struct
{
    int valid;                
    int needsUpdate;
    GLuint name;        /* OpenGL texture name (texture id) */
    int texId;
    GLenum format;        /* The color format of the texture */
    GLenum dataType;
    int w, h;                /* The width and height of the original surface */
    int yOffset;        /* y-offset into source surface and screen */

    char *data;
    int BytesPerPixel;
} OGL_Texture;

#define OGL_UNKNOWN 0

extern OGL_Texture *screenTexture;
extern OGL_Texture *panelTexture;

#endif

typedef struct surface_s
{
    int w, h;
    SDL_Surface        *surface;
    char **pixmap;
#ifdef USE_OPENGL
    OGL_Texture        *texture;
#endif
    char *name;
} surface_t;

typedef struct
{
    tape        *Tape;
    
    SDL_Surface        *orgBitmap;
    SDL_Surface *orgData;
    
    SDL_Surface *currentBitmap;
    SDL_Surface *currentData;
    
    int currentZoom;
    int currentRotation;

    vec_t lastPos;
    vec_t lastSize;
    vec_t lockedPos;
    int stencilLocked;

#ifdef USE_OPENGL
    OGL_Texture        *texture;
#endif
    
} stencil_t;

typedef struct
{
    FUNCPTR handler;
    int            wid, hei;
    char    **icon;
    char    *help;
    intptr_t    userData;
    setting *pSetting;
    int attr;
} pWidget;

typedef screeninfo * (*MOVEFUNC) (particle *, screeninfo *, threadData *, int, particle2*);
typedef particle * (*NEWPART_FCT) (int x, int y, int vx, int vy, int sz, int chg, int m, int frcInv, uint32_t col );
typedef struct
{
    MOVEFUNC        moveParticle;                /* move single particle */
    MOVEFUNC        moveParticleCurrent;        
    FUNCPTR     setType;                /* change particle type to this */
    FUNCPTR     unsetType;                /* change particle type to other */
    FUNCPTR        startDraw;                /* start a draw operation (mouse down) */
    FUNCPTR        stopDraw;                /* finish draw operation (mouse up) */
    FUNCPTR        init;                        /* initialize ptype module */
    FUNCPTR        canCreate;                /* can create new particle? */
    FUNCPTR        hit;                        /* hit particle with vector */
    FUNCPTR        getRot;                        /* get particle rotation */
    FUNCPTR        compare;                /* compare with other particle for same properties */
    FUNCPTR        applySetting;                /* apply a setting to particle */
    FUNCPTR        getColor;                /* get particle's color index */
    FUNCPTR        iteration;                /* report interation done to module */
    FUNCPTR        iterationFrozen;        /* report interation done to module (frozen mode) */
    FUNCPTR        iterationCurrent;
    FUNCPTR        placementHint;                /* placement hint (PHINT_xxx) */
    FUNCPTR        walkSiblings;                /* walk all siblings: walk(p, fct, arg) */
    FUNCPTR        redraw;                        /* same as moveParticle, just redraw */
    FUNCPTR        link;                        /* link with other particle */
    FUNCPTR        numLinks;                /* number of links */
    NEWPART_FCT newParticle;                /* independent "newParticle" function */
    FUNCPTR        activate;                /* particle type has been (de)selected */
    char        *name;
    char        *descr;
    char        *help;
    pWidget         widgets[MAX_PWIDGETS];
    struct _panel *pPanel;
    int                 hasPanel;
    char        **pm;
    int                 isWimp;
    int                 isGPUParticle;
    int                 noSize;                /* doesn't have a defined size (water) */
    int          noDecel;
    int                 noContDraw;
    int                 initStatus;
    int                 skip;
    int          mpObject;
    FUNCPTR         drawInterval;

    setting         *crangeColors[3];                /* colors for the three charges */


} pdesc;

typedef struct
{
    int initialized;
    char *dir;
    char *file;
    int fileIndexFirst;
    int fileIndexSelected;

    FUNCPTR loadFunc;
} fileRep;

typedef struct _scrPoly
{
    struct _scrPoly *next;

    int refcount;

    /* whether object is active */
    int active;        

    /* whether object is visible on screen */
    int visible;

    int changed;

    /* whether poly is in screen or world corrdinates */
    uint flags;

    /* base position */
    vec_t pos;

    /* (max.) number of edges */
    int num, max;

    /* edge colors */
    uint32 *colors;

    /* relative offsets */
    fvec_t *offsets;

    /* absolute coordinates */
    fvec_t *abs;

    /* tesselated trianges */
    int numTri;
    triangle *triangles;
    uint32 fillColor;

    /* default color */
    uint32 dflCol;
} scrPoly;

typedef struct icon_s
{
    char *name;
    char *png;
    SDL_Surface *s;
} icon_t;

typedef enum 
{
    bt_normal,
    bt_marked,
    bt_temporary
} btype_t;

typedef struct 
{
    screeninfo *oldSi;
    vec_t        screenPosOld;
    int                oldOff;

    vec_t        screenPosNew;
} pWork;

typedef union
    {
        v4sf col;
        float c[4];
    } fcolor_t;

typedef struct callback_s
{
    void *arg;
    FUNCPTR fct;

    struct callback_s *next;
} callback_t;


typedef struct
{
    int isMutex;
    int isCount;
    int count;
    int        waiters;

    void *mutexTakenBy;
    int recurseCount;

    pthread_mutex_t mutex;
    pthread_cond_t  cond;
} sem_t;

typedef struct
{
    vec_t        start, end;

    float dyDx;    /* dy/dx */
    vec_t vec, normal;
    int intersections;
} line_t;

typedef struct
{
    float       ox, oy;
    int         nLinks, usedLinks;
    int         linkTo[6];
} mPartEntry_t;

typedef struct
{
    char *name;
    int num;
    void *s;
    mPartEntry_t tbl[];
} mPartTpl_t;

typedef struct 
{
    float ns;
    float c;
} scTable_t;

typedef struct
{
    int                 interval;
    void        *cbId;

    vec_t         pos;
    vec_t         vector;
    int                 angle;

    int                 prob, starting;

    char        **stencil;

    void         *poly;

    void *next;
} pWell_t;

struct cfg_options
{
    int bench;
    int clGlSharing;
    int usedClDevice;
    int usedClPlatform;
    int useGpu;
    int useGlCursor;

    int minZoom;

    int fullscreen;
    int noScreenOutput;

    char *loadFile;

    uint32 drawMode;
    int scrWid, scrHei;
    int bigBang;
    int maxParticles;
    int maxGpuParticles[CL_NUM_PARTICLE_SETS];

    int         numThreads;
    int cpuCoreBase;
    int randomPosition;
    int maxLoops;
    int maxTime;
    int exitOnTapeEnd;
    int loadTapeImm;

};

