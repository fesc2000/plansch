/*

Copyright (C) 2015 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

static int hidePanel = 0;
static int shift = 0;
static int shiftReleased = 0;
static int ctrl = 0;
static int movement = 1;
static FUNCPTR escHandler = NULL;
static int borderOn = 0;
static int lockBorders = 0;
static int freezeParticles = 0;
static int throttled = 0;
static int wasThrottled = 0;
static int motionCaptureButton = 0;
static int throttle = 0;

vec_t mousePos;
vec_t iPositionLast;
static vec_t motionLast;
static vec_t positionLast;
static uint32 motionTime = 0;
extern vec_t arrowVec;

int motionCapture (int button)
{
    if (button == -1)
        return motionCaptureButton;

    motionCaptureButton = button;

    return motionCaptureButton;
}


void setThrottle (int t)
{
    throttled = t;
}

int isThrottled(void)
{
    return throttled;
}

int enginesStopped (void)
{
    if (freezeParticles || throttled)
        return 1;

    return 0;
}


void clearBorderOn (void)
{
    borderOn = 0;
}

int bordersLocked(void)
{
    return lockBorders;
}

void
wipe ()
{
    int x, y;
    SDL_Rect r;
 
    removeAllPolys();

    for (x = 0; x < wid; x++)
        for (y = 0; y < hei; y++)
        {
            setEmpty (x, y, 1);
        }

    if (getSelection(&r))
    {
        makeNormals (r.x - 17, r.y - 17, r.x + r.w + 17, r.y + r.h + 17, NORM_SIMPLE_SAVE);
    }


    borderOn = 0;
}


static void toggleBorder(void)
{
    int x,y;
    int xl, xr, yu, yd;

    xl = zoomx;
    xr = xl + wid/zoom;
    yu = zoomy;
    yd = yu + hei/zoom;

    if (hidePanelSection())
    {
        yd = MIN(hei-1, yu + options->scrHei/zoom);
    }

    borderOn = 1 - borderOn;

    if (borderOn)
    {
        for (x = xl; x < xr; x++)
        {
            setBrick (x, yu);
            setBrick (x, yu+1);
            setBrick (x, yd-1);
            setBrick (x, yd-2);
        }
        for (y = yu; y < yd; y++)
        {
            setBrick (xl, y);
            setBrick (xl+1, y);
            setBrick (xr-1, y);
            setBrick (xr-2, y);
        }
    }
    else
    {
        for (x = xl; x < xr; x++)
        {
            setEmpty (x, yu, 1);
            setEmpty (x, yu+1, 1);
            setEmpty (x, yd-1, 1);
            setEmpty (x, yd-2, 1);
        }

        for (y = yu; y < yd; y++)
        {
            setEmpty (xl+0, y, 1);
            setEmpty (xl+1, y, 1);
            setEmpty (xr-1, y, 1);
            setEmpty (xr-2, y, 1);
        }

    }

    
    makeNormals (0,     0,     wid-1, 3,     NORM_SIMPLE_SAVE);
    makeNormals (0,     hei-4, wid-1, hei-1, NORM_SIMPLE_SAVE);
    makeNormals (0,     0,     3,     hei-1, NORM_SIMPLE_SAVE);
    makeNormals (wid-3, 0,     wid-1, hei-1, NORM_SIMPLE_SAVE);


}

static void zoomIn (void)
{

#ifdef ZOOM
    if (zoom >= MAX_ZOOM)
        return;

    zoom = zoom * 2;

    izoomx += INTERNC(mousePos.d.x / zoom);
    izoomy += INTERNC(mousePos.d.y / zoom);

    if (izoomx < 0)
        izoomx = 0;
    else if (izoomx + INTERNC(wid/zoom) > INTERNC(wid))
        izoomx = INTERNC(wid - wid/zoom);

    if (izoomy < 0)
        izoomy = 0;
    else if (izoomy + INTERNC(hei/zoom) > INTERNC(hei))
        izoomy = INTERNC(hei - hei/zoom);

    zoomx = SCREENC(izoomx);
    zoomy = SCREENC(izoomy);

    positionLast.d.x = mousePos.d.x / zoom + zoomx;
    positionLast.d.y = mousePos.d.y / zoom + zoomy;

    redrawScreen ();

    borderOn = 0;

#endif
}

static void zoomOut (void)
{
#ifdef ZOOM
    if (zoom == 1)
        return;

    izoomx -= INTERNC(mousePos.d.x / zoom);
    izoomy -= INTERNC(mousePos.d.y / zoom);

    zoom = zoom / 2;

    if (izoomx < 0)
        izoomx = 0;
    else if (izoomx + INTERNC(wid/zoom) > INTERNC(wid))
        izoomx = INTERNC(wid - wid/zoom);

    if (izoomy < 0)
        izoomy = 0;
    else if (izoomy + INTERNC(hei/zoom) > INTERNC(hei))
        izoomy = INTERNC(hei - hei/zoom);

    zoomx = SCREENC(izoomx);
    zoomy = SCREENC(izoomy);

    positionLast.d.x = mousePos.d.x / zoom + zoomx;
    positionLast.d.y = mousePos.d.y / zoom + zoomy;

    redrawScreen ();

    borderOn = 0;

#endif
}
static void mouseMotion (SDL_Event *pEvent, int *ix, int *iy)
{
    static int drawingStencil = 0;

    pEvent->motion.x -= CORR_X;
    pEvent->motion.y -= CORR_Y;
    mousePos.d.x = MAX((signed short)pEvent->motion.x, 0);
    mousePos.d.y = MAX((signed short)pEvent->motion.y, 0);

    iPositionLast.d.x = SX_TO_IX(mousePos.d.x);
    iPositionLast.d.y = SY_TO_IY(mousePos.d.y);

    if (ix)
        *ix = iPositionLast.d.x;

    if (iy)
        *iy = iPositionLast.d.y;


    if (motionTime == 0)
    {
        motionLast.d.x = (signed short)pEvent->motion.x;
        motionLast.d.y = (signed short)pEvent->motion.y;
        motionVec.d.x = motionVec.d.y = 0;
        motionTime = planschTime();
    }
    else if ((planschTime() - motionTime) > 100)
    {
        motionVec.d.x = (signed short)pEvent->motion.x - motionLast.d.x;
        motionVec.d.y = (signed short)pEvent->motion.y - motionLast.d.y;
        motionLast.d.x = (signed short)pEvent->motion.x;
        motionLast.d.y = (signed short)pEvent->motion.y;

        motionTime = planschTime();
    }

    if (!hidePanelSection() && (motionLast.d.y > hei))
    {
        clearArrow();
    }
    else
    {
#ifndef USE_OPENGL
        INTERVAL_RUN(50, showArrow (mousePos.d.x, mousePos.d.y));
        INTERVAL_RUN(50, showCircle (iPositionLast.d.x, iPositionLast.d.y));
#else
        showArrow (mousePos.d.x, mousePos.d.y);
        showCircle (iPositionLast.d.x, iPositionLast.d.y);
#endif
    }

#ifdef USE_OPENGL
    showGlCursor (mousePos.d.x, mousePos.d.y);
#endif

    if (((signed short)pEvent->motion.x < wid) && ((signed short)pEvent->motion.y < hei))
    {
        pEvent->motion.x = (signed short)pEvent->motion.x / zoom + zoomx;
        pEvent->motion.y = (signed short)pEvent->motion.y / zoom + zoomy;

        positionLast.d.x = (signed short)pEvent->motion.x;
        positionLast.d.y = (signed short)pEvent->motion.y;
    }

    if (stencilBitmap(NULL) && !drawingSelection())
    {
        if (!hidePanelSection() && (mousePos.d.y > hei))
        {
            INTERVAL_RUN (50, drawStencilSprite (NULL, wid/2, hei/2, 1));
        }
        else
        {
            INTERVAL_RUN (50, drawStencilSprite (NULL, positionLast.d.x, positionLast.d.y, 0));
        }
        drawingStencil = 1;
    }
    else if (drawingStencil)
    {
        drawStencilSprite (NULL, positionLast.d.x, positionLast.d.y, 1);
        drawingStencil = 0;
    }

    seekPipe (positionLast.d.x, positionLast.d.y, COL_LINE);

#if 0
    if (getDrawFct() == DO_PARTICLES)
        seekBorder (iPositionLast.d.x, iPositionLast.d.y, selectVector.d.x, selectVector.d.y);
#endif

    toolTipMove (mousePos.d.x, mousePos.d.y);

}

/* call back to be called if escape is pressed, or NULL to terminate the current handler.
 * If there is an existing escape handler and differs from the new one, call it 
 * before reassigning the new one 
 */
void escConnect (FUNCPTR h)
{
    if ((h != NULL) && (escHandler != NULL) && (escHandler != h))
        escHandler();

    escHandler = h;
}


int hidePanelSection()
{
#ifdef USE_OPENGL
    return hidePanel;
#else
    return 0;
#endif
}

int shiftPressed(void)
{
    return shift;
}

int ctrlPressed(void)
{
    return ctrl;
}



int handleInput (void)
{
    SDL_Event event;
    int capture;
    int ix=0, iy=0;
    int i;

    while (SDL_PollEvent (&event))
    {
        processCalloutQueue();

        capture = keyCaptureActive();

        switch (event.type)
        {
        case SDL_QUIT:
            return 1;

        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
            case SDLK_TAB:                   
                if (shiftPressed())
                    hidePanel = 1 - hidePanel;
                else
                    togglePanelGid();
                break;
               
            case SDLK_RSHIFT:
            case SDLK_LSHIFT:
                shift = 1;
                movement = hei / (zoom*2);
                break;
            case SDLK_RCTRL:
            case SDLK_LCTRL:
                ctrl = 1;
                break;
            default:
                keyDownCapture (event.key.keysym.sym, shift, ctrl);
                break;
            }
            break;
        case SDL_KEYUP:
            if (capture)
            {
                break;
            }

            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                if (escHandler)
                    escHandler();

                keyCapture (NULL, NULL);

                escHandler = NULL;
                break;

            case '?':
            case SDLK_F1:
                showHelp (mousePos.d.x, mousePos.d.y);
                break;

            case 'k':
                reorderParticles();
                break;

            case 'j':
                randomizeParticles();
                break;

            case '#':
                lockBorders = 1 - lockBorders;

                break;

            case ' ':
                freezeParticles = 1 - freezeParticles;

                if (freezeParticles)
                {
                    freezeTime();
                    wasThrottled = throttle;
                    throttle = 0;
                }
                else
                {
                    unfreezeTime();
                    throttle = wasThrottled;
                }

                startParticleMovement (!freezeParticles);

                break;

            case 't':
                if (freezeParticles)
                    break;

                throttle = 1 - throttle;

                if (!throttle)
                    startParticleMovement (1);

                break;


            case 'f':
                {
                    SDL_Rect r;
                    r.x = zoomx;
                    r.y = zoomy;
                    r.w = wid/zoom;
                    r.h = hei/zoom;

                    saveSelection (&r, 1, 1);
                }

                break;

            case 'v':
                modifySetting (&hideParticles, 1 - GETVAL32(hideParticles), 0);
                break;

            case 'a':
                zoomOut();
                break;
            case 's':
                zoomIn();
                break;
            case SDLK_RSHIFT:
            case SDLK_LSHIFT:
                shift = 0;
                shiftReleased = 1;
                movement = 1;
                break;
            case SDLK_RCTRL:
            case SDLK_LCTRL:
                ctrl = 0;
                break;
            case 'q':
                {
                    static int last = 0;

                    if ((planschTime() - last) < 500)
                        return 1;

                    last = planschTime();
                    break;
                }

            case 'x':
                {
                    static int last = 0;

                    if ((planschTime() - last) < 500)
                    {
                        removeAllParticles();
#ifdef OPENCL
                        clSchedClearAllParticles();
#endif
                    }

                    last = planschTime();

                    break;        
                }

            case '8':
            case ',':
                i = MAX(1, GETVAL32(maxFps) - (shiftPressed() ? 10 : 1));

                modifySetting (&maxFps, i, 0);
                
                break;

            case '9':
            case '.':
                i = GETVAL32(maxFps) + (shiftPressed() ? 10 : 1);

                modifySetting (&maxFps, i, 0);

                break;

            case '\'':
                modifySetting (&updateInterval, 
                           GETVAL32(updateInterval) + (shiftPressed() ? 10 : 1), 0);

                break;

            case ';':
                modifySetting (&updateInterval, 
                           GETVAL32(updateInterval) - (shiftPressed() ? 10 : 1), 0);

                break;


            case 'l':
                redrawScreen();
                break;

            case 'r':
                randomPlacement = 1 - randomPlacement;

                statText (3, randomPlacement ? "random" : "fixed");
                break;

            case '1':
                setPartsPerPixel (1);

                statText (3, "1 parts per pixel");

                break;

            case '2':
                setPartsPerPixel (4);
                statText (3, "4 parts per pixel");
                break;

            case '3':
                setPartsPerPixel (16);
                statText (3, "16 parts per pixel");
                break;

            case 'o':
                overwrite = 1 - overwrite;

                statText (3, overwrite ? "overwrite:ON" : "overwrite:OFF");
                break;

            case 'b':
                toggleBorder();
                break;

            case 'm':
                mp_disable = 1 - mp_disable;
                thread_aff_change++;
                break;


#ifdef ZOOM
            case SDLK_LEFT:
                if (zoomx - movement >= 0)
                    zoomx -= movement;
                else
                    zoomx = 0;
                redrawScreen();
                break;

            case SDLK_RIGHT:
                if (zoomx + wid/zoom + movement <= wid)
                    zoomx += movement;
                else
                    zoomx = wid - wid/zoom;
                redrawScreen();
                break;

            case SDLK_UP:
                if (zoomy - movement >= 0)
                    zoomy -= movement;
                else
                    zoomy = 0;
                redrawScreen();
                break;

            case SDLK_DOWN:
                if (zoomy + hei/zoom + movement <= hei)
                    zoomy += movement;
                else
                    zoomy = hei - hei/zoom;
                redrawScreen();
                break;
#endif
            default:
                break;
            }
            break;

        case SDL_MOUSEBUTTONDOWN:
            mouseMotion (&event, &ix, &iy);
    
            if ((event.button.button == 1) && !mouseButtonPressed(1))
            {
                if (handleButton (MKEVENT(BUTTON_PRESS,1), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }

                hideArrow (1);
                drawOperation (ix, iy, MKEVENT(BUTTON_PRESS, 1));
            }
            else if ((event.button.button == 2) && !mouseButtonPressed(2))
            {
                if (handleButton (MKEVENT(BUTTON_PRESS,2), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }

#ifdef USE_OPENCL
                if (shiftPressed())
                {
                    siMarkClear();
                    markPixels (SCREENC(ix), SCREENC(iy));
                    clParticleFollow (0, ix, iy);
                    siMarkClear();
                }
                else
#endif
                {
#ifdef USE_OPENCL
                    clParticleFollow (1, 0, 0);
#endif
                    doPan (&mousePos, 0);
                }

                drawOperation (ix, iy, MKEVENT(BUTTON_PRESS, 2));
            }
            else if ((event.button.button == 3) && !mouseButtonPressed(3))
            {
                if (handleButton (MKEVENT(BUTTON_PRESS,3), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }

                drawOperation (ix, iy, MKEVENT(BUTTON_PRESS, 3));

            }

            break;

        case SDL_MOUSEBUTTONUP:
            mouseMotion (&event, &ix, &iy);

            if ((event.button.button == 1) && mouseButtonPressed(1))
            {
                if (handleButton (MKEVENT(BUTTON_RELEASE,1), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }

                drawOperation (ix, iy, MKEVENT(BUTTON_RELEASE,1));

                hideArrow (0);
            }
            else if (event.button.button == 2)
            {        
                doPan (NULL, 1);

                if (handleButton (MKEVENT(BUTTON_RELEASE,2), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }

                drawOperation (ix, iy, MKEVENT(BUTTON_RELEASE,2));

            }
            else if (event.button.button == 3)
            {
                if (handleButton (MKEVENT(BUTTON_RELEASE,3), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }

                drawOperation (ix, iy, MKEVENT(BUTTON_RELEASE,3));

            }
            else if (event.button.button == 4)
            {
                if (!handleButton (MKEVENT(SCROLLUP,1), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    if (shift)
                    {
                        drawOperation (ix, iy, MKEVENT(SCROLLUP_SH,1));
                    }
                    else if (ctrl)
                    {
                        drawOperation (ix, iy, MKEVENT(SCROLLUP_CT,1));
                    }
                    else
                    {
                        zoomIn();
                        event.motion.x = mousePos.d.x + CORR_X;
                        event.motion.y = mousePos.d.y + CORR_X;
                        mouseMotion (&event, NULL, NULL);
                        drawStencilSprite (NULL, positionLast.d.x, positionLast.d.y, 0);
                    }
                }
            }
            else if (event.button.button == 5)
            {
                if (!handleButton (MKEVENT(SCROLLDOWN,1), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    if (shift)
                    {
                        drawOperation (ix, iy, MKEVENT(SCROLLDOWN_SH,1));
                    }
                    else if (ctrl)
                    {
                        drawOperation (ix, iy, MKEVENT(SCROLLDOWN_CT,1));
                    }
                    else
                    {
                        zoomOut();
                        event.motion.x = mousePos.d.x + CORR_X;
                        event.motion.y = mousePos.d.y + CORR_X;
                        mouseMotion (&event, NULL, NULL);
                        drawStencilSprite (NULL, positionLast.d.x, positionLast.d.y, 0);
                    }
                }
            }

            break;

        case SDL_MOUSEMOTION:
            mouseMotion (&event, &ix, &iy);

#ifdef USE_OPENGL
            if (!options->useGlCursor)
#endif
            {
                if (!hidePanelSection() && (((signed short)event.motion.y > hei) || ((signed short)event.motion.x > wid)))
                {
                    if (SDL_GetCursor () != dflCursor)
                        SDL_SetCursor (dflCursor);

                }
                else
                {
                    if (SDL_GetCursor () != cursor)
                        SDL_SetCursor (cursor);
                }
            }

            if (mouseButtonPressed(1) || (motionCaptureButton == 1))
            {
                if (handleButton (MKEVENT(BUTTON_MOVE,1), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }
                else 
                {
#if 0
                    if ((signed short)event.motion.y >= hei)
                    {
                        event.motion.x = (signed short)event.motion.x / zoom + zoomx;
                        event.motion.y = (signed short)(hei-1) / zoom + zoomy;
                    }
#endif

                    drawOperation (ix, iy, MKEVENT(BUTTON_MOVE, 1));

                }
            }

            if (mouseButtonPressed(2) || (motionCaptureButton == 2))
            {

                if (handleButton (MKEVENT(BUTTON_MOVE,2), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }
                else
                {
                    drawOperation (ix, iy, MKEVENT(BUTTON_MOVE, 2));
                }

            }

            if (mouseButtonPressed(3) || (motionCaptureButton == 3))
            {
                if (handleButton (MKEVENT(BUTTON_MOVE,3), (signed short)event.motion.x, (signed short)event.motion.y))
                {
                    break;
                }
                else
                {
                    drawOperation (ix, iy, MKEVENT(BUTTON_MOVE, 3));
                }

#if 0                   
                collTest ((signed short)event.motion.x, (signed short)event.motion.y);
#endif

#if 0
                doHeat ((signed short)event.motion.x, (signed short)event.motion.y, 0, 0);
#endif
            }

            if (!mouseButtonPressed(1) && !mouseButtonPressed(2) && !mouseButtonPressed(3))
            {
                handleButton (MKEVENT(BUTTON_MOVE,0), (signed short)event.motion.x, (signed short)event.motion.y);
            }
            break;
        }
    }

    return 0;
}
