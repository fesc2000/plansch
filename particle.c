/*
Copyright (C) 2008-2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

ASETTING(particleSize,20)  = 
{{ 
    .defaultValue = 255,
    .minValue = 0,
    .maxValue = 255,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Size"
}};

ASETTING(particleWeight,20)  = 
{{ 
    .minValue = 1,
    .maxValue = 255,
    .defaultValue = 75,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Weight"
}};

ASETTING(particleAttractionForce,20)  = 
{{ 
    .minValue = -1,
    .maxValue = 1,
    .defaultValue = 1,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .discrete = 1,
    .numDiscrete = 3,
    .valueArray = { -1, 0, 1},
    .colorSelection = 1,
    .name = "Charge"
}};

ASETTING(particleForceInv,20)  = 
{{ 
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "ForceInvert"
}};


#define LT_FORMULA(L)        RND((L)*(L)/10)
ASETTING(particleLifetime,20)  = 
{{ 
    .minValue = 10,
    .maxValue = 1000,
    .defaultValue = 1000,
    .minDiff = 1,
    .canBeRandom = 1,
    .partAttr = 1,
    .name = "Lifetime"
}};

SETTING(particleLifetimeLambda)  = 
{ 
    .minValue = 1,
    .maxValue = 100,
    .defaultValue = 1,
    .minDiff = 1,
    .partAttr = 0,
    .canBeRandom = 1,
    .name = "Lifetime L"
};

ASETTING(wimpPair,20) =
{{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Pair New Particle with WIMP",
}};

ASETTING(repulseChained,20) =
{{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Attract or repulse connected particle",
}};


struct newPart
{
    vec_t                 pos;
    vec_t                 vec;

    signed char                 charge;
    unsigned char         mass;
    unsigned char         type;
    unsigned char         size;
    int                         lifetime;
    particle                *linkto;
    int                         count;
    uint32_t                 color;
};

static int ntype = -1;

int numNewParticles = 0;
struct newPart *createParticleList = NULL;
intptr_t removeParticleCb (particle *p, intptr_t arg);
intptr_t decrAndRemoveCb (particle *p, intptr_t arg);

static FT_TYPE *newPartFlags = NULL;
static FT_TYPE *dying = NULL;
FT_TYPE *alive = NULL;

static callback_t *createCbList = NULL;
static callback_t *removeCbList = NULL;


signed char oscArray[128][256];

particle *partArray;
particle *freeList;
#ifndef PID_IN_PSTRUCT
particle **prevTbl;
#endif

particle2 *partArray2;

pdesc *curPd = NULL;
int curPi = 0;

static particle dummy;

particle *dummyPart(void)
{
    return &dummy;
}

int isDummy(particle *p)
{
    return (p == &dummy);
}
    

void initOscArray()
{
    int amp, p;

    for (amp = 0; amp < 128; amp++)
    {
        for (p = 0; p < 256; p++)
        {
            oscArray[amp][p] = (signed char)(cos (DEG2RAD((FLOAT)p * (360.f / 256.f))) * (FLOAT)amp);
        }
    }
}

void activatePtype (int pt, int activate)
{
    if (pt == -1)
    {
        pt = getValue(&ptype);
    }

    if (pdescs[pt]->activate)
        pdescs[pt]->activate (activate);
}

void setPtypeCb (setting *s)
{
    static int oldPanel = -1;
    int pt = GETVAL32(*s);

    if (oldPanel != -1)
    {
        activatePtype (oldPanel, 0);

        if (pdescs[oldPanel]->hasPanel)
        {
            showPanel (pdescs[oldPanel]->pPanel, 0);
        }
    }

    if (pdescs[pt]->hasPanel)
    {
        showPanel (pdescs[pt]->pPanel, 1);
        oldPanel = pt;
    }
    else
    {
        oldPanel = -1;
    }

    showMpObjectButton (pdescs[pt]->mpObject);

    curPd = pdescs[pt];
    curPi = pt;

    activatePtype (pt, 1);

    drawOpSelect (DO_PARTICLES);
}



void registerRemoveCallback (FUNCPTR fct, void *arg)
{
    callback_t *cb =
        malloc(sizeof(callback_t));
    
    cb->fct = fct;
    cb->arg = arg;

    cb->next = removeCbList;
    removeCbList = cb;
}

void broadcastRemove (particle *p)
{
    callback_t *cb;

    for (cb = removeCbList; cb != NULL; cb = cb->next)
    {
        cb->fct (p, cb->arg);
    }
}
        
void registerCreateCallback (FUNCPTR fct, void *arg)
{
    callback_t *cb =
        malloc(sizeof(callback_t));
    
    cb->fct = fct;
    cb->arg = arg;

    cb->next = createCbList;
    createCbList = cb;
}

void broadcastCreate (particle *p)
{
    callback_t *cb;

    for (cb = createCbList; cb != NULL; cb = cb->next)
    {
        cb->fct (p, cb->arg);
    }
}
        

int isDeleted(particle *p)
{
    return IS_DELETED(p);
}

void initParticles ()
{
    int i;

    newPartFlags = flagFieldAlloc();

    dying = flagFieldAlloc();
    alive = flagFieldAlloc();

    createParticleList = malloc(options->maxParticles * sizeof(*createParticleList));
    numNewParticles = 0;

#ifndef PID_IN_PSTRUCT
    prevTbl = calloc(sizeof(particle) * options->maxParticles, 1);
#endif

    freeList = partArray;
    for (i = 0; i < options->maxParticles-1; i++)
    {
        partArray[i].next = &partArray[i+1];
        SET_DELETED (&partArray[i]);
    }
    SET_DELETED (&partArray[i]);

    initOscArray();

}

void setNewParticle (particle *p)
{
    FLAG_SET(newPartFlags, p);
}

void clearNewParticle (particle *p)
{
    if (newPartFlags == NULL)
    {
        return;
    }

    FLAG_CLR(newPartFlags, p);
}

int hasNewParticle (screeninfo *pSi)
{
    particle *p;

    if (newPartFlags == NULL)
        return 0;

    if (pSi->particleList <= SCR_BRICK)
        return 0;

    for (p = pSi->particleList;  p != NULL; p = p->next)
    {
        if (FLAG_ISSET(newPartFlags, p))
            return 1;
    }
    return 0;
}

void clearNewParticleList (void)
{
    FLAG_CLRALL(newPartFlags);
}

void warpParticles (screeninfo *oldSi, screeninfo *newSi)
{
    int srcThread, dstThread;
    particle *p, *pp;
    int        px, py;

    px = INTERNC(SI2X(newSi));
    py = INTERNC(SI2Y(newSi));

    p = oldSi->particleList;

    if (p <= SCR_BRICK)
        return;

    /* update particles for new pixel */
    for (pp = p;; pp = pp->next)
    {
        pp->pos.d.x = px + pp->pos.d.x % GRID;
        pp->pos.d.y = py + pp->pos.d.y % GRID;

        srcThread = threadGet (PTYPE(pp), SI2X(oldSi), SI2Y(oldSi));
        dstThread = threadGet (PTYPE(pp), SI2X(newSi), SI2Y(newSi));

        SET_SCREENINFO (pp, newSi);

        if (pushAddsImpuls)
        {
#if 0
            pp->vec.d.x += dx * 100;
            pp->vec.d.y += dy * 100;
#else
            pp->vec.d.x += motionVec.d.x;
            pp->vec.d.y += motionVec.d.y;
#endif
        }

        /* if destination pixel crosses a thread work area boundary,
         * assign them to new thread
         */
        if (srcThread != dstThread)
        {
            removePFromThread (srcThread, pp);
            addPToThread (dstThread, pp);
        }

        if (pp->next == NULL)
            break;
    }

    /* chain destination pixels at end of current pixel list */
    pp->next = newSi->particleList;
    if (pp->next)
    {
        PREV_SET(pp->next, pp);
    }

    newSi->particleList = p;

    SI_CHARGE(newSi) += SI_CHARGE(oldSi);
    SI_CHARGE(oldSi) = 0;

    oldSi->particleList = NULL;
}

int warpParticle (screeninfo *oldSi, screeninfo *newSi, particle *p)
{
    particle *prev;
    if (newSi->particleList == SCR_BRICK)
        return 0;

    if (pdescs[PTYPE(p)]->isWimp)
    {
        oldSi->wimps--;
        newSi->wimps++;
        SI_CHARGE(oldSi) -= PCHARGE(p);
    }
    else
    {
        /* unlink particle from old pixel */
        prev = PREV_GET(p);
        if (prev)
        {
            prev->next = p->next;
            if (p->next)
            {
                PREV_SET(p->next, prev);
            }
        }
        else
        {
            oldSi->particleList = p->next;
            if (p->next)
            {
                PREV_SET(p->next, NULL);
                SI_CHARGE(oldSi) -= PCHARGE(p);
            }
            else
            {
                SI_CHARGE(oldSi) = 0;
            }
        }

        p->next = newSi->particleList;
        PREV_SET(p, NULL);
        if (p->next)
        {
            PREV_SET(p->next, p);
        }
        newSi->particleList = p;
    }
    SI_CHARGE(newSi) += PCHARGE(p);

    return 1;
}

void unlinkFromPixel (particle *p, screeninfo *pSi)
{
        particle *prev = PREV_GET(p);

        /* unlink particle from old pixel */
        if (prev)
        {
            prev->next = p->next;
            if (p->next)
            {
                PREV_SET(p->next, prev);
            }
        }
        else
        {
            pSi->particleList = p->next;
            if (p->next)
            {
                PREV_SET(p->next, NULL);
            }
        }
}

void removeParticle (particle *p, uint flags )
{
    screeninfo *pSi;
    int i;
    int type;
    int wimp;
    
    if (!p)
        return;

    if (IS_DELETED(p))
    {
        printf ("Particle %d deleted twice!\n", (int)PARTICLE_ID(p));
        BACKTRACE
        return;
    }


    type = PTYPE(p);
    wimp = pdescs[type]->isWimp;

    if (pdescs[type]->unsetType)
        pdescs[type]->unsetType (p);

    pSi = GET_SCREENINFO(p);

    if (!wimp)
    {
        particle *prev = PREV_GET(p);

        /* unlink particle from old pixel */
        if (prev)
        {
            prev->next = p->next;
            if (p->next)
            {
                PREV_SET(p->next, prev);
            }
        }
        else
        {
            pSi->particleList = p->next;
            if (p->next)
            {
                PREV_SET(p->next, NULL);
            }
        }
    }
        
    if ((flags & NO_THREADLIST) == 0)
    {
        for (i = 0; i < options->numThreads; i++)
        {
            if (removePFromThread (i, p))
            {
                tData[i]->v.partCount--;
                break;
            }
        }
    }

#pragma omp critical
    {
        p->next = freeList;
        freeList = p;
        numParticles--;

        unlinkAll (p);

        removeElementFromMoveList ((void*)p);

        removeParticleFromTape (&playbackTape, p);
        recordRemoveParticle (&playbackTape, p);

        unschedParticle (p);
        clearNewParticle (p);
        markRemoveCb (p);

        broadcastRemove (p);

        if (pdescs[type]->isWimp)
        {
            pSi->wimps--;
        }

        if (flags & DENS_UPDATE)
        {
            if ((pSi->particleList == NULL) && (pSi->wimps == 0))
            {
                SI_CHARGE(pSi) = 0;
            }
            else
            {
                SI_CHARGE(pSi) -= PCHARGE(p);
            }
        }

        FLAG_CLR(dying, p);

        SET_DELETED(p);
    }
}

void deleteParticles (void)
{
    particle2 *p2;
    int              t;
    threadData *td;

    for (t = 0; t < options->numThreads; t++)
    {
        td = tData[t];

        for (p2 = td->deleteList; p2 != NULL; p2 = p2->del_next)
        {
            removeParticle (p2->p, DENS_UPDATE);
        }
        td->deleteList = NULL;
    }
}

particle *
newParticle (uint32 x, uint32 y, int vx, int vy)
{
    int sx = SCREENC (x);
    int sy = SCREENC (y);
    particle *p;
    particle2 *p2;
    screeninfo *newSi;
    int pt = getValue (&ptype);
    int lifetime = getValue(&particleLifetime[curPi]);
    int thread = 0;

    if (pdescs[pt]->newParticle)
    {
        int charge = (int)getValue (&particleAttractionForce[pt]);

        return pdescs[pt]->newParticle (x, y, vx, vy,
                (int)getValue (&particleSize[pt]),
                charge,
                (int)getValue (&particleWeight[pt]),
                (int)getValue (&particleForceInv[pt]),
                getColorSetting (pt, charge));
    }

    thread = threadGet (pt, sx, sy);

    if (thread == -1)
        return NULL;

    if (pdescs[pt]->canCreate)
        if (!pdescs[pt]->canCreate(x, y))
            return NULL;

    newSi = &siArray[sx + sy * wid];

    if ((newSi->particleList == SCR_BRICK) ||
        (newSi->flags & SI_LINE))
        return NULL;

    p = freeList;

    if (p == NULL)
        return NULL;

    freeList = p->next;

    memset ((char*)p, 0, sizeof(*p));

    p2 = &partArray2[PARTICLE_ID(p)];
    memset ((char*)p2, 0, sizeof(*p2));

    p->pos.d.x = x;
    p->pos.d.y = y;
    p->vec.d.x = vx;
    p->vec.d.y = vy;
    PREV_SET(p, NULL);

    p2->lpos = p->pos;
    p2->p = p;

    if (pdescs[pt]->isWimp)
    {
        newSi->wimps++;
    }
    else
    {
        p->next = newSi->particleList;
        if (p->next)
        {
            PREV_SET(p->next, p);
        }
        newSi->particleList = p;
        SET_SCREENINFO (p, newSi);
    }

    SET_CHARGE(p, getValue (&particleAttractionForce[pt]));
    SET_FRC_INV(p, getValue (&particleForceInv[pt]));
    SET_MASS(p, getValue (&particleWeight[pt]));
    SET_PTYPE(p, pt);
    p->size = getValue (&particleSize[pt]);
    SET_ALIVE(p);

    setParticleColorARGB (p, getColorSetting (PTYPE(p), CHARGE_SIGN(p)), 1, 0xffffffff);

    int location = 0;

    /* add particle to thread's particle list. To maintain some indeterminism,
     * insert it into a random location within the threads's work array. 
     * The current particle at the insertion point goes to the end of the list.
     */
    
    if (tData[thread]->numParticles)
    {
        location = rand() % tData[thread]->numParticles;
        tData[thread]->particleList[tData[thread]->numParticles] = tData[thread]->particleList[location];
    }

    tData[thread]->particleList[location] = PARTICLE_ID(p);

    tData[thread]->numParticles++;

    linkPartReset (p);

    numParticles++;
 
    if (pdescs[pt]->setType)
    {
        if (!pdescs[pt]->setType (p, newSi))
        {        
            removeParticle (p, 0);
            return NULL;
        }
    }

    SI_CHARGE(newSi) += PCHARGE(p);

    recordNewParticle (&playbackTape, p);

    if (lifetime < particleLifetime[pt].maxValue)
    {
        int lt;

#if 0
        lt = lifetime * fabs (ran_gaussian (getValue (&particleLifetimeLambda))) + 1;
#else
        lt = LT_FORMULA (lifetime);
#endif

        schedParticle (p, lt, (FUNCPTR)removeParticleCb, lifetime);
    }

    broadcastCreate (p);

    return p;
}

particle *
newParticle2 (uint32 x, uint32 y, int vx, int vy, int type, int charge, int mass, int size, uint32_t color)
{
    int sx = SCREENC (x);
    int sy = SCREENC (y);
    particle *p;
    particle2 *p2;
    screeninfo *newSi;
    int lifetime = getValue(&particleLifetime[type]);
    int thread = 0;

    if (pdescs[type]->newParticle)
    {
        return pdescs[type]->newParticle (x, y, vx, vy,
                size,
                charge,
                mass,
                getValue (&particleForceInv[type]),
                color
                );
    }

    thread = threadGet (type, sx, sy);

    if (thread == -1)
    {
        return NULL;
    }

    if (pdescs[type]->canCreate)
        if (!pdescs[type]->canCreate(x, y))
            return NULL;

    newSi = &siArray[sx + sy * wid];

    if ((newSi->particleList == SCR_BRICK) ||
        (newSi->flags & SI_LINE))
        return NULL;

    p = freeList;

    if (p == NULL)
        return NULL;

    freeList = p->next;

    memset ((char*)p, 0, sizeof(*p));

    p2 = &partArray2[PARTICLE_ID(p)];
    memset ((char*)p2, 0, sizeof(*p2));

    p->pos.d.x = x;
    p->pos.d.y = y;
    p2->lpos = p->pos;
    p->vec.d.x = vx;
    p->vec.d.y = vy;

    if (pdescs[type]->isWimp)
    {
        newSi->wimps++;
    }
    else
    {
        p->next = newSi->particleList;
        PREV_SET(p, NULL);
        if (p->next)
        {
            PREV_SET(p->next, p);
        }
        newSi->particleList = p;
        SET_SCREENINFO (p, newSi);
    }

    if (mass == 0)
    {
        printf ("newParticle2: bad mass!\n");
        mass = 1;
    }
    p->charge = charge;
    p->frcInv = 1;
    SET_MASS(p, mass);
    SET_PTYPE(p, type);
    p->size = size;
    SET_ALIVE(p);

    p2->p = p;

    defaultParticleColor(p);

//    COLOR (sx, sy, p, newSi);

    tData[thread]->particleList[tData[thread]->numParticles] = PARTICLE_ID(p);
    tData[thread]->numParticles++;

    linkPartReset (p);

    numParticles++;
 
    if (pdescs[type]->setType)
    {
        if (!pdescs[type]->setType (p))
        {        
            removeParticle (p, 0);
            return NULL;
        }
    }

    SI_CHARGE(newSi) += PCHARGE(p);

    recordNewParticle (&playbackTape, p);

    if (lifetime < particleLifetime[type].maxValue)
    {
        int lt;


#if 0
        lt = lifetime * fabs (ran_gaussian (getValue (&particleLifetimeLambda))) + 1;
#else
        lt = LT_FORMULA (lifetime);
#endif

        schedParticle (p, lt, (FUNCPTR)removeParticleCb, lifetime);
    }

    broadcastCreate (p);

    return p;
}

void createParticleAsync (vec_t *pos, 
                vec_t *vec,        
                int charge, 
                int mass, 
                int type, 
                int size,
                int lifetime,
                uint32_t color,
                      particle *linkto, 
                int count)
{
    if (!count)
        return;

#pragma omp critical
    {
        if (numNewParticles < options->maxParticles)
        {
            createParticleList[numNewParticles].pos = *pos;
            createParticleList[numNewParticles].vec = *vec;
            createParticleList[numNewParticles].charge = charge;
            createParticleList[numNewParticles].mass = mass;
            createParticleList[numNewParticles].type = type;
            createParticleList[numNewParticles].size = size;
            createParticleList[numNewParticles].lifetime = lifetime;
            createParticleList[numNewParticles].linkto = linkto;
            createParticleList[numNewParticles].count = count;
            createParticleList[numNewParticles].color = color;

            numNewParticles++;
        }
    }
}

void cloneParticleAsync (particle *p, int mass, int vx, int vy)
{
    vec_t v;

    v.d.x = vx;
    v.d.y = vy;

    createParticleAsync (&p->pos, &v, p->charge, mass, p->type, p->size, 0, getParticleColorARGB(p), NULL, 1);
}

void cloneParticleAsync2 (particle *p, int mass, int lifetime, int vx, int vy)
{
    vec_t v;

    v.d.x = vx;
    v.d.y = vy;

    createParticleAsync (&p->pos, &v, p->charge, mass, p->type, p->size, lifetime, getParticleColorARGB(p), NULL, 1);
}

void createAsyncParticles()
{
    int i, j;
    particle *p;

    for (i = 0; i < numNewParticles; i++)
    {
        for (j = 0; j < createParticleList[i].count; j++)
        {
            p = newParticle2 (createParticleList[i].pos.d.x, 
                                    createParticleList[i].pos.d.y, 
                                    0, 0,
                                    createParticleList[i].type, 
                                    createParticleList[i].charge, 
                                    createParticleList[i].mass, 
                                    createParticleList[i].size,
                                    createParticleList[i].color); 

            if (p && !isDummy(p))
            {
                p->vec = createParticleList[i].vec;

                if (createParticleList[i].lifetime)
                {
                    schedParticle (p, 20, (FUNCPTR)removeParticleCb, 0);
                }

                if (createParticleList[i].linkto)
                {
                    if (pdescs[PTYPE(p)]->link)
                        pdescs[PTYPE(p)]->link (p, createParticleList[i].linkto);
                }
            }
        }
    }
    numNewParticles = 0;
}

void applySettingToParticle (particle *p, setting *s)
{
    int val = getValue (s);
    int pt = PTYPE(p);

    if (pdescs[pt]->applySetting)
        pdescs[pt]->applySetting (p, s);
                
    if (s == &particleSize[pt])
    {
        p->size = val;
    }
    else if (s == &particleWeight[pt])
    {
        SET_MASS(p, val);
    }
    else if (s == &particleAttractionForce[pt])
    {
        SET_CHARGE(p, val);
    }
    else if (s == &particleForceInv[pt])
    {
        SET_FRC_INV(p, val);
    }
    else if (s == &ptype)
    {
        if (pt == val)
            return;

        convertParticleType (p, GETVAL32(ptype));
        
    }
    else if (s == &particleLifetime[pt])
    {
        int lifetime = getValue (s);
        unschedParticle (p);

        if (lifetime < s->maxValue)
        {
            int lt;

#if 0
            lt = lifetime * fabs (ran_gaussian (getValue (&particleLifetimeLambda))) + 1;
#else
            lt = LT_FORMULA (lifetime);
#endif

            schedParticle (p, lt, (FUNCPTR)removeParticleCb, lifetime);
        }
    }
    else
    {
        return;
    }

}


intptr_t removeParticleCb (particle *p, intptr_t arg)
{
    removeParticle (p, DENS_UPDATE);

    return 0;
}

intptr_t decrAndRemoveCb (particle *p, intptr_t arg)
{
    screeninfo *pSi = GET_SCREENINFO(p);
    int lt;

    if (p->mass == 1)
    {
        removeParticle (p, DENS_UPDATE);
        return 0;
    }
    SI_CHARGE(pSi) -= PCHARGE(p);
    p->mass--;
    SI_CHARGE(pSi) += PCHARGE(p);


#if 0
    lt = (int)arg * fabs(ran_gaussian(1.0)) + 1;
#else
    lt = arg;
#endif

    schedParticle (p, lt, (FUNCPTR)decrAndRemoveCb, arg);

    return 0;
}

void removeParticleDelayed(particle *p)
{
    int lt;



    if (FLAG_ISSET(dying, p))
        return;

    FLAG_SET(dying, p);
    unschedParticle(p);

#if 0
    lt = 30 * fabs(ran_gaussian(1.0)) + 1;
    if (lt > 50)
        lt = 50;
#else
    lt = RND(50);
#endif

    schedParticle (p, lt, (FUNCPTR)removeParticleCb, 0);
}

void neutrinoType (int type)
{
    ntype = type;
}

int getNeutrinoType ()
{
    return ntype;
}

void convertParticleType (particle *p, int t)
{
    int oldType;

    if (PTYPE(p) == t)
        return;

    if (pdescs[t]->canCreate)
    {   
        /* before checking whether a particle can be created here, change the current
         * particle type. e.g. chain particles might refuse to be generated if there
         * are particles of other types on the same pixel
         */
        oldType = PTYPE(p);
        SET_PTYPE(p, t);
        if (!pdescs[t]->canCreate(p->pos.d.x, p->pos.d.y))
        {
            SET_PTYPE(p, oldType);
            return;
        }
        SET_PTYPE(p, oldType);
    }

    /* wimp types can not be converted without destroying the old one
     */
    if (pdescs[PTYPE(p)]->isWimp != pdescs[t]->isWimp)
    {
        particle tp = *p;
        pLinks plSaved, *pl;
        uint32 col = getParticleColorARGB(p);

        pl = linkPartners (p);
        if (pl)
                plSaved = *pl;

        removeParticle (p, DENS_UPDATE);

        p = newParticle2 (tp.pos.d.x, tp.pos.d.y, tp.vec.d.x, tp.vec.d.y, t, tp.charge, tp.mass, tp.size, col);

        if (p && !isDummy(p))
        {
            setLinkPartners (p, &plSaved);
        }
    }
    else
    {
        if (pdescs[PTYPE(p)]->unsetType)
            pdescs[PTYPE(p)]->unsetType (p);

        SET_PTYPE(p, GETVAL32(ptype));

        if (pdescs[PTYPE(p)]->setType)
            pdescs[PTYPE(p)]->setType (p);
    }
}

void walkAllParticles (FUNCPTR cb, intptr_t arg)
{
    int thread;
    int i;

    for (thread = 0; thread < options->numThreads; thread++)
    {
        for (i = tData[thread]->numParticles; i >= 0; i--)
        {
            cb (&partArray[tData[thread]->particleList[i]], arg);;
        }
    }
}

void
removeAllParticles ()
{
    int        i;
    int marked = particlesMarked();
    int selection = haveSelection();
    int thread;
    particle *p;

#ifdef USE_OPENCL
    clSchedClearAllParticles();
#endif
    
    for (thread = 0; thread < options->numThreads; thread++)
    {
        for (i = tData[thread]->numParticles-1; i >= 0; i--)
        {
            p = &partArray[tData[thread]->particleList[i]];

            if (marked && !pIsMarked(p))
                continue;

            if (selection && !inSelection (SCREENC(p->pos.d.x), SCREENC(p->pos.d.y)))
                continue;

            removeParticle (p, DENS_UPDATE);
        }
    }
}

/* check whether particles are of same type (ptype and particle spec. comparison)
 * returns 1 if yes, 0 if not
 */
int compareParticle (particle *p1, particle *p2)
{
    if (PTYPE(p1) != PTYPE(p2))
        return 0;

    if (pdescs[PTYPE(p1)]->compare)
        if (!pdescs[PTYPE(p1)]->compare (p1, p2))
            return 0;

    return 1;
}

int pdescFind (char *name)
{
    int i;

    for (i = 0; i < MAX_PDESCS; i++)
    {
        if (pdescs[i] == NULL)
            return -1;

        if (!strcmp (name, pdescs[i]->name))
            return i;
    }
    return -1;
}

particle *nearestNewParticle (int ix, int iy, int rad)
{
    particle *pp = NULL;
    particle *tpp;
    int64_t minSpq = MAX_SPEEDQ;
    unsigned int pRad, ppRad;
    screeninfo *pSi, *tmpSi;
    int64_t ppDistQ, spq;
    int i;
    int cx, cy;

    pSi = &siArray[SCREENC(ix) + SCREENC(iy) * wid];
    pRad = rad;

    CLEAR_MARKERS("+col");
    DBG_BOX(ix, iy, GRID/4, ARGB(0xffffff00), "+col");

    tmpSi = pSi;
    i = -1;

    while (1)
    {
        if (tmpSi->particleList > SCR_BRICK)
        {
            for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
            {
                if (!FLAG_ISSET(newPartFlags, tpp))
                    continue;
            
                ppRad = PART_RADIUS(tpp);
                ppDistQ = (QUAD_TYPE)(pRad + ppRad) * (QUAD_TYPE)(pRad + ppRad);

                cx = tpp->pos.d.x - ix;
                cy = tpp->pos.d.y - iy;

                spq = (int64_t)cx * (int64_t)cx + (int64_t )cy * (int64_t)cy;

                if (spq < ppDistQ)
                {
                    /* no need to check other particles if we find at least one which is within
                     * collission range
                     */
                    DBG_BOX(tpp->pos.d.x, tpp->pos.d.y, GRID/4, ARGB(0xffff0000), "+col");
                    return tpp;

                    if (spq < minSpq)
                    {
                        minSpq = spq;
                        pp = tpp;
                    }
                }
            }
        }
        i++;
        if (i >= 8)
            break;
        tmpSi = SI_DIR(pSi, i);
    }

    return pp;
}

particle *nearestParticle (int ix, int iy, int rad, FUNCPTR filter)
{
    particle *pp = NULL;
    particle *tpp;
    int64_t minSpq = MAX_SPEEDQ;
    unsigned int pRad, ppRad;
    screeninfo *pSi, *tmpSi;
    int64_t ppDistQ, spq;
    int i;
    int cx, cy;

    pSi = &siArray[SCREENC(ix) + SCREENC(iy) * wid];
    pRad = rad;

    CLEAR_MARKERS("+col");
    DBG_BOX(ix, iy, GRID/4, ARGB(0xffffff00), "+col");

    tmpSi = pSi;
    for (i = -1; i < 8;)
    {
        if (tmpSi->particleList <= SCR_BRICK)
        {
            i++;
            tmpSi = SI_DIR(pSi, i);
            continue;
        }
        
        for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
        {
            if (filter && filter (tpp))
                continue;
        
            ppRad = PART_RADIUS(tpp);
            ppDistQ = (QUAD_TYPE)(pRad + ppRad) * (QUAD_TYPE)(pRad + ppRad);

            cx = tpp->pos.d.x - ix;
            cy = tpp->pos.d.y - iy;

            spq = (int64_t)cx * (int64_t)cx + (int64_t )cy * (int64_t)cy;

            if (spq < ppDistQ)
            {
#if 0
                /* no need to check other particles if we find at least one which is within
                 * collission range
                 */
                DBG_BOX(tpp->pos.d.x, tpp->pos.d.y, GRID/4, ARGB(0xffff0000), "+col");
                return tpp;
#endif

                if (spq < minSpq)
                {
                    minSpq = spq;
                    pp = tpp;
                }
            }
        }
        i++;
        tmpSi = SI_DIR(pSi, i);
    }

    return pp;
}

void walkParticlesAt (int ix, int iy, int maxDist, FUNCPTR cb, void *cbArg)
{
    particle *tpp;
    screeninfo *pSi, *tmpSi;
    int64_t maxDistQ, spq;
    int i;
    int cx, cy;

    pSi = &siArray[SCREENC(ix) + SCREENC(iy) * wid];

    maxDistQ = (QUAD_TYPE)(maxDist) * (QUAD_TYPE)(maxDist);

    tmpSi = pSi;
    for (i = -1; i < 8;)
    {
        if (tmpSi->particleList <= SCR_BRICK)
        {
            i++;
            tmpSi = SI_DIR(pSi, i);
            continue;
        }
        
        for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
        {
            cx = tpp->pos.d.x - ix;
            cy = tpp->pos.d.y - iy;

            spq = (int64_t)cx * (int64_t)cx + (int64_t )cy * (int64_t)cy;

            if (spq < maxDistQ)
            {
                if (cb (tpp, cbArg))
                {
                    return;
                }
            }
        }
        i++;
        tmpSi = SI_DIR(pSi, i);
    }
}

typedef struct
{
    vec_t pos;
    int64_t minSpq;
    particle *nearest;
    FUNCPTR filter;
    void *arg;
} pfind_t;

static int findNearestCb (particle *p, void *arg)
{
    pfind_t *pf = (pfind_t*)arg;
    int cx, cy;
    int64_t spq;

    if (pf->filter && pf->filter (p))
        return 0;

    cx = p->pos.d.x - pf->pos.d.x;
    cy = p->pos.d.y - pf->pos.d.y;

    spq = (int64_t)cx * (int64_t)cx + (int64_t )cy * (int64_t)cy;

    if (spq < pf->minSpq)
    {
        pf->minSpq = spq;
        pf->nearest = p;
    }
    return 0;
}

/* find nearest particle under stencil
 * int filter(particle *)
 *      Return code 1 skips a particle
 */
particle *nearestParticleSt (int ix, int iy, FUNCPTR filter)
{
    pfind_t pf;
    
    pf.pos.d.x = ix;
    pf.pos.d.y = iy;
    pf.nearest = NULL;
    pf.minSpq = 0x7fffffffffffffffULL;
    pf.filter = filter;
    pf.arg = NULL;

    walkStencilParticles (ix, iy, (FUNCPTR)findNearestCb, (void*)&pf);

    return pf.nearest;
}

/* walk all particles under stencil
 * int cb(particle *, void *arg)
 *      Return code != 0 terminats search.
 *
 */
void walkStencilParticles(int ix, int iy, FUNCPTR cb, void *arg)
{
    int xx, yy, px, py;
    int sx;
    int sy;
    screeninfo *pSi, *tmpSi;
    particle *p;
    int i;
    int dx, dy;
    int wimps = 0;
    
    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = ix + INTERNC(xx - 16 + CORR_X);
            py = iy + INTERNC(yy - 16 + CORR_Y);
            sx = SCREENC(px);
            sy = SCREENC(py);
            
            if (!inSelection(sx, sy))
                continue;

            pSi = &siArray[sx + sy*wid];

            if (pSi->wimps)
            {
                wimps = 1;
                pSi->flags = SI_MARKER;
            }
            
            for (i = 0; i < 9; i++)
            {
                tmpSi = SI_QUAD(pSi, i);

                for (p = tmpSi->particleList; p > SCR_BRICK; p = p->next)
                {
                    dx = abs(p->pos.d.x - px);
                    dy = abs(p->pos.d.y - py);
                    
                    if ((dx <= GRID/2) && (dy <= GRID/2))
                    {
                        if (cb (p, arg) == 1)
                            return;
                    }
                }
            }
        }
    }
    
    if (!wimps)
        return;

    /* process all WIMPs ..*/
    {
        int thread;
        int i;

        for (thread = 0; thread < options->numThreads; thread++)
        {
            for (i = tData[thread]->numParticles-1; i >= 0; i--)
            {
                p = &partArray[tData[thread]->particleList[i]];

                if (!pdescs[PTYPE(p)]->isWimp)
                    continue;
                
                tmpSi = GET_SCREENINFO(p);
                
                if ((tmpSi->flags & SI_MARKER) == 0)
                    continue;
#if 1
                if (cb (p, arg) == 1)
                    return;
#else
                px = (p->pos.d.x & 0xffff0000) | (ix & 0xffff);
                py = (p->pos.d.y & 0xffff0000) | (iy & 0xffff);

                dx = abs(p->pos.d.x - px);
                dy = abs(p->pos.d.y - py);

                if ((dx <= GRID/2) && (dy <= GRID/2))
                {
                    if (cb (p, arg) == 1)
                        return;
                }     
#endif
            }
        }
    }
    
    /* .. and remove pixel markers */
    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            px = ix + INTERNC(xx - 16 + CORR_X);
            py = iy + INTERNC(yy - 16 + CORR_Y);
            sx = SCREENC(px);
            sy = SCREENC(py);
            siArray[sx + sy*wid].flags &= ~SI_MARKER;
        }
    }
}

typedef struct pfollow
{
    particle *p;
    FUNCPTR followFct;
    void *fctArg;

    struct pfollow *next;
} pfollow_t;

pfollow_t *followList = NULL;

void followParticles (void)
{
    pfollow_t *f;

    for (f = followList; f != NULL; f = f->next)
    {
        if (IS_DELETED(f->p))
            continue;

        f->followFct (f->p, f->fctArg);
    }
}
    

void followParticle (particle *p, FUNCPTR followFct, void *fctArg)
{
    pfollow_t *nf;

    nf = calloc (1, sizeof(*nf));

    if (!nf)
        return;

    nf->p = p;
    nf->followFct = followFct;
    nf->fctArg = fctArg;
    nf->next = followList;
    followList = nf;
}

void leaveParticle (particle *p, FUNCPTR followFct, void *fctArg)
{
    pfollow_t *f;
    pfollow_t *pf = NULL, *nf;

    for (f = followList; f != NULL; f = nf)
    {
        nf = f->next;

        if (p != f->p)
        {
            pf = f;
            continue;
        }

        if (followFct && (followFct != f->followFct))
        {
            pf = f;
            continue;
        }
        
        if (fctArg && (fctArg != f->fctArg))
        {
            pf = f;
            continue;
        }

        if (pf == NULL)
            followList = f->next;
        else
            pf->next = f->next;
        
        free (f);
    }
}

/* reorder per-thread particle lists so that parcicles are processed in the order 
 * the descriptors are located in memory.
 * This enhances performances significantly.
 */
void reorderParticles (void)
{
    int t, i;
    int oldCount = 0;
    int newCount = 0;
    particle *p;

    for (t = 0; t < options->numThreads; t++)
    {
        oldCount += tData[t]->numParticles;
        tData[t]->numParticles = 0;
    }

    for (i = 0; i < options->maxParticles; i++)
    {
        p = &partArray[i];
        if (IS_DELETED(p))
            continue;

        t = threadGet (PTYPE(p), SCREENC(p->pos.d.x), SCREENC(p->pos.d.y));

        tData[t]->particleList[tData[t]->numParticles++] = PARTICLE_ID(p);
        newCount++;
    }

    if (oldCount != newCount)
    {
        printf ("reorderParticles: old=%d new=%d\n", oldCount, newCount);
    }
}

/* randomize per-thread particle lists (for testing)
 */
void randomizeParticles (void)
{
    int t, i, ni, tmp;
    threadData *td;

    for (t = 0; t < options->numThreads; t++)
    {
        td = tData[t];

        for (i = 0; i < td->numParticles; i++)
        {
            ni = rand() % td->numParticles;

            tmp = td->particleList[i];
            td->particleList[i] = td->particleList[ni];
            td->particleList[ni] = tmp;
        }
    }
}
