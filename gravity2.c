
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#ifdef USE_OPENGL
#undef SCREENOUT
#endif

extern SDL_Surface *screen;

#ifndef FCT
#define FCT         densSpreadTSS1
#endif

#undef SHOW_POTENTIAL
#undef SHOW_RIPPLE
#define SHOW_POT_AND_RIPPLE

#if 0
/* Machine independent version of gravity propagation */
void densSpread##GTYPE (DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int num)
{
    screeninfo *pSi;
    int dx, dy;
    int i;
    DENSITY_TYPE di, dst;

    int        a2, b1, b2, b3, c2;
    DENSITY_TYPE        *pp, *ppDest, *dp, *ip;

    ip = diff;
    dp = dens;
    pp = src;
    ppDest = dest;

    b2 = *pp;
    a2 = *(pp-1);
    c2 = *(pp+1);
    b1 = *(pp-wid);
    b3 = *(pp+wid);

    for (i = 0; i < num; i++)
    {
        dst = (b1 + a2 + c2 + b3) / 4 + (*dens) ;

        di = (dst - b2) / 4;

        *ip = ((*ip + di) * 999) / 1000;
        *dest = b2 + *ip;

        ip++;
        dp++;
        pp++;
        ppDest++;
        a2 = b2;
        b2 = c2;
        c2 = *(pp+1);
        b1 = *(pp - wid);
        b3 = *(pp + wid);
    }
}
#endif

extern int m128Align;


/* calculate gravity array
 * dens is the mass*charge array for each pixel. When a particle enters a pixel, the mass/charge entry is increased, and
 * decreased if it leaves the pixel.
 * src is the currently used gravity field, dest is the next iteration of the gravity field.
 * diff is the 1st derivation of the gravity field (change rate of gravity).
 *
 * Algorithm:
 * For each src pixel:
 * 1) Calculate the mean of the 4 sourrinding pixels, and add the mass/charge pixel.
 * 2) Calculate the difference of result (1) and the current src gravity.
 * 3) Add the result of (2) (divided by a constant) to (diff), i.e. (2) is in fact
 *    the 2nd derivation of the gravity field (acceleration).
 * 4) Decrease (3) by 1/1000 (to prevent gravity waves to propagate endlessly).
 * 5) Add (4) to src and store it to dst.
 * 6) Add/subtract 1 to dst if greater/less than 0 respectively (see notes).
 *
 * NOTES:
 * o It would be probably more "realistic" to calculate the mean of all 5 involved pixels, BUT
 *   - This decreases the range of the gravity in a way that paricles would no longer hold together.
 *
 *   - It's faster to divide by 4 (arithmetic shift) then by 5 since sse2 has no integer divide.
 *     Drawbacks of this method:
 *     Fields rapidly change if a mass is on a pixel, but it does not if there is no mass on a pixel
 *     (e.g. if a particle leaves a pixel). Actually it will happen that the field can never
 *     again reach zero. 
 *     Workaround: Always decrease the gravity field by 1 in step (6). Not very elegant ...
 *
 * o A general problem is that, lacking a integer division SSE instruction, we need to do 
 *   arithmetic shift. This has the drawback that it rounds towards infinity for negative numbers and zero
 *   for positive numbers. The compensation code consists of 5 instructions (3 when SSSE3 is available).
 * 
 */
void FCT(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last)
{
    __m128i        a2, b1, b2, b3, c2, dif, dif2;
    __m128i        dst, tmp;
    __m128i        *pp, *ppDest, *dp, *ip;
    __m128i        zero = _mm_set1_epi16 (0);
#ifndef USE_SSSE3
    __m128i        three = _mm_set1_epi16 (3);
#endif
    int                wa = wid/8;
    int         i;


#if 0
#define DIV_SHIFT 7
    __m128i        divMask = _mm_set1_epi16 (63);
    __m128i        gDivisor = _mm_set1_epi16 (127);
#else
#define DIV_SHIFT 9
    __m128i        divMask = _mm_set1_epi16 (255);
    __m128i        gDivisor = _mm_set1_epi16 (511);
#endif

#ifdef SCREENOUT
    __m128i     *scrp, scr, iscol, gra, grah, gral, isneg;
    __m128i     thresh = _mm_set1_epi32 (0x01000000);

    uintptr_t        k;

    k = (uintptr_t)screen->pixels;
    if (k & 0xf)
    {
        k = (k + 0xf) & ~0xf;

        if (!last)
            num+=8;
    }
    
    scrp =  (__m128i*)(k + (uintptr_t)pixel*4);
#endif
        
    /* number of 128 bit (8*16) elements
     */
    num /= 8;

    dp = (__m128i*)dens;
    ip = (__m128i*)diff;
    pp = (__m128i*)src;
    ppDest = (__m128i*)dest;

    a2 = *(pp-1);
    b2 = *pp;
    c2 = *(pp+1);
    b1 = *(pp - wa);
    b3 = *(pp + wa);

    for (i = 0; i < num; i++)
    {
        /* add the values of 4 involved pixels and divide it by 4 afterwards.
         * SSE does not provide a integer divide instruction, so we use arithmetic shift,
         * even if the arithmetic shift does not round "correctly" for negative numbers.
         *
         * Each operation requires 4 neighbouring 128-bit SSE2 values:
         *    b1   
         * a2 b2 c2
         *    b3   
         *
         * b2 is the current processing element, we also need a2, b1, b3 and c2.
         */


        /* mean of 4 neighbours */
        dst = _mm_adds_epi16(b1, b3);

        dst = _mm_adds_epi16(dst, _mm_slli_si128 (b2, 2));
        dst = _mm_adds_epi16(dst, _mm_srli_si128 (a2, 7*2));

        dst = _mm_adds_epi16(dst, _mm_srli_si128(b2, 2));
        dst = _mm_adds_epi16(dst, _mm_slli_si128(c2, 7*2));

//dst = _mm_adds_epi16(dst, b2);

        /* add mass on processing pixel */
        dst = _mm_adds_epi16 (dst, *dp);

#undef EXACT_AVG
#ifndef EXACT_AVG

        /* divide by four */
#if 1
        dst = _mm_srai_epi16 (dst,2);
#else
        
        /* divide with compensation for negative numbers, not necessary here */

#ifdef USE_SSSE3
        tmp = _mm_abs_epi16 (dst);
        tmp = _mm_srai_epi16 (tmp, 2);
        dst = _mm_sign_epi16 (tmp, dst);
#else
        tmp = _mm_and_si128 (dst, three);        
        tmp = _mm_cmpgt_epi16 (tmp, zero);
        tmp = _mm_and_si128 (tmp,_mm_cmpgt_epi16 (zero, dst));
        dst = _mm_subs_epi16 (dst,tmp);
#endif

#endif

#else
        {
            signed short __attribute__ ((aligned (64))) _tmp[8];
            *(__m128i*)_tmp = dst;

            _tmp[0] /= 5;
            _tmp[1] /= 5;
            _tmp[2] /= 5;
            _tmp[3] /= 5;
            _tmp[4] /= 5;
            _tmp[5] /= 5;
            _tmp[6] /= 5;
            _tmp[7] /= 5;

            dst = *(__m128i*)_tmp;
        }
#endif


//        dst = _mm_adds_epi16 (dst, *dp);
        
//        dst = _mm_srai_epi16 (dst,1);

        /* 2nd derivation of gravity is difference between current and "target" gravity
         * divided by a constant (4 here)
         */
        dif2 = _mm_subs_epi16(dst, b2);

        /* divide by four, compensate for negative numbers */
#ifdef USE_SSSE3
        tmp = _mm_abs_epi16(dif2);
        tmp = _mm_srai_epi16(tmp, 2);
        dif2 = _mm_sign_epi16 (tmp, dif2);
#else
        dif2 = _mm_srai_epi16(dif2, 2);
        tmp = _mm_and_si128 (dif2, three);        
        tmp = _mm_cmpgt_epi16 (tmp, zero);
        tmp = _mm_and_si128 (tmp,_mm_cmpgt_epi16 (zero, dif2));
        dif2 = _mm_subs_epi16 (dif2,tmp);
#endif


        /* update gravity change rate and multiply it with ((1<<DIV_SHIFT)-1) / (1<<DIV_SHIFT) */
        dif = *ip;
        dif = _mm_adds_epi16(dif, dif2);
        dst = _mm_adds_epi16(b2, dif);

        /* multiply by (1<<DIV_SHIFT)-1  .. */
        dif = _mm_mullo_epi16 (dif, gDivisor);

#if 1
        /* and divide by (1<<DIV_SHIFT). Use arithmetic shift.
         * Arithmetic shift rounds towards -infinity for negative numbers, so add 1 (subtract -1)
         * if necessary.
         */

        /* tmp: -1 if bit mask 0x3f is not 0 and negative number, 0 otherwise */
        tmp = _mm_and_si128 (dif, divMask);        
        tmp = _mm_cmpgt_epi16 (tmp, zero);
        tmp = _mm_and_si128 (tmp,_mm_cmpgt_epi16 (zero, dif));

        dif = _mm_srai_epi16(dif, DIV_SHIFT);
        dif = _mm_subs_epi16(dif, tmp);
        *ip = dif;
#else
#define DIVISOR                128

        *ip = dif;
        ((signed short*)ip)[0] /=  DIVISOR;
        ((signed short*)ip)[1] /=  DIVISOR;
        ((signed short*)ip)[2] /=  DIVISOR;
        ((signed short*)ip)[3] /=  DIVISOR;
        ((signed short*)ip)[4] /=  DIVISOR;
        ((signed short*)ip)[5] /=  DIVISOR;
        ((signed short*)ip)[6] /=  DIVISOR;
        ((signed short*)ip)[7] /=  DIVISOR;
#endif

#if 1
        /* move gravity towards zero ... :-( */
        dst = _mm_adds_epi16(dst, _mm_cmpgt_epi16 (dst, zero));
        dst = _mm_subs_epi16(dst, _mm_cmplt_epi16 (dst, zero));
#endif
        *ppDest = dst;

#ifdef SCREENOUT
        /* place the calculated gravity data into the framebuffer:
         * pack two epi16 values into an epi8 value after division by 2^n
         * merge into framebuffer value (only replace pixels with values < thresh)
         * store back to framebuffer
         * The countdown value has two purposes:
         * - The framebuffer data may be unaligned for a _m128i value, so we may need to
         *   omit the first screen element until we are aligned.
         * - The processing is don by merging two epi16 elements into an api8 element, so
         *   we gather one epi16 into oldDst in the first pass (countdown > 0) and do
         *   the actual processing in the 2nd pass when we have the second epi16 available.
         */
#ifdef SHOW_POTENTIAL
        gra = _mm_srai_epi16 (dst, GETVAL32(potDivider));
#endif
#ifdef SHOW_RIPPLE
        gra = _mm_srai_epi16 (dif, GETVAL32(rippleDivider));
#endif
#ifdef SHOW_POT_AND_RIPPLE
        gra = _mm_srai_epi16 (dst, GETVAL32(potDivider));
        gra = _mm_adds_epi16(gra, _mm_srai_epi16 (dif, GETVAL32(rippleDivider)));
#endif

        /* we have 8 signed 16-bit values. */

        /* .. pack to 8 8-bit values. This does the clamping to [-128; 127] */
        gra = _mm_packs_epi16 (gra, zero);

        /* unpack again to 8 16-bit values (upper 8-bits zero) */
        gra = _mm_unpacklo_epi8 (gra, zero);

        /* unpack upper 4 values to 4 32-bit values (only lower 8-bits set, blue channel)
         */
        grah = _mm_unpackhi_epi16(gra, zero);

        /* we want positive values to go into the red channel, negative values to the green channel */
        isneg = _mm_cmplt_epi8 (grah, zero);
        grah = _mm_or_si128 (_mm_slli_si128(_mm_andnot_si128 (isneg, grah), 2),        /* positive: blue to reg */
                             _mm_slli_si128(_mm_and_si128 (isneg, _mm_subs_epi8 (zero, grah)), 1)); /* negative: negate and to green */

        /* same for lower 4 values */
        gral = _mm_unpacklo_epi16(gra, zero);
        isneg = _mm_cmplt_epi8 (gral, zero);
        gral = _mm_or_si128 (_mm_slli_si128(_mm_andnot_si128 (isneg, gral), 2),
                             _mm_slli_si128(_mm_and_si128 (isneg, _mm_subs_epi8 (zero, gral)), 1));

        /* merge color values to framebuffer. Only for color values with alpha channel 0!
         * Alpha channel > 0 is reserved for static screen data (walls).
         */
        scr = *scrp;

        iscol = _mm_cmpgt_epi32 (thresh, scr);
        scr = _mm_or_si128 (gral, _mm_andnot_si128 (iscol, scr));
        *scrp++ = scr;

        scr = *scrp;
        iscol = _mm_cmpgt_epi32 (thresh, scr);
        scr = _mm_or_si128 (grah, _mm_andnot_si128 (iscol, scr));
        *scrp++ = scr;
#endif

        /* move current and result pointer */
        ip++;
        dp++;
        pp++;
        ppDest++;

        /* fetch next elements */
        a2 = b2;
        b2 = c2;
        c2 = *(pp+1);
        b1 = *(pp - wa);
        b3 = *(pp + wa);
    }

    _mm_empty();        
}

