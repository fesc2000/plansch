/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define IS_NEW(P)                PFLAG3(P)
#define SET_NEW(P)                PFLAG3_SET(P)
#define CLEAR_NEW(P)                PFLAG3_CLR(P)

int energyType = -1;

/* energy interaction:
 * 0 : always repulses
 * 1 : always attracts
 * 2 : repulses/attracts if same/opposite charge
 */
SETTING(energyInteraction) =
{
    .minValue = 0,
    .maxValue = 2,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Energy interaction mode",
};

SETTING(energyColorVal) =
{
    .minValue = 0,
    .maxValue = 0,
    .defaultValue = 0,
    .name = "Energy Color",
    .cb = (FUNCPTR)buttonColorBg
};

SETTING(energyNotAbsorped) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Absorp energy particles",
};

SETTING(energyColormerge) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 1,
    .partAttr = 0,
    .name = "Merge energy colors",
};


screeninfo *
moveEnergy (particle *p, screeninfo *oldSi, threadData *td, int oss, particle2 *pa2 )
{
    vec_t screenPos;
    vec_t newPos;
    screeninfo *newSi;
    vec_t tvec;
    float vec_len;

    if (IS_NEW(p))
    {
        if ((p->vec.d.x == 0) && (p->vec.d.y == 0))
        {
            p->vec.d.x = RND_MASK(8) - 127;
            p->vec.d.y = RND_MASK(8) - 127;
        }
        if ((p->vec.d.x == 0) && (p->vec.d.y == 0))
            return NULL;
    
        vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);
        p->vec.d.x = ((float)p->vec.d.x * MAX_SPEEDF) / vec_len;
        p->vec.d.y = ((float)p->vec.d.y * MAX_SPEEDF) / vec_len;
        
        CLEAR_NEW(p);
    }

    td->v.partCount++;


    tvec = p->vec;
    V_ADD(newPos, p->pos, tvec);
    V_TO_SCREEN(screenPos, newPos);

    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];


    if (oldSi == newSi)
    {
        /* no screen movement */
        p->pos = newPos;

        return oldSi;
    }

    if (newSi->particleList == SCR_BRICK)
    {
        if (GETVAL32(wallTempr))
        {
            int t = (FLOAT)((GRID - TEMPR(newSi))/2) * wallDamping;

            TEMPR(newSi) += t;
        }
        td->drawLinkLine = 1;
        
        return NULL;
    }
    else if (newSi->flags & SI_LINE)
    {
        td->drawLinkLine = 1;
        return NULL;
    }

    if (newSi->particleList > SCR_BRICK)
    {
        int totw = 0;
        float dx, dy;
        float pimp;
        int m = p->mass;
        particle *pp;
        
        for (pp = newSi->particleList; pp != NULL; pp = pp->next)
        {
            totw += 1;
        }
        
        for (pp = newSi->particleList; (pp != NULL) && (m > 0); pp = pp->next)
        {
            dx = pp->pos.d.x - p->pos.d.x;
            dy = pp->pos.d.y - p->pos.d.y;
            vec_len = VEC_LEN(dx, dy);
            pimp = ((m * 250) / (float)(totw * pp->mass));
            m -= pp->mass;
            
            /* attract instead of repulse if explictly selected and/or charge is different
             */
            switch (getValue (&energyInteraction))
            {
                case 1:
                    pimp = -pimp;
                    break;
                case 2:
                    if (PFLAG_12VAL(p) != CHARGE_SIGN_CODE(pp))
                    {
                        pimp = -pimp;
                    }
            }

            /* reverse attraction mode if shift is pressed
             */
            if (shiftPressed())
                pimp = -pimp;

            HIT(pp, ((dx / vec_len) * pimp), ((dy / vec_len) * pimp));
            if (GETVAL32(energyColormerge))
            {
                mergeParticleColors (p, pp);
            }
            td->drawLinkLine = 1;
        }
        
        if (GETVAL32(energyNotAbsorped) == 0)
        {
            if (m <= 0)
            {
                p->mass = 0;
                td->drawLinkLine = 1;
                return NULL;
            }

            p->mass = m;
        }
    }

    oldSi->wimps--;

    if (oldSi->wimps <= 0)
    {
        oldSi->wimps = 0;
    }

    if (((uint)screenPos.d.x >= wid) || ((uint)screenPos.d.y >= hei))
    {
        td->drawLinkLine = 1;
        return NULL;
    }

    newSi->wimps++;

    p->pos = newPos;

    return newSi;
}

static uint32_t energyColor (uint32_t *pCol)
{
    value_u *u = getValue_u(&energyColorVal);

    if (!u)
        return 0;

    *pCol = ARGB(u->cv.argb);

    return 1;
}

static int setEnergyType (particle *p)
{
    SET_NEW(p);

    PFLAG_12VAL_SET(p, CHARGE_SIGN_CODE(p));

    SET_CHARGE(p, 0);
    
    return 1;
}

static int energyInit (int id)
{
    button *b;
    value_u col;

    energyType = id;

    encodeARGB(&col.cv, 1.0, 1.0, 1.0, 0.0);

    b = buttonFindByName ("Energy Color");

    if (b)
    {
        b->pSetting = b->bgColorSetting = &energyColorVal;
        energyColorVal.pButton = b;
        energyColorVal.defaultValue = col.va64;
        modifySetting (&energyColorVal, col.va64, 0);
    }

    pdescs[id]->crangeColors[0] = &energyColorVal;

    return 1;
}

void energyColorSet (button *b)
{
    pickColor (b->pSetting);
}


pdesc MOD = {
    .init = (FUNCPTR)energyInit,
    .moveParticle = moveEnergy,
    .setType = (FUNCPTR)setEnergyType,
    .name = "Energy",
    .getColor = (FUNCPTR)energyColor,
    .descr = "",
    .pm = energy_pm,
    .help = "Energy",
    .isWimp = 1,
    .noDecel = 1,
    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},
#ifdef USE_OPENGL
        {(FUNCPTR)pushButton, 32, 67, colors_xpm , "Energy Color", (intptr_t)&energyColorSet, NULL, B_NEXTRC },
        {(FUNCPTR)onOffButtonS, 32, 32, energycm_xpm, "Merge Colors", 0, &energyColormerge, B_NEXTRC },
#endif
        {(FUNCPTR)onOffButtonS, 32, 32, energyabsorp_xpm, "Don't Absorp Energy", 0, &energyNotAbsorped, B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 22, 19, rep_xpm, "Repulse", 0, &energyInteraction, B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 22, 19, attr_xpm, "Attract", 1, &energyInteraction, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 19, attr_rep_xpm, "Repulse_same_charge", 2, &energyInteraction, B_NEXT|B_SPC(1) },
    }
};
