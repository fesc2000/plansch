/* Copyright (C) 2008-2018 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/
#include "plansch.h"
#include <stdio.h>
#include <fcntl.h>
#include <CL/cl.h>
#include <CL/cl_ext.h>
#include <CL/cl_gl.h>

#include "opencl.h"
#include "kernels.h"


/* field and screen data
 */
int           curField = 0;
clBuffer_t   *fieldBuffer[2];
clBuffer_t   *densBuffer;
clBuffer_t   *fixDensBuffer;
int           fixDensBufferUpdate = 0;
clBuffer_t   *diffBuffer;
clBuffer_t   *diffBufferHost;
clBuffer_t    gravBufferShared;
clBuffer_t   *gravBuffer;
clBuffer_t   *particleDens;
clBuffer_t   *clWallTempr[2];
int           curTempr = 0;

void runField()
{
    cl_int   status;
    size_t globalThreads[2];
    int                 arg;
    cl_int        iterations = GETVAL32(gravIterations);
    cl_int      toHost = numParticles ? 1 : 0;
    cl_kernel   fieldRun = GETVAL32(gpuBorderFieldCutoff) ? fieldRunI : fieldRunZ;
    
    /* TODO some strange limit (AMD?) */
    if (iterations > 63)
        iterations = 63;

    globalThreads[0] = 1;
    globalThreads[1] = hei;

    arg = 0;
    kernelBufferArg (fieldRun, arg++, fieldBuffer[1-curField]);
    if (totalGpuParticles)
    {
        kernelBufferArg (fieldRun, arg++, particleDens);
    }
    else
    {
        kernelBufferArg (fieldRun, arg++, densBuffer);
    }
    kernelBufferArg (fieldRun, arg++, fieldBuffer[curField]);
    kernelBufferArg (fieldRun, arg++, fixDensBuffer);
    kernelBufferArg (fieldRun, arg++, diffBuffer);
    kernelBufferArg (fieldRun, arg++, diffBufferHost);
    kernelBufferArg (fieldRun, arg++, accelBuffer);
    kernelBufferArg (fieldRun, arg++, clParamBuffer);
    clSetKernelArg (fieldRun, arg++, sizeof(cl_int), (void *)&iterations);
    clSetKernelArg (fieldRun, arg++, sizeof(cl_int), (void *)&toHost);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 fieldRun,
                 1,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    CL_FINISH;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s, %d)\n", __FUNCTION__, status);
    }
    gpuLoops += iterations;
}

void iterateField()
{
    cl_int   status;
    size_t globalThreads[2];
    cl_kernel fieldIterate;
    
    fieldIterate = GETVAL32(gpuBorderFieldCutoff) ? fieldIterateI : fieldIterateZ;

    globalThreads[0] = wid;
    globalThreads[1] = hei;

    kernelBufferArg (fieldIterate, 0, fieldBuffer[1-curField]);
    if (totalGpuParticles)
    {
        kernelBufferArg (fieldIterate, 1, particleDens);
    }
    else
    {
        kernelBufferArg (fieldIterate, 1, densBuffer);
    }
    kernelBufferArg (fieldIterate, 2, fieldBuffer[curField]);
    kernelBufferArg (fieldIterate, 3, fixDensBuffer);
    kernelBufferArg (fieldIterate, 4, clParamBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 fieldIterate,
                 2,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    CL_FINISH;

    curField = 1 - curField;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", __FUNCTION__);

        if (localSizeDim2 != 1)
        {
            printf ("Trying to set localThreads[1] to 1\n");
            localSizeDim2 = 1;
        }

        return;
    }
}

void iterateTempr(void)
{
    cl_int   status;
    size_t globalThreads[2];

    globalThreads[0] = wid;
    globalThreads[1] = hei;

    kernelBufferArg (iterateWallTempr, 0, CUR_TEMPR_BUF);
    kernelBufferArg (iterateWallTempr, 1, ALT_TEMPR_BUF);
    kernelBufferArg (iterateWallTempr, 2, fixDensBuffer);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 iterateWallTempr,
                 2,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", __FUNCTION__);

        return;
    }
    CL_FINISH;
    SWITCH_TEMPR_BUF;
}

void runCalcDiff (int toHost)
{
    cl_int   status;
    size_t globalThreads[2];

    globalThreads[0] = wid;
    globalThreads[1] = hei;

    if (useLocalMem)
    {
        kernelBufferArg (calcDiff, 0, fieldBuffer[curField]);
        kernelBufferArg (calcDiff, 1, diffBuffer);
        status = clSetKernelArg(
                    calcDiff, 
                    2, 
                    maxWorkGroupSize*sizeof(cl_float),
                    (void *)NULL);
        kernelBufferArg (calcDiff, 3, diffBufferHost);
        kernelBufferArg (calcDiff, 4, accelBuffer);
            clSetKernelArg( calcDiff, 5, sizeof(cl_int), (void *)&toHost);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     calcDiff,
                     2,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);
    }
    else
    {
        kernelBufferArg (calcDiffnl, 0, fieldBuffer[curField]);
        kernelBufferArg (calcDiffnl, 1, diffBuffer);
        kernelBufferArg (calcDiffnl, 2, diffBufferHost);
        kernelBufferArg (calcDiffnl, 3, accelBuffer);
            clSetKernelArg( calcDiffnl, 4, sizeof(cl_int), (void *)&toHost);

        status = clEnqueueNDRangeKernel(
                     commandQueue,
                     calcDiffnl,
                     2,
                     NULL,
                     globalThreads,
                     NULL,
                     0,
                     NULL,
                     NULL);

    }
    CL_FINISH;

    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", __FUNCTION__);
        return;
    }
}

void runFieldToTex()
{
    cl_int   status;
    cl_float cm, gm;
    size_t globalThreads[2];
    cl_kernel k;
    float alpha = gravAlpha.current.normFloat;
    int arg = 0;

    if (GETVAL32(gravAlpha) == 0)
        return;

    globalThreads[0] = wid;
    globalThreads[1] = hei;

    gm = getValueNorm (&potDivider) * 0.1f;
    cm = getValueNorm (&rippleDivider) * 0.1f;

    if (getValue(&fieldLines))
        k = fieldToTexLines;
    else
        k = fieldToTex;

    kernelBufferArg (k, arg++, fieldBuffer[curField]);
    kernelBufferArg (k, arg++, diffBuffer);
    kernelBufferArg (k, arg++, CUR_TEMPR_BUF);
    kernelBufferArg (k, arg++, gravBuffer);
    status = clSetKernelArg( k, arg++, sizeof(cl_float), (void *)&gm);
    status = clSetKernelArg( k, arg++, sizeof(cl_float), (void *)&cm);
    status = clSetKernelArg( k, arg++, sizeof(cl_float), (void *)&alpha);

    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 k,
                 2,
                 NULL,
                 globalThreads,
                 NULL,
                 0,
                 NULL,
                 NULL);
    if(status != CL_SUCCESS) 
    { 
        printf( "Error: Enqueueing kernel onto command queue. (%s)\n", __FUNCTION__);
        return;
    }
    CL_FINISH;

    if (!options->clGlSharing)
    {
        /* hack to transfer opencl buffer to openGl texture if sharing does not work (e.g. for CPU cl device ...)
         * performance doesn't matter here anyway ...
         */
        clBufferToHost (gravBuffer, 0);
    }
}

void clDoGrav(int scrUpdate)
{
    int i;
    int densBufferCopied;
    
    if (densityUpdate)
    {
        clBufferToGPU (densBuffer, 1);
        densityUpdate = 0;
    }

    densBufferCopied = 0;
    for (i = 0; i < CL_NUM_PARTICLE_SETS; i++)
    {
        if (particleSet[i].particleCountAlgn)
        {
            if (!densBufferCopied)
            {
                COPY_BUFFER (particleDens, densBuffer);

                densBufferCopied = 1;
            }

            runParticleToDens(&particleSet[i]);
        }
    }

    if (haveOcl2)
    {
        runField ();

#if 0
        if (totalGpuParticles || numParticles)
            runCalcDiff (numParticles ? 1 : 0);
#endif
    }
    else
    {
        for (i = GETVAL32(gravIterations); i > 0; i--)
        {
            gpuLoops++;

            iterateField ();
            iterateField ();
        }

#if 0
        if (scrUpdate && showGravity)
        {
            runFieldToTex();
        }
#endif

        /* calculate accelleration vectors if there are particles that care
         */
        if (totalGpuParticles || numParticles)
            runCalcDiff (numParticles ? 1 : 0);
    }

    /* transfer data to host if there are any particles that care about it
     * and we are not using pinned memory where diffBuffer is mapped into
     * host memory.
     * Pinned memory seems very slightly slower for me ..
     */
    if (numParticles && !tryPinned)
    {
        clBufferToHost (diffBufferHost, 1);
    }

    CL_FINISH
}


