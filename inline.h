/*
Copyright (C) 2010 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

static inline uint64_t ullTime(void)
{
        struct timeval tv;
            
        gettimeofday (&tv, NULL);
                    
        return ((uint64_t)tv.tv_sec * 1000000ULL + (uint64_t)tv.tv_usec);
}


static inline void setPixel (int x, int y, int col)
{
#ifdef ZOOM
#ifndef USE_OPENGL
    if (zoom != 1)
    {
        SDL_Rect r;
        if (IS_VISIBLE (x,y))
        {
            r.x = MIN(SCREENX(x), wid-2);
            r.y = MIN(SCREENY(y), hei-2);

            r.w = r.h = zoom;
            if (r.x + r.w >= wid)
                r.w = wid - r.x;

            if (r.y + r.h >= hei)
                r.h = hei - r.y;


#ifndef OPENMP
            SDL_FillRect (screen, &r, col);
#else
            if (omp_in_parallel())
            {
                int xx, yy;

                for (yy = r.y; yy < r.y * r.h; yy++)
                {
                    for (xx = r.x; xx < r.x + r.w; xx++)
                    {
                        SCR_SET (xx, yy, col);
                    }
                }
            }
            else
            {
                SDL_FillRect (screen, &r, col);
            }
#endif
        }
        return;
    }
#endif
#endif
    SCR_SET (x, y, col);
}

static inline int isLinked (particle *p1, particle *p2)
{
    pLinks *pl = &linkTbl[PARTICLE_ID(p1)];
    int i;

    for (i = pl->num; i > 0;)
    {
        i--;
        if (pl->links[i] == p2)
            return 1;
    }

    return 0;
}

static inline void addLinkLine (threadData *td, int x1, int y1, int x2, int y2, v4sf c1, v4sf c2)
{
    td->linkLines[td->numLinkLines].d.x = x1;
    td->linkLines[td->numLinkLines].d.y = y1;
    td->linkColors[td->numLinkLines] = c1;
    td->linkLines[td->numLinkLines+1].d.x = x2;
    td->linkLines[td->numLinkLines+1].d.y = y2;
    td->linkColors[td->numLinkLines+1] = c2;
    td->numLinkLines+=2;
}                                

static inline void addLinkLineV (threadData *td, vec_t v1, vec_t v2, v4sf c1, v4sf c2)
{
    td->linkLines[td->numLinkLines] = v1;
    td->linkColors[td->numLinkLines] = c1;
    td->linkLines[td->numLinkLines+1] = v2;
    td->linkColors[td->numLinkLines+1] = c2;
    td->numLinkLines+=2;
}                                

#define ADD_LINK_LINE(x1, y1, x2, y2, c1, c2) {        \
    td->linkLines[td->numLinkLines].d.x = x1;                \
    td->linkLines[td->numLinkLines].d.y = y1;                \
    td->linkColors[td->numLinkLines] = c1;                \
    td->linkLines[td->numLinkLines+1].d.x = x2;                \
    td->linkLines[td->numLinkLines+1].d.y = y2;                \
    td->linkColors[td->numLinkLines+1] = c2;                \
    td->numLinkLines+=2; }                                

#define ADD_LINK_LINE_V(v1, v2, c1, c2) {        \
    td->linkLines[td->numLinkLines] = (v1);                \
    td->linkColors[td->numLinkLines] = c1;                \
    td->linkLines[td->numLinkLines+1] = (v2);                \
    td->linkColors[td->numLinkLines+1] = c2;                \
    td->numLinkLines+=2; }                                


/* count # of bits
 */
static inline int popcount(uint32_t i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

