/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"

#define MAGIC        123

typedef struct cq
{
    unsigned int time;
    FUNCPTR         fct;
    void        *arg;
    int                 valid;

    struct cq *next;
} callout_queue_entry;


static callout_queue_entry *calloutQueue = NULL;


void *calloutNew (void)
{
    callout_queue_entry *e;

    e = malloc(sizeof(*e));

    if (e == NULL)
        return NULL;

    e->valid = MAGIC;

    return e;
}

void calloutSched (void *id, int time, FUNCPTR fct, void *arg)
{
    callout_queue_entry        *e, *se, *pe;

    e = id;

    if ((e == NULL) || (e->valid != MAGIC))
        return;

    calloutUnsched (id);

    if (time < 0)
        return;


    e->time = SDL_GetTicks() + time;
    e->fct = fct;
    e->arg = arg;

    pe = NULL;
    for (se = calloutQueue; se != NULL; se = se->next)
    {
        if (se->time > e->time)
        {
            break;
        }
        pe = se;
    }

    if (pe == NULL)
    {
        e->next = calloutQueue;
        calloutQueue = e;
    }
    else
    {
        e->next = pe->next;
        pe->next = e;
    }
}

void calloutUnsched (void *id)
{
    callout_queue_entry        *se, *pe;

    pe = NULL;
    for (se = calloutQueue; se != NULL; se = se->next)
    {
        if (se == id)
        {
            break;
        }
        pe = se;
    }

    if (!se)
        return;

    if (pe == NULL)
        calloutQueue = se->next;
    else
        pe->next = se->next;
}


void processCalloutQueue (void)
{
    callout_queue_entry        *se;
    unsigned int curTime;

    while (calloutQueue != NULL)
    {
        curTime = SDL_GetTicks();

        if (calloutQueue->time > curTime)
            break;

        se = calloutQueue;

        calloutQueue = calloutQueue->next;

        se->fct (se, se->arg);

    }
}


static unsigned int frozen = 0;
static unsigned int sysTime = 0;
static unsigned int mainLoopCount = 0;

unsigned int planschTime()
{
    return SDL_GetTicks();
}

unsigned int systemTime()
{
    return sysTime;
}

unsigned int systemLoops()
{
    return mainLoopCount;
}

void freezeTime()
{
    frozen = 1;
}

void unfreezeTime()
{
    frozen = 0;
}

void systemTimeTick()
{
    mainLoopCount++;
    if (!frozen)
        sysTime++;
}

int expired (uint32_t *tick, int timeout, uint32_t current)
{
    int rc;

    if (!current)
        current = planschTime();

    rc = ((current - *tick) > timeout);

    if (rc)
        *tick = current;

    return rc;
}

