/*
Copyright (C) 2010 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#define MAX_PWIDGETS        128
#define SAVE_PATH_LEN        1024
#define PIXMAP_WID        32
#define MAX_ZOOM        128
#define MARK_SHOW_INTERVAL  150
#define MAX_PDESCS 32
#define PART_COLL_MAX        256
#define PID_IN_PSTRUCT

#define CL_NUM_PARTICLE_SETS        1

#define SIN_COS_TABLE_ENTRIES        (360*5)

/* required by (experimental) carnivore particle type:
 */ 
#undef LINK_HAVE_OTHER_IDX
