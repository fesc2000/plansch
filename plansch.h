/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEBUG
#define DEBUG 0
#endif

#include <sys/types.h>
#include <stdio.h>
#include <SDL.h>
#include <string.h>
#ifdef TTF_SUPPORT
#include <SDL_ttf.h>
#endif
#ifdef LOADSTORE
#include <SDL_image.h>
#endif
#include <malloc.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <getopt.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#if !defined(_MINGW)
#include <execinfo.h>
#include <sys/mman.h>
#else
#define getpagesize() 0x1000
void *memalign(size_t size, size_t align);
#endif

#include "SDL_picofont.h"

#include "SDL_gfxPrimitives.h"

#ifdef INCLUDE_CCSH
#include "cexpr.h"
#endif

#include "clshared.h"



#ifdef USE_OPENGL

#define NO_SDL_GLEXT

#ifndef OPENGL_GLEW
# define GL_GLEXT_PROTOTYPES
# include <GL/gl.h>
# include <GL/glext.h>
#else
# ifdef _MINGW
#  define GLAPI __declspec(dllimport)
#  define GLEW_STATIC
# endif
# include <GL/glew.h>
#endif

#if defined(_MINGW)
# define GL_CONTEXT_T void *
#else
# include <GL/glx.h>
# define GL_CONTEXT_T GLXContext
#endif

#include <SDL_opengl.h>
#endif

#include "pixmaps.h"

#include "constants.h"

#define VERSION        "1.25"

#define TRUECOLOR

#define ZOOM

#if defined(USE_OPENGL) || defined(USE_OPENCL)
#define USE_GPU
#endif

#ifndef MAXPATHLEN
#define MAXPATHLEN 1024
#endif

#ifdef OPENMP
#include <omp.h>

#ifdef BIND_THREADS

#ifndef __CPU_ZERO_S
# define __CPU_ZERO_S(setsize, cpusetp) \
  do {                                                                        \
    size_t __i;                                                               \
    size_t __imax = (setsize) / sizeof (__cpu_mask);                          \
    __cpu_mask *__bits = (cpusetp)->__bits;                                   \
    for (__i = 0; __i < __imax; ++__i)                                        \
      __bits[__i] = 0;                                                        \
  } while (0)
#endif

#ifndef __CPU_SET_S
# define __CPU_SET_S(cpu, setsize, cpusetp) \
  (__extension__                                                              \
   ({ size_t __cpu = (cpu);                                                   \
      __cpu < 8 * (setsize)                                                   \
      ? (((__cpu_mask *) ((cpusetp)->__bits))[__CPUELT (__cpu)]               \
         |= __CPUMASK (__cpu))                                                \
      : 0; }))
#endif



#define CPU_SET(cpu, cpusetp)   __CPU_SET_S (cpu, sizeof (cpu_set_t), cpusetp)
#define CPU_ZERO_S(setsize, cpusetp)       __CPU_ZERO_S (setsize, cpusetp)
#ifndef __CPU_ALLOC
# define __CPU_ALLOC(count) malloc (count)
#endif

#define CPU_ALLOC(count) __CPU_ALLOC (count)

#ifndef __CPU_ALLOC_SIZE
# define __CPU_ALLOC_SIZE(count) \
  ((((count) + __NCPUBITS - 1) / __NCPUBITS) * sizeof (__cpu_mask))
#endif


#define CPU_ALLOC_SIZE(count) __CPU_ALLOC_SIZE (count)
#define CPU_ISSET(cpu, cpusetp) __CPU_ISSET_S (cpu, sizeof (cpu_set_t), \
                                                cpusetp)
#define CPU_SET_S(cpu, setsize, cpusetp)   __CPU_SET_S (cpu, setsize, cpusetp)
#define CPU_ISSET_S(cpu, setsize, cpusetp) __CPU_ISSET_S (cpu, setsize, \
                                                           cpusetp)


extern int pthread_setaffinity_np (pthread_t __th, size_t __cpusetsize,
                                   __const cpu_set_t *__cpuset)
     __THROW __nonnull ((3));
#else
# define CPU_SET(cpu, cpusetp)
# define CPU_ZERO_S(setsize, cpusetp)
# define CPU_ALLOC(count)
# define CPU_ALLOC_SIZE(count)
# define CPU_ISSET(cpu, cpusetp)
# define CPU_SET_S(cpu, setsize, cpusetp)
# define CPU_ISSET_S(cpu, setsize, cpusetp)
#endif


#endif

#ifdef INCLUDE_CCSH
# define THREAD_NAME(n) ccsh_task_self(n, 0)
#else
# define THREAD_NAME(n)
#endif
 
#include <sched.h>

extern SDL_Surface *screen;
#include <emmintrin.h>
#ifdef _SSE41
#include <smmintrin.h>
#endif

#ifdef USE_SSSE3
#include "tmmintrin.h"
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#define ERASE_IMM        1
#define ERASE_MARKED        2

#define D_CYCLING        1
#define D_PIXEL                2

#define THREAD_DIST_HORIZ

#define DSTACK_SEP        (button*)1

#define PAGE_ALIGN  getpagesize()

#ifndef HUGE
#ifdef MAXFLOAT
#define HUGE MAXFLOAT
#else
#define HUGE    3.40282347e+38F
#endif
#endif

#define SP_BUTTON1                (1<<1)
#define SP_BUTTON2                (1<<2)
#define SP_BUTTON3                (1<<3)
#define SP_CENTER_CURRENT        (1<<10)

#define IS_SUB_BUTTON(b) ((b)->parentButton != NULL)



#define SETTING(name) setting name
#define ASETTING(name,n) setting name[n]

#ifndef USE_OPENGL
#define SCR_CHANGED
#define SCR_CHANGE_START
#define SCR_CHANGE_FINISH
#else

#define SCR_CHANGE_START    {scrChangeFlag = 0;}
#define SCR_CHANGED            if (scrChangeFlag) {screenTexture->needsUpdate = panelTexture->needsUpdate = 1;}
#define SCR_CHANGE_FINISH   {scrChangeFlag = 1; SCR_CHANGED}

#endif

#define POLY_SCREEN 1
#define POLY_FILLED 2
#define POLY_OPEN   4
#define POLY_CONVEX   8
#define POLY_BW1        16

/* default stencils */
#define ST_NONE                0
#define ST_PIXEL        1
#define ST_VSMALLBALL        2
#define ST_SMALLBALL        3
#define ST_BALL                4
#define ST_BRICK        5
#define ST_BOX                6
#define ST_FCIRCLE        7
#define ST_SELECTOR        9

#define STEN_ALL_MASK        0xffff


/* draw modes */
#define DR_STAMP        0
#define DR_DRAW                1
#define        DR_LINE                2
#define DR_FLOOD        3
#define DR_CIRCLE        4
#define DR_CIRCLE_FILL        5
#define DR_MAX                6


#define DR_ALL_MASK        0xffff

/* draw operations */
#define DO_PARTICLES        0
#define DO_WALL                1
#define DO_STENCIL        2
#define DO_ERASE_WALL        3
#define DO_ERASE_PAR        4
#define DO_ERASE_PUMP        5
#define DO_ERASE_OBJ        6
#define DO_PIPE                7
#define DO_PUMP                8
#define DO_PUSH                9
#define DO_ACCEL        10
#define DO_GRAB                11
#define DO_CONVERT        12
#define DO_PROBE        13
#define DO_SELECTOR        14
#define DO_PIPE_ACCEL        15

#define DO_WIPE_ALL        16
#define DO_REMPART_ALL        17

#define DO_MOVE                18

#define DO_WALL_POLY        19

#define DO_MARK                20
#define DO_MARK_ALL        21
#define DO_UNMARK_ALL        22

#define DO_WALL_AUTOPOLY        23
#define DO_PAINT        24
#define DO_DECEL        25
#define DO_MAX                26

#define DO_STENCIL_NOFS        (DO_STENCIL | 0x10000000)

#define SI_CALC

#define PTYPE_START_DRAW   if (pdescs[GETVAL32(ptype)]->startDraw)        \
    {        \
        pdescs[GETVAL32(ptype)]->startDraw();   \
        recordStartDraw (&playbackTape, 1, GETVAL32(ptype));        \
    }
#define PTYPE_STOP_DRAW(a)   if (pdescs[GETVAL32(ptype)]->stopDraw)        \
    {        \
        pdescs[GETVAL32(ptype)]->stopDraw(a);   \
        recordStartDraw (&playbackTape, 0, GETVAL32(ptype));        \
    }

/* return code for particleHint method */
#define PHINT_RANDOM        0   /* random placement */
#define PHINT_FIXED        1   /* fixed location */
#define PHINT_FIXED1        2   /* fixed location, limit to 1 ppp */

#define PARTICLE_ID(p)        ((unsigned int)((p) - partArray))
#define PARTICLE_DESC(I)        (&partArray[I])

#define FLOAT            float
#define NELEMENTS(x)        (sizeof(x) / sizeof(x[0]))


#define CORR_X        1
#define CORR_Y        2

extern int zoomOffX;
extern int zoomOffY;

#define IS_VISIBLE(x,y) ((x >= zoomx) && (x <= (zoomx + wid/zoom)) && (y >= zoomy+0) && (y <= (zoomy + hei/zoom)))
#define SCREENX(x)  (((x) - zoomx) * zoom)
#define SCREENY(y)  (((y) - zoomy) * zoom)

#define REALX(X)    (((X) / zoom) + zoomx)
#define REALY(Y)    (((Y) / zoom) + zoomy)

#if 0
#define SX_TO_IX(x)        (((x) / zoom + zoomx) * GRID + ((x) % zoom) * (GRID/zoom))
#define SY_TO_IY(y)        (((y) / zoom + zoomy) * GRID + ((y) % zoom) * (GRID/zoom))
#else
#define SX_TO_IX(x)        (izoomx + (x) * (GRID / zoom))
#define SY_TO_IY(y)        (izoomy + (y) * (GRID / zoom))
#endif

#define IX_TO_SX(x)        (SCREENC(x) - zoomx)*zoom + ((x) & MAX_SPEED) / (GRID / zoom)
#define IY_TO_SY(y)        (SCREENC(y) - zoomy)*zoom + ((y) & MAX_SPEED) / (GRID / zoom)

#define IX_TO_SX_S(x)        (SCREENC((int)x) - zoomx)*zoom + ((int)(x) & MAX_SPEED) / (GRID / zoom)
#define IY_TO_SY_S(y)        (SCREENC((int)y) - zoomy)*zoom + ((int)(y) & MAX_SPEED) / (GRID / zoom)

#define FOREACH_STENCIL_SI(sx,sy,SI)                                                     \
    { int _xx, _yy, _px, _py;                                                        \
    for (_xx = 0; _xx < 32; _xx++) for (_yy = 0; _yy < 32; _yy++) {        \
        if (stencil[3 + _yy][_xx] == ' ') continue;                                    \
        _px = sx + _xx - 16 + CORR_X;                                                    \
        _py = sy + _yy - 16 + CORR_Y;                                                    \
        if (((uint)_px >= wid) || ((uint)_py >= hei))                                  \
            continue;                                                                    \
        SI = &siArray[_px+_py*wid];

#define ENDFOR        }}
        


/* panel raster options
 */
#define P_RIGHT_DOWN        0   /* first right, then down */
#define P_RIGHT_UP        1   /* first right, then up */
#define P_LEFT_DOWN        2   /* first left, then down */
#define P_LEFT_UP        3   /* first left, then up */
#define P_DOWN_RIGHT        4
#define P_DOWN_LEFT        5
#define P_UP_RIGHT        6
#define P_UP_LEFT        7

#define PRIMARY_X(r)        ((r) < 4)

/* button connection
 */
#define B_NEXT            0        /* continue in current row/column */
#define B_NEXTRC    1        /* continue in next row/column */
#define B_FILL            2        /* fill current row/comumn */
#define B_CMASK            3
#define B_OVERLAP   4        /* may overlap with next row */
#define B_SPC(s)        ((((s)+1) & 0xff) << 8)
#define B_SPC_GET(r)        (((r) >> 8) & 0xff)


#define QUAD_TYPE        int64_t
#define CSHIFT                16
#define GRID                (1<<(CSHIFT))
#define GRIDF                65536.0
#define MAX_SPEED        (GRID-1)
#define MAX_SPEEDF        65535.0
#define MAX_SPEEDQ        4294836225ULL
#define MAX_SPEEDQF        4294836225.0

#define RADIUS_SHIFT        (CSHIFT-9)
#define PART_RADIUS(P)        ((P)->size << RADIUS_SHIFT)
#define PART_RADIUSQ(P) (PART_RADIUS(P) * PART_RADIUS(P))
#define MAX_RADIUS        (0xff << RADIUS_SHIFT)

#define CURRENT_RADIUS        (getValue (&particleSize[GETVAL32(ptype)]) << RADIUS_SHIFT)

#define SCREENC(c)        ((c)>>CSHIFT)
#define S_SCREENC(c)        ((c) / GRID)
#define INTERNC(c)        ((c)<<CSHIFT)
#define S_INTERNC(c)        ((c)*GRID)
#define INTERNC_M(c)        (INTERNC(c) + GRID/2)
#define ALIGNC(c)        ((c) & ~MAX_SPEED)
#define OFFC(c)                ((c) & MAX_SPEED)
#define SCREENC_ZX(c)        (((c)-zoomx*GRID)/(GRID/zoom))
#define SCREENC_ZY(c)        (((c)-zoomy*GRID)/(GRID/zoom))
#define ZX(c)                (((c)-zoomx)*zoom)
#define ZY(c)                (((c)-zoomy)*zoom)

#define ALIGN_IC(c)        ((c) & ~MAX_SPEED)
#define SCREENC_F(c)        ((c) / GRIDF)

#define SCR_CONTENT(X,Y)        siArray[(X) + (Y)*wid].particleList
#define SI_GET(X,Y)        &siArray[(X) + (Y)*wid]
#define SI_GETI(X,Y)        &siArray[SCREENC(X) + SCREENC(Y)*wid]

#define SI_X(si)        (SI_OFFSET(si) % wid)
#define SI_Y(si)        (SI_OFFSET(si) / wid)

/* if the per-pixel "prev" pointer is not in the particle structure, 
 * use a dedicated array.
 *
 * Otherwise, encode the particle pointer directly in the particle
 * structure
 */
#ifndef PID_IN_PSTRUCT
# define PREV_GET(P)            prevTbl[PARTICLE_ID(P)]
# define PREV_SET(P,PP)            prevTbl[PARTICLE_ID(P)] = PP
#else
# define PREV_GET(P)            (P)->prev
# define PREV_SET(P,PP)     (P)->prev = (PP)
#endif

#ifdef USE_OPENCL
#define GGRAV_1x2
#define DENSITY_TYPE        int
#define GRAV_TYPE        signed int
#undef FIX_DENS_FLOAT
#endif

#ifdef USE_OPENGL
#if !defined(OPENGL_NOGRAVITY) && !defined(USE_OPENCL)
#define GGRAV_1x3
#define DENSITY_TYPE        float
#define GRAV_TYPE        signed short
#endif
#endif

#ifndef DENSITY_TYPE
# define DENSITY_TYPE        signed short
#endif

#ifndef GRAV_TYPE
# define GRAV_TYPE        signed short
#endif

#ifndef FIX_DENS_FLOAT
# define FDTYPE                unsigned char
# define FDZERO         0
# define DENS_TO_FIXDENS(d) (unsigned char)(d)
# define FIXDENS_TO_DENS(d) (DENSITY_TYPE)(d)
#else
# define FDTYPE                float
# define FDZERO                0.0f
# define DENS_TO_FIXDENS(d) ((float)(d) / 255.0f)
# define FIXDENS_TO_DENS(d) (DENSITY_TYPE)((d) * 255.0f)
#endif

#define SI_CHARGE(I)        (densArray[SI_OFFSET(I)])
#ifdef USE_OPENCL
#define SET_WALL_DENS(I,D)  { densArray[SI_OFFSET(I)] = D; \
                              fixDensArray[SI_OFFSET(I)] = DENS_TO_FIXDENS(abs(D)); \
                              clFixDensArrayUpdate(); }
                              
#define SET_WALL_DENS_DFL(I)  { densArray[SI_OFFSET(I)] = WALL_CHARGE; \
        fixDensArray[SI_OFFSET(I)] = DENS_TO_FIXDENS(getValue(&particleWeight[wallArrayIndex])); \
        clFixDensArrayUpdate(); }
        
#define ADD_WALL_DENS_DFL(I) { \
    densArray[SI_OFFSET(I)] += WALL_CHARGE; \
    if (densArray[SI_OFFSET(I)] < -255) densArray[SI_OFFSET(I)] = -255;\
    if (densArray[SI_OFFSET(I)] > 255) densArray[SI_OFFSET(I)] = 255;\
    fixDensArray[SI_OFFSET(I)] = DENS_TO_FIXDENS(abs(densArray[SI_OFFSET(I)])); \
    clFixDensArrayUpdate(); }

#define CLR_WALL_DENS(I)  { densArray[SI_OFFSET(I)] = 0; \
    fixDensArray[SI_OFFSET(I)] = FDZERO; \
    clFixDensArrayUpdate(); }
    
#define WALL_MASS_SET(I,W) { fixDensArray[SI_OFFSET(I)] = DENS_TO_FIXDENS(W); \
    clFixDensArrayUpdate(); }
    
#else

#define SET_WALL_DENS(I,D)  { densArray[SI_OFFSET(I)] = D; }
#define SET_WALL_DENS_DFL(I) { densArray[SI_OFFSET(I)] = WALL_CHARGE; }
#define ADD_WALL_DENS_DFL(I) { densArray[SI_OFFSET(I)] += WALL_CHARGE; }
#define CLR_WALL_DENS(I)  { densArray[SI_OFFSET(I)] = 0; }
#define WALL_MASS_SET(I,W) { fixDensArray[SI_OFFSET(I)] = (W); }
#endif

#define WALL_MASS_GET(I) FIXDENS_TO_DENS(fixDensArray[SI_OFFSET(I)])

#define FIELD(I)        (grvArray[gravIdx][SI_OFFSET(I)])
#define FIELD_SH(I)        (grvArray[1-gravIdx][SI_OFFSET(I)])

#define WALL_CHARGE        (getValue(&particleAttractionForce[wallArrayIndex]) * \
                         getValue(&particleWeight[wallArrayIndex]))

#ifndef MAX
#define MAX(a,b)        ((a) > (b) ? (a):(b))
#define MIN(a,b)        ((a) < (b) ? (a):(b))
#endif

#define YSTART(s,y)        ((s)->pitch * (y))
#define SADDR(s,x,y,l)        ((uintptr_t)(s)->pixels + YSTART(s,y) + (x)*(l))
#define SCLIP(s,x,y)        (((unsigned int)(x) < (s)->w) && ((unsigned int)(y) < (s)->h))


#define SURFACE_SET8(s,x,y,v) if (SCLIP(s,x,y)) {*(char*)SADDR(s,x,y,1) = (v);}
#define SURFACE_GET8(s,x,y)  (SCLIP(s,x,y) ? *(char*)SADDR(s,x,y,1) : 0)
#define SURFACE_SET32(s,x,y,v)  if (SCLIP(s,x,y)) {*(int*)SADDR(s,x,y,4) = (v);}
#define SURFACE_GET32(s,x,y) (SCLIP(s,x,y) ? *(int*)SADDR(s,x,y,4) : 0)

#define IMP_LIMIT(imp)        if (unlikely(imp >= MAX_SPEED))\
                        { lostImpuls += imp - MAX_SPEED; imp = MAX_SPEED; }\
                        else if (unlikely(imp <= -MAX_SPEED))\
                        { lostImpuls += -MAX_SPEED - imp; imp = -MAX_SPEED;}


#define MAX_TEMP        0xffff
#define TEMPR(SI)        (SI)->u.tempr[temprIdx]

#ifdef USE_OPENCL
#define SI_SET_TEMPR(SI,t)        clSchedHeatAt (SI_OFFSET(SI), t)
#else
#define SI_SET_TEMPR(SI,t)        (SI)->u.tempr[0] = (SI)->u.tempr[1] = t
#endif

#define FRICTION(pSi)        pSi->friction

#define ACCEL_TO(P,VX,VY) pdescs[PTYPE(P)]->hit (P, (VX) - (P)->vec.d.x, (VY) - (P)->vec.d.y);

#define BRICK_DATA_ENCODE(tempr, dens)        ((unsigned int)((tempr) << 16) | \
                                        ((unsigned int)(dens) & 0x0000ffff))
                                        
#define BRICK_DATA_TEMPR(data)        (((data) >> 16) & 0xffff)
#define BRICK_DATA_DENS(data)        ((data) & 0xffff)

#define ACCEL_DATA_ENCODE(dx, dy)        ((unsigned int)((dx) << 16) | \
                                         ((unsigned int)(dy) & 0x0000ffff))
#define ACCEL_DATA_DX(data)        (((data) >> 16) & 0xffff)
#define ACCEL_DATA_DY(data)        ((data) & 0xffff)

#define HIT(P,VX,VY) pdescs[PTYPE(P)]->hit (P, (int)(VX), (int)(VY));

#define CHECK_DRAWING        {                \
    static int drawing = 0;                \
    if (event == BUTTON_PRESS)                \
        drawing = 1;                        \
    else if ((event == BUTTON_RELEASE) && !(shiftPressed())){        \
        if (!drawing) return;                \
        drawing = 0;                        \
    } else if (!drawing)                \
        return;                                \
}

/* vector operations. MMX provides some nice operations which can be used, and actually do
 * reduce the number of instructions required, BUT for some reason they increase
 * cache and TLB misses, making the execution if fact slower ... !
 */
#undef USE_MMX

#ifdef USE_MMX
#define V_TO_SCREEN(R,T) (R).m =_mm_srli_pi32((T).m, 16)
#define V_TO_INTERN(R,T) (R).m =_mm_slli_pi32((T).m, 16)
#define V_ADD(R,T1,T2)        (R).m = _mm_add_pi32((T1).m, (T2).m)
#define V_SUB(R,T1,T2)        (R).m = _mm_sub_pi32((T1).m, (T2).m)
#define MM_EMPTY    _mm_empty();
#else
#define V_TO_SCREEN(R,T) {(R).d.x = SCREENC((T).d.x); (R).d.y = SCREENC((T).d.y);}
#define V_TO_INTERN(R,T) {(R).d.x = INTERNC((T).d.x); (R).d.y = INTERNC((T).d.y);}
#define V_ADD(R,T1,T2)        {(R).d.x = (T1).d.x + (T2).d.x; (R).d.y = (T1).d.y + (T2).d.y;}
#define V_SUB(R,T1,T2)        {(R).d.x = (T1).d.x - (T2).d.x; (R).d.y = (T1).d.y - (T2).d.y;}
#define MM_EMPTY
#endif

#define MM_EMPTY

//#define IMP_ASSERT(imp)       if (abs(imp) > GRID) printf ("IMP_ASSERTION FAILED: %d (%d)\n", __LINE__, imp);
#define IMP_ASSERT(imp)

#if 1
/* this hurts more than it leads to more performance (very slightly)
 */
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)
#else
#define likely(x)       (x)
#define unlikely(x)     (x)
#endif

#define SI_POLY_MASK        (SI_LINE|SI_IN_POLY|SI_OUTER_LINE)

#define GRAVX(si)   (grvArray[gravIdx][SI_OFFSET(si)+1] - \
                     grvArray[gravIdx][SI_OFFSET(si)-1])*gSgn
#define GRAVY(si)   (grvArray[gravIdx][SI_OFFSET(si)+wid] - \
                     grvArray[gravIdx][SI_OFFSET(si)-wid])*gSgn

/* The GPU already provides the correct acc. vector ..
*/
#ifndef GFACT
#define GFACT 0
#endif

#define GRASTER                (1<<GFACT)
#define GRAVC(c)        ((c)>>(CSHIFT-GFACT))
#define GRAVWID                (wid << GFACT)
#define GRAVHEI                (hei << GFACT)

#if (GFACT == 0)

#ifdef GGRAV_1x3
# define GGRAVX(si,p)   (grvArray[0][SI_OFFSET(si)*3])*p->frcInv
# define GGRAVY(si,p)   (grvArray[0][SI_OFFSET(si)*3+1])*p->frcInv
#elif defined(GGRAV_1x2)
# define GGRAVX(si,p)   (grvArray[0][SI_OFFSET(si)*2])*p->frcInv
# define GGRAVY(si,p)   (grvArray[0][SI_OFFSET(si)*2+1])*p->frcInv
#else
# define GGRAV_2x1
# define GGRAVX(si,p)   (grvArray[0][SI_OFFSET(si)])*p->frcInv
# define GGRAVY(si,p)   (grvArray[1][SI_OFFSET(si)])*p->frcInv
#endif

#else
#define GGRAVX(si,p)   (grvArray[0][GRAVC(p->pos.d.x) + GRAVWID * GRAVC(p->pos.d.y)])*p->frcInv
#define GGRAVY(si,p)   (grvArray[1][GRAVC(p->pos.d.x) + GRAVWID * GRAVC(p->pos.d.y)])*p->frcInv
#endif

#define XACCEL(si)  ((si)->u.accel[0])
#define YACCEL(si)  ((si)->u.accel[1])

#define PART_GRAV(p, si)                                        \
    if (!GETVAL32(particleAttraction) || NO_CHARGE(p))                \
    {                                                                \
        (p)->vec.d.x += XACCEL(si);                                \
        (p)->vec.d.y += YACCEL(si);                                \
    }                                                                \
    else if (options->useGpu)                                                \
    {                                                                \
        if (NEG_CHARGE(p))                                        \
        {                                                        \
            (p)->vec.d.x += -GGRAVX(si,p) + XACCEL(si);                \
            (p)->vec.d.y += -GGRAVY(si,p) + YACCEL(si);                \
        }                                                        \
        else                                                        \
        {                                                        \
            (p)->vec.d.x += GGRAVX(si,p) + XACCEL(si);                \
            (p)->vec.d.y += GGRAVY(si,p) + YACCEL(si);                \
        }                                                        \
    }                                                                \
    else                                                        \
    {                                                                \
        if (NEG_CHARGE(p))                                        \
        {                                                        \
            (p)->vec.d.x += -GRAVX(si) + XACCEL(si);                \
            (p)->vec.d.y += -GRAVY(si) + YACCEL(si);                \
        }                                                        \
        else                                                        \
        {                                                        \
            (p)->vec.d.x += GRAVX(si) + XACCEL(si);                \
            (p)->vec.d.y += GRAVY(si) + YACCEL(si);                \
        }                                                        \
    }


#define SI_OFFSET(SI)        ((int)((SI) - siArray))

#define SET_DELETED(p)        FLAG_CLR(alive,p)
#define SET_ALIVE(p)        FLAG_SET(alive,p)
#define IS_DELETED(p)        !FLAG_ISSET(alive,p)

#define IS_CPU_PARTICLE(p) (pdescs[PTYPE(p)]->isGPUParticle == 0)
#define IS_GPU_PARTICLE(p) (pdescs[PTYPE(p)]->isGPUParticle != 0)

extern int thread_aff_change;
extern int mp_disable;

#ifdef OPENMP
# if defined(INCLUDE_CCSH) || defined(BIND_THREADS)
#  define OMPVARS static int omp_initialized = 0;
#  define OMPDONE if (omp_initialized + 1) omp_initialized = thread_aff_change;
# else
#  define OMPVARS
#  define OMPDONE
# endif
# ifdef INCLUDE_CCSH
#  define OMP_THREAD_NAME(n,i)        {                        \
    char _tname[32];                                        \
    if (!omp_initialized) { sprintf(_tname,"omp_%s%d",n,i);        \
        ccsh_task_self(_tname, 0);}}
# else
#  define OMP_THREAD_NAME(n,i)
# endif
# ifdef BIND_THREADS
#  define OMP_THREAD_INIT_BIND(thread) if (omp_initialized != thread_aff_change){        \
    pthread_setaffinity_np (pthread_self(), tData[thread]->setsize, tData[mp_disable ? 0 : thread]->set); \
    printf ("Thread %d (%p) locked to core mask 0x%x\n", thread, (void*)pthread_self(), *(int*)(tData[mp_disable ? 0 : thread]->set));\
    }
# else
#  define OMP_THREAD_INIT_BIND(...)
#  define OMP_THREAD_INIT(...)
# endif
#else
# define OMP_THREAD_NAME(...)
# define OMPVARS
# define OMPDONE
# define OMP_THREAD_INIT_BIND(...)
# define OMP_THREAD_INIT(...)
# define omp_init_lock(...)
# define omp_lock_t int
#endif

#define BUTTON_PRESS        1
#define BUTTON_MOVE        2
#define BUTTON_RELEASE        3
#define MOUSEIN                4
#define MOUSEOUT        5
#define SCROLLUP        6
#define SCROLLDOWN        7
#define REDRAW                8
#define SCROLLUP_SH        9
#define SCROLLDOWN_SH        10
#define SCROLLUP_CT        11
#define SCROLLDOWN_CT        12
#define SUBPANEL_SELECT        13
#define PROGRESS        14

#define BUTTON_ID(a)        (((a) & 0xf0000) >> 16)
#define EVENT_CODE(a)        ((a) & 0xffff)
#define MKEVENT(e,b)        (((b) << 16) | (e))

#define IS_RESET(action)   ((BUTTON_ID(action) == 3) && (EVENT_CODE(action) == BUTTON_PRESS))
#define DFL_ACTION        MKEVENT(BUTTON_PRESS, 3)

#define BUTTON_WID(b) (b->x2 - b->x1 - 1)
#define BUTTON_HEI(b) (b->y2 - b->y1 - 1)
#define MOUSE_OVER_BUTTON(b,x,y)        ((x > 0) && (x < BUTTON_WID(b)) && (y > 0) && (y < BUTTON_HEI(b)))


#if 0
#define SAVE_MARKER (FUNCPTR)1
#define RESTORE_MARKER (FUNCPTR)2
#define RESTORE_MARKER_H (FUNCPTR)3
#define DOWN_MARKER (FUNCPTR)4
#endif


#if 1
#define PCHARGE(P)        (CHARGE_SIGN(P) * (P)->mass)
#define PMASS(P)        (P)->size

#define CHARGE_SIGN(P)        ((P)->charge)
#define CHARGE_SIGN_CODE(P)        ((P)->charge + 1)
#define NO_CHARGE(P)        (CHARGE_SIGN(P) == 0)
#define NEG_CHARGE(P)        (CHARGE_SIGN(P) == -1)
#define POS_CHARGE(P)        (CHARGE_SIGN(P) == 1)
#define SET_CHARGE(P,D)        (P)->charge = (D)
#define CHARGE_ENCODE(O,C)        (C)
#define SET_FRC_INV(P,D)        p->frcInv = (D) ? -1 : 1

#define CRANGE(P)        (partArray2[PARTICLE_ID(P)].crange)
#define SET_CRANGE(P,D)        partArray2[PARTICLE_ID(P)].crange = (D)

/* particle flags: private use of different particle engines */
#define PFLAG_1a2(P)        (((P)->pflags & 0x03) == 0x3)
#define PFLAG_1o2(P)        ((P)->pflags & 0x03)

#define PFLAG_12VAL(P)        ((P)->pflags & 3)
#define PFLAG_12VAL_SET(P,V)        (P)->pflags = ((P)->pflags & ~0x03) | ((V) & 3)


#define PFLAG1_VAL        0x01
#define PFLAG2_VAL        0x02
#define PFLAG3_VAL        0x04
#define PFLAG4_VAL      0x08
#define PFLAGS_MASK        (PFLAG1_VAL|PFLAG2_VAL|PFLAG3_VAL|PFLAG4_VAL)

#define PFLAG1(P)        ((P)->pflags & PFLAG1_VAL)
#define PFLAG2(P)        ((P)->pflags & PFLAG2_VAL)
#define PFLAG3(P)        ((P)->pflags & PFLAG3_VAL)
#define PFLAG4(P)        ((P)->pflags & PFLAG4_VAL)

#define PFLAG1_SET(P)        (P)->pflags |= PFLAG1_VAL
#define PFLAG2_SET(P)        (P)->pflags |= PFLAG2_VAL
#define PFLAG3_SET(P)        (P)->pflags |= PFLAG3_VAL
#define PFLAG4_SET(P)        (P)->pflags |= PFLAG4_VAL

#define PFLAG1_CLR(P)        (P)->pflags &= ~PFLAG1_VAL
#define PFLAG2_CLR(P)        (P)->pflags &= ~PFLAG2_VAL
#define PFLAG3_CLR(P)        (P)->pflags &= ~PFLAG3_VAL
#define PFLAG4_CLR(P)        (P)->pflags &= ~PFLAG4_VAL

#define PFLAGS_CLR(P)        (P)->pflags = 0

#define SCR_OFF(x,y)        ((x)+(y)*wid)
#define SCR_IOFF(x,y)        (SCREENC(x)+SCREENC(y)*wid)

typedef uint32_t       FT_TYPE;
#define FT_SHIFT       5
#define FT_MASK        0x1f

/* operations on flag field F (one bit information per particle) */
#define FLAG_ISSET(F,P)        (((F)[PARTICLE_ID(P)>>FT_SHIFT] & (1 << (PARTICLE_ID(P) & FT_MASK))) != 0)
#define FLAG_SET(F,P)        (F)[PARTICLE_ID(P)>>FT_SHIFT] |= (1 << (PARTICLE_ID(P) & FT_MASK))
#define FLAG_CLR(F,P)        (F)[PARTICLE_ID(P)>>FT_SHIFT] &= ~(1 << (PARTICLE_ID(P) & FT_MASK))
#define FLAG_CLRALL(F)        memset ((char*)F, 0, (options->maxParticles+7)/8)

/* operations on screen flag field F (one bit information per screen pixel) */
#define SIFLAG_ISSET(F,O)        (((F)[(O)>>FT_SHIFT] & (1 << ((O) & FT_MASK))) != 0)
#define SIFLAG_SET(F,O)                (F)[(O)>>FT_SHIFT] |= (1 << ((O) & FT_MASK))
#define SIFLAG_CLR(F,O)                (F)[(O)>>FT_SHIFT] &= ~(1 << ((O) & FT_MASK))
#define SIFLAG_CLRALL(F)        memset ((char*)F, 0, (wid*hei+7)/8)

#define XFLAG(P)        pflags[PARTICLE_ID(P)]

#define SET_MASS(P,S)        (P)->mass = S;
#define MAX_MASS    255

#else
#define WEIGHT_CODE(P)        ((P)->weight & 3)
#define WEIGHT(P)        1
#define SET_WEIGHT(P,W)        (P)->weight = ((P)->weight & ~3) | ((W)-1)
#define MAX_WEIGHT        3
#endif

#if 0
#define SET_NOHIT(P)        ((P)->weight |= 4)
#define CLR_NOHIT(P)        ((P)->weight &= ~4)
#define NOHIT(P)        ((P)->weight & 4)
#else
#define SET_NOHIT(P)
#define CLR_NOHIT(P)
#define NOHIT(P)        0

#endif

#define PTYPE(P)                (P)->type
#define SET_PTYPE(P,T)                (P)->type = (T)

#ifdef SI_CALC
#define GET_SCREENINFO(P) (&siArray[SCREENC(P->pos.d.x) + SCREENC(P->pos.d.y)*wid])
#define SET_SCREENINFO(P, SI)
#else
#define GET_SCREENINFO(P)   P->pSi
#define SET_SCREENINFO(P, SI)   P->pSi = SI
#endif


#define SCR_SINK        (particle*)1
#define SCR_BRICK        (particle*)2
#define SCR_NONE        (particle*)0

#define IS_BRICK(SI)        ((SI)->particleList == SCR_BRICK)

#ifdef USE_OPENCL
#define CL_SET_WALL(x,y)   clWallSet(x,y)
#else
#define CL_SET_WALL(x,y) 
#endif


#define GRAV_SHOW


#define INTERVAL_RUN(interval,fct)                    \
{                                                    \
    static uint32 lastTicks = 0;                    \
    if ((planschTime() - lastTicks) > interval)    \
    {                                                    \
        fct;                                            \
        lastTicks = planschTime();                    \
    }                                                    \
}                                                    





#define NORM_SIMPLE            0
#define NORM_SIMPLE_SAVE    1
#define NORM_VECTOR            2

#define SI_QUAD_NEG(SI,D)        ((screeninfo*)(((uintptr_t)(SI)) + gd.quadSiNeg[D]))
#define SI_QUAD(SI,D)        ((screeninfo*)(((uintptr_t)(SI)) + gd.quadSi[D]))
#define SI_DIR(SI,D)        ((screeninfo*)(((uintptr_t)(SI)) + gd.dirSi[D]))
#define SI2Y(si)        (((uintptr_t)(si)-(uintptr_t)(siArray)) / (wid*sizeof(screeninfo)))
#define SI2X(si)        ((((uintptr_t)(si)-(uintptr_t)(siArray)) / sizeof(screeninfo)) % wid)

#define QUAD2DIR(quad)        ((0x07d63688 >> (((quad) * 3))) & 7)
//#define QUAD2DIR(quad)        ((quad > 3) ? quad-1:quad)
#define DIR2QUAD(dir)        (((dir) < 4) ? (dir) : ((dir)+1))

#define SI_OFF(SI,dx,dy)    

#if !defined(_MINGW)
#define BACKTRACE                                        \
       {                                                \
           int j, nptrs;                                \
           void *buffer[1000];                                \
            char **strings;                                \
           nptrs = backtrace(buffer, 1000);                \
           strings = backtrace_symbols(buffer, nptrs);        \
           if (strings == NULL) {                        \
               perror("backtrace_symbols");                \
           } else {                                        \
           printf ("Backtrace:\n");                        \
           for (j = 0; j < nptrs; j++)                        \
               printf("|  %s\n", strings[j]);                \
           free(strings);                                \
            }                                                \
       }
#else
#define BACKTRACE
#endif



#define NO_THREADLIST 1
#define NO_CQ 2
#define DENS_UPDATE 4

#define STENCIL_DIR  "stencils"
#define STENCIL_FILE "plansch_%d"

#define TAPE_DIR    "tapes"
#define TAPE_FILE   "plansch_%d"

#define NO_WAIT        0

#if (DEBUG == 1)
#define debug(fmt, ...) printf ("%s:%d " fmt, __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define debug(...)
#endif

#if (DEBUG == 1)
#define DBG_PIXEL(x,y,col)        setPixel(x,y,col)
#define DBG_MARKER(x,y,rad,col)        dbgMarker(x,y,rad,col,NULL)
#define DBG_MARKER_N(x,y,rad,col,n) dbgMarker(x,y,rad,col,n)
#define DBG_BOX(x,y,rad,col,n)        dbgMarkerBox(x,y,rad,col,n)
#define DBG_LINE(x,y,x1,y1,col,n)        dbgLine(x,y,x1,y1,col,n)
#define CLEAR_MARKERS(n)                clearMarkers(n);
#else
#define DBG_PIXEL(x,y,col) 
#define DBG_MARKER(x,y,rad,col)        
#define DBG_MARKER_N(x,y,rad,col,n)
#define DBG_BOX(x,y,rad,col,n)        
#define CLEAR_MARKERS(n)        
#define DBG_MARKER(x,y,rad,col)
#define DBG_LINE(x,y,x1,y1,col,n)
#endif


/* drawCircle flags */
#define DRELEM_FIRST        1
#define DRELEM_LAST        2

/* flood modes */
#define FLOOD_MODE_SCR_4        0
#define FLOOD_MODE_SCR_8        1
#define FLOOD_MODE_INT_HEX        2

#define LEFT_SIDE   0
#define TOP_SIDE    1
#define BOT_SIDE    2
#define RIGHT_SIDE  3


#if defined(_MINGW)
#define MADVISE(P,S,A)
#else
#define MADVISE(P,S,A)        madvise((void*)P, (size_t)S, A)
#endif

#include "types.h"
#include "plmath.h"
#include "globals.h"
#include "colors.h"

#include "protos.h"
#include "inline.h"
#include "settings.h"

#ifdef __cplusplus
}
#endif
