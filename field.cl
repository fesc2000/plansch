/*
Copyright (C) 2011 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/
#include "clprivate.clh"

/* Calculate new field value
 *
 * pixelDens - The current density on the field element (sum of all particles' and walls' charge*mass)
 * target    - Target potential (density plus averaged 8 neighbour field values)
 * oldField  - Current field state
 * wallMass  - Wall mass (0..1), defines permeability of field at this point (1 = no permeability, locked to pixelDens)
 * gravStrength - Multiplicator for oldField (field range)
 * gravDamping  - Damping of field acceleration
 * gravWaveDamping - wave damping
 * 
 * Returns: new field state (potential = .x / change rate = .y)
 */
inline FTYPE2 newFieldValue( FTYPE pixelDens,
        FTYPE target, 
        FTYPE2 oldField, 
        FTYPE wallMass, 

        const FTYPE gravStrength_, 
        const FTYPE gravDamping_, 
        const FTYPE gravWaveDamping_)
{
    FTYPE2 nf;

    /* weight factor */
    target *= gravStrength_;

    /* Calculate field accellaration towards target value and set new field change rate 
     */
    nf.y = MAD((target - oldField.x), gravDamping_, oldField.y);

    /* new field value */
    nf.x = ((oldField.x + nf.y) * gravWaveDamping_);

    /* a wall has a density of 255 (no field permeability) to 1 (nearly full permeability)
     * No permeability means field waves are reflected to 100%
     * This is indenpendent from the wall's charge (i.e. walls with no charge also
     * affect the field).
     */
    nf.x = MAD(nf.x, ((FTYPE)1.0f-wallMass), wallMass * pixelDens);

    return nf;
}

/* calculate change rage of gravity field (8 neghbouring pixels).
 *
 * The field buffer consists of two floats (x/y) per pixel: 
 * field.x is the current potential
 * field.y is the current change rate
 *
 * the dens buffer contains the current density of a pixel, determined by the
 * particles and walls currently within the pixel. Each particle wall adds it's
 * charge * mass to the dens buffer (charge is -1, 0 or 1), mass is 1 .. 255.
 *
 * The field attempts to obtain a potential identical to the density, i.e. it is 
 * accelerated towards the density.
 *
 * Three parameters influence the field iteration:
 * gravStrength - Influences the weight of the neighbouring field values, thereby 
 *                determining the gravity strength.
 * gravDamping -  Factor for the accelleration of the field towards the target potential.
 *                Determines the damping of gravity waves.
 * gravWaveDamping - Influences the oscillation frequency of a field scalar.
 *
 * No use of local memory
 *
 * Z variant: Screen border forces field to 0
 * I variant: Screen border does not cut field (inaccurate)
 */
KERNEL_F2(fieldIterateZ) (global FTYPE2 *newField,
            global DENSITY_TYPE *dens,
            global FTYPE2 *field,
            global FDTYPE  *fixDens,
            constant clParameters_t *params
            )
{
    DECLARE_GBL_X
    DECLARE_GBL_Y
    DECLARE_GBL_W
    DECLARE_GBL_H
    FTYPE tmp;
    FTYPE d;
    int tid = TID2;

    if ((uint)(GBL_X-1) >= (SCR_WID-2) || (uint)(GBL_Y-1) >= (SCR_HEI-2))
    {
        /* cut field */
        newField[tid].x = (FTYPE)0.0f;
        return;
    }

    /* the target potential is the average of the current density and the potentional
     * of the surrounding 8 field values
     */
    tmp = d = CONVERT_FTYPE(dens[tid]) * 0.1f;

    /* get neighbour values. 
     */
    uint up = tid-GBL_W;
    uint down = tid+GBL_W;
    uint left = tid-1;
    uint right = tid+1;

    tmp += field[left].x;
    tmp += field[right].x;
    tmp += field[up].x;
    tmp += field[down].x;

    tmp = MAD(field[up-1].x + field[up+1].x + field[down-1].x + field[down+1].x, (FTYPE)0.5f, tmp);

    newField[tid] = 
        newFieldValue (d, tmp, field[tid], 
                       FIX_DENS_TO_FTYPE(fixDens[tid]), 
                       FIELD_STRENGTH(params), 
                       FIELD_DAMPING(params), 
                       FIELD_WAVE_DAMPING(params));
}

KERNEL_F2(fieldIterateI) (global FTYPE2 *newField,
            global DENSITY_TYPE *dens,
            global FTYPE2 *field,
            global FDTYPE  *fixDens,
            constant clParameters_t *params
            )
{
    DECLARE_GBL_X
    DECLARE_GBL_Y
    DECLARE_GBL_W
    DECLARE_GBL_H
    FTYPE tmp;
    FTYPE d;
    int tid = TID2;

    if ((uint)(GBL_X-1) >= (SCR_WID-2) || (uint)(GBL_Y-1) >= (SCR_HEI-2))
    {
#if 0
        /* do not cut field, border pixel potential is set identical to adjacent
         * pixel inwards.
         * The offset to the inwards pixel is encoded in .y element of border field
         * tuple
          */
        int inOff = (int)field[tid].y;
        int inOff2 = (inOff == 0) ? 0 : (abs(inOff == 1) ? SCR_WID : 1);
        FTYPE inField = field[tid + inOff].x;
        FTYPE guessedOffset = (inField * 2.0f - field[tid + inOff + inOff2].x - field[tid + inOff - inOff2].x) * 0.5f;

        newField[tid].x = (inField - guessedOffset)*0.95f;
#else
        newField[tid].x = field[tid + (int)field[tid].y].x * params->fieldBorderCutoff;
#endif
        return;
    }

    /* the target potential is the average of the current density and the potentional
     * of the surrounding 8 field values
     */
    tmp = d = CONVERT_FTYPE(dens[tid]) * 0.1f;

    /* get neighbour values. 
     */
    uint up = tid-GBL_W;
    uint down = tid+GBL_W;
    uint left = tid-1;
    uint right = tid+1;

    tmp += field[left].x;
    tmp += field[right].x;
    tmp += field[up].x;
    tmp += field[down].x;

    tmp = MAD(field[up-1].x + field[up+1].x + field[down-1].x + field[down+1].x, (FTYPE)0.5f, tmp);

    newField[tid] = 
        newFieldValue (d, tmp, field[tid], 
                       FIX_DENS_TO_FTYPE(fixDens[tid]), 
                       FIELD_STRENGTH(params), 
                       FIELD_DAMPING(params), 
                       FIELD_WAVE_DAMPING(params));
}

/* compute the accelleration vectors on a pixel based on the gravity differences to
 * the neighbouring pixels. The resulting arrays are passed back to the particle engine.
 */
KERNEL_F2(calcDiff) (global FTYPE2 *field,
                        global FTYPE2 *diff,
                        __local FTYPE *c,
                        global int2 *idiff,
                        global FTYPE2 *accel,
                        const int forHost)
{
    uint x = get_global_id(0);
    uint y = get_global_id(1);
    uint wid = get_global_size(0);
    uint hei = get_global_size(1);
    uint tid = GBL_OFF_2D(x, y);
    uint lx = get_local_id(0);
    uint ly = get_local_id(1);
    uint lwid = get_local_size(0);
    uint lhei = get_local_size(1);
    uint ltid = LCL_OFF_2D(lx, ly);
    uint up, down, left, right;
    FTYPE2 fd;

    c[ltid] = field[tid].x;
    barrier (CLK_LOCAL_MEM_FENCE);

    if ((x-1) >= (SCR_WID-2) || (y-1) >= (SCR_HEI-2))
    {
        /* No accelleration vector on borders */
        fd = (FTYPE2)(0.f,0.f);
    }
    else
    {
#if 0
        up = tid-wid;
        down = tid+wid;
        left = tid-1;
        right = tid+1;
        fd.x = (field[right].x - field[left].x) * 2.f;
        fd.x += (field[right-wid].x - field[left-wid].x);
        fd.x += (field[right+wid].x - field[left+wid].x);
        fd.y = (field[down].x - field[up].x) * 2.f;
        fd.y += (field[down-1].x - field[up-1].x);
        fd.y += (field[down+1].x - field[up+1].x);
#endif

#if 0
        if ((lx == 0) || (ly == 0) || (lx == lwid-1) || (ly == lhei-1))
        {
            up = tid-wid;
            down = tid+wid;
            left = tid-1;
            right = tid+1;
            fd.x = (field[right].x - field[left].x) * 2.f;
            fd.x += (field[right-wid].x - field[left-wid].x);
            fd.x += (field[right+wid].x - field[left+wid].x);
            fd.y = (field[down].x - field[up].x) * 2.f;
            fd.y += (field[down-1].x - field[up-1].x);
            fd.y += (field[down+1].x - field[up+1].x);
        }
        else
        {
            up = ltid-lwid;
            down = ltid+lwid;
            left = ltid-1;
            right = ltid+1;

            fd.x = (c[right] - c[left]) * 2.f;
            fd.x += (c[right-lwid] - c[left-lwid]);
            fd.x += (c[right+lwid] - c[left+lwid]);

            fd.y = (c[down] - c[up]) * 2.f;
            fd.y += (c[down-1] - c[up-1]);
            fd.y += (c[down+1] - c[up+1]);
        }
#endif
        up = tid-wid;
        down = tid+wid;
        left = tid-1;
        right = tid+1;

        if (lx == 0)
            fd.x = (c[ltid+1] - field[left].x) * 2.f;
        else if (lx == lwid-1)
            fd.x = (field[right].x - c[ltid-1]) * 2.f;
        else
            fd.x = (c[ltid+1] - c[ltid-1]) * 2.f;

        fd.x += (field[up+1].x - field[up-1].x);
        fd.x += (field[down+1].x - field[down-1].x);
        fd.y = (field[down].x - field[up].x) * 2.f;
        fd.y += (field[down-1].x - field[up-1].x);
        fd.y += (field[down+1].x - field[up+1].x);

        fd *= (FTYPE2)(0.25f, 0.25f);
    }

    diff[tid] = fd + accel[tid];

    /* The GPU engine works with floats, the CPU engine with integers.
     * If data is to be passed back to the CPU particle engine, clamp and convert it to integers.
     */
    if (forHost)
        idiff[tid] = convert_int2_rte(clamp((FTYPE2)fd, (FTYPE2)(-64000.f, -64000.f), (FTYPE2)(64000.f,64000.f)));
}

/* compute the accelleration vectors on a pixel based on the gravity differences to
 * the neighbouring pixels. The resulting arrays are passed back to the particle engine.
 *
 * No use of local memory
 */
KERNEL_F2(calcDiffnl) (global FTYPE2 *field,
                          global FTYPE2 *diff,
                          global int2 *idiff,
                            global FTYPE2 *accel,
                          const int forHost)
{
    uint x = get_global_id(0);
    uint y = get_global_id(1);
    uint tid = GBL_OFF_2D(x, y);
    uint up, down, left, right;
    FTYPE2 fd;

    if ((x-1) >= (SCR_WID-2) || (y-1) >= (SCR_HEI-2))
    {
        /* No accelleration vector on borders */
        fd = (FTYPE2)(0.f,0.f);
    }
    else
    {
        up = tid-SCR_WID;
        down = tid+SCR_WID;
        left = tid-1;
        right = tid+1;
        fd.x = (field[right].x - field[left].x) * 2.f;
        fd.x += (field[right-SCR_WID].x - field[left-SCR_WID].x);
        fd.x += (field[right+SCR_WID].x - field[left+SCR_WID].x);
        fd.y = (field[down].x - field[up].x) * 2.f;
        fd.y += (field[down-1].x - field[up-1].x);
        fd.y += (field[down+1].x - field[up+1].x);

        fd = fd * (FTYPE2)(0.25f, 0.25f);
    }

    diff[tid] = fd + accel[tid];

    if (forHost)
        idiff[tid] = convert_int2_rte(clamp((FTYPE2)fd, (FTYPE2)(-64000.f, -64000.f), (FTYPE2)(64000.f,64000.f)));

}

/* Transfer field into a OpenGL Texture.
 * Positive charge goes to red channel, negative to blue, 
 * change rate (absolute) to green.
 */
KERNEL_F2(fieldToTex) (global FTYPE2 *field,
            global FTYPE2 *diff,
            global float *tempr,
            __write_only image2d_t tex,
            const float gm,
            const float cm,
            const float alpha)
{
    FTYPE2 f = field[TID2];
    float tmp;

    tmp = (f.x + tempr[TID2]) * gm;

    write_imagef (tex, (int2)(get_global_id(0),get_global_id(1)), (float4)(tmp, fabs(f.y) * cm, -tmp, alpha));
}

/* Transfer field into a OpenGL Texture.
 * Positive charge goes to red channel, negative to blue, 
 * change rate (absolute) to green.
 * Show field lines.
 */
KERNEL_F2(fieldToTexLines) (global FTYPE2 *field,
            global FTYPE2 *diff,
            global float *tempr,
            __write_only image2d_t tex,
            const float gm,
            const float cm,
            const float alpha)
{
    float m;
    FTYPE2 f = field[TID2];
    FTYPE2 f1 = field[TID2-1];
    FTYPE2 f2 = field[TID2+1];
    FTYPE2 f3 = field[TID2-SCR_WID];
    FTYPE2 f4 = field[TID2+SCR_WID];
    int i = (int)(f.x * gm);
    int i1 = (int)(f1.x * gm);
    int i2 = (int)(f2.x * gm);
    int i3 = (int)(f3.x * gm);
    int i4 = (int)(f4.x * gm);
    float tmp = f.x * gm;

    m = ((i < i1) | (i < i2) | (i < i3) | (i < i4)) ? 1.0f : 0.0f;

    write_imagef (tex, (int2)(get_global_id(0),get_global_id(1)), (float4)(m/tmp, fabs(f.y) * cm, -m/tmp, alpha));
}

/* Transfer field into a OpenGL Texture.
 * Positive charge goes to red channel, negative to blue, 
 * change rate (absolute) to green.
 *
 * This is for the case that the data nis NOT shared directly with OpenGl
 * and rendered into a buffer instead of a texture.
 */
KERNEL_F2(fieldToTexBuf) (global FTYPE2 *field,
            global FTYPE2 *diff,
            global float *tempr,
            global uchar4 *tex,
            const float gm,
            const float cm,
            const float alpha)
{
    int x = get_global_id(0);
    int y = get_global_id(1);
    int tid = GBL_OFF_2D(x,y);
    FTYPE2 f = field[tid];
    float tmp;
    float4 tmp4;

    tmp = f.x * gm;

    tmp4 = (float4)(tmp, fabs(f.y) * cm, -tmp, alpha) * (float4)(255.f, 255.f, 255.f, 255.f);

    tex[tid] = convert_uchar4_sat(tmp4);
}


/* Transfer field into a OpenGL Texture.
 * Positive charge goes to red channel, negative to blue, 
 * change rate (absolute) to green.
 * Show field lines.
 *
 * This is for the case that the data nis NOT shared directly with OpenGl
 * and rendered into a buffer instead of a texture.
 */
KERNEL_F2(fieldToTexLinesBuf) (global FTYPE2 *field,
            global FTYPE2 *diff,
            global float *tempr,
            global uchar4 *tex,
            const float gm,
            const float cm,
            const float alpha)
{
    int x = get_global_id(0);
    int y = get_global_id(1);
    int tid = GBL_OFF_2D(x, y);
    float4 col;
    float m;
    FTYPE2 f = field[tid];
    FTYPE2 f1 = field[tid-1];
    FTYPE2 f2 = field[tid+1];
    FTYPE2 f3 = field[tid-SCR_WID];
    FTYPE2 f4 = field[tid+SCR_WID];
    int i = (int)(f.x * gm);
    int i1 = (int)(f1.x * gm);
    int i2 = (int)(f2.x * gm);
    int i3 = (int)(f3.x * gm);
    int i4 = (int)(f4.x * gm);
    float tmp = f.x * gm;

    m = ((i < i1) | (i < i2) | (i < i3) | (i < i4)) ? 1.0f : 0.0f;

    col = (float4)(m/tmp, fabs(f.y) * cm, -m/tmp, alpha) * (float4)(255.f, 255.f, 255.f, 255.f);

    tex[tid] = convert_uchar4_sat(col);
}

KERNEL_I(getFieldStats) (
    global FTYPE2         *field,
    global float          *tempr,
    global clStatus_t     *stats
    )
{
    local clFieldStats_t lstats;
    
    if (LID0 == 0)
    {
        lstats.energy = 0.0f;
        lstats.potential = 0.0f;
        lstats.temperature = 0.0f;
    }
    barrier (CLK_LOCAL_MEM_FENCE);
    
    atomic_add_local (&lstats.potential, fabs(field[GID0].x));
    atomic_add_local (&lstats.energy, fabs(field[GID0].y));
    atomic_add_local (&lstats.temperature, tempr[GID0]);

    barrier (CLK_LOCAL_MEM_FENCE);
    
    if (LID0 == 0)
    {
        atomic_add_global (&stats->fieldStats.potential, lstats.potential);
        atomic_add_global (&stats->fieldStats.energy, lstats.energy);
        atomic_add_global (&stats->fieldStats.temperature, lstats.temperature);
    }
}

KERNEL_F2(iterateWallTempr) (global float *temprIn,
        global float *temprOut,
        global FDTYPE  *fixDens
        )
{
    DECLARE_GBL_X
    DECLARE_GBL_Y
    int tid = GBL_OFF_2D(GBL_X,GBL_Y);
    FDTYPE dens;
    unsigned int center;
    float w;
    int count;
#ifdef FIX_DENS_FLOAT
    float dens_sum;
#else
    unsigned int dens_sum;
#endif
    float dens_sum_flt;

    center = tid;

    if (fixDens[center] == FDZERO)
    {
        temprOut[center] = 0.0f;
        return;
    }
    
    count = 1;
    dens_sum = fixDens[center];
    w = temprIn[center];

#define T_ADD(o)                            \
    if (o < GBL_W*GBL_H) {                  \
    dens = fixDens[o];                             \
    if (dens > FDZERO)  {                   \
        w += temprIn[o];                    \
        dens_sum += dens;                    \
        count ++;                            \
    }}

    T_ADD(center - GBL_W - 1);
    T_ADD(center - GBL_W);
    T_ADD(center - GBL_W + 1);
    T_ADD(center - 1);
    T_ADD(center + 1);
    T_ADD(center + GBL_W - 1);
    T_ADD(center + GBL_W);
    T_ADD(center + GBL_W + 1);

    dens_sum_flt = FIX_DENS_TO_FLOAT(dens_sum) / (float)count;

    temprOut[center] = temprIn[center] + ((w / (float)count) - temprIn[center]) * dens_sum_flt;
}


/* OpenCL 2.0 variant with device queue:
 */
KERNEL_F2(fieldRunZ) (
             global FTYPE2 *newField
            ,global DENSITY_TYPE *dens
            ,global FTYPE2 *field
            ,global FDTYPE  *fixDens
            ,global FTYPE2 *diff
            ,global int2 *idiff
            ,global FTYPE2 *accel
            ,constant clParameters_t *params
            ,const     int iterations
            ,const     int forHost
)
{
#ifdef HAVE_OCL2
    const size_t r[2] = {SCR_WID, SCR_HEI}; 
    ndrange_t ndrange;
    int i;

    ndrange = ndrange_2D (r);

    if (GID0 == 0)
    {
        for (i = 0; i < iterations; i++)
        {
            enqueue_kernel (get_default_queue(),
                    0,
                    ndrange,
                    ^{fieldIterateZ(newField, dens, field, fixDens, params);});

            enqueue_kernel (get_default_queue(),
                    0,
                    ndrange,
                    ^{fieldIterateZ(field, dens, newField, fixDens, params);});
        }
#if 1
        /* TODO: does not execute for high iteration counts ... */
        enqueue_kernel (get_default_queue(),
                CLK_ENQUEUE_FLAGS_WAIT_KERNEL,
                ndrange,
                ^{calcDiffnl(field, diff, idiff, accel, forHost);});
#endif
    }
#endif
}


KERNEL_F2(fieldRunI) (
             global FTYPE2 *newField
            ,global DENSITY_TYPE *dens
            ,global FTYPE2 *field
            ,global FDTYPE  *fixDens
            ,global FTYPE2 *diff
            ,global int2 *idiff
            ,global FTYPE2 *accel
            ,constant clParameters_t *params
            ,const     int iterations
            ,const     int forHost
)
{
#ifdef HAVE_OCL2
    const size_t r[2] = {SCR_WID, SCR_HEI}; 
    ndrange_t ndrange;
    int i;

    ndrange = ndrange_2D (r);

    if (GID0 == 0)
    {
        for (i = 0; i < iterations; i++)
        {
            enqueue_kernel (get_default_queue(),
                    0,
                    ndrange,
                    ^{fieldIterateI(newField, dens, field, fixDens, params);});

            enqueue_kernel (get_default_queue(),
                    0,
                    ndrange,
                    ^{fieldIterateI(field, dens, newField, fixDens, params);});
        }
#if 1
        /* TODO: does not execute for high iteration counts ... */
        enqueue_kernel (get_default_queue(),
                CLK_ENQUEUE_FLAGS_WAIT_KERNEL,
                ndrange,
                ^{calcDiffnl(field, diff, idiff, accel, forHost);});
#endif
    }
#endif
}


