
## Library search paths
#
#LIBDIRS += -L$(SDLROOT)/lib 

## Include directories
#
INCDIRS += -I$(SDLROOT)/include/SDL 
INCDIRS += -I$(SDLROOT)/include

CFLAGS += -DVARIANT=\"$(VARIANT)/$(MACH)\"

TARGET=plansch_$(VARIANT)$(exesuffix)

OBJDIR=		$(ARCHDIR)/$(VARIANT)

GRAVITY_TYPES	= spreadSS1 spreadSS1Scr spreadSS1_s3 spreadSS1Scr_s3

PARTICLE_OBJECTS	= $(patsubst %,particle_%.o,$(PARTICLE_TYPES))

GRAVITY_OBJECTS		= $(patsubst %,gravity_%.o,$(GRAVITY_TYPES))

GENMOVE_OBJECTS		= $(GENMOVE_TYPES:%=genmove_%.o)


OBJECTS =      $(PARTICLE_OBJECTS) $(GRAVITY_OBJECTS) $(APPL_OBJECTS) $(MOD_OBJECTS) $(GENMOVE_OBJECTS) settings.o pixmaps.o

BUILDOBJECTS = $(addprefix $(OBJDIR)/, $(OBJECTS))


-include $(wildcard $(OBJDIR)/*.d)

CCOMP   = echo "CC  $(3)"; $(CC) $(CFLAGS) $(INCDIRS) $(OFLAGS) $(1) -c $(2) -o $(3) && $(CC) -MM  $(CFLAGS) $(INCDIRS) $(OFLAGS) $(1) $(2) | sed -e 's!.*\.o:!$(3):!' > $(3:%.o=%.d)
CXXCOMP = echo "CXX $(3)"; $(CXX) $(CFLAGS) $(INCDIRS) $(OFLAGS) $(1) -c $(2) -o $(3) && $(CXX) -MM  $(CFLAGS) $(INCDIRS) $(OFLAGS) $(1) $(2) | sed -e 's!.*\.o:!$(3):!' > $(3:%.o=%.d)



$(OBJDIR)/gravity_spreadSS1.o:	gravity2.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=spreadSS1,$<,$@)

$(OBJDIR)/gravity_spreadSS1Scr.o:	gravity2.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=spreadSS1Scr -DSCREENOUT,$<,$@)

$(OBJDIR)/gravity_spreadSS1_s3.o:	gravity2.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-mssse3 -DFCT=spreadSS1_s3 -DUSE_SSSE3,$<,$@)

$(OBJDIR)/gravity_spreadSS1Scr_s3.o:	gravity2.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-mssse3 -DFCT=spreadSS1Scr_s3 -DUSE_SSSE3 -DSCREENOUT,$<,$@)

$(OBJDIR)/genmove_gas_sse2.o:	genmove.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_gas_sse2 -D_GAS -D_SSE2,$<,$@)

$(OBJDIR)/genmove_gas_sse41.o:	genmove.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_gas_sse41 -D_GAS -D_SSE41 -msse4,$<,$@)

$(OBJDIR)/genmove_gas_avx.o:	genmove.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_gas_avx -D_GAS -D_AVX -mavx,$<,$@)

$(OBJDIR)/genmove_gas_c.o:	genmove.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_gas_c -D_GAS,$<,$@)

$(OBJDIR)/genmove_chain_sse2.o:	genmove.c chain.h
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_chain_sse2 -D_CHAIN -D_SSE2,$<,$@)

$(OBJDIR)/genmove_chain_sse41.o:	genmove.c chain.h
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_chain_sse41 -D_CHAIN -D_SSE41 -msse4,$<,$@)

$(OBJDIR)/genmove_chain_avx.o:	genmove.c chain.h
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_chain_avx -D_CHAIN -D_AVX -mavx,$<,$@)

$(OBJDIR)/genmove_chain_c.o:	genmove.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DFCT=move_chain_c -D_CHAIN,$<,$@)

$(OBJDIR)/%.o:	%.c
		@mkdir -p $(OBJDIR)
		@$(call CCOMP,-DMOD=$(patsubst particle_%.c,%,$<),$<,$@)


clean:		
		$(RM) -f $(OBJDIR)/*.o

$(PARTICLE_OBJECTS): genmove.c do_move.c

$(OBJECTS):	$(DEPS)



$(TARGET):	$(BUILDOBJECTS) 
		$(CC) $(CFLAGS) -o $(TARGET) $(BUILDOBJECTS) $(LIBDIRS) $(LIBS)
		@mkdir -p bin/$(KERNEL)/$(MACH)
		cp $(TARGET) plansch_$(VARIANT)_$(MACH)$(exesuffix) 

all:		$(TARGET)


