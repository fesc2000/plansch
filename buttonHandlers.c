/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define ARRAY_BORDER        8

typedef struct 
{
        setting *s;
        button *b;
        int disable;
} bsDep_s;

int maxButtonDependencies = 0;
int numButtonDependencies = 0;
static bsDep_s *buttonDependencies = NULL;

static int getSPixel (int x, int y, int h)
{
    if (y < h-3)
    {
        if (x < ARRAY_BORDER-4)
            return 0;

        return ((x == ARRAY_BORDER-4) || (x == ARRAY_BORDER-2)) ? COL_ICONDRAW : COL_GRAY;
    }
    else if (y == h-3)
    {
        if (x < ARRAY_BORDER-5)
            return 0;

        return ((x == ARRAY_BORDER-5) || (x == ARRAY_BORDER-2)) ? COL_ICONDRAW : COL_GRAY;
    }
    else if (y == h-2)
    {
        if (x < ARRAY_BORDER-6)
            return 0;

        return ((x == ARRAY_BORDER-6) || (x == ARRAY_BORDER-3)) ? COL_ICONDRAW : COL_GRAY;
    }
    else if (y == h-1)
    {
        if (x < ARRAY_BORDER-7)
            return 0;

        return ((x == ARRAY_BORDER-7) || (x == ARRAY_BORDER-4) || (x == ARRAY_BORDER-1)) ? COL_ICONDRAW : COL_GRAY;
    }
    if (x < ARRAY_BORDER-8)
        return 0;

    return ((x == ARRAY_BORDER-8) || (x == ARRAY_BORDER-5) || (x == ARRAY_BORDER-2)) ? COL_ICONDRAW : COL_GRAY;
}

static void addArrayBorder (SDL_Rect *r, int array)
{
    int x, y;
    int sPixel;
    int c;
    int sx, sy;

    for (y = 0; y < r->h; y++)
    {
        sy = r->y + y;
        for (x = 0; x < ARRAY_BORDER; x++)
        {
            sx = r->x + r->w - ARRAY_BORDER + x;

            sPixel = getSPixel (x, y, r->h);
            c = SCR_GET(sx, sy);

            if (sPixel)
            {
                if (array == 2)
                {
                    AVG_TO(c, sPixel, 3);
                    CH_SET_A(c, 0xff);
                }
                else
                {
                    c = sPixel;
                }
            }
            SCR_SET_SAFE(sx, sy, c);
        }
    }
}

/* fill button up to y-offset maxy.
 * arrayMode:
 * 0 : no array mode
 * 1 : array mode, not enabled
 * 2 : array mode, enabled
 */
void fillButton (button *b, int miny, int maxy, int color, int array)
{
    SDL_Rect r;

    if (!b->visible)
        return;

    r.x = b->x1 + 1;
    r.y = b->y1 + 1 + miny;
    r.w = BUTTON_WID(b);
    r.h = maxy - miny;

    SDL_FillRect (screen, &r, color);

    if (array)
    {
        r.y = b->y1 + 1;
        r.h = BUTTON_HEI(b);
        addArrayBorder (&r, array);
    }
}

/* fill button up to x-offset maxy.
 * arrayMode:
 * 0 : no array mode
 * 1 : array mode, not enabled
 * 2 : array mode, enabled
 */
void fillButtonH (button *b, int minx, int maxx, int color, int array)
{
    SDL_Rect r;

    if (!b->visible)
        return;

    r.x = b->x1 + 1 + minx;
    r.y = b->y1 + 1;
    r.w = maxx - minx;
    r.h = BUTTON_HEI(b);

    SDL_FillRect (screen, &r, color);

    if (array)
    {
        r.y = b->y1 + 1;
        r.h = BUTTON_HEI(b);
        addArrayBorder (&r, array);
    }
}

void fillButtonHV (button *b, int minc, int maxc, int color, int array, int ho)
{
    if (ho)
        fillButtonH (b, minc, maxc, color, array);
    else
        fillButton (b, minc, maxc, color, array);

}

void buttonDependsOn (button *b, setting *s, int disable)
{
    if ((b == NULL) || (s == NULL))
        return;

    if (buttonDependencies == NULL)
    {
        maxButtonDependencies = 100;
        buttonDependencies = (bsDep_s*)malloc(sizeof(bsDep_s) * maxButtonDependencies);
    }

    buttonDependencies[numButtonDependencies].s = s;
    buttonDependencies[numButtonDependencies].b = b;
    buttonDependencies[numButtonDependencies].disable = disable;

    numButtonDependencies++;
    doButtonDependencies(s);
}

void doButtonDependencies (setting *s)
{
    int i;

    for (i = 0; i < numButtonDependencies; i++)
    {
        if (buttonDependencies[i].s != s)
            continue;

        if (buttonDependencies[i].disable)
        {
            enableButton (buttonDependencies[i].b, getValue(s) != 0);
        }
    }
}

void
onOffButtonS (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);
    int nv;
    setting *s;

    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, event);
        return;
    }

    s = b->pSetting;

    if (event == REDRAW)
    {
        int arrayMode = 0;

        if (b->settingInstances > 1)
        {
            arrayMode = *b->applyToArray ? 2 : 1;
        }

        markButton (b, GETVAL32(*s) != 0, arrayMode);
        return;
    }

    if ((b->settingInstances > 1) && (x > BUTTON_WID(b)-6) && (x <= BUTTON_WID(b)))
    {
        if (EVENT_CODE(action) == BUTTON_PRESS)
        {
            *b->applyToArray ^= 1;

            if (*b->applyToArray)
                modifySetting (s, GETVAL32(*s), 1);

            redrawButton (b);

            clearCurrentButtonSelection ();
        }
        return;
    }
    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            buttonBorderColor (b, COL_ORANGE);
        }
        else
        {
            buttonBorderColor (b, s->marked ? COL_RED: COL_ICONDRAW);
        }

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, s->marked ? COL_RED: COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            nv = 1 - getValue(s);

            modifySetting (s, nv, b->applyToArray ? *b->applyToArray : 0);

            redrawButton (b);
        }
    }

    if ((mouseButton == 2) && (event == BUTTON_PRESS))
    {
        markSetting (s, -1);
        return;
    }
}

void selMarker (button *b, int color, int array)
{
    int x,y;
    SDL_Rect r;

    if (!b->visible)
        return;

#if 0    
    for (y = 0; y < BUTTON_HEI(b)/2+1; y++)
    {
        for (x = 0; x < y; x++)
        {
            buttonDrawPixelB (b, x, y, color);
            buttonDrawPixelB (b, x, BUTTON_HEI(b)-y, color);
            buttonDrawPixelB (b, BUTTON_WID(b)-x, y, color);
            buttonDrawPixelB (b, BUTTON_WID(b)-x, BUTTON_HEI(b)-y, color);
        }
    }
#else
    for (y = 0; y < BUTTON_HEI(b); y++)
    {
        buttonDrawPixelB (b, 0, y, color);
        buttonDrawPixelB (b, 1, y, 0);
        buttonDrawPixelB (b, 2, y, color);
        buttonDrawPixelB (b, BUTTON_WID(b)-3, y, color);
        buttonDrawPixelB (b, BUTTON_WID(b)-2, y, 0);
        buttonDrawPixelB (b, BUTTON_WID(b)-1, y, color);
    }
    for (x = 2; x < BUTTON_WID(b)-2; x++)
    {
        buttonDrawPixelB (b, x, 1, 0);
        buttonDrawPixelB (b, x, 2, color);
        buttonDrawPixelB (b, x, BUTTON_HEI(b)-3, color);
        buttonDrawPixelB (b, x, BUTTON_HEI(b)-2, 0);
    }
    for (x = 0; x < BUTTON_WID(b); x++)
    {
        buttonDrawPixelB (b, x, 0, color);
        buttonDrawPixelB (b, x, BUTTON_HEI(b)-1, color);
    }

#endif
    
    if (array)
    {
        r.x = b->x1 + 1;
        r.y = b->y1 + 1;
        r.h = BUTTON_HEI(b);
        r.w = BUTTON_WID(b);
        addArrayBorder (&r, array);
    }    
}

void markButton (button *b, int mark, int array)
{   
    if (!mark)
    {
        fillButton (b, 0, BUTTON_HEI(b), b->bgColor, array);
    }
    else
    {
        if (b->useSelMarker )
            selMarker (b, b->selectCol, array);
        else
            fillButton (b, 0, BUTTON_HEI(b), b->selectCol, array);
        
    }
}

void onOffGroupS (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);
    int nv;
    setting *s;
    button *gb;

    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, event);
        return;
    }

    s = b->pSetting;

    if (event == REDRAW)
    {
        int arrayMode = 0;

        if (b->settingInstances > 1)
        {
            arrayMode = *b->applyToArray ? 2 : 1;
        }

        markButton (b, b->userData == GETVAL32(*s), arrayMode);

        return;
    }

    if ((b->settingInstances > 1) && (x > BUTTON_WID(b)-6) && (x <= BUTTON_WID(b)))
    {
        if (EVENT_CODE(action) == BUTTON_PRESS)
        {
            *b->applyToArray ^= 1;

            if (*b->applyToArray)
                modifySetting (s, GETVAL32(*s), 1);

            for (gb = b->groupFirst; gb != NULL; gb = gb->groupNext)
            {
                redrawButton (gb);
            }
            clearCurrentButtonSelection ();
        }
        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            buttonBorderColor (b, COL_ORANGE);
        }
        else
        {
            buttonBorderColor (b, COL_ICONDRAW);
        }

        groupBorderColor (b, s->marked ? COL_RED: COL_ICONDRAW);

        return;
    }

    if ((mouseButton == 2) && (event == BUTTON_PRESS))
    {
        markSetting (s, -1);
        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        int pickerWasActive = 0;

        buttonBorderColor (b, COL_ICONDRAW);
        groupBorderColor (b, s->marked ? COL_RED: COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            nv = b->userData;

            if (shiftPressed() && s->canBeRandom)
            {
                if (!s->current.isRandom)
                {
                    randomSetting (s, 1, *b->applyToArray);
                }
                else
                {
                    if (valueIsMasked (s, nv))
                    {
                        maskRandomValue (s, nv, 0, 0);
                    }
                    else
                    {
                        maskRandomValue (s, nv, 1, 0);
                    }

                    if (s->current.isRandom)
                        buttonSetRandom (b, !valueIsMasked (s, nv));
                }
            }
            else
            {
                modifySetting (s, nv, *b->applyToArray);
            }

            if (b->groupFirst == NULL)
            {
                redrawButton (b);
            }
            else
            {
                for (gb = b->groupFirst; gb != NULL; gb = gb->groupNext)
                {
                    redrawButton (gb);

                    if (pickerIsActive (gb))
                    {
                        pickerWasActive = 1;
                        exitPicker();
                    }
                }

                if (pickerWasActive)
                {
                    pickColor (b->bgColorSetting);
                }
            }
        }
    }
}

void
onOffButtonDouble (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);
    static uint32 lastRelTime = 0;

    int *pValue = (int*)b->userData;

    if (event == REDRAW)
    {
//        selectButton (b, *pValue);
        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            if ((planschTime() - lastRelTime) < 500)
            {
                *pValue = 1 - *pValue;

                selectButton (b, *pValue);
            }
            else
            {
                lastRelTime = planschTime();
            }
        }
    }
}

void
onOffButton (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);

    int *pValue = (int*)b->userData;

    if (event == REDRAW)
    {
        markButton (b, b->selected, 0);
//        if (b->selected)
//            fillButton (b, 0, BUTTON_HEI(b), b->selectCol, 0);
        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            *pValue = 1 - *pValue;

            selectButton (b, *pValue);

        }
    }
}

void pushButton (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);

    FUNCPTR cb = (FUNCPTR)b->userData;

    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, event);
        return;
    }

    if (event == REDRAW)
    {
        markButton (b, b->selected, 0);
//        if (b->selected)
//            fillButton (b, 0, BUTTON_HEI(b), b->selectCol, 0);
        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            if (cb)
                cb (b);
        }
    }
}

void
invertButton (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);

    int *pValue = (int*)b->userData;

    if (event == REDRAW)
    {
        markButton (b, *pValue < 0, 0);
//        if (*pValue < 0)
//            fillButton (b, 0, BUTTON_HEI(b), b->selectCol, 0);
        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            *pValue = - *pValue;

            selectButton (b, (*pValue < 0));
        }
    }
}

void
actionButtonHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);

    FUNCPTR cb = (FUNCPTR)b->userData;

    if (action == REDRAW)
    {
        markButton (b, b->selected, 0);
//        if (b->selected)
//            fillButton (b, 0, BUTTON_HEI(b), b->selectCol, 0);
        return;
    }

    if (IS_SUB_BUTTON(b))
    {
        if (event != SUBPANEL_SELECT)
            return;

        event = BUTTON_RELEASE;
        mouseButton = 1;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            if (cb)
                cb(b);
        }
    }
}

void
arraySliderS (button * b, int action, int ox, int oy)
{
    setting *pSetting;
    FLOAT range;
    int value;
    int tmp;

    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, EVENT_CODE(action));
        return;
    }

    pSetting = b->pSetting;
    value = GETVAL32(*pSetting);

    range = pSetting->maxValue - pSetting->minValue;

    if (IS_RESET(action))
    {
        value = pSetting->defaultValue;
        oy = ((FLOAT)(value - pSetting->minValue) * BUTTON_HEI(b)) / range;
    }
    else if (action == REDRAW)
    {
        if (b->visible)
        {
            if (pSetting->current.rndRange)
            {
                oy = ((FLOAT)(pSetting->current.rndMin - pSetting->minValue) * BUTTON_HEI(b)) / range;
                tmp = ((FLOAT)(pSetting->current.rndMax - pSetting->minValue) * BUTTON_HEI(b)) / range;

                fillButton (b, oy, tmp, b->selectCol, *b->applyToArray ? 2 : 1);
            }
            else
            {
                oy = ((FLOAT)(value - pSetting->minValue) * BUTTON_HEI(b)) / range;

                fillButton (b, 0, oy, b->selectCol, *b->applyToArray ? 2 : 1);
            }
        }
        return;
    }
    else if (EVENT_CODE(action) == SCROLLDOWN)
    {
        value += pSetting->minDiff;

        if (value > pSetting->maxValue)
            value = pSetting->maxValue;

        oy = ((FLOAT)(value - pSetting->minValue) * BUTTON_HEI(b)) / range;

    }
    else if (EVENT_CODE(action) == SCROLLUP)
    {
        value -= pSetting->minDiff;

        if (value < pSetting->minValue)
            value = pSetting->minValue;

        oy = ((FLOAT)(value - pSetting->minValue) * BUTTON_HEI(b)) / range;
    }
    else
    {
        if ((BUTTON_ID(action) == 2) && (EVENT_CODE(action) == BUTTON_PRESS))
        {
            markSetting (pSetting, -1);

            goto out;
        }

        if (BUTTON_ID(action) != 1)
            return;

        if ((b->settingInstances > 1) && (ox > BUTTON_WID(b)-6) && (ox <= BUTTON_WID(b)))
        {
            if (EVENT_CODE(action) == BUTTON_PRESS)
            {
                *b->applyToArray ^= 1;

                redrawButton (b);
                clearCurrentButtonSelection ();

                if (*b->applyToArray)
                {
                    cloneSettingToArray (pSetting);
                }
            }
            return;
        }

        if (oy < 0)
            oy = 0;
        if (oy > BUTTON_HEI(b))
            oy = BUTTON_HEI(b);

        value = pSetting->minValue + ((oy * range) / BUTTON_HEI(b));

        if (pSetting->canBeRandom && shiftPressed())
        {
            if (EVENT_CODE(action) == BUTTON_PRESS)
            {
                b->randomRangeStart = value;
            }

            buttonSetRandom (b, 1);
            rndRange (pSetting, 1, MIN(b->randomRangeStart, value), MAX(b->randomRangeStart, value), *b->applyToArray);

            redrawButton (b);
            return;
        }
    }

out:
    modifySetting (pSetting, value, *b->applyToArray);
}

void
sliderS (button * b, int action, int ox, int oy)
{
    setting *pSetting;
    FLOAT range;
    int value, tmp;
    int horizontal = BUTTON_HEI(b) < BUTTON_WID(b);
    int mul;
    int off;
    
    if (horizontal)
        mul = BUTTON_WID(b);
    else
        mul = BUTTON_HEI(b);

    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, EVENT_CODE(action));
        return;
    }

    pSetting = b->pSetting;
    value = GETVAL32(*pSetting);

    range = pSetting->maxValue - pSetting->minValue;

    if (IS_RESET(action))
    {
        value = pSetting->defaultValue;
        oy = ((FLOAT)(value - pSetting->minValue) * mul) / range;
    }
    else if (action == REDRAW)
    {
        oy = ((FLOAT)(value - pSetting->minValue) * mul) / range;
        
        if (b->visible)
        {
            if (pSetting->current.rndRange)
            {
                oy = ((FLOAT)(pSetting->current.rndMin - pSetting->minValue) * mul) / range;
                tmp = ((FLOAT)(pSetting->current.rndMax - pSetting->minValue) * mul) / range;

                fillButtonHV (b, oy, tmp, b->selectCol, 0, horizontal);
            }
            else
            {
                fillButtonHV (b, 0, oy, b->selectCol, 0, horizontal);
            }
        }
        return;
    }
    else if (EVENT_CODE(action) == SCROLLDOWN)
    {
        value += pSetting->minDiff;

        if (value > pSetting->maxValue)
            value = pSetting->maxValue;

        oy = ((FLOAT)(value - pSetting->minValue) * mul) / range;

    }
    else if (EVENT_CODE(action) == SCROLLUP)
    {
        value -= pSetting->minDiff;

        if (value < pSetting->minValue)
            value = pSetting->minValue;

        oy = ((FLOAT)(value - pSetting->minValue) * mul) / range;
    }
    else
    {
        if ((BUTTON_ID(action) == 2) && (EVENT_CODE(action) == BUTTON_PRESS))
        {
            markSetting (pSetting, -1);

            goto out;
        }

        if (BUTTON_ID(action) != 1)
            return;

        if (horizontal)
            off = ox;
        else
            off = oy;
        
        if (off < 0)
            off = 0;
        if (off > mul)
            off = mul;

        value = pSetting->minValue + ((off * range) / mul);

        if (pSetting->canBeRandom && shiftPressed())
        {
            if (EVENT_CODE(action) == BUTTON_PRESS)
            {
                b->randomRangeStart = value;
            }

            buttonSetRandom (b, 1);
            rndRange (pSetting, 1, MIN(b->randomRangeStart, value), MAX(b->randomRangeStart, value), 0);

            redrawButton (b);
            return;
        }
    }

out:
    modifySetting (pSetting, value, 0);    
}

void
slider (button * b, int action, int ox, int oy)
{
    char tmpStr[128];

    if (b->settingInstances > 1)
        arraySliderS (b, action, ox, oy);
    else
        sliderS (b, action, ox, oy);
    
    if (b->pSetting)
    {
        if (b->pSetting->current.rndRange)
        {
            sprintf (tmpStr, "%s%s=%"PRId64"..%"PRId64"%s",
                b->pSetting->name,
                (*b->applyToArray) ? "[]" : "", 
                (int64_t)b->pSetting->current.rndMin,
                (int64_t)b->pSetting->current.rndMax,
                b->pSetting->marked ? " *":"");
            statText (4, tmpStr);
        }
        else if (b->pSetting->current.isRandom)
        {
            sprintf (tmpStr, "%s%s=%"PRId64"..%"PRId64"%s",
                b->pSetting->name,
                (*b->applyToArray) ? "[]" : "", 
                (int64_t)b->pSetting->minValue,
                (int64_t)b->pSetting->maxValue,
                b->pSetting->marked ? " *":"");
            statText (4, tmpStr);
        }
        else
        {
            sprintf (tmpStr, "%s%s=%"PRId64"%s",
                b->pSetting->name,
                (*b->applyToArray) ? "[]" : "", 
                (int64_t)getValue(b->pSetting),
                b->pSetting->marked ? " *":"");
            statText (4, tmpStr);
        }
    }
}

void
asySlider (button * b, int action, int ox, int oy)
{
    int x, y;
    float bh = BUTTON_HEI(b);
    int value;
    setting *pSetting = b->pSetting;
    float range;
    int min;
    char tmpStr[100];


    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, EVENT_CODE(action));
        return;
    }

    value = GETVAL32(*pSetting);
    min = pSetting->minValue;
    range = pSetting->maxValue - pSetting->minValue + 1;

    if (IS_RESET(action))
    {
        value = pSetting->defaultValue;
        oy = 1.0f / (1.0f - (float)(value-min) / range) - 1.0f;
    }
    else if (action == REDRAW)
    {
        oy = 1.0f / (1.0f - (float)(value-min) / range) - 1.0f;

        for (x = 0; x < BUTTON_WID(b); x++)
        {
            for (y = 0; y < BUTTON_HEI(b); y++)
            {
                if (y >= oy)
                {
                    buttonDrawPixel (b, x, y, COL_EMPTY);
                }
                else 
                {
                    if (x > (BUTTON_WID(b) - ((FLOAT)BUTTON_WID(b) / (FLOAT)y)))
                    {
                        buttonDrawPixel (b, x, y, COL_EMPTY);
                    }
                    else
                    {
                        buttonDrawPixel (b, x, y, b->selectCol);
                    }
                }
            }
        }
        return;
    }
    else if (EVENT_CODE(action) == SCROLLDOWN)
    {
        value = MIN(value + pSetting->minDiff, pSetting->maxValue);

        oy = 1.0f / (1.0f - (float)(value-min) / range) - 1.0f;
    }
    else if (EVENT_CODE(action) == SCROLLUP)
    {
        value = MAX(value - pSetting->minDiff, pSetting->minValue);

        oy = 1.0f / (1.0f - (float)(value-min) / range) - 1.0f;
    }
    else
    {
        if (BUTTON_ID(action) != 1)
            return;

        if (oy < 0)
            oy = 0;
        if (oy > BUTTON_HEI(b)-1)
            oy = BUTTON_HEI(b)-1;

        value = ((bh - (bh / ((FLOAT)(oy+1))))) * (range / bh) + min;
    }
        
    modifySetting (pSetting, value, 0);

    sprintf (tmpStr, "%s=%d%s", pSetting->name, value, pSetting->marked ? " *":"");
    statText (4, tmpStr);
}

void
expSlider (button * b, int action, int ox, int oy)
{
    int x, y;
    float bh = BUTTON_HEI(b);
    int value;
    setting *pSetting = b->pSetting;
    float range;
    int min;
    char tmpStr[100];


    if (IS_SUB_BUTTON(b))
    {
            handleSettingEvent (b, EVENT_CODE(action));
        return;
    }

    value = GETVAL32(*pSetting);
    min = pSetting->minValue;
    range = pSetting->maxValue - pSetting->minValue;

    if (IS_RESET(action))
    {
        value = pSetting->defaultValue;
        oy = bh - (bh / ((((FLOAT)(value-min) * bh) / range) + 1.0f));
    }
    else if (action == REDRAW)
    {
        oy = bh - (bh / ((((FLOAT)(value-min) * bh) / range) + 1.0f));

        for (x = 0; x < BUTTON_WID(b); x++)
        {
            for (y = 0; y < BUTTON_HEI(b); y++)
            {
                if (y >= oy)
                {
                    buttonDrawPixel (b, x, y, COL_EMPTY);
                }
                else 
                {
                    if (x < ((FLOAT)BUTTON_WID(b) / (FLOAT)(BUTTON_HEI(b) - (y))))
                    {
        //                    buttonDrawPixel (b, x, y, COL_EMPTY);
                    }
                    else
                    {
                        buttonDrawPixel (b, x, y, b->selectCol);
                    }
                }
            }
        }

        return;
    }
    else if (EVENT_CODE(action) == SCROLLDOWN)
    {
        value = MIN(value + pSetting->minDiff, pSetting->maxValue);

        oy = bh - (bh / ((((FLOAT)(value-min) * bh) / range) + 1.0f));
    }
    else if (EVENT_CODE(action) == SCROLLUP)
    {
        value = MAX(value - pSetting->minDiff, pSetting->minValue);

        oy = bh - (bh / ((((FLOAT)(value-min) * bh) / range) + 1.0f));
    }
    else
    {
        if (BUTTON_ID(action) != 1)
            return;

        if (oy < 0)
            oy = 0;
        if (oy > BUTTON_HEI(b)-1)
            oy = BUTTON_HEI(b)-1;

        value = ((bh / (bh - (FLOAT)oy)) - 1.0f) * (range / bh) + min;
    }
        
    modifySetting (pSetting, value, 0);

    sprintf (tmpStr, "%s=%d%s", pSetting->name, value, pSetting->marked ? " *":"");
    statText (4, tmpStr);
}

void
nullButton (button * b, int action, int ox, int oy)
{

}
