/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

/* Color Picker ....
 *
 * Color value is stored in a setting's value_u, using the colorVal_t union
 * element. It encodes the color vector, luminosity and the ARGB value.
 */ 

#include "plansch.h"

#if 0
#define debug printf
#else
#define debug(...)
#endif

#define COL_HISTORY_SIZE    (5*5)

static int pickerActive = 0;
static int statusOverlap  =0;
static panel *colorPickPanel = NULL;
static button *pExitButton = NULL;
static button *wheelButton = NULL;
static button *textButton = NULL;
static button *fixedCol[6] = { NULL, NULL, NULL, NULL, NULL, NULL };
static button *recent[COL_HISTORY_SIZE];
static button *lumSlider = NULL;
#ifdef USE_OPENGL
static button *alphaSlider = NULL;
static button *modifyAlphaButton = NULL;
#endif

static int history_size;

static setting *colorSetting = NULL;

static uint32_t argbMask = 0xffffffff;

/* saved possition of crosshair
 */
static int haveOldPosition = 0;
static int oldx, oldy;
static float currentLum;

static struct
{
    float dx, dy, lum, alpha;
} defaultSetting;

static float bwRatioX, bwRatioY;

void redrawWheel (button *b, int force);
void setLumCb (setting *s);
void setAlphaCb (setting *s);
void addToHistory (uint32_t col);
void cvRGBUpdate (colorVal_t *cv);
void updateWheel (void);

/* luminosity slider
 */
SETTING(colLum) =
{
    .minValue = 0,
    .maxValue = 255,
    .minDiff = 1,
    .defaultValue = 127,
    .name = "Luminosity",
    .cb = (FUNCPTR)setLumCb
};

/* alpha channel slider (releavent for opengl only)
 */
SETTING(colAlpha) =
{
    .minValue = 0,
    .maxValue = 255,
    .minDiff = 1,
    .defaultValue = 255,
    .name = "Alpha Channel"
#ifdef USE_OPENGL
    ,
    .cb = (FUNCPTR)setAlphaCb
#endif
};


#ifdef USE_OPENGL
static void activateColorSelection (int activate)
{
    int i;

    enableButton (wheelButton, activate);
    enableButton (lumSlider, activate);
    for (i = 0; i < 6; i++)
        enableButton (fixedCol[i], activate);
}

static void setChangeAlpha (button *b, int val)
{
    if (!b)
        return;

    selectButton (b, val);
    activateColorSelection (!val);
    
    if (val)
        argbMask = 0xff000000;
    else
        argbMask = 0xffffffff;
}

static void changeAlphaCb (button *b)
{
    setChangeAlpha (b, 1 - b->selected);
}

/* Callback for alpha slider 
 */
void setAlphaCb (setting *s)
{
    value_u val;

    if (!pickerActive)
        return;

    val.va64 = getValue (colorSetting);
    setAlpha (&val.cv, getValueNorm (s));

    modifySetting (colorSetting, val.va64, 0);

    updateWheel();
}
#endif

/* Callback for luminosity slider 
 * Encodes lum into setting's color value and redisplays the color wheel.
 */
void setLumCb (setting *s)
{
    value_u val;

    if (!pickerActive)
        return;

    val.va64 = getValue (colorSetting);
    val.cv.lum = getValue(s) & 0xff;

    cvRGBUpdate (&val.cv);

    modifySetting (colorSetting, val.va64, 0);

    updateWheel();
}

/* update the color wheel widgets
 */
void updateWheel (void)
{
    if (!pickerActive)
        return;

    value_u val;

    val.va64 = getValue (colorSetting);

    modifySetting (&colLum, val.cv.lum, 0);
#ifdef USE_OPENGL
    modifySetting (&colAlpha, (val.cv.argb >> 24) & 0xff, 0);
#endif

    redrawWheel(wheelButton, 1);

    if (colorSetting->pButton)
        buttonBgColor (colorSetting->pButton, ARGB(val.cv.argb));
}

/* Encode a normalized x/y vector in the color wheel and the luminosity
 * into a integer setting value
 */
void setColorVec (setting *s, float dx, float dy, float lum, float alpha)
{
    value_u val;
    
    val.va64 = getValue (s);

    encodeColorVec (&val.cv, dx, dy, lum, alpha);

    modifySetting (s, val.va64, 0);

    updateWheel();
}

/* Color the button's x/y position with the currect hsv value 
 * or black if outside the wheen
 */
void setWheelPixel (button *button, int x, int y, float lum)
{
    float        h, s, r, g, b, a;
    uint32_t    argb;
    int                c = BUTTON_WID(button) / 2;
    float        dx = (float)(x - c) / (float)c;
    float        dy = (float)(y - c) / (float)c;

    getColorWheelValue(dx, dy, &h, &s);
    if (s <= 1.0)
    {
        // Antialias the edge of the circle.
        if (s > 0.99) a = (1.0 - s) * 100;
        else a = 1.0;

        HSL2RGB(h, s, lum, &r, &g, &b);
    }
    else
    {
        r = g = b = a = 0.0f;
    }

    argb = ((uint32_t)(r * (float)0xff) << 16) | 
           ((uint32_t)(g * (float)0xff) <<  8) |
           ((uint32_t)(b * (float)0xff) <<  0) |
           ((uint32_t)(a * (float)0xff) << 24);
    
    buttonDrawPixelB (button, x, y, ARGB(argb));
}

/* put the selection cross over the color wheel.
 * If the color button has the canBeRandom value set, 
 * draw a box instead.
 */
void setCross (button *b, int ox, int oy)
{
    int i;
    uint32_t c;
    int rndBox = 0;

    if (colorSetting)
    {
        rndBox = colorSetting->canBeRandom;
    }

    if (rndBox)
    {
        int sBox = rndBox / bwRatioX;
        if (sBox == 0)
            sBox = 1;

        for (i = 0; i <= sBox; i++)
        {
            c = ((i + ox + oy) & 1) ? 0xffffffff : 0;
            buttonDrawPixelB (b, ox, oy, c);
            buttonDrawPixelB (b, ox - i, oy - sBox, c);
            buttonDrawPixelB (b, ox + i, oy - sBox, c);
            buttonDrawPixelB (b, ox - i, oy + sBox, c);
            buttonDrawPixelB (b, ox + i, oy + sBox, c);
            buttonDrawPixelB (b, ox - sBox, oy - i, c);
            buttonDrawPixelB (b, ox - sBox, oy + i, c);
            buttonDrawPixelB (b, ox + sBox, oy - i, c);
            buttonDrawPixelB (b, ox + sBox, oy + i, c);
        }
    }
    else
    {
        buttonDrawPixelB (b, ox - 1, oy, 0xffffffff);
        buttonDrawPixelB (b, ox - 2, oy, 0xffffffff);
        buttonDrawPixelB (b, ox + 1, oy, 0xffffffff);
        buttonDrawPixelB (b, ox + 2, oy, 0xffffffff);
        buttonDrawPixelB (b, ox, oy - 1, 0xffffffff);
        buttonDrawPixelB (b, ox, oy - 2, 0xffffffff);
        buttonDrawPixelB (b, ox, oy + 1, 0xffffffff);
        buttonDrawPixelB (b, ox, oy + 2, 0xffffffff);
        buttonDrawPixelB (b, ox, oy,     0xff000000);
        buttonDrawPixelB (b, ox-1, oy-1, 0xff000000);
        buttonDrawPixelB (b, ox-2, oy-2, 0xff000000);
        buttonDrawPixelB (b, ox-1, oy+1, 0xff000000);
        buttonDrawPixelB (b, ox-2, oy+2, 0xff000000);
        buttonDrawPixelB (b, ox+1, oy-1, 0xff000000);
        buttonDrawPixelB (b, ox+2, oy-2, 0xff000000);
        buttonDrawPixelB (b, ox+1, oy+1, 0xff000000);
        buttonDrawPixelB (b, ox+2, oy+2, 0xff000000);
    }
}



/* redraw
 *        color wheel with a marker for the current selection
 *        The current argb text value
 *        The exit button with the current color
 */
void redrawWheel (button *b, int forceRedraw)
{
    int                x, y;
    int                x1, x2, y1, y2;
    int                bw = BUTTON_WID(b);
    int                bh = BUTTON_HEI(b);
    float        dx, dy, lum, alpha;
    int                ox, oy;
    int                argb;
    value_u        *val;

    val = getValue_u (colorSetting);

    argb = val->cv.argb;
    decodeColorVec (&val->cv, &dx, &dy, &lum, &alpha);

    ox = dx * (float)(bw/2) + (float)(bw/2);
    oy = dy * (float)(bh/2) + (float)(bh/2);

    if (forceRedraw || haveOldPosition == 0 || lum != currentLum)
    {
        x1 = 0; x2 = bw;
        y1 = 0; y2 = bh;
    }
    else
    {
        x1 = oldx - 2;
        x2 = oldx + 3;
        y1 = oldy - 2;
        y2 = oldy + 3;
    }
    
    SCR_CHANGE_START

    for (y = y1; y < y2; y++)
    {
        for (x = x1; x < x2; x++)
        {
            setWheelPixel (b, x, y, lum);
        }
    }
    oldx = ox;
    oldy = oy;
    haveOldPosition = 1;
    currentLum = lum;

    setCross (b, ox, oy);

    SCR_CHANGE_FINISH

    buttonBgColor (pExitButton, ARGB(argb));

    sprintf (textButton->text, "r%d g%d b%d a%d",
                (argb >> 16)  & 0xff,
                (argb >>  8)  & 0xff,
                (argb >>  0)  & 0xff,
                (argb >>  24) & 0xff
                );
    redrawButton (textButton);
}


/* color wheel button handler
 */
void colorPickHandler (button * b, int action, int ox, int oy)
{
    int                bw = BUTTON_WID(b);
    int                bh = BUTTON_HEI(b);
    value_u        *val = getValue_u (colorSetting);
    float        l = (float)val->cv.lum / 255.0;
    float        a = getAlpha (&val->cv);
    float        fx, fy;
    setting        *s = colorSetting;

    if (action == REDRAW)
    {
        /* redraw the color wheel 
         */
        redrawWheel (b, 1);

        return;
    }

    if (IS_SUB_BUTTON(b))
    {
        float _a, _r, _g, _b;

        _r = (float)(CH_GET_R(b->bgColor)) / 255.0f;
        _g = (float)(CH_GET_G(b->bgColor)) / 255.0f;
        _b = (float)(CH_GET_B(b->bgColor)) / 255.0f;
        _a = (float)(CH_GET_A(b->bgColor)) / 255.0f;

        pickerSetARGB (_a, _r, _g, _b);

        return;
    }

    if (IS_RESET(action))
    {
        /* restore default 
         */
        setColorVec (colorSetting, defaultSetting.dx, defaultSetting.dy,
                     defaultSetting.lum, defaultSetting.alpha);
        redrawWheel (wheelButton, 1);
        return;
    }
    else if ((BUTTON_ID(action) != 1) && (BUTTON_ID(action) != 2))
    {
        return;
    }

    if (s && EVENT_CODE(action) == SCROLLDOWN)
    {
        s->canBeRandom = MAX(s->canBeRandom-bwRatioX, 0);
        redrawWheel(wheelButton, 1);
        return;
    }
    if (s && EVENT_CODE(action) == SCROLLUP)
    {
        s->canBeRandom = MIN(s->canBeRandom+bwRatioX, XY_INT_MAX);
        redrawWheel(wheelButton, 1);
        return;
    }

    /* handle movement when button is pressed
     */
    if (ox < 1)
    {
        ox = 1;
    }
    if (ox >= bw)
    {
        ox = bw;
    }
    if (oy < 1)
    {
        oy = 1;
    }
    if (oy >= bh)
    {
        oy = bh;
    }

    fx = (float)(ox - bw / 2) / (float)(bw/2);
    fy = (float)(oy - bh / 2) / (float)(bw/2);

    debug ("%d/%d -> %f %f\n", ox, oy, fx, fy);

    setColorVec (colorSetting, fx, fy, l, a);
}

/* exit color picker 
 */
void exitPicker (void)
{
    if (!pickerActive)
        return;

    addToHistory (pExitButton->bgColor);

    showPanel (colorPickPanel, 0);

    if (statusOverlap)
    {
        showPanel (recorder, 1);
        showPanel (statusPanel, 1);
        showPanel (cutCopySavePanel, 1);
    }
    
    pickerActive = 0;
    argbMask = 0xffffffff;
}


/* callback for fixed-color buttons
 */
static void colorPickCb (button * b)
{
    float hue;
    float dx, dy;
    int i;

    if (!pickerActive)
        return;

    for (i = 5; i >= 0; i--)
    {
        if (b == fixedCol[i])
        {
            hue = cvToHue (i) * 2 * M_PI;

            dx = cos (hue);
            dy = -sin (hue);
            setColorVec (colorSetting, dx, dy, 0.5f, 1.0f);

            break;
        }
    }
}

/* Handler to the "exit" button. Copy of pushButton() with different sub-button handling
 *
 * Save the current color selection in the color history.
 * Exit the color picker
 */
void exitButtonHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);

    if (event == REDRAW)
    {
        markButton (b, b->selected, 0);
        return;
    }

    if (IS_SUB_BUTTON(b))
    {
        float _a, _r, _g, _b;

        _r = (float)(CH_GET_R(b->bgColor)) / 255.0f;
        _g = (float)(CH_GET_G(b->bgColor)) / 255.0f;
        _b = (float)(CH_GET_B(b->bgColor)) / 255.0f;
        _a = (float)(CH_GET_A(b->bgColor)) / 255.0f;

        pickerSetARGB (_a, _r, _g, _b);

        return;
    }


    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {

            exitPicker();
        }
    }
}

/* Initialize color picker panel
 */
void initColorPicker (int gid)
{
    int cpSize = ((options->scrHei - hei) * 3) / 4;
    int cpX = 0;
    int cpY = 0;
    int th = cpSize/6 - 1;
    int i;
    button *b;

    if (wid > 1200)
    {
        statusOverlap = 0;
        cpY = hei + 2;
        cpX = statusPanel->box.x - (cpSize) - 68;
    }
    else
    {
        statusOverlap = 1;
        cpX = (wid) - (cpSize) - 68;
        cpY = hei + 2;
    }

    colorPickPanel = makePanel ("colorPicker", gid, cpX, cpY, P_DOWN_RIGHT, 0);

    if (!colorPickPanel)
        return;

#if 0
    if (!statusOverlap)
        connectPanel (colorPickPanel, statusPanel, LEFT_SIDE);
#endif

    history_size = 0;

    wheelButton = makeButton (colorPickPanel, colorPickHandler, cpSize, cpSize, NULL, "Color Picker", 0, NULL, B_NEXT);

    textButton  = makeButton (colorPickPanel, nullButton, cpSize, 10, NULL,
        "Color Value", 0, NULL, B_NEXT | B_OVERLAP);
    textButton->text = (char*)calloc(1, 100);
    pExitButton = makeButton (colorPickPanel, exitButtonHandler, cpSize, 21,
        check_xpm, "Exit Color Picker", 0, NULL, B_NEXT | B_OVERLAP);

    makeSubPanel (pExitButton, P_RIGHT_DOWN, 0, SP_BUTTON3);

    b = pExitButton;

    for (i = 0; i < COL_HISTORY_SIZE; i++)
    {
        recent[i] = addSubButton (b, 32, 32, NULL, NULL, (intptr_t)i, ((i % 5) == 0) ? B_NEXTRC : B_NEXT);
    }

    pExitButton->subPanel->lastSel = recent[0];

    lumSlider = makeButton (colorPickPanel, slider, 20, cpSize, lum_pm, "Luminosity", 0, &colLum, B_NEXTRC);
#ifdef USE_OPENGL
    alphaSlider = makeButton (colorPickPanel, slider, 20, cpSize, alpha_pm, "Alpha", 0, &colAlpha, B_NEXTRC);
    modifyAlphaButton = makeButton (colorPickPanel, pushButton, 20, 16, NULL, "ModifyAlpha", (intptr_t)&changeAlphaCb, NULL, B_NEXT);
#endif

    fixedCol[0] = makeButton (colorPickPanel, pushButton, 20, th, NULL, "Red", (intptr_t)colorPickCb, NULL, B_NEXTRC);
    buttonBgColor (fixedCol[0], ARGB(0xffff0000));
    fixedCol[1] = makeButton (colorPickPanel, pushButton, 20, th, NULL, "Green", (intptr_t)colorPickCb, NULL, B_NEXT);
    buttonBgColor (fixedCol[1], ARGB(0xff00ff00));
    fixedCol[2] = makeButton (colorPickPanel, pushButton, 20, th, NULL, "Blue", (intptr_t)colorPickCb, NULL, B_NEXT);
    buttonBgColor (fixedCol[2], ARGB(0xff0000ff));
    fixedCol[3] = makeButton (colorPickPanel, pushButton, 20, th, NULL, "Cyan", (intptr_t)colorPickCb, NULL, B_NEXT);
    buttonBgColor (fixedCol[3], ARGB(0xff00ffff));
    fixedCol[4] = makeButton (colorPickPanel, pushButton, 20, th, NULL, "Magenta", (intptr_t)colorPickCb, NULL, B_NEXT);
    buttonBgColor (fixedCol[4], ARGB(0xffff00ff));
    fixedCol[5] = makeButton (colorPickPanel, pushButton, 20, th, NULL, "Yellow", (intptr_t)colorPickCb, NULL, B_NEXT);
    buttonBgColor (fixedCol[5], ARGB(0xffffff00));

    bwRatioX = XY_INT_MAXF / (float)cpSize;
    bwRatioY = XY_INT_MAXF / (float)cpSize;

    showPanel (colorPickPanel, 0);
}

/* Store a color value to the color history
 */
void addToHistory (uint32_t col)
{
    int i;

    for (i = 0; i < COL_HISTORY_SIZE; i++)
        if (col == recent[i]->bgColor)
            return;

    for (i = COL_HISTORY_SIZE-1; i > 0; i--)
        recent[i]->bgColor = recent[i-1]->bgColor;

    recent[0]->bgColor = col;

    pExitButton->subPanel->lastSel = recent[0];
}

/* Assign an ARGB value to the color picker 
 */
void pickerSetARGB (float a, float r, float g, float b)
{
    float        h, s, l;
    value_u        val;

    if (!pickerActive)
        return;

    val.va64 = getValue (colorSetting);

    if (!RGB2HSL (r, g, b, &h, &s, &l))
        return;

    encodeHSLA (&val.cv, h, s, l, a);

    modifySetting (colorSetting, val.va64, 0);

    updateWheel ();
}

uint32_t pickerMask(void)
{
    return argbMask;
}

/* start color picker and pick a color into setting s
 * Current value of s will be used as default.
 */
void pickColor (setting *s)
{
    value_u *val;

    if (pickerActive)
    {
        exitPicker();
    }

    val = getValue_u (s);

    colorSetting = s;
    pickerActive = 1;
#ifdef USE_OPENCL
    setChangeAlpha (modifyAlphaButton, 0);
#endif

    decodeColorVec (&val->cv, &defaultSetting.dx, &defaultSetting.dy,
                    &defaultSetting.lum, &defaultSetting.alpha);

    if (statusOverlap)
    {
        showPanel (recorder, 0);
        showPanel (statusPanel, 0);
        showPanel (cutCopySavePanel, 0);
    }

    showPanel (colorPickPanel, 1);
    updateWheel ();
}

int pickerIsActive (button *b)
{
    if (!pickerActive)
        return 0;

    if (b && (colorSetting->pButton != b))
        return 0;

    return 1;
}
