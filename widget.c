/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"

extern SDL_Surface *IMG_ReadXPMFromArray(char **xpm);

button * currentButton = NULL;
panel *currentSubPanel = NULL;
int mouseButtonStates = 0;

button *buttonList = NULL;

static panel *panels = NULL;

static int helpVisible = 0;
static int currentGid = 1;

button **disableStack = NULL;
int dstackSize = 0;
int dstackPointer = 0;


FUNCPTR keyCaptureCb = NULL;
void *keyCaptureCbArg;

intptr_t clearHelp();
int checkButtonPixel (button *b, int sx, int sy);
void checkPanelSize (panel *p, int force, int nodeps);
void clearTT();

button panelBg;

void panelToTop (panel *pPanel)
{


}

button *buttonFindByName (char *name)
{
    button *b;

    for (b = buttonList; b != NULL; b = b->next)
    {
        if (!strcmp (b->name, name))
            return b;
    }

    return NULL;
}

/* push a button onto the button stack
 */
void dstackPush (button *b)
{
    if (dstackSize == 0)
    {
        disableStack = (button**)calloc(1000, sizeof(*disableStack));
        dstackSize = 1000;
    }
    else if (dstackPointer == dstackSize)
    {
        dstackSize += 1000;
        disableStack = (button**)realloc(disableStack, dstackSize * sizeof(*disableStack));
    }

    disableStack[dstackPointer] = b;
    dstackPointer++;
}

/* pull a button from the button stack
 */
button *dstackPull()
{
    if (dstackPointer == 0)
        return NULL;

    dstackPointer--;

    return disableStack[dstackPointer];
}

void buttonSetPixel (button *b, int x, int y, uint32 col)
{
    if ((x < 1) || (x >= BUTTON_WID(b)) ||
        (y < 1) || (y >= BUTTON_HEI(b)))
    {
        return;
    }

}

void buttonColor (button * b, int col, int overwrite)
{
    int        x,y;

    if (!b->visible)
        return;

    SCR_CHANGE_START

    for (x = b->x1+1; x < b->x2-1; x++)
    {
        for (y = b->y1+1; y < b->y2-1; y++)
        {
            if (!checkButtonPixel (b, x, y))
                continue;

            if (!overwrite && SCR_IS_RGB(x, y, COL_ICONFG))
                continue;

            SCR_SET_SAFE(x, y, col);
        }
    }

    SCR_CHANGE_FINISH
}

void buttonSelectColor (button *b, int col)
{
    b->selectCol = col;

    redrawButton (b);
}

void buttonSetRandom (button *b, int random)
{
    int instances;
    int i;
    setting *sArray;

    if (*b->applyToArray && b->pSetting && (b->pSetting->id->instances > 1))
    {
        sArray = b->pSetting->id->pSetting;
        instances = b->pSetting->id->instances;

        for (i = 0; i < instances; i++)
        {
            b = sArray[i].pButton;

            if (b)
            {
                b->random = random;
                redrawButton (b);
            }
        }

        return;
    }

    b->random = random;

    redrawButton (b);
}

static void setButtonRect (button *b)
{
    b->r.x = b->x1 + 1 + b->pmoffx;
    b->r.y = b->y1 + 1 + b->pmoffy;
    b->r.w = b->x2 - b->r.x - 1;
    b->r.h = b->y2 - b->r.y - 1;
}

void groupBorderColor (button * b, int col)
{
    int        x,y;

    if (b->groupFirst == NULL)
        return;

    b = b->groupFirst;

    b->groupCol = col;

    if (!b->visible || b->disabled)
        return;

    SCR_CHANGE_START

    for (x = b->groupRect.x; x < b->groupRect.x + b->groupRect.w; x++)
    {
        SCR_SET_SAFE (x, b->groupRect.y, col);
        SCR_SET_SAFE (x, b->groupRect.y + b->groupRect.h, col);
    }

    for (y = b->groupRect.y; y < b->groupRect.y + b->groupRect.h; y++)
    {
        SCR_SET_SAFE (b->groupRect.x, y, col);
        SCR_SET_SAFE (b->groupRect.x + b->groupRect.w, y, col);
    }

    SCR_CHANGE_FINISH
}

void buttonBgColor (button *b, unsigned int color)
{

    if (!b)
        return;

    b->bgColor = color;

    if (!b->visible)
        return;
    
    drawButton (b, 1);
}

void buttonBorderColor (button * b, int col)
{
    int        x,y;

    b->borderCol = col;

    if (!b->visible)
        return;

    if (b->disabled)
    {
        CH_SET_R(col, (CH_GET_R(col) + 0x60)/3);
        CH_SET_G(col, (CH_GET_G(col) + 0x60)/3);
        CH_SET_B(col, (CH_GET_B(col) + 0x60)/3);
    }

    SCR_CHANGE_START

    for (x = b->x1; x <= b->x2; x++)
    {
        SCR_SET_SAFE (x, b->y1, col);
        SCR_SET_SAFE (x, b->y2, col);
    }

    for (y = b->y1; y <= b->y2; y++)
    {
        SCR_SET_SAFE (b->x1, y, col);
        SCR_SET_SAFE (b->x2, y, col);
    }

    if (b->groupFirst)
    {
        groupBorderColor (b->groupFirst, b->groupFirst->groupCol);
    }
    SCR_CHANGE_FINISH
}

void setButtonSurface (button *b, SDL_Surface *s)
{
    if (!b || !s)
        return;

    int wx = BUTTON_WID(b);
    int wy = BUTTON_HEI(b);

//    if (b->s)
//        SDL_FreeSurface (b->s);

    b->pmx = s->w;
    b->pmy = s->h;
    b->pmoffx = b->pmoffy = 0;

    if (wx > b->pmx)
        b->pmoffx = (wx - b->pmx) / 2;

    if (wy > b->pmy)
        b->pmoffy = (wy - b->pmy) / 2;

    SDL_SetColorKey (s, SDL_SRCCOLORKEY, 0);

    b->s = s;
}

void setButtonPixmap (button *b, char **pixmap)
{
    SDL_Surface *s;

    if (pixmap && pixmap[0])
    {
        b->pixmap = pixmap;

        s = IMG_ReadXPMFromArray (pixmap);
        if (s)
        {
            if (b->s)
                SDL_FreeSurface (b->s);
            b->s = NULL;
            setButtonSurface (b, s);
        }
    }
    else
    {
        b->pixmap = NULL;
        if (b->s)
            SDL_FreeSurface (b->s);
        b->s = NULL;
    }
}

panel * makePanel (char *name, int gid, int startx, int starty, int raster, int spacing)
{
    panel *p = malloc(sizeof(*p));

    assert(p);

    memset ((char*)p, 0, sizeof(*p));

    p->gid = gid;
    p->name = name;
    p->start.d.x = p->cur.d.x = startx;
    p->start.d.y = p->cur.d.y = starty;
    p->spacing = spacing;

    p->startRC.d.x = startx;
    p->startRC.d.y = starty;
    p->nextRC = p->startRC;

    p->primaryIsX = PRIMARY_X(raster);
    p->dir.d.x = ((raster == P_RIGHT_DOWN) || (raster == P_RIGHT_UP) ||
                  (raster == P_DOWN_RIGHT) || (raster == P_UP_RIGHT)) ? 1 : -1;
    p->dir.d.y = ((raster == P_RIGHT_DOWN) || (raster == P_LEFT_DOWN) ||
                  (raster == P_DOWN_RIGHT) || (raster == P_DOWN_LEFT)) ? 1 : -1;

    p->box.x = p->start.d.x;
    p->box.y = p->start.d.y;
    p->box.w = p->box.h = 0;

    p->max.d.x = p->max.d.y = 0;

    p->next = panels;
    
    panels = p;
    
    return p;
}

void setPanelMax (panel *p, int maxx, int maxy)
{
    if (maxx == -1)
    {
        maxx = options->scrWid - p->box.x;
    }

    if (maxy == -1)
    {
        maxy = options->scrHei - p->box.y;
    }
    p->max.d.x = maxx;
    p->max.d.y = maxy;
}


button * makeButton (panel *pPanel, VOIDFUNCPTR handler, int wx, int wy, char **pixmap, char *help, intptr_t userData, setting *s, int attr)
{
    button *b = (button*)calloc (1, sizeof(button));
    vec_t edge1, edge2;
    int spacing = 0;
    
    if (pPanel->buttons)
    {
        spacing = B_SPC_GET(attr);
        if (spacing == 0)
            spacing = pPanel->spacing;
        else
            spacing--;

        spacing--;
    }

    memset ((char*)b, 0, sizeof(*b));

    /* for the border */
    wx += 2;
    wy += 2;

    switch (attr & B_CMASK)
    {
        case B_NEXTRC:
            panelRCNext (pPanel, spacing);
            break;
        case B_NEXT:
            if (pPanel->primaryIsX)
            {
                pPanel->cur.d.x += spacing * pPanel->dir.d.x; 
            }
            else
            {
                pPanel->cur.d.y += spacing * pPanel->dir.d.y; 
            }
            break;
        case B_FILL:
            if (!pPanel->primaryIsX)
            {
                pPanel->cur.d.x += spacing * pPanel->dir.d.x; 
            }
            else
            {
                pPanel->cur.d.y += spacing * pPanel->dir.d.y; 
            }
            break;
    }

    edge1.d.x = pPanel->cur.d.x;
    edge1.d.y = pPanel->cur.d.y;
    edge2.d.x = pPanel->cur.d.x + (wx - 1) * pPanel->dir.d.x;
    edge2.d.y = pPanel->cur.d.y + (wy - 1) * pPanel->dir.d.y;

    b->x1 = MIN(edge1.d.x, edge2.d.x);
    b->x2 = MAX(edge1.d.x, edge2.d.x);
    b->y1 = MIN(edge1.d.y, edge2.d.y);
    b->y2 = MAX(edge1.d.y, edge2.d.y);

    pPanel->size.d.x = MAX(pPanel->size.d.x, abs(b->x1 - pPanel->start.d.x));
    pPanel->size.d.x = MAX(pPanel->size.d.x, abs(b->x2 - pPanel->start.d.x));
    pPanel->size.d.y = MAX(pPanel->size.d.y, abs(b->y1 - pPanel->start.d.y));
    pPanel->size.d.y = MAX(pPanel->size.d.y, abs(b->y2 - pPanel->start.d.y));

    panelRCContinue (pPanel, wx, wy, attr);

    setButtonPixmap (b, pixmap);

    b->handler = handler;
    b->selected = 0;
    b->disabled = 0;
    b->borderCol = COL_ICONDRAW;
    b->groupCol = COL_ICONDRAW;
    b->selectCol = COL_ACTIVATED;

    b->name = help ? help : "anon";
    
    b->help = NULL;
    b->userData = userData;

    b->bgColor = COL_EMPTY;

    b->pPanel = pPanel;
    b->panelNext = NULL;

    b->subPanel = NULL;
    b->parentButton = NULL;

    /* if the setting is part of a settings array, we may use the option to apply
     * a value to all instances via the same button.
     * For that, opy the information of the settings array into the button 
     * structure
     */
    b->applyToArray = NULL;
    if (s)
    {
        if (s->id)
        {
            b->settingInstances = s->id->instances;
            b->settingArray = s->id->pSetting;
            b->applyToArray = &s->id->isGrouped;
        }
        else
        {
            b->settingInstances = 1;
            b->applyToArray = 0;
            b->settingArray = NULL;
        }
    }

    if (pPanel->lastButton == NULL)
        pPanel->buttons = b;
    else
        pPanel->lastButton->panelNext = b;

    pPanel->lastButton = b;

    setButtonRect (b);

    b->pSetting = s;
    if (s)
    {
        if (s->pButton)
        {
            /* Multiple buttons with the same variable. Group buttons.
             */
            button *groupLast;
            SDL_Rect *r;

            /* find the last button in the chain. While doing so, set the pointer to the
             * first button in the chain for all.
             */
            for (groupLast = s->pButton; groupLast->groupNext != NULL; groupLast = groupLast->groupNext)
            {
                groupLast->groupFirst = s->pButton;
            }

            groupLast->groupNext = b;
            b->groupNext = NULL;
            b->groupFirst = s->pButton;

            r = &b->groupFirst->groupRect;

            r->x = MIN(r->x, b->x1);
            r->y = MIN(r->y, b->y1);
            r->w = MAX(r->w, (b->x2 - r->x));
            r->h = MAX(r->h, (b->y2 - r->y));
        }
        else
        {
            s->pButton = b;

            b->groupRect.x = b->x1;
            b->groupRect.y = b->y1;
            b->groupRect.w = b->x2 - b->x1;
            b->groupRect.h = b->y2 - b->y1;
        }
        makeSettingPanel (b, s);

        if (s->help)
        {
            b->help = getHelpString(s->help);
        }
    }
    if (!b->help)
        b->help = getHelpString (help);

    checkPanelSize (pPanel, 0, 0);

    b->next = buttonList;
    buttonList = b;

    return b;
}

void panelRCNext(panel *p, int spacing)
{
    if (!p)
        return;

    p->startRC = p->nextRC;

    if (p->primaryIsX)
        p->startRC.d.y += p->dir.d.y * spacing;
    else
        p->startRC.d.x += p->dir.d.x * spacing;

    p->cur.d.x = p->startRC.d.x;
    p->cur.d.y = p->startRC.d.y;
}

void panelRCContinue (panel *p, int wx, int wy, int attr)
{
    int noexpand = ((attr & B_OVERLAP) != 0);
    int expand;
    int primaryIsX;
    if (!p)
        return;

    primaryIsX = p->primaryIsX;

    if ((attr & B_CMASK) == B_FILL)
        primaryIsX = !primaryIsX;

    if (primaryIsX)
    {
        p->cur.d.x += wx * p->dir.d.x;

        if (!noexpand)
        {
            expand = p->startRC.d.y + wy * p->dir.d.y;
            if (p->dir.d.y > 0)
            {
                p->nextRC.d.y = MAX(p->nextRC.d.y, expand);
            }
            else
            {
                p->nextRC.d.y = MIN(p->nextRC.d.y, expand);
            }
        }
    }
    else
    {
        p->cur.d.y += wy * p->dir.d.y;

        if (!noexpand)
        {
            expand = p->startRC.d.x + wx * p->dir.d.x;
            if (p->dir.d.x > 0)
            {
                p->nextRC.d.x = MAX(p->nextRC.d.x, expand);
            }
            else
            {
                p->nextRC.d.x = MIN(p->nextRC.d.x, expand);
            }
        }
    }
}


#if 0
void setSubPanelAttr (int spacing, int dx, int dy, uint32 flags)
{
    sbSpacing = spacing;
    sbDx = dx;
    sbDy = dy;
    sbFlags = flags;
}
#endif

panel * makeSubPanel (button *parent, int raster, int spacing, int sbFlags)
{
    panel *p;

    if (!parent)
        return NULL;

    if (parent->subPanel)
        return parent->subPanel;

    if (sbFlags == 0)
        sbFlags = SP_BUTTON3;

    p = parent->subPanel = (panel*)malloc(sizeof(panel));

    memset ((char*)p, 0, sizeof(*p));

    p->gid = parent->pPanel->gid;
    p->start.d.x = p->start.d.y = 0;
    p->cur.d.x = p->cur.d.y = 0;
    p->spacing = spacing;
    p->flags = sbFlags;

    p->primaryIsX = PRIMARY_X(raster);
    p->dir.d.x = ((raster == P_RIGHT_DOWN) || (raster == P_RIGHT_UP) ||
                  (raster == P_DOWN_RIGHT) || (raster == P_UP_RIGHT)) ? 1 : -1;
    p->dir.d.y = ((raster == P_RIGHT_DOWN) || (raster == P_LEFT_DOWN) ||
                  (raster == P_DOWN_RIGHT) || (raster == P_DOWN_LEFT)) ? 1 : -1;

    return p;
}

button * addSubButton (button *parent, int wx, int wy, char **pixmap, char *help, intptr_t userData, int attr)
{
    panel *p;
    button *b;

    if (!parent)
        return NULL;

    p = parent->subPanel;

    if (p == NULL)
        return NULL;

    b = makeButton (p, parent->handler, wx, wy, pixmap, help, userData, NULL, attr);
    b->parentButton = parent;

    return b;
}

void addArrow (button *b)
{
    unsigned int col = COL_ICONFG;

    buttonDrawPixel(b, BUTTON_WID(b)-3, 1, col);
    buttonDrawPixel(b, BUTTON_WID(b)-3, 2, col);
    buttonDrawPixel(b, BUTTON_WID(b)-2, 2, col);
    buttonDrawPixel(b, BUTTON_WID(b)-6, 3, col);
    buttonDrawPixel(b, BUTTON_WID(b)-5, 3, col);
    buttonDrawPixel(b, BUTTON_WID(b)-4, 3, col);
    buttonDrawPixel(b, BUTTON_WID(b)-3, 3, col);
    buttonDrawPixel(b, BUTTON_WID(b)-2, 3, col);
    buttonDrawPixel(b, BUTTON_WID(b)-1, 3, col);
    buttonDrawPixel(b, BUTTON_WID(b)-3, 4, col);
    buttonDrawPixel(b, BUTTON_WID(b)-2, 4, col);
    buttonDrawPixel(b, BUTTON_WID(b)-3, 5, col);
}

    

void drawButton (button *pButton, int doDraw)
{
    int x,y;
    int bx, by;

    SCR_CHANGE_START

    if (pButton->visible)
        for (y = pButton->y1; y <= pButton->y2; y++)
            for (x = pButton->x1; x <= pButton->x2; x++)
                SCR_SET_SAFE (x, y, doDraw ? pButton->bgColor : COL_CONTROL_BG);

    if (!doDraw)
    {
        pButton->visible = 0;

        SCR_CHANGE_FINISH
        return;
    }

    pButton->visible = 1;


    if (pButton->handler && doDraw)
    {
        pButton->handler (pButton, REDRAW, 0, 0);
    }

    if (pButton->text)
    {
        textPlotC ( pButton->x1+2,  pButton->y1+2, pButton->text, COL_ICONFG, 1);
    }

    if (pButton->s)
    {
        SDL_BlitSurface (pButton->s, &pButton->s->clip_rect, screen, &pButton->r);
    }

    for (y = pButton->y1; y <= pButton->y2; y++)
    {
        for (x = pButton->x1; x <= pButton->x2; x++)
        {
            if (!checkButtonPixel (pButton, x, y))
                continue;

            bx = x - pButton->x1;
            by = y - pButton->y1;

            if ((by < 10) && (bx < 10) &&  pButton->random && (random_pm[3 + by][bx] != ' '))
            {
                SCR_SET (x+1, y, 0xffff0000);
                SCR_SET (x, y, 0xff00ffff);
            }

            if (pButton->disabled)
            {
                unsigned int cv = SCR_GET(x, y);
                CH_SET_R(cv, ((int)CH_GET_R(cv) + 0x60)/3);
                CH_SET_G(cv, ((int)CH_GET_G(cv) + 0x60)/3);
                CH_SET_B(cv, ((int)CH_GET_B(cv) + 0x60)/3);
                SCR_SET(x, y, cv);
            }
        }
    }

    if (pButton->subPanel)
        addArrow (pButton);

    buttonBorderColor (pButton, pButton->borderCol);

    SCR_CHANGE_FINISH
}

void redrawButton (button *pButton)
{
    if (!pButton->visible)
        return;

    /* if the button belongs to a button group, redraw all since they might
     * depend on each other
     */
    if (pButton->groupFirst)
    {
        button *b;

        for (b = pButton->groupFirst; b != NULL; b = b->groupNext)
            drawButton (b, 1);
    }
    else
    {
        drawButton (pButton, 1);
    }
}


void enableButton (button *b, int enable)
{
    int disablePicker;

    if (enable == -1)
    {
        disablePicker = 0;
        enable=0;
    }
    else
    {
        disablePicker = !enable;
    }

    b->disabled = !enable;

    if (b->visible)
        drawButton (b, 1);

    if (disablePicker && pickerIsActive (b))
        exitPicker();
}

void enablePanel (panel *pPanel, int enable)
{
    button *pButton;

    if (enable == !pPanel->disabled)
        return;

    pPanel->disabled = !enable;

    for (pButton = pPanel->buttons; pButton != NULL; pButton = pButton->panelNext)
    {
        enableButton (pButton, enable);
    }
}

void showPanel (panel *pPanel, int doShow)
{
    button *pButton;

    if (!pPanel)
        return;

    clearHelp();
    clearTT();

    if (pPanel->gid && (currentGid != pPanel->gid))
    {
        pPanel->restore = doShow;
        pPanel->visible = 0;
        return;
    }
    
    if (doShow == pPanel->visible)
        return;

    pPanel->visible = doShow;

    {
        SDL_FillRect (screen, &pPanel->box, COL_CONTROL_BG);

        for (pButton = pPanel->buttons; pButton != NULL; pButton = pButton->panelNext)
        {
            pButton->visible = 1;
            drawButton (pButton, doShow);
        }
    }
}


void selectButton (button * b, int select)
{
    b->selected = select;

    if (!b->visible || b->drawing)
        return;

    b->drawing = 1;
    redrawButton (b);
    b->drawing = 0;
}

void selectPanelButtons (panel *pPanel, int sel)
{
    button *pButton;

    for (pButton = pPanel->buttons; pButton != NULL; pButton = pButton->panelNext)
    {
        selectButton (pButton, sel);
    }
}

button *posToButton (int x, int y)
{
    button *b = NULL;

    for (b = buttonList; b != NULL; b = b->next)
    {
        if (!b->visible || b->disabled)
            continue;

        if ((currentSubPanel != NULL) && (b->pPanel != currentSubPanel))
            continue;

        if ((x >= b->x1) && (x <= b->x2) &&
            (y >= b->y1) && (y <= b->y2))
        {
            return b;
        }
    }

    /* pseudo button for handling background actions in button area
     */
    if ((y >= hei) && (currentSubPanel == NULL))
        return &panelBg;

    return NULL;

}


void redrawAllButtons()
{
    button *b = NULL;
    SDL_Rect r;

    r.x = 0;
    r.y = hei;
    r.w = wid;
    r.h = options->scrHei - hei;

    SDL_FillRect (screen, &r, COL_CONTROL_BG);

    for (b = buttonList; b != NULL; b = b->next)
    {
        if (b->disabled)
            drawButton (b, b->visible);
    }

    for (b = buttonList; b != NULL; b = b->next)
    {
        if (!b->disabled)
            drawButton (b, b->visible);
    }
}

/* Start a new frame on the button stack and push all active buttons not belonging to
 * panel p
 */
void tmpDeactivateAllExcept (panel *p)
{
    button *b;

    dstackPush (DSTACK_SEP);
    for (b = buttonList; b != NULL; b = b->next)
    {
        if (!b->disabled && b->visible && (b->pPanel != p))
        {
            dstackPush (b);
            enableButton (b, -1);
        }
    }
}

/* activate all buttons in current button stack frame
 */
void reactivateAll ()
{
    button *b;

    while (1)
    {
        b = dstackPull();

        if ((b == NULL) || (b == DSTACK_SEP))
            return;
        
        enableButton (b, 1);
    }
}

void clearSubPanel (void)
{
    if (currentSubPanel == NULL)
        return;

    showPanel (currentSubPanel, 0);
    currentSubPanel = NULL;

    redrawAllButtons();
}

void getPanelRect (panel *p, SDL_Rect *r)
{
    int minx, miny, maxx, maxy;
    button *b;

    minx = p->start.d.x;
    miny = p->start.d.y;
    maxx = p->start.d.x;
    maxy = p->start.d.y;

    for (b = p->buttons; b != NULL; b = b->panelNext)
    {
        minx = MIN(minx, b->x1);
        miny = MIN(miny, b->y1);

        maxx = MAX(maxx, b->x2);
        maxy = MAX(maxy, b->y2);
    }

    r->x = minx;
    r->y = miny;
    r->w = maxx - minx;
    r->h = maxy - miny;
}

static void movePanelRel (panel *p, int dx, int dy)
{
    button *b;

    if (!p)
        return;

    p->cur.d.x += dx;
    p->cur.d.y += dy;
    p->start.d.x += dx;
    p->start.d.y += dy;

    p->nextRC.d.x += dx;
    p->nextRC.d.y += dy;
    p->startRC.d.x += dx;
    p->startRC.d.y += dy;

    for (b = p->buttons; b != NULL; b = b->panelNext)
    {
        b->x1 += dx;
        b->y1 += dy;
        b->x2 += dx;
        b->y2 += dy;

        b->groupRect.x += dx;
        b->groupRect.y += dy;

        setButtonRect (b);
    }
}


void checkPanelSize (panel *p, int force, int nodeps)
{
    SDL_Rect newBox;
    int dx1, dy1, dx2, dy2;
    int x, y;
    int i;

    getPanelRect (p, &newBox);

    dx1 = newBox.x - p->box.x;
    dy1 = newBox.y - p->box.y;

    dx2 = (newBox.x + newBox.w) - (p->box.x + p->box.w);
    dy2 = (newBox.y + newBox.h) - (p->box.y + p->box.h);

    p->box = newBox;

    if (nodeps)
        return;

    if (!force && (dx1 == 0) && (dx2 == 0) && (dy1 == 0) && (dy2 == 0))
        return;

    for (i = 0; i < MAX_PANEL_LINK; i++)
    {
        if (p->above[i])
            movePanel (p->above[i], p->box.x, p->box.y - p->above[i]->box.h - 2);
    }

    x = p->start.d.x;
    y = p->box.y + p->box.h + 4;
    for (i = 0; i < MAX_PANEL_LINK; i++)
    {
        if (p->below[i])
            movePanel (p->below[i], x, y);
    }

    for (i = 0; i < MAX_PANEL_LINK; i++)
    {
        if (p->left[i])
            movePanel (p->left[i], p->box.x - p->left[i]->box.w, p->box.y);
    }

    x = p->box.x + p->box.w + 4;
    y = p->start.d.y;
    for (i = 0; i < MAX_PANEL_LINK; i++)
    {
        if (p->right[i])
            movePanel (p->right[i], x, y);
    }
}


void movePanelNodep (panel *p, int x, int y)
{
    int dx;
    int dy;

    if (!p)
        return;

    if (y < hei)
        y = hei;

    dx = x - p->box.x;
    dy = y - p->box.y;

    movePanelRel (p, dx, dy);

    checkPanelSize (p, 0, 1);
}

void movePanel (panel *p, int x, int y)
{
    int dx;
    int dy;

    if (!p)
        return;

    dx = x - p->box.x;
    dy = y - p->box.y;

    movePanelRel (p, dx, dy);

    checkPanelSize (p, 0, 0);
}

void connectPanel (panel *p, panel *to, int side)
{
    int i;
    panel **arr;

    switch (side)
    {
        case LEFT_SIDE:
            arr = to->left;
            break;
        case TOP_SIDE:
            arr = to->above;
            break;
        case BOT_SIDE:
            arr = to->below;
            break;
        case RIGHT_SIDE:
            arr = to->right;
            break;

        default:
            return;
    }

    for (i = 0; i < MAX_PANEL_LINK; i++)
    {
        if (arr[i] == NULL)
        {
            arr[i] = p;
            break;
        }
    }

    checkPanelSize (to, 1, 0);
}

int handleSubPanel (int action, int x, int y)
{
    static int savedPosX, savedPosY, savePos;
    button *b, *cb;
    panel *p;
    int px, py;
    int bx, by;

    switch (EVENT_CODE(action))
    {
        case BUTTON_PRESS:
             b = posToButton (x,y);

            if ((b == NULL) || (b->subPanel == NULL))
                return 0;
        
            p = b->subPanel;

            if ((1 << BUTTON_ID(action) & p->flags) == 0)
                return 0;
 
             if ((p->flags & SP_CENTER_CURRENT) && (p->lastSel))
            {
                button *lb = p->lastSel;

                /* try to place the panel so that the most recent selection is under the mouse
                 */
                px = x - ((lb->x1 + lb->x2)/2 - p->start.d.x);
                py = y - ((lb->y1 + lb->y2)/2 - p->start.d.y);
            }
            else
            {
                px = x-10;
                py = y-10;
            }

            /* remember panel position for mouse warping */

            bx = px;
            by = py;

            /* correct panel position so that it fits on the screen */
            if ((px + p->size.d.x) >= wid)
                px -= (px + p->size.d.x) - wid + 1;

            if (px < 0)
                px = 0;

            if ((py + p->size.d.y) >= options->scrHei)
                py -= (py + p->size.d.y) - options->scrHei + 1;
        
            if (py < hei+5)
                py = hei+5;

            savePos = 1;
            savedPosX = x+CORR_X;
            savedPosY = y+CORR_Y;

            /* warp mouse pointer to default position */
            if ((bx != px) || (by != py))
            {
                SDL_WarpMouse (x + (px - bx), y + (py - by));
            }
            
            /* move panel to requested position */
            movePanel (p, px, py);

            /* deactivate all buttons except the panel buttons */
            tmpDeactivateAllExcept (p);

            showPanel (p, 1);

            currentSubPanel = p;
            currentButton = NULL;

            /* fall through */
        case BUTTON_MOVE:
            if (currentSubPanel == NULL)
                return 0;

            if (currentButton)
                buttonBorderColor (currentButton, COL_ICONDRAW);
                
            cb = posToButton (x, y);

            if (cb == NULL)
            {
                currentButton = NULL;
                return 1;
            }
            
            buttonBorderColor (cb, COL_ORANGE);

            currentButton = cb;

            return 1;
        
        case BUTTON_RELEASE:
            if (currentSubPanel == NULL)
                return 0;

            reactivateAll();
            cb = posToButton (x, y);

            if (cb)
            {
                if (cb->handler)
                {
                    cb->handler (cb, MKEVENT(SUBPANEL_SELECT, 2), x - cb->x1, y - cb->y1);
                }

                buttonBorderColor (cb, COL_ICONDRAW);

                currentSubPanel->lastSel = cb;

                if (savePos)
                {
                    SDL_WarpMouse (savedPosX, savedPosY);
                    savePos = 0;
                }
            }
            else
            {
                currentSubPanel->lastSel = NULL;
            }

            currentButton = NULL;

            clearSubPanel ();

            return 1;
    }
    return 0;
}

int 
handleButton (int action, int x, int y)
{
    button * b = NULL;
    static int panMove = 0;
    static int panMoveX, panMoveY;
    int button = BUTTON_ID(action);
    int event = EVENT_CODE(action);

    if (event == BUTTON_PRESS)
    {
        if (mouseButtonStates)
            return 1;
        
        mouseButtonStates |= 1 << button;
    }
    else if (event == BUTTON_RELEASE)
    {
        if (!mouseButtonPressed(button))
            return 1;
        
        mouseButtonStates &= ~(1 << button);
    }

    if (hidePanelSection())
        return 0;

    if (motionCapture(-1))
        return 0;

    if (handleSubPanel (action, x, y))
        return 1;

    switch (event)
    {
        case BUTTON_MOVE:
            b = currentButton;
            if (panMove)
                break;

            if (BUTTON_ID(action) == 0)
                return 0;

            break;
        
        case BUTTON_PRESS:
            if (clearHelp())
                return 1;

        case SCROLLUP:
        case SCROLLDOWN:
            b = posToButton (x, y);

            if (shiftPressed() && ctrlPressed() && b)
            {
                panMove = 1;
                panMoveX = b->pPanel->box.x - x;
                panMoveY = b->pPanel->box.y - y;
            }
            break;

        case BUTTON_RELEASE:
            if (!panMove)
            {
                b = currentButton;
                currentButton = NULL;
            }
            panMove = 0;
            break;

        default:
            b = NULL;
            break;
    }

    if (b != NULL)
    {
        if (panMove)
        {
            if (button == 3)
            {
                panelToTop (b->pPanel);
            }
            else
            {
                movePanelNodep (b->pPanel, x + panMoveX, y + panMoveY);
                redrawAllButtons();
            }
        }
        else
        {
            if (b->handler)
            {
                b->handler (b, action, x - b->x1, y - b->y1);
            }
        }
    }

    currentButton = b;

    return ((b != NULL));
}

void panelBgHandler (button * b, int action, int ox, int oy)
{
    int button = BUTTON_ID(action);
    int event = EVENT_CODE(action);

    if ((button == 3) && (event == BUTTON_RELEASE))
    {
        togglePanelGid();
    }
}

void init_widgets(void)
{
    memset ((void *) &panelBg, 0, sizeof (panelBg));

    panelBg.handler = panelBgHandler;

    redrawAllButtons();
}

void setButtonHelp (button *b, char *help)
{
    b->help = getHelpString(help);
}

static void *toolTipCallout = NULL;
static SDL_Surface *curTTBack = NULL;
static SDL_Rect  curTTPos;

void clearTT()
{
    if (curTTBack == NULL)
        return;

#if 0
    SDL_SetAlpha (curTTBack, 0, 0xff);
    SDL_SetAlpha (screen, 0, 0xff);
#endif

    SDL_BlitSurface (curTTBack, &curTTBack->clip_rect, screen, &curTTPos);
    SCR_CHANGED

    SDL_FreeSurface (curTTBack);

    curTTBack = NULL;
}

void showTT(void *cid, void* arg)
{
    button *b = (button*)arg;
    char *text;

    if (helpVisible)
        return;

    clearTT();

    if (b->pSetting)
        text = getTooltipString(b->pSetting->help);
    else
        text = getTooltipString(b->name);

    if (text)
    {
        renderTextBox (mousePos.d.x + 8, mousePos.d.y + 8, NULL, text, &curTTBack, &curTTPos);
    }
}



void toolTipMove (int x, int y)
{
    button *current;

    if (toolTipCallout == NULL)
        toolTipCallout = calloutNew();

    if (toolTipCallout == NULL)
        return;

    clearTT();
    calloutUnsched (toolTipCallout);

    current = posToButton (x, y);

    if (!current)
        return;

    calloutSched (toolTipCallout, 750, (FUNCPTR)showTT, (void*)current);
}

intptr_t clearHelp()
{
    if (helpVisible)
    {
        reactivateAll();
        redrawAllButtons();
        helpVisible = 0;
        return 1;
    }
    return 0;
}

void showHelp(int x, int y)
{
    button *b = posToButton (x, y);
    char *help, *banner;

    clearTT();

    if (clearHelp())
        return;

    if (b == &panelBg)
    {
        help = getHelpString("keys");
        banner = getTooltipString ("keys");
    }
    else
    {
        if ((help = b->help) == NULL)
            return;

        banner = getTooltipString (b->name);
    }

    if (!banner || !help)
        return;

    tmpDeactivateAllExcept (NULL);
    renderTextBox (-1, -1, banner, help, NULL, NULL);

    helpVisible = 1;
    escConnect (clearHelp);
}

int checkButtonPixel (button *b, int sx, int sy)
{
    if ((sx < b->x1) || (sx > b->x2) || (sy < b->y1) || (sy > b->y2))
        return 0;

    if ((sx < 0) || (sx >= options->scrWid) || (sy < hei) || (sy >= options->scrHei))
        return 0;

    return 1;
}
int checkButtonPixelB (button *b, int sx, int sy)
{
    if ((sx <= b->x1) || (sx >= b->x2) || (sy <= b->y1) || (sy >= b->y2))
        return 0;

    if ((sx < 0) || (sx >= options->scrWid) || (sy < hei) || (sy >= options->scrHei))
        return 0;

    return 1;
}

void buttonDrawPixel (button *b, int x, int y, unsigned int color)
{
    x += b->x1 + 1;
    y += b->y1 + 1;

    if (!checkButtonPixel (b, x, y))
        return;

    SCR_SET(x, y, color);
}

void buttonDrawPixelB (button *b, int x, int y, unsigned int color)
{
    x += b->x1 + 1;
    y += b->y1 + 1;

    if (!checkButtonPixelB (b, x, y))
        return;

    SCR_SET(x, y, color);
}

unsigned int buttonGetPixel (button *b, int x, int y)
{
    x += b->x1 + 1;
    y += b->y1 + 1;

    if (!checkButtonPixel (b, x, y))
        return 0;

    if ((x < 0) || (x >= options->scrWid) || (y < hei) || (y >= options->scrHei))
        return 0;

    return SCR_GET(x, y);
}

unsigned int buttonGetRGB (button *b, int x, int y)
{
    return CH_GET_RGB(buttonGetPixel(b, x, y));
}

void clearCurrentButtonSelection (void)
{
    if (currentButton == NULL)
        return;

    handleButton (MKEVENT(BUTTON_RELEASE,1), 0, 0);

    currentButton = NULL;
    mouseButtonStates = 0;
}

int mouseButtonPressed(int b)
{
    if (mouseButtonStates & (1<<b))
        return 1;

    return 0;
}

void setCurrentPanelGid (int gid)
{
    panel *p;
    
    clearTT();
    clearHelp();
       
    for (p = panels; p != NULL; p = (panel*)p->next)
    {
        if (!p->gid || (p->gid != currentGid))
            continue;
        
        p->restore = p->visible;
        showPanel (p, 0);
    }
    
    currentGid = gid;
    
    for (p = panels; p != NULL; p = (panel*)p->next)
    {
        if (!p->gid || (p->gid != currentGid))
            continue;
        
        showPanel (p, p->restore);
    }    
}

int getCurrentPanelGid(void)
{
    return currentGid;
}

void keyDownCapture (char key, int ctrl, int shift)
{
    if (!keyCaptureCb)
        return;

    keyCaptureCb (key, ctrl, shift, keyCaptureCbArg);
}

void keyCapture (FUNCPTR cb, void *cbArg)
{
    keyCaptureCbArg = cbArg;
    keyCaptureCb = cb;
}

int keyCaptureActive()
{
    return (keyCaptureCb != NULL);
}

int rectIntersects (SDL_Rect *r1, SDL_Rect *r2)
{
    if ((r1->x + r1->w) < r2->x)
        return 0;

    if ((r1->y + r1->h) < r2->h)
        return 0;

    if ((r2->x + r2->w) < r1->x)
        return 0;

    if ((r2->y + r2->h) < r1->h)
        return 0;

    return 1;
}

void walkPanelsBelow (panel *p, FUNCPTR cb, void *cbArg)
{
    panel *wp;

    for (wp = panels; wp != NULL; wp = wp->next)
    {
        if (wp == p)
            continue;
        
        if (rectIntersects (&p->box, &wp->box))
        {
            if (cb (p, wp, cbArg))
                return;
        }
    }
}
