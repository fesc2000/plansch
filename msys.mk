##### system paths

SRCDIR =	$(shell pwd)

## Root path for SDL headers/libs (default is /usr)
#
SDLROOT=/c/dev/SDL-1.2.14
#SDLROOT=/c/dev/SDL2-2.0.3

# should contain SDL, glew
# 
WINLIB = 	$(SRCDIR)/winlib32


ARCH=32

##### Optimization settings ######


## Default compiler flags (SSE2 is required)
#
#CFLAGS = -O3 -m32 
#CFLAGS = -O3 -msse2 -m32
CFLAGS = -O3 -msse2 -mfpmath=sse -m$(ARCH) -fno-builtin
#CFLAGS =  -g -msse2 -m32 -m$(ARCH) -fno-builtin
CFLAGS += -g

## CPU optimization
#CPU=core2
#CFLAGS += -march=$(CPU) -mtune=$(CPU)

LIBDIRS += -L$(WINLIB) -L$(SDLROOT)/lib

# Depends on whether 32- or 64-bit is used. Required (e.g.) by OpenCL
#
LIBARCH=x86
#LIBARCH=x86_64

###### Features ###########

USE_SSE2=y
USE_SSE41=n
USE_AVX=n

## Variants to build
#
# default, ssse3 and opengl stuff should build on must systems.
#
VARIANTS =	default opengl
VARIANTS +=	opencl

## MP support 
# Defining BIND_THREADS associates threads with specific CPUs
#
CFLAGS += -DOPENMP -fopenmp
#CFLAGS += -DBIND_THREADS

# OpenCL development basedir. This might as well work for other vendors ...
#
OPENCLROOT=/c/OpenCL

## Display some statistics
#
#CFLAGS += -DSTATS 

## 
# multi-threading by dividing the draw area
CFLAGS += -DMT_BORDERS

# Multi-threading by using a dedicated thread for wimps and non-wimps (buggy)
# CFLAGS += -DMT_WIMPS

#CFLAGS += -DTTF_SUPPORT -DTTF_FONT=\"plansch.ttf\"
#LIBS += -lSDL_ttf
################## System specific settings ##################

RM = del

## Compiler to use
#
CC = gcc
CPP = g++ -fpermissive

## Mingw support
#
CFLAGS += -D_MINGW -DUSE_GLEW -DOPENGL_GLEW 
#CFLAGS += -D_MINGW
LIBS = -lSDLmain -lSDL -lpthread 
OPENGL_LIBS = -lopengl32 -lglew32
#OPENGL_LIBS = -lopengl

exesuffix   =	.exe

KERNEL	    = win
MACH	    = i386
