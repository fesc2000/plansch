/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define DYDX_MAX    1e12

#define CHECK

signed char adj2n[256 * 2];
static int dir2deg[] = { 315, 0, 45, 270, 90, 225, 180, 135 };
static int dir2len[] = { 2, 3, 2, 3, 3, 2, 3, 2 };

int maxrad = 0;
int minrad = 0;

scTable_t scTable[SIN_COS_TABLE_ENTRIES];

/* gaussian random number (from the From the GNU Scientific Library)
*/
float
ran_gaussian (float sigma)
{
  float x, y, r2;

  do
    {
      /* choose x,y in uniform square (-1,-1) to (+1,+1) */

      x = -1 + 2 * ((float)rand() / (RAND_MAX + 1.0));
      y = -1 + 2 * ((float)rand() / (RAND_MAX + 1.0));

      /* see if it is in the unit circle */
      r2 = x * x + y * y;
    }
  while (r2 > 1.0 || r2 == 0.0);

  /* Box-Muller transform */
  return sigma * y * sqrt (-2.0 * log (r2) / r2);
}

/* calculate the position where a vector hits the pixel border [0,MAX_SPEED][[0,MAX_SPEED]
 * vector's starting position is pos, the direction is vec
 * result is stord in vec.
 * Probably not the most efficient algorithm ..
 */
void toBorder (vec_t *pos, vec_t *vec)
{
    vec_t rc;
    float slope; /* should use fixed point arith .. */
    int cy, cx;
#ifdef CHECK
    int sx = SCREENC(pos->d.x);
    int sy = SCREENC(pos->d.y);
#endif

    rc.d.x = pos->d.x & MAX_SPEED;
    rc.d.y = pos->d.y & MAX_SPEED;

    /* the easy part: x or y vector component is 0 .. */
    if (vec->d.x == 0)
    {
        if (vec->d.y < 0)
        {
            pos->d.y &= ~MAX_SPEED;
        }
        else if (vec->d.y > 0)
        {
            pos->d.y |= MAX_SPEED;
        }
        goto check;
    }
    else if (vec->d.y == 0)
    {
        if (vec->d.x < 0)
        {
            pos->d.x &= ~MAX_SPEED;
        }
        else if (vec->d.x > 0)
        {
            pos->d.x |= MAX_SPEED;
        }
        goto check;
    }

    /* try if we hit the left or right boundary within the boulding box 
     */
    slope = (float)vec->d.y / (float)vec->d.x;

    if (vec->d.x < 0)
    {
        cy = -rc.d.x * slope + rc.d.y;

        if ((unsigned int)cy < GRID)
        {
            pos->d.y = (pos->d.y & ~MAX_SPEED) + cy;
            pos->d.x = pos->d.x & ~MAX_SPEED;
            goto check;
        }
    }
    else
    {
        cy = -rc.d.x * slope + (float)MAX_SPEED * slope + rc.d.y;

        if ((unsigned int)cy < GRID)
        {
            pos->d.y =(pos->d.y & ~MAX_SPEED) + cy;
            pos->d.x |= MAX_SPEED;
            goto check;
        }
    }

    /* try if we hit the upper or lower boundary within the boulding box 
     */
    slope = (float)vec->d.x / (float)vec->d.y;

    if (vec->d.y < 0)
    {
        cx = -rc.d.y * slope + rc.d.x;
        if ((unsigned int)cx < GRID)
        {
            pos->d.x = (pos->d.x & ~MAX_SPEED) + cx;
            pos->d.y = (pos->d.y & ~MAX_SPEED);
            goto check;
        }
    }
    else
    {
        cx = -rc.d.y * slope + MAX_SPEED * slope + rc.d.x;

        if ((unsigned int)cx < GRID)
        {
            pos->d.x = (pos->d.x & ~MAX_SPEED) + cx;
            pos->d.y |= MAX_SPEED;
            goto check;
        }
    }

    /* should never reach this place .. */
    printf ("toBorder Failed!\n");
check:
#ifdef CHECK
    if ((sx != SCREENC(pos->d.x)) || (sy != SCREENC(pos->d.y)))
    {
        printf ("toBorder Failed!\n");
    }
#endif
}

/* calculate the position where a vector hits the adjacent pixel border [-1,GRID][[-1,GRID]
 * vector's starting position is pos, the direction is vec
 * result is stord in vec.
 * Probably not the most efficient algorithm ..
 */
void toNextBorder (vec_t *pos, vec_t *vec)
{
    vec_t rc;
    float slope; /* should use fixed point arith .. */
    int cy, cx;
#ifdef CHECK
    int sx = SCREENC(pos->d.x);
    int sy = SCREENC(pos->d.y);
#endif

    rc.d.x = pos->d.x & MAX_SPEED;
    rc.d.y = pos->d.y & MAX_SPEED;

    /* the easy part: x or y vector component is 0 .. */
    if (vec->d.x == 0)
    {
        if (vec->d.y < 0)
        {
            pos->d.y &= ~MAX_SPEED;
            pos->d.y -= 1;
            goto check;
        }
        else if (vec->d.y > 0)
        {
            pos->d.y |= MAX_SPEED;
            pos->d.y += 1;
            goto check;
        }
        printf ("dx = dy = 0\n");
        goto check;
    }
    else if (vec->d.y == 0)
    {
        if (vec->d.x < 0)
        {
            pos->d.x &= ~MAX_SPEED;
            pos->d.x -= 1;
            goto check;
        }
        else if (vec->d.x > 0)
        {
            pos->d.x |= MAX_SPEED;
            pos->d.x += 1;
            goto check;
        }
        printf ("dx = dy = 0\n");
        goto check;
    }

    /* try if we hit the left or right boundary within the bounding box 
     */
    slope = (float)vec->d.y / (float)vec->d.x;

    if (vec->d.x < 0)
    {
        cy = -rc.d.x * slope + (float)(-1) * slope + rc.d.y;

        if ((cy >= -1) && (cy <= GRID))
        {
            pos->d.y = (pos->d.y & ~MAX_SPEED) + cy;
            pos->d.x = (pos->d.x & ~MAX_SPEED) - 1;
            goto check;
        }
    }
    else
    {
        cy = -rc.d.x * slope + (float)GRID * slope + rc.d.y;

        if ((cy >= -1) && (cy <= GRID))
        {
            pos->d.y =(pos->d.y & ~MAX_SPEED) + cy;
            pos->d.x = (pos->d.x | MAX_SPEED) + 1;
            goto check;
        }
    }

    /* try if we hit the upper or lower boundary within the boulding box 
     */
    slope = (float)vec->d.x / (float)vec->d.y;

    if (vec->d.y < 0)
    {
        cx = -rc.d.y * slope + (float)(-1) * slope + rc.d.x;
        if ((cx >= -1) && (cx <= GRID))
        {
            pos->d.x = (pos->d.x & ~MAX_SPEED) + cx;
            pos->d.y = (pos->d.y & ~MAX_SPEED) - 1;
            goto check;
        }
    }
    else
    {
        cx = -rc.d.y * slope + (float)GRID * slope + rc.d.x;

        if ((cx >= -1) && (cx <= GRID))
        {
            pos->d.x = (pos->d.x & ~MAX_SPEED) + cx;
            pos->d.y = (pos->d.y | MAX_SPEED) + 1;
            goto check;
        }
    }

    /* should never reach this place .. */
    printf ("toNextBorder Failed!\n");
check:
#ifdef CHECK
    if ((sx == SCREENC(pos->d.x)) && (sy == SCREENC(pos->d.y)))
    {
        printf ("toNextBorder Failed (same)!\n");
    }

    {
        int dx = SCREENC(pos->d.x) - sx;
        int dy = SCREENC(pos->d.y) - sy;

        if ((dx < -1) || (dx > 1) || (dy < -1) || (dy > 1))
        {
            printf ("toNextBorder Failed (diff > 1)! %d %d\n", dx, dy);
        }

#if 0
        if ((dx != 0) && (dy != 0))
        {
            printf ("toNextBorder Failed (both != 0): d=%d/%d!\n", vec->d.x, vec->d.y);
            BACKTRACE
        }
#endif
    }
#endif
}


int lIntersect (pLine *l, vec_t *vStart, vec_t *vec, vec_t *iCord)
{
    PLINE_FPTYPE ix, iy;
    PLINE_FPTYPE dyDx, y0;

    /* check where the lines intersect
     */
    if (vec->d.x == 0)
    {
                if (l->dyDx >= DYDX_MAX)
                {
                        /* paricle moves vertical, line as well -> never intersect 
                        */
                        return 0;
                }
                ix = (PLINE_FPTYPE)vStart->d.x;
                iy = l->y0 + ix * l->dyDx;
    }
    else 
    {
        /* get line formula of particle */
        dyDx = (PLINE_FPTYPE)vec->d.y / (PLINE_FPTYPE)vec->d.x;

        y0 = (PLINE_FPTYPE)vStart->d.y - (PLINE_FPTYPE)vStart->d.x * dyDx;

        /* x/y intersection point */
        ix = (y0 - l->y0) / (l->dyDx - dyDx);
        iy = y0 + ix * dyDx;
    }

    iCord->d.x = (int)ix;
    iCord->d.y = (int)iy;


    return 1;
}

void vecToLine (vec_t *vStart, vec_t *vec, pLine *l)
{
    if (vec->d.x == 0)
    {
        l->dyDx = DYDX_MAX;
        return;
    }

    l->dyDx = (PLINE_FPTYPE)vec->d.y / (PLINE_FPTYPE)vec->d.x;
    l->y0 = (PLINE_FPTYPE)vStart->d.y - (PLINE_FPTYPE)vStart->d.x * l->dyDx;
}



FLOAT xquad2deg[] = {
    225,                        // 0
    225,                        // 1
    180,                        // 2
    180,                        // 3
    135,                        // 4
    135,                        // 5

    315,                        // 6
    0,                                // 7
    0,                                // 8
    45,                                // 9
    45,                                // 10
    45                                // 11
};

int yquad2deg[] = {
    45,                                // 0
    45,                                // 1
    90,                                // 2
    90,                                // 3
    135,                        // 4
    135,                        // 5

    315,                        // 6
    270,                        // 7
    270,                        // 8
    225,                        // 9
    225,                        // 10
    225,                        // 11
};

int xquad2vec[12 * 2];
int yquad2vec[12 * 2];

void vecInit (void)
{
    int i;

    for (i = 0; i < 12; i++)
    {
        xquad2vec[i * 2] = NORM_X (xquad2deg[i]);
        xquad2vec[i * 2 + 1] = NORM_Y (xquad2deg[i]);


        yquad2vec[i * 2] = NORM_X (yquad2deg[i]);
        yquad2vec[i * 2 + 1] = NORM_Y (yquad2deg[i]);

    }

}

int nd = 0;
void normalize (int *dx, int *dy)
{
#undef TABLE_BASED_NORMALIZE
#ifdef TABLE_BASED_NORMALIZE
    int quad;
    int hx, hy;

    hx = *dx / 2;
    hy = *dy / 2;

    if (unlikely ((hy == 0) && (hx == 0)))
        return;

    if (abs (*dx) >= abs (*dy))
    {
        quad = (*dx - *dy + hx) / abs (hx) + 5;
        *dx = xquad2vec[quad * 2];
        *dy = xquad2vec[quad * 2 + 1];
    }
    else
    {
        quad = (*dy - *dx + hy) / abs (hy) + 5;
        *dx = yquad2vec[quad * 2];
        *dy = yquad2vec[quad * 2 + 1];
    }
#else
    FLOAT vec_len;

    vec_len = VEC_LEN (*dx, *dy);

    *dx = (FLOAT)(*dx * NORM_LEN) / vec_len;
    *dy = (FLOAT)(*dy * NORM_LEN) / vec_len;
#endif

}


void reflect (int *ax, int *ay, int nx, int ny)
{
    int vp;

    vp = ((*ax * nx) + (*ay * ny)) / NORM_LEN;

    *ax = *ax - 2 * nx * vp / NORM_LEN;
    *ay = *ay - 2 * ny * vp / NORM_LEN;

}

void mathInit (void)
{
    int i, dir;
    int ax, ay;

    vecInit();

    maxrad = (int)(sqrt(MAX_SPEEDQF / 2.0)) / 2;
    minrad = - maxrad;

    for (i = 1; i < 256; i++)
    {
        ax = ay = 0;

        for (dir = 0; dir < 8; dir++)
        {
            if ((i & (1 << dir)) != 0)
            {

                ax += (int) NORM_X (dir2deg[dir]) * dir2len[dir];
                ay += (int) NORM_Y (dir2deg[dir]) * dir2len[dir];
            }
        }
        if (ax || ay)
        {
            FLOAT l = VEC_LEN (ax, ay);
            ax = (int) ((((FLOAT) ax * NORM_LEN) / l));
            ay = (int) ((((FLOAT) ay * NORM_LEN) / l));
        }

        adj2n[i * 2] = ax;
        adj2n[i * 2 + 1] = ay;
    }

    for (i = 0; i < SIN_COS_TABLE_ENTRIES; i++)
    {
        scTable[i].ns = -sin (((float)i * 2 * M_PI) / (float)SIN_COS_TABLE_ENTRIES);
        scTable[i].c =   cos (((float)i * 2 * M_PI) / (float)SIN_COS_TABLE_ENTRIES);
    }
}

const float rads (float dx, float dy)
{
    float r = atan2(-dy, dx);

    if (r < 0.0)
        r += 2 * M_PI;

    return r;
}

const int degs (float dx, float dy)
{
    int d = (rads(dx, dy)) * (360.0 / 2 * M_PI);

    return d % 360;
}

const int deg_sub(int d1, int d2)
{
    int d = d2 - d1;

    while (d >= 360)
        d -= 360;

    while (d <= -360)
        d += 360;

    if (d > 180)
        d -= 180;

    return d;
}

const int bitsSet (uint val, int bsize)
{
    int i;
    int rc = 0;

    if (val == 0)
        return 0;

    if (bsize == 0)
        bsize = 32;

    for (i = 0; i < bsize; i++)
    {
        if (val & (1<<i))
            rc++;
    }

    return rc;
}

const int lowestBit (uint val, int bsize)
{
    int i;

    if (val == 0)
        return -1;

    if (bsize == 0)
        bsize = 32;

    for (i = 0; i < bsize; i++)
    {
        if (val & (1<<i))
            return i;
    }

    return -1;
}

const int highestBit (uint val, int bsize)
{
    int i;

    if (val == 0)
        return -1;

    if (bsize == 0)
        bsize = 32;

    for (i = bsize-1; i >= 0; i--)
    {
        if (val & (1<<i))
            return i;
    }

    return -1;
}

/* compare slope of two lines. Uses fixed point arithmetic, f is the 
 * multiplicator which determines the accuracy (1 for integer arithmetic).
 * Slope is identical if (dx*f)/dy and (dy*f)/dx  of both vectors are
 * identical.
 */ 
const int sameSlope (const vec_t *v1, const vec_t *v2, int f)
{
    /* h/v only?
     */
    if (((v1->d.x == 0) && (v2->d.x == 0)) ||
        ((v1->d.y == 0) && (v2->d.y == 0)))
        return 1;

#if 0
    /* dont divide by zero
    */
    if ((v1->d.x == 0) || (v2->d.x == 0) ||
        (v1->d.y == 0) || (v2->d.y == 0))
        return 0;
#endif

    if ((v1->d.y) && (v2->d.y))
        if (((v1->d.x * f) / v1->d.y) == ((v2->d.x * f) / v2->d.y))
            return 1;

    if ((v1->d.x) && (v2->d.x))
        if (((v1->d.y * f) / v1->d.x) == ((v2->d.y * f) / v2->d.x))
            return 1;

    return 0;
}

int f_popcount(uint32_t i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

/* float to half conversion from http://blog.fpmurphy.com/2008/12/half-precision-floating-point-format_14.html
 */
uint16_t
static floatToHalfI(uint32_t i)
{
    register int s =  (i >> 16) & 0x00008000;                   // sign
    register int e = ((i >> 23) & 0x000000ff) - (127 - 15);     // exponent
    register int f =   i        & 0x007fffff;                   // fraction

    // need to handle NaNs and Inf?
    if (e <= 0) {
        if (e < -10) {
            if (s)                                              // handle -0.0
               return 0x8000;
            else
               return 0;
        }
        f = (f | 0x00800000) >> (1 - e);
        return s | (f >> 13);
    } else if (e == 0xff - (127 - 15)) {
        if (f == 0)                                             // Inf
            return s | 0x7c00;
        else {                                                  // NAN
            f >>= 13;
            return s | 0x7c00 | f | (f == 0);
        }
    } else {
        if (e > 30)                                             // Overflow
            return s | 0x7c00;
        return s | (e << 10) | (f >> 13);
    }
}

uint16_t
floatToHalf(float i)
{
    union { float f; uint32_t i; } v;
    v.f = i;
    return floatToHalfI(v.i);
}

