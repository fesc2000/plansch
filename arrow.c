/*
Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

int arrowFixed = 0;
int arrowEnabled = 0;
int arrowHidden = 0;
vec_t lastArrowPos;
void *arrowObj = NULL;
void *angleObj = NULL;
int startX, startY;
int vecX, vecY;
int arrowAngle = 0;

void hideArrow (int hide)
{
    if (!arrowEnabled)
        return;

    arrowHidden = hide;

    if (hide)
        clearArrow();
    else
        showArrow(lastArrowPos.d.x, lastArrowPos.d.y);
}

void clearArrow()
{
    if (!arrowEnabled)
        return;

    polyHide (arrowObj);
    polyHide (angleObj);
}

void enableArrow (int enable)
{
    int col;

    arrowEnabled = enable;

    if (arrowObj == NULL)
    {
        col = COL_LINE;
#ifdef USE_OPENGL
        CH_SET_A(col, 0x80);
        arrowObj = polyNew (10, POLY_SCREEN|POLY_FILLED, col);
#else
        arrowObj = polyNew (10, POLY_SCREEN, col);
#endif
    }

    if (angleObj == NULL)
    {
        col = COL_LINE;
#ifdef USE_OPENGL
        CH_SET_A(col, 0x80);
        angleObj = polyNew (360, POLY_SCREEN|POLY_FILLED, col);
#else
        angleObj = polyNew (360, POLY_SCREEN, col);
#endif
    }

    if (!arrowObj || !angleObj)
    {
        arrowEnabled = 0;
        return;
    }

    if (!enable)
    {
        arrowFixed = 0;
    }

    arrowEnabled = enable;
}


void setArrowVec (int dx, int dy)
{
    vec_t a[400];
    int n;

    if (!arrowEnabled)
        return;

    vecX = dx;
    vecY = dy;

    polyHide (arrowObj);
    polyHide (angleObj);
    n = makeArrowPoly (a, 0, 0, dx, dy);
    polySetCoords (arrowObj, n, a, NULL);
    polyShow (arrowObj);

    if ((arrowAngle != 0) && ((dx != 0) || (dy != 0)))
    {
        n = makeAnglePoly (a, 0, 0, dx, dy, arrowAngle, 100);
        polySetCoords (angleObj, n, a, NULL);
        polyShow (angleObj);
    }
}

void getArrowVec (int *px, int *py)
{
    if (!arrowFixed)
    {
        *px = *py = 0;
    }
    else
    {
        *px = vecX;
        *py = vecY;
    }
}

int getArrowAngle (void)
{
    return arrowAngle;
}

void setArrowAngle (int angle)
{
    char tmpStr[50];
    vec_t a[400];
    int n;

    if (angle < 0)
        angle = 0;

    if (angle > 359)
        angle = 360;

    if (angle == arrowAngle)
        return;

    arrowAngle = angle;

    n = makeAnglePoly (a, 0, 0, vecX, vecY, arrowAngle, 100);
    polySetCoords (angleObj, n, a, NULL);
    polyShow (angleObj);

    sprintf (tmpStr, "Angle: %d", arrowAngle);
    statText (4, tmpStr);
}

void updateArrow (int x, int y)
{
    if (!arrowEnabled)
        return;

    if (arrowFixed)
    {
        if ((y < INTERNC(hei)) && !arrowHidden)
        {
            polyShow (arrowObj);
            polyMove (arrowObj, x, y);
            polyShow (angleObj);
            polyMove (angleObj, x, y);
        }
        else
        {
            polyHide (arrowObj);
            polyHide (angleObj);
        }
    }
    else
    {
        setArrowVec (x - startX, y - startY);
    }
}

void resetArrow (int x, int y)
{
    uint32 col;

    if (arrowObj == NULL)
    {
        col = COL_LINE;
#ifdef USE_OPENGL
        CH_SET_A(col, 0x80);
#endif
        arrowObj = polyNew (10, POLY_SCREEN|POLY_FILLED, col);
    }

    if (!arrowObj)
        return;

    polyClear (arrowObj);
    polyMove (arrowObj, x, y);
    polyClear (angleObj);
    polyMove (angleObj, x, y);

    startX = x;
    startY = y;
    vecX = vecY = 0;

    arrowFixed = 0;

    arrowEnabled = 1;
}

void fixArrow ()
{
    if (!arrowEnabled)
        return;

    arrowFixed = 1;
}

void showArrow (int x, int y)
{
    if (!arrowEnabled)
        return;

    lastArrowPos.d.x = x;    
    lastArrowPos.d.y = y;    
    updateArrow (x,y);
}

