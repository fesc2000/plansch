/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include "unistd.h"

static pthread_cond_t        gpuStartCond = PTHREAD_COND_INITIALIZER;
static pthread_cond_t        gpuDoneCond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t gpuMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t gpuThread;
static int startGpu = 0;
static int gpuDone = 0;

int glReady = 0;
int glScreenUpdate = 0;
int oglNoThread = 0;

int oglDedicatedThread (void)
{
    return !oglNoThread;
}

int oglDedicatedThreadSet (int val)
{
    oglNoThread = (val == 0);
    return val;
}

void *gc (void *arg)
{
#ifdef BIND_THREADS
    cpu_set_t *cset;
    unsigned int setsize;

    if (options->numThreads < numCores)
    {
        cset = CPU_ALLOC(numCores);
        setsize = CPU_ALLOC_SIZE(numCores);
        CPU_ZERO_S(setsize, cset);
        CPU_SET_S (numCores-1, setsize, cset);

        pthread_setaffinity_np (pthread_self(), setsize, cset);

        printf ("OpenGL thread bound to core mask 0x%x\n", 1<<(numCores-1));
    }
#endif

    THREAD_NAME("oglWork");

    oglWorkThread ();

    return NULL;
}

int grav_gpu_init (intptr_t fullscreen)
{
    if (oglNoThread)
        return 1;

    /* release opengl context. The graphics thread will restore it later
     */
    openglReleaseContext();

#if 1
    pthread_create (&gpuThread, NULL,  gc, (void*)fullscreen);
#else
    SDL_CreateThread (gc, (void*)fullscreen);
#endif

    while (!oglInitDone())
        sleep(1);

    return 1;
}

void gpuStart(int scrUpdate)
{
    if (oglNoThread)
    {
        glScreenUpdate = scrUpdate;
        oglWork (scrUpdate);
        return;
    }

    pthread_mutex_lock (&gpuMutex);
    startGpu = 1;
    glScreenUpdate = scrUpdate;
    gpuDone = 0;
    pthread_cond_signal (&gpuStartCond);
    pthread_mutex_unlock (&gpuMutex);
}

void gpuWait(void)
{
    if (oglNoThread)
        return;

    pthread_mutex_lock (&gpuMutex);
    if (!gpuDone && GETVAL32(gpuSync))
        pthread_cond_wait (&gpuDoneCond, &gpuMutex);
    pthread_mutex_unlock (&gpuMutex);
}

void gpuWaitScreenUpdateDone ()
{
    if (oglNoThread)
        return;

    pthread_mutex_lock (&gpuMutex);
    if (glScreenUpdate)
        pthread_cond_wait (&gpuDoneCond, &gpuMutex);
    pthread_mutex_unlock (&gpuMutex);
}

void gpuSignalScreenUpdateDone()
{
    if (oglNoThread)
        return;

    pthread_mutex_lock (&gpuMutex);
    glScreenUpdate = 0;
    pthread_cond_signal (&gpuDoneCond);
    pthread_mutex_unlock (&gpuMutex);
} 

int gpuWaitForStart()
{
    if (oglNoThread)
        return 0;

    int rc;
    pthread_mutex_lock (&gpuMutex);
    if (!startGpu && GETVAL32(gpuSync))
        pthread_cond_wait (&gpuStartCond, &gpuMutex);
    rc = glScreenUpdate;
    pthread_mutex_unlock (&gpuMutex);

    return rc;
}

void gpuSignalDone()
{
    if (oglNoThread)
        return;

    pthread_mutex_lock (&gpuMutex);
    gpuDone = 1;
    startGpu = 0;
    pthread_cond_signal (&gpuDoneCond);
    pthread_mutex_unlock (&gpuMutex);
} 
