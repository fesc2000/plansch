
/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include "chain.h"

#define CANSHARE

int chConnectToPrevious = 0;
int chConnectToNeighbour = 1;
int chConnectToWall = 0;
int chConnectToAll = 0;

static int drawing = 0;
static int drawingFirst = 0;
static particle *firstParticle = NULL;
static particle *lastParticle = NULL;

static void connectHandler (setting *s);

static void *searchCallout = NULL;

int chainPtype = 0;
static int wimpType = 0;

static int connectPhase = 0;
static particle *connectP1 = NULL;
static particle *connectP2 = NULL;
static particle *nearest = NULL;
static int disconnecting = 0;
static int doDisconnect;

static scrPoly __attribute__ ((unused)) *connectSeek;

#ifdef USE_AVX
screeninfo * move_chain_avx(particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );
#endif
#ifdef USE_SSE41
screeninfo * move_chain_sse41(particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );
#endif
#ifdef USE_SSE2
screeninfo * move_chain_sse2 (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );
#endif
screeninfo * move_chain_c (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *p2 );

static scrPoly *m1, *m2, *conn;

static void chainInit (int ptype);
static void searchNearestAndFollow (void *arg);
static void chainFollow (particle *p, void *arg);
static void chainApplySetting (particle *p, setting *s);

SETTING(numLinks) =
{
    .minValue = 1,
    .maxValue = MAX_LINKS,
    .defaultValue = 6,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .discrete = 1,
    .numDiscrete = 6,
    .valueArray = { 1, 2, 3, 4, 5, 6 },

    .name = "NumberOfLinks"
};

SETTING(chainConnectToPassive) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect with closed link",
};


SETTING(chainActivate) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 1,
    .name = "Activate/deactivate links",
};

SETTING(chainMaxDist) =
{
    .minValue = 1,
    .maxValue = 500,
    .minDiff = 1,
    .defaultValue = 500,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Maximum part. dist.",
    .cb = (FUNCPTR)NULL,
};

SETTING(chainConnectToPrevious) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect particles while drawing",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(chainReconnectToFirst) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .partAttr = 0,
    .name = "Connect particle while drawing (ring)",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(chainConnectToNeighbour) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 1,
    .partAttr = 0,
    .name = "Connect Particle to neighbours",
    .cb = (FUNCPTR)connectHandler,
};

SETTING(chainAttraction) =
{
    .minValue = 1,
    .maxValue = GRID/30,
    .minDiff = 1,
    .defaultValue = 300,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Chain Attraction",
    .cb = (FUNCPTR)NULL,
};

void connectTwoCb (void);
void connectTwoReset (void);

SETTING(connectTwo) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .name = "ConnectTwo",
    .cb = (FUNCPTR)connectTwoCb,
};

SETTING(actPotential) =
{
    .minValue = 0,
    .maxValue = 100,
    .minDiff = 1,
    .defaultValue = 100,
    .partAttr = 1,
    .name = "actionPotential",
};

static void connectHandler (setting *s)
{
    int val = getValue (s);
    chConnectToPrevious = 0;
    chConnectToNeighbour = 0;

    if ((s == &chainConnectToPrevious) && val)
    {
        modifySetting (&chainReconnectToFirst, 0, 0);
        modifySetting (&chainConnectToNeighbour, 0, 0);
        chConnectToPrevious = 1;
    }
    else if ((s == &chainReconnectToFirst) && val)
    {
        modifySetting (&chainConnectToNeighbour, 0, 0);
        modifySetting (&chainConnectToPrevious, 0, 0);
        chConnectToPrevious = 1;

    }
    else if ((s == &chainConnectToNeighbour) && val)
    {

        modifySetting (&chainReconnectToFirst, 0, 0);
        modifySetting (&chainConnectToPrevious, 0, 0);
        chConnectToNeighbour = 1;
    }

    pdescs[chainPtype]->noSize = chConnectToNeighbour;


}

static void chainUnsetType (particle *p)
{
    unlinkAll (p);

    CLEAR_MARKED(p);
    CLEAR_STICKED(p);
}

static void chainDrawStart()
{
    drawing = 1;
    drawingFirst = 1;
}

static void chainDrawStop()
{
    int i, t;
    int pi;
    particle *p;

    if (!tapeOperation())
    {
        if (chConnectToPrevious && 
                getValue (&chainReconnectToFirst) &&
                firstParticle &&
                lastParticle &&
                (lastParticle != firstParticle))
        {
            /* install double-link to first drawn particle */
            linkParticles (firstParticle, lastParticle, 0, DFL_LINK_DATA);
            linkParticles (firstParticle, lastParticle, 0, DFL_LINK_DATA);
        }

        /* close all open links of marked particles
         */
        for (t = 0; t < options->numThreads; t++)
        {
            for (i = 0; i < tData[t]->numParticles; i++)
            {
                pi = tData[t]->particleList[i];

                p = &partArray[pi];

                if (PTYPE(p) != chainPtype)
                    continue;

                if (!getValue (&chainActivate) && IS_MARKED(p))
                {
                    closeOpenLinks (p, 0);
                }
                CLEAR_MARKED(p);
            }
        }
    }

    drawing = 0;
    drawingFirst = 0;
    firstParticle = lastParticle = NULL;
}

static int connectedWith (particle *p, particle *pp)
{
    return isLinked(p, pp);
}


static void findBestPart (screeninfo *pSi, particle *p, int *minLinks, particle **lp)
{
    particle *pp;

    if (pSi->particleList <= SCR_BRICK)
    {
        if ((pSi->particleList == SCR_BRICK) && chConnectToWall)
            SET_STICKED(p);

        return;
    }

    for (pp = pSi->particleList; pp != NULL; pp = pp->next)
    {
        if ((pp == p) || connectedWith(p, pp) || (openLinks(pp, getValue (&chainConnectToPassive)) == 0))
            continue;

        if ((PTYPE(p) == PTYPE(pp)) || (chConnectToAll && (pdescs[PTYPE(pp)]->getRot)))
        {
            *lp = pp;
            return;
        }
    }
}

static int chainSetType(particle *p)
{
    if (drawingFirst)
    {
        firstParticle = p;
    }

    chainApplySetting (p, &actPotential);
    
    p->pPtr = (void*)linkPartners (p);

    drawingFirst = 0;

    unlinkAll (p);

    /* Link operations are not doen when loading from tape so that we don not interact
     * with the link records in the tape
     */
    if (!tapeOperation())
    {
        CLEAR_STICKED(p);

#if 0
        if (wimpType != -1)
        {
            vec_t pp = p->pos;
            pp.d.x += GRID;
            createParticleAsync (&pp, &p->vec, 0, p->mass, wimpType, p->size, p);
        }
#endif


        if (chConnectToPrevious && drawing)
        {
            if (lastParticle)
            {
                /* establish double link to previous particle */
                linkParticles (p, lastParticle, 0, DFL_LINK_DATA);
                linkParticles (p, lastParticle, 0, DFL_LINK_DATA);
            }
        }
        else if (chConnectToNeighbour && drawing)
        {
            screeninfo *pSi = GET_SCREENINFO(p);
            particle *pp = NULL;
            int minLinks = 8;
            int i;
            int sy = SCREENC(p->pos.d.y);

#if (MAX_LINKS == 6)
#if 0
            p->pos.d.y = INTERNC(sy) + GRID/2;
            p->pos.d.x = INTERNC(SCREENC(p->pos.d.x));

            if (sy & 1)
            {
                p->pos.d.x = (p->pos.d.x & ~MAX_SPEED) + GRID/3;
            }
            else
            {
                p->pos.d.x = (p->pos.d.x & ~MAX_SPEED) + ((GRID*2)/3);
            }
#endif

            for (i = 0; i < MAX_LINKS; i++)
            {
                if (openLinks(p, 0) == 0)
                    break;

                pp = NULL;
                minLinks = 3;

                if (sy & 1)
                {
                    findBestPart (SI_DIR(pSi, 0), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 1), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 2), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 3), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 4), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 6), p, &minLinks, &pp);
                }
                else
                {
                    findBestPart (SI_DIR(pSi, 1), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 3), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 4), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 5), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 6), p, &minLinks, &pp);
                    findBestPart (SI_DIR(pSi, 7), p, &minLinks, &pp);
                }

                if (pp != NULL)
                {
                    linkParticles (p, pp, getValue (&chainConnectToPassive), DFL_LINK_DATA);

                    /* mark particle so that we later can  close all remaining links */
                    SET_MARKED(p);
                    SET_MARKED(pp);
                }

            }
#else
            for (i = 0; i < MAX_LINKS; i++)
            {
                if (openLinks(p, 0) == 0)
                    break;

                pp = NULL;
                minLinks = 3;
                findBestPart (SI_DIR(pSi, 1), p, &minLinks, &pp);
                findBestPart (SI_DIR(pSi, 3), p, &minLinks, &pp);
                findBestPart (SI_DIR(pSi, 4), p, &minLinks, &pp);
                findBestPart (SI_DIR(pSi, 6), p, &minLinks, &pp);

                if (pp != NULL)
                {
                    linkParticles (p, pp, getValue (&chainConnectToPassive), DFL_LINK_DATA);

                    /* mark particle so that we later can  close all remaining links */
                    SET_MARKED(p);
                    SET_MARKED(pp);
                }

            }
#endif
        }
        else
        {
            if (!getValue (&chainActivate))
            {
                closeOpenLinks (p, 0);
            }
        }

        lastParticle = p;
    }

    return 1;
}

static void chainApplySetting (particle *p, setting *s)
{
    pLinks *pl, *pl2;
    int i,j;
    int val = getValue (s);

    if (s == &chainActivate)
    {
        if (val)
            activateClosedLinks (p);
        else
            closeOpenLinks (p, 0);
    }
    else if (s == &chainAttraction)
    {
        pl = linkPartners(p);

        for (i = 0; i < pl->num; i++)
        {
            SET_CHAIN_ATTRACT(pl->linkData[i], val);

            pl2 = linkPartners(pl->links[i]);

            for (j = 0; j < pl2->num; j++)
            {
                if (pl2->links[j] == p)
                {
                    SET_CHAIN_ATTRACT(pl2->linkData[j], val);
                }
            }
        }
    }
    else if (s == &chainMaxDist)
    {
        pl = linkPartners(p);

        for (i = 0; i < pl->num; i++)
        {
            SET_MAX_DIST(pl->linkData[i], CURRENT_MAX_DIST);

            pl2 = linkPartners(pl->links[i]);

            for (j = 0; j < pl2->num; j++)
            {
                if (pl2->links[j] == p)
                {
                    SET_MAX_DIST(pl2->linkData[j], CURRENT_MAX_DIST);
                }
            }
        }
    }
    else if (s == &numLinks)
    {
        linkSetCount (p, getValue(s));
    }
    else if (s == &repulseChained[chainPtype])
    {
        pl = linkPartners(p);

        for (i = 0; i < pl->num; i++)
        {
            if (pIsMarked (p) && !pIsMarked(pl->links[i]))
                continue;

            pl2 = linkPartners(pl->links[i]);

            SET_REPEL(pl->linkData[i], getValue(s));

            for (j = 0; j < pl2->num; j++)
            {
                if (pl2->links[j] == p)
                {
                    SET_REPEL(pl2->linkData[j], getValue(s));
                }
            }
        }
    }
    else if (s == &actPotential)
    {
        int v = getValue(s);
        if (v == s->maxValue)
        {
            CLEAR_ACTPOT(p);
        }
        else
        {
            particle2 *p2 = &partArray2[PARTICLE_ID(p)];
            SET_ACTPOT(p);
            
            float fv = ((float)v) / 100.0f;
            
            p2->actPot.c[0] = fv;
            p2->actPot.c[1] = fv;
            p2->actPot.c[2] = fv;
            p2->actPot.c[3] = 1000.0f;
        }
        
    }
}

static int filterChainCanConnect (particle *p)
{
    if (p == connectP1)
        return 1;

    if (PTYPE(p) != chainPtype)
    {
        if (chConnectToAll && (pdescs[PTYPE(p)]->getRot))
            return 0;

        return 1;  
    }

    if (availLinks (p) == 0)
        return 1;

    return 0;
}

static int filterChainCanDisconnect (particle *p)
{
    if (connectP1 == NULL)
    {
        if (hasLink (p))
        {
            return 0;
        }
    }
    else
    {
        if (linkedTogether (connectP1, p))
        {
            return 0;
        }
    }
    
    return 1;
}


static int chainCanCreate (int x, int y)
{
    screeninfo *pSi;
    particle *p;

    if (getValue (&connectTwo))
    {
        particle *p = nearest;

        if (p == NULL)
            return 0;

        switch (connectPhase)
        {
            case 2:
                    connectP1 = p;
                if (nearest)
                {
                    leaveParticle (nearest, (FUNCPTR)chainFollow, NULL);
                    nearest = NULL;
                }
                followParticle (p, (FUNCPTR)chainFollow, NULL);
                escConnect ((FUNCPTR)connectTwoReset);
                
                doDisconnect = disconnecting;
                break;
            case 1:
                    connectP2 = p;
                break;
            default:
                    connectPhase = 0;
                return 0;
        }
        connectPhase--;

        if (connectPhase == 0)
        {
            if (connectP1 != connectP2)
            {
                if (doDisconnect)
                {
                    unlinkTwo (connectP1, connectP2);
                }
                else
                {
                    if (openLinks(connectP1, 0) == 0)
                        activeNClosedLinks (connectP1, 1);

                    if (openLinks(connectP2, 0) == 0)
                        activeNClosedLinks (connectP2, 1);

                    if (PTYPE(connectP1) == PTYPE(connectP2))
                    {
                        linkParticles (connectP1, connectP2, 1, DFL_LINK_DATA);
                    }
                    else
                    {
                        linkParticles (connectP1, connectP2, 1, DFL_LINK_DATA);
                    }
                }
            }

            connectTwoReset();
            if (shiftPressed())
            {
                connectP1 = p;
                connectPhase = 1;
                followParticle (p, (FUNCPTR)chainFollow, NULL);
                escConnect ((FUNCPTR)connectTwoReset);
            }
        }

        return 0;
    }

    /* if we are in "connect to other particles" mode, do not create chain particles
     * on pixels with other particles
     */
    if (!chConnectToAll)
        return 1;

    pSi = &siArray[SCREENC(x) + SCREENC(y)*wid];

    for (p = pSi->particleList; p > SCR_BRICK; p = p->next)
        if (PTYPE(p) !=  chainPtype)
            return 0;
    
    return 1;
}

static int chainPlacementHint(void)
{
    if (chConnectToNeighbour)
        return PHINT_FIXED1;
    else
        return PHINT_RANDOM;
}

static void chainWalk (particle *p, FUNCPTR cb, void *arg)
{
    walkLinks (p, cb, arg);
}

static void chainIter(int scrUpdate)
{
    if (scrUpdate)
    {
        ldToggle = ldToggle ^ LINK_MARKER;
    }
}

static screeninfo *chainRedraw (particle *p, screeninfo *oldSi, threadData *td, int off )
{
    pLinks *pl = (pLinks*)p->pPtr;
    particle *pp;
    int i;

    td->v.partCount++;

    if (!GETVAL32(showLinks))
        return oldSi;

    for (i = 0; i < pl->num; i++)
    {
        pp = pl->links[i];

#ifndef USE_OPENGL
        mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y),
                             (IX_TO_SX(pp->pos.d.x) + IX_TO_SX(p->pos.d.x)) / 2, 
                             (IY_TO_SY(pp->pos.d.y) + IY_TO_SY(p->pos.d.y)) / 2,
                             ICOLOR(p).col32);
#else
        if (!LINKS_DRAWN(pp))
        {
            ADD_LINK_LINE (p->pos.d.x, p->pos.d.y, pp->pos.d.x, pp->pos.d.y, p->color.cv, pp->color.cv);
        }
#endif
    }
#ifdef USE_OPENGL
    LINK_MARK(p);
#endif

    return oldSi;
}

static int getNumLinks (particle *p)
{
    return getValue(&numLinks);
}

void connectTwoReset ()
{
    connectPhase = 2;
    if (connectP1)
        leaveParticle (connectP1, (FUNCPTR)chainFollow, NULL);
    if (nearest)
        leaveParticle (nearest, (FUNCPTR)chainFollow, NULL);

    connectP1 = NULL;
    connectP2 = NULL;
    nearest = NULL;

    polyHide (m1);
    polyHide (m2);
    polyHide (conn);
    
    doDisconnect = 0;
    
    escConnect (NULL);
}

void connectTwoCb ()
{
    if (getValue (&connectTwo))
    {
        modifySetting (&chainConnectToNeighbour, 0, 0);
        calloutSched (searchCallout, 100, (FUNCPTR)searchNearestAndFollow, NULL);
    }

    setDrawMode (DR_STAMP);

    connectTwoReset();
}

static void chainFollow (particle *p, void *arg)
{
    if (p == connectP1)
    {
        polyMove (m1, p->pos.d.x, p->pos.d.y);
        polyShow (m1);

        polyChangeCoord (conn, 0, p->pos.d.x, p->pos.d.y, ARGB(0xc0f08010));
        if (nearest)
        {
            polyChangeCoord (conn, 1, nearest->pos.d.x, nearest->pos.d.y, ARGB(0xc0f08010));
        }
        else
        {
            polyChangeCoord (conn, 1, iPositionLast.d.x, iPositionLast.d.y , ARGB(0xc0208010));
        }
        polyShow (conn);
    }
    else
    {
        polyMove (m2, p->pos.d.x, p->pos.d.y);
        polyShow (m2);
    }

}

static void searchNearestAndFollow (void *arg)
{
    particle *p;
    
    if (getValue(&connectTwo) == 0)
        return;
    
    if (ctrlPressed() || doDisconnect)
    {
        p = nearestParticleSt (iPositionLast.d.x, iPositionLast.d.y, 
                                (FUNCPTR)filterChainCanDisconnect);
        disconnecting = 1;
    }
    else
    {
        p = nearestParticleSt (iPositionLast.d.x, iPositionLast.d.y, 
                                (FUNCPTR)filterChainCanConnect);
        disconnecting = 0;
    }

    if (!p)
    {
        if (nearest)
        {
            leaveParticle (nearest, (FUNCPTR)chainFollow, NULL);
        }
        nearest = NULL;
        polyHide (m2);
        polyHide (conn);
    }
    else
    {
        if ((p != nearest) && (p != connectP1))
        {
            if (nearest)
                leaveParticle (nearest, (FUNCPTR)chainFollow, NULL);
            nearest = p;
            followParticle (p, (FUNCPTR)chainFollow, NULL);
        }
    }
    calloutSched (searchCallout, 100, (FUNCPTR)searchNearestAndFollow, NULL);
}



static void chainModuleActivate (int activate)
{
    modifySetting (&connectTwo, 0, 0);

    if (activate && getValue (&connectTwo))
    {
        calloutSched (searchCallout, 100, (FUNCPTR)searchNearestAndFollow, NULL);
    }
    else
    {
        calloutUnsched (searchCallout);
    }
}

pdesc MOD = {
    .name = "Chain",
    .descr = "",
    .init = (FUNCPTR)chainInit,
    .unsetType = (FUNCPTR)chainUnsetType,
    .startDraw = (FUNCPTR)chainDrawStart,
    .stopDraw = (FUNCPTR)chainDrawStop,
    .setType = (FUNCPTR)chainSetType,
    .applySetting = (FUNCPTR)chainApplySetting,
    .redraw = (FUNCPTR)chainRedraw,
    .activate = (FUNCPTR)chainModuleActivate,
    .help = "chain",
    .pm = chain_pm,
    .canCreate = (FUNCPTR)chainCanCreate,
    .placementHint = (FUNCPTR)chainPlacementHint,
    .walkSiblings = (FUNCPTR)chainWalk,
    .iteration = (FUNCPTR)chainIter,
    .iterationFrozen = (FUNCPTR)chainIter,
    .numLinks = (FUNCPTR)getNumLinks,
    .isWimp = 0,
    .noSize = 1,

    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, psize_pm, "Particle Size", 0, &particleSize[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},

            {(FUNCPTR)onOffButtonS, 32, 32, connect_wimp_xpm, "Pair_with_WIMP", 0, &wimpPair[0], B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, chain_attract2_pm , "Chain Attraction", 0, &chainAttraction, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, chain_maxdist_pm, "Max. Distance", 0, &chainMaxDist, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, NULL, "Action Potential", 0, &actPotential, B_NEXTRC },
//        {(FUNCPTR)onOffButtonS, 32, 32, connect_pm, "Draw connected", 0, &chainConnectToPrevious, B_NEXTRC },
//        {(FUNCPTR)onOffButtonS, 32, 32, connect_ring_pm, "Reconnect to first", 0, &chainReconnectToFirst, B_NEXTRC },
        {(FUNCPTR)onOffButtonS, 32, 32, connect_solid_pm, "Solid", 0, &chainConnectToNeighbour, B_NEXTRC },
        {(FUNCPTR)onOffButtonS, 32, 32, chain_activate_pm, "Activate/deactivate links", 0, &chainActivate, B_NEXT },
        {(FUNCPTR)onOffButton, 32, 32, connect_blob_xpm, "Connect to blob", (intptr_t)&chConnectToAll, NULL, B_NEXTRC },
        {(FUNCPTR)onOffButton, 32, 32, connect_wall_xpm, "Connect to wall", (intptr_t)&chConnectToWall, NULL, B_NEXT },
        {(FUNCPTR)onOffButtonS, 32, 32, repulse_xpm, "Attract/Repulse", 0, &repulseChained[0], B_NEXTRC },
        {(FUNCPTR)onOffButtonS, 32, 32, connect_two_xpm, "ConnectTwo", 0, &connectTwo, B_NEXT },
        {(FUNCPTR)onOffGroupS, 22, 21, l1_xpm, "1 Link", 1, &numLinks, B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 22, 21, l3_xpm, "3 Links", 3, &numLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l5_xpm, "5 Links", 5, &numLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l2_xpm, "2 Links", 2, &numLinks, B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l4_xpm, "4 Links", 4, &numLinks, B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 22, 21, l6_xpm, "6 Links", 6, &numLinks, B_NEXT|B_SPC(1) },
#ifdef CONNECT_PASSVE_LINKS
        {(FUNCPTR)onOffButtonS, 32, 32, chain_fconnect_pm, "Connect to inactive link", 0, &chainConnectToPassive, B_NEXTRC },
#endif
        { NULL }
        }
};

static void chainInit (int ptype)
{
    chainPtype = ptype;

    wimpType = pdescFind ("Wimp");

#ifdef USE_AVX
    if (have_avx)
    {
        MOD.moveParticle = move_chain_avx;
    }
    else
#endif
#ifdef USE_SSE41
    if (have_sse41)
    {
        MOD.moveParticle = move_chain_sse41;
    }
    else
#endif
#ifdef USE_SSE2
    if (have_sse2)
    {
        MOD.moveParticle = move_chain_sse2;
    }
    else
#endif
        MOD.moveParticle = move_chain_c;

    linkInit();

    searchCallout = calloutNew ();

    m1 = scrBox (0, 0, GRID, COL_BLUE);
    polyHide (m1);
    m2 = scrBox (0, 0, GRID, COL_BLUE);
    polyHide (m2);
    conn = scrLine (0, 0, 0, 0, COL_RED);
    polyHide (conn);
}
