
/*
Copyright (C) 2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


/* Generic move algorithm for gas and chain particles with more or less exact behaviour
 *
 *
 * Implemented for generic C (no SSE), SSE2, SSE4.1
 *
 * AVX is experimental, not yet functional and too slow anyway (probably due to lacking
 * integer instructions which are supposed to be available with AVX2).
 */

#include "plansch.h"

#ifdef _AVX
#include <immintrin.h>

typedef __m256i v8si;
typedef __m256 v8sf;
#endif

#ifdef _SSE41
#include <smmintrin.h>

#endif




#ifdef _GAS

#define WALL_EXACT

#endif

#ifdef _CHAIN
# include "chain.h"
#endif

#if defined(_SSE2) || defined(_SSE41) || defined (_AVX)
#define _SSE
#endif

/* stolen from sse_mathfun.h .. */

# define ALIGN16_BEG
# define ALIGN16_END __attribute__((aligned(16)))
# define ALIGN32_BEG
# define ALIGN32_END __attribute__((aligned(32)))

#define PRINT_V4SF(name,val)        \
{                                \
    float *_fa = (float*)&(val);        \
    printf ("%s: %.2f %.2f %.2f %.2f\n", name, _fa[0], _fa[1], _fa[2], _fa[3]);\
}

#define PRINT_V4I(name,val)        \
{                                \
    int *_ia = (int*)&(val);        \
    printf ("%s: %08x %08x %08x %08x\n", name, _ia[0], _ia[1], _ia[2], _ia[3]);\
}

#define PRINT_V4IX(name,val)        \
{                                \
    int *_ia = (int*)&(val);        \
    printf ("%s: %08x %08x %08x %08x\n", name, _ia[0], _ia[1], _ia[2], _ia[3]);\
}

#if 1
#undef PRINT_V4SF
#define PRINT_V4SF

#undef PRINT_V4I
#define PRINT_V4I
#endif

/* #define SET0(v)        v = _mm_xor_si128 (v, v); */
#define SET0(v) v = _mm_setzero_si128();

#ifdef USE_OPENGL
static float _ps_red[4] __attribute__((aligned(16))) = { 1.0, 0, 0, 0 };
#endif

static int modinit = 0;

screeninfo *
FCT (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *pa2 )
{
    vec_t newPos;
    vec_t screenPos;
    vec_t normVec;
    particle *pp, *tpp;
    screeninfo *newSi;
    int cx, cy;
    int dir;
    FLOAT vec_len;
    vec_t tvec;
#define DTYPE float
#define SDTYPE float
    DTYPE spq;
    DTYPE minSpq;
#ifndef _CHAIN
    DTYPE minPpDistQ;
#endif
    unsigned int pRad, ppRad;

    screeninfo *tmpSi;

if (modinit == 0)
    printf ("%s\n", __FUNCTION__);
modinit = 1;

#ifdef _CHAIN
    vec_t ttvec;
    pLinks *pl = (pLinks*)p->pPtr;
    int linked;
#endif
    int lhit;
#ifdef CARNIVORE
    int eat = 0;
#endif

    PART_GRAV(p,oldSi);


#ifdef _CHAIN

    int i;
    for (i = 0; i < pl->num; i++)
    {
        uint32 linkData = pl->linkData[i];
        int maxDist = MAX_DIST(linkData);
        int attract = CHAIN_ATTRACT(linkData);

        pp = pl->links[i];
        ttvec.d.x = (pp->pos.d.x - p->pos.d.x);
        ttvec.d.y = (pp->pos.d.y - p->pos.d.y);
        spq = (int64_t)ttvec.d.x * (int64_t)ttvec.d.x + (int64_t)ttvec.d.y * (int64_t)ttvec.d.y;

        if (HAS_ACTPOT(p))
        {
            /* check if the color channel difference between two particles is
             * higher than the threshold.
             * If yes, color channel value(s) is/are passed to particle with lower value.
             *
             * Based on the setting of the global color restore rate the particle will
             * slowly regain it's original color
             */
#ifndef USE_SSE2
            int _i;

            for (_i = 0; _i < 3; _i++)
            {
                float d = p->color.c[_i] - pp->color.c[_i];
                if (d > pa2->actPot.c[_i])
                {
                    pp->color.c[_i] = p->color.c[_i];
                }
                else if (d < -pa2->actPot.c[_i])
                {
                    p->color.c[_i] = pp->color.c[_i];
                }
            }
#else
            v4sf diff;
            v4sf isgt;
            diff = _mm_sub_ps (p->color.cv, pp->color.cv);
            isgt = _mm_cmpgt_ps (diff, pa2->actPot.cv);
            pp->color.cv = _mm_or_ps (_mm_and_ps(isgt, p->color.cv),
                                      _mm_andnot_ps(isgt, pp->color.cv));

            diff = _mm_sub_ps (pp->color.cv, p->color.cv);
            isgt = _mm_cmpgt_ps (diff, pa2->actPot.cv);
            p->color.cv = _mm_or_ps (_mm_and_ps(isgt, pp->color.cv),
                                     _mm_andnot_ps(isgt, p->color.cv));
#endif
        }   

#ifdef CARNIVORE
        eat += pl->give[i];


        pl->give[i] = 0;

        if (PTYPE(pp) == PTYPE(p))
        {
            vec_t av;

//printf ("%d %d\n", p->food, pp->food);

            if ((p->food - pp->food) > FOOD_TRANSFER_THRESH)
            {
                pLinks *otherPl = (pLinks*)pp->pPtr;
                int otherIdx = pl->otherIdx[i];

                /* give the other one some food */
                otherPl->give[otherIdx] = FOOD_TRANSFER;

                /* transferring food costs some energy ..
                */
                p->food -= FOOD_TRANSFER + TRANSFER_LOSS;

//printf ("particle %d has given %d food to %d (has now %d left)\n", PARTICLE_ID(p), 5, PARTICLE_ID(pp), p->food);

                /* becasue we also attract him a little bit in the hot that this 
                 * accellerates us into the direction of the food source
                 */
                av.d.x = ((float)ttvec.d.x / sqrt(spq)) * 10.f;
                av.d.y = ((float)ttvec.d.y / sqrt(spq)) * 10.f;
                pdescs[PTYPE(pp)]->hit (pp, (int)-av.d.x, (int)-av.d.y);
            }
        }

#endif

#if 0
        if (GETVAL32(showLinks) && (zoom > 2))
        {
            mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y), IX_TO_SX(pp->pos.d.x), IY_TO_SY(pp->pos.d.y), PCOLOR(p));
        }
#endif


        if (maxDist && (spq > ((int64_t)maxDist * (MAX_SPEEDQ / 10))))
        {
            /* distance too large, unlink particles
             */
            unlinkTwo (p, pp);

#if !defined( USE_OPENGL) || !defined(_SSE)
            if (GETVAL32(showLinks) && (zoom > 2))
            {
                mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y), IX_TO_SX(pp->pos.d.x), IY_TO_SY(pp->pos.d.y), 0x00ff0000);
            }
#else
            if (GETVAL32(showLinks))
            {
                /* temporary color particles red */
                p->color.cv = *(v4sf*)_ps_red;
                pp->color.cv = *(v4sf*)_ps_red;

                if (!LINKS_DRAWN(pp))
                {
                    ADD_LINK_LINE_V (p->pos, pp->pos, p->color.cv, pp->color.cv);
                }
            }
#endif

        }
        //else if (spq >= MAX_SPEEDQ)
        else if (spq >= (PART_RADIUSQ(p) + PART_RADIUSQ(pp))
                         || (pdescs[PTYPE(pp)]->isWimp)
                        )
        {
            /* add the distance of the two particles to the speed vector.
             */
#if 0
            /* subtracting the minimum distance from the attraction vector makes
             * linked particles behave "better" if very near, but these are
             * two additional sqrt()s ...
             */
            ttvec.d.x -= ((float)ttvec.d.x / sqrt(spq)) * (MAX_SPEEDF);
            ttvec.d.y -= ((float)ttvec.d.y / sqrt(spq)) * (MAX_SPEEDF);
#endif
            if (REPEL(linkData))
            {
                    int dd = -(MAX_SPEEDQ / (spq / 4));

                    ttvec.d.x *= dd;
                    ttvec.d.y *= dd;
            }

            ttvec.d.x /= attract;
            ttvec.d.y /= attract;

#if 0
            {
                int d = spq / (GRID*1000);
                if (d)
                {
                    ttvec.d.x = -(ttvec.d.x / d);
                    ttvec.d.y = -(ttvec.d.y / d); 
                }
                else
                {
                    ttvec.d.x = ttvec.d.y = 0;

                }
            }
#endif

            p->vec.d.x = p->vec.d.x + ttvec.d.x;
            p->vec.d.y = p->vec.d.y + ttvec.d.y;

            pdescs[PTYPE(pp)]->hit (pp, (int)-ttvec.d.x, (int)-ttvec.d.y);

#ifndef USE_OPENGL
            if (GETVAL32(showLinks) && ((zoom > 2) || (spq >= MAX_SPEEDQ*8)))
            {
                mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y),
                                     (IX_TO_SX(pp->pos.d.x) + IX_TO_SX(p->pos.d.x)) / 2, 
                                     (IY_TO_SY(pp->pos.d.y) +
                                     IY_TO_SY(p->pos.d.y)) / 2,
                                     ICOLOR(p).col32);
            }
#else
            if (GETVAL32(showLinks) /* && (spq >= MAX_SPEEDQ*2) */)
            {
                if (!LINKS_DRAWN(pp))
                {
                    ADD_LINK_LINE_V (p->pos, pp->pos, p->color.cv, pp->color.cv);
                }
            }
#endif
        }
#ifndef USE_OPENGL
        else if (GETVAL32(showLinks) && (zoom > 4))
        {

            mylineColor (screen, IX_TO_SX(p->pos.d.x), IY_TO_SY(p->pos.d.y),
                                     (IX_TO_SX(pp->pos.d.x) + IX_TO_SX(p->pos.d.x)) / 2, 
                                     (IY_TO_SY(pp->pos.d.y) +
                                     IY_TO_SY(p->pos.d.y)) / 2, ICOLOR(p).col32);
        }
#else
        else if (GETVAL32(showLinks))
        {
            if (!LINKS_DRAWN(pp))
            {
                ADD_LINK_LINE_V (p->pos, pp->pos, p->color.cv, pp->color.cv);
            }
        }
#endif
    }

#if 0
    if (HAS_ACTPOT(p))
    {
            v4sf div = _mm_set1_ps ((float)pl->num);
            v4sf diff;
            v4sf isgt;

            sum = _mm_div_ps (sum, div);

            diff = _mm_sub_ps (sum, p->color.cv);
            isgt = _mm_cmpgt_ps (diff, pa2->actPot.cv);
            p->color.cv = _mm_or_ps(_mm_and_ps(isgt, sum),
                                      _mm_andnot_ps(isgt, p->color.cv));
    }
#endif

    LINK_MARK(p);

#ifdef CARNIVORE
    /* unlikely that we exceed MAX_FOOD ...
     */
    p->food = (p->food + eat);

    p->color.c[1] = (float)p->food / (float)MAX_FOOD;
    p->color.c[1] = p->color.c[1] * 255.f;

//if (eat) printf ("particle %d has eaten %d (now has %d food)\n", PARTICLE_ID(p), eat, p->food);
#endif

    if (oldSi->flags & SI_LINE)
    {
        lhit = pHitLine (p, &p->vec, off, td, oldSi);
    }
    else
    {
        lhit = 0;
    }


    if (IS_STICKED(p))
    {
        int d;

        /* particle is sticked to wall. Dont move it
         * XXX transfer speed to wall temperature ...
         */
        p->vec.d.x = 0;
        p->vec.d.y = 0;

        /* check if there is still an adjacent wall. If not, unstick it
        */
        for (d = 0; d < 8; d++)
        {
            if (((SI_DIR(oldSi, d)->particleList == SCR_BRICK) ||
                (SI_DIR(oldSi, d)->flags & SI_LINE)))
                break;
        }

        if (d >= 8)
        {
            CLEAR_STICKED(p);
        }

        tvec = p->vec;

    }
    else
#endif /* _CHAIN */
    {
        if (oldSi->flags & SI_LINE)
        {
            lhit = pHitLine (p, &p->vec, off, td, oldSi);
        }
        else
        {
            lhit = 0;
        }

        tvec = p->vec;

        spq = (int64_t)tvec.d.x * (int64_t)tvec.d.x + (int64_t)tvec.d.y * (int64_t)tvec.d.y;
        if (spq >= MAX_SPEEDQ)
        {
            vec_len = sqrt(spq);
            tvec.d.x = (((FLOAT)tvec.d.x * MAX_SPEEDF)) / vec_len;
            tvec.d.y = (((FLOAT)tvec.d.y * MAX_SPEEDF)) / vec_len;
        }
    }

    V_ADD(newPos, p->pos, tvec);
    V_TO_SCREEN(screenPos, newPos);

    td->v.partCount++;
    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

    if (newSi->particleList == SCR_BRICK)
    {
        FLOAT cxf, cyf, vpf;
        vec_t screenPosOld;
        vec_t screenPosDiff;

        V_TO_SCREEN(screenPosOld, p->pos);
        V_SUB(screenPosDiff, screenPos, screenPosOld);
        dir = QUAD2DIR((screenPosDiff.d.x + 1) * 3 + screenPosDiff.d.y + 1);

        td->v.wallHit++;

        td->drawLinkLine = 1;
        
        /* wall hit */

#ifdef _CHAIN
        if (openLinksActive(p) && chConnectToWall)
        {
            SET_STICKED(p);

            return oldSi;
        }
#endif        
                
        /* get normal vector of current screen position (direction based) 
         *
         * This is fixed-point arithmetic, except vector length calculation.
         * Normals are stored in NORM_LEN units (+-127).
         *
         */
        /* normal vector in normVec */
        normVec.d.x = oldSi->normals[dir * 2];
        normVec.d.y = oldSi->normals[dir * 2 + 1];
        cxf = tvec.d.x;
        cyf = tvec.d.y;

        /* normalize */
        vec_len = VEC_LEN (cxf, cyf);

        cxf = (cxf * NORM_LEN) / vec_len;
        cyf = (cyf * NORM_LEN) / vec_len;


        if (FRICTION(newSi))
        {
            float vl;
            normVec.d.x = (normVec.d.x * (100 - FRICTION(newSi)) - cxf * FRICTION(newSi));
            normVec.d.y = (normVec.d.y * (100 - FRICTION(newSi)) - cyf * FRICTION(newSi));
            vl = VEC_LEN(normVec.d.x, normVec.d.y);
            normVec.d.x = (normVec.d.x * NORM_LEN) / vl;
            normVec.d.y = (normVec.d.y * NORM_LEN) / vl;

        }

        /* reflect at normal vector */
        /*        reflect (&cx, &cy, normVec.d.x, normVec.d.y);*/
        vpf = ((cxf * normVec.d.x) + (cyf * normVec.d.y));
        cxf = cxf - (2 * normVec.d.x * vpf) / (NORM_LEN*NORM_LEN);
        cyf = cyf - (2 * normVec.d.y * vpf) / (NORM_LEN*NORM_LEN);

        /* handle energy transfer between particle and wall.  */

        if (GETVAL32(wallTempr))
        {
            int t = (FLOAT)((((vec_len * p->mass) / MAX_MASS) - TEMPR(newSi))/2) * wallDamping;

            TEMPR(newSi) += t;

            if (t > 0.0)
            {
                vec_len -= (t * MAX_MASS) / p->mass;
                if (vec_len < 0)
                    vec_len = 0;

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));
            }
            else
            {
                vec_t tmpvec;

                frandomize (cxf, cyf, (-t * MAX_MASS) / p->mass, &tmpvec);

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

                p->vec.d.x += tmpvec.d.x;
                p->vec.d.y += tmpvec.d.y;

            }

            /* Make sure perticle does not get too hot */
            spq = (QUAD_TYPE)p->vec.d.x * (QUAD_TYPE)p->vec.d.x + (QUAD_TYPE)p->vec.d.y * (QUAD_TYPE)p->vec.d.y;
            if (spq >= MAX_SPEEDQ)
            {
                vec_len = sqrt(spq);
                p->vec.d.x = (((FLOAT)p->vec.d.x * MAX_SPEEDF)) / vec_len;
                p->vec.d.y = (((FLOAT)p->vec.d.y * MAX_SPEEDF)) / vec_len;
            }

            adjustParticleColor (p, (float*)_ps_red, (FLOAT)TEMPR(newSi) / (FLOAT)MAX_TEMP); 
        }
        else
        {
            /* apply original impuls to reflection vector */

            p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf * wallDamping) / (FLOAT)(NORM_LEN));
            p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf * wallDamping) / (FLOAT)(NORM_LEN));
        }
        
        /* Check pixel in new direction. */

        newPos.d.x = p->pos.d.x + p->vec.d.x;
        newPos.d.y = p->pos.d.y + p->vec.d.y;
        V_TO_SCREEN(screenPos, newPos);
        if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
        {
#ifdef WALL_EXACT
            /* pixel in new direction is blocked. May happen if hitting
             * a wall pixel at a slope.
             * Set direction equal to normal vector, retaining the particle
             * speed. There shouldn't be a pixel in this direction.
             * This is not ideal for slope collisions, but prevents
             * paricle warping.
             */
            vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

            p->vec.d.x = (normVec.d.x * vec_len) / NORM_LEN;
            p->vec.d.y = (normVec.d.y * vec_len) / NORM_LEN;
            newPos = p->pos;
            V_TO_SCREEN(screenPos, newPos);
#else
            /* warp particle to adjacent free pixel or stay put. Makes collisions
             * at slopes more realistic, but warping creates unrealistic motion effects
             * if there is a particle at the destination pixel.
             */
            newPos.d.x += (normVec.d.x * GRID) / (NORM_LEN);
            newPos.d.y += (normVec.d.y * GRID) / (NORM_LEN);
            V_TO_SCREEN(screenPos, newPos);

            if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
            {
                p->vec.d.x = -p->vec.d.x;
                p->vec.d.y = -p->vec.d.y;
                newPos = p->pos;
                V_TO_SCREEN(screenPos, newPos);
            }
#endif
        }

        newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];
        goto move;
        
    }

    if (lhit)
    {

        if (newSi != oldSi)
            goto move;
        
        p->pos = newPos;

        return oldSi;
    }

    pRad = PART_RADIUS(p);
    pp = NULL;

#ifndef _SSE
    {
        int        px, py;
        int        i;

        minSpq = 0xffffffffffffffffULL;
        px = py = 0;

        tmpSi = oldSi;
        for (i = -1; i < 8;)
        {
            if (tmpSi->particleList <= SCR_BRICK)
            {
                i++;
                tmpSi = SI_DIR(oldSi, i);
                continue;
            }
            
            for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
            {
                uint64_t ppDistQ;

                if (tpp == p)
                    continue;

#if 0
                if ((IS_LINKED(p) && (LINKED_PARTICLE(p) == tpp)))
                    continue;

                if (PTYPE(tpp) == PTYPE(p))
                {
                    if (IS_LINKED(p) && IS_LINKED(tpp) && ROT_DIR_EQUAL(p,tpp))
                    {
                        continue;
                    }
                }
#endif

                ppRad = PART_RADIUS(tpp);

                ppDistQ = (pRad + ppRad) * (pRad + ppRad);

                cx = tpp->pos.d.x - newPos.d.x;
                cy = tpp->pos.d.y - newPos.d.y;

                spq = (uint64_t)cx * (uint64_t)cx + (uint64_t )cy * (uint64_t)cy;

                if (spq < ppDistQ)
                {
                    if (spq < minSpq)
                    {
                        minSpq = spq;
#if 0
                        minPpDistQ = ppDistQ;
#endif
                        pp = tpp;
                    }
                }
#if 0
                else if (particleAttraction)
                {
                    px += cx;
                    py += cy;
                }
#endif
            }
            i++;
            tmpSi = SI_DIR(oldSi, i);
        }

        p->vec.d.x += px/5000;
        p->vec.d.y += py/5000;

        if (pp == NULL)
        {
            if (newSi != oldSi)
                goto move;
            
            p->pos = newPos;

            return oldSi;
        }
    }
#else
    {
        /* SSE Implementation for finding nearest particle */

        int        pi;
        int        idx = 0;
#ifndef _AVX
        v4si    px, py;
        v4si    iDx, iDy;
        v4si    ppx;
        v4sf    rad4;
        v4sf    fDxQ, fDy;
        v4sf    fppRad4Q;
        v4sf    tmp, tmp2;
        v4sf    minDistQ;        /* current minimum distance */
        v4si    currentP;        /* particle vectors associated with mindist */
#else
        v8si    ppx;
        v8sf        px, py, rad4;
        v8sf    fDxQ, fDy, fDx;
        v8sf    fppRad4Q;
        v8sf    tmp, tmp2;
        v8sf    minDistQ;        /* current minimum distance */
        v8si    currentP;        /* particle vectors associated with mindist */
#endif
#ifdef _SSE2
        float   darr[4] ALIGN16_END;
        unsigned int parr[4] ALIGN16_END;
#endif
#ifdef _AVX
        float   darr[8] ALIGN32_END;
        unsigned int parr[8] ALIGN32_END;
#endif

        /* this particles attributes:
         * - Position
         * - radius^2
         */
#ifndef _AVX
        px = _mm_set1_epi32 (newPos.d.x);
        py = _mm_set1_epi32 (newPos.d.y);
        rad4 = _mm_cvtepi32_ps (_mm_set1_epi32 (pRad));

        /* start values
         * currentP contains the 4 particle descriptors associated to
         * the current 4 particles being calculated. 
         */
// #pragma GCC diagnostic ignored "-Wuninitialized"
        SET0(currentP);
        minDistQ = _mm_set1_ps (MAX_SPEEDQF);
#else
        px = _mm256_set1_ps ((float)newPos.d.x);
        py = _mm256_set1_ps ((float)newPos.d.y);
        rad4 = _mm256_set1_ps ((float)pRad);

        /* start values
         * currentP contains the 4 particle descriptors associated to
         * the current 4 particles being calculated. 
         */
        currentP = _mm256_setzero_si256();
        minDistQ = _mm256_set1_ps (MAX_SPEEDQF);
#endif

        /* calcluate the nearest neightbour particle
         * step1 collects all particles on the neighbouring 8 pixels into 4 arrays (x, y, radius and particle descriptor)
         * step2 reduces these arrays to 2 sse2 double-quad words: minDistQ with the 4 minimum distances^2, currentP
         * with the particle desctiptors associated with the 4 minimum distances (or 0).
         * step3 takes the particle with the smalles distance out of the rmaining four.
         *
         * step 3 is not very elegant, and could be completely eliminitated if further processing also uses sse math.
         * this would have the benefit that we could calculate collission with four particles simultanously, not just one.
         *
         * Another possibility would be to do step 1 for all pariticles and offload step 2 to the GPU ..
         */

        {
#ifdef _AVX
            v8si xa_v;
            v8si ya_v;
            v8si pa_v;
            v8si rada_v;
            v8si *xa_p = (__m256i*)td->xa_arr;
            v8si *ya_p = (__m256i*)td->ya_arr;
            v8si *pa_p = (__m256i*)td->pa_arr;
            v8si *rada_p = (__m256i*)td->rada_arr;
#else
            v4si xa_v;
            v4si ya_v;
            v4si pa_v = _mm_set1_epi32 (0);
            v4si rada_v;
            v4si *xa_p = (__m128i*)td->xa_arr;
            v4si *ya_p = (__m128i*)td->ya_arr;
            v4si *pa_p = (__m128i*)td->pa_arr;
            v4si *rada_p = (__m128i*)td->rada_arr;
#endif
            idx = 0;

            /* 8 neighbour pixels
             */
            tmpSi = oldSi;
            dir = -1;

            /* used to be a for loop, but as of gcc 4.8.2 with -O3 it doesn't
             * work any more ..
             */
            while (1)
            {
                for (tpp = tmpSi->particleList; tpp > SCR_BRICK; tpp = tpp->next)
                {
                    if (tpp == p)
                        continue;

                    td->xa_arr[idx] = tpp->pos.d.x;
                    td->ya_arr[idx] = tpp->pos.d.y;
                    td->pa_arr[idx] = PARTICLE_ID(tpp);
#ifdef _AVX
                    td->rada_arr[idx] = tpp->size << RADIUS_SHIFT;
#else
                    td->rada_arr[idx] = tpp->size;
#endif
                    idx++;

                    if (idx >= PART_COLL_MAX)
                        goto labort;
                }
                dir++;
                if (dir >= 8)
                    break;
                tmpSi = SI_DIR(oldSi,dir);
            }

            if (idx == 0)
            {
                if (newSi != oldSi)
                    goto move;
                
                p->pos = newPos;

                return oldSi;
            }

            /* invalidate unused particle attributes
             */
            for (;(idx & 3) != 0; idx++)
                td->pa_arr[idx] = 0;

labort:

#if 1
            /* count average number of neighbour particles 
             */
            td->v.gas_nb_total += idx;
            td->v.gas_nb_ccount ++;
#endif

            /* Now calculate the 4 particles with the smallest distance
             * in parallel (4 particles in each iteration)
             */
            for (; idx > 0; idx -= 4)
            {
                xa_v = *xa_p++;
                ya_v = *ya_p++;
                pa_v = *pa_p++;
                rada_v = *rada_p++;

#ifdef _AVX
{
    static int __i=0;
    if (!__i)
        printf ("Using AVX!\n");
    __i = 1;
}
                /* calculate distance^2 (fDxQ)
                 */
                fDx = _mm256_sub_ps(_mm256_cvtepi32_ps (xa_v), px);
                fDy = _mm256_sub_ps(_mm256_cvtepi32_ps (ya_v), py);

                fDxQ = _mm256_add_ps (_mm256_mul_ps(fDx, fDx), _mm256_mul_ps(fDy, fDy));

                /* calculate (rad1 + rad2)^2 (fppRad4Q) */
                fppRad4Q = _mm256_cvtepi32_ps (rada_v);
                fppRad4Q = _mm256_add_ps (fppRad4Q, rad4);
                fppRad4Q = _mm256_mul_ps (fppRad4Q, fppRad4Q);

                /* check whether distance is > radius
                 */
                tmp = _mm256_cmp_ps (fppRad4Q, fDxQ, _CMP_GT_OS );

                /* check against current minimum distance */
                tmp2 = _mm256_cmp_ps (fDxQ, minDistQ, _CMP_LT_OS);

                /* select particles if distance < radius and distance < current minDist */
                tmp = _mm256_and_ps (tmp, tmp2);

                fDxQ = _mm256_and_ps (fDxQ, tmp);
                ppx = (v8si)_mm256_and_ps ((v8sf)pa_v, tmp);
                
                minDistQ = _mm256_andnot_ps (tmp, minDistQ);
                currentP = (v8si)_mm256_andnot_ps ( tmp, (v8sf)currentP);

                minDistQ = _mm256_or_ps (minDistQ, fDxQ);
                currentP = (v8si)_mm256_or_ps ((v8sf)currentP, (v8sf)ppx);
#else
                /* calculate distance^2 (fDxQ)
                 */
                iDx = _mm_sub_epi32 (xa_v, px);
                iDy = _mm_sub_epi32 (ya_v, py);
                fDxQ = _mm_cvtepi32_ps (iDx);
                fDy = _mm_cvtepi32_ps (iDy);

                fDxQ = _mm_mul_ps (fDxQ, fDxQ);
                fDy = _mm_mul_ps (fDy, fDy);
                fDxQ = _mm_add_ps (fDxQ, fDy);


                /* calculate (rad1 + rad2)^2 (fppRad4Q) */
                fppRad4Q = _mm_cvtepi32_ps (_mm_slli_epi32(rada_v, RADIUS_SHIFT));
                fppRad4Q = _mm_add_ps (fppRad4Q, rad4);
                fppRad4Q = _mm_mul_ps (fppRad4Q, fppRad4Q);

                /* check whether distance is > radius
                 */
                tmp = _mm_cmpgt_ps (fppRad4Q, fDxQ);

                /* check against current minimum distance */
                tmp2 = _mm_cmplt_ps (fDxQ, minDistQ);

                /* select particles if distance < radius and distance < current minDist */
                tmp = _mm_and_ps (tmp, tmp2);

                fDxQ = _mm_and_ps (fDxQ, tmp);
                ppx = _mm_and_si128 (pa_v, (v4si)tmp);
                
                minDistQ = _mm_andnot_ps (tmp, minDistQ);
                currentP = _mm_andnot_si128 ( (v4si)tmp, currentP);

                minDistQ = _mm_or_ps (minDistQ, fDxQ);
                currentP = _mm_or_si128 (currentP, ppx);
#endif
            }
        }

        pi = 0;

#ifdef _AVX
        {
            _mm256_store_si256 ((__m256i*)darr, (__m256i)minDistQ);
            _mm256_store_si256 ((__m256i*)parr, (__m256i)currentP);

            minSpq = MAX_SPEEDQF;

            if (parr[0])
            {
                minSpq = darr[0];
                pi = (parr[0]);
            }

            if (parr[1] && (darr[1] < minSpq))
            {
                minSpq = darr[1];
                pi = (parr[1]);
            }

            if (parr[2] && (darr[2] < minSpq))
            {
                minSpq = darr[2];
                pi = (parr[2]);
            }

            if (parr[3] && (darr[3] < minSpq))
            {
                minSpq = darr[3];
                pi = (parr[3]);
            }

            if (parr[4] && (darr[4] < minSpq))
            {
                minSpq = darr[4];
                pi = (parr[4]);
            }

            if (parr[5] && (darr[5] < minSpq))
            {
                minSpq = darr[5];
                pi = (parr[5]);
            }

            if (parr[6] && (darr[6] < minSpq))
            {
                minSpq = darr[6];
                pi = (parr[6]);
            }

            if (parr[7] && (darr[7] < minSpq))
            {
                minSpq = darr[7];
                pi = (parr[7]);
            }
        }
#elif defined(_SSE41)
        {
            int tmpP;
            float tmpDistQ;
            minSpq = MAX_SPEEDQF;

            if ((tmpP = _mm_extract_epi32 (currentP, 0)) != 0)
            {
                _MM_EXTRACT_FLOAT(minSpq, minDistQ, 0);
                pi = tmpP;
            }

            if ((tmpP = _mm_extract_epi32 (currentP, 1)) != 0)
            {
                _MM_EXTRACT_FLOAT(tmpDistQ, minDistQ, 1);

                if (tmpDistQ < minSpq)
                {
                    pi = tmpP;
                    minSpq = tmpDistQ;
                }
            }

            if ((tmpP = _mm_extract_epi32 (currentP, 2)) != 0)
            {
                _MM_EXTRACT_FLOAT(tmpDistQ, minDistQ, 2);

                if (tmpDistQ < minSpq)
                {
                    pi = tmpP;
                    minSpq = tmpDistQ;
                }
            }

            if ((tmpP = _mm_extract_epi32 (currentP, 3)) != 0)
            {
                _MM_EXTRACT_FLOAT(tmpDistQ, minDistQ, 3);

                if (tmpDistQ < minSpq)
                {
                    pi = tmpP;
                    minSpq = tmpDistQ;
                }
            }

        }
#else
        {
            _mm_store_si128 ((__m128i*)darr, (__m128i)minDistQ);
            _mm_store_si128 ((__m128i*)parr, (__m128i)currentP);

            minSpq = MAX_SPEEDQF;

            if (parr[0])
            {
                minSpq = darr[0];
                pi = (parr[0]);
            }

            if (parr[1] && (darr[1] < minSpq))
            {
                minSpq = darr[1];
                pi = (parr[1]);
            }

            if (parr[2] && (darr[2] < minSpq))
            {
                minSpq = darr[2];
                pi = (parr[2]);
            }

            if (parr[3] && (darr[3] < minSpq))
            {
                minSpq = darr[3];
                pi = (parr[3]);
            }
        }
#endif

        if (pi == 0)
        {
            if (newSi != oldSi)
                goto move;
            
            p->pos = newPos;

            return oldSi;
        }

        pp = PARTICLE_DESC(pi);
    }
#endif /* _SSE */

    ppRad = PART_RADIUS(pp);
#ifndef _CHAIN
    minPpDistQ = (pRad + ppRad) * (pRad + ppRad);
#endif


#ifdef _CHAIN
    if (!isLinked(p, pp))
    {
        linked = 0;

        /* not colliding with connected particle
         */
        if (openLinksActive(p)) 
        {
            /* Have at least one open connection */

            if (PTYPE(p) == PTYPE(pp))
            {
                /* hit chain particle. Always connect if partner has at least one
                 * open connection
                 */
                if (openLinks(pp, GETVAL32(chainConnectToPassive)))
                {
                    linkParticles (p, pp, GETVAL32(chainConnectToPassive), DFL_LINK_DATA);

                    linked = 1;
                }
            }
            else if (chConnectToAll && (pdescs[PTYPE(pp)]->getRot))
            {
                /* connect to other particle if it's part of a multi-particle body
                 * (e.g. a blob, or a dla type).
                 */
                linkParticles (p, pp, 1, DFL_LINK_DATA);
                linked = 1;

            }
#ifdef CARNIVORE
            else
            {
                /* just eat it ..
                 */
                pp->mass -= 1;

                p->food += PMASS_TO_FOOD(1);
                
                if (pp->mass == 0)
                    removeParticleDelayed(pp);
            }
#endif
        }
#ifdef CARNIVORE
        else if (PTYPE(p) != PTYPE(pp))
        {
            /* just eat it ..
             */
            pp->mass -= 1;

            p->food += PMASS_TO_FOOD(1);
            
            if (pp->mass == 0)
                removeParticleDelayed(pp);
        }
#endif

    }
    else
    {
        linked = 1;
    }
#endif

#ifndef _CHAIN
    /* fusion if particles are close  */
    if ((minSpq < (minPpDistQ * GETVAL32(peFusionRad))/100) && (PTYPE(p) == PTYPE(pp)))
    {
        /* do it only if distance fell below minimum 7 consecutive times
         */
        if (p->pflags == 7)
        {
            if (p->mass + pp->mass <= MAX_MASS)
            {
                /* fusion particles, eliminate one, release energy
                 */
                pp->mass = p->mass + pp->mass;

                if (getValue(&particleAttraction))
                {
#ifdef USE_OPENCL
                    clSchedKickFieldAt (&screenPos, pp->mass << 8);
#else
                    diffArray[screenPos.d.x + screenPos.d.y*wid] += pp->mass << 7;
#endif
                }
#if 1
                /* create a bunch of energy particles ..
                 */
                if (GETVAL32(peFusionGenEnergy))
                {
                    static vec_t nullvec = {{0,0}};
                    int m = p->mass / GETVAL32(peFusionGenEnergy) + 1;
                    extern int energyType;

                    createParticleAsync (&p->pos, &nullvec, 0, m, energyType, 1, 0, getColorSetting(energyType, 0), NULL, GETVAL32(peFusionGenEnergy));
                }
#endif


                td->drawLinkLine = 1;
                return NULL;
            }
#if 0
            else
            {
                p->mass = (p->mass + pp->mass) - MAX_MASS;
                pp->mass = MAX_MASS;
            }
#endif
        }
        p->pflags ++;
    }
    else
    {
        p->pflags = 0;
    }
#endif

    ppRad = PART_RADIUS(pp);

    if (minSpq > 0.0)
    {
        FLOAT dlen;
        FLOAT dx, dy;
        FLOAT frc;

        FLOAT diffy = pp->pos.d.y - newPos.d.y;
        FLOAT diffx = pp->pos.d.x - newPos.d.x;
        FLOAT impx1, impy1, impx2, impy2;
        int off;

        dlen = minSpq;

        /* We need the relative mass difference of the two involved particles.
         * wp is in the range of 0..1, 0.5 for the case that the particles are of same
         * mass.
         */
        FLOAT wp = (FLOAT)(((int)pp->mass) << 1) / (FLOAT)(p->mass + pp->mass);

        dlen = sqrt(dlen) + 0.5;

        dx = (FLOAT)diffx / dlen;
        dy = (FLOAT)diffy / dlen;

        td->v.collissions++;

        cx = pp->pos.d.x - p->pos.d.x;
        cy = pp->pos.d.y - p->pos.d.y;

#if 0
        p->vec.d.x = pp->vec.d.x = (p->vec.d.x * p->mass + pp->vec.d.x * pp->mass) / (p->mass + pp->mass);
        p->vec.d.y = pp->vec.d.y = (p->vec.d.y * p->mass + pp->vec.d.y * pp->mass) / (p->mass + pp->mass);
#endif

        /* check whether the two particles move away from each other or approach eath other
         */
        if (((-cy)*(tvec.d.y - pp->vec.d.y) - (cx)*(tvec.d.x - pp->vec.d.x)) < 0)
        {
            /* particle are approaching each other: do a collisison
             * In the next iteration we will probably do the "else" path (move particles further
             * away from each other if they are still within their collission area).
             */
            FLOAT wf;
           
#if 1
            /* XXX immedeately do the "squeeze-out", 
             */
            off = (((dlen - (pRad + ppRad))) * wp ) / 16.0 + 0.5;

            newPos.d.x += (dx) * off;
            newPos.d.y += (dy) * off;

            screenPos.d.x = SCREENC (newPos.d.x);
            screenPos.d.y = SCREENC (newPos.d.y);
#endif

#ifdef _CHAIN
            if (linked)
            {
                p->vec.d.x = (p->vec.d.x * p->mass + pp->vec.d.x * pp->mass) / ((int)p->mass + (int)pp->mass);
                p->vec.d.y = (p->vec.d.y * p->mass + pp->vec.d.y * pp->mass) / ((int)p->mass + (int)pp->mass);

                ACCEL_TO(pp, p->vec.d.x, p->vec.d.y);

            }
//            else
#endif
            {
                if (GETVAL32(preassure))
                {
                    wf = ((FLOAT)(pRad + ppRad) / (dlen)) * ((float)GETVAL32(preassure)/1000.0);
                }
                else
                {
                    wf = 0.0;
                }

                frc = (tvec.d.x * dx + tvec.d.y * dy);
                impx1 = frc * dx;
                impy1 = frc * dy;

                dx = -dx;
                dy = -dy;

                frc = (pp->vec.d.x * dx + pp->vec.d.y * dy);
                impx2 = frc * dx;
                impy2 = frc * dy;

                /* adjust both particle's vectors, based on
                 * - The impuls resulting from the collission, adjusted by the relative weight
                 *   of the two particles
                 * - Constant collLoss, which can be adjusted so that particles lose energy
                 *   when colliding.
                 * - "push-out" direction times 1/(relative weight) times perticle preassure:
                 *   Heavy particle gets less impuls from light particle and vice versa.
                 */
//                wp *= 2;

                p->vec.d.x = (FLOAT)(p->vec.d.x + ((impx2 - impx1)*wp)) + dx * wf * wp;
                p->vec.d.y = (FLOAT)(p->vec.d.y + ((impy2 - impy1)*wp)) + dy * wf * wp;

                wp = 2 - wp;

                pdescs[PTYPE(pp)]->hit (pp, (int)(((impx1 - impx2)*wp) - dx * wf * wp), (int)(((impy1 - impy2)*wp) - dy * wf * wp));
            }
#if 0
            int _i;
            for (_i = 0; _i < 3; _i++)
            if ((pp->color.c[_i] - p->color.c[_i]) > 0.8f)
                p->color.c[_i] = pp->color.c[_i];
#else
#ifdef _CHAIN
            if (!linked)
#endif
                mergeParticleColors (p, pp);
#endif
        }
        else
        {
            /* particles move away from each other:
             * move particles in the direction of the collisison radis, but dont actually do a collission
             */
            FLOAT wf;
            if (GETVAL32(preassure))
            {
                wf = ((FLOAT)(pRad + ppRad) / (dlen)) * ((float)GETVAL32(preassure)/1000.0) * wp;
            }
            else
            {
                wf = 0.0;
            }

//            if (!lhit)
            {
                /* when two particles collide, the distance will temporarily be smaller than allowed (i.e. one
                 * particle is inside the other. off is the distance we need to move the particle
                 * "out" so that it is exatly at the other particle's border.
                 *
                 * Divide by 8 so that the particle is "pushed out" gradually.
                 * Multiply by wp so that (relativly) heavy particles don't get pushed out as fast.
                 */
                off = (((dlen - (pRad + ppRad))) * wp ) / 8;

                /* push out the particle without changing the impuls */
                newPos.d.x += (dx) * off;
                newPos.d.y += (dy) * off;

                screenPos.d.x = SCREENC (newPos.d.x);
                screenPos.d.y = SCREENC (newPos.d.y);
            }

            wp *= 2;
            p->vec.d.x += dx * wf * wp;
            p->vec.d.y += dy * wf * wp;

            wp = 2 - wp;

            pdescs[PTYPE(pp)]->hit (pp, (int)(-dx * wf * wp), (int)(-dy * wf * wp));
        }
    }

    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

    if (newSi->particleList == SCR_BRICK)
    {
        return oldSi;
    }
    
move:
#include "do_move.c"
}

#if 0
void prep_move (particle *p, screeninfo *oldSi, threadData *td)
{
    screeninfo *tmpSi;
#ifndef _SSE
    {
        int        px, py;
        int        i;

        minSpq = 0xffffffffffffffffULL;
        px = py = 0;

        tmpSi = oldSi;
        for (i = -1; i < 8;)
        {
            if (tmpSi->particleList <= SCR_BRICK)
            {
                i++;
                tmpSi = SI_DIR(oldSi, i);
                continue;
            }
            
            for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
            {
                uint64_t ppDistQ;

                if (tpp == p)
                    continue;

#if 0
                if ((IS_LINKED(p) && (LINKED_PARTICLE(p) == tpp)))
                    continue;

                if (PTYPE(tpp) == PTYPE(p))
                {
                    if (IS_LINKED(p) && IS_LINKED(tpp) && ROT_DIR_EQUAL(p,tpp))
                    {
                        continue;
                    }
                }
#endif

                ppRad = PART_RADIUS(tpp);

                ppDistQ = (pRad + ppRad) * (pRad + ppRad);

                cx = tpp->pos.d.x - newPos.d.x;
                cy = tpp->pos.d.y - newPos.d.y;

                spq = (uint64_t)cx * (uint64_t)cx + (uint64_t )cy * (uint64_t)cy;

                if (spq < ppDistQ)
                {
                    if (spq < minSpq)
                    {
                        minSpq = spq;
#if 0
                        minPpDistQ = ppDistQ;
#endif
                        pp = tpp;
                    }
                }
#if 0
                else if (particleAttraction)
                {
                    px += cx;
                    py += cy;
                }
#endif
            }
            i++;
            tmpSi = SI_DIR(oldSi, i);
        }

        p->vec.d.x += px/5000;
        p->vec.d.y += py/5000;

        if (pp == NULL)
        {
            if (newSi != oldSi)
                goto move;
            
            p->pos = newPos;

            return oldSi;
        }
    }
#else
    {
        /* SSE Implementation for finding nearest particle */

        int        pi;
        int        idx = 0;
#ifndef _AVX
        v4si    px, py;
        v4si    iDx, iDy;
        v4si    ppx;
        v4sf    rad4;
        v4sf    fDxQ, fDy;
        v4sf    fppRad4Q;
        v4sf    tmp, tmp2;
        v4sf    minDistQ;        /* current minimum distance */
        v4si    currentP;        /* particle vectors associated with mindist */
#else
        v8si    ppx;
        v8sf        px, py, rad4;
        v8sf    fDxQ, fDy, fDx;
        v8sf    fppRad4Q;
        v8sf    tmp, tmp2;
        v8sf    minDistQ;        /* current minimum distance */
        v8si    currentP;        /* particle vectors associated with mindist */
#endif
#ifdef _SSE2
        float   darr[4] ALIGN16_END;
        unsigned int parr[4] ALIGN16_END;
#endif
#ifdef _AVX
        float   darr[8] ALIGN32_END;
        unsigned int parr[8] ALIGN32_END;
#endif

        /* this particles attributes:
         * - Position
         * - radius^2
         */
#ifndef _AVX
        px = _mm_set1_epi32 (newPos.d.x);
        py = _mm_set1_epi32 (newPos.d.y);
        rad4 = _mm_cvtepi32_ps (_mm_set1_epi32 (pRad));

        /* start values
         * currentP contains the 4 particle descriptors associated to
         * the current 4 particles being calculated. 
         */
#pragma GCC diagnostic ignored "-Wuninitialized"
        SET0(currentP);
        minDistQ = _mm_set1_ps (MAX_SPEEDQF);
#else
        px = _mm256_set1_ps ((float)newPos.d.x);
        py = _mm256_set1_ps ((float)newPos.d.y);
        rad4 = _mm256_set1_ps ((float)pRad);

        /* start values
         * currentP contains the 4 particle descriptors associated to
         * the current 4 particles being calculated. 
         */
        currentP = _mm256_setzero_si256();
        minDistQ = _mm256_set1_ps (MAX_SPEEDQF);
#endif

        /* calcluate the nearest neightbour particle
         * step1 collects all particles on the neighbouring 8 pixels into 4 arrays (x, y, radius and particle descriptor)
         * step2 reduces these arrays to 2 sse2 double-quad words: minDistQ with the 4 minimum distances^2, currentP
         * with the particle desctiptors associated with the 4 minimum distances (or 0).
         * step3 takes the particle with the smalles distance out of the rmaining four.
         *
         * step 3 is not very elegant, and could be completely eliminitated if further processing also uses sse math.
         * this would have the benefit that we could calculate collission with four particles simultanously, not just one.
         *
         * Another possibility would be to do step 1 for all pariticles and offload step 2 to the GPU ..
         */

        {
#ifdef _AVX
            v8si xa_v;
            v8si ya_v;
            v8si pa_v;
            v8si rada_v;
            v8si *xa_p = (__m256i*)td->xa_arr;
            v8si *ya_p = (__m256i*)td->ya_arr;
            v8si *pa_p = (__m256i*)td->pa_arr;
            v8si *rada_p = (__m256i*)td->rada_arr;
#else
            v4si xa_v;
            v4si ya_v;
            v4si pa_v = _mm_set1_epi32 (0);
            v4si rada_v;
            v4si *xa_p = (__m128i*)td->xa_arr;
            v4si *ya_p = (__m128i*)td->ya_arr;
            v4si *pa_p = (__m128i*)td->pa_arr;
            v4si *rada_p = (__m128i*)td->rada_arr;
#endif
            idx = 0;

            /* 8 neighbour pixels
             */
            tmpSi = oldSi;
            dir = -1;
            while (dir < 8)
            {
                for (tpp = tmpSi->particleList; tpp > SCR_BRICK; tpp = tpp->next)
                {
                    if (tpp == p)
                        continue;

                    td->xa_arr[idx] = tpp->pos.d.x;
                    td->ya_arr[idx] = tpp->pos.d.y;
                    td->pa_arr[idx] = PARTICLE_ID(tpp);
#ifdef _AVX
                    td->rada_arr[idx] = tpp->size << RADIUS_SHIFT;
#else
                    td->rada_arr[idx] = tpp->size;
#endif
                    idx++;

                    if (idx >= PART_COLL_MAX)
                        goto labort;
                }
                dir++;
                tmpSi = SI_DIR(oldSi,dir);
            }

            if (idx == 0)
            {
                if (newSi != oldSi)
                    goto move;
                
                p->pos = newPos;

                return oldSi;
            }

            /* invalidate unused particle attributes
             */
            for (;(idx & 3) != 0; idx++)
                td->pa_arr[idx] = 0;

labort:

#if 1
            /* count average number of neighbour particles 
             */
            td->v.gas_nb_total += idx;
            td->v.gas_nb_ccount ++;
#endif

            /* Now calculate the 4 particles with the smallest distance
             * in parallel (4 particles in each iteration)
             */
            for (; idx > 0; idx -= 4)
            {
                xa_v = *xa_p++;
                ya_v = *ya_p++;
                pa_v = *pa_p++;
                rada_v = *rada_p++;

#ifdef _AVX
{
    static int __i=0;
    if (!__i)
        printf ("Using AVX!\n");
    __i = 1;
}
                /* calculate distance^2 (fDxQ)
                 */
                fDx = _mm256_sub_ps(_mm256_cvtepi32_ps (xa_v), px);
                fDy = _mm256_sub_ps(_mm256_cvtepi32_ps (ya_v), py);

                fDxQ = _mm256_add_ps (_mm256_mul_ps(fDx, fDx), _mm256_mul_ps(fDy, fDy));

                /* calculate (rad1 + rad2)^2 (fppRad4Q) */
                fppRad4Q = _mm256_cvtepi32_ps (rada_v);
                fppRad4Q = _mm256_add_ps (fppRad4Q, rad4);
                fppRad4Q = _mm256_mul_ps (fppRad4Q, fppRad4Q);

                /* check whether distance is > radius
                 */
                tmp = _mm256_cmp_ps (fppRad4Q, fDxQ, _CMP_GT_OS );

                /* check against current minimum distance */
                tmp2 = _mm256_cmp_ps (fDxQ, minDistQ, _CMP_LT_OS);

                /* select particles if distance < radius and distance < current minDist */
                tmp = _mm256_and_ps (tmp, tmp2);

                fDxQ = _mm256_and_ps (fDxQ, tmp);
                ppx = (v8si)_mm256_and_ps ((v8sf)pa_v, tmp);
                
                minDistQ = _mm256_andnot_ps (tmp, minDistQ);
                currentP = (v8si)_mm256_andnot_ps ( tmp, (v8sf)currentP);

                minDistQ = _mm256_or_ps (minDistQ, fDxQ);
                currentP = (v8si)_mm256_or_ps ((v8sf)currentP, (v8sf)ppx);
#else
                /* calculate distance^2 (fDxQ)
                 */
                iDx = _mm_sub_epi32 (xa_v, px);
                iDy = _mm_sub_epi32 (ya_v, py);
                fDxQ = _mm_cvtepi32_ps (iDx);
                fDy = _mm_cvtepi32_ps (iDy);

                fDxQ = _mm_mul_ps (fDxQ, fDxQ);
                fDy = _mm_mul_ps (fDy, fDy);
                fDxQ = _mm_add_ps (fDxQ, fDy);


                /* calculate (rad1 + rad2)^2 (fppRad4Q) */
                fppRad4Q = _mm_cvtepi32_ps (_mm_slli_epi32(rada_v, RADIUS_SHIFT));
                fppRad4Q = _mm_add_ps (fppRad4Q, rad4);
                fppRad4Q = _mm_mul_ps (fppRad4Q, fppRad4Q);

                /* check whether distance is > radius
                 */
                tmp = _mm_cmpgt_ps (fppRad4Q, fDxQ);

                /* check against current minimum distance */
                tmp2 = _mm_cmplt_ps (fDxQ, minDistQ);

                /* select particles if distance < radius and distance < current minDist */
                tmp = _mm_and_ps (tmp, tmp2);

                fDxQ = _mm_and_ps (fDxQ, tmp);
                ppx = _mm_and_si128 (pa_v, (v4si)tmp);
                
                minDistQ = _mm_andnot_ps (tmp, minDistQ);
                currentP = _mm_andnot_si128 ( (v4si)tmp, currentP);

                minDistQ = _mm_or_ps (minDistQ, fDxQ);
                currentP = _mm_or_si128 (currentP, ppx);
#endif
            }
        }

        pi = 0;

#ifdef _AVX
        {
            _mm256_store_si256 ((__m256i*)darr, (__m256i)minDistQ);
            _mm256_store_si256 ((__m256i*)parr, (__m256i)currentP);

            minSpq = MAX_SPEEDQF;

            if (parr[0])
            {
                minSpq = darr[0];
                pi = (parr[0]);
            }

            if (parr[1] && (darr[1] < minSpq))
            {
                minSpq = darr[1];
                pi = (parr[1]);
            }

            if (parr[2] && (darr[2] < minSpq))
            {
                minSpq = darr[2];
                pi = (parr[2]);
            }

            if (parr[3] && (darr[3] < minSpq))
            {
                minSpq = darr[3];
                pi = (parr[3]);
            }

            if (parr[4] && (darr[4] < minSpq))
            {
                minSpq = darr[4];
                pi = (parr[4]);
            }

            if (parr[5] && (darr[5] < minSpq))
            {
                minSpq = darr[5];
                pi = (parr[5]);
            }

            if (parr[6] && (darr[6] < minSpq))
            {
                minSpq = darr[6];
                pi = (parr[6]);
            }

            if (parr[7] && (darr[7] < minSpq))
            {
                minSpq = darr[7];
                pi = (parr[7]);
            }
        }
#elif defined(_SSE41)
        {
            int tmpP;
            float tmpDistQ;
            minSpq = MAX_SPEEDQF;

            if ((tmpP = _mm_extract_epi32 (currentP, 0)) != 0)
            {
                _MM_EXTRACT_FLOAT(minSpq, minDistQ, 0);
                pi = tmpP;
            }

            if ((tmpP = _mm_extract_epi32 (currentP, 1)) != 0)
            {
                _MM_EXTRACT_FLOAT(tmpDistQ, minDistQ, 1);

                if (tmpDistQ < minSpq)
                {
                    pi = tmpP;
                    minSpq = tmpDistQ;
                }
            }

            if ((tmpP = _mm_extract_epi32 (currentP, 2)) != 0)
            {
                _MM_EXTRACT_FLOAT(tmpDistQ, minDistQ, 2);

                if (tmpDistQ < minSpq)
                {
                    pi = tmpP;
                    minSpq = tmpDistQ;
                }
            }

            if ((tmpP = _mm_extract_epi32 (currentP, 3)) != 0)
            {
                _MM_EXTRACT_FLOAT(tmpDistQ, minDistQ, 3);

                if (tmpDistQ < minSpq)
                {
                    pi = tmpP;
                    minSpq = tmpDistQ;
                }
            }
        }
#else
        {
            _mm_store_si128 ((__m128i*)darr, (__m128i)minDistQ);
            _mm_store_si128 ((__m128i*)parr, (__m128i)currentP);

            minSpq = MAX_SPEEDQF;

            if (parr[0])
            {
                minSpq = darr[0];
                pi = (parr[0]);
            }

            if (parr[1] && (darr[1] < minSpq))
            {
                minSpq = darr[1];
                pi = (parr[1]);
            }

            if (parr[2] && (darr[2] < minSpq))
            {
                minSpq = darr[2];
                pi = (parr[2]);
            }

            if (parr[3] && (darr[3] < minSpq))
            {
                minSpq = darr[3];
                pi = (parr[3]);
            }
        }
#endif

        if (pi == 0)
        {
            if (newSi != oldSi)
                goto move;
            
            p->pos = newPos;

            return oldSi;
        }

        pp = PARTICLE_DESC(pi);
    }
#endif /* _SSE */


}


#endif
