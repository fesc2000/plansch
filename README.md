
Abstract
--------
This is plansch, a little particle "simulation".
It started as game for my son as an attempt to avoid the mess in the bathroom
when he played with water.
In the meantime, playing with water got boring, plansch got too complicated,
so it's my lunchtime programming game now ..


Usage
-----
Although there are various variants, the only one maintained is the
OpenCL code.

To start: "plansch_opencl" / plansch_opencl.exe
Try the "ngs.bat" files (-C option) if it does not work on exotic
(i.e. non-AMD) GPUs.

Other variants:
- default
    No GPU suppport, should run on all SSE2 capable CPUs.

- opengl  
    With GPU support using GLSL shaders for force field calculation.
	Screen output done with OpenGL. Not as fast as OpenCL code, GPU
    calculations can be disabled with "-G" switch if it causes problems.

- sopengl
    Like "default", output is done by OpenGL.

Focus is on the OpenCl version, the other ones aren't really maintained any
more.

Command Line Parameters
-----------------------

  -n     Maximum number of particles
  -N     OpenCL: Maximum number of GPU particles
  -c	  Starting CPU core
  -G	  Force not to use GPU
  -T	  Default particle type
  -m	  Which mode to run at (shell wrapper option)
  -X     Check whether mode is supported and exit
  -t     Number of particle processing threads (1, 2, 4)
  -r     Place all particles randomly on the screen, using a random charge
         between -1 and 1
  -H     No screen output
  -g     Screen geometry in <width>x<height>
  -f     Fullscreen mode
  -u     Particle field iterations per movement iterations
         Default: 1 (5 with GPU support)
  -M     Number of iterations.
  -I     Runtime in seconds
  -l     Load .tape file at startup.
  -L     Options for startup file:
         exit - Exit program when tape is finished
         imm  - Load tape contents immedeately
  -C     OpenCl: Do not try to directly render into OpenGl textures/PBOs.
         Make the data take a detour via host memory instead, which is 
         slower but sometimes required (e.g. for CPU OpenCl platforms ...
  -D i[,p] OpenCl: Use the i-th available device on the p-th platform (default = 0,0)
  -O     OpenGl/Cl: Run in main loop instead of spawning a dedicated thread
  -p     OpenCL: Try to used pinned memory
  -o 0|1 OpenCL: Force to use/not to use local memory (default 0)
  -A     OpenCL: Use half instead of float for field calculation (experimental)
  -2     OpenCL: Try to use OpenCL 2 code even if supported by device.
  -U     OpenGL: Use a cursor rendered by OpenGL


System Requirements
-------------------
- A x86 CPU supporting SSE2. Recommended are at least 2 cores.
  SSSE3 or SSE4.1 code is enabled dynamically if the CPU supports it.
- SDL runtime library
- Optionally: SDL_ttf runtime library
- Optionally: OpenGL/GLSL support 
- Optionally: OpenCL 1.2 (64-bit atomics required)

User interface
--------------
At the bottom are a bunch of buttons you can play around with. Pressing
F1 over a button provides some help, pressing F1 somewhere else lists the
keyboard shortcuts.
Pressing tab or the right mouse button on in an unoccupied menu space toggles
between additional panels.
RMB over a button opens a sub-menu.

Top line of buttons:
- The op buttons define the "draw operation"
 - Walls (pixel based and vectorized)
 - Create particles
 - Paint particles
 - Use stencil from copied screen area (CPU only, not maintained)
 - Erase walls, particles
 - Draw pipes (with or without acceleration)
 - Draw acceleration field (direction defined by RMB)
 - Push particles (CPU only)
 - Capture and drag particles
 - Apply selected attributes to particles
 - Mark/unmark particles
- Next are selections how the operation is applied:
 - Once below the selected stancil ("stamp")
 - Continuously while pressing LMB and moving
 - Using a defined line/circle
 - Within bounded areas ("fill mode")
- Rest of first line defines a set of stencils to use for drawing.

Bottom left selects the particle type to use. Each particle type has
a set of type specific controls which appear right of the type
selection.
Common controls are
- Mass of a particle (1-255)
- Charge of a particle (-1, 0, +1)
- Color of a particle (open sub-menu with RMB over one of the three
  charge selectors to enable a color wheel)
- etcpp (read button help)

The rest of the button affects various stuff like
- Gravity vector
- Wall heating on/off
- Visualization options
- Color merging options
- Force field options

Additional runtime settings are available on the 2nd panel (press RMB on empty button area)


The rest (drawing particles, walls, etc.) happens in the draw area:

Some keyboard/mouse shortcuts:
xx			        - Remove all particles
qq			        - Quit
b			        - Draw a bounding box around current screen area
space			    - Stop/resume particle engines
Mouse wheel	        - Zoom in/out
Wheel button+Drag	- Move viewport
Wheel button+Shift	- Move viewport with particles under mouse (OpenCL only)
Shift+wheel button  - Follow partices under mouse (OpenCL), wheel button to stop following.
Shift+mouse wheel	- Define angle of ejected particles.
RMB+Drag		    - Define direction/speed of generated particles
	

Building
--------
For Linux, a simple make should do.

For windows its best to cross-compile under Linux using mxe
(opencl headers need to be installed on host system)

Follow the installation guides for mxe at http://pkg.mxe.cc/
I assume mxe is installed in /usr/lib/mxe, otherwise edit mxe.mk

Ubuntu example:
sudo sh -c 'echo "deb http://mirror.mxe.cc/repos/apt xenial main" > /etc/apt/sources.list.d/mxeapt.list'
sudo apt-get update && apt install -y --allow-unauthenticated mxe-i686-w64-mingw32.static-sdl mxe-i686-w64-mingw32.static-glew opencl-headers ocl-icd-libopencl1

Then do:
make CROSS=1

For a 64-bit binary:
sudo apt-get update && apt install -y --allow-unauthenticated mxe-x86-64-w64-mingw32.static-sdl mxe-x86-64-w64-mingw32.static-glew opencl-headers ocl-icd-libopencl1
make CROSS=1 MACH=x86_64


Bugs/Limitations
----------------
- Many, it will always be an alpha version ..
- Save/restore/copy/paste related code hasn't been maintained 
- Some features apply only to specific particle engines (GPU/CPU)


Issues
------
- AMD/fglrx/Ubuntu/Debian : If opencl crashes during startup with an X error, or some messages
  regarding swrast appear, do the following:

    cd /usr/lib/x86_64-linux-gnu
    sudo mv libGL.so libGL.so_
    sudo ln -s libGL.so.1 libGL.so

    Problem is that application is linked against libGL.so, whereas opencl explictly 
    attempts to load libGL.so.1 again. If these two are different, OpenCL initialization
    crashes when using OpenGL/OpenCL interoperability.

- OpenGL/CL sharing
    It might happen that startup fails e.g. on CPU or other untested platforms (Intel, Nvidia)
    Disable this with the -C switch and try again (_ngs.bat files on windows).
    Some limitation apply (no link lines)


Have fun.


P.S. If anyone is reading this and actually succeeded in getting this running, i'd be 
interested in performance numbers:
- Select GPU particles
- Draw border ("b")
- Select "Create all particles" at the bottom right
- Select 50 for "Particle Engine time factor" (clock symbol in lower row)
- Move the gravity arrow down as far as possible.
- Wait for the water to settle, read the "wps" number in the right-most status window
  (million particles/sec) and tell me ..
	Current results:
	- AMD HD5770: 8 MPPS
	- AMD RX480: 50 MPPS
	- AMD RX Vega 56: 85 MPPS

