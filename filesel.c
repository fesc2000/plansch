/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include "SDL_rotozoom.h"

#define MAX_FILE_SELECTORS        20
static int numFileSelectors;
static int fsHeight;
static fileRep *rep = NULL;

#define ANTIALIAS 1

tape stencilTape =
{
    .tapePos = 0,
    .tapeLen = 0,
    .recordStart = 0,
    .buffer = NULL,
    .recording = 0
};

panel *pFileSel;


void startSelectionBox (int start);

extern button * buttonLoad;
extern button * buttonSelector;

typedef struct 
{
    int fileIndex;
    SDL_Surface *surface;
    char descr[1024];
    int fw, fh;
} fileData;

struct
{
    SDL_Rect        r;
    button *button;
    unsigned int removeTimestamp;

    fileData        d;

} thumbnails[MAX_FILE_SELECTORS];

void initFileRep (fileRep *r)
{
    if (r->initialized)
        return;

    r->fileIndexFirst = 0;
    r->fileIndexSelected = -1;
    r->initialized = 1;
}

SDL_Surface *findNextImage (int current, int offset, int *pIndex)
{
    SDL_Surface        *s;
    SDL_Surface *tcS;
    int        i;

    for (i = current + offset; (i > 0) && (i < current + 1000); i += offset)
    {
        if (loadFile (rep, i, &s, NULL, NULL) == 0)
        {
            *pIndex = i;

            tcS = SDL_ConvertSurface (s, screen->format, 0);

            SDL_SetAlpha (tcS, 0, 0xff);
            
            SDL_FreeSurface (s);

            return tcS;
        }
    }

    return NULL;

}

void redrawThumbnail (int i)
{
    SDL_Rect src, dst;

    if (!thumbnails[i].d.surface)
        return;
    
    dst = thumbnails[i].r;
//    dst.x += (dst.w - thumbnails[i].d.surface->clip_rect.w) / 2;
//    dst.y += (dst.h - thumbnails[i].d.surface->clip_rect.h) / 2;

    src = dst;
    src.x = src.y =0;

    if (SDL_BlitSurface (thumbnails[i].d.surface, 
                     &src, 
                     screen, 
                     &dst) == -1)
    {
        printf ("initFsButtons: SDL_BlitSurface failed:%s\n", SDL_GetError());
        SDL_UnlockSurface (screen);
    }
}

static void setSurfaceAlpha (SDL_Surface *s, int val)
{
    if (!s)
        return;

    SDL_SetAlpha (s, 0, val);
}

void initFsButtons(int scrl)
{
    SDL_Surface *full = NULL;
    int                 i, fi;
    double          bZoom;
    int                 searchStart;
    int                 rightmost = numFileSelectors - 1;
    int                 dstSize = fsHeight - 2;
    SDL_Rect         dst;

    if (!pFileSel->visible)
        return;

    if (scrl == 1)
    {
        if (thumbnails[rightmost].d.fileIndex != 0)
        {
            full = findNextImage (thumbnails[rightmost].d.fileIndex, 1, &fi);

            if (full)
            {
                if (thumbnails[0].d.surface)
                    SDL_FreeSurface (thumbnails[0].d.surface);

                thumbnails[0].d.surface = NULL;

                for (i = 0; i < rightmost; i++)
                {
                    thumbnails[i].d = thumbnails[i+1].d;
                }
                thumbnails[rightmost].d.fileIndex = fi;

                bZoom = (double)dstSize / (double)MAX(full->w, full->h);
                thumbnails[rightmost].d.surface = zoomSurface (full, bZoom, bZoom, ANTIALIAS);

                setSurfaceAlpha (thumbnails[rightmost].d.surface, 0xff);

                thumbnails[rightmost].d.fw = full->w;
                thumbnails[rightmost].d.fh = full->h;
            }
        }
    }
    else if (scrl == -1)
    {
        if (thumbnails[0].d.fileIndex != 0)
        {
            full = findNextImage (thumbnails[0].d.fileIndex, -1, &fi);

            if (full)
            {
                if (thumbnails[rightmost].d.surface)
                    SDL_FreeSurface (thumbnails[rightmost].d.surface);

                thumbnails[rightmost].d.surface = NULL;

                for (i = rightmost; i > 0; i--)
                {
                    thumbnails[i].d = thumbnails[i-1].d;
                }
                thumbnails[0].d.fileIndex = fi;

                bZoom = (double)dstSize / (double)MAX(full->w, full->h);
                thumbnails[0].d.surface = zoomSurface (full, bZoom, bZoom, ANTIALIAS);
                setSurfaceAlpha (thumbnails[0].d.surface, 0xff);

                thumbnails[0].d.fw = full->w;
                thumbnails[0].d.fh = full->h;
            }
        }
    }

    if (full)
        SDL_FreeSurface (full);

    for (i = 0; i < numFileSelectors; i++)
    {
        SDL_FillRect (screen, &thumbnails[i].r, COL_EMPTY);

        if (thumbnails[i].d.fileIndex == 0)
        {
            if (i == 0)
            {
                searchStart = 0;
            }
            else if (thumbnails[i-1].d.fileIndex != 0)
            {
                searchStart = thumbnails[i-1].d.fileIndex;
            }
            else
            {
                goto cont;
            }

            full = findNextImage (searchStart, 1, &fi);

            if (full == NULL)
            {
                goto cont;
            }

            thumbnails[i].d.fw = full->w;
            thumbnails[i].d.fh = full->h;

            if (thumbnails[i].d.surface)
                SDL_FreeSurface (thumbnails[i].d.surface);

            bZoom = (double)dstSize / (double)MAX(full->w, full->h);
            thumbnails[i].d.surface = zoomSurface (full, bZoom, bZoom, ANTIALIAS);
            setSurfaceAlpha (thumbnails[i].d.surface, 0xff);
            SDL_FreeSurface (full);

            thumbnails[i].d.fileIndex = fi;
        }

cont:
        if (thumbnails[i].d.fileIndex != 0)
        {
            SDL_Rect src;

            enableButton (thumbnails[i].button, 1);

            dst = thumbnails[i].r;

            src = dst;
            src.x = src.y = 0;


//            dst.x += (dst.w - thumbnails[i].d.surface->clip_rect.w) / 2;
//            dst.y += (dst.h - thumbnails[i].d.surface->clip_rect.h) / 2;

            if (SDL_BlitSurface (thumbnails[i].d.surface, 
                                 &src,
                             screen, 
                             &dst) == -1)
            {
                printf ("initFsButtons: SDL_BlitSurface failed:%s\n", SDL_GetError());
                SDL_UnlockSurface (screen);
            }

            if (rep->fileIndexSelected == thumbnails[i].d.fileIndex)
            {
                buttonBorderColor (thumbnails[i].button, COL_ORANGE);
            }
            else
            {
                buttonBorderColor (thumbnails[i].button, COL_ICONDRAW);
            }

            mkFileName (thumbnails[i].d.descr, rep, NULL, NULL, thumbnails[i].d.fileIndex);
            sprintf (thumbnails[i].d.descr + strlen(thumbnails[i].d.descr),
                         "  %dx%d",
                     thumbnails[i].d.fw,
                     thumbnails[i].d.fh);
        }
        else
        {
            strcpy (thumbnails[i].d.descr, "");
            enableButton (thumbnails[i].button, 0);
        }

    }

    updateSelPanels();
}

void fileLeft ()
{
    initFsButtons (-1);
}
 
void fileRight ()
{
    initFsButtons (1);
}

void clearThumbnail (int i)
{
    if (thumbnails[i].d.surface)
        SDL_FreeSurface (thumbnails[i].d.surface);

    memset ((char*)&thumbnails[i].d, 0, sizeof(thumbnails[i].d));

    SDL_FillRect (screen, &thumbnails[i].r, COL_EMPTY);
}

int buttonToFilesel (button *b)
{
    int i;

    for (i = 0; i < numFileSelectors; i++)
        if (thumbnails[i].button == b)
            return i;
    return -1;
}

void fileDelete (button *b)
{
    int i = buttonToFilesel (b);

    if (i == -1)
        return;

    /* remove this file
     */
    removeSaveFile (rep, thumbnails[i].d.fileIndex);

    /* clear this button and the ones to the reight */
    for (;i < numFileSelectors; i++)
    {
        clearThumbnail (i);
    }

    /* redraw stencil buttons */
    initFsButtons (0);
}

void fileSel (button * b)
{
    int                i;
    int                selected = 0;

    for (i = 0; i < numFileSelectors; i++)
    {
        if (thumbnails[i].button == b)
        {
            if (thumbnails[i].d.fileIndex <= 0)
                return;

            if (rep->loadFunc == NULL)
                return;
        
            rep->fileIndexSelected = thumbnails[i].d.fileIndex;
            rep->loadFunc (rep, thumbnails[i].d.fileIndex);

            selected = 1;
        }
    }

    if (!selected)
    {
        rep->fileIndexSelected = -1;
    }

#if 0
    for (i = 0; i < numFileSelectors; i++)
    {
        if (thumbnails[i].d.fileIndex == rep->fileIndexSelected)
            buttonBorderColor (thumbnails[i].button, COL_ORANGE);
        else
            buttonBorderColor (thumbnails[i].button, COL_ICONDRAW);
    }
#endif

    updateSelPanels();
}

void
thumbnailButtonHandler (button * b, int action, int x, int y)
{
    int event = EVENT_CODE(action);
    int mouseButton = BUTTON_ID(action);
    int ti = (intptr_t)b->userData;

    if (IS_SUB_BUTTON(b))
    {
        if (event != SUBPANEL_SELECT)
            return;

        if (b->userData == 1)
        {
            fileSel (b->parentButton);
        }
        else if (b->userData == 2)
        {
            fileDelete (b->parentButton);
        }

        return;
    }


    if (action == REDRAW)
    {
        redrawThumbnail (ti);

        return;
    }

    if (((event == BUTTON_PRESS) || (event == BUTTON_MOVE)) && (mouseButton == 1))
    {
        if (MOUSE_OVER_BUTTON(b,x,y))
            buttonBorderColor (b, COL_ORANGE);
        else
            buttonBorderColor (b, COL_ICONDRAW);

        return;
    }

    if ((event == BUTTON_RELEASE) && (mouseButton == 1))
    {
        buttonBorderColor (b, COL_ICONDRAW);

        if (MOUSE_OVER_BUTTON(b,x,y))
        {
            fileSel (b);
        }
    }
}

void selectFileIndex (int fileIndex)
{
    int i;

    if (fileIndex <= 0)
        return;

    for (i = 0; i < numFileSelectors; i++)
    {
        if ((thumbnails[i].d.fileIndex != 0) && 
            (thumbnails[i].d.fileIndex == fileIndex))
        {
            fileSel (thumbnails[i].button);
        }
    }
}

void fileSelection (int on, fileRep *r)
{
    int i;

    if (on == 2)
        on = 1 - pFileSel->visible;

    if (on == pFileSel->visible)
        return;

    if (!on)
    {
        showPanel (pFileSel, on);
        updateSelPanels();
    }

    showPanel (globalSettings, !on);
    showPanel (gravPanel, !on);
    showPanel (viewPanel, !on);
    showWallControls ( on ? 2 : 3);

    if (on)
    {
        initFileRep (r);
        rep = r;

        showPanel (pFileSel, on);
        rep->fileIndexSelected = -1;
        initFsButtons (0);

        updateSelPanels();

    }
    else
    {
        rep = NULL;
        for (i = 0; i < numFileSelectors; i++)
        {
            thumbnails[i].d.fileIndex = 0;
            if (thumbnails[i].d.surface)
                SDL_FreeSurface (thumbnails[i].d.surface);
            thumbnails[i].d.surface = NULL;
        }
    }
}

void initFileSelectorPanel (panel *p)
{
    int i;
    button *b;

    pFileSel = p;

    fsHeight = p->max.d.y;
    numFileSelectors = (p->max.d.x - 34) / (fsHeight + 1);
    fsHeight = (p->max.d.x - 34) / numFileSelectors - 3;


    makeButton (pFileSel, actionButtonHandler, 15, fsHeight, arrowLeft_pm, "Previous File", (intptr_t)fileLeft, NULL, B_NEXT);
    memset ((char*)thumbnails, 0, sizeof(thumbnails));

    for (i = 0; i < numFileSelectors; i++)
    {
        b = makeButton (pFileSel, thumbnailButtonHandler, fsHeight, fsHeight, NULL, thumbnails[i].d.descr,
                i, NULL, B_NEXT);
        makeSubPanel (b, P_DOWN_RIGHT, 1, SP_BUTTON3);

        thumbnails[i].r.x = b->x1 + 1;
        thumbnails[i].r.y = b->y1 + 1;
        thumbnails[i].r.w = BUTTON_WID(b);
        thumbnails[i].r.h = BUTTON_HEI(b);
        thumbnails[i].button = b;

        addSubButton (b, 60, 34, load_pm, "Load file", 1, B_NEXT);
        addSubButton (b, 60, 34, trash_pm, "Delete file", 2, B_NEXT);
    }
    makeButton (pFileSel, actionButtonHandler, 15, fsHeight, arrowRight_pm, "Next File", (intptr_t)fileRight, NULL, B_NEXT);
}


