/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

int settingChanged = 0;
int applyToAll = 0;
void applySettingToScreen (setting *s);
void addButtonColorHandler (button *b, uint32_t col);
settingStore_t *findStoredSetting (setting *s, void *key);

static callback_t *modifyCbList = NULL;

static void *key = NULL;

static setting *allSettings = NULL;

void registerSettingCallback (FUNCPTR fct, void *arg)
{
    callback_t *cb =
        malloc(sizeof(callback_t));
    
    cb->fct = fct;
    cb->arg = arg;

    cb->next = modifyCbList;
    modifyCbList = cb;
}

void broadcastSetting (setting *s)
{
    callback_t *cb;
    static int recurse = 0;

    if (recurse)
        return;

    for (cb = modifyCbList; cb != NULL; cb = cb->next)
    {
        recurse = 1;
        cb->fct (s, cb->arg);
        recurse = 0;
    }
    doButtonDependencies (s);
}

static void registerSetting (setting *s)
{
    s->next = allSettings;
    allSettings = s;
}

uint32 namesToHash (char *str, char *str2)
{
    int i;
    uint32 hash = 0;
    char *hp = (char*)&hash;

    for (i = 0; i < strlen(str); i++)
    {
        hp[i % sizeof(hash)] += str[i];
    }
    for (i = 0; i < strlen(str2); i++)
    {
        hp[i % sizeof(hash)] += str2[i];
    }

    return hash;
}

uint32 nameIdxToHash (char *str, int index)
{
    int i;
    uint32 hash = 0;
    char *hp = (char*)&hash;

    for (i = 0; i < strlen(str); i++)
    {
        hp[i % sizeof(hash)] += str[i] + index;
    }

    return hash;
}

setting *hashToSetting (uint32 hash)
{
    settingId *si;
    int i;

    for (si = settingsList; si->name != NULL; si++)
    {
        for (i = 0; i < si->instances; i++)
        {
            if (si->pSetting[i].hash == hash)
            {
                return &si->pSetting[i];
            }
        }
    }

    return NULL;
}

void applyDefaultSettings()
{
    settingId *si;
    int i;

    for (si = settingsList; si->name != NULL; si++)
    {
        for (i = 0; i < si->instances; i++)
        {
            si->pSetting[i].current.val.va64 = si->pSetting[i].defaultValue + 1;

            modifySetting (&si->pSetting[i], si->pSetting[i].defaultValue, 0);
        }
    }
}

void registerInternalSetting (setting *s)
{

    if (s->name == NULL)
        return;

    if (!s->help)
        s->help = s->name;

    s->current.val.va64 = s->defaultValue;
    s->valid = 1;

    if (s->minDiff == 0)
    {
        s->minDiff = 1;

        if ((s->minValue == 0) && (s->maxValue == 0))
        {
            s->maxValue = 0x7fffffff;
        }
    }

    if ((s->defaultValue < s->minValue) || 
        (s->defaultValue > s->maxValue))
    {
        s->defaultValue = s->minValue;
    }

    if (s->floatDivi == 0)
        s->floatDivi = MAX (abs(s->minValue), abs(s->maxValue));

    if (s->floatDivi == 0)
        s->floatDivi = 1;

    s->id = NULL;

    s->isArray = 0;
    s->index = 0;
    s->arrayFirst = NULL;

    registerSetting (s);
}


void initSettings()
{
    settingId *si;
    int inst;
    int i;

    for (si = settingsList; si->name != NULL; si++)
        si->pSetting->valid = 0;

    for (si = settingsList; si->name != NULL; si++)
    {
        for (i = 0; i < si->instances; i++)
        {
            if (si->pSetting[i].hash == 0)
            {
                si->pSetting[i].hash = nameIdxToHash (si->name, i);
            }
        }

        si->pSetting->help = si->name;
        si->pSetting->current.val.va64 = si->pSetting->defaultValue;
        si->pSetting->valid = 1;

        if (getHelpString(si->pSetting->help) == NULL)
            printf ("No help for \"%s\"\n", si->pSetting->help);

        if (si->pSetting == &potDivider)
        {
            if (!options->useGpu)
                si->pSetting->maxValue = 15;
        }
        if (si->pSetting == &rippleDivider)
        {
            if (!options->useGpu)
                si->pSetting->maxValue = 15;
        }

        if (si->pSetting->minDiff == 0)
        {
            si->pSetting->minDiff = 1;

            if ((si->pSetting->minValue == 0) && (si->pSetting->maxValue == 0))
            {
                si->pSetting->maxValue = 0x7fffffff;
            }
        }

        if ((si->pSetting->defaultValue < si->pSetting->minValue) || 
            (si->pSetting->defaultValue > si->pSetting->maxValue))
        {
            si->pSetting->defaultValue = si->pSetting->minValue;
        }

        if (si->pSetting->floatDivi == 0)
            si->pSetting->floatDivi = MAX (abs(si->pSetting->minValue),
                                               abs(si->pSetting->maxValue));

        if (si->pSetting->floatDivi == 0)
            si->pSetting->floatDivi = 1;

        si->pSetting->id = si;

        si->pSetting->isArray = (si->instances > 1);
        si->pSetting->index = 0;
        si->pSetting->arrayFirst = si->pSetting;

        for (inst = 1; inst < si->instances; inst++)
        {
            si->pSetting[inst] = si->pSetting[0];
            si->pSetting[inst].index = inst;

            registerSetting (&si->pSetting[inst]);
        }

    }

}

/* check if a discrete value is currently masked */
int valueIsMasked (setting *s, int val)
{
    int i;

    if (!s || !s->discrete || !s->current.isRandom)
        return 0;

    for (i = 0; i < s->current.numUnmasked; i++)
    {
        if (s->rndMapArray[i] == val)
            return 0;
    }

    return 1;
}

void maskRandomValue (setting *s, int val, int mask, int all)
{
    int i;
    int dscIndex = -1;
    if (!s || !s->discrete)
        return;
    
    if (!all)
    {
        for (i = 0; i < s->numDiscrete; i++)
        {
            if (s->valueArray[i] == val)
            {
                dscIndex = i;
                break;
            }
        }
        if (dscIndex == -1)
            return;
    }

    if (mask)
    {
        if (all)
            s->current.rndMask = 0xffffffffffffffffULL;
        else
            s->current.rndMask |= 1ULL << dscIndex;
    }
    else
    {
        if (all)
            s->current.rndMask = 0ULL;
        else
            s->current.rndMask &= ~(1ULL << dscIndex);
    }

    /* map random number index to value */
    s->current.numUnmasked = 0;
    for (i = 0; i < s->numDiscrete; i++)
    {
        if ((s->current.rndMask & (1ULL << i)) == 0ULL)
        {
            s->rndMapArray[s->current.numUnmasked] = s->valueArray[i];
            s->current.numUnmasked++;
        }
    }

    if (s->marked && (applyToAll || particlesMarked()))
    {
        settingChanged = 1;

        applySettingToScreen (s);
    }
}

void rndRange (setting *pSetting, int ena, int rndMin, int rndMax, int toArray)
{
    int i;

    if (!pSetting->canBeRandom || pSetting->discrete)
        return;

    if (pSetting->isArray && toArray)
    {
        for (i = 0; i < pSetting->id->instances; i++)
        {
            rndRange (&pSetting->arrayFirst[i], ena, rndMin, rndMax, 0);
        }
        return;
    }

    if (!pSetting->current.isRandom && ena)
    {
        pSetting->current.isRandom = 1;
    }

    pSetting->current.rndRange = ena;

    if (!ena)
        return;

    pSetting->current.rndMin = MAX(rndMin, pSetting->minValue);
    pSetting->current.rndMax = MIN(rndMax, pSetting->maxValue);

    if (pSetting->marked && (applyToAll || particlesMarked()))
    {
        settingChanged = 1;

        applySettingToScreen (pSetting);
    }
}

int randomSetting (setting *pSetting, int random, int toArray)
{
    int i;

    if (toArray && pSetting->isArray)
    {
        for (i = 0; i < pSetting->id->instances; i++)
        {
            randomSetting (&pSetting->arrayFirst[i], random, 0);
        }
        return pSetting->current.isRandom;
    }

    if (!pSetting->canBeRandom)
    {
        pSetting->current.isRandom = 0;
    }
    else
    {
        pSetting->current.isRandom = random;
    }

    /* initially unmask all discrete values and don't define a random range */
    maskRandomValue (pSetting, 0, 0, 1);
    pSetting->current.rndRange = 0;

    if (pSetting->pButton)
    {
        button *b;
        if (pSetting->pButton->groupFirst)
        {
            for (b = pSetting->pButton->groupFirst; b != NULL; b = b->groupNext)
            {
                buttonSetRandom (b, pSetting->current.isRandom);
            }
        }
        else
        {
            buttonSetRandom (pSetting->pButton, pSetting->current.isRandom);
        }
    }

    if (pSetting->marked && (applyToAll || particlesMarked()))
    {
        applySettingToScreen (pSetting);
    }

    return pSetting->current.isRandom;
}

int markSetting (setting *pSetting, int mark)
{
    if (!pSetting->partAttr)
    {
        pSetting->marked = 0;
        return 0;
    }

    if (mark == -1)
        pSetting->marked = 1 - pSetting->marked;
    else
        pSetting->marked = mark;

    if (mark)
    {
        settingChanged = 1;
    }


    if (pSetting->pButton)
    {
        if (pSetting->pButton->groupFirst)
        {
            groupBorderColor (pSetting->pButton, pSetting->marked ? COL_RED : COL_ICONDRAW);
        }
        else
        {
            buttonBorderColor (pSetting->pButton, pSetting->marked ? COL_RED : COL_ICONDRAW);
        }
    }

    recordSetting (&playbackTape, pSetting, 1);

    if (pSetting->marked)
    {
#if 0
        if (!particlesMarked())
        {
            setApplyToAll (1);
        }
#endif

        if (applyToAll || particlesMarked())
        {
            applySettingToScreen (pSetting);
        }
    }

    return pSetting->marked;
}

int64_t getValue (setting *pSetting)
{
    settingStore_t *store;

    if (!pSetting->valid)
    {
        return 0;
    }

    if (key)
    {
        store = findStoredSetting (pSetting, key);
        if (!store)
            store = &pSetting->current;
    }
    else
    {
        store = &pSetting->current;
    }


    if (!store->isRandom)
        return store->val.va64;
    
    if (!pSetting->discrete || !store->rndMask)
    {
        if (!store->rndRange)
            return rand() % (pSetting->maxValue + 1 - pSetting->minValue) + pSetting->minValue;
        else
            return rand() % (store->rndMax + 1 - store->rndMin) + store->rndMin;
    }

    /* select a discreate random number. If all numbers are masked, provide the current 
     * setting value
     */
    if (store->numUnmasked == 0)
        return store->val.va64;

    return pSetting->rndMapArray[rand() % store->numUnmasked];
}

value_u *getValue_u (setting *pSetting)
{
    if (!pSetting->valid)
        return NULL;

    settingStore_t *store;

    if (key)
    {
        store = findStoredSetting (pSetting, key);
        if (!store)
            store = &pSetting->current;
    }
    else
    {
        store = &pSetting->current;
    }

    if (!store->isRandom)
    {
        return &store->val;
    }
    
    if (!pSetting->discrete || !store->rndMask)
    {
        return NULL;
    }

    /* select a discreate random number. If all numbers are masked, provide the current 
     * setting value
     */
    if (store->numUnmasked == 0)
        return &store->val;

    return (value_u*)&pSetting->rndMapArray[rand() % store->numUnmasked];
}


float getValueFlt (setting *pSetting)
{
    settingStore_t *store;

    if (key)
    {
        store = findStoredSetting (pSetting, key);
        if (!store)
            return 0;
    }
    else
    {
        store = &pSetting->current;
    }

    if (!store->isRandom)
        return store->floatVal;
    
    return (float)(rand() % (pSetting->maxValue + 1 - pSetting->minValue) + pSetting->minValue);
}

float getValueNorm (setting *pSetting)
{
    settingStore_t *store;

    if (key)
    {
        store = findStoredSetting (pSetting, key);
        if (!store)
            return 0;
    }
    else
    {
        store = &pSetting->current;
    }

    if (!store->isRandom)
        return store->normFloat;
    
    return (float)(rand() % (pSetting->maxValue + 1 - pSetting->minValue) + pSetting->minValue) / (float)pSetting->floatDivi;
}

#define RESET_TO_DEFAULT    1
#define TOGGLE_GLOBAL            2
#define SET_MIN                    3
#define SET_MAX                    4
#define TOGGLE_RANDOM            5
#define TOGGLE_ARRAY_MODE   6
#define SET_COLOR            7

void makeSettingPanel (button *b, setting *s)
{
    if (b->subPanel)
        return;

    makeSubPanel (b, P_DOWN_RIGHT, 1, SP_BUTTON3);

    if (s->colorSelection)
        addButtonColorHandler (b, 0);
 
    addSubButton (b, 50,30, default_pm, "Reset to Default", (intptr_t)RESET_TO_DEFAULT, B_NEXT);
    if (((s->maxValue - s->minValue) / s->minDiff) > 2)
    {
        addSubButton (b, 50,20, minval_pm, "Set to Min", SET_MIN, B_NEXT);
        addSubButton (b, 50,20, maxval_pm, "Set to Max", SET_MAX, B_NEXT);
    }

    if (s->canBeRandom)
    {
        addSubButton (b, 50,20, bigrandom_pm, "Toggle Random (Random value is used)", (intptr_t)TOGGLE_RANDOM, B_NEXT);
    }

    if (s->partAttr)
    {
        addSubButton (b, 50,20, makeGlobal_pm, "Toggle Global Flag (Apply to selected particles immedeately)", (intptr_t)TOGGLE_GLOBAL, B_NEXT);
    }

    if (s->isArray)
    {
        addSubButton (b, 50,20, array_pm, "Toggle Array Mode (Apply to all particle types)", (intptr_t)TOGGLE_ARRAY_MODE, B_NEXT);
    }
}

void addButtonColorHandler (button *b, uint32_t col)
{
    addSubButton (b, 50,20, colors_xpm, "COLOR", (intptr_t)SET_COLOR, B_NEXT);

    b->bgColorSetting = calloc (1, sizeof(setting));
    b->bgColorSetting->name = "Color";
    b->bgColorSetting->minValue = 0;
    b->bgColorSetting->maxValue = 0;
    b->bgColorSetting->defaultValue = col;
    b->bgColorSetting->cb = (FUNCPTR)buttonColorBg;
    b->bgColorSetting->pButton = b;
    b->bgColorSetting->partAttr = 1;

    registerInternalSetting (b->bgColorSetting);
}

int handleSettingEvent (button *b, int event)
{
    setting *s = b->parentButton->pSetting;

    if (event != SUBPANEL_SELECT)
    {
        return 0;
    }

    switch (b->userData)
    {
        case SET_COLOR:
            if (b->parentButton->bgColorSetting)
            {
                pickColor (b->parentButton->bgColorSetting);
            }

            break;

        case TOGGLE_ARRAY_MODE:
            *b->parentButton->applyToArray ^= 1;
                
            if (*b->parentButton->applyToArray)
            {
                cloneSettingToArray (s);
            }
            break;

        case RESET_TO_DEFAULT:
                modifySetting (s, s->defaultValue, *b->parentButton->applyToArray);
                return 2;

        case TOGGLE_GLOBAL:
                markSetting (s, 1-s->marked);

                if (b->groupFirst)
                {
                    groupBorderColor (b, s->marked ? COL_RED : COL_ICONDRAW);
                }
                else
                {
                    buttonBorderColor (b, s->marked ? COL_RED : COL_ICONDRAW);
                }
                return 2;
                
        case SET_MIN:
                modifySetting (s, s->minValue, *b->parentButton->applyToArray);
                return 2;

        case SET_MAX:
                modifySetting (s, s->maxValue, *b->parentButton->applyToArray);
                return 2;

        case TOGGLE_RANDOM:
                randomSetting (s, 1 - s->current.isRandom, *b->parentButton->applyToArray);
                return 2;
    }

    return 1;
}

void applySettingToPixel (screeninfo *pSi, setting *s)
{
    if (pSi->particleList != SCR_BRICK)
        return;

    if (s == &wallFriction)
    {
        FRICTION(pSi) = getValue (s);
        if (pSi->flags & SI_BOUNDARY)
            setPixel (SI2X(pSi), SI2Y(pSi), BOUNDARY_COL(pSi));
    }
    else if ((s == &particleWeight[wallArrayIndex]) ||
             (s == &particleAttractionForce[wallArrayIndex]))
    {
        SET_WALL_DENS_DFL(pSi);
    }
}

void applyWallSettingsToPixel (screeninfo *pSi)
{
    if (pSi->particleList != SCR_BRICK)
        return;

    if (wallFriction.marked)
    {
        FRICTION(pSi) = getValue (&wallFriction);
        if (pSi->flags & SI_BOUNDARY)
            setPixel (SI2X(pSi), SI2Y(pSi), BOUNDARY_COL(pSi));
    }

    if (particleWeight[wallArrayIndex].marked ||
        particleAttractionForce[wallArrayIndex].marked)
    {
        SI_CHARGE(pSi) = WALL_CHARGE;
    }
}

int walked = 0;
static int applySettingsCb (particle *p, intptr_t arg)
{
    screeninfo *pSi;
    int marked = particlesMarked();
    int i;
    setting *s;
    settingId *si;

    pSi = GET_SCREENINFO(p);

    if (marked && !pIsMarked(p))
        return 1;

    if ((pSi->flags & SI_MARKER) == 0)
        return 1;

    for (si = settingsList; si->name != NULL; si++)
    {
        for (i = 0; i < si->instances; i++)
        {
            s = &si->pSetting[i];

            if (!s->marked)
                continue;

            SI_CHARGE(pSi) -= PCHARGE(p);

            applySettingToParticle (p, s);

            SI_CHARGE(pSi) += PCHARGE(p);
        }
    }
#if 0
    RESTORE_PARTICLE_COLOR (p);
#endif

    return 1;
}

int applySettingsToMarkedCb (intptr_t arg)
{
    screeninfo *pSi = (screeninfo*)arg;
    int dens;
    particle *p;
    setting *s;
    settingId *si;
    int i;

    if (pSi->particleList == SCR_BRICK)
    {
        for (si = settingsList; si->name != NULL; si++)
        {
            for (i = 0; i < si->instances; i++)
            {
                s = &si->pSetting[i];

                if (!s->marked)
                    continue;

                applySettingToPixel (pSi, s);
            }
        }
    }
    else
    {
        if (siMarkedHasWimps() || (pSi->particleList <= SCR_BRICK))
            return 1;

        dens = 0;
        for (p = pSi->particleList; p > SCR_BRICK; p = p->next)
        {
            for (si = settingsList; si->name != NULL; si++)
            {
                for (i = 0; i < si->instances; i++)
                {
                    s = &si->pSetting[i];

                    if (!s->marked)
                        continue;

                    SI_CHARGE(pSi) -= PCHARGE(p);
                    
                    applySettingToParticle (p, s);

                    dens += PCHARGE(p);
                }
            }
            RESTORE_PARTICLE_COLOR (p);
        }
        SI_CHARGE(pSi) = dens;
    }

    return 1;
}

void applySettingsToMarked (int x, int y)
{
    if (siMarkedHasWimps())
    {
        walkAllParticles ((FUNCPTR)applySettingsCb, (intptr_t)0);
    }

    siWalkMarked ((FUNCPTR)applySettingsToMarkedCb, 0);

    /* tell GPU to change the marked attributes of all particles on marked pixels
     */
    clParticleChattrSelected (x, y);
}

typedef struct 
{
    vec_t tl, br;
    int all;
    setting *s;
    int marked;
    int count;
} sApply_s;

static int applySettingCb (particle *p, intptr_t arg)
{
    sApply_s        *sa = (sApply_s*)arg;
    int                 i;
    screeninfo        *pSi;
    setting *s;
    settingId *si;

    if ((p->pos.d.x < sa->tl.d.x) || (p->pos.d.x > sa->br.d.x)
        || (p->pos.d.y < sa->tl.d.y) || (p->pos.d.y > sa->br.d.y))
        return 1;

    if (sa->marked && !pIsMarked(p))
        return 1;

    pSi = GET_SCREENINFO(p);

    SI_CHARGE(pSi) -= PCHARGE(p);

    if (sa->all)
    {
        for (si = settingsList; si->name != NULL; si++)
        {
            for (i = 0; i < si->instances; i++)
            {
                s = &si->pSetting[i];

                if (!s->marked)
                    continue;

                applySettingToParticle (p, s);
                sa->count++;
            }
        }
    }
    else
    {
        applySettingToParticle (p, sa->s);
        sa->count++;
    }

    SI_CHARGE(pSi) += PCHARGE(p);
#if 0
    RESTORE_PARTICLE_COLOR (p);
#endif

    return 1;
}

void applySettingToScreen (setting *s)
{
    SDL_Rect r;
    sApply_s sa;
    int x, y;
    screeninfo *pSi;

    r.x = 0;
    r.y = 0;
    r.w = wid;
    r.h = hei;

    getSelection (&r);

    if (s->wallSetting)
    {
        for (y = r.y; y < r.y + r.h; y++)
        {
            pSi = &siArray[r.x + y * wid];

            for (x = r.x; x < r.x + r.w; x++, pSi++)
            {
                applySettingToPixel (pSi, s);
            }
        }
    }
    else
    {
        sa.tl.d.x = INTERNC(r.x);
        sa.tl.d.y = INTERNC(r.y);
        sa.br.d.x = INTERNC(r.x + r.w)-1;
        sa.br.d.y = INTERNC(r.y + r.h)-1;
        sa.s = s;
        sa.all = 0;
        sa.marked = particlesMarked();

        walkAllParticles ((FUNCPTR)applySettingCb, (intptr_t)&sa);
#ifdef USE_OPENCL
        clApplyParticleSetting (s, &r);
        clCommitJobs();
#endif
    }

}

void applyAllSettingsToScreen (void)
{
    SDL_Rect r;
    screeninfo *pSi;
    int x,y;
    sApply_s sa;

    r.x = 0;
    r.y = 0;
    r.w = wid;
    r.h = hei;

    getSelection (&r);
    sa.tl.d.x = INTERNC(r.x);
    sa.tl.d.y = INTERNC(r.y);
    sa.br.d.x = INTERNC(r.x + r.w)-1;
    sa.br.d.y = INTERNC(r.y + r.h)-1;
    sa.all = 1;
    sa.marked = particlesMarked();

    walkAllParticles ((FUNCPTR)applySettingCb, (intptr_t)&sa);

    for (y = r.y; y < r.y + r.h; y++)
    {
        pSi = &siArray[r.x + y * wid];

        for (x = r.x; x < r.x + r.w; x++, pSi++)
        {
            applyWallSettingsToPixel (pSi);
        }
    }
}

void cloneSettingToArray (setting *pSetting)
{
    int i;
    setting *s;

    for (i = 0; i < pSetting->id->instances; i++)
    {
        s = &pSetting->arrayFirst[i];

        if (s == pSetting)
            continue;
        
        if (pSetting->current.isRandom)
        {
            randomSetting (s, 1, 0);

            s->current.rndRange = pSetting->current.rndRange;
            s->current.rndMin = pSetting->current.rndMin;
            s->current.rndMax = pSetting->current.rndMax;

            memcpy ((char*)s->rndMapArray, (char*)pSetting->rndMapArray, sizeof(s->rndMapArray));
            s->current.rndMask = pSetting->current.rndMask;
            s->current.numUnmasked = pSetting->current.numUnmasked;

            applySettingToScreen (s);
        }
        else
        {
            modifySetting (s, pSetting->current.val.va64, 0);
        }
    }
}

void modifySetting (setting *pSetting, int64_t newValue, int toArray)
{
    int i;

    if (toArray && pSetting->isArray)
    {
        for (i = 0; i < pSetting->id->instances; i++)
        {
            modifySetting (&pSetting->arrayFirst[i], newValue, 0);
        }
        return;
    }


    if (newValue == pSetting->current.val.va64)
        return;

    if (pSetting->current.isRandom)
        randomSetting (pSetting, 0, 0);

    pSetting->current.val.va64 = newValue;

    pSetting->current.floatVal = (float)newValue;


    if (pSetting->floatDivi)
        pSetting->current.normFloat = (float)newValue / (float)pSetting->floatDivi;

    if (pSetting->cb)
        pSetting->cb (pSetting);

    if (pSetting->marked && (applyToAll || particlesMarked()))
    {
        settingChanged = 1;

        applySettingToScreen (pSetting);
    }

    recordSetting (&playbackTape, pSetting, 0);
    broadcastSetting (pSetting);

    if (pSetting->pButton)
    {
        redrawButton (pSetting->pButton);
    }

    pSetting->prevValue = pSetting->current.val.va64;
}

void cbApplyToAll (setting *s)
{
    applyToAll = getValue (s);

    if (applyToAll)
    {
        applyAllSettingsToScreen();
    }
}


settingStore_t *findStoredSetting (setting *s, void *key)
{
    settingStore_t *sst;

    for (sst = s->store; sst != NULL; sst = sst->next)
    {
        if (sst->key == key)
        {
            debug ("%s/%d: using key %p, value 0x%08llx\n", s->name, s->index, key, (int64_t)sst->val.va64 );
            return sst;
        }
    }
        
    return NULL;
}

void freezeSetting (setting *s, void *key)
{
    settingStore_t *sst = findStoredSetting (s, key);

    if (!sst)
        sst = calloc (sizeof(*sst), 1);

    *sst = s->current;

    sst->key = key;
    
    sst->next = s->store;
    s->store = sst;

    debug ("Freezing %s/%d using key %p (value 0x%016llx)\n", s->name, s->index, key, (int64_t unsigned int)sst->val.va64 );
}

void freezeParticleSettings (void *key)
{
    setting *s;

    for (s = allSettings; s != NULL; s = s->next)
    {
        if (s->partAttr == 0)
            continue;

        freezeSetting (s, key);
    }
}

void removeFrozenSet (void *key)
{
    setting *s;
    settingStore_t *sst, *prev;

    for (s = allSettings; s != NULL; s = s->next)
    {
        prev = NULL;
        for (sst = s->store; sst != NULL; prev = sst, sst = sst->next)
        {
            if (sst->key == key)
            {
                if (prev == NULL)
                    s->store = sst->next;
                else
                    prev->next = sst->next;

                free (sst);

                break;
            }
        }
    }
}

void useFrozenSet (void *useKey)
{
    key = useKey;

    debug ("Using key %p\n", useKey);

}
