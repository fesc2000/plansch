/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#include "SDL/SDL_mouse.h"

static int partsPerPixel = 1;


void drawFctConvert  (int x, int y, int action, int mode);
void drawFctParticle (int x, int y, int action, int mode);
void drawFctWall     (int x, int y, int action, int mode);
void drawFctProbe    (int x, int y, int action, int mode);
void drawFctClear    (int x, int y, int action, int mode);
void drawFctPush     (int x, int y, int action, int mode);
void drawFctGrav     (int x, int y, int action, int mode);
void drawFctPipe     (int x, int y, int action, int mode);
void drawFctPump     (int x, int y, int act, int mode);
void drawFctStencil  (int x, int y, int action, int mode);
void drawFctSelector (int x, int y, int act, int mode);
void drawFctMove     (int x, int y, int act, int mode);
void drawFctMark     (int x, int y, int act, int mode);
void drawFctPaint    (int x, int y, int act, int mode);
void drawFctDecel    (int x, int y, int act, int mode);

void drawOpVecArea (int x, int y, int act, int mode);

static void pushDrawAccel (int x, int y);

int vecAreaCut = 0;
vec_t lastBrick;
int pumpMode = 0;
vec_t pumpVec;
int randomPlacement = 1;
int overwrite = 1;

int moveDefinesImpuls = 0;
int chainParticles = 0;

void pushClear (int x, int y, int updateNormals);

vec_t probePosition;

static int haveAccelVec = 0;
static vec_t accelVec;

static void *pushDrawCallout = NULL;
static vec_t currentPushPos;

#define PAINT_PARTICLES 0
#define PAINT_WALL 1

int drPolySize = 0;
vec_t drPoly[1000];
scrPoly *dp = NULL;

scrPoly *circlePolys[2];

typedef struct 
{
    int            internc;        /* 1 if draw fct expects internal coords */
    int            id;
    FUNCPTR drawFct;
    int            stencilMask;
    int     dflSten;
    int            drawModeMask;
    int     dflMode;
    FUNCPTR progressFct;
} drawOp;

#define POSITION_CACHE_SZ        1000
static int positionCacheMax = 0;
static int positionCacheSize = 0;
static struct
{
    vec_t pos;
    int size;
    int rad;
} *positionCache = NULL;


typedef struct 
{
    int bucketMax, bucketSize;
    int *bucket;
} posHash_t;

static posHash_t *posHashBuckets = NULL;

static drawOp *currentDrawOp = NULL;

drawOp drawOps[] =
{
    { 0, DO_WALL,             (FUNCPTR)drawFctWall,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },
    { 1, DO_PARTICLES,  (FUNCPTR)drawFctParticle,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 0, DO_STENCIL,    (FUNCPTR)drawFctStencil,
            0,             ST_PIXEL, (1<<DR_STAMP), DR_STAMP },    
    { 0, DO_ERASE_WALL, (FUNCPTR)drawFctClear,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 0, DO_ERASE_PAR,  (FUNCPTR)drawFctClear,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 0, DO_ERASE_PUMP, (FUNCPTR)drawFctClear,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 0, DO_PIPE,       (FUNCPTR)drawFctPipe,
            (1<<ST_VSMALLBALL)|(1<<ST_SMALLBALL)|(1<<ST_BALL), ST_BALL, ~(1<<DR_FLOOD), DR_DRAW },    
    { 0, DO_PIPE_ACCEL, (FUNCPTR)drawFctPipe,
            (1<<ST_PIXEL)|(1<<ST_VSMALLBALL)|(1<<ST_SMALLBALL)|(1<<ST_BALL), ST_BALL, ~(1<<DR_FLOOD), DR_DRAW },    
    { 0, DO_PUMP,       (FUNCPTR)drawFctPump,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 0, DO_PUSH,       (FUNCPTR)drawFctPush,
            STEN_ALL_MASK, ST_BALL, (1<<DR_DRAW), DR_DRAW },    
    { 0, DO_CONVERT,    (FUNCPTR)drawFctConvert,
           STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 1, DO_SELECTOR,    (FUNCPTR)drawFctSelector,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 1, DO_MOVE,             (FUNCPTR)drawFctMove,
            STEN_ALL_MASK, ST_BALL, 1<<DR_DRAW, DR_DRAW },    
    { 0, DO_WALL_POLY,  (FUNCPTR)drawOpVecArea,
            (1<<ST_PIXEL), ST_PIXEL, (1<<DR_DRAW)|(1<<DR_STAMP), DR_STAMP},    
    { 0, DO_WALL_AUTOPOLY,     (FUNCPTR)drawFctWall,
            STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW, },
//    { DO_PROBE,      (FUNCPTR)drawFctProbe, STEN_ALL_MASK, DR_ALL_MASK },    
    { 0, DO_MARK,      (FUNCPTR)drawFctMark, STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 1, DO_PAINT,      (FUNCPTR)drawFctPaint, STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { 1, DO_DECEL,      (FUNCPTR)drawFctDecel, STEN_ALL_MASK, ST_BALL, DR_ALL_MASK, DR_DRAW },    
    { -1 }
};



static int cDrawing = 0;
static vec_t cCenter;
static int cRx, cRy;
static int cRound;
static int cTx, cTy;
//int cOffX, cOffY;


int pcDropped;
void clearPosCache()
{
    int i;
    positionCacheSize = 0;
    pcDropped = 0;

    if (posHashBuckets)
    {
        for (i = 0; i < hei; i++)
        {
            posHashBuckets[i].bucketSize = 0;
        }
    }
}

/* add a position to the position cache. We assume that we add a particle at
 * position x/y. 
 * The "cache" is a simple, unsorted list of coordinates.
 * If a particle at the given position overlaps with at least one other
 * particle in the position cache, it's not added.
 * To make this check efficient, a has table is used. The hash key is the y
 * position in screen coordinates. Each hash bucket keeps a list of positions
 * sorted in x coordinates.
 */
 
void addToPosCache (int x, int y)
{
    int i, j;
    int size = getValue (&particleSize[GETVAL32(ptype)]);
    int rad = (size << RADIUS_SHIFT);
    int64_t d;
    int sy = (y + cCenter.d.y) / GRID;
    int sx = (x + cCenter.d.x) / GRID;
    int idx;
    posHash_t *bu;

    if ((unsigned)sy > hei-1)
        return;
    if ((unsigned)sx > wid-1)
        return;

    if (posHashBuckets == NULL)
    {
        posHashBuckets = calloc(hei, sizeof(*posHashBuckets));
    }

    if (positionCache == NULL)
    {
        positionCacheMax = POSITION_CACHE_SZ;
        positionCache = malloc(positionCacheMax * sizeof(*positionCache));
        positionCacheSize = 0;
    }

    if (positionCacheSize >= positionCacheMax)
    {
        positionCacheMax += POSITION_CACHE_SZ;
        positionCache = realloc (positionCache, positionCacheMax * sizeof(*positionCache));
    }

    for (i = MAX(0, sy-1); i <= MIN(hei-1, sy+1); i++)
    {
        bu = &posHashBuckets[i];
        for (j = 0; j < bu->bucketSize; j++)
        {
            idx = bu->bucket[j];

            d = (int64_t)(x - positionCache[idx].pos.d.x) * (int64_t)(x - positionCache[idx].pos.d.x) +
                (int64_t)(y - positionCache[idx].pos.d.y) * (int64_t)(y - positionCache[idx].pos.d.y);

            if (d <= (int64_t)(positionCache[idx].rad + rad) * (int64_t)(positionCache[idx].rad + rad))
            {
                pcDropped++;
                return;
            }
        }
    }

    positionCache[positionCacheSize].pos.d.x = x;
    positionCache[positionCacheSize].pos.d.y = y;
    positionCache[positionCacheSize].size = size;
    positionCache[positionCacheSize].rad = rad;

    bu = &posHashBuckets[sy];
    if (bu->bucket == NULL)
    {
        bu->bucketMax = 50;
        bu->bucket = malloc(bu->bucketMax * sizeof(int));
        bu->bucketSize = 0;
    }
    else if (bu->bucketSize >= bu->bucketMax)
    {
        bu->bucketMax += 50;
        bu->bucket = realloc(bu->bucket, bu->bucketMax * sizeof(int));
    }
#if 0
    /* insert sorted */
    for (i = 0; i < bu->bucketSize; i++)
    {
        idx = bu->bucket[i];
        if (SCREENC(positionCache[idx].pos.d.x) >= sx)
        {
            memmove (&bu->bucket[i+1], &bu->bucket[i], (bu->bucketSize - i) * sizeof(int));
            break;
        }
    }
    bu->bucket[i] = positionCacheSize;
#else
    bu->bucket[bu->bucketSize] = positionCacheSize;
#endif

    bu->bucketSize++;
    positionCacheSize++;
}


void initDrPoly()
{
    drPolySize = 0;
    if (!dp)
        dp = polyNew (1000, POLY_OPEN, COL_LINE);
    polyClear (dp);
}

void clearDrPoly()
{
    polyHide (dp);
}

void convertDrPolyToRc()
{
    int i;

    for (i = 0; i < drPolySize; i++)
    {
        drPoly[i].d.x = S_SCREENC(drPoly[i].d.x);
        drPoly[i].d.y = S_SCREENC(drPoly[i].d.y);
    }
}

int polyLast (int *px, int *py)
{
    if (!drPolySize)
        return 0;

    *px = drPoly[drPolySize-1].d.x;
    *py = drPoly[drPolySize-1].d.y;

    return 1;
}

void polyEscape ()
{
    initDrPoly();
    motionCapture (0);
}

void addToDrPoly(int x, int y)
{
    if (drPolySize >= NELEMENTS(drPoly))
        return;

//    if ((x == drPoly[drPolySize-1].d.x) && (y == drPoly[drPolySize-1].d.y))
//        rc = 1;

    drPoly[drPolySize].d.x = x;
    drPoly[drPolySize].d.y = y;

    polyAddCoord (dp, x, y, 0);
    polyShow (dp);

    drPolySize++;

    escConnect ((FUNCPTR)polyEscape);

}

int polyLastIdentical()
{
    if (drPolySize < 2)
        return 0;

    if ((drPoly[drPolySize-1].d.x == drPoly[drPolySize-2].d.x) &&
        (drPoly[drPolySize-1].d.y == drPoly[drPolySize-2].d.y))
        return 1;

    return 0;

}

void polySetLast (int x, int y)
{
    if (drPolySize == 0)
        return;

    drPoly[drPolySize-1].d.x = x;
    drPoly[drPolySize-1].d.y = y;

    polyChangeCoord (dp, drPolySize-1, x, y, 0);
    polyShow (dp);
}



void clearMarker (int x1, int y1, int x2, int y2)
{
    int x,y;
    screeninfo *pSi;

    x1 = MAX(0, x1);
    y1 = MAX(0, y1);
    x2 = MIN(wid-1, x2);
    y2 = MIN(hei-1, y2);

    for (y = y1; y <= y2; y++)
    {
        pSi = &siArray[x1 + y*wid];

        for (x = x1; x <= x2; x++, pSi++)
        {
            pSi->flags &= ~SI_MARKER;
        }
    }
}

void clearPipeStartMarker (int x1, int y1, int x2, int y2)
{
    int x,y;
    screeninfo *pSi;

    x1 = MAX(0, x1);
    y1 = MAX(0, y1);
    x2 = MIN(wid-1, x2);
    y2 = MIN(hei-1, y2);

    for (y = y1; y <= y2; y++)
    {
        pSi = &siArray[x1 + y*wid];

        for (x = x1; x <= x2; x++, pSi++)
        {
            if (pSi->flags & SI_MARKER)
                pSi->flags &= ~(SI_IS_PIPE|SI_MARKER);
        }
    }
}


void setPartsPerPixel (int parts)
{
    partsPerPixel = parts;
}

int
np (int ix, int iy, int vx, int vy, int size)
{
    particle *p;
    int offx, offy;
    int grid = GRID / zoom;
    int xo, yo;
    int i;
    int even = 0;
    vec_t pos;
    int phint = PHINT_RANDOM;
    int rc = 0;
    int pcount = partsPerPixel;
    int sx = SCREENC(ix);
    int sy = SCREENC(iy);
    int noSize = pdescs[getValue (&ptype)]->noSize;

    if (!inSelection(sx,sy))
    {
        return 0;
    }

    if (pdescs[GETVAL32(ptype)]->placementHint)
        phint = pdescs[GETVAL32(ptype)]->placementHint();

    if ((sx < 0) || (sx >= wid) || (sy < 0) || (sy >= hei))
    {
        return 0;
    }

    if (SCR_CONTENT (sx, sy) == SCR_BRICK)
    {
        return 0;
    }

    if (siArray[sx + sy*wid].flags & (SI_IN_POLY|SI_LINE))
        return 0;

    if (noSize)
    {
        ix = INTERNC(sx) + GRID/2;
        iy = INTERNC(sy) + GRID/2;

        if (hasNewParticle (&siArray[sx + sy*wid]))
            return 0;
    }
    else if (pdescs[GETVAL32(ptype)]->isWimp == 0)
    {
        if (nearestNewParticle (ix, iy, CURRENT_RADIUS))
            return 0;
    }


    if ((currentDrawMode() == DR_CIRCLE) || (currentDrawMode() == DR_CIRCLE_FILL))
    {
        p = newParticle (ix, iy, vx, vy);

        if (!p)
            return 0;
        
        p->size = size;

        if (!isDummy(p))
            setNewParticle (p);

        return 1;

    }

#if 0
    if (!overwrite && (SCR_CONTENT (sx, sy) != NULL))
    {
        return 0;
    }
    else if ((strcmp (pdescs[GETVAL32(ptype)]->name, "Blob") || floodOngoing()) && hasNewParticle (&siArray[sx + sy*wid]))
    {
        return 0;
    }
#endif

    if (floodOngoing() && hasNewParticle (&siArray[sx + sy*wid]))
        return 0;

//    ix = INTERNC(sx);
//    iy = INTERNC(sy);

    offx = grid * (mousePos.d.x % zoom);

    if (phint == PHINT_RANDOM)
    {
        for (i = 0; i < pcount; i++)
        {
            offy = grid * (mousePos.d.y % zoom);

            offx += RND (grid);
            offy += RND (grid);

            offx &= MAX_SPEED;
            offy &= MAX_SPEED;

            /* XXX */
            offx = offy = 0;

            p = newParticle (ix + offx, iy + offy, vx, vy);

            if (!p)
                continue;

            p->size = size;

            rc = 1;

            if (!isDummy(p))
                setNewParticle (p);
        }
        
        return rc;
    }

    if (phint == PHINT_FIXED1)
    {
        pcount = 1;
    }

    if (pcount == 1)
    {
        pos.d.x = ALIGN_IC(ix) + (GRID/3) * ((sy & 1) + 1);
        pos.d.y = ALIGN_IC(iy) + GRID/2;

        p = newParticle (pos.d.x, pos.d.y, vx, vy);

        if (p)
        {
            p->size = size;
            if (!isDummy(p))
                setNewParticle (p);
        }
    }
    else
    {
        int divi = sqrt(partsPerPixel);

        yo = GRID / (divi + 1);
        xo = (GRID / (divi * 3)) * (even ? 1 : 2);

        for (i = 0; i < pcount; i++)
        {
            pos.d.x = ix + xo;
            pos.d.y = iy + yo;

            if ((i+1) % divi == 0)
            {
                even = 1 - even;
                xo = (GRID / (divi * 3)) * (even ? 1 : 2);
                yo += GRID / divi;
            }
            else
            {
                xo += GRID/divi;
            }

            p = newParticle (pos.d.x, pos.d.y, vx, vy);

            if (!p)
                continue;

            p->size = size;

            rc = 1;

            if (!isDummy(p))
                setNewParticle (p);
        }
    }

    return rc;
}

void doDrawParticles (int ix, int iy, int ldx, int ldy, int a1, int a2)
{
    int xx, yy;
    int px, py;
    vec_t vec;
    int vx, vy;
    int a;
    int i;
    int pt = getValue(&ptype);
    int rx, ry;

    getArrowVec (&vec.d.x, &vec.d.y);
    a = getArrowAngle();

    rx = ry = 0;

    if (positionCacheSize)
    {
        for (i = 0; i < positionCacheSize; i++)
        {
            if (randomPlacement)
            {
                rx = rand() & 2;
                ry = rand() & 2;
            }

            makeParticleVec (&vec, a, &vx, &vy);
            np ((ix + positionCache[i].pos.d.x) ^ rx,
                (iy + positionCache[i].pos.d.y) ^ ry,
                vx, vy, positionCache[i].size);
        }
    }
    else
    {
        for (xx = 0; xx < 32; xx++)
        {
            for (yy = 0; yy < 32; yy++)
            {
                if (stencil[3 + yy][xx] == ' ')
                    continue;

                px = ix + INTERNC(xx - 16 + CORR_X);
                py = iy + INTERNC(yy - 16 + CORR_Y);

                if ((px < 0) || (px >= INTERNC(wid)) || (py < 0) || (py >= INTERNC(hei)))
                    continue;

                makeParticleVec (&vec, a, &vx, &vy);

                if (randomPlacement)
                {
                    px ^= rand() & 2;
                    py ^= rand() & 2;
                }
                np (px, py, vx, vy, getValue (&particleSize[pt]));
            }
        }
    }
#ifdef USE_OPENCL
    clCommitJobs();
#endif
}

void drawFctProbe (int x, int y, int act, int mode)
{
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    CHECK_DRAWING

    if (mouseButton != 1)
        return;

    if ((event == BUTTON_PRESS) || (event == BUTTON_MOVE))
    {
        probePosition.d.x = x;
        probePosition.d.y = y;

        probeShow();
        probeClear();
        probeStart(1);
        probe(x,y);
    }

#ifdef PROBE
    if (event == BUTTON_RELEASE)
    {
        buttonColor (buttonProbe, COL_ORANGE, 0);
        probeRestoreActionButton ();
    }
#endif

}


int floodFctParticle (int x, int y)
{
    if (numParticles >= options->maxParticles)
        return -1;

    if (pdescs[GETVAL32(ptype)]->canCreate)
    {
        if (!pdescs[GETVAL32(ptype)]->canCreate(INTERNC(x), INTERNC(y)))
        {
            return 0;
        }
    }

    if (np (INTERNC(x), INTERNC(y), 0, 0, getValue (&particleSize[GETVAL32(ptype)])) == 0)
    {
        return 0;
    }

    return 1;
}

void makeParticleVec (vec_t *vec, int angle, int *dx, int *dy)
{
    FLOAT vecLen;
    FLOAT nx, ny;
    FLOAT a, rd;
    FLOAT fa = (FLOAT)angle / 360.0;

    if ((vec->d.x == 0) && (vec->d.y == 0))
    {
        *dx = *dy = 0;
        return;
    }

    vecLen = VEC_LEN(vec->d.x, vec->d.y);
    nx = (FLOAT)vec->d.x / vecLen;
    ny = (FLOAT)vec->d.y / vecLen;

    if (vecLen > 400.0)
        vecLen = 400.0;

    vecLen = (vecLen * MAX_SPEEDF) / 400.0;

    if (angle == 0)
    {
        *dx = nx * vecLen;
        *dy = ny * vecLen;

        return;
    }

    rd = ((FLOAT)rand() / (FLOAT)RAND_MAX) * fa - fa/2; 

    a = rads (nx, ny) + rd * 2 * M_PI;

    *dx = (int)(cos (a) * vecLen);
    *dy = (int)(-sin (a) * vecLen);
}

void
drawFctParticle (int ix, int iy, int act, int mode)
{
    static int startX, startY, lastSX, lastSY;
    static int wellStarted = 0;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    int sx = SCREENC(ix);
    int sy = SCREENC(iy);
    int interval = pdescs[getValue (&ptype)]->drawInterval ? pdescs[getValue (&ptype)]->drawInterval() : 0;

    if (interval == 0)
            interval = GRID;

    if (event == SCROLLUP_SH)
    {
        setArrowAngle (getArrowAngle() + 10);
        return;
    }
    else if (event == SCROLLDOWN_SH)
    {
        setArrowAngle (getArrowAngle() - 10);
        return;
    }

    CHECK_DRAWING

    if (floodMode())
    {
        if ((mouseButton == 1) && (event == BUTTON_RELEASE))
        {
            PTYPE_START_DRAW;

            flood (sx, sy, (FUNCPTR)floodFctParticle, 1, 0);

#ifdef USE_OPENCL
            clCommitJobs();
#endif
            PTYPE_STOP_DRAW(1);
        }

        return;
    }

    if (mouseButton == 2)
    {
        return;
    }

    if (mouseButton == 3)
    {
        if (event == BUTTON_PRESS)
        {
            startX = lastSX = mousePos.d.x;
            startY = lastSY = mousePos.d.y;

            resetArrow (startX, startY);

            return;
        }

        if (event == BUTTON_MOVE)
        {
            lastSX = mousePos.d.x;
            lastSY = mousePos.d.y;

            updateArrow (lastSX, lastSY);

            return;
        }


        if (event == BUTTON_RELEASE)
        {
            lastSX = mousePos.d.x;
            lastSY = mousePos.d.y;

            fixArrow();
            updateArrow (lastSX, lastSY);
        }

        getArrowVec (&selectVector.d.x, &selectVector.d.y);

        return;
    }

    if (event == BUTTON_PRESS)
    {
        PTYPE_START_DRAW;
        wellStarted = 0;

        switch (mode)
        {
            case DR_LINE:
                startX = ix;
                startY = iy;
                break;

            case DR_DRAW:
                if (ctrlPressed())
                {
                    wellStarted = 1;
                    createWell (ix, iy);
                    break;
                }
                startX = ix;
                startY = iy;
                break;

            case DR_STAMP:
                if (ctrlPressed())
                {
                    wellStarted = 1;
                    createWell (ix, iy);
                    break;
                }

                doDrawParticles (ix, iy, 0, 0, 0, 0);
                break;
            case DR_FLOOD:
                return;
        }
    }

    if ((event == BUTTON_MOVE) && !wellStarted)
    {
        switch (mode)
        {
            case DR_LINE:
                interval = GRID;
            case DR_DRAW:
                if ((startX == ix) && (startY == iy))
                    doDrawParticles (ix, iy, 0, 0, 0, 0);
                else
                    drawLineIF (startX, startY, ix, iy, interval, (FUNCPTR)doDrawParticles, 0,0,0,0); 
                startX = ix;
                startY = iy;
                break;

            case DR_STAMP:
                return;
            case DR_FLOOD:
                return;
        }
    }
    if (event == BUTTON_RELEASE)
    {
        if (wellStarted)
        {
            completeWell ();
            wellStarted = 0;

            return;
        }

        clearNewParticleList();

        PTYPE_STOP_DRAW(1);

        return;
    }


    lastSX = sx;
    lastSY = sy;
}

int
doHeat (int x, int y, int dx, int dy)
{
    int xx, yy;
    int px, py;
    screeninfo *nSi;

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei) || !inSelection(px,py))
                continue;

            nSi = &siArray[px + py*wid];

            if (nSi->flags & SI_BRICK)
            {
                SI_SET_TEMPR(nSi, 0xff * (getValue(&particleWeight[wallArrayIndex])-1));

#ifndef USE_OPENCL
#if defined(ZOOM) && !defined(USE_OPENGL)
                {
                    SDL_Rect rect;
                    unsigned int sx, sy;

                    rect.w = rect.h = zoom;

                    sx = (px - zoomx) * zoom;
                    sy = (py - zoomy) * zoom;

                    if ((sx <= wid-zoom) && (sy <= hei-zoom))
                    {
                        rect.x = sx;
                        rect.y = sy;
                        SDL_FillRect (screen, &rect, WALL_COL(TEMPR(nSi), WALL_MASS_GET(nSi)));
                    }
                }
#else  /* ZOOM */
                SCR_SET(px, py, WALL_COL(TEMPR(nSi),  WALL_MASS_GET(nSi)));
#endif /* ZOOM */
#endif /* USE_OPENCL */
            }
        }

#ifdef USE_OPENCL
        clCommitJobs();
#endif

    return 1;
}

int
doMark (int x, int y, int dx, int dy)
{
    int xx, yy;
    int px, py;
    screeninfo *pSi;

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei) || !inSelection(px,py))
                continue;

            pSi = &siArray[px + py*wid];

            siMark (pSi);
        }

    return 1;
}

int
doDrawWall (int x, int y, int dx, int dy)
{
    int xx, yy;
    int px, py;

    if (!pumpMode)
        pushDraw (x, y, 0);

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei))
                continue;

            if (pumpMode)
            {
                setPump (px, py, &pumpVec);
            }
            else
            {
                if (vecAreaCut && ((siArray[px + py*wid].flags & SI_LINE) == 0))
                    continue;

                setBrick (px, py);

                lastBrick.d.x = px;
                lastBrick.d.y = py;
            }
        }

    makeNormals (x - 17, y - 17, x + 35, y + 35, NORM_SIMPLE_SAVE);
    return 1;
}

int convertPump;
int floodFctPump (int x, int y)
{
    screeninfo *pSi = &siArray[x + y*wid];

    /* do not convert bricks, already converted pixels and
     * pumps in piles
     */
    if ((pSi->particleList == SCR_BRICK) ||
        (pSi->flags & (SI_MARKER|SI_IS_PIPE)))
        return 0;

    if (convertPump && ((pSi->flags & SI_IS_PUMP) == 0))
        return 0;

    pSi->flags |= SI_MARKER;

    setPump (x, y, &pumpVec);

    return 1;
}

void
drawFctPump (int x, int y, int act, int mode)
{
    static int startX, startY, lastX, lastY;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);

    CHECK_DRAWING

    if (floodMode() && (mouseButton == 1))
    {
        if ((mouseButton == 1) && (event == BUTTON_RELEASE))
        {
            /* if the current pixel is a pump, convert only pump pixels.
             * Otherwise apply pump to all non-bricks
             */
            convertPump = 0;
            if (siArray[x + y*wid].flags & SI_IS_PUMP)
                convertPump = 1;

            flood (x, y, (FUNCPTR)floodFctPump, 1, 0);

            clearMarker (getFloodRect()->x, getFloodRect()->y, getFloodRect()->x + getFloodRect()->w, getFloodRect()->y + getFloodRect()->h);
        }

        return;
    }

    if (mouseButton == 3)
    {
        if (event == BUTTON_PRESS)
        {
            startX = lastX = mousePos.d.x;
            startY = lastY = mousePos.d.y;

            resetArrow (startX, startY);

            return;
        }

        if (event == BUTTON_MOVE)
        {
            lastX = mousePos.d.x;
            lastY = mousePos.d.y;

            updateArrow (lastX, lastY);

            return;
        }


        if (event == BUTTON_RELEASE)
        {
            lastX = mousePos.d.x;
            lastY = mousePos.d.y;

            fixArrow();
            updateArrow (lastX, lastY);
        }

        getArrowVec (&pumpVec.d.x, &pumpVec.d.y);

        return;
    }

    if (mouseButton == 1)
        doDrawWall (x, y, 0, 0);

}

int floodFctConvert (int x, int y)
{
    screeninfo *pSi = &siArray[x + y*wid];

    if (((pSi->particleList <= SCR_BRICK) && (pSi->wimps == 0)) ||
        (pSi->flags & SI_MARKER))
    {
        return 0;
    }

    siMark (pSi);
    return 1;
}

void drawFctConvert (int x, int y, int act, int mode)
{
    static int lastX, lastY;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);

    CHECK_DRAWING


    if (floodMode())
    {
        if ((mouseButton == 1) && (event == BUTTON_RELEASE))
        {
            flood (x, y, (FUNCPTR)floodFctConvert, 1, 0);

            applySettingsToMarked (x, y);

            siMarkClear();
        }

        return;
    }

    if (mouseButton != 1)
        return;

    switch (event)
    {
        case BUTTON_PRESS:
            doMark (x, y, 0, 0);
            applySettingsToMarked (x, y);
            siMarkClear();

            break;

        case BUTTON_MOVE:
        case BUTTON_RELEASE:
            drawLineF (lastX, lastY, x, y, (FUNCPTR)doMark, 1, 2, 3, 4);
            applySettingsToMarked (x, y);
            siMarkClear();
            break;
    }

    if ((event == BUTTON_RELEASE) && !haveAccelVec)
        pushClear (x, y, 1);

    lastX = x;
    lastY = y;
}

screeninfo *checkFree (screeninfo *pSi)
{
    if ((pSi->particleList != SCR_BRICK) &&
        ((pSi->flags & SI_PUSHBORDER) == 0))
        return pSi;

    return NULL;
}

/* find the closest free pixel on a box with a given "radius" around px/py
 */
screeninfo * findFree (int px, int py, int radius, int bx1, int by1, int bx2, int by2, FUNCPTR checkFunc)
{
    screeninfo *pSi;
    int        xl, xr, yu ,yl;
    int r;

    xl = MAX(bx1, px - radius);
    xr = MIN(bx2, px + radius);
    yu = MAX(by1, py - radius);
    yl = MIN(by2, py + radius);

    xl = MAX(0, xl);
    xr = MIN(wid-1, xr);
    yu = MAX(0, yu);
    yl = MIN(hei-1, yl);

#define CHECK_FREE(x,y)        \
    if (((unsigned int)x < wid) && ((unsigned int)y < hei) && (pSi = (screeninfo*)checkFunc(&siArray[(x)+(y)*wid])) != NULL) return pSi;

    for (r = 0; r < radius; r++)
    {
        if (xr != px)
        {
            CHECK_FREE(xr, py-r);
            CHECK_FREE(xr, py+r);
        }
        if (xl != px)
        {
            CHECK_FREE(xl, py-r);
            CHECK_FREE(xl, py+r);
        }
        if (yl != py)
        {
            CHECK_FREE(px-r, yl);
            CHECK_FREE(px+r, yl);
        }
        if (yu != py)
        {
            CHECK_FREE(px-r, yu);
            CHECK_FREE(px+r, yu);
        }
    }

    return NULL;
}

/* find something adjacent to px/py on a given screen area
 */
screeninfo *
find_adjacent (int px, int py, int x1, int y1, int w, int h, FUNCPTR checkFP)
{
    screeninfo *pSi;
    int rad;

    for (rad = 1; rad <= MAX(w/2,h/2); rad++)
    {
        pSi = findFree (px, py, rad, x1, y1, x1+w, y1+h, checkFP);

        if (pSi)
            return pSi;
    }
    return NULL;
}



intptr_t stencilWalkCb ( int ox, int oy, void *arg1, void *arg2)
{
    vec_t *pos = (vec_t*)arg1;
    int sx, sy;

    ox += pos->d.x;
    oy += pos->d.y;

    sx = SCREENC(ox);
    sy = SCREENC(oy);

    if (!inSelection (sx, sy))
        return 0;

    if (SCR_CONTENT(sx, sy) == SCR_BRICK)
        return 0;

    newParticle (ox, oy, 0, 0);

    return 0;

}

void drawFctStencil (int x, int y, int act, int mode)
{
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    screeninfo *pSi, *newSi;
    unsigned int data;
    int xx, yy;
    int px, py;
    int sx, sy;
    int stenOffX, stenOffY;
    int stenx, steny;
    int w,h;
    SDL_Surface *bitmap, *bitmapData;
    int pixel;

    stencilPos (NULL, &sx, &sy);
    stencilSize (NULL, &w, &h);


    if (event == SCROLLUP_SH)
    {
        rotateStencil (NULL, 1, 0, 1);
        drawStencilSprite (NULL, sx+w/2, sy+h/2, 0);
        return;
    }
    else if (event == SCROLLDOWN_SH)
    {
        rotateStencil (NULL, -1, 0, 1);
        drawStencilSprite (NULL, sx+w/2, sy+h/2, 0);
        return;
    }
    else if (event == SCROLLUP_CT)
    {
        rotateStencil (NULL, 0, 5, 1);
        drawStencilSprite (NULL, sx+w/2, sy+h/2, 0);
        return;
    }
    else if (event == SCROLLDOWN_CT)
    {
        rotateStencil (NULL, 0, -5, 1);
        drawStencilSprite (NULL, sx+w/2, sy+h/2, 0);
        return;
    }


    CHECK_DRAWING

    if (mouseButton == 3)
    {
        if (event == BUTTON_PRESS)
        {
            lockStencilCenter (NULL, 1);

            stencilEdge (NULL, &xx, &yy);
            xx = MIN(SCREENX(xx), wid-1);
            xx = MAX(xx, 0);
            yy = MIN(SCREENY(yy), hei-1);
            yy = MAX(yy, 0);

            SDL_WarpMouse (xx, yy);

            return;
        }

        if (event == BUTTON_RELEASE)
        {
            stencilPos (NULL, &xx, &yy);
            stencilSize (NULL, &w, &h);
            lockStencilCenter (NULL, 0);

            xx += w/2;
            yy += h/2;

            xx = MIN(SCREENX(xx), wid-1);
            xx = MAX(xx, 0);
            yy = MIN(SCREENY(yy), hei-1);
            yy = MAX(yy, 0);

            SDL_WarpMouse (xx, yy);
        }

        return;
    }

    if (mouseButton != 1)
        return;

    if (event != BUTTON_PRESS)
        return;

    if ((bitmap = stencilBitmap(NULL)) == NULL)
        return;

    
    if (stencilDrawParticles && stencilWallToParticle)
    {
        vec_t pos;
        int oo = overwrite;
        overwrite = 1;

        pos.d.x = INTERNC(x);
        pos.d.y = INTERNC(y);

        PTYPE_START_DRAW;

        stencilWalk (NULL, stencilWalkCb, (void*)&pos, 0);

        overwrite = oo;

        PTYPE_STOP_DRAW(1);

        goto skipWall;
    }

    if (!stencilDrawWall && !stencilParticleToWall)
        goto skipWall;

    if ((bitmapData = stencilData(NULL)) == NULL)
        return;

    stenOffX = stenOffY = 0;

    if (sx < 0)
    {
        w += sx;
        stenOffX = -sx;
        sx = 0;
    }

    if ((sx + w) >= wid)
        w -= (sx + w + 1) - wid;

    if (sy < 0)
    {
        h += sy;
        stenOffY = -sy;
        sy = 0;
    }
    if ((sy + h) >= hei)
        h -= (sy + h + 1) - hei;
#if 1
    /* mark brick elements from stencil */
    for (yy = 0; yy < h; yy++)
    {
        py = sy + yy;
        pSi = &siArray[sx + py * wid];

        for (xx = 0; xx < w; xx++, pSi++)
        {
            stenx = stenOffX + xx;
            steny = stenOffY + yy;

            pixel = SURFACE_GET8(bitmap, stenx, steny);

            if (stencilParticleToWall)
            {
                if (pixel == R_PART)
                    pSi->flags |= SI_PUSHBORDER;
            }
            if (!stencilWallToParticle && ((pixel == R_BRICK) || (pixel == R_POLYBRICK)))
            {
                pSi->flags |= SI_PUSHBORDER;
            }
        }
    }

    for (yy = 0; yy < h; yy++)
    {
        py = sy + yy;
        steny = stenOffY + yy;
        pSi = &siArray[sx + py * wid];

        for (xx = 0; xx < w; xx++, pSi++)
        {
            px = sx + xx;
            stenx = stenOffX + xx;

            if ((pSi->particleList > SCR_BRICK) &&
                (pSi->flags & SI_PUSHBORDER))
            {
                /* warp pixels out */

                /* find destination pixel */
                newSi = find_adjacent (px, py, sx, sy, w, h, (FUNCPTR)checkFree);

                if (!newSi)
                {
                    pSi->flags &= ~SI_PUSHBORDER;
                }
                else
                {
                    /* found, move all particles there */
                    warpParticles (pSi, newSi);
                }
            }

            /* draw stencil data */
            data = SURFACE_GET32(bitmapData, stenx, steny);

            if (SURFACE_GET8(bitmap, stenx, steny) == R_ACCEL)
            {
                pumpVec.d.x = (signed short)(data >> 16);
                pumpVec.d.y = (signed short)(data & 0xffff);
                setPump (px, py, &pumpVec);
            }

                
            if (pSi->flags & SI_PUSHBORDER)
            {
                if (stencilParticleToWall &&
                    (SURFACE_GET8(bitmap, stenx, steny) == R_PART))
                {
                    data = WALL_CHARGE | (1<<16);
                }

                /* draw brick from stencil, but only if it is still marged as stencil pixel
                 * (might not be able to draw brick because particles can not be moved out
                 */
                setBrick (px, py);

                TEMPR(pSi) = data >> 16;
                SET_WALL_DENS(pSi, data & 0xffff);
                FRICTION(pSi) = getValue (&wallFriction);

                if (inSelection (px, py))
                {
                    setPixel (px, py, siColor(pSi));
                }
            }
        }
    }

    /* clear stencil markers */
    for (yy = 0; yy < h; yy++)
    {
        py = sy + yy;
        pSi = &siArray[sx + py * wid];

        for (xx = 0; xx < w; xx++, pSi++)
        {
            pSi->flags &= ~SI_PUSHBORDER;
        }
    }
#endif
skipWall:
    /* if there are particles saved in a tape, draw them
     */
    if (stencilDrawParticles && !stencilParticleToWall && stencilTape.tapeLen)
    {
        vec_t center;
        vec_t offset;

        offset.d.x = x;
        offset.d.y = y;

        stencilOrgSize (NULL, &center.d.x, &center.d.y);
        center.d.x /= 2;
        center.d.y /= 2;

        center.d.x += SCREENC(stencilTape.rect.x);
        center.d.y += SCREENC(stencilTape.rect.y);

        offset.d.x = x - center.d.x;
        offset.d.y = y - center.d.y;

        playbackAll (&stencilTape, &offset, &center, stencilZoom(NULL), stencilRotation(NULL),
                     (1<<R_PART) | (1<<R_RMPART) | (1<<R_DRSTART) | (1<<R_LINK));
    }

    makeNormals (sx-1, sy-1, sx + w + 2, sy + h + 2, NORM_SIMPLE_SAVE);
}

int floodFctWall (int x, int y)
{
    if (siArray[x + y*wid].particleList == SCR_NONE)
    {
        setBrick (x, y);
        return 1;
    }

    return 0;
}

void drawFctWall (int x, int y, int act, int mode)
{
    static int lastX, lastY;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);

    CHECK_DRAWING

    if (pumpMode)
    {
        drawFctPump (x, y, act, mode);
        return;
    }

    if (floodMode())
    {
        if ((mouseButton == 1) && (event == BUTTON_RELEASE))
            flood (x, y, (FUNCPTR)floodFctWall, 1, 0);

        makeNormals (getFloodRect()->x-10,
                     getFloodRect()->y-10,
                     getFloodRect()->x + getFloodRect()->w + 10,
                     getFloodRect()->y + getFloodRect()->h + 10,
                     NORM_SIMPLE_SAVE);
        return;
    }


    if ((mouseButton == 3) && (currentDrawOp->id == DO_WALL_AUTOPOLY))
    {
        vecAreaCut = 1;
        mouseButton = 1;
    }

    else if (mouseButton == 3)
    {
        if ((event == BUTTON_PRESS) || (event == BUTTON_MOVE))
            doHeat (x, y, 0, 0);
        return;
    }

    if (mouseButton != 1)
        return;


    switch (event)
    {
        case BUTTON_PRESS:

            doDrawWall (x, y, 0, 0);
            break;

        case BUTTON_MOVE:
            /* fall through */

        case BUTTON_RELEASE:

            if ((mode == DR_DRAW) ||
                (mode == DR_LINE))
            {
                drawLineF (lastX, lastY, x, y, (FUNCPTR)doDrawWall, 1, 2, 3, 4);
            }

            if ((event == BUTTON_RELEASE) && (currentDrawOp->id == DO_WALL_AUTOPOLY))
            {
                pushClear (x, y, 0);
                vecCutMode (vecAreaCut);
                vectorizePixels (lastBrick.d.x, lastBrick.d.y);
                vecCutMode (0);

            }

            break;
    }

    if (event == BUTTON_RELEASE)
    {
        pushClear (x, y, 0);
        vecAreaCut = 0;
    }

    lastX = x;
    lastY = y;
}


int
doDrawClear (int x, int y, int dx, int dy)
{
    int xx, yy;
    int px, py;

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei))
                continue;

            setEmpty (px, py, 0);
        }

    makeNormals (x - 17, y - 17, x + 35, y + 35, NORM_SIMPLE_SAVE);
    return 1;
}

int siEraseParticles (screeninfo *pSi, unsigned int flags)
{
    particle *p, *next;

    if (pSi->particleList <= SCR_BRICK)
        return pSi->wimps ? 1 : 0;

    for (p = pSi->particleList; p != NULL; p = next)
    {
        next = p->next;

        if ((flags & ERASE_MARKED) && !pIsMarked (p))
            continue;

        if ((flags & ERASE_IMM) == 0)
        {
            removeParticleDelayed (p);
        }
        else
        {
            removeParticle(p, 0);
            SI_CHARGE(pSi) -= PCHARGE(p);
        }
    }

    return pSi->wimps ? 1 : 0;
}

static void removeWimps (particle *p, intptr_t arg)
{
    int t = PTYPE(p);
    int x, y;

    if (!pdescs[t]->isWimp)
        return;

    x = SCREENC(p->pos.d.x);
    y = SCREENC(p->pos.d.y);

    if (siArray[x + y*wid].flags & SI_MARKER)
    {
        removeParticle (p, DENS_UPDATE);
    }
}


int
eraseParticles (int x, int y)
{
    int xx, yy;
    int px, py;

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei) || !inSelection(px,py))
                continue;

            if (siEraseParticles (&siArray[px + py*wid], shiftPressed() ? 0 : ERASE_IMM))
            {
                /* Mark pixel, wimps on this pixel are removed later
                 */
                siMark (&siArray[px + py*wid]);
            }
#ifdef USE_OPENCL
            clSchedMarkAt (INTERNC(px), INTERNC(py), 1);
#endif
        }

#ifdef USE_OPENCL
        clSchedDeleteStart();
        clCommitJobs();
#endif


    return 1;
}


int floodFctEmpty (int x, int y)
{
    if (!pumpMode)
    {
        if (siArray[x + y*wid].particleList == SCR_BRICK)
        {
            setEmpty (x, y, 1);
            return 1;
        }
    }
    else
    {
        if (siArray[x + y*wid].flags & SI_IS_PUMP)
        {
            setEmpty (x, y, 1);
            return 1;
        }
    }

    return 0;
}


int floodFctPErase (int x, int y)
{
    if (siArray[x + y*wid].particleList > SCR_BRICK)
    {
        siEraseParticles (&siArray[x + y*wid], shiftPressed() ? 0 : ERASE_IMM);
        return 1;
    }

    return 0;
}

void drawFctClear (int x, int y, int act, int mode)
{
    static int lastX, lastY;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    CHECK_DRAWING

    if (floodMode())
    {
        if ((mouseButton == 1) && (event == BUTTON_RELEASE))
        {
            if (currentDrawOp->id == DO_ERASE_PAR)
            {
                /* erase particles
                */
                flood (x, y, (FUNCPTR)floodFctPErase, 1, 0);
            }
            else
            {
                /* erase walls */
                flood (x, y, (FUNCPTR)floodFctEmpty, 1, 0);

                makeNormals (getFloodRect()->x-10,
                             getFloodRect()->y-10,
                             getFloodRect()->x + getFloodRect()->w + 10,
                             getFloodRect()->y + getFloodRect()->h + 10,
                             NORM_SIMPLE_SAVE);
            }
        }
        return;
    }

    if (mouseButton != 1)
        return;

    switch (event)
    {
        case BUTTON_PRESS:
            if (currentDrawOp->id == DO_ERASE_PAR)
            {
                eraseParticles (x, y);
                if (siMarkedHasWimps())
                    walkAllParticles ((FUNCPTR)removeWimps, 0);
                siMarkClear();
            }
            else
            {
                doDrawClear (x, y, 0, 0);
            }
            break;

        case BUTTON_MOVE:
        case BUTTON_RELEASE:
            if (currentDrawOp->id == DO_ERASE_PAR)
            {
                drawLineF (lastX, lastY, x, y, (FUNCPTR)eraseParticles, 1, 2, 3, 4);
                if (siMarkedHasWimps())
                    walkAllParticles ((FUNCPTR)removeWimps, 0);
                siMarkClear();
            }
            else
            {
                drawLineF (lastX, lastY, x, y, (FUNCPTR)doDrawClear, 1, 2, 3, 4);
            }
            break;
    }

    lastX = x;
    lastY = y;
}


int
do_push (int *px, int *py, int sx, int sy, char **sten)
{
    unsigned int dist, minDist;
    int newx=0, newy=0;
    int x, y;

    sx -= 16;
    sy -= 16;

    minDist = 10000;

    for (x = sx; x < sx + 32; x++)
    {
        if ((x < 0) || (x >= wid) || !inSelection(x, sy))
            continue;

        for (y = sy; y < sy + 32; y++)
        {
            if ((y < 0) || (y >= hei) || !inSelection(x,y))
                continue;

            if (sten[y - sy + 3][x - sx] != ' ')
                continue;

            if (SCR_CONTENT (x, y) == SCR_BRICK)
                continue;

            dist =  (*px - x)*(*px - x) + (*py - y) * (*py - y);

            if (dist < minDist)
            {
                minDist = dist;
                newx = x;
                newy = y;
            }
        }
    }

    if (minDist == 10000)
        return 0;

    *px = newx;
    *py = newy;
    return 1;
}



void pushClear (int x, int y, int updateNormals)
{
    int xx, yy;
    int px, py;
    screeninfo *oldSi;

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            /* clear old selection */
            px = x + xx - 16 + CORR_X;
            py = y + yy - 16 + CORR_Y;

            if ((px < 0) || (px > wid - 1) || (py < 0) || (py >= hei))
                continue;

            oldSi = &siArray[px + py * wid];

            if (oldSi->flags & SI_PUSHBORDER)
            {
                oldSi->flags &= ~SI_PUSHBORDER;

                setPixel (px, py, COL_EMPTY);

                if (oldSi->particleList == SCR_BRICK)
                {
                    oldSi->particleList = NULL;
                    CLR_WALL_DENS(oldSi);
                }
            }
        }

    if (updateNormals)
        makeNormals (x - 17, y - 17, x + 35, y + 35, NORM_SIMPLE_SAVE);

}


static void drawAccelCb (void *cp, void *arg)
{
    pushDrawAccel (currentPushPos.d.x, currentPushPos.d.y);

    calloutSched (pushDrawCallout, 1, (FUNCPTR)drawAccelCb, NULL);
}

static void pushDrawAccel (int x, int y)
{
    int xx, yy;
    int px, py;
    particle *p;
    screeninfo *oldSi;

    for (xx = 0; xx < 32; xx++)
    {
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 ;
            py = y + yy - 16 ;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei) || !inSelection(px, py))
                continue;

            oldSi = &siArray[px + py * wid];
            p = oldSi->particleList;

            if (p <= SCR_BRICK)
                continue;

            for (; p != NULL; p = p->next)
            {
                pdescs[PTYPE(p)]->hit (p, accelVec.d.x, accelVec.d.y);
            }
        }
    }

                
    currentPushPos.d.x = x;
    currentPushPos.d.y = y;
}

int
pushDraw (int x, int y, int updateNormals)
{
    int xx, yy;
    int px, py;
    particle *p, *pp;
    screeninfo *oldSi, *newSi;
    DENSITY_TYPE density;
    int        srcThread, dstThread;
    static int oldx, oldy;

    x += CORR_X;
    y += CORR_Y;

    /* clear old selection */
    if (!haveAccelVec)
        pushClear (oldx - CORR_X, oldy - CORR_Y, updateNormals);

    for (xx = 0; xx < 32; xx++)
        for (yy = 0; yy < 32; yy++)
        {
            if (stencil[3 + yy][xx] == ' ')
                continue;

            px = x + xx - 16 ;
            py = y + yy - 16 ;

            if ((px < 0) || (px >= wid) || (py < 0) || (py >= hei) || !inSelection(px, py))
                continue;

            oldSi = &siArray[px + py * wid];
            p = oldSi->particleList;

            if (p == SCR_NONE)
            {
                oldSi->particleList = SCR_BRICK;
                SET_WALL_DENS_DFL (oldSi);

                oldSi->flags |= SI_PUSHBORDER;

                continue;
            }
            if (p == SCR_BRICK)
            {
                continue;
            }

            setPixel (px, py, COL_EMPTY);

            oldSi = GET_SCREENINFO (p);

            srcThread = threadGet (PTYPE(p), px, py);

            if (!do_push (&px, &py, x, y, stencil))
                continue;

            /* screen info of destination pixel */
            newSi = &siArray[px + py * wid];

            dstThread = threadGet (PTYPE(p), px, py);

            /* update particles for new pixel */
            for (pp = p;; pp = pp->next)
            {
                pp->pos.d.x = INTERNC (px) + pp->pos.d.x % GRID;
                pp->pos.d.y = INTERNC (py) + pp->pos.d.y % GRID;

                SET_SCREENINFO (pp, newSi);

                if (pushAddsImpuls)
                {
#if 0
                    pp->vec.d.x += dx * 100;
                    pp->vec.d.y += dy * 100;
#else
                    pp->vec.d.x += motionVec.d.x;
                    pp->vec.d.y += motionVec.d.y;
#endif
                }

                /* if destination pixel crosses a thread work area boundary,
                 * assign them to new thread
                 */
                if (srcThread != dstThread)
                {
                    removePFromThread (srcThread, pp);
                    addPToThread (dstThread, pp);
                }

                if (pp->next == NULL)
                    break;
            }

            /* chain destination pixels at end of current pixel list */
            pp->next = newSi->particleList;
            if (pp->next)
            {
                PREV_SET(pp->next, pp);
            }

            newSi->particleList = p;

            density =  SI_CHARGE(oldSi) +  SI_CHARGE(newSi);
            SI_CHARGE(newSi) = density;
            SET_WALL_DENS_DFL(oldSi);

            oldSi->particleList = SCR_BRICK;
            oldSi->flags |= SI_PUSHBORDER;


        }

    if (updateNormals)
    {
        makeNormals (x - 17, y - 17, x + 35, y + 35, NORM_SIMPLE_SAVE);
        if ((oldx != x) || (oldy != y))
            makeNormals (oldx - 17, oldy - 17, oldx + 35, oldy + 35, NORM_SIMPLE_SAVE);
    }

    oldx = x;
    oldy = y;

    return 1;
}


void drawFctPush (int x, int y, int act, int mode)
{
    static int startX, startY;
    static int lastX, lastY;
    static int pressed = -1;

    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    int dx, dy;

    if (pushDrawCallout == NULL)
        pushDrawCallout = calloutNew();

    CHECK_DRAWING


    if (mouseButton == 2)
        return;

    switch (event)
    {
        case BUTTON_PRESS:
            if (pressed == -1)
            {
                if (mouseButton == 1)
                {
                    startX = x;
                    startY = y;

                    if (haveAccelVec)
                    {
                        pushDrawAccel (x, y);

                        calloutSched (pushDrawCallout, 1, (FUNCPTR)drawAccelCb, NULL);
                    }
                    else
                    {
                        pushDraw (x, y, 1);
                    }

                    pressed = 1;
                }
                else if (mouseButton == 3)
                {
                    startX = lastX = mousePos.d.x;
                    startY = lastY = mousePos.d.y;

                    resetArrow (startX, startY);

                    pressed = 3;
                    return;
                }
            }
            else if ((pressed == 1) && (mouseButton == 3))
            {
                if (!haveAccelVec)
                {
                    pushClear (x,y, 1);
                    drawFctWall (x,y, MKEVENT(BUTTON_PRESS, 1), mode);
                    drawFctWall (x,y, MKEVENT(BUTTON_RELEASE, 1), mode);
                    pressed = -1;
                }
                return;
            }
            else
            {
                return;
            }
            break;

        case BUTTON_MOVE:
            calloutUnsched (pushDrawCallout);

            if (pressed == 3)
            {

                lastX = mousePos.d.x;
                lastY = mousePos.d.y;

                updateArrow (lastX, lastY);
                return;
            }
        case BUTTON_RELEASE:
            calloutUnsched (pushDrawCallout);

            if ((pressed == 1) && (mouseButton == 3))
            {
                return;
            }
            else if ((pressed == 3) && (mouseButton == 3))
            {
                lastX = mousePos.d.x;
                lastY = mousePos.d.y;

                fixArrow();
                updateArrow (lastX, lastY);

                getArrowVec (&dx, &dy);

                if (dx && dy)
                {
                    haveAccelVec = 1;
                    accelVec.d.x = dx;
                    accelVec.d.y = dy;

                    setArrowVec (dx, dy);
                    enableArrow(1);
                    showArrow(mousePos.d.x, mousePos.d.y);
                }
                else
                {
                    haveAccelVec = 0;
                }

                pressed = -1;
                return;
            }
            else if ((pressed == 1) && (mouseButton == 1))
            {
                if (!haveAccelVec)
                {
                    drawLineF (lastX, lastY, x, y, (FUNCPTR)pushDraw, 1, 1, 1, 1);
                }
                else
                {
                    drawLineF (lastX, lastY, x, y, (FUNCPTR)pushDrawAccel, 1, 1, 1, 1);
                    calloutSched (pushDrawCallout, 1, (FUNCPTR)drawAccelCb, NULL);
                }

                if (event == BUTTON_RELEASE)
                {
                    calloutUnsched (pushDrawCallout);
                    if (!haveAccelVec)
                        pushClear (x, y, 1);
                    pressed = -1;
                }

                break;
            }
            else
            {
                return;
            }
    }

    lastX = x;
    lastY = y;
}

void drawFctPushInit()
{
    if (haveAccelVec)
    {
        enableArrow(1);
        setArrowVec (accelVec.d.x, accelVec.d.y);
        fixArrow();
    }
    else
    {
        enableArrow (0);
    }
}


/* set brick at given location unless there's a particle there */
void
setBrick (int x, int y)
{
    screeninfo *pSi;
    int f = getValue (&wallFriction);
    int d = WALL_CHARGE;

    if (((uint) x >= wid) || ((uint) y >= hei) || !inSelection(x,y))
        return;

    pSi = &siArray[x + y * wid];

    recordBrick (&playbackTape, x, y, d, 0, f, WALL_MASS_GET(pSi));

    if (pSi->particleList > SCR_BRICK)
        return;

    pSi->flags &= ~(SI_PUSHBORDER|SI_BOUNDARY|SI_IS_PIPE|SI_MARKER);
    if ((pSi->flags & SI_BRICK) == 0)
    {
        pSi->flags |= SI_BRICK;
        updateNeighbours (pSi, 1);
    }

    pSi->particleList = SCR_BRICK;
    SET_WALL_DENS_DFL (pSi);
    TEMPR(pSi) = 0;
    FRICTION(pSi) = f;

    setPixel (x, y, COL_BRICK);
    CL_SET_WALL(x,y);
}



void
setEmpty (int x, int y, int force)
{
    screeninfo *pSi;

    if (((uint) x >= wid) || ((uint) y >= hei) || !inSelection(x,y))
        return;

    pSi = &siArray[x + y * wid];

    removeWell (x, y);

    recordErase (&playbackTape, x, y);

    if (!force && pumpMode && (pSi->particleList == SCR_BRICK))
    {
        return;
    }

    if ((pSi->flags & (SI_IS_PUMP | SI_BRICK | SI_IS_PIPE | SI_MARKER)) &&
        (options->drawMode & D_CYCLING))
    {
        setPixel (x, y, SCR_GET (x,y) & 0xffffff);
    }

    if (pSi->particleList == SCR_BRICK)
    {
        pSi->particleList = SCR_NONE;

        if (!options->useGpu)
            diffArray[x + y*wid] = -2 * FIELD(pSi);

        CLR_WALL_DENS(pSi);
        SI_SET_TEMPR(pSi,0);

        updateNeighbours (pSi, -1);
    }

    pSi->flags &= SI_POLY_MASK;
    XACCEL(pSi) = gravVec.d.x;
    YACCEL(pSi) = gravVec.d.y;
    CL_SET_WALL(x,y);
}

void setPump (int x, int y, vec_t *pVec)
{
    screeninfo *pSi;

    if (pVec == NULL)
        pVec = &pumpVec;

    if (!inSelection(x,y))
        return;

    if ((x < 0) || (x > wid - 1))
        return;

    if ((y < 0) || (y > hei - 1))
        return;

    pSi = &siArray[x + y * wid];

    if (pSi->particleList == SCR_BRICK)
        return;

    if (!dirPixel (x, y, pVec->d.x, pVec->d.y))
    {
        setPixel (x, y, COL_ACCEL);
    }
    else
    {
        if (options->drawMode & D_CYCLING)
            setPixel (x, y, COL_EMPTY);
        else
            setPixel (x, y, COL_EMPTY);
    }

    if (pSi->particleList == SCR_BRICK)
    {
        setEmpty (x,y,1);
    }

    recordAccel (&playbackTape, x, y, pVec->d.x, pVec->d.y);

    XACCEL(pSi) = pVec->d.x;
    YACCEL(pSi) = pVec->d.y;
    pSi->flags |= SI_IS_PUMP;

#ifdef USE_OPENCL
    clWallSet (x, y);
#endif
}

int circleDrawCb (int x, int y, int flag, int first, int last)
{
    if (!currentDrawOp->internc)
    {
        x = SCREENC(x);
        y = SCREENC(y);
    }

    if (circleStencilStarted())
    {
        /* when drawing a stencil base on a filled circle the first time, add
         * all possible positions into an array which is simlpy "played back" later
         * in the actual draw operation
         */
        addToPosCache (x - cCenter.d.x, y - cCenter.d.y);
        return 1;
    }

    if (flag & DRELEM_FIRST)
        currentDrawOp->drawFct (x, y, MKEVENT(BUTTON_PRESS, 1), DR_DRAW);

    currentDrawOp->drawFct (x, y, MKEVENT(BUTTON_MOVE, 1), DR_DRAW, x, y);

    if (flag & DRELEM_LAST)
        currentDrawOp->drawFct (x, y, MKEVENT(BUTTON_RELEASE, 1), DR_DRAW);

    return 1;
}

int circleScreenCb (int x, int y, int flag, uintptr_t arg1, uintptr_t arg2)
{
    scrPoly *po = (scrPoly*)arg1;

    polyAddCoord (po, x, y, arg2);

    return 1;
}

void showCircle (int ix, int iy)
{
    if ((currentStencil != ST_FCIRCLE) || circleStencilStarted())
    {
        polyHide (circlePolys[0]);
        polyHide (circlePolys[1]);

        return;
    }

    polyMove (circlePolys[0], ix, iy);
    polyMove (circlePolys[1], ix, iy);
}


void circlePreview (int clear)
{
//    drawLineF (SCREENC(cCenter.d.x), SCREENC(cCenter.d.y),
//                SCREENC(cCenter.d.x + cRx), SCREENC(cCenter.d.y + cRy), lineCb, COL_LINE, 1, clear, 0);

    if (clear)
    {
        polyHide (circlePolys[0]);
        polyHide (circlePolys[1]);
        return;
    }

    polyClear (circlePolys[0]);
    drawCircle (0, 0, cRx - cTx, cRy - cTy,
                GRID*2, 1,
                (FUNCPTR)circleScreenCb,
                (uintptr_t)circlePolys[0], (uintptr_t)COL_LINE, (uintptr_t)0, (uintptr_t)0);
    polyMove (circlePolys[0], cCenter.d.x, cCenter.d.y);
    polyShow (circlePolys[0]);

    if (cTx || cTy)
    {
        polyClear (circlePolys[1]);
        drawCircle (0, 0, cRx + cTx, cRy + cTy,
                    GRID, 1,
                    (FUNCPTR)circleScreenCb,
                    (uintptr_t)circlePolys[1], (uintptr_t)COL_LINE, (uintptr_t)0, (uintptr_t)0);
        polyMove (circlePolys[1], cCenter.d.x, cCenter.d.y);
        polyShow (circlePolys[1]);
    }
}

void circleEscape()
{
    if (!cDrawing)
        return;

    circlePreview (1);

    cDrawing = 0;

}
void drawOpCircle (int x, int y, int event)
{
    static int initialized = 0;
    int step;

    if (!initialized)
    {
        circlePolys[0] = polyNew (1000, POLY_OPEN, COL_LINE);
        circlePolys[1] = polyNew (1000, POLY_OPEN, COL_LINE);

        initialized = 1;
    }

    switch (event)
    {
        case BUTTON_PRESS:
            if (!cDrawing)
            {
                cCenter.d.x = x;
                cCenter.d.y = y;

                cRx = cRy = 0;

                motionCapture (1);

                cDrawing = 1;

                cRound = shiftPressed();

                if ((currentDrawMode() == DR_CIRCLE_FILL) || circleStencilStarted())
                {
                    cTx = cTy = 0;
                }
                else
                {
                    currentStencilRadius(&cTx, &cTy);
                    cTx = INTERNC(cTx);
                    cTy = INTERNC(cTy);
                }

                escConnect ((FUNCPTR)circleEscape);

                break;
            }

            circlePreview (1);

            if (currentDrawOp->id == DO_PARTICLES)
                step = CURRENT_RADIUS / 4;
            else
                step = GRID;

            if ((currentDrawMode() == DR_CIRCLE_FILL) || circleStencilStarted())
            {
                if (circleStencilStarted())
                    clearPosCache();

                drawFilledCircle (cCenter.d.x, cCenter.d.y, cRx, cRy, step, (FUNCPTR)circleDrawCb, 0, 0, 0, 0);

                printf("%d particles, %d dropped\n", positionCacheSize, pcDropped);

                stopCircleStencil();
                circlePreview (0);
            }
            else
            {
                drawCircle (cCenter.d.x, cCenter.d.y, cRx, cRy, step, 1, (FUNCPTR)circleDrawCb, 0, 0, 0, 0);
            }

            cDrawing = 0;
            motionCapture (0);


            break;

        case BUTTON_MOVE:
            if (!cDrawing)
                break;

            circlePreview (1);

            cRx = abs(x - cCenter.d.x);
            cRy = abs(y - cCenter.d.y);

            if (currentStencil == ST_FCIRCLE)
            {
                cRx = MIN(INTERNC(40), cRx);
                cRy = MIN(INTERNC(40), cRy);
            }

            if (cRound)
            {
                if (cRx > cRy)
                    cRy = cRx;
                else
                    cRx = cRy;
            }

            circlePreview (0);

            break;

        case BUTTON_RELEASE:
            break;
    }
}

static void vaEscape()
{
    motionCapture (0);

    removeCurrentPoly();
}

void drawOpVecArea (int x, int y, int act, int mode)
{
    int first = !polyDrawing();
    static int relX = -1, relY = -1;
    int sx, sy;
    int event = EVENT_CODE(act);
    int button = BUTTON_ID(act);
    int autoMode = (currentDrawMode() == DR_DRAW);

#if 0
    if ((button == 2) && (event == BUTTON_RELEASE))
    {
        vectorizePixels (x, y);

        return;
    }
#endif
    

    if (button != 1)
        return;

    sx = REALX(mousePos.d.x);
    sy = REALY(mousePos.d.y);


    switch (event)
    {
        case BUTTON_PRESS:
            if (first)
            {
                polyStart (sx, sy, 1);
                relX = sx;
                relY = sy;

                motionCapture (1);

                first = 0;

                escConnect ((void*)vaEscape);

                break;
            }

            if ((sx == relX) && (sy == relY))
            {
                if (polyFinish ())
                {
                    motionCapture (0);
                    first = 1;
                }
                return;
            }

            polyMoveEdge (sx, sy, autoMode);
            polyFix();

            relX = sx;
            relY = sy;

            break;

        case BUTTON_MOVE:
            if (first)
                break;

            polyMoveEdge (sx, sy, autoMode);

            break;

        case BUTTON_RELEASE:
            relX = sx;
            relY = sy;

            if (autoMode)
            {
                if (polyFinish ())
                {
                    motionCapture (0);
                }
                else
                {
                    motionCapture (0);
                    removeCurrentPoly();
                }
                first = 1;
            }

            break;
    }
}

void drawOpPoly (int x, int y, int event)
{
    int first = (drPolySize == 0);
    static int continuing = 0;
    int i;

    switch (event)
    {
        case BUTTON_PRESS:
            if (first)
            {

                initDrPoly();

                addToDrPoly (x, y);
                addToDrPoly (x+1, y);

                continuing = shiftPressed();

                motionCapture (1);

                break;
            }

            if (continuing)
            {
                if (!polyLastIdentical ())
                {
                    addToDrPoly (x, y);
                    break;
                }
            }

            polySetLast (x, y);

            if (!currentDrawOp->internc)
                convertDrPolyToRc ();

            currentDrawOp->drawFct (drPoly[0].d.x, drPoly[0].d.y, MKEVENT(BUTTON_PRESS, 1), DR_DRAW);

            for (i = 1; i < drPolySize; i++)
            {
                currentDrawOp->drawFct (drPoly[i].d.x, drPoly[i].d.y, MKEVENT(BUTTON_MOVE, 1), DR_DRAW);
            }

            currentDrawOp->drawFct (drPoly[i-1].d.x, drPoly[i-1].d.y, MKEVENT(BUTTON_RELEASE, 1), DR_DRAW);

            motionCapture (0);

            initDrPoly();

            break;

        case BUTTON_MOVE:
            if (first)
                break;

            polySetLast (x, y);
            break;

        case BUTTON_RELEASE:
            break;
    }
}

int drawOperation (int ix, int iy, int act)
{
    static int isDrawOp = 0;
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    int x, y;

    if (!currentDrawOp)
        return 0;

    if ((iy >= INTERNC(hei)) && !isDrawOp)
        return 0;

    if (event == BUTTON_PRESS)
    {
        isDrawOp = 1;
    }
    else if (event == BUTTON_RELEASE)
    {
        isDrawOp = 0;
    }

    ix = MAX(0, ix);
    ix = MIN(INTERNC(wid)-1, ix);

    iy = MAX(0, iy);
    iy = MIN(INTERNC(hei)-1, iy);

    if (!currentDrawOp->internc)
    {
        /* oldstyle, draw function wants pixel coordinates
         */
        x = SCREENC(ix);
        y = SCREENC(iy);
    }
    else
    {
        x = ix;
        y = iy;
    }

    if (mouseButton == 1)
    {
        if (currentDrawMode() == DR_LINE)
        {
            drawOpPoly (ix, iy, event);

            return 1;
        }
        if ((currentDrawMode() == DR_CIRCLE) || (currentDrawMode() == DR_CIRCLE_FILL))
        {
            drawOpCircle (ix, iy, event);

            return 1;
        }

        if (circleStencilStarted())
        {
            drawOpCircle (ix, iy, event);

            return 1;
        }
    }

    currentDrawOp->drawFct (x, y, act, currentDrawMode());

    return 1;
}

void setDrawFct (int f)
{
    int i;

    for (i = 0; i < NELEMENTS(drawOps); i++)
    {
        if (drawOps[i].id == f)
            break;
    }
    if (i >= NELEMENTS(drawOps))
        return;

    if (currentDrawOp && (currentDrawOp->id != f))
    {
        if (currentDrawOp->id == DO_PARTICLES)
            activatePtype (-1, 0);

        if (f == DO_PARTICLES)
            activatePtype (-1, 1);
    }

    if ((drawOps[i].drawModeMask & (1<< currentDrawMode())) == 0)
        setDrawMode (drawOps[i].dflMode);

    currentDrawOp = &drawOps[i];

    updateDrButtons (drawOps[i].stencilMask, 
                         drawOps[i].dflSten,
                         drawOps[i].drawModeMask, 
                     drawOps[i].dflMode);

    pumpMode = 0;

    switch (f)
    {
        case DO_PARTICLES:
            enableArrow (selectVector.d.x || selectVector.d.y);
            setArrowVec (selectVector.d.x, selectVector.d.y);
            fixArrow();
            break;

        case DO_PUMP:
            enableArrow (pumpVec.d.x || pumpVec.d.y);
            setArrowVec (pumpVec.d.x, pumpVec.d.y);
            fixArrow();
            pumpMode = 1;
            break;

        case DO_PUSH:
            drawFctPushInit();
            break;

        case DO_WALL_POLY:
            setDrawMode (DR_STAMP);
            break;

        default:
            enableArrow (0);
            break;
    }
}

int getDrawFct()
{
    return currentDrawOp->id;
}

int drawOpInternc()
{
    if (!currentDrawOp)
        return 0;

    if (currentDrawOp->id == DO_PARTICLES)
        return (pdescs[getValue (&ptype)]->noSize == 0);

    return currentDrawOp->internc;
}

void drawFctDecel (int x, int y, int act, int mode)
{
}
