/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#if !defined(_MINGW)
#include <netinet/in.h>
#else
#define ntohl(a) (a)
#define htonl(a) (a)
#endif

/* open a new save file "plansch_<idx>.bmp
*/
int getSaveFile ( char *name, int nameLen, fileRep *r)
{
    static char fname[SAVE_PATH_LEN];
    char errStr[32];
    int                i = 1;
    struct stat        st;
    int                found = 0;

    if (stat (r->dir, &st) < 0)
    {
#if !defined(_MINGW)
        if (mkdir (r->dir, 0777) == -1)
#else
        if (mkdir (r->dir) == -1)
#endif

        {
            perror("dirName");
            sprintf (errStr, "ERR: mkdir %s", r->dir);
            statText (4, errStr);
            return -1;
        }
    }
    else if (!S_ISDIR(st.st_mode))
    {
        sprintf (errStr, "ERR: %s no dir.", r->dir);
        statText (4, errStr);
        return -1;
    }

    for (i = 1; i < 1000; i++)
    {
        mkFileName (fname, r, NULL, ".bmp", i);

        if (stat (fname, &st) < 0)
        {
            found = 1;
            break;
        }
    }

    if (!found)
    {
        return -1;
    }

    if (name)
    {
        mkFileName (name, r, NULL, NULL, i);
    }

    return i;
}

void doSaveParticles (SDL_Rect *pRect, char *baseName)
{
    tape                Tape =
        {
            .tapePos = 0,
            .tapeLen = 0,
            .recordStart = 0,
            .buffer = NULL,
            .recording = 0
        };

    recordScreenSection (&Tape, pRect);

    if (Tape.tapePos == 0)
    {
        eraseTape (&Tape);   
        return;
    }

    tapeSave (&Tape, baseName, 1);

    eraseTape (&Tape);
}

/* save selected rect.
 * Filename is "plansch_<index>.bmp and plansch_<index>.raw"
 * The first one is a 8-bit bmp with a simple mask (wall=1, 0 otherwise).
 * The second one is a raw file with 32-bit values encoding temperature and
 * density (for wall), or the accelleration vector otherwise.
 * Index is increased until a unused file name is found.
 */
void saveSelection (SDL_Rect *pRect, int saveWall, int saveParticles)
{
    char                     baseName[SAVE_PATH_LEN];
    char                     imageName[SAVE_PATH_LEN];
    SDL_RWops                    *rw;
    SDL_Surface                    *surface;
    screeninfo                    *pSi;
    int                             x,y;
    int                             sx, sy;

    if (getSaveFile (baseName, SAVE_PATH_LEN, &stencilRep) == -1)
        return;

    /* save wall mask */
    sprintf (imageName, "%s.bmp", baseName);
    rw = SDL_RWFromFile (imageName, "w+");

    if (rw == NULL)
    {
        printf ("SDL_RWFromFile failed (%s): %s\n", imageName,  SDL_GetError());
        return;
    }

    surface = SDL_CreateRGBSurface (SDL_SWSURFACE, pRect->w, pRect->h, 32, 0xff0000, 0x00ff00, 0x0000ff,0);

    if (surface == NULL)
    {
        printf ("SDL_CreateRGBSurface failed (%s): %s\n", imageName,  SDL_GetError());
        SDL_RWclose (rw);
        return;

    }

//    SDL_SetPalette (surface, SDL_LOGPAL, colors, 0, 255);

    for (y = pRect->y; y < pRect->y + pRect->h; y++)
    {
        pSi = &siArray[pRect->x + y*wid];
        
        sy = y - pRect->y;
        for (x = pRect->x; x < pRect->x + pRect->w; x++, pSi++)
        {
            sx = x - pRect->x;
            SURFACE_SET32 (surface,sx,sy,0);

            if (((pSi->particleList == SCR_BRICK) && saveWall)  ||
                (pSi->flags & SI_IS_PUMP) ||
                ((pSi->particleList > SCR_BRICK) && saveParticles))
            {
                SURFACE_SET32(surface,sx,sy, rgb32ToSurfaceColor(surface, pixelColorGet (x,y)));
            }

#if 0
            if ((pSi->particleList > SCR_BRICK) && saveParticles)
                SURFACE_SET32(surface,sx,sy, PCOLOR(pSi->particleList));
#endif
        }
    }

    if (SDL_SaveBMP_RW (surface, rw, 1) == -1)
    {
        printf ("SDL_SaveBMP_RW failed (%s): %s\n", imageName,  SDL_GetError());
        SDL_RWclose (rw);
        SDL_FreeSurface (surface);
        return;

    }

    SDL_FreeSurface (surface);

    doSaveParticles (pRect, baseName);

    sprintf (imageName, "Saved as %s", baseName);
    statText (5, imageName);

}

/* load image <baseName>, provide 8-bit stencil in *stencil and additional data
 * (wall density, temperature, acceleration vector) in *data. A tape with .tape suffix
 * is loaded to pTape (unless NULL).
 */
int loadFile (fileRep *r, int index, SDL_Surface **stencil, SDL_Surface **data, tape *pTape)
{
    int x,y;
    char imageName[SAVE_PATH_LEN];
    int fd;
    int bmSize;
    int haveDataFile;
    unsigned int    tmp;
    SDL_Surface            *tmpSurface;
    SDL_PixelFormat format;

    format = *screen->format;
//    format.BitsPerPixel = 32;

    mkFileName (imageName, r, NULL, ".bmp", index);

    tmpSurface = SDL_LoadBMP_RW (SDL_RWFromFile(imageName, "r"), 1);

    if (tmpSurface == NULL)
    {
        return -1;
    }

    *stencil = SDL_ConvertSurface (tmpSurface, &format, 0);

    SDL_FreeSurface (tmpSurface);

    if (*stencil == NULL)
    {
        return -1;
    }


    if (data == NULL)
        return 0;

    /* try to open data file. If non exists (or bad size), then only use stencil
     */
    mkFileName (imageName, r, NULL, ".raw", index);

    fd = open (imageName, O_RDONLY);

    haveDataFile = (fd != -1);

    bmSize = (*stencil)->h * (*stencil)->w * 4;

    *data = SDL_CreateRGBSurface (SDL_SWSURFACE, (*stencil)->w, (*stencil)->h, 32, 0,0,0,0);

    if ((*data == NULL) || (((*data)->h * (*data)->pitch) != bmSize))
    {
        printf ("SDL_CreateRGBSurface failed (%s): %s\n", imageName,  SDL_GetError());
        SDL_FreeSurface (*stencil);

        if (haveDataFile)
            close (fd);

        return -1;
    }

    if (haveDataFile)
    {
        if (read (fd, (*data)->pixels, bmSize) != bmSize)
        {
            haveDataFile = 0;
            close (fd);
        }
    }

    for (y = 0; y < (*stencil)->h; y++)
    {
        for (x = 0; x < (*stencil)->w; x++)
        {
            if (haveDataFile)
            {
                tmp = SURFACE_GET32(*data, x, y);
                /* just convert to host order */
                SURFACE_SET32(*data, x, y, ntohl(tmp));
            }
            else
            {
                char c = SURFACE_GET8(*stencil, x, y);

                /* transfer settings from stencil */
                if ((c == 0) || (c == COL_BLUE_I))
                {
                    SURFACE_SET32(*data, x, y, 0);
                }
                else
                {
                    /* set density value of 1 */
                    SURFACE_SET32(*data, x, y, htonl (1));
                }
            }
        }
    }

    if (haveDataFile)
        close (fd);

    if (pTape)
    {
        /* try to open a saved tape
         */
        mkFileName (imageName, r, NULL, ".tape", index);

        if (!tapeLoad (pTape, imageName))
        {
            pTape->tapeLen = 0;
        }
    }

    return 0;
}

void mkFileName (char *fname, fileRep *r, char *prefix, char *suffix, int index)
{
    char tmpStr[SAVE_PATH_LEN];

    if (prefix == NULL)
        prefix = "";

    if (suffix == NULL)
        suffix = "";

    snprintf (tmpStr, SAVE_PATH_LEN, "%s/%s%s%s", r->dir, prefix, r->file, suffix);
    snprintf (fname, SAVE_PATH_LEN, tmpStr, index);
}

void removeSaveFile (fileRep *r, int index)
{
    char fileName[SAVE_PATH_LEN];
    char fileName2[SAVE_PATH_LEN];

    mkFileName (fileName, r, NULL, ".bmp", index);
    mkFileName (fileName2, r, "_rm_", ".bmp", index);
    rename (fileName, fileName2);

    mkFileName (fileName, r, NULL, ".raw", index);
    mkFileName (fileName2, r, "_rm_", ".raw", index);
    rename (fileName, fileName2);

    mkFileName (fileName, r, NULL, ".tape", index);
    mkFileName (fileName2, r, "_rm_", ".tape", index);
    rename (fileName, fileName2);
}
