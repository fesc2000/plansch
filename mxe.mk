##### system paths

SRCDIR =	$(shell pwd)

## The MXE installation directory
#
MXEROOT=/usr/lib/mxe

# whether to create a 32 or 64 bit binary
# For the OpenCL path only, 32-bit is faster for whatever reason
# There isn't much math going on, probably better cache ussage
#
ifeq ($(MACH),)
MACH=i686
#MACH=x86_64
endif

## Root path for SDL headers/libs (default is /usr)
#
SDLROOT=    $(MXEROOT)/usr/$(MACH)-w64-mingw32.static/

# should contain SDL, glew
# 
WINLIB =    $(MXEROOT)/usr/$(MACH)-w64-mingw32.static/lib/

# rule to avoid adding /usr/include to include path
# We only need usr/include/CL from there
#
all:	tmp/opencl/include/CL

# A hack to get the opencl headers from the host include path
# without having to use /usr/include
#
tmp/opencl/include/CL:
	mkdir -p tmp/opencl/include
	ln -sf /usr/include/CL tmp/opencl/include

OPENCLROOT=tmp/opencl


##### Optimization settings ######


## Default compiler flags (SSE2 is required)
#
#CFLAGS = -O3 -m32 
#CFLAGS = -O3 -msse2 -m32
CFLAGS = -O3 -msse2 -mfpmath=sse -fno-builtin 
#CFLAGS =  -g -msse2 -m32
#CFLAGS += -g

## CPU optimization
#CPU=core2
#CFLAGS += -march=$(CPU) -mtune=$(CPU)

#LIBDIRS += -L$(WINLIB) -L$(SDLROOT)/lib  -Llib
LIBDIRS += -Llib/$(MACH)

# Depends on whether 32- or 64-bit is used. Required (e.g.) by OpenCL
#
LIBARCH=x86
#LIBARCH=x86_64

###### Features ###########

USE_SSE2=y
USE_SSE41=n
USE_AVX=n

## Variants to build
#
# default, ssse3 and opengl stuff should build on must systems.
#
#VARIANTS =	default opengl sopengl
VARIANTS +=	opencl 

## MP support 
# Defining BIND_THREADS associates threads with specific CPUs
#
CFLAGS += -DOPENMP -fopenmp
#CFLAGS += -DBIND_THREADS

## Display some statistics
#
#CFLAGS += -DSTATS 

## 
# multi-threading by dividing the draw area
CFLAGS += -DMT_BORDERS

# Multi-threading by using a dedicated thread for wimps and non-wimps (buggy)
# CFLAGS += -DMT_WIMPS

#CFLAGS += -DTTF_SUPPORT -DTTF_FONT=\"plansch.ttf\"
#LIBS += -lSDL_ttf
################## System specific settings ##################

RM = del

## Compiler to use
#
#export CC := x86_64-w64-mingw32-gcc
export CC := $(MXEROOT)/usr/bin/$(MACH)-w64-mingw32.static-gcc
export CXX := $(MXEROOT)/usr/bin/$(MACH)-w64-mingw32.static-g++ -fpermissive -std=gnu++11

## Mingw support
#
#CFLAGS += -D_MINGW -DUSE_GLEW -DOPENGL_GLEW -D_MINGW_CROSS -Duint=uint32_t
CFLAGS += -DUSE_GLEW -DOPENGL_GLEW -Duint=uint32_t -D_MXE -D_MINGW
LIBS = -lSDLmain -lSDL -lpthread -ldxguid -lgdi32 -lwinmm 
OPENGL_LIBS = -lglew32 -lopengl32 

exesuffix   =	.exe

KERNEL	    = win
