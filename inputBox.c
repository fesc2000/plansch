/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define MAX_TEXT_LEN        100

static int inputBoxVisible = 0;
static char inputBoxText[MAX_TEXT_LEN+1];
static char visibleText[MAX_TEXT_LEN+1];
static int cursorPos;
static int visibleTextStart;
static FUNCPTR        closeCb;
static void *closeCbArg;

void textBoxInput (char sym, int shift, int ctrl)
{

}

void inputBox (char *banner, char *initialText, int x, int y, int textLen, FUNCPTR cb, void *cbArg)
{
    int i;
    if (inputBoxVisible)
        return;

    strncpy (inputBoxText, "");
    if (initialText)
        strncpy (inputBoxText, initialText, MAX_TEXT_LEN);

    closeCb = cb;
    closeCbArg = cbArg;

    textLen = MIN(MAX_TEXT_LEN, textLen);

    strncpy (visibleText, inputBoxText, textLen);
    for (i = strlen (visibleText; i < textLen; i++)
        strcat (visibleText, " ");

    clearTT();
    tmpDeactivateAllExcept (NULL);
    renderTextBox (x, y, banner, visibleText, NULL, NULL);

    inputBoxVisible = 1;

    keyCapture (textBoxInput, NULL);
    escConnect (clearTextBox);
}
