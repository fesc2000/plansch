
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define NORM_UPDATE_ARRAY_SIZE 30000000

static int verbose = 0;

int updateRectValid = 0;
vec_t currentUpdate1 = {{0,0}};
vec_t currentUpdate2 = {{0,0}};


int simpleNormals = 0;

int makeNormalVectorized(int direction, screeninfo *pStartSi, int adj, vec_t *normal);
int findAdjacentFree (screeninfo **ppBrick, screeninfo **ppFree, int searchMask);
int findAdjacentBrick (screeninfo **ppBrick, screeninfo **ppFree, int searchMask);

/* defines for environment bits 
 *
 * X0 X3 X5
 * X1 -- X6
 * X2 X4 X7
 */
#define X0  (1<<0)
#define X1  (1<<1)
#define X2  (1<<2)
#define X3  (1<<3)
#define X4  (1<<4)
#define X5  (1<<5)
#define X6  (1<<6)
#define X7  (1<<7)


/* mask environment bits based on quadrant
 */
static char quadMask[] = {
    X0 | X1 | X2 | X3 | X5,        // NW
    X0 | X1 | X2 | X3 | X4 | X5 | X7,        // W
    X0 | X1 | X2 | X4 | X7,        // SW
    X0 | X1 | X2 | X3 | X5 | X6 | X7,        // N
    0,
    X0 | X1 | X2 | X4 | X5 | X6 | X7,        // S 
    X0 | X3 | X5 | X6 | X7,        // NE
    X0 | X2 | X3 | X4 | X5 | X6 | X7,        // E
    X2 | X4 | X5 | X6 | X7,        // SE
};

/* mask environment bits based on the direction we are looking 
 */
#if 0
static char dirMask[] = {
    X0 | X1 | X3,        // NW
    X0 | X1 | X2,        // W
    X1 | X2 | X4,        // SW
    X0 | X3 | X5,        // N
    X2 | X4 | X7,        // S 
    X3 | X5 | X6,        // NE
    X5 | X6 | X7,        // E
    X4 | X6 | X7,        // SE
};
#else
static char dirMask[] = {
    X0 | X1 | X2 | X3 | X5,        // NW
    X0 | X1 | X2 | X3 | X4 | X5 | X7,        // W
    X0 | X1 | X2 | X4 | X7,        // SW
    X0 | X1 | X2 | X3 | X5 | X6 | X7,        // N
    X0 | X1 | X2 | X4 | X5 | X6 | X7,        // S 
    X0 | X3 | X5 | X6 | X7,        // NE
    X0 | X2 | X3 | X4 | X5 | X6 | X7,        // E
    X2 | X4 | X5 | X6 | X7,        // SE
};

#endif



void resetUpdateRect()
{
    currentUpdate1.d.x = wid;
    currentUpdate1.d.y = hei;
    currentUpdate2.d.x = 0;
    currentUpdate2.d.y = 0;

    updateRectValid = 0;
}

static void
normalSimple (int quad, int adj, signed char *nx, signed char *ny)
{
    int off;
    int dir = QUAD2DIR(quad);

    if ((quad == 4) || ((adj & (1<<dir)) == 0))
    {
        *nx = *ny = 0;
        return;
    }

    off = (adj & quadMask[quad]) * 2;
    *nx = adj2n[off];
    *ny = adj2n[off + 1];
}


static int adjGetSi (screeninfo *pSi)
{
    int dir;
    int adj = 0;

    for (dir = 0; dir < 8; dir++)
    {
        if (SI_DIR(pSi, dir)->particleList == SCR_BRICK)
            adj |= 1<<dir;
    }
    return adj;
}

/* update normal vectors within a box (simple method)
 */
void
makeNormalsSimple (int x1, int y1, int x2, int y2, int mode)
{
    int x;
    int y;
    int adj;
    screeninfo *pSi;
    int quad, dir;

    x1 = MAX(0,x1);
    y1 = MAX(0,y1);
    x2 = MIN(wid-1, x2);
    y2 = MIN(hei-1, y2);

    if (mode == NORM_SIMPLE_SAVE)
    {
        /* update/initialize the current update bounding box. Vectorized normals
         * will be update asynchronously in updateNormals()
         */
        currentUpdate1.d.x = MIN(currentUpdate1.d.x, x1);
        currentUpdate1.d.y = MIN(currentUpdate1.d.y, y1);
        currentUpdate2.d.x = MAX(currentUpdate2.d.x, x2);
        currentUpdate2.d.y = MAX(currentUpdate2.d.y, y2);

        updateRectValid = 1;
    }

    for (y = y1; y <= y2; y++)
    {
        if ((y < 0) || (y >= hei))
            continue;

        for (x = x1; x <= x2; x++)
        {
            if ((x < 0) || (x >= wid))
                continue;

            pSi = &siArray[x + y * wid];

            if (pSi->flags & (SI_IN_POLY|SI_LINE))
                continue;

            pSi->flags &= ~SI_BOUNDARY;

            if (pSi->particleList == SCR_BRICK)
            {
                memset ((void*)pSi->normals, 0, sizeof(pSi->normals));

                if (adjGetSi (pSi) == 0xff)
                {
                    setPixel (x, y, WALL_COL(TEMPR(pSi), WALL_MASS_GET(pSi)/2));
                }

                continue;
            }

            adj = adjGetSi (pSi);

            if (verbose==2)
            {
                printf ("adj=%x ", adj);
            }

            if (!adj)
            {
                memset ((void*)pSi->normals, 0, sizeof(pSi->normals));
                continue;
            }


            for (quad = 0; quad < 9; quad++)
            {
                dir = QUAD2DIR(quad);
                normalSimple (quad, adj, &pSi->normals[dir * 2], &pSi->normals[dir * 2 + 1]);

                if (verbose==2)
                    printf ("q%d:%d/%d ", quad, pSi->normals[dir * 2], pSi->normals[dir * 2 + 1]);

            }

            CL_SET_WALL (x,y);
        }
    }
}

void waitKey(void)
{
        SDL_Event event;

        while (1)
        while (SDL_PollEvent (&event))
                if (event.type == SDL_KEYUP)
                        return;
}

void makeNormalsVectorized (screeninfo *pSi)
{
    int dir;
    vec_t normal;
    int adj;

    if (pSi->particleList == SCR_BRICK)
        return;

    adj = adjGetSi (pSi);

    for (dir = 0; dir < 8; dir++)
    {
        makeNormalVectorized (dir, pSi, adj, &normal);
    }
}

/* provide normal vector for pixel and a given direction (0..7).
 * */
int makeNormalVectorized(int direction, screeninfo *pStartSi, int adj, vec_t *normal)
{
    int                 vecNum;
    vec_t         vectors[2];
    int                 vecLen[2];
    screeninfo        *pWalkFree, *pWalkBrick, *pStartBrick;
    vec_t          tangent, normR, normL;
    int                 i, j;
    screeninfo        *visitList[100];
    int                 visitCount = 0;
    int                 found;
    int                 ll, lr;
    int                 dir, searchMask;
    int                 quad = DIR2QUAD(direction);
    signed char         nx, ny;
    int                 dx, dy;
    int                 sx, sy;
    int                 len;

    normal->d.x = normal->d.y = 0;
    
    if (pStartSi->particleList == SCR_BRICK)
    {
        return 0;
    }

    sx = SI2X(pStartSi);
    sy = SI2Y(pStartSi);

    /* first, calculate the normal vector using the simple method
     * (i.e. looking at adjacent 8 pixels). This is the fallback
     * solution in case something goes wrong
     */
    normalSimple (quad, adj, &nx, &ny);

    if (nx == 0 && ny == 0)
    {
        return 0;
    }

    normal->d.x = nx;
    normal->d.y = ny;
    

    /* check for brick in given direction */
    pStartBrick = SI_DIR(pStartSi, direction);

    if (pStartBrick->particleList != SCR_BRICK)
    {
        if (verbose==2)
            printf ("dir=%d: nob", direction);
        
        return 0;
    }
    
    setPixel(SI2X(pStartBrick), SI2Y(pStartBrick), BOUNDARY_COL(pStartBrick));

    /* track surface in two directions */
    for (vecNum = 0; vecNum < 2; vecNum++)
    {
        pWalkBrick = pStartBrick;
        pWalkFree = pStartSi;
        pWalkFree->flags |= SI_VISITED;
        pWalkBrick->flags |= SI_VISITED;
    
        visitList[visitCount++] = pWalkFree;
        visitList[visitCount++] = pWalkBrick;

        searchMask = 0xff;

        vecLen[vecNum] = 0;

        /* track up to 10 pixels */
        for (i = 0; i < 10; i++)
        {
            found = 0;
            for (j = 0; j < 4; j++)
            {
                /* find adjacent unvisited free pixel having the current brick as neighbour */
                if ((dir = findAdjacentFree (&pWalkBrick, &pWalkFree, searchMask)) != -1)
                {
                    visitList[visitCount++] = pWalkFree;
                    pWalkFree->flags |= SI_VISITED;

                    /* once the seach direction is set, continue searching in this dir.
                     */
                    if (searchMask == 0xff)
                    {
                        searchMask = dirMask[dir];
                    }
                    found = 1;
                }
                /* find adjacent unvisited brick having the current free pixel as neighbour */
                if ((dir = findAdjacentBrick (&pWalkBrick, &pWalkFree, searchMask)) != -1)
                {
                    visitList[visitCount++] = pWalkBrick;
                    pWalkBrick->flags |= SI_VISITED|SI_BOUNDARY;

                    found = 1;

                    /* once the seach direction is set, continue searching in this dir.
                     */
                    if (searchMask == 0xff)
                    {

                    }
                }
                if (found)
                    break;
            }

            if (!found)
                break;
            
            dx = SI2X(pWalkFree) - sx;
            dy = SI2Y(pWalkFree) - sy;
            len = VEC_LEN(dx, dy);

            if (len > vecLen[vecNum])
            {
                vectors[vecNum].d.x = SI2X(pWalkFree);
                vectors[vecNum].d.y = SI2Y(pWalkFree);
                vecLen[vecNum] = len;
            }
        }
    }

    /* clear visit-flag in all pixels */
    for (i = 0; i < visitCount; i++)
    {
        visitList[i]->flags &= ~SI_VISITED;
    }


    /* fail if at least one of thw two surface vectors could not be calculated, or 
     * if one is very short
     */
    if ((vecNum < 2) || (vecLen[0] < 3) || (vecLen[1] < 3))
        return 0;
    
//    setPixel(SI2X(pStartSi), SI2Y(pStartSi), COL_RED);

    /* the tangent to the surface is the difference between the two surface vectors
     */
    tangent.d.x = vectors[0].d.x - vectors[1].d.x;
    tangent.d.y = vectors[0].d.y - vectors[1].d.y;

    normalize (&tangent.d.x, &tangent.d.y);

    /* we can calculate two normal vectors for the tangent 
     */
    normR.d.x = -tangent.d.y;
    normR.d.y = tangent.d.x;

    normL.d.x = tangent.d.y;
    normL.d.y = -tangent.d.x;

    /* to decide which normal vector points away from the surface, we use the
     * normal vector obtained using the simple method. We want the one pointing
     * into the same direction, i.e. the one resulting in a longer vector when
     * added to the "simple normal".
     */
    lr = (normR.d.x + nx) * (normR.d.x + nx) + (normR.d.y + ny) * (normR.d.y + ny);
    ll = (normL.d.x + nx) * (normL.d.x + nx) + (normL.d.y + ny) * (normL.d.y + ny);

    if (lr > ll)
        *normal = normR;
    else
        *normal = normL;
    
    return 1;
} 

#if 0
int trackSurface(screeninfo *pStartSi, vec_t **ap)
{
    int                vi = 0;
    screeninfo        *pWalkFree, *pWalkBrick, *pStartBrick;
    int                 i, j;
    screeninfo        *visitList[10000];
    int                 visitCount = 0;
    int                 found;
    int                 dir, searchMask;
    int                 sx, sy;
    int                 direction;
    int                 max = 1000;
    vec_t        *tArray;

    if (pStartSi->particleList == SCR_BRICK)
    {
        return 0;
    }

    tArray = (vec_t*)malloc(max * sizeof(vec_t));

    sx = SI2X(pStartSi);
    sy = SI2Y(pStartSi);

    /* check for brick in given direction */
    for (direction = 0; direction < 8; direction++)
    {
        pStartBrick = SI_DIR(pStartSi, direction);
        if (pStartBrick->particleList == SCR_BRICK)
            break;
    }

    if (pStartBrick->particleList != SCR_BRICK)
    {
        if (verbose==2)
            printf ("dir=%d: nob", direction);
        
        free (tArray);
        return 0;
    }
    
    setPixel(SI2X(pStartBrick), SI2Y(pStartBrick), BOUNDARY_COL(pStartBrick));

    /* track surface in two directions */

    pWalkBrick = pStartBrick;
    pWalkFree = pStartSi;
    pWalkFree->flags |= SI_VISITED;
    pWalkBrick->flags |= SI_VISITED;

    while (1)
    {
        visitList[visitCount++] = pWalkFree;
        visitList[visitCount++] = pWalkBrick;

        searchMask = 0xff;

        /* track up to 10 pixels */
        for (i = 0; i < 10; i++)
        {
            found = 0;
            for (j = 0; j < 8; j++)
            {
                /* find adjacent unvisited free pixel having the current brick as neighbour */
                if ((dir = findAdjacentFree (&pWalkBrick, &pWalkFree, searchMask)) != -1)
                {
                    visitList[visitCount++] = pWalkFree;
                    pWalkFree->flags |= SI_VISITED;

                    /* once the seach direction is set, continue searching in this dir.
                     */
                    if (searchMask == 0xff)
                    {
                        searchMask = dirMask[dir];
                    }
                    found = 1;
                }
                /* find adjacent unvisited brick having the current free pixel as neighbour */
                if ((dir = findAdjacentBrick (&pWalkBrick, &pWalkFree, searchMask)) != -1)
                {
                    visitList[visitCount++] = pWalkBrick;
                    pWalkBrick->flags |= SI_VISITED|SI_BOUNDARY;

                    found = 1;

                    /* once the seach direction is set, continue searching in this dir.
                     */
                    if (searchMask == 0xff)
                    {
                        searchMask = dirMask[dir];
                    }
                }
                if (found)
                    break;
            }

            if (!found)
                break;

        }

        if (found)
        {

            tArray[vi].d.x = INTERNC(SI2X(pWalkFree));
            tArray[vi].d.y = INTERNC_M(SI2Y(pWalkFree));

            vi++;

            if (vi >= max-1)
            {
                max += 1000;

                tArray = (vec_t*)realloc (tArray, max * sizeof(vec_t));
            }
        }
        else
        {
            if (searchMask == 0xff)
            {
                break;
            }
        }
    }

    if (vi == 0)
    {
        free (tArray);
        return 0;
    }

    tArray[vi].d.x = INTERNC_M(SI2X(pStartSi));
    tArray[vi].d.y = INTERNC_M(SI2Y(pStartSi));
    vi++;

    /* clear visit-flag in all pixels */
    for (i = 0; i < visitCount; i++)
    {
        visitList[i]->flags &= ~SI_VISITED;
    }

    *ap = tArray;

    return vi;

}
#endif

int findAdjacentFree (screeninfo **ppBrick, screeninfo **ppFree, int searchMask)
{
    int i, j;
    screeninfo *pFreeCheck;
    
    for (i = 0 ; i < 8; i++)
    {
        if ((searchMask & (1<<i)) == 0)
            continue;

        /* look for adjacent free pixel .. */
        if (i == -1)
            pFreeCheck = *ppFree;
        else
            pFreeCheck = SI_DIR(*ppFree, i);
        
        if ((pFreeCheck->particleList == SCR_BRICK) ||
            (pFreeCheck->flags & SI_VISITED))
            continue;
            
        /* .. having the current brick as neighbour */

        for (j = 0; j < 8; j++)
        {
            if (SI_DIR(pFreeCheck, j) != *ppBrick)
                continue;
        
            *ppFree = pFreeCheck;
            return i;
        }
    }

    return -1;
}

int findAdjacentBrick (screeninfo **ppBrick, screeninfo **ppFree, int searchMask)
{
    int i, j;
    screeninfo *pBrickCheck;
    
    for (i = 0 ; i < 8; i++)
    {
        if ((searchMask & (1<<i)) == 0)
            continue;

        /* look for adjacent brick pixel .. */
        pBrickCheck = SI_DIR(*ppBrick, i);

        if (!pBrickCheck)
            continue;
        
        if ((pBrickCheck->particleList != SCR_BRICK) ||
            (pBrickCheck->flags & SI_VISITED))
            continue;
            
        /* .. having the current free pixel as neighbour */

        for (j = 0; j < 8; j++)
        {
            if (SI_DIR(pBrickCheck, j) != *ppFree)
                continue;
                
            *ppBrick = pBrickCheck;
            return i;
        }
    }
    return -1;
}

/* calculate normal vectors in a box
 * mode can be
 * NORM_SIMPLE - Use simple and fast method
 * NORM_SIMPLE_SAVE - Use simple method, but remember all pixels
 * where the normals have been changed. Vectorized normals can be calculated
 * later by calling updateNormals().
 * NORM_VECTOR - Use vector based method (more exact but slow)
 */
void
makeNormals (int x1, int y1, int x2, int y2, int mode)
{
    int x,y;
    screeninfo *pSi;
    int t;
    int ws;
    OMPVARS

    if (x1 < 0)
        x1 = 0;

    if (x2 >= wid)
        x2 = wid-1;

    if (y1 < 0)
        y1 = 0;

    if (y2 >= hei)
        y2 = hei-1;

    if ((x1 > x2) || (y1 > y2))
        return;

    if (simpleNormals)
    {
        mode = NORM_SIMPLE;
    }

    if ((mode == NORM_SIMPLE) || (mode == NORM_SIMPLE_SAVE))
    {
        makeNormalsSimple (x1, y1, x2, y2, mode);
        return;
    }

    ws = (y2-y1) / numCores + 1;

verbose=0;
#pragma omp parallel for 
    for (t = 0; t < numParallel; t++)
    {
        OMP_THREAD_NAME("normals", t);
        for (y = y1 + t*ws; y < y1 + (t+1)*ws; y++)
        {
            if (y >= hei)
                break;

            pSi = &siArray[x1 + y * wid];

            for (x = x1; x <= x2; x++, pSi++)
            {
                makeNormalsVectorized (pSi);
                CL_SET_WALL (x,y);
            }
        }
    }
    OMPDONE
}

/* update normals in the current bounding box.
 * Do maximally count updates but at least one line.
 */
void updateNormals (int count)
{
    screeninfo *pSi;
    int dir, quad;
    int updated = 0;
    vec_t normal;
    int adj;
    int x, y;
    int lines = 0;

    if (!updateRectValid)
        return;

    verbose=0;

    for (y = currentUpdate1.d.y; y <= currentUpdate2.d.y; y++, lines++)
    {
        if (count <= 0)
        {
            currentUpdate1.d.y = y;

            return;
        }

        pSi = &siArray[currentUpdate1.d.x + y * wid];

        for (x = currentUpdate1.d.x; x <= currentUpdate2.d.x; x++, pSi++)
        {
            if (pSi->flags & (SI_IN_POLY|SI_LINE))
                continue;

            if (pSi->particleList == SCR_BRICK)
            {
                memset ((void*)pSi->normals, 0, sizeof(pSi->normals));

                continue;
            }

            adj = adjGetSi (pSi);
            
            updated = 0;
            for (dir = 0; dir < 8; dir++)
            {
                quad = dir;
                if (dir > 3)
                    quad++;

                if (makeNormalVectorized (dir, pSi, adj, &normal))
                    updated = 1;

                pSi->normals[dir * 2] = normal.d.x;
                pSi->normals[dir * 2 + 1] = normal.d.y;
            }
            CL_SET_WALL (x,y);
        }

        if (updated)
        {
            count--;
        }
    }
    resetUpdateRect();
}
