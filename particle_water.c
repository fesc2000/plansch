/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

static const float _ps_red[4] __attribute__((aligned(16))) = { 1.0, 0, 0, 0 };

#undef CANSHARE

#undef AVGCOLL
#undef OLDCOLL
#define EXACT_COLL
#undef UNSHARE2
#define FAST_DIR
#undef COLLIDE_WHEN_STUCK

static int fusion = 0;
static int fusionLimit = 0;

SETTING(wFusion2SliderSetting) =
{ 
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "FusionLimit"
    };

SETTING(wFusionGenEnergy) =
{ 
    .minValue = 0,
    .maxValue = 10,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "FusionToEnergy"
    };

#ifndef UNSHARE2
#undef _N
#undef _S

#define _N  3,0,-1,0
#define _NE 5,1,-1,1
#define _E  6,1,0,0
#define _SE 7,1,1,1
#define _S  4,0,1,0
#define _SW 2,-1,1,1
#define _W 1,-1,0,0
#define _NW 0,-1,-1,1
#define DIRTYPE        signed char
#define DIRDESCSIZE 4

#undef DIRSIZE
#define DIRSIZE (DIRDESCSIZE*8)

#define DIRALIGN 16

static  DIRTYPE __attribute__ ((aligned (DIRALIGN))) north[] = {
    _N, _NE, _NW, _E, _W, _S, _SE, _SW,
    _N, _NW, _NE, _W, _E, _S, _SW, _SE,
};

static DIRTYPE __attribute__ ((aligned (DIRALIGN))) northeast[] = {
    _NE, _E, _N, _SE, _NW, _S, _W, _SW,
    _NE, _N, _E, _NW, _SE, _W, _S, _SW
};

static DIRTYPE __attribute__ ((aligned (DIRALIGN))) east[] = {
    _E, _SE, _NE, _S, _N, _W, _SW, _NW,
    _E, _NE, _SE, _N, _S, _W, _NW, _SW
};
static DIRTYPE __attribute__ ((aligned (DIRALIGN))) southeast[] = {
    _SE, _E, _S, _NE, _SW, _N, _W, _NW,
    _SE, _S, _E, _SW, _NE, _W, _N, _NW,
};
static DIRTYPE __attribute__ ((aligned (DIRALIGN))) south[] = {
    _S, _SW, _SE, _W, _E, _N, _NW, _NE,
    _S, _SE, _SW, _E, _W, _N, _NE, _NW,
};
static DIRTYPE __attribute__ ((aligned (DIRALIGN))) southwest[] = {
    _SW, _W, _S, _NW, _SE, _N, _E, _NE,
    _SW, _S, _W, _SE, _NW, _E, _N, _NE,
};
static DIRTYPE __attribute__ ((aligned (DIRALIGN))) west[] = {
    _W, _SW, _NW, _S, _N, _E, _SE, _NE, 
    _W, _NW, _SW, _N, _S, _E, _NE, _SE,
};

static DIRTYPE __attribute__ ((aligned (DIRALIGN))) northwest[] = {
    _NW, _W, _N, _SW, _NE, _S, _E, _SE,
    _NW, _N, _W, _NE, _SW, _E, _S, _SE
};

static signed char *ft[] = {
    northwest,
    west,
    southwest,
    NULL,
    north,
    NULL,
    south,
    NULL,
    northeast,
    east,
    southeast,
    NULL,
};

static signed char *ftRnd[] = {
    north,
    northwest,
    northeast,
    south,
    southwest,
    southeast,
    east,
    west,
};
#endif

#ifndef EXACT_COLL
static struct
{
    int mul;
    int base;
} sinTbl[] =
{
    { 0, 0 },
    { 0, 0 },
    { 2, -GRID*2 },
    { -2, 0 },
    { 2, GRID*2 },
    { 0, 0 },
    { 0, 0 },
};

int cosTbl[] = { 0,0,-1,-1,-1,0,0 };

#define SIN(quad, x) (sinTbl[quad].base - (x) * sinTbl[quad].mul)
#define COS(quad, x) ((GRID - abs(x)) & cosTbl[quad])


#define ATR_CURVE(quad, x) (attrTbl[quad].base + (x) * attrTbl[quad].mul)
#define DECELD 9999
#define DECELQ 10000
#define FMUL(val,d,q) (((val)*d)/q)
#define ADIV 4

#endif


screeninfo *
moveWater2 (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *pa2 )
{
    vec_t screenPos;
    vec_t screenPosOld;
    vec_t screenPosDiff;
    vec_t newPos;
    vec_t normVec;
    particle *pp;
    screeninfo *newSi;
    FLOAT vec_len;
#ifndef EXACT_COLL
    int impx, impy;
    uint quad;
    int diff;
    int totw;
#endif
    vec_t tvec;
    QUAD_TYPE spq;
    int dir;
#ifndef UNSHARE2
    int first;
    DENSITY_TYPE d;
    int md;
    int minI;
    int minN;
    int mul;
    int xo, yo;
    int i;
    signed char *ftp;
    vec_t absVec;
#endif

    PART_GRAV(p,oldSi);
    V_TO_SCREEN(screenPosOld, p->pos);

    if (oldSi->flags & SI_LINE)
    {
        pHitLine (p, &p->vec, off, td, oldSi);
    }


    tvec = p->vec;
#if 0
    {
        __m64 mx, my;
        __m128i sum;
        mx = _mm_set1_pi32(p->vec.d.x);
        mx = _mm_mul_su32 (mx,mx);
        my = _mm_set1_pi32(p->vec.d.y);
        my = _mm_mul_su32 (my,my);

        sum = _mm_add_epi64(_mm_set_epi64(mx,mx), _mm_set_epi64(my,my));

    }



#else
    spq = (QUAD_TYPE)tvec.d.x * (QUAD_TYPE)tvec.d.x + (QUAD_TYPE)tvec.d.y * (QUAD_TYPE)tvec.d.y;
    if (spq >= MAX_SPEEDQ)
    {
        vec_len = sqrt(spq);
        tvec.d.x = (((FLOAT)p->vec.d.x * MAX_SPEEDF)) / vec_len;
        tvec.d.y = (((FLOAT)p->vec.d.y * MAX_SPEEDF)) / vec_len;

        p->vec = tvec;

        lostImpuls += vec_len - MAX_SPEEDF;
    }
#endif
    V_ADD(newPos, p->pos, tvec);

    V_TO_SCREEN(screenPos, newPos);
    V_SUB(screenPosDiff, screenPos, screenPosOld);

#ifdef FAST_DIR
    /* dir is -4 .. 4 */
    dir = ((screenPosDiff.d.x) * 3 + screenPosDiff.d.y);
#else
    /* dir is 0 .. 7 */
    dir = ((screenPosDiff.d.x + 1) * 3 + screenPosDiff.d.y + 1);
    if (dir > 3) dir--;
#endif

    td->v.partCount++;

#ifdef SUPPORT_CANSHARE
    if ((screenPosDiff.d.x == 0) && (screenPosDiff.d.y == 0))
#else

#ifdef FAST_DIR
    if (dir == 0)
#else
    if ((screenPosDiff.d.x == 0) && (screenPosDiff.d.y == 0))
#endif
#endif
    {
        /* no pixel movement. Check whether there's at least one more particle here 
         */
#ifndef CANSHARE
        if (likely (oldSi->particleList->next == NULL))
#endif
        {
            /* no, no screen movement */
            p->pos = newPos;

            td->v.nomove++;
            
            return oldSi;
        }

#ifdef UNSHARE2
        {
            int dx, dy;
            int xo = p->pos.d.x & MAX_SPEED, yo = p->pos.d.y & MAX_SPEED;
            vec_t tmpPos;
            int so = SI_OFFSET(oldSi);
            int ddl, ddr, ddu, ddd;

            pp = p->next;
            if (!pp)
                pp = oldSi->particleList;
            
#define DMUL        100
            ddl = (densArray[so-1] - densArray[so]) * DMUL;
            ddr = (densArray[so] - densArray[so+1]) * DMUL;
            ddu = (densArray[so-wid] - densArray[so]) * DMUL;
            ddd = (densArray[so] - densArray[so+wid]) * DMUL;

            dx = (ddl * (GRID - xo) + ddr * xo) / (100 * p->mass);
            dy = (ddu * (GRID - yo) + ddd * yo) / (100 * p->mass);

            spq = (QUAD_TYPE)dx * (QUAD_TYPE)dx + (QUAD_TYPE)dy * (QUAD_TYPE)dy;
            if (spq >= MAX_SPEEDQ)
            {
                vec_len = sqrt(spq);
                dx = (((FLOAT)dx * MAX_SPEEDF)) / vec_len;
                dy = (((FLOAT)dy * MAX_SPEEDF)) / vec_len;
            }

//            dx = pp->pos.d.x - p->pos.d.x;
//            dy = pp->pos.d.y - p->pos.d.y;
//            vec_len = VEC_LEN(dx, dy);

            tmpPos = newPos;

            tmpPos.d.x += dx;
            tmpPos.d.y += dy;
            p->vec.d.x += dx/1000;
            p->vec.d.y += dy/1000;

//            if (vec_len < GRID)
//            {
//                dx = (dx * (GRID)) / (vec_len * 4);
//                dy = (dy * (GRID)) / (vec_len * 4);
//                tmpPos.d.x -= dx;
//                tmpPos.d.y -= dy;
//
//                p->vec.d.x -= dx/1000;
//                p->vec.d.y -= dy/1000;
//            }

            screenPos.d.x = SCREENC (tmpPos.d.x);
            screenPos.d.y = SCREENC (tmpPos.d.y);
            newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

            if ((oldSi != newSi) && (newSi->particleList != SCR_BRICK))
            {
                newPos = tmpPos;
                goto move;
            }
            else
            {
                p->pos = newPos;

                td->v.nomove++;

                return oldSi;
            }
        }
#else

        /* calculate absolute vector values */
        absVec.d.x = abs (p->vec.d.x);
        absVec.d.y = abs (p->vec.d.y);

        /* roughly determine the direction we are moving.
         * xo/yo is 0, 1 or 2 for left/up, center, right/down resp.
         */
        if (likely(absVec.d.x))
        {
            xo = (absVec.d.y >= absVec.d.x * 2) ? 1 : 
                 -((p->vec.d.x >> 30) & 2) + 2;
        }
        else
        {
            xo = 1;
        }

        if (likely(absVec.d.y))
        {
            yo = (absVec.d.x >= absVec.d.y * 2) ? 1 : 
                 -((p->vec.d.y >> 30) & 2) + 2;
        }
        else
        {
            yo = 1;
        }

        /* get a list of preferred pixel directions based on the movement
         * calculated above
         */
        ftp = ft[(xo << 2) + yo];

        /* very unlikely case that particle does not move at all ..
         * select random direction list 
         */
        if (unlikely(ftp == NULL))
        {
            ftp = ftRnd[RND_MASK(3)];
        }

        /* now go through the offset list. If a pixel is empty, directly jump to
         * it.
         * Also calculate the minimum density of the adjacent pixels (lower then
         * rthe current pixel's density.
         * If no free pixel is found at the end of the loop, jump to the one with
         * lowest density and add impulse to the 1st particle on the
         * destination pixel, based on the unshareImpuls variable and the density
         * difference of the pixels.
         *
         * tbo is toggled to "randomly" select one of the two offset lists for the
         * current direction.
         *
         * This most likely could use some optimization! Some hardware min/max 
         * operations would be nice ...
         */
        td->tbo = DIRSIZE - td->tbo;
        minN = abs(SI_CHARGE(oldSi));
        minI = -1;
        md = 0;
        first=1;

        for (i = td->tbo; i < td->tbo + DIRSIZE; i += DIRDESCSIZE, first=0)
        {
            newSi = SI_DIR(oldSi, ftp[i]);

#if 1
            if (likely (newSi->particleList == SCR_NONE))
            {
                /* found free pixel, move there */
                newPos.d.x += ftp[i + 1] * GRID;
                newPos.d.y += ftp[i + 2] * GRID;

                screenPos.d.x = SCREENC(newPos.d.x);
                screenPos.d.y = SCREENC(newPos.d.y);

                td->v.unshareFree++;
                goto move;
            }
            else
#endif
            if ((newSi->particleList == SCR_BRICK) 
                /*|| ((newSi->particleList > SCR_BRICK) && (newSi->particleList->pPtr))*/
                || (newSi->flags & SI_LINE))
                
            {
                /* when moving into a wall, do not relocate this particle, wait until we collide
                 */
                if (first)
                {
                    p->pos = newPos;
                    return oldSi;
                }
                continue;
            }
            /* find adjacent pixel with lowest density
             * If there are more than one, prefer those in current
             * direction (use <, NOT <= !!).
             */
//            d = SI_CHARGE(newSi) + ftp[i+3];
            d = abs(SI_CHARGE(newSi));
            md += d;
            if ( d < minN)
            {
                minN =  d;
                minI = i;
            }
        }

#ifdef COLLIDE_WHEN_STUCK
        newSi = oldSi;
        pp = p->next;
        if (pp == NULL)
            pp = p->prev;
        goto collission;
#else
        /* No empty adjacent pixel found, move to the one with the
         * lowest density (lower than our own!).
         * If none matches, stay put.
         */
        if (minI != -1)
        {
            i = minI;


            newSi = SI_DIR(oldSi, ftp[i]);
            newPos.d.x += ftp[i + 1] * GRID;
            newPos.d.y += ftp[i + 2] * GRID;
            screenPos.d.x = SCREENC(newPos.d.x);
            screenPos.d.y = SCREENC(newPos.d.y);

#if 1
            if (unlikely (newSi->particleList == SCR_NONE))
            {
                    /* XXX this shoudn't happen, probably interger overflow in dens. array? */
                goto move;
            }
#endif

            pp = newSi->particleList;
#ifdef PART_PUSH
            /* push particle on destination pixel into same direction 
             */
            SET_PUSHDIR(pp, ftp[i]);
#endif

            /* add impuls to 1st particle of destination pixel, remove this
             * impuls from current particle.
             * The impuls is analog to the density difference.
             * The direction of the impuls is one of eight, based on the pixel
             * movement. This is simplified, we could also use the vector
             * of the two particles, which is more comutation effort.
             * Physically, this isn't correct anyway, so as long as the total
             * impuls is maintained it should be ok.
             */
            if (likely(GETVAL32(preassure)))
            {
#undef VECTOR_BASED_UNSHARE
#ifdef VECTOR_BASED_UNSHARE
                int dx, dy;

                dx = (GRID - (pp->vec.d.x - newPos.d.x)) / unshareHit;
                dy = (GRID - (pp->vec.d.y - newPos.d.y)) / unshareHit;

                p->vec.d.x -= dx;
                p->vec.d.y -= dy;
                pp->vec.d.x += dx;
                pp->vec.d.y += dy;
#else
                mul = ( SI_CHARGE(oldSi) -  SI_CHARGE(newSi)) * GETVAL32(preassure);
                p->vec.d.x += (ftp[i + 1] * mul) / p->mass;
                p->vec.d.y += (ftp[i + 2] * mul) / p->mass;

                if (PTYPE(pp) == PTYPE(p))
                {
                    pp->vec.d.x -= (ftp[i + 1] * mul) / pp->mass;
                    pp->vec.d.y -= (ftp[i + 2] * mul) / pp->mass;
                }
                else
                {
                    pdescs[PTYPE(pp)]->hit (pp, (int)-(ftp[i + 1] * mul) / pp->mass, (int)-(ftp[i + 2] * mul) / pp->mass);
                }
#endif
            }


            td->v.unshareSqueeze++;
            goto move;
        }
        else if (fusion)
        {
            if (((md) / p->mass) > fusionLimit)
            {
                pp = p->next;
                if (!pp)
                    pp = PREV_GET(p);

                if ((p->mass + pp->mass) <= MAX_MASS)
                {
                    pp->mass = p->mass + pp->mass;

                    SI_CHARGE(oldSi) += PCHARGE(p);

                    if (getValue(&particleAttraction))
                    {
#ifdef USE_OPENCL
                        clSchedKickFieldAt (&screenPos, pp->mass << 8);
#else
                        diffArray[screenPos.d.x + screenPos.d.y*wid] += pp->mass << 7;
#endif
                    }

                    if (GETVAL32(wFusionGenEnergy))
                    {
                        static vec_t nullvec = {{0,0}};
                        int m = p->mass / GETVAL32(wFusionGenEnergy) + 1;
                        extern int energyType;

                        createParticleAsync (&p->pos, &nullvec, 0, m, energyType, 1, 0, getColorSetting (energyType, 0), NULL, GETVAL32(wFusionGenEnergy));
                    }

                    return NULL;
                }
            }
        }

        /* no screen movement */
        p->pos = newPos;

        td->v.unshareStuck++;
        return oldSi;
#endif
#endif
    }

#ifdef FAST_DIR
    newSi = SI_QUAD_NEG(oldSi, dir);
#else
    newSi = SI_DIR(oldSi, dir);
#endif

    if (unlikely (newSi->particleList == SCR_BRICK))
    {
        FLOAT cxf, cyf, vpf;

        td->v.wallHit++;
        
        /* wall hit */
        td->drawLinkLine = 1;

                
        /* get normal vector of current screen position (direction based) 
         *
         * This is fixed-point arithmetic, except vector length calculation.
         * Normals are stored in NORM_LEN units (+-127).
         *
         */
        /* normal vector in normVec */
#ifdef FAST_DIR
        dir = dir - ((((unsigned int)dir >> 31) ^ 1));
        normVec.d.x = ((signed char*)&oldSi->normals[8])[dir * 2];
        normVec.d.y = ((signed char*)&oldSi->normals[8])[dir * 2+1];
#else
        normVec.d.x = oldSi->normals[dir * 2];
        normVec.d.y = oldSi->normals[dir * 2 + 1];
#endif
        cxf = tvec.d.x;
        cyf = tvec.d.y;

        /* normalize */
        vec_len = VEC_LEN (cxf, cyf);

        cxf = (cxf * NORM_LEN) / vec_len;
        cyf = (cyf * NORM_LEN) / vec_len;

        if (FRICTION(newSi))
        {
            float vl;
            normVec.d.x = (normVec.d.x * (100 - FRICTION(newSi)) - cxf * FRICTION(newSi));
            normVec.d.y = (normVec.d.y * (100 - FRICTION(newSi)) - cyf * FRICTION(newSi));
            vl = VEC_LEN(normVec.d.x, normVec.d.y);
            normVec.d.x = (normVec.d.x * NORM_LEN) / vl;
            normVec.d.y = (normVec.d.y * NORM_LEN) / vl;

        }

        /* reflect at normal vector */
        vpf = ((cxf * normVec.d.x) + (cyf * normVec.d.y));
        cxf = cxf - (2 * normVec.d.x * vpf) / (NORM_LENQ);
        cyf = cyf - (2 * normVec.d.y * vpf) / (NORM_LENQ);

        /* handle energy transfer between particle and wall.  */

        if (GETVAL32(wallTempr))
        {
            int t = (FLOAT)((((vec_len * p->mass) / MAX_MASS) - TEMPR(newSi))/2) * wallDamping;

            TEMPR(newSi) += t;

            if (t > 0.0)
            {
                vec_len -= (t * MAX_MASS) / p->mass;

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));
            }
            else
            {
                vec_t tvec;

                frandomize (cxf, cyf, (-t * MAX_MASS) / p->mass, &tvec);

                p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

                p->vec.d.x += tvec.d.x;
                p->vec.d.y += tvec.d.y;

            }

            /* Make sure perticle does not get too hot */
            spq = (QUAD_TYPE)p->vec.d.x * (QUAD_TYPE)p->vec.d.x + (QUAD_TYPE)p->vec.d.y * (QUAD_TYPE)p->vec.d.y;
            if (spq >= MAX_SPEEDQ)
            {
                vec_len = sqrt(spq);
                p->vec.d.x = (((FLOAT)p->vec.d.x * MAX_SPEEDF)) / vec_len;
                p->vec.d.y = (((FLOAT)p->vec.d.y * MAX_SPEEDF)) / vec_len;
            }

            adjustParticleColor (p, (float*)_ps_red, (FLOAT)TEMPR(newSi) / (FLOAT)MAX_TEMP); 
        }
        else
        {
            /* apply original impuls to reflection vector */

            p->vec.d.x = (int) (((vec_len) * (FLOAT)cxf * wallDamping) / (FLOAT)(NORM_LEN));
            p->vec.d.y = (int) (((vec_len) * (FLOAT)cyf * wallDamping) / (FLOAT)(NORM_LEN));
        }

#if 0
        /* move particle away from wall in the direction of the normal vector
         * to minimize probability of multiple wall hits.
         */
        newPos.d.x = INTERNC(screenPos.d.x) + GRID/2 +
                     (normVec.d.x * GRID) / (NORM_LEN);
        newPos.d.y = INTERNC(screenPos.d.y) + GRID/2 +
                     (normVec.d.y * GRID) / (NORM_LEN);
        V_TO_SCREEN(screenPos, newPos);
 
        if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
        {
            newPos = p->pos;
            V_TO_SCREEN(screenPos, newPos);
        }
        
        /* assume immediate screen movement after reflection */
        screenPos.d.x = SCREENC (newPos.d.x);
        screenPos.d.y = SCREENC (newPos.d.y);
        newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];
        goto move;
#else
        /* move away only if it seems necessary */
        newPos.d.x = p->pos.d.x + p->vec.d.x;
        newPos.d.y = p->pos.d.y + p->vec.d.y;
        V_TO_SCREEN(screenPos, newPos);
        if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
        {
            newPos.d.x += (normVec.d.x * GRID) / (NORM_LEN);
            newPos.d.y += (normVec.d.y * GRID) / (NORM_LEN);
            V_TO_SCREEN(screenPos, newPos);

            if (SCR_CONTENT(screenPos.d.x, screenPos.d.y) == SCR_BRICK)
            {
                newPos = p->pos;
                V_TO_SCREEN(screenPos, newPos);
            }
        }

        newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];
        goto move;
        
#endif


    }

#ifndef NO_PARTICLE_COLLISSION
    if ((newSi->particleList) && !NOHIT(p))
    {
#ifdef COLOR_WITH_COL
        p->col+=2;
#endif

        /* particle collision. We will share the pixel with the particle we hit,
         * in the hope that the hit particle is gone next time we're here.
         * If not, it is probable that in the next iteration we will find an adjacent
         * free pixel to avoid an explosion effect.
         */
        

#ifdef AVGCOLL
        p->vec.d.x = pp->vec.d.x = (p->vec.d.x * p->mass + pp->vec.d.x * pp->mass) / (p->mass + pp->mass);
        p->vec.d.y = pp->vec.d.y = (p->vec.d.y * p->mass + pp->vec.d.y * pp->mass) / (p->mass + pp->mass);
#endif

#ifdef EXACT_COLL
#define CL_TYPE        int
#define FP_MUL        100
#define FP_MUL1        (FP_MUL+FP_MUL/10)
        td->v.collissions++;
        for (pp = newSi->particleList; pp != NULL; pp = pp->next)
        {
            CL_TYPE dx = pp->pos.d.x - p->pos.d.x;
            CL_TYPE dy = pp->pos.d.y - p->pos.d.y;
            if (((-dy)*(p->vec.d.y - pp->vec.d.y) - (dx)*(p->vec.d.x - pp->vec.d.x)) < 0)
            {
                CL_TYPE frc;
                CL_TYPE impx1, impy1;
                CL_TYPE impx2, impy2;
                CL_TYPE wp = ((pp->mass * FP_MUL * 2) / (p->mass + pp->mass));

                vec_len = VEC_LEN (dx, dy);
                dx = (dx * NORM_LEN) / vec_len;
                dy = (dy * NORM_LEN) / vec_len;


                frc = (CL_TYPE)p->vec.d.x * dx + (CL_TYPE)p->vec.d.y * dy;

                impx1 = (frc * dx) / NORM_LEN;
                impy1 = (frc * dy) / NORM_LEN;

                dx = -dx;
                dy = -dy;

                frc = ((CL_TYPE)pp->vec.d.x * dx + (CL_TYPE)pp->vec.d.y * dy);
                impx2 = (frc * dx) / NORM_LEN;
                impy2 = (frc * dy) / NORM_LEN;

                p->vec.d.x = p->vec.d.x + ((impx2 - impx1) * wp) / (FP_MUL1 * NORM_LEN);
                p->vec.d.y = p->vec.d.y + ((impy2 - impy1) * wp) / (FP_MUL1 * NORM_LEN);

                wp = FP_MUL * 2 - wp;

                pdescs[PTYPE(pp)]->hit (pp, ((impx1 - impx2)*wp) / (FP_MUL1 * NORM_LEN), (((impy1 - impy2)*wp)) / (FP_MUL1 * NORM_LEN));

#if 0
                if ((int64_t)(p->mass + pp->mass) *
                    (((int64_t)p->vec.d.x * (int64_t)p->vec.d.x + (int64_t)p->vec.d.y * (int64_t)p->vec.d.y) +
                     ((int64_t)pp->vec.d.x*(int64_t)pp->vec.d.x + (int64_t)pp->vec.d.y * (int64_t)pp->vec.d.y)) > MAX_SPEEDQ*255)
                {
                    if (p->mass > 1)
                    {
                        p->mass /= 2;
                        cloneParticleAsync (p, p->mass, p->vec.d.x / 2, p->vec.d.y/2);
                    }
                }
#endif
                mergeParticleColors (p, pp);
                break;
            }
        }
#endif

#ifdef OLDCOLL
        /* calculate the hit impuls based on the relative position
         * This would require sin/cos calculations. There are
         * three possibilities:
         * - Float sin/cos (too slow)
         * - Use a lookup table: would be quite large, adding signigficant
         *   cache/TLB misses
         * - Use a linear approximation of the sin/cos curves (used).
         */
        diff = pp->pos.d.y - p->pos.d.y;
        quad = (diff / (GRID/2) + 3);
//printf ("%d %d (%d)  %d %d (%d)\n", p->vec.d.x, p->vec.d.y, (int)VEC_LEN(p->vec.d.x, p->vec.d.y), pp->vec.d.x, pp->vec.d.y, (int)VEC_LEN(pp->vec.d.x, pp->vec.d.y));

        impx = ((p->vec.d.x - pp->vec.d.x) * COS(quad, diff)) / GRID;
        impy = ((p->vec.d.x - pp->vec.d.x) * SIN(quad, diff)) / GRID;

        diff = pp->pos.d.x - p->pos.d.x;
        quad = (diff / (GRID/2) + 3);

        impy += ((p->vec.d.y - pp->vec.d.y) * COS(quad, diff)) / GRID;
        impx += ((p->vec.d.y - pp->vec.d.y) * SIN(quad, diff)) / GRID;

//printf (" %d %d (%d)\n", impx, impy, (int)VEC_LEN(impx, impy));

        if (likely((impx != 0) || (impy != 0)))
        {
            vec_t ppVec;

            td->v.collissions++;
            
            /* Share the impuls and calculate new vectors for
             * both particles (based on the current vectors and
             * where the x/y offset 
            impx = impx / 2;
            impy = impy / 2;
             */
             
            totw = p->mass + pp->mass;

            ppVec.d.x = ppVec.d.y = 0;
            if (impx)
            {
                p->vec.d.x -= (impx * pp->mass) / totw;
                ppVec.d.x = (impx * p->mass) / totw;
//                pp->vec.d.x += (impx * p->mass) / totw;

                if (impy)
                {
                    p->vec.d.y -= (impy * pp->mass) / totw;
                    ppVec.d.y = (impy * p->mass) / totw;
//                    pp->vec.d.y += (impy * p->mass) / totw;
                }
            }
            else
            {
                if (impy)
                {
                    p->vec.d.y -= (impy * pp->mass) / totw;
//                    pp->vec.d.y += (impy * p->mass) / totw;
                    ppVec.d.y = (impy * p->mass) / totw;
                }
            }

            pdescs[PTYPE(pp)]->hit (pp, ppVec.d.x, ppVec.d.y);

#if 0
            {
                vec_t _s;

#if 1
            _s = newPos;
            toBorder (&newPos, p->vec.d.x, p->vec.d.y);
            if ((screenPos.d.x != SCREENC(newPos.d.x)) ||
                (screenPos.d.y != SCREENC(newPos.d.y)))
            {
                printf ("%x/%x + %d/%d -> %x/%x !!\n",
                        _s.d.x, _s.d.y, p->vec.d.x, p->vec.d.y, 
                        newPos.d.x, newPos.d.y);
            }
#endif
#if 1
            _s = pp->pos;
            toBorder (&pp->pos, pp->vec.d.x, pp->vec.d.y);
            if ((SCREENC(_s.d.x) != SCREENC(pp->pos.d.x)) ||
                (SCREENC(_s.d.y) != SCREENC(pp->pos.d.y)))
            {
                printf ("%x/%x + %d/%d -> %x/%x !!\n",
                        _s.d.x, _s.d.y, pp->vec.d.x, pp->vec.d.y, 
                        pp->pos.d.x, pp->pos.d.y);
            }
#endif
            }
#endif

            mergeParticleColors (p, pp);
        }
#endif

        newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];
        
        goto move;
    }
    CLR_NOHIT(p);
#endif /* COLLISSION */
    
    /* regular movement */
    td->v.move++;

move:
#include "do_move.c"
}


static void
fusionSlider (button *b, int action, int ox, int oy)
{
    slider (b, action, ox, oy);

    if (getValue(&wFusion2SliderSetting) == 0)
    {
        fusion = 0;
    }
    else
    {
        fusionLimit = 100 - getValue(&wFusion2SliderSetting);
        fusion = 1;
    }

}

void applySettingWater (particle *p, setting *s)
{
    p->size = MAX_RADIUS >> RADIUS_SHIFT;
}

pdesc MOD = {
    .moveParticle = moveWater2,
    .applySetting = (FUNCPTR)applySettingWater,
    .name = "Water",
    .canCreate = NULL,
    .isWimp = 0,
    .noSize = 1,
    .descr = "",
    .pm = water_pm,
    .help = "water",
    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
//        {(FUNCPTR)slider, 34, 67, psize_pm, "Particle Size", 0, &particleSize[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},
        {(FUNCPTR)fusionSlider, 32, 67, fusion_xpm, "Fusion", 0, &wFusion2SliderSetting, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, fusion_to_e_xpm, "Fusion To Energy", (intptr_t)0, &wFusionGenEnergy, B_NEXTRC },
    }
};
