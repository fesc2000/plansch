/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#ifdef USE_OPENCL
#include "opencl.h"
#endif

#ifdef USE_OPENGL
v4sf *wallColors;
#endif

#define H1  ((2*M_PI) / 6.0)
#define H2  (2.0 * H1)
#define H3  (3.0 * H1)
#define H4  (4.0 * H1)
#define H5  (5.0 * H1)
#define H6  (2*M_PI)

SDL_Color tcBoundaryColors[101];

/* base color + zoomColor */

/* Remembered target colors (to restore original color after merging) */
COLOR_TYPE *tgtCol;
COLOR_TYPE *dflCol;


/* boundary color based on friction */
int tcBoundaryColor(screeninfo *pSi)
{
    unsigned int fr = FRICTION(pSi);
    int rc;

    if (fr > 100)
        fr = 0;

    rc =  ARGB(0x02000000 | (tcBoundaryColors[fr].r << 16) | (tcBoundaryColors[fr].g << 8) | tcBoundaryColors[fr].b);
#ifdef USE_OPENGL
    rc = CH_ADD_A(rc, 0xd0);
#endif

    return rc;
}

void buttonColorBg (setting *s)
{
    if (s->pButton)
    {
        buttonBgColor (s->pButton, ARGB(s->current.val.cv.argb | 0xff000000));
    }
}


SETTING(paintColor) =
{
    .minValue = 0,
    .maxValue = 0,
    .defaultValue = 0,
    .name = "paintColor",
    .colorSelection = 1,
    .cb=(FUNCPTR)buttonColorBg
};


void cbColorMerge (setting *s);
SETTING(colMergeSpeed) =
{
    .defaultValue = 30,
    .minValue = 0,
    .maxValue = 50,
    .floatDivi = 100,
    .minDiff = 1,
    .name = "Color assimilation rate",
    .cb = (FUNCPTR)cbColorMerge
};

void cbColorRestore (setting *s);
SETTING(colRestoreSpeed) =
{
    .defaultValue = 20,
    .minValue = 0,
    .maxValue = 500,
    .floatDivi = 10000,
    .minDiff = 1,
    .name = "Color restore rate",
    .cb = (FUNCPTR)cbColorRestore
};

#ifdef USE_OPENGL
v4sf *colorMergeSpeed;
v4sf *colorRestoreSpeed;

void cbColorMerge (setting *s)
{
    float f = getValueNorm(s);
    *colorMergeSpeed = _mm_set1_ps (f);
#ifdef USE_OPENCL
    clColorMergeSet (f);
#endif
}

void cbColorRestore (setting *s)
{
    float f = getValueNorm(s);
    *colorRestoreSpeed = _mm_set1_ps (f);
#ifdef USE_OPENCL
    clColorRestoreSet (f);
#endif
}
#else
void cbColorMerge (setting *s)
{
}
void cbColorRestore (setting *s)
{
}
#endif

void makeColors (SDL_Color *colors, int start, int num, 
                 SDL_Color startC, SDL_Color endC)
{
    int i;
    FLOAT div = num-1;
    SDL_Color col;

    if (num == 0)
        return;

    if (num == 1)
    {
        colors[(unsigned char)start] = startC;
        return;
    }

    for (i = 0; i < num; i++)
    {
        col = startC;

        col.r += ((FLOAT)(endC.r - startC.r) / div) * (FLOAT)i;
        col.g += ((FLOAT)(endC.g - startC.g) / div) * (FLOAT)i;
        col.b += ((FLOAT)(endC.b - startC.b) / div) * (FLOAT)i;

        colors[(unsigned char)(start+i)] = col;
    }
}

void initColors (SDL_Color *colors)
{
    SDL_Color col1 = {0,0,0,0}, col2 = {0,0,0,0};

    col1.r = 255;
    col1.g = 128;
    col1.b = 0;
    col2.r = 0;
    col2.g = 128;
    col2.b = 255;


    makeColors (tcBoundaryColors, 0, 101, col1, col2);

    dflCol = (void*)memalign (0x1000, options->maxParticles * sizeof(*dflCol));

    if (dflCol == NULL)
    {
        fprintf (stderr, "Failed to allocate %x bytes for dflCol\n", (int)(options->maxParticles * sizeof(*dflCol)));
    }

#ifdef USE_OPENGL
    int i;
    tgtCol = (COLOR_TYPE*)memalign (0x1000, options->maxParticles * sizeof(*tgtCol));

    if (tgtCol == NULL)
    {
        fprintf (stderr, "Failed to allocate %x bytes for tgtCol\n", (int)(options->maxParticles * sizeof(*tgtCol)));
    }

    wallColors = (v4sf*)memalign (0x1000, wid * hei * sizeof(v4sf));
    for (i = 0; i < wid*hei; i++)
    {
        wallColors[i] = _mm_set1_ps (0.0f);
    }

    colorMergeSpeed = memalign(0x10, 0x10);
    colorRestoreSpeed = memalign(0x10, 0x10);
#endif
}

uint32_t getColorSetting (int type, int charge)
{
    int crange = -1;
    uint32_t        col = 0xffffffff;
    uint32_t    mask = 0xffffffff;

#ifndef USE_OPENGL
    /* no alpha channel here 
     */
    mask = 0x00ffffff;
#endif

    if (pdescs[type]->getColor && pdescs[type]->getColor(&col))
    {
        return col & mask;
    }

    crange = charge + 1;
    if ((crange < 0) || (crange > 2))
        crange = 0;

    if (pdescs[type]->crangeColors[crange])
    {
        value_u *u = getValue_u(pdescs[type]->crangeColors[crange]);
        int rndBox = pdescs[type]->crangeColors[crange]->canBeRandom;
        value_u ru;

        if (!u)
        {
            col = 0xffffffff;
        }
        else
        {
            ru = *u;

            randomizeColorVal (&ru.cv, rndBox);

            col = ARGB(ru.cv.argb);
        }
    }

    return col & mask;
}


void defaultParticleColor(particle *p)
{
    uint32_t        col = getColorSetting (PTYPE(p), CHARGE_SIGN(p));

    setParticleColorARGB (p, col, 1, 0xffffffff);
}

void setParticleColorF (particle *p, fcolor_t *col, int permanent)
{
#ifdef USE_OPENGL
    FCOLOR(p).cv = col->col;
    if (permanent)
        tgtCol[PARTICLE_ID(p)].col = FCOLOR(p).cv;    
#else
    ICOLOR(p).col32 = (uint32_t)(col->c[2] * 255.f) | 
            ((uint32_t)(col->c[1] * 255.f)<<8) | 
            ((uint32_t)(col->c[0] * 255.f) << 16);
#endif
}

void setParticleColorAlpha (particle *p, uint32_t a, int permanent)
{
#ifdef USE_OPENGL
    FCOLOR(p).c[3] = (float)a / 255.0;
    if (permanent)
        dflCol[PARTICLE_ID(p)].col = tgtCol[PARTICLE_ID(p)].col = FCOLOR(p).cv;    
#endif
}

void setParticleColorARGB (particle *p, uint32_t argb, int permanent, uint32_t mask)
{
#ifdef USE_OPENGL
    if (IS_CPU_PARTICLE(p))
    {
        if (CH_GET_R(mask))
            FCOLOR(p).c[0] = (float)CH_GET_R(argb) / 255.0;
        if (CH_GET_G(mask))
            FCOLOR(p).c[1] = (float)CH_GET_G(argb) / 255.0;
        if (CH_GET_B(mask))
            FCOLOR(p).c[2] = (float)CH_GET_B(argb) / 255.0;
        if (CH_GET_A(mask))
            FCOLOR(p).c[3] = (float)CH_GET_A(argb) / 255.0;
        if (permanent)
            dflCol[PARTICLE_ID(p)].col = tgtCol[PARTICLE_ID(p)].col = FCOLOR(p).cv;    
    }
    else
    {
        ICOLOR(p).col32 = (ICOLOR(p).col32 & ~mask) | (argb & 0x00ffffff & mask);
    }
#else
    ICOLOR(p).col32 = (ICOLOR(p).col32 & ~mask) | (argb & 0x00ffffff & mask);
#endif
}

uint32_t getParticleColorARGB (particle *p)
{
#ifdef USE_OPENGL
    if (IS_CPU_PARTICLE(p))
    {
        COLOR_TYPE *fc = &tgtCol[PARTICLE_ID(p)];
        uint32_t col = 0;

        CH_SET_R(col, (int)(fc->c[0] * 255.0));
        CH_SET_G(col, (int)(fc->c[1] * 255.0));
        CH_SET_B(col, (int)(fc->c[2] * 255.0));
        CH_SET_A(col, (int)(fc->c[3] * 255.0));

        return col;
    }
    else
    {
        return ICOLOR(p).col32;
    }
#else
    return ICOLOR(p).col32;
#endif

}


Uint32 rgb32ToSurfaceColor (SDL_Surface *s, Uint32 col)
{
    return SDL_MapRGB (s->format, CH_GET_R(col), CH_GET_G(col), CH_GET_B(col));
}

void HSL2RGB(float h, float s, float l, float* outR, float* outG, float* outB)
{
    float temp1, temp2;
    float temp[3];
    int i;

//    h = (h * 3.0f) / M_PI;

    // Check for saturation. If there isn't any just return the luminance value for each, which results in gray.
    if(s == 0.0)
    {
        *outR = l;
        *outG = l;
        *outB = l;
        return;
    }

    // Test for luminance and compute temporary values based on luminance and saturation 
    if(l < 0.5)
        temp2 = l * (1.0 + s);
    else
        temp2 = l + s - l * s;
    temp1 = 2.0 * l - temp2;

    // Compute intermediate values based on hue
    temp[0] = h + 1.0 / 3.0;
    temp[1] = h;
    temp[2] = h - 1.0 / 3.0;

    for(i = 0; i < 3; ++i)
    {
        // Adjust the range
        if(temp[i] < 0.0)
            temp[i] += 1.0;
        if(temp[i] > 1.0)
            temp[i] -= 1.0;


        if(6.0 * temp[i] < 1.0)
            temp[i] = temp1 + (temp2 - temp1) * 6.0 * temp[i];
        else {
            if(2.0 * temp[i] < 1.0)
                temp[i] = temp2;
            else {
                if(3.0 * temp[i] < 2.0)
                    temp[i] = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - temp[i]) * 6.0;
                else
                    temp[i] = temp1;
            }
        }
    }

    // Assign temporary values to R, G, B
    *outR = temp[0];
    *outG = temp[1];
    *outB = temp[2];

}

/* convert rgb to hsl. 
 * returns 1 on success, 0 if it fails (h/s/l is black in this case).
 */
int RGB2HSL (float r, float g, float b, float*h, float *s, float *l)
{
    float v;
    float m;
    float vm;
    float r2, g2, b2;

    *h = 0.0; // default to black
    *s = 0.0;
    *l = 0.0;

    v = MAX(r,g);
    v = MAX(v,b);
    m = MIN(r,g);
    m = MIN(m,b);
    *l = (m + v) / 2.0;

    if (*l <= 0.0)
    {
        return 0;
    }
    vm = v - m;
    *s = vm;

    if (*s > 0.0)
    {
        *s /= (*l <= 0.5) ? (v + m ) : (2.0 - v - m) ;
    }
    else
    {
        return 0;
    }

    r2 = (v - r) / vm;
    g2 = (v - g) / vm;
    b2 = (v - b) / vm;
    if (r == v)
    {
        *h = (g == m ? 5.0 + b2 : 1.0 - g2);
    }
    else if (g == v)
    {
        *h = (b == m ? 1.0 + r2 : 3.0 - b2);
    }
    else
    {
        *h = (r == m ? 3.0 + g2 : 5.0 - r2);
    }
    *h /= 6.0;
//    *h = ((6.0f - *h) * M_PI) / 3.0f;
//    *h = (6.0f - *h);

    *h = 1.0f - *h;


    return 1;
}

#define _CV_STEP        (1.f / 6.f)
static int cv_to_hue[6] = 
{
    0,
    4,
    2,
    3,
    1,
    5
};

float cvToHue (int cv)
{
    if ((cv < 0) || (cv > 5))
        return 0.f;
    
    return (float)cv_to_hue[cv] / 6.f ;
}

static int paintParticle (particle *p, void *arg)
{
    int perm = *(int*)arg;
    if (shiftPressed())
    {
#ifdef USE_OPENGL
        setParticleColorF (p, &dflCol[PARTICLE_ID(p)], perm);
#else
        setParticleColorARGB (p, dflCol[PARTICLE_ID(p)], perm, 0xffffffff);
#endif
    }
    else
    {
        setParticleColorARGB (p, ARGB(paintColor.current.val.cv.argb), perm, pickerMask());
    }
    
    return 0;
}

int
doDrawPaint (int x, int y, int dx, int dy, int permanent, int once)
{
    walkStencilParticles (x, y, (FUNCPTR)paintParticle, (void*)&permanent);
#ifdef USE_OPENCL
    /* mark all cursor pixels, clear old markers
     */
    vec_t pos;
    int op;
    pos.x = x;
    pos.y = y;

    op = CL_CHATTR_ALPHA;
    if (pickerMask() & 0x00ffffff)
        op |= CL_CHATTR_COLOR;
    
    if (permanent)
        op |= CL_CHATTR_BASECOL;
        
    if (once)
        op |= CL_CHATTR_ONCE;
    
    clMarkCursorPixels (x, y, 1 | CL_CLEAR_MARKERS);
    clSchedChattrMarked (ARGB(paintColor.current.val.cv.argb), 
                    0, 0, 0, &pos, 0, op);
#endif

    return 1;
}

typedef struct
{
    float r, g, b, a;
    int count;
} colorPick_t;

static int getColorParticle (particle *p, void *arg)
{
    colorPick_t *pick = (colorPick_t*)arg;

#ifdef USE_OPENGL
    pick->r += p->color.c[0];
    pick->g += p->color.c[1];
    pick->b += p->color.c[2];
    pick->a += p->color.c[3];
#else
    pick->r += p->color.icolor.c[0];
    pick->g += p->color.icolor.c[1];
    pick->b += p->color.icolor.c[2];
    pick->a += p->color.icolor.c[3];
#endif

    pick->count++;
    
    return 0;
}

int
doDrawPickColor (int x, int y, int dx, int dy, float *argb)
{
    colorPick_t pick;

    pick.count = 0;
    pick.a = pick.r = pick.g = pick.b = 0.0f;

    walkStencilParticles (x, y, (FUNCPTR)getColorParticle, (void*)&pick);

    if (pick.count == 0)
        return 0;

    pick.r /= (float)pick.count;
    pick.g /= (float)pick.count;
    pick.b /= (float)pick.count;
    pick.a /= (float)pick.count;

    argb[0] = pick.a;
    argb[1] = pick.r;
    argb[2] = pick.g;
    argb[3] = pick.b;

    return 1;
}

void drawFctPaint (int x, int y, int act, int mode)
{
    int event = EVENT_CODE(act);
    int mouseButton = BUTTON_ID(act);
    int permanent = 0;
    int pick = 0;
    float argb[4];

    CHECK_DRAWING

    switch (mouseButton)
    {
        case 1:
            permanent = 0;
            break;
        case 2:
            pick = 1;
            break;
        case 3:
            permanent = 1;
            break;
        default:
            return;
    }

    switch (event)
    {
        case BUTTON_PRESS:
            if (pick)
            {
                if (doDrawPickColor (x, y, 0, 0, argb))
                {
                    pickerSetARGB (argb[0], argb[1], argb[2], argb[3]);
                }
                return;
            }

            doDrawPaint (x, y, 0, 0, permanent, (mode == DR_STAMP));
            break;

        case BUTTON_MOVE:
            if (mode != DR_STAMP)
                doDrawPaint (x, y, 0, 0, permanent, 0);
            break;

        case BUTTON_RELEASE:
            if (pick)
            {
                if (doDrawPickColor (x, y, 0, 0, argb))
                {
                    pickerSetARGB (argb[0], argb[1], argb[2], argb[3]);
                }
                return;
            }

            if (mode != DR_STAMP)
                doDrawPaint (x, y, 0, 0, permanent, 1);

            break;
    }
}


/******************************************************************************
 * Color Value encoding support functions
 *
 * a color value (colorVal_t) encodes colors as
 * - 12-bit signed x/y value, which is the coordinate in the color wheel
 *   Allowed range is -2000 to 2000, where +-2000 corresponds to a normalized
 *   value of +-1.
 * - 8-bit saturation (0=black, 255=white)
 * - an argb value, where the rgb value corresponds to above x/y/sat,
 *   a is the alpha channel value (OpenGL only)
 */

/* encode alpha channel in cv 
 */
void setAlpha (colorVal_t *cv, float a)
{
    cv->argb &= 0x00ffffff;
    cv->argb |= (((int)(a * 255.0)) & 0xff) << 24;
}

/* decode alpha channel from cv 
 * returns alpha normalized format
 */
float getAlpha (colorVal_t *cv)
{
    int a = (cv->argb >> 24) & 0xff;

    return (float)a / (float)255.0;
}

/* Convert normalized x/y value into hue (angle) and saturation (0..1)
 */
void getColorWheelValue(float dx, float dy, float *outH, float *outS)
{
    float d = sqrtf(dx*dx + dy*dy);

    *outS = d;
    *outH = acosf((float)dx / d) / M_PI / 2.0f;

    if (dy < 0)
    {
        *outH = 1.0 - *outH;
    }
}


/* Randomize a given color value, using a box around the encoded x/y value
 */
void randomizeColorVal (colorVal_t *cv, int box)
{
    int b2 = box * 2;
    int dx, dy;
    int x, y;
    int i;

    if (box == 0)
        return;

    /* 5 attempts to stay within allowed range 
     */
    for (i = 0; i < 5; i++)
    {
        dx = RND(b2) - box;
        dy = RND(b2) - box;

        x = (int)cv->x + dx;
        y = (int)cv->y + dy;

        if (LENQ(x,y) < (XY_INT_MAX*XY_INT_MAX))
        {
            cv->x = x & 0xfff;
            cv->y = y & 0xfff;

            cvRGBUpdate (cv);

            return;
        }
    }
}

/* encode HSL value and alpha channel into color structure and update cv->argb.
 */
void encodeHSLA (colorVal_t *cv, float hue, float sat, float lum, float alpha)
{
    float dx, dy;

    hue *= 2 * M_PI;
    
    dx = cos (hue) * sat;
    dy = -sin (hue) * sat;

    encodeColorVec (cv, dx, dy, lum, alpha);
}

/* encode ARGB value into color structure and update x/y/sat
 */
void encodeARGB (colorVal_t *cv, float a, float r, float g, float b)
{
    float h, s, l;

    RGB2HSL (r, g, b, &h, &s, &l);

    encodeHSLA (cv, h, s, l, a);

    /* encodeHSLA has updated the RGB value. Restore the input ARGB to
     * prevent rounding errors
     */
    cv->argb = ((uint32_t)(r * (float)0xff) << 16) | 
                ((uint32_t)(g * (float)0xff) <<  8) |
                ((uint32_t)(b * (float)0xff) <<  0) |
                ((uint32_t)(a * (float)0xff) << 24);
}


/* encode color vector (normalized x/y coordinate of color wheel), luminosity and
 * alpha channel into color structure and update cv->argb.
 */
void encodeColorVec (colorVal_t *cv, float dx, float dy, float lum, float alpha)
{
    float fl = sqrtf(dx*dx + dy*dy);

    if (fl > 1.0f)
    {
        dx /= fl;
        dy /= fl;
    }

    cv->x = (int)(dx * XY_INT_MAXF) & 0xfff;
    cv->y = (int)(dy * XY_INT_MAXF) & 0xfff;
    cv->lum = (int)(lum * 255.0f) & 0xff;

    setAlpha (cv, alpha);
    cvRGBUpdate (cv);

}


/* Decode color value into normalized x/y vector in the color
 * wheel and the luminosity
 */
void decodeColorVec (colorVal_t *cv, float *dx, float *dy, float *lum,
                     float *alpha)
{
    *dx = (float)cv->x / XY_INT_MAXF;
    *dy = (float)cv->y / XY_INT_MAXF;
    *lum = (float)cv->lum / 255.f;
    *alpha = getAlpha (cv);

}

/* Update RGB value based on x/y/lum in colorVal structure
 * Keep alpha channel value.
 */
void cvRGBUpdate (colorVal_t *cv)
{
    float        h, s, r, g, b, a;
    float        dx, dy, lum;

    /* convert to normalized vector/luminosity 
     */
    decodeColorVec (cv, &dx, &dy, &lum, &a);

    /* get HSL value
     */
    getColorWheelValue(dx, dy, &h, &s);

    /* convert to RGB 
     */
    if (s <= 1.0)
    {
        HSL2RGB(h, s, lum, &r, &g, &b);
    }
    else
    {
        r = g = b = a = 0.0f;
    }

    cv->argb = ((uint32_t)(r * 0xff) << 16) | 
                ((uint32_t)(g * 0xff) <<  8) |
                ((uint32_t)(b * 0xff) <<  0) |
                ((uint32_t)(a * 0xff) << 24);
}
