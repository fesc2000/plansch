/* Copyright (C) 2008-2016 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"
#include <stdio.h>
#include <fcntl.h>
#include <CL/cl.h>
#include <CL/cl_ext.h>
#include <CL/cl_gl.h>
#include "opencl.h"

#undef NEW_CREATE

/* Locals */
#define JOB_QUEUE_SIZE        0x10000
static clJob_t        jobQueue[JOB_QUEUE_SIZE];
static int        jobQueueHead, jobQueueTail;
static int        jobQueueHeadWorking;
static int        haveParticleAddJobs = 0;
static jobDispatch_t       
                clDispatchTbl[JOB_MAXCNT];
static int        initialized = 0;

/* globals */

cl_int4 *jobBufferHost;
int jobBufferEntries = 0;


/* Implementation */

/* get entry in job queue
 * delay some time if queue is full
 */
static clJob_t *getJobQueueEntry(void)
{
    int h = (jobQueueHead + 1) % JOB_QUEUE_SIZE;
    int t = planschTime();
    int delayed = 0;
    clJob_t *rc = &jobQueue[jobQueueHead];
    
    while (h == jobQueueTail)
    {
        /* If GPU runs in main thread, do a GPU run now and 
         * check a second time if a job queue entry is available
         */
        if (!oglDedicatedThread())
        {
            clCommitJobs();
            oglWork (needScreenUpdate (planschTime()));

            if (h == jobQueueTail)
            {
                printf ("getJobQueueEntry failed, giving up\n");
                return NULL;
            }
        }
        else
        {
            usleep(10);
            if ((t - planschTime()) > 1000)
            {
                if (!delayed)
                {
                    /* commit pending jobs and try again
                     */
                    clCommitJobs();
                    t = planschTime();
                    delayed = 1;
                    printf ("getJobQueueEntry timed out, trying commit\n");
                }
                else
                {   
                    printf ("getJobQueueEntry timed out, giving up\n");
                    return NULL;
                }
            }
        }
    }
    
    /* new head will be commited by clCommitJobs()
     */
    jobQueueHead = h;
    return rc;
}

/* add request for new GPU particle into the job queue
 * queue is handled in the GPU thread.
 *
 * Needs explicit commit.
 */
int clSchedNewParticle (vec_t *pos, 
    vec_t *vec, 
    int charge, 
    int mass, 
    uint32_t col, 
    int links, 
    uint32_t flags,
    unsigned char size)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return 0;

    /* Avoid creation of new particles at the screen pixels
     * where other particles exist that have been created in the current
     * draw operation:
     * - cpuScrFlags is a bit field where the main thread remembers
     *   whether there is a new particle at the current posision
     *   (and rejects new particles it it's the case).
     *   This is to avoid creating unnecessary entries in the job queue.
     * - The cpuScrFlags is cleared as soon as the creation jobs are 
     *   commited to the GPU thread.
     * - The job handling in the GPU thread does the same, using the
     *   gpuScrFlagsHost bit field (which is the host data for the 
     *   GPU buffer "gpuScrFlags"):
     *  -- Particles are not created if a screen pixel is marked
     *  -- When creating a GPU particle the corrseponding bit in the 
     *     gpuScrFlagsHost field is set.
     *  -- A regular iteration step if the particle engine is executed.
     *  -- As long as there might particles with the "new" flag set
     *     indicated by the "haveNewParticles" flag):
     *     the kernel getNewFlags() is executed after each particle 
     *     engine iteration. It re-intializes the gpuScrFlags buffer 
     *     using the current position of the particles.
     *
     * If the draw operation is stopped (mouse button released), all 
     * bit fields are cleared, the "new" marker of all gpu particles is
     * cleared, and the getNewFlags() kernel execution is stopped.
     */
    uint off = SCREENC(pos->x)+SCREENC(pos->y)*wid;

    if (flags & CL_MFLG_NEW)
    {
            if (SIFLAG_ISSET(cpuScrFlags, off))
                return 0;
            
            SIFLAG_SET(cpuScrFlags, off);
            cpuScrFlagsDirty = 1;
    }

    pJob->what = 
        GETVAL32(clOscillator) ? CL_JOB_CREATE_OSCPART : CL_JOB_CREATE_PART;

    pJob->pos = *pos;
    pJob->vec = *vec;
    pJob->charge = charge;
    pJob->mass = mass;
    pJob->tpl = getMpTemplate();
    pJob->color = col;
    pJob->links = links;
    pJob->flags = flags;
    pJob->size = size;
    
    haveParticleAddJobs = 1;

    return 1;
}

void clSchedClearNewFlags (void)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_CLEARNEW;

    clCommitJobs();
}

void clSchedMkblob (void)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_MKBLOB;

    clCommitJobs();
}

/* Clear all CL particles (immedeate commit)
 */
void clSchedClearAllParticles()
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;


    pJob->what = CL_JOB_DELETEALL;
    
    clCommitJobs();
}

/* Clear all particle trails (immedeate commit)
 */
void clSchedClearTrails()
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_CLEARTRAILS;
    
    clCommitJobs();
}

/* Kick filed at position (immedate commit)
 */
void clSchedKickFieldAt (vec_t *pos, int value)
{
    uint off = pos->d.x + pos->d.y * wid;
    
    if (off >= wid*hei)
        return;

    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_FIELD_IMPULSE;
    pJob->pos.x = off;
    pJob->pos.y = value;
    
    clCommitJobs();
}

/* Kick filed at position (no commit)
 */
void clSchedHeatAt (uint off, int value)
{
    if (off >= wid*hei)
        return;

    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_HEAT;
    pJob->pos.x = off;
    pJob->pos.y = value;
}

/* Mark pixels (delayed commit)
 */
int clSchedMarkAt (int ix, int iy, int mark)
{
    int sx = SCREENC(ix);
    int sy = SCREENC(iy);
    uint off = sx + sy * wid;
    
    if (off >= wid*hei)
        return 0;
    
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return 0;

    pJob->what = CL_JOB_MARK_PIXEL;
    pJob->vec.x = off;
    pJob->vec.y = mark;
    

    return 0;
}

/* Mark/unmark pixels for screeninfo ptr. (mark = 1/0)
 * Callback to siWalkMarked.
 */
static int clMarkSi (screeninfo *pSi, intptr_t mark)
{
    clSchedMarkAt (INTERNC(SI_X(pSi)), INTERNC(SI_Y(pSi)), (int)mark);
    
    return 1;
}

void particleDump (gpuParticleSet_t *pSet, int all, int hostOnly, char *when)
{
    int i;
    attr_t *attr = (attr_t*)CURRENT_ATTR(pSet)->hostBuffer;
    float *pos = (float*)CURRENT_POS(pSet)->hostBuffer;
    float *vec = (float*)CURRENT_VEC(pSet)->hostBuffer;
    float *col = (float*)CURRENT_COL(pSet)->hostBuffer;
    uint32_t *bcol = (uint32_t*)CURRENT_BASE_COL(pSet)->hostBuffer;

    if (!hostOnly)
    {
            clBufferToHost (CURRENT_ATTR(pSet), 0);
            clBufferToHost (CURRENT_POS(pSet), 0);
            clBufferToHost (CURRENT_VEC(pSet), 0);
            clFinish(commandQueue);
    }

    printf ("%s\n", when);

    for (i = 0; i < 10; i++)
    {
        printf ("%d: a[%08x] p[%.2f %.2f] v[%.2f %.2f] c[%.2f %.2f %.2f %.2f] bc[%08x]\n",
            i,
            attr[i].flags,
            pos[(i<<1) + 0],
            pos[(i<<1) + 1],
            vec[(i<<1) + 0],
            vec[(i<<1) + 1],
            col[(i<<2) + 0],
            col[(i<<2) + 1],
            col[(i<<2) + 2],
            col[(i<<2) + 3],
            bcol[i]
            );

        if (!all && attr[i].flags == 0)
            break;
    }
}

#ifndef NEW_CREATE

/* Write chunk of particle data (pSet->startId to pSet->lastId) from current buffers
 * to GPU.
 * Return amount of elements written.
 */
int writePdata (gpuParticleSet_t *pSet)
{    
    int writeStart, writeCount;

    writeCount = pSet->lastId - pSet->startId;

    if (writeCount)
    {
        writeStart = pSet->startId;

        clBufferToGPUPart (CURRENT_ATTR(pSet),                1, writeStart, writeCount);
        clBufferToGPUPart (ALT_ATTR(pSet),                1, writeStart, writeCount);
        clBufferToGPUPart (CURRENT_POS(pSet),                1, writeStart, writeCount);
        clBufferToGPUPart (CURRENT_VEC(pSet),                1, writeStart, writeCount);
        clBufferToGPUPart (ALT_COL(pSet),                1, writeStart, writeCount);
        clBufferToGPUPart (CURRENT_COL(pSet),                1, writeStart, writeCount);
        clBufferToGPUPart (CURRENT_BASE_COL(pSet),        1, writeStart, writeCount);
        clBufferToGPUPart (pSet->particleConnections,        1, writeStart, writeCount);

        pSet->startId = pSet->lastId;
    }

    return writeCount;
}

static int processParticleJob (gpuParticleSet_t *pSet, clJob_t *job)
{
    int                 j, k;
    int                 count;
    int                 firstId;
    mPartTpl_t        *tpl  = job->tpl;
    attr_t        *attr = (attr_t*)CURRENT_ATTR(pSet)->hostBuffer;
    float        *pos  = (float*)CURRENT_POS(pSet)->hostBuffer;
    float        *vec  = (float*)CURRENT_VEC(pSet)->hostBuffer;
    cl_float4        *col  = (cl_float4*)pSet->particleColors;
    cl_uchar4        *bcol  = (cl_uchar4*)CURRENT_BASE_COL(pSet)->hostBuffer;

    count = tpl ? tpl->num : 1;

    if (!pSet->pdataRead)
    {
        /* compress particle tables so that we can simply add the new particles at the end
         */
        clCompressTables (pSet, 0);


        pSet->pdataRead = 1;

        pSet->startId = pSet->particleCount;
        pSet->lastId = pSet->particleCount;
    }

    /* the first index to use. Make sure index 0 is always empty
     */
    if (pSet->lastId == 0)
        pSet->lastId = 1;

    firstId = pSet->lastId;

    if (firstId + count >= pSet->maxGpuParticles)
        return 0;

    for (j = 0; j < count; j++)
    {
        mPartEntry_t *e = NULL;
        
        if (tpl)
            e = &tpl->tbl[j];
        
        int pid = pSet->lastId++;

        if (pid == 1)
        {
            /* invalidate particle with index 0
             */
            attr[0].flags = 0;
            attr[0].radius = 0.0f;
            pos[0] = pos[1] = vec[0] = vec[1] = 0.0f;
            memset (pSet->particleConnections->hostBuffer, 0, sizeof(cl_uint8));
        }

        if (job->what == CL_JOB_CREATE_PART)
        {
            attr[pid].flags = CL_TYPE_MOVING | ENCODE_MASS(job->mass) | ENCODE_CHARGE(job->charge) |
                        (job->flags & (CL_MFLG_INVERT_FRC|CL_MFLG_NEW|CL_MFLG_BLOB_NEW|CL_MFLG_COLLIDE|CL_MFLG_WIMP|CL_MFLG_STICKY));
            attr[pid].radius = (float)job->size / (float)(255 * 2);

            pos[pid*2 + 0] = (float)job->pos.x / (float)GRID;
            pos[pid*2 + 1] = (float)job->pos.y / (float)GRID;
            vec[pid*2 + 0] = (float)job->vec.x / (float)(GRID/10);
            vec[pid*2 + 1] = (float)job->vec.y / (float)(GRID/10);

            cl_uint8 * conn = &((cl_uint8*)(pSet->particleConnections->hostBuffer))[pid];
            memset ((char*)conn, 0, sizeof(*conn));
            if (tpl == NULL)
            {
                if (job->links)
                {
                    conn->s[7] = job->links;
                }
                else
                {
                    attr[pid].flags |= CL_MFLG_LINKFULL;
                }
            }
            else
            {
                for (k = 0; k < e->nLinks; k++)
                {
                    if (e->linkTo[k])
                    {
                        conn->s[k] = firstId + e->linkTo[k]-1;
                        conn->s[6]++;
                    }
                }
                conn->s[7] = e->nLinks;
                
                pos[pid*2 + 0] += e->ox;
                pos[pid*2 + 1] += e->oy;
                
                attr[pid].flags |= CL_MFLG_LINKED;

                if (conn->s[6] >= conn->s[7])
                    attr[pid].flags |= CL_MFLG_LINKFULL;
            }
                
            if (randomPlacement)
            {
                pos[pid*2 + 0] += FRND(1.0/1000.0) - (1.0/2000.0);
                pos[pid*2 + 1] += FRND(1.0/1000.0) - (1.0/2000.0);
            }
            
            if (job->flags & CL_MFLG_NEW)
                pSet->haveNewParticles = 1;
        }

        col[pid].s[0] = (float)(bcol[pid].s[0] = CH_GET_R(job->color)) / 255.0f;
        col[pid].s[1] = (float)(bcol[pid].s[1] = CH_GET_G(job->color)) / 255.0f;
        col[pid].s[2] = (float)(bcol[pid].s[2] = CH_GET_B(job->color)) / 255.0f;
        col[pid].s[3] = (float)(bcol[pid].s[3] = CH_GET_A(job->color)) / 255.0f;
    }
    
    return 1;
}
#else
static int processParticleJob (gpuParticleSet_t *pSet, clJob_t *job)
{
    int                 j, k;
    int                 count;
    int          pid;
    int          ids[100];
    particleJob_t *gpuJob;
    mPartTpl_t        *tpl  = job->tpl;

    count = tpl ? tpl->num : 1;

    if (count > 100)
        return 0;

    for (j = 0; j < count; j++)
    {
        ids[j] = clGetFreeParticle (pSet);

        if (ids[j] == -1)
            return 0;
    }

    for (j = 0; j < count; j++)
    {
        mPartEntry_t *e = NULL;

        if (tpl)
            e = &tpl->tbl[j];

        pid = ids[j];

        pSet->maxPid = MAX(pid, pSet->maxPid);

        gpuJob = clGetCreateSlot (pSet);
        if (!gpuJob)
            return 0;

        gpuJob->attr.flags = CL_TYPE_MOVING | ENCODE_MASS(job->mass) | ENCODE_CHARGE(job->charge) |
                        (job->flags & (CL_MFLG_INVERT_FRC|CL_MFLG_NEW|CL_MFLG_BLOB_NEW|CL_MFLG_COLLIDE|CL_MFLG_WIMP|CL_MFLG_STICKY));
        gpuJob->attr.radius = (float)job->size / (float)(255 * 2);

        gpuJob->pos.x = (float)job->pos.x / (float)GRID;
        gpuJob->pos.y = (float)job->pos.y / (float)GRID;
        gpuJob->vec.x = (float)job->vec.x / (float)(GRID/10);
        gpuJob->vec.y = (float)job->vec.y / (float)(GRID/10);

        if (tpl == NULL)
        {
            if (job->links)
            {
                gpuJob->links.s[7] = job->links;
            }
            else
            {
                gpuJob->attr.flags |= CL_MFLG_LINKFULL;
            }
        }
        else
        {
            for (k = 0; k < e->nLinks; k++)
            {
                if (e->linkTo[k])
                {
                    gpuJob->links.s[k] = ids[e->linkTo[k]-1];
                    gpuJob->links.s[6]++;
                }
            }
            gpuJob->links.s[7] = e->nLinks;

            gpuJob->pos.x += e->ox;
            gpuJob->pos.y += e->oy;

            gpuJob->attr.flags |= CL_MFLG_LINKED;

            if (gpuJob->links.s[6] >= gpuJob->links.s[7])
                gpuJob->attr.flags |= CL_MFLG_LINKFULL;
        }

        if (randomPlacement)
        {
            gpuJob->pos.x += FRND(1.0/1000.0) - (1.0/2000.0);
            gpuJob->pos.y += FRND(1.0/1000.0) - (1.0/2000.0);
        }

        if (job->flags & CL_MFLG_NEW)
            pSet->haveNewParticles = 1;

        gpuJob->col.s[0] = CH_GET_R(job->color);
        gpuJob->col.s[1] = CH_GET_G(job->color);
        gpuJob->col.s[2] = CH_GET_B(job->color);
        gpuJob->col.s[3] = CH_GET_A(job->color);

        gpuJob->pid = pid;
    }

    return 1;
}
#endif


static void jobFillAll (clJob_t *job)
{
    void clFillAll (gpuParticleSet_t *pSet, int arg);

    clFillAll (&particleSet[0], job->flags);
}

static void jobCreatePart (clJob_t *job)
{
    int off = SCREENC(job->pos.x)+SCREENC(job->pos.y)*wid;

    if (job->flags & CL_MFLG_NEW)
            if (SIFLAG_ISSET(gpuScrFlagsHost, off))
                return;

    if (processParticleJob (&particleSet[movingPartSet], job))
    {
        if (job->flags & CL_MFLG_NEW)
                SIFLAG_SET(gpuScrFlagsHost, off);
    }
}

static void jobCreateOscpart (clJob_t *job)
{
    int off = SCREENC(job->pos.x)+SCREENC(job->pos.y)*wid;

    if (SIFLAG_ISSET(gpuScrFlagsHost, off))
        return;

    if (processParticleJob (&particleSet[oscPartSet], job))
        SIFLAG_SET(gpuScrFlagsHost, off);
}

static void jobMarkParticles (clJob_t *job)
{
    JOB_ACTIVATE(CL_JOB_MARK_PARTICLES);
    particleMarkOp = job->flags;
    JOB_ACTIVATE(CL_JOB_UNMARK_AFTER);
}

void jobDelMarked (clJob_t *job)
{
    JOB_ACTIVATE(CL_JOB_DEL_MARKED);
    JOB_ACTIVATE(CL_JOB_UNMARK_AFTER);
}

static void jobGrabMarked (clJob_t *job)
{
    if (job->flags)
    {
        JOB_ACTIVATE(CL_JOB_GRAB_MARKED);
        JOB_ACTIVATE(CL_JOB_UNMARK_AFTER);
        clGrabCenter.x = SCREENC_F(job->pos.x);
        clGrabCenter.y = SCREENC_F(job->pos.y);
    }
    else
    {
        clClearGrabFlags(NULL);
    }
}

static void jobChattrMarked (clJob_t *job)
{
    if (job->flags & CL_CHATTR_STOP)
    {
        /* stop painting
         */
        PENDING_JOB_DONE(CL_JOB_CHATTR_MARKED);
        clClearPixelMarkers();
    }
    else
    {
        JOB_ACTIVATE(CL_JOB_CHATTR_MARKED);

        if (job->flags & CL_CHATTR_ONCE)
            JOB_ACTIVATE(CL_JOB_UNMARK_AFTER);

        currentChattrOp.op = SO_CHATTR | (job->flags & SO_CHATTR_MASK);
        currentChattrOp.color.s[0] = (float)CH_GET_R(job->color) / 255.0f;
        currentChattrOp.color.s[1] = (float)CH_GET_G(job->color) / 255.0f;
        currentChattrOp.color.s[2] = (float)CH_GET_B(job->color) / 255.0f;
        currentChattrOp.color.s[3] = (float)CH_GET_A(job->color) / 255.0f;
        currentChattrOp.attr = ENCODE_MASS(job->mass) | ENCODE_CHARGE(job->charge) 
                               | ((job->flags & CL_CHATTR_INVVAL) ? CL_MFLG_INVERT_FRC : 0)
                               | ((job->flags & CL_CHATTR_STVAL) ? CL_MFLG_STICKY : 0)
                               | ((job->flags & CL_CHATTR_COLLVAL) ? CL_MFLG_COLLIDE : 0)
                               | ((job->flags & CL_CHATTR_COLLVAL) ? 0: CL_MFLG_WIMP);

        currentChattrOp.maxLinks = job->links;
        currentChattrOp.radius = (float)job->size / (float)(255*2);

        /* search all particles on marked pixels with pixel offset
         */
        currentChattrOp.centerPos.x = SCREENC_F(job->pos.x);
        currentChattrOp.centerPos.y = SCREENC_F(job->pos.y);
    }
}
static void jobMarkPixel (clJob_t *job)
{
    if (jobBufferEntries >= JOB_FIELD_SIZE)
        return;

    int off = jobBufferEntries++; 

    jobBufferHost[off].s[0] = CL_JOB_MARK_PIXEL;
    jobBufferHost[off].s[1] = job->vec.x; /* offset */
    jobBufferHost[off].s[2] = job->vec.y; /* mark/unmark */
}

static void jobFieldImpulse (clJob_t *job)
{
    int grv = getValue (&particleAttraction);

    if (!grv)
        return;

    if (jobBufferEntries >= JOB_FIELD_SIZE)
        return;

    int off = jobBufferEntries++; 

    /* give force field a kick at a specific location
     * Each element in the GPU buffer consists of an offset into the field
     * and a "kick value" which is added once to the current
     * field change rate at this coordinate.
     */
    jobBufferHost[off].s[0] = CL_JOB_FIELD_IMPULSE;
    jobBufferHost[off].s[1] = job->pos.x; /* offset */
    jobBufferHost[off].s[2] = job->pos.y; /* strength */
}

static void jobHeat (clJob_t *job)
{
    if (jobBufferEntries >= JOB_FIELD_SIZE)
        return;

    int off = jobBufferEntries++; 

    jobBufferHost[off].s[0] = CL_JOB_HEAT;
    jobBufferHost[off].s[1] = job->pos.x; /* offset */
    jobBufferHost[off].s[2] = job->pos.y; /* strength */
}

void jobDeleteAll (clJob_t *job)
{
    int set;
    
    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        if (particleSet[set].particleCountAlgn)
        {
            clearParticleBuffers(&particleSet[set]);
        }
    }
}

static void jobClearTrails (clJob_t *job)
{
    int set;
    
    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        clearParticleTrails (&particleSet[set]);
    }
}

static void jobUnmarkAll (clJob_t *job)
{
    clClearPixelMarkers();
}

static void jobClearNew (clJob_t *job)
{
    clearNewMarkers();
}

static void jobMkblob (clJob_t *job)
{
    clMakeBlobFromNew (&particleSet[0]);
}

static void jobNotImplemented (clJob_t *job)
{
}

/* force pending transactions (GPU job buffer entries, particle creation jobs)
 * to be flushed to GPU
 */
void jobApplyPending (clJob_t *job)
{
    int set;

    /* check for work for the GPU job processing kernel 
     */
    if (jobBufferEntries)
    {
        /* clear out rest of host array */
        memset (&jobBufferHost[jobBufferEntries], 0, sizeof(cl_int4) * (JOB_FIELD_SIZE - jobBufferEntries));

        /* transfer to GPU */
        clBufferToGPU (jobBuffer, 1);

        /* the number of entries to be processed by the GPU. keep it aligned.
         */
        jobBufferEntries = (jobBufferEntries + 0x1fff) & ~0x1fff;
    }

    totalGpuParticles = 0;
#ifdef NEW_CREATE
    /* flush remaining particle creation jobs
     *
     */
    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        clCreateParticles (&particleSet[set]);

        if (particleSet[set].maxPid > particleSet[set].particleCount)
            setNGpuParticles (&particleSet[set], particleSet[set].maxPid);
        
        particleSet[set].dropCreateJobs = 0;
        particleSet[set].maxPid = 0;

        totalGpuParticles += particleSet[set].particleCountAlgn;
    }
#else
    /* write modified particle data back to GPU
     */
    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        int written = writePdata (&particleSet[set]);

        if (written)
        {
            setNGpuParticles (&particleSet[set], particleSet[set].particleCount + written);
        }
        totalGpuParticles += particleSet[set].particleCountAlgn;
    }
#endif
}   

/* Process Job-queue for OpenCl thread
 */
void processClJobQueue()
{
    clJob_t *job;
    int first = 1;
    int previous = 0;
    int set;
    
    if (jobQueueHeadWorking == jobQueueTail)
        return;

    jobBufferEntries = 0;

    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
#ifndef NEW_CREATE
        particleSet[set].pdataRead = 0;
        particleSet[set].startId = 0;
        particleSet[set].lastId = 0;
#endif
        particleSet[set].dropCreateJobs = 0;
        particleSet[set].maxPid = 0;
    }

    while (1)
    {
        job = &jobQueue[jobQueueTail];

        /* apply pending transactions if job type differs from previous 
         * (e.g. after a bulk of particle jobs)
         */
        if (!first && (previous != job->what))
            jobApplyPending (NULL);

        clDispatchTbl[job->what](job);

        jobQueueTail = (jobQueueTail + 1) % JOB_QUEUE_SIZE;

        if (jobQueueTail == jobQueueHeadWorking)
            break;

        first = 0;
        previous = job->what;
    }

    jobApplyPending(NULL);
}

/* transfer wall information to openCl buffer 
 */
void clWallSet (int x, int y)
{
    screeninfo *pSi;
    unsigned int off = x + y*wid;
    pSi = &siArray[off];
    cl_uchar        flag;
    line_t line;
    float *accelFloat;
    uint16_t *accelHalf;

    if (!initialized || !clWallsHost || (off >= wid * hei))
        return;
    
    accelFloat = (float*)accelBuffer->hostBuffer;
    accelHalf = (uint16_t*)accelBuffer->hostBuffer;
    flag = pSi->flags & CL_FLAG_MASK;

    clWallsHost[off].wallFlags = flag;
    memcpy(clWallsHost[off].brickNormals, pSi->normals, sizeof(clWallsHost[off].brickNormals));
    clWallsHost[off].friction = pSi->friction;

    if (pSi->flags & SI_IS_PUMP)
    {
        if (useHalfField)
        {
            accelHalf[off*2]   = floatToHalf((float)pSi->u.accel[0] / (float)327.680);
            accelHalf[off*2+1] = floatToHalf((float)pSi->u.accel[1] / (float)327.680);
        }
        else
        {
            accelFloat[off*2]   = (float)pSi->u.accel[0] / 327.680f;
            accelFloat[off*2+1] = (float)pSi->u.accel[1] / 327.680f;
        }
    }
    else
    {
        if (useHalfField)
        {
            accelHalf[off*2] = accelHalf[off*2+1] = floatToHalf(0.0f);
        }
        else
        {
            accelFloat[off*2] = accelFloat[off*2+1] = 0.0f;
        }
    }

    if (flag & (SI_LINE | SI_IN_POLY))
    {
        if (getLine (off, &line))
        {
            clWallsHost[off].lineStart.x = (float)line.start.x / MAX_SPEEDF;
            clWallsHost[off].lineStart.y = (float)line.start.y / MAX_SPEEDF;
            clWallsHost[off].lineNormal.x = (float)line.normal.x / (float)NORM_LEN;
            clWallsHost[off].lineNormal.y = (float)line.normal.y / (float)NORM_LEN;
            clWallsHost[off].lineDyDx = line.dyDx;
        }
        else
        {
            clWallsHost[off].wallFlags &= ~(SI_LINE | SI_IN_POLY);
        }
    }

    clWallUpdateFlag = 1;
}

/* unmark all pixels (immedeate commit)
 */
void clSchedUnmarkAll (void)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_UNMARK_ALL;
    
    clCommitJobs();
}

/* Mark/unmark pixels under current cursor (mark = 1/0)
 * If CL_CLEAR_MARKERS is ORED to mark, all pixels are unmarked before
 */
void clMarkCursorPixels (int x, int y, int mark)
{
    if (mark & CL_CLEAR_MARKERS) 
    {
        clSchedUnmarkAll();

        if ((mark & 1) == 0)
            return;
    }

    walkCursor (x, y, (FUNCPTR)clSchedMarkAt, (void*)(intptr_t)(mark & 1));
}



/* Transfer marker information fro screeninfo array to opencl
 */
void clTransferSiMarker (void)
{
    siWalkMarked ((FUNCPTR)clMarkSi, 1);
}


/* move all cl particles on marked pixels (immeadeate commit)
 */
void clSchedStartGrab (int ix, int iy)
{
    if (clDoGrab)
        return;
    
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_GRAB_MARKED;
    pJob->flags = 1;
    pJob->pos.x = ix;
    pJob->pos.y = iy;
    
    clCommitJobs();
}


/* stop grabbing cl particles
 */
void clSchedStopGrab(void)
{
    if (!clDoGrab)
        return;
    
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_GRAB_MARKED;
    pJob->flags = 0;

    clCommitJobs();
}

/* delete marked pixels (immeadeate commit)
 */
void clSchedDeleteStart(void)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_DEL_MARKED;
    
    clCommitJobs();
}

void clDeleteStop(void)
{
}

/* change attributes of marked particles (imm. commit)
 */
void clSchedChattrMarked (uint32_t argb, 
                     signed char charge, 
                     unsigned char mass, 
                     unsigned char links,
                     vec_t *ipos,
                     unsigned char size,
                     int flags)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_CHATTR_MARKED;
    pJob->charge = charge;
    pJob->mass  = mass;
    pJob->links = links;
    pJob->color = argb;
    pJob->flags = flags;
    pJob->pos = *ipos;
    pJob->size = size;
    
    clCommitJobs();
}

/* mark particles on marked pixels (imm. commit)
 */
void clSchedMarkParticles (int flags, vec_t *ipos)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_MARK_PARTICLES;
    pJob->flags = flags;
    pJob->pos = *ipos;

    clCommitJobs();
}

void clFollowStop(void)
{
    int set;

    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
        particleSet[set].following = 0;
}

void clSchedMarkAll(int mark)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_MARK_PARTICLES;
    pJob->flags = SO_SEARCH_ALL | (mark ? SO_MARK_ADD:SO_MARK_CLEAR);

    clCommitJobs();
}

/* change attributes of marked particles, or those
 * in bounding box (imm. commit)
 */
void clSchedChattrAll (uint32_t argb, 
                     signed char charge, 
                     unsigned char mass, 
                     unsigned char links,
                     SDL_Rect *r,
                     unsigned char size,
                     int flags)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_CHATTR_MARKED;
    pJob->charge = charge;
    pJob->mass  = mass;
    pJob->links = links;
    pJob->color = argb;
    pJob->flags = flags | SO_SEARCH_ALL;
    pJob->pos.x = r->x;
    pJob->pos.y = r->y;
    pJob->vec.x = r->w;
    pJob->vec.y = r->h;
    pJob->size = size;

    clCommitJobs();
}

/* Set a sync point where previous jobs are applied to the GPU
 * (e.g. bulk transactions like creating particles)
 */
void clSchedApplyPending (void)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_APPLY;

    clCommitJobs();
}

/* test pattern 
 */
void clSchedFill (int arg)
{
    clJob_t *pJob = getJobQueueEntry();

    if (!pJob)
        return;

    pJob->what = CL_JOB_FILL_ALL;
    pJob->flags = arg;

    clCommitJobs();
}

void clCommitJobs(void)
{
    if (cpuScrFlagsDirty)
    {
        SIFLAG_CLRALL(cpuScrFlags);
        cpuScrFlagsDirty = 0;
    }   

    debug ("Committing %d jobs\n", 
        ((jobQueueHead + JOB_QUEUE_SIZE) - jobQueueHeadWorking) % JOB_QUEUE_SIZE);

    jobQueueHeadWorking = jobQueueHead;
}

void clSync (void)
{
    int t = planschTime();

    while (jobQueueHead == jobQueueTail)
    {
        usleep(10);
        if ((t - planschTime()) > 1000)
                break;
    }
}

static void clInitDispatchTable(void)
{
    int i;
    
    for (i = 0; i < NELEMENTS(clDispatchTbl); i++)
        clDispatchTbl[i] = jobNotImplemented;

    clDispatchTbl[CL_JOB_CREATE_PART]     = jobCreatePart;
    clDispatchTbl[CL_JOB_FIELD_IMPULSE]   = jobFieldImpulse;
    clDispatchTbl[CL_JOB_CREATE_OSCPART]  = jobCreateOscpart;
    clDispatchTbl[CL_JOB_DELETEALL]       = jobDeleteAll;
    clDispatchTbl[CL_JOB_CLEARTRAILS]     = jobClearTrails;
    clDispatchTbl[CL_JOB_MARK_PIXEL]      = jobMarkPixel;
    clDispatchTbl[CL_JOB_GRAB_MARKED]     = jobGrabMarked;
    clDispatchTbl[CL_JOB_DEL_MARKED]      = jobDelMarked;
    clDispatchTbl[CL_JOB_UNMARK_ALL]      = jobUnmarkAll;
    clDispatchTbl[CL_JOB_CHATTR_MARKED]   = jobChattrMarked;
    clDispatchTbl[CL_JOB_FILL_ALL]        = jobFillAll;
    clDispatchTbl[CL_JOB_MARK_PARTICLES]  = jobMarkParticles;
    clDispatchTbl[CL_JOB_CLEARNEW]        = jobClearNew;
    clDispatchTbl[CL_JOB_HEAT]            = jobHeat;
    clDispatchTbl[CL_JOB_MKBLOB]          = jobMkblob;
    clDispatchTbl[CL_JOB_APPLY]           = jobApplyPending;
}

void clJobsInit(void)
{
    initialized = 1;
    jobQueueHead = jobQueueHeadWorking = jobQueueTail = 0;

    clInitDispatchTable();
}

void clRecordScreenSection (tape *Tape, SDL_Rect *pRect)
{
    attr_t *attr;
    cl_float2 *pos;
    cl_float2 *vec;
    uint32_t *bcol;
    gpuParticleSet_t *pSet;
    int set, i, r = 0; 
    particle p;

    memset (&p, 0, sizeof(p));

    for (set = 0; set < CL_NUM_PARTICLE_SETS; set++)
    {
        pSet = &particleSet[set];

        clBufferToHost (CURRENT_ATTR(pSet), 0);
        clBufferToHost (CURRENT_POS(pSet), 0);
        clBufferToHost (CURRENT_VEC(pSet), 0);
        clBufferToHost (CURRENT_BASE_COL(pSet), 0);

        attr = (attr_t*)CURRENT_ATTR(pSet)->hostBuffer;
        pos = (cl_float2*)CURRENT_POS(pSet)->hostBuffer;
        vec = (cl_float2*)CURRENT_VEC(pSet)->hostBuffer;
        bcol = (uint32_t*)CURRENT_BASE_COL(pSet)->hostBuffer;

        for (i = 0; i < pSet->particleCount; i++)
        {
            if ((attr[i].x & CL_TYPE_MOVING) == 0)
            {
                continue;
            }

            if (((int)pos[i].x < pRect->x) ||
                ((int)pos[i].x > pRect->x + pRect->w) ||
                ((int)pos[i].y < pRect->y) ||
                ((int)pos[i].y > pRect->y + pRect->h))
            {
                continue;
            }

            p.pos.x = pos[i].x * GRIDF;
            p.pos.y = pos[i].y * GRIDF;
            p.vec.x = vec[i].x * GRIDF;
            p.vec.y = vec[i].y * GRIDF;
            p.charge = PART_CHARGE(attr[i]);
            p.mass = PART_MASS(attr[i]);
            p.type = oclPartType();
            p.size = 255;
            ICOLOR(&p).col32 = bcol[i];

            recordNewParticle (Tape, &p);
            r++;
        }
        recordStartDraw (Tape, 0, oclPartType());
    }

    printf ("Recorded %d OCL particles\n", r);
}


