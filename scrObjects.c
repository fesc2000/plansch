/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"


scrPoly *worldPolys = NULL;
scrPoly *screenPolys = NULL;
static int worldPolysChanged = 0;

static sem_t        *mutex;


#define SX(po,i)        (po->flags & POLY_SCREEN) ? S_SCREENC(po->abs[i].d.x) : IX_TO_SX_S(po->abs[i].d.x)
#define SY(po,i)        (po->flags & POLY_SCREEN) ? S_SCREENC(po->abs[i].d.y) : IY_TO_SY_S(po->abs[i].d.y)

#define DO_LOCK        


#ifndef USE_OPENGL
static void toScreenFilled (scrPoly *po, int show)
{
    Sint16 *vx, *vy;
    int n = 0, max;
    int i;

    max = po->num + 1;

    vx = alloca(max * sizeof(*vx));
    vy = alloca(max * sizeof(*vy));

    for (i = 0; i < po->num; i++, n++)
    {
        vx[n] = SX(po, i);
        vy[n] = SY(po, i);
    }

    clipToDrawArea();
    myfilledPolygonColor (screen, vx, vy, n, COL_BRICK);
//    mypolygonColor (screen, vx, vy, n, COL_ORANGE);
    clipDisable();
}

static void toScreenImpl (scrPoly *po, int show)
{
    int i;
    int x1, y1, x2, y2;

    if (po->visible == show)
        return;

    if (po->flags & POLY_FILLED)
    {
        toScreenFilled (po, show);
    }

    for (i = 0; i < po->num; i++)
    {
        x1 = SX(po, i);
        y1 = SY(po, i);

        if (i+1 < po->num)
        {
            x2 = SX(po,i+1);
            y2 = SY(po,i+1);
        }
        else if (po->flags & POLY_OPEN)
        {
            break;
        }
        else
        {
            x2 = SX(po, 0);
            y2 = SY(po, 0);
        }

        drawLineF (x1, y1, x2, y2, lineCb, po->colors[i], 1, !show, 1);
    }

    po->visible = show;
}
#else
static void toScreenImpl (scrPoly *po, int show)
{
    /* filled polygons need to be tesselated (converted to triangles) first
     */
    if ((po->changed && (po->flags & POLY_FILLED)) ||
        ((po->flags & POLY_FILLED) && (po->triangles == NULL)))
    {
        if ((po->flags & (POLY_FILLED | POLY_CONVEX)) == POLY_FILLED)
        {
            if (po->triangles)
            {
                free (po->triangles);
                po->triangles = NULL;
            }

            po->numTri = to_poly (po->num, po->abs, &po->triangles);
        }
    }
}
#endif

static void toScreen (scrPoly *po, int show)
{

    semTake (mutex, -1);
    toScreenImpl (po, show);

    po->changed = 0;

    if ((po->flags & POLY_SCREEN) == 0)
    {
        worldPolysChanged = 1;
    }
    semGive (mutex);
}


void initPolys()
{
    worldPolysChanged = 0;
    mutex = semMCreate(0);
}

void polyDelete (scrPoly *po)
{
    scrPoly *p, *prev;

    if (!po)
        return;

    semTake (mutex, -1);

    po->refcount--;

    if (po->refcount > 0)
    {
        semGive (mutex);
        return;
    }

    toScreen(po, 0);

    /* search on world list */
    for (prev = NULL, p = worldPolys; p != NULL; p = p->next)
    {
        if (p == po)
        {
            if (prev)
            {
                prev->next = po->next;
            }
            else
            {
                worldPolys = po->next;
            }

            goto release;
        }
        prev = p;
    }

    /* search on screen list */
    for (prev = NULL, p = screenPolys; p != NULL; p = p->next)
    {
        if (p == po)
        {
            if (prev)
            {
                prev->next = po->next;
            }
            else
            {
                screenPolys = po->next;
            }

            goto release;
        }
        prev = p;
    }

    semGive (mutex);
    return;

release:
    if (po->triangles)
    {
        free (po->triangles);
        po->triangles = NULL;
    }

    if (po->colors)
        free (po->colors);

    if (po->offsets)
        free (po->offsets);

    if (po->abs)
        free (po->abs);

    free (po);

    semGive (mutex);
}

scrPoly *polyNew (int count, int flags, uint32 dflColor)
{
    scrPoly *po;

    po = (scrPoly*)malloc(sizeof(*po));

    if (po == NULL)
        return NULL;

    po->active = 0;
    po->visible = 0;
    po->num = 0;
    po->max = count;
    po->colors = malloc(count * sizeof(*po->colors));
    po->offsets = malloc(count * sizeof(*po->offsets));
    po->abs = malloc(count * sizeof(*po->abs));
    po->pos.d.x = po->pos.d.y = 0;
    po->dflCol = dflColor;
    po->flags = flags;
    po->changed = 0;
    po->triangles = NULL;
    po->numTri = 0;
    po->fillColor = 0x80808080;

    semTake (mutex, -1);
    if (flags & POLY_SCREEN)
    {
        po->next = screenPolys;
        screenPolys = po;
    }
    else
    {
        po->next = worldPolys;
        worldPolys = po;
    }

    po->refcount = 0;

    semGive (mutex);

    return (void*)po;
}


void polyAddCoord (scrPoly *po, int x, int y, uint32 col)
{
    int o;

    if (po == NULL)
        return;

    semTake (mutex, -1);
    if (po->num >= po->max)
    {
        po->max += 100;

        po->offsets = realloc ((void*)po->offsets, po->max * sizeof(*po->offsets));
        po->abs = realloc ((void*)po->abs, po->max * sizeof(*po->abs));

        if (po->colors)
            po->colors = realloc ((void*)po->colors, po->max * sizeof(*po->colors));
    }

    o = po->num;
    
    if (po->flags & POLY_SCREEN)
    {
        x = S_INTERNC(x);
        y = S_INTERNC(y);
    }

    if (po->active)
        toScreen (po, 0);

    po->offsets[o].d.x = x;
    po->offsets[o].d.y = y;
    
    po->abs[o].d.x = po->pos.d.x + x;
    po->abs[o].d.y = po->pos.d.y + y;

    if (po->colors)
        po->colors[o] = col ? col : po->dflCol;

    po->num++;
    po->changed = 1;

    if (po->active)
        toScreen (po, 1);

    semGive (mutex);

}

void polyChangeCoord (scrPoly *po, int num, int x, int y, uint32 col)
{
    if ((po == NULL) || (num >= po->num))
        return;

    semTake (mutex, -1);
    if (po->flags & POLY_SCREEN)
    {
        x = S_INTERNC(x);
        y = S_INTERNC(y);
    }

    if (po->active)
        toScreen (po, 0);

    po->offsets[num].d.x = x;
    po->offsets[num].d.y = y;
    po->abs[num].d.x = po->pos.d.x + x;
    po->abs[num].d.y = po->pos.d.y + y;
    po->colors[num] = col ? col : po->dflCol;
    po->changed = 1;

    if (po->active)
        toScreen (po, 1);

    semGive (mutex);
}

void polyClear (scrPoly *po)
{
    if (po == NULL)
        return;

    semTake (mutex, -1);

    toScreen (po, 0);
    po->changed = 1;

    po->num = 0;

    semGive (mutex);
}

void polyChangeFillColor (scrPoly *po, uint32 color)
{
    if (!po)
        return;

    semTake (mutex, -1);

    if (po->active)
        toScreen (po, 0);

    if (color == 0)
        color = po->dflCol;

    po->fillColor = color;

    if (po->active)
        toScreen (po, 1);

    semGive (mutex);
}
void polyChangeColor (scrPoly *po, int o, uint32 color)
{
    if (!po)
        return;

    semTake (mutex, -1);

    if (po->active)
        toScreen (po, 0);

    if (color == 0)
        color = po->dflCol;

    if (o == -1)
    {
        for (o = 0; o < po->num; o++)
        {
            po->colors[o] = color;
        }
    }
    else if ((o >= 0) && (o < po->num))
    {
        po->colors[o] = color;
    }

    if (po->active)
        toScreen (po, 1);

    semGive (mutex);
}

void polyChangeFlags (scrPoly *po, int flags, int mask)
{
    if (!po)
        return;

    semTake (mutex, -1);
    mask &= POLY_FILLED|POLY_CONVEX|POLY_BW1;

    po->flags = (po->flags & ~mask) | (flags & mask);

    po->changed = 1;
    po->visible = 0;

    if (po->active)
        toScreen (po, 1);

    semGive (mutex);
}

void polySetCoords (scrPoly *po, int num, vec_t *coords, uint32 *colors)
{
    int o;
    int x, y;

    if (po == NULL)
        return;

    semTake (mutex, -1);
    if (po->active)
        toScreen (po, 0);

    if (num > po->max)
    {
        po->max = num+10;

        po->offsets = realloc ((void*)po->offsets, po->max * sizeof(*po->offsets));
        po->abs = realloc ((void*)po->abs, po->max * sizeof(*po->abs));

        if (po->colors)
            po->colors = realloc ((void*)po->colors, po->max * sizeof(*po->colors));
    }

    for (o = 0; o < num; o++)
    {
        x = coords[o].d.x;
        y = coords[o].d.y;

        
        if (po->flags & POLY_SCREEN)
        {
            x = S_INTERNC(x);
            y = S_INTERNC(y);
        }

        po->offsets[o].d.x = x;
        po->offsets[o].d.y = y;
    
        po->abs[o].d.x = po->pos.d.x + x;
        po->abs[o].d.y = po->pos.d.y + y;

        if (po->colors)
        {
            if (colors)
                po->colors[o] = colors[o] ? colors[o] : po->dflCol;
            else
                po->colors[o] = po->dflCol;
        }

    }

    po->num = o;
    po->changed = 1;

    if (po->active)
        toScreen (po, 1);

    semGive (mutex);
}

void polyMove (scrPoly *po, int x, int y)
{
    int i;
    int dx, dy;

    if (po == NULL)
        return;

    if (po->flags & POLY_SCREEN)
    {
        x = S_INTERNC(x);
        y = S_INTERNC(y);
    }

    dx = x - po->pos.d.x;
    dy = y - po->pos.d.y;

    if ((dx == 0) && (dy == 0))
    {
        return;
    }

    semTake (mutex, -1);

    toScreen (po, 0);

    for (i = 0; i < po->num; i++)
    {
        po->abs[i].d.x = x + po->offsets[i].d.x;
        po->abs[i].d.y = y + po->offsets[i].d.y;
    }

    if (po->triangles)
    {
        for (i = 0; i < po->numTri; i++)
        {
            po->triangles[i][0].d.x += dx;
            po->triangles[i][1].d.x += dx;
            po->triangles[i][2].d.x += dx;

            po->triangles[i][0].d.y += dy;
            po->triangles[i][1].d.y += dy;
            po->triangles[i][2].d.y += dy;
        }
    }

    po->pos.d.x = x;
    po->pos.d.y = y;

    if (po->active)
    {
        toScreen (po, 1);
    }

    semGive (mutex);
}

void polyShow (scrPoly *po)
{
    if (!po)
        return;

    semTake (mutex, -1);

    po->active = 1;

    toScreen (po, 1);

    semGive (mutex);
}

void polyHide (scrPoly *po)
{
    if (!po)
        return;

    if (!po->active)
        return;

    semTake (mutex, -1);

    po->active = 0;

    toScreen (po, 0);
    semGive (mutex);
}

void walkScreenObjects (FUNCPTR cb, void *arg)
{
    scrPoly *po;

    semTake (mutex, -1);
    for (po = screenPolys; po != NULL; po = po->next)
    {
        if (po->active)
            cb (po, arg);
    }
    semGive (mutex);
}

void walkWorldObjects (FUNCPTR cb, void *arg)
{
    scrPoly *po;

    semTake (mutex, -1);
    for (po = worldPolys; po != NULL; po = po->next)
    {
        if (po->active)
            cb (po, arg);
    }
    semGive (mutex);
}

int worldObjectsChanged()
{
    int rc = worldPolysChanged;

    worldPolysChanged = 0;

    return rc;

}

/* redraw screen objects. Not required for OpenGL, it's done
 * in the OpenGl code
 */
void redrawScreenObjects ()
{
#ifndef USE_OPENGL
    scrPoly *po;

    semTake (mutex, -1);
    for (po = screenPolys; po != NULL; po = po->next)
    {
        if (po->active)
        {
            po->visible = 0;
            toScreen (po, 1);
        }
    }

    for (po = worldPolys; po != NULL; po = po->next)
    {
        if (po->active)
        {
            po->visible = 0;
            toScreen (po, 1);
        }
    }
    semGive (mutex);
#endif
}

void clearScreenObjects (FUNCPTR cb, void *arg)
{
    scrPoly *po;


    semTake (mutex, -1);
    for (po = screenPolys; po != NULL; po = po->next)
    {
        if (po->active)
        {
            toScreen (po, 0);
        }
    }
    semGive (mutex);
}


