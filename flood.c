/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"

int floodDir;
int floodVecs[8] = { 1,0, 0,1, -1,0, 0, -1};
int floodVecs8[16] = { 1,0, 0,1, -1,0, 0,-1, -1,-1, -1,1, 1,-1, 1,1};
unsigned lastFloodTs = 0;
unsigned int flooded;
unsigned int floodMax;
SDL_Rect floodRect;
char floodMsg[32];
int floodAbort;
int flooding = 0;
int floodStackPos;
svec_t *floodStack = NULL;
static vec_t floodStart;

static int hexOffX[6];
static int hexOffY[6];

static FT_TYPE *floodFlags;

#define FPUSH(X,Y)        {                        \
            floodStack[floodStackPos].x = X;        \
            floodStack[floodStackPos].y = Y;        \
            floodStackPos++;                        \
        }

#define FPOP(X,Y) {                                \
        floodStackPos--;                        \
        X = floodStack[floodStackPos].x;        \
        Y = floodStack[floodStackPos].y;        \
        }
        
int floodOngoing()
{
    return flooding;
}

void floodInit()
{
    int i;

    for (i = 0; i < 6; i++)
    {
        hexOffX[i] = -cos (DEG2RAD(i*60)) * (FLOAT)(CURRENT_RADIUS * 2);
        hexOffY[i] = sin (DEG2RAD(i*60)) * (FLOAT)(CURRENT_RADIUS * 2);
    }
    floodFlags = scrFlagFieldAlloc();
}

void doFloodI (int x, int y, FUNCPTR action, int p1, int mode)
{
    int i, j;
    int rc;
    int minDir, dist,marked, dir;
    int dx, dy;

    FPUSH (x,y)

    while ((flooded < floodMax) && 
           (floodStackPos < floodMax) &&
           (floodStackPos > 0))
        {

            FPOP (x,y)

            if (((unsigned int)x >= INTERNC(wid)) ||
                ((unsigned int)y >= (INTERNC(hei)) ||
                !inSelection(SCREENC(x),SCREENC(y))))
                {
                continue;
                }
    
                rc = action (x, y, p1);

            if (rc == 0)
                continue;
            else if (rc == -1)
                break;

                flooded++;
                floodRect.x = MIN(floodRect.x, x);
                floodRect.y = MIN(floodRect.y, y);
                floodRect.w = MAX(floodRect.w, floodRect.x + floodRect.w);
                floodRect.h = MAX(floodRect.h, floodRect.y + floodRect.h);

            if ((planschTime() - lastFloodTs) > 100)
            {
                sprintf (floodMsg, "Flooding: %d\n", flooded);
                statText (4, floodMsg);
                requestScreenUpdate();
                if (escapePressed())
                    return;
                lastFloodTs = planschTime();
            }

            marked = 0;

            /* Push four adjacent pixels to stack. Sort by distance relative to the start position.
             * The nearest one is pushed last, since it will be the 1st one poped from the stack
             * again. 
             */
            if (mode == FLOOD_MODE_INT_HEX)
            {
                for (j = 0; j < 6; j++)
                {
                    dist = 0;
                    minDir = -1;
                    for (i = 0; i < 8; i++)
                    {
                        dir = (floodDir + i) & 7;

                        if (marked & (1<<dir))
                            continue;

                        dx = x + floodVecs8[dir*2] - floodStart.x;
                        dy = y + floodVecs8[dir*2+1] - floodStart.y;

                        if ((dx*dx+dy*dy) >= dist)
                        {
                            dist = (dx*dx+dy*dy);
                            minDir = dir;
                        }
                    }

                    if (minDir > -1)
                    {
                        FPUSH (x + floodVecs8[minDir*2], y + floodVecs8[minDir*2+1]);
                        marked |= (1<<minDir);
                    }
                }
            }
            else if (mode == FLOOD_MODE_SCR_4)
            {
                for (j = 0; j < 4; j++)
                {
                    dist = 0;
                    minDir = -1;
                    for (i = 0; i < 4; i++)
                    {
                        dir = (floodDir + i) & 3;

                        if (marked & (1<<dir))
                            continue;

                        dx = x + floodVecs[dir*2] - floodStart.x;
                        dy = y + floodVecs[dir*2+1] - floodStart.y;

                        if ((dx*dx+dy*dy) >= dist)
                        {
                            dist = (dx*dx+dy*dy);
                            minDir = dir;
                        }
                    }

                    if (minDir > -1)
                    {
                        FPUSH (x + floodVecs[minDir*2], y + floodVecs[minDir*2+1]);
                        marked |= (1<<minDir);
                    }
                }
            }
            floodDir--;
        }
}

void doFlood (int x, int y, FUNCPTR action, int p1, int mode)
{
    int i, j;
    int rc;
    int minDir, dist,marked, dir;
    int dx, dy;

    SIFLAG_CLRALL (floodFlags);


    FPUSH (x,y)

    while ((flooded < floodMax) && 
           (floodStackPos < floodMax) &&
           (floodStackPos > 0))
        {

            FPOP (x,y)

            if (((unsigned int)x >= wid) ||
                ((unsigned int)y >= hei) ||
                SIFLAG_ISSET(floodFlags, SCR_OFF(x,y)) ||
                !inSelection(x,y))
                {
                continue;
                }
    
            SIFLAG_SET(floodFlags, SCR_OFF(x,y));

                rc = action (x, y, p1);

            if (rc == 0)
                continue;
            else if (rc == -1)
                break;

                flooded++;
                floodRect.x = MIN(floodRect.x, x);
                floodRect.y = MIN(floodRect.y, y);
                floodRect.w = MAX(floodRect.w, floodRect.x + floodRect.w);
                floodRect.h = MAX(floodRect.h, floodRect.y + floodRect.h);

            if ((planschTime() - lastFloodTs) > 100)
            {
                sprintf (floodMsg, "Flooding: %d\n", flooded);
                statText (4, floodMsg);
                requestScreenUpdate();
                if (escapePressed())
                    return;
                lastFloodTs = planschTime();
            }

            marked = 0;

            /* Push four adjacent pixels to stack. Sort by distance relative to the start position.
             * The nearest one is pushed last, since it will be the 1st one poped from the stack
             * again. 
             */
            if (mode == FLOOD_MODE_SCR_8)
            {
                for (j = 0; j < 8; j++)
                {
                    dist = 0;
                    minDir = -1;
                    for (i = 0; i < 8; i++)
                    {
                        dir = (floodDir + i) & 7;

                        if (marked & (1<<dir))
                            continue;

                        dx = x + floodVecs8[dir*2] - floodStart.x;
                        dy = y + floodVecs8[dir*2+1] - floodStart.y;

                        if ((dx*dx+dy*dy) >= dist)
                        {
                            dist = (dx*dx+dy*dy);
                            minDir = dir;
                        }
                    }

                    if (minDir > -1)
                    {
                        FPUSH (x + floodVecs8[minDir*2], y + floodVecs8[minDir*2+1]);
                        marked |= (1<<minDir);
                    }
                }
            }
            else if (mode == FLOOD_MODE_SCR_4)
            {
                for (j = 0; j < 4; j++)
                {
                    dist = 0;
                    minDir = -1;
                    for (i = 0; i < 4; i++)
                    {
                        dir = (floodDir + i) & 3;

                        if (marked & (1<<dir))
                            continue;

                        dx = x + floodVecs[dir*2] - floodStart.x;
                        dy = y + floodVecs[dir*2+1] - floodStart.y;

                        if ((dx*dx+dy*dy) >= dist)
                        {
                            dist = (dx*dx+dy*dy);
                            minDir = dir;
                        }
                    }

                    if (minDir > -1)
                    {
                        FPUSH (x + floodVecs[minDir*2], y + floodVecs[minDir*2+1]);
                        marked |= (1<<minDir);
                    }
                }
            }
            floodDir--;
        }
}

void flood (int x, int y, FUNCPTR action, int p1, int mode)
{
    floodInit();

    flooded = 0;
    floodRect.x = x;
    floodRect.y = y;
    floodRect.w = 0;
    floodRect.h = 0;

    floodStart.x = x;
    floodStart.y = y;

    floodAbort = 0;
    floodMax = wid*hei*4;
    if (floodStack == NULL)
        floodStack = malloc(floodMax * sizeof(*floodStack));
    floodStackPos = 0;

    flooding = 1;
    doFlood (x, y, action, p1, mode);
    flooding = 0;

    sprintf (floodMsg, "Flooded: %d %s\n", flooded, floodAbort ? "ABORT":"");
    statText (4, floodMsg);
}

SDL_Rect *getFloodRect ()
{
    return &floodRect;
}


