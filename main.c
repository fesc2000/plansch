/*

Copyright (C) 2008-2009 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#define SDL_MAIN_HANDLED

#include "plansch.h"
#include "ptypes.h"
#include <sys/stat.h>

#ifdef OPENMP
#include "omp.h"
#include <sched.h>

#endif

#if !defined(_MINGW) && defined(USE_OPENGL)
#include "X11/Xlib.h"
#endif


#ifndef VARIANT
#define VARIANT        "?default"
#endif

#undef main

int showParticles = 1;
int exitButton = 0;

globalData gd;

struct cfg_options *options = NULL;

int gSgn = 1;

int wallRemoves = 0;

int numParticles = 0;

int doMeasure = 0;

int paintPixel;


int usedClPlatform = 0;
int usedClDevice = 0;

uint64_t gpuTransToGpu = 0;
uint64_t gpuTransToGpuCnt = 0;
uint64_t gpuTransToGpuLat = 0;
uint64_t gpuTransToHost = 0;

int zoom;
int izoomx=0, izoomy=0;
int zoomx=0, zoomy=0;
int zoomOffX=0, zoomOffY=0;
int panning = 0;

int gravIdx = 0;


unsigned int minPps = 0xffffffff;
unsigned int maxPps = 0;
uint64_t totalPps;
int savedPpsCnt;


int planschInitialized = 0;

int densityUpdate;

int have_sse2 = 0;
int have_ssse3 = 0;
int have_sse41 = 0;
int have_avx = 0;

vec_t lastRedrawPos;
uint lastRedrawTicks = 0;

#define cpuid(func,ax,bx,cx,dx)\
    __asm__ __volatile__ ("cpuid":\
        "=a" (ax), "=b" (bx), "=c" (cx), "=d" (dx) : "a" (func));

char *dataDir;

int ldToggle = 0;


#ifdef USE_OPENCL
#include "opencl.h"
#endif



fileRep stencilRep =
{
    .dir = STENCIL_DIR,
    .file = STENCIL_FILE
};

fileRep tapeRep =
{
    .dir = TAPE_DIR,
    .file = STENCIL_FILE
};



SETTING(gpuGravWaveAbsorp) =
{
    .minValue = 0,
    .maxValue = 2000,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Factor for obsorption of gravity by mass"
};

SETTING(gpuGravDamping)  =
{
    .minValue = 0,
    .maxValue = 95,
    .defaultValue = 60,
    .minDiff = 1,
    .partAttr = 0,
    .name = "GPU grav. damping",
#ifdef USE_OPENCL
    .cb = (FUNCPTR)clParameterCb,
#endif
};
SETTING(gpuGravStrength)   =
{
    .minValue = 0,
    .maxValue = 10000,
    .defaultValue = 9500,
    .minDiff = 1,
    .partAttr = 0,
    .name = "GPU grav. strength",
#ifdef USE_OPENCL
    .cb = (FUNCPTR)clParameterCb,
#endif
};

SETTING(gpuGravWaveDamping)  = 
{
    .minValue = 0,
    .maxValue = 10000,
    .defaultValue = 9000,
    .minDiff = 1,
    .partAttr = 0,
    .name = "GPU grav. wave",
#ifdef USE_OPENCL
    .cb = (FUNCPTR)clParameterCb,
#endif
};


/* Whether a push operation adds an impuls to the particle
 */
int pushAddsImpuls = 1;

#ifdef USE_OPENCL
void clParameterCb (setting *s);
#endif

/* Wall temperature?
 */
SETTING  (wallTempr) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
#ifdef USE_OPENCL
    .cb = (FUNCPTR)clParameterCb,
#endif
    .name = "WallHeat",
};

/* Visualize particle links?
 */
void cbShowLinks (setting *s)
{
#ifdef USE_OPENCL
    clShowLinks (getValue(s));
#endif
}

SETTING  (showLinks) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "ShowLinks",
    .cb = (FUNCPTR)cbShowLinks
};


SETTING  (wallFriction) =
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .wallSetting = 1,
    .name = "WallFriction"
};


vec_t gravVec = {.d.x = 0,.d.y = 0 };

void cbGrav (setting *s);
SETTING (gvX) =
{
    .minValue = -30000,
    .maxValue = 30000,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Gravity(X)",
    .cb = (FUNCPTR)cbGrav
};

SETTING (gvY) =
{
    .minValue = -30000,
    .maxValue = 30000,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Gravity(Y)",
    .cb = (FUNCPTR)cbGrav
};



/* fraction of impuls preserved after hitting a wall
 */
void wallDampCb (setting *s);
SETTING  (wallDampingI) =
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "WallDamping",
    .cb = (FUNCPTR)wallDampCb
};

SETTING (preassure) =
{
    .minValue = 0,
    .maxValue = 0xffff,
    .defaultValue = 50,
    .minDiff = 1,
    .name = "ParticleRepulsion",
};

void trailsCb (setting *s);
SETTING (particleTrails) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .name = "Particle Trails",
    .cb = (FUNCPTR)trailsCb
};

void hideCb (setting *s);
SETTING (hideParticles) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .name = "Hide Particles",
    .cb = (FUNCPTR)hideCb
};

SETTING (particleVisSize) =
{
    .minValue = 0,
    .maxValue = 100,
    .minDiff = 1,
    .defaultValue = 100,
    .name = "Visual particle size"
};

SETTING (particleSmooth) =
{
    .minValue = 0,
    .maxValue = 1,
    .minDiff = 1,
    .defaultValue = 0,
    .name = "Smooth Particles"
};


float particleFadeF;
void cbFade (setting *s)
{
    particleFadeF = (float)GETVAL32(*s) / 100.0;
}

SETTING(particleFade) =
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 50,
    .minDiff = 1,
    .name = "Fading Multiplicator",
    .cb = (FUNCPTR)cbFade,
};

float particleBlurF;
void cbBlur (setting *s)
{
    particleBlurF = (float)GETVAL32(*s) / 100.0;
}

SETTING(particleBlur) =
{
    .minValue = 1,
    .maxValue = 400,
    .defaultValue = 350,
    .minDiff = 1,
    .name = "Blur",
    .cb = (FUNCPTR)cbBlur,
};

static void cbDecel (setting *s)
{
    char tmpStr[30];
    deceleration = 1 << GETVAL32(*s);

    sprintf (tmpStr, "Decel=%.4fppm", (0.01 / deceleration) * 1000000.0);
    statText (4, tmpStr);

#ifdef USE_OPENCL
    clDecelSet (deceleration);
#endif
}

SETTING(decelLog) =
{
    .minValue = 0,
    .maxValue = 29,
    .defaultValue = 29,
    .minDiff = 1,
    .name = "Particle deceleration",
    .cb = (FUNCPTR)cbDecel,
};


FLOAT wallDamping = 1.0;

#if 0
SDL_Color colors[256];
#endif

vec_t selectVector;

int deceleration = 0x20000000;

SETTING(updateInterval) =
{
    .minValue = 1,
    .maxValue = 100,
#ifdef USE_OPENGL
    .defaultValue = 30,
#else
    .defaultValue = 50,
#endif
    .minDiff = 1,
    .name = "Update Interval"
};

SETTING(drawWithScreen) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .name = "Update Particles with screen"
};

static void maxFpsCb (setting *s);

SETTING(maxFps) =
{
    .minValue = 1,
    .maxValue = 1000,
    .defaultValue = 1000,
    .minDiff = 1,
    .name = "Max IPS",
    .cb = (FUNCPTR)maxFpsCb
};


int showGravity = 0;

static screeninfo brick;

uint64_t lostImpuls;

screeninfo *siArray;

GRAV_TYPE *grvArray[2];
DENSITY_TYPE *densArray;
FDTYPE *fixDensArray;
DENSITY_TYPE *diffArray;

#ifdef TTF_SUPPORT
TTF_Font *sdlFont = NULL;
#endif

SDL_Cursor *cursor = NULL;
SDL_Cursor *dflCursor = NULL;
SDL_Cursor *emptyCursor = NULL;

uint wallUpdateCount = 0;
uint decCount = 0;

int curCol = 1;
int currentWeight = 1;

uint32 partCount;
int runSecs;
int bigBangEnd = 0;

int stopUpdate = 0;

int temprIdx = 0;


/* Direction in which to process the threads' particle list
 * 0 = 0..n-1, 1 = n-1 .. 0
 */
int workOrder = 0;

/* whether to reverse the work order with each iteration
 */
int workOrderToggle = 1;

void workOrderCb (setting *s)
{
    switch (getValue(s))
    {
        case 0:
            workOrder = 0;
            workOrderToggle = 0;
            break;
        case 1:
            workOrder = 1;
            workOrderToggle = 0;
            break;
        case 2:
            workOrderToggle = 1;
            break;
    }
}
SETTING(workOrderSetting) =
{
    .minValue = 0,
    .maxValue = 2,
    .defaultValue = 2,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Work order",
    .cb = (FUNCPTR)workOrderCb
};

SETTING(gpuSync) =
{
    .minValue = 0,
    .maxValue = 1,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 0,
    .name = "CPU/GPU Synchronization",
};

SETTING(gravIterations) =
{
    .minValue = 1,
    .maxValue = 100,
    .defaultValue = 10,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Field Iter/loop",
};

void graphAlphaCb (setting *s)
{
#ifdef USE_OPENGL
    showGravity = (GETVAL32(*s) != 0);
#endif
}

SETTING(gravAlpha) =
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Alpha value field display",
    .cb = (FUNCPTR)graphAlphaCb
};


void partAlphaCb (setting *s)
{
    modifySetting (&hideParticles, (GETVAL32(*s) == 0), 0);
}

SETTING(partAlpha) =
{
    .minValue = 0,
    .maxValue = 100,
    .defaultValue = 100,
    .minDiff = 1,
    .partAttr = 0,
    .name = "Alpha value of particle display",
    .cb = (FUNCPTR)partAlphaCb
};


/* multi-thread stuff
 */
#define MAX_THREADS 16

/* total number of particle threads */
unsigned int    numParallel = 1;
unsigned int         numCores = 1;

threadData        *tData[MAX_THREADS];
int                 thread_aff_change = 1;
int                 mp_disable = 0;
int                 threadDone[MAX_THREADS];
int                 tdone;

/* x/y averages of particles. Needed to assign work areas for
 * threads.
 */

/* synchronization */

/* stats */

uint64_t totImp = 0;
uint64_t totWallTempr = 0;
uint64_t wallPixels = 0;
int oob = 0;

static uint32 locked;

static uint32 move = 0;
static uint32 nomove = 0;
static uint32 unshareFree = 0;
static uint32 unshareStuck = 0;
static uint32 unshareSqueeze = 0;
static uint32 collissions = 0;
static uint32 wallHit = 0;
static int mean_n, mean_i;


vec_t motionVec = {{0, 0}};

void cleanup (void);
void statMovement (void);
void statThread (void);
void statProbe (void);
void threadBorders (int);

int curStatFunc = 0;
FUNCPTR        statFunctions[] = 
{
    (FUNCPTR)statProbe,
    (FUNCPTR)statMovement,
    (FUNCPTR)statThread,
    NULL
};

FUNCPTR statFunc = NULL;

PDECLS

void setPtypeCb(void);
SETTING(ptype)  =
{
    .partAttr = 1,
    .name = "Particle Type",
    .cb = (FUNCPTR)setPtypeCb

};

pdesc        *pdescs[MAX_PDESCS] = { PTYPES };

char  **directionPm[9] =
{
    upleft_pm,
    left_pm,
    downleft_pm,
    up_pm,
    nodir_pm,
    down_pm,
    upright_pm,
    right_pm,
    downright_pm
};


static void maxFpsCb (setting *s)
{
    int t = (GETVAL32(*s) < s->maxValue);

    setThrottle (t);

    if (!t)
        startParticleMovement (1);

}

#ifdef _MINGW

void *memalign (size_t align, size_t num)
{
    intptr_t mem = (intptr_t)malloc(align + num);

    mem = (mem + (align-1)) & ~(align-1);

    return (void*)mem;
}

#endif



#ifdef INCLUDE_CCSH
intptr_t ccsh_adrs_remap (intptr_t adrs)
{
            return adrs;
}

intptr_t ccsh_virt_to_io ( intptr_t virt)
{
        return virt;
}


char *ccsh_prompt(void)
{
    return "Plansch> ";
}

void ccsh_do_io_map (void)
{
}

void ccsh_start_net(void)
{
    ccsh_net(10000, ns_mode_tty, 1);
}

#endif

void get_cpu_caps()
{
    int ax, bx, cx, dx;

    cpuid (1, ax, bx, cx, dx);

    printf ("CPUID1: %08x %08x %08x %08x\n", ax, bx, cx, dx);

    if (dx & (1<<26))
    {
        have_sse2 = 1;
    }

    if (cx & (1<<9))
    {
        have_ssse3 = 1;
    }

    if (cx & (1<<19))
    {
        have_sse41 = 1;
    }

    if (cx & (1<<28))
    {
        have_avx = 1;
    }
}

void wallDampCb (setting *s)
{
    wallDamping = (float)getValue(s) / 100.0;
}

static inline int vec2dir (int dx, int dy)
{
    int ax, ay, xo, yo;

    ax = abs(dx);
    ay = abs(dy);

    if (likely(ax))
    {
        xo = (ay >= ax * 2) ? 1 : 
             -((dx >> 30) & 2) + 2;
    }
    else
    {
        xo = 1;
    }

    if (likely(ay))
    {
        yo = (ax >= ay * 2) ? 1 : 
             -((dy >> 30) & 2) + 2;
    }
    else
    {
        yo = 1;
    }

    return xo * 3 + yo;
}

int dirPixel (int x, int y, int dx, int dy)
{
    int dir = vec2dir (dx, dy);
    char    **pm;

    pm = directionPm[dir];

    x = x % 8;
    y = y % 8;

    if (pm[3+y][x] == ' ')
        return 1;

    return 0;
}


SDL_Cursor *
makeEmptyCursor (void)
{
    Uint8 data[4 * 32];
    Uint8 mask[4 * 32];

    memset (data, 0, sizeof(data));
    memset (mask, 0, sizeof(mask));

    return SDL_CreateCursor (data, mask, 32, 32, 16, 16);
}

SDL_Cursor *
makeCursor (const char *image[])
{
    int i, row, col;
    Uint8 data[4 * 32];
    Uint8 mask[4 * 32];

    i = -1;
    for (row = 0; row < 32; ++row)
    {
        for (col = 0; col < 32; ++col)
        {
            if (col % 8)
            {
                data[i] <<= 1;
                mask[i] <<= 1;
            }
            else
            {
                ++i;
                data[i] = mask[i] = 0;
            }
            if (image[3 + row][col] != ' ')
            {
                data[i] |= 0x00;
                mask[i] |= 0x01;
            }
        }
    }
    return SDL_CreateCursor (data, mask, 32, 32, 16, 16);
}

void statClear(void)
{
#ifdef STATS
    SDL_Rect rect = {statAreaX1, statAreaY1, 
                    statAreaX2-statAreaX1, 
                    statAreaY2-statAreaY1};

    SDL_FillRect(screen, &rect, COL_EMPTY);    
#endif
    }

void statBar (int px1, int px2, int py, int col, SDL_Surface *tag)
{
#ifdef STATS
    SDL_Rect rect;
    int tagh = 0;
    int        wid, hei;

    if (tag)
        tagh = tag->clip_rect.h;
    
    wid = statAreaX2 - statAreaX1;
    hei = statAreaY2 - statAreaY1 - tagh;
    
    rect.x = statAreaX1 + (px1 * wid) / 100 + 1; 
    rect.w = (statAreaX1 + (px2 * wid) / 100) - rect.x - 1;
    rect.h = (py * hei) / 100;
    rect.y = statAreaY2 - rect.h - tagh;

    SDL_FillRect(screen, &rect, col);
    
    if (tag)
    {
        rect.y = rect.y + rect.h;
        
        if (SDL_BlitSurface (tag, &tag->clip_rect, screen, &rect) == -1)
        {
            printf ("statBar: SDL_BlitSurface failed (src=%d/%d/%d/%d dst=%d/%d/%d/%d):\n%s\n",
                    tag->clip_rect.x, tag->clip_rect.y, tag->clip_rect.w, tag->clip_rect.h, 
                    rect.x, rect.y, rect.w, rect.h,
                    SDL_GetError());
            SDL_UnlockSurface (screen);
        }
    }
#endif   
}

void statThread (void)
{
    int        i;
    int        max = 1;
    int        wx;
    int col;
    int        current[MAX_THREADS];
    int px;
    
    statClear();

    for (i = 0; i < options->numThreads; i++)
    {
        current[i] = tData[i]->lastPartCount; 
        if (current[i] > max)
            max = current[i];
    }

    px = 0;
    wx = 100/options->numThreads;
    col = COL_RED;
    for (i = 0; i < options->numThreads; i++)
    {
        statBar (px, px+wx, (100 * current[i]) / max, col, NULL);
        px += wx;
    }
}

void statMovement (void)
{
    uint32 _move = move;
    uint32 _nomove = nomove;
    uint32 _unshareFree = unshareFree;
    uint32 _unshareStuck = unshareStuck;
    uint32 _unshareSqueeze = unshareSqueeze;
    uint32 _collissions = collissions;
    uint32 _wallHit = wallHit;
    uint32 max = 1;
    int px;
    int        wx;
    static SDL_Color textCol = {255,255,255,0};
    static int initialized = 0;
    static SDL_Surface *moveTag = NULL;
    static SDL_Surface *nomoveTag = NULL;
    static SDL_Surface *unshareFreeTag = NULL;
    static SDL_Surface *unshareStuckTag = NULL;
    static SDL_Surface *unshareSqueezeTag = NULL;
    static SDL_Surface *collissionsTag = NULL;
    static SDL_Surface *wallHitTag = NULL;
    
    if (!initialized)
    {
        moveTag = FNT_Render ("M", textCol);
        nomoveTag = FNT_Render ("N", textCol);
        unshareFreeTag = FNT_Render ("f", textCol);
        unshareStuckTag = FNT_Render ("s", textCol);
        unshareSqueezeTag = FNT_Render ("p", textCol);
        collissionsTag = FNT_Render ("C", textCol);
        wallHitTag = FNT_Render ("W", textCol);
        
        initialized = 1;
    }
    
    move = nomove = unshareFree = unshareStuck = 
        unshareSqueeze = collissions = wallHit = 0;
    
    max = MAX(_move, max);
    max = MAX(_nomove, max);
    max = MAX(_unshareFree, max);
    max = MAX(_unshareStuck, max);
    max = MAX(_unshareSqueeze, max);
    max = MAX(_collissions, max);
    max = MAX(_wallHit, max);
    max++;
    
    statClear();
    
    wx = 100 / 7;
    px = 0;

    statBar (px, px+wx, (100 * _move) / max, COL_STAT, moveTag); 
    px += wx;
    statBar (px, px+wx, (100 * _nomove) / max, COL_STAT, nomoveTag); 
    px += wx;
    statBar (px, px+wx, (100 * _unshareFree) / max, COL_STAT, unshareFreeTag); 
    px += wx;
    statBar (px, px+wx, (100 * _unshareStuck) / max, COL_STAT, unshareStuckTag); 
    px += wx;
    statBar (px, px+wx, (100 * _unshareSqueeze) / max, COL_STAT, unshareSqueezeTag); 
    px += wx;
    statBar (px, px+wx, (100 * _collissions) / max, COL_STAT, collissionsTag); 
    px += wx;
    statBar (px, px+wx, (100 * _wallHit) / max, COL_STAT, wallHitTag); 

}

void statProbe()
{
//    probeHistory (statAreaX1, statAreaY1, statAreaX2 - statAreaX1, statAreaY2 - statAreaY1);
}



void statText (int line, char *text)
{

    SDL_Surface        *textSurface;
    SDL_Color textCol = {255,255,255,0};
    SDL_Rect src = {0, 0, 0, 0 };
    SDL_Rect dst;
    SDL_Rect *box = statBox();

    if (box == NULL)
        return;
            
#ifdef TTF_SUPPORT
    if (sdlFont == NULL)
    {
        printf ("%s\n", text);
        return;
    }

    {
        SDL_Color bgCol = { 0,0,0 };
        textSurface = TTF_RenderText_Shaded(sdlFont, text, textCol, bgCol);
    }
#else
    textSurface = FNT_Render(text, textCol);
#endif

    if (textSurface == NULL)
    {
        printf ("TTF_RenderText_Solid failed \n");
        return;
    }

    dst = *box;
    dst.y += (line-1) * textSurface->clip_rect.h;
    dst.h = textSurface->clip_rect.h;

    if (dst.y >= box->y + box->h)
        return;

    if (dst.y + dst.h > box->y + box->h)
        dst.h -= (dst.y + dst.h) - (box->y + box->h);


    src.w = textSurface->clip_rect.w;
    src.h = textSurface->clip_rect.h;
    
    SDL_FillRect(screen, &dst, 0);    
    if (SDL_BlitSurface(textSurface, &src, 
                        screen, &dst) == -1)
    {
            printf ("statText: SDL_BlitSurface failed (src=%d/%d/%d/%d dst=%d/%d/%d/%d):\n%s\n",
                    src.x, src.y, src.w, src.h, 
                    dst.x, dst.y, dst.w, dst.h,
                    SDL_GetError());
            SDL_UnlockSurface (screen);

    }
    SDL_FreeSurface (textSurface);

    SCR_CHANGED
}

void (*gravFct)(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last);
void (*gravFctScr)(DENSITY_TYPE *dens, GRAV_TYPE *src, GRAV_TYPE *dest, DENSITY_TYPE *diff, int pixel, int num, int last);

void cpuGravInit(void)
{
    if (have_ssse3)
    {
        gravFct = spreadSS1_s3;
        gravFctScr = spreadSS1Scr_s3;
    }
    else
    {
        gravFct = &spreadSS1;
        gravFctScr = &spreadSS1Scr;
    }
}


void gravCalc (int scr)
{
    int t;
    int        lines = hei / numCores;
    int linesLast = lines + (hei - lines * numCores);
    static int rtnIdx = 0;
    OMPVARS

#ifdef USE_OPENGL
    scr = 0;
    showGravity = 0;
#endif

    if (!scr || !showGravity || (zoom != 1))
    {
#pragma omp parallel for schedule(static,1) 
        for (t = 0; t < numParallel; t++)
        {
            OMP_THREAD_NAME("gravCalc", t)
            if (t < numCores-1)
            {
                gravFct (densArray + lines*wid*t, 
                           grvArray[gravIdx] + lines*wid*t, 
                       grvArray[1-gravIdx]+lines*wid*t, 
                       diffArray + lines*wid*t, 
                       t * lines*wid, 
                       lines*wid,
                       0);
            }
            else
            {
                gravFct (densArray + lines*wid*t, 
                           grvArray[gravIdx] + lines*wid*t, 
                           grvArray[1-gravIdx]+lines*wid*t, 
                           diffArray + lines*wid*t, 
                           t * lines*wid, 
                           linesLast*wid, 
                           1);
            }
        }
    }
    else
    {
#pragma omp parallel for schedule(static,1) 
        for (t = 0; t < numParallel; t++)
        {
            OMP_THREAD_NAME("gravCalc", t)
            if (t < numCores-1)
            {
                gravFctScr (densArray + lines*wid*t, grvArray[gravIdx] + lines*wid*t, grvArray[1-gravIdx]+lines*wid*t, diffArray + lines*wid*t, t * lines*wid, lines*wid,
                            0);
            }
            else
            {
                gravFctScr (densArray + lines*wid*t, grvArray[gravIdx] + lines*wid*t, grvArray[1-gravIdx]+lines*wid*t, diffArray + lines*wid*t, t * lines*wid, linesLast*wid,
                            1);

            }
        }
    }
    OMPDONE

    gravIdx = 1-gravIdx;
    rtnIdx = (rtnIdx + 1) % 8;

//    if (zoom >= 8)
//        INTERVAL_RUN(1000, redrawScreen())

    if (scr && showGravity && (zoom != 1))
        gravShow();

}

int removePFromThread (int t, particle *p)
{
    int pid = PARTICLE_ID(p);
    int pi;


//    for (t = 0; t < options->numThreads; t++)
    {
        for (pi = 0; pi < tData[t]->numParticles; pi++)
        {
            if (tData[t]->particleList[pi] == pid)
            {
                tData[t]->particleList[pi] = tData[t]->particleList[tData[t]->numParticles - 1];
                tData[t]->numParticles--;

                return 1;
            }
        }
    }
    return 0;
}

void addPToThread (int t, particle *p)
{
    tData[t]->particleList[tData[t]->numParticles] = PARTICLE_ID(p);
    tData[t]->numParticles++;
}

void handoverPToThread (int src, int dst, int srcId, particle *p)
{

#pragma omp critical
    {
        tData[dst]->particleList[tData[dst]->handoverStart + tData[dst]->numHandover] = PARTICLE_ID(p);
        tData[dst]->numHandover++;
        tData[src]->numParticles--;
        tData[src]->particleList[srcId] = tData[src]->particleList[tData[src]->numParticles];
    }

}


int threadGet (int type, int x, int y)
{
#ifdef MT_BORDERS
    int i;
#ifdef THREAD_DIST_HORIZ
    /* get thread ID handling a specific X corrdinate */
    
    for (i = 0; i < options->numThreads; i++)
    {
        if ((x >= tData[i]->workLeft) && (x <= tData[i]->workRight))
            return i;
    }
    printf ("ERROR: x coordinate %d not assigned to any thread !!\n",
            x);
            
    return -1;
#else
    
    for (i = 0; i < options->numThreads; i++)
    {
        if ((y >= tData[i]->workTop) && (y <= tData[i]->workBot))
            return i;
    }
    printf ("ERROR: y coordinate %d not assigned to any thread !!\n",
            y);
            
    return -1;
#endif
#endif

#ifdef MT_WIMPS
    if (options->numThreads > 1)
    {
        if (pdescs[type]->isWimp)
            return 1;
        
        return 0;
    }
#endif

    return 0;
}


screeninfo *moveParticleFrozen (particle *p, screeninfo *pSi, threadData *tData, int off, particle2 *p2)
{
    tData->v.partCount++;
    return pSi;
}

void startParticleMovement (int start)
{
    int i;

    for (i = 0; i < MAX_PDESCS; i++)
    {
        if (pdescs[i] == NULL)
            break;

        if (start)
        {
            pdescs[i]->moveParticleCurrent = pdescs[i]->moveParticle;
            pdescs[i]->iterationCurrent = pdescs[i]->iteration;
        }
        else
        {
            if (pdescs[i]->redraw)
                pdescs[i]->moveParticleCurrent = (MOVEFUNC)pdescs[i]->redraw;
            else
                pdescs[i]->moveParticleCurrent = (MOVEFUNC)moveParticleFrozen;

            pdescs[i]->iterationCurrent = pdescs[i]->iterationFrozen;
        }
    }

    setThrottle (!start);
}

static void resetTrails (particle *p)
{
    particle2 *p2 = &partArray2[PARTICLE_ID(p)];
    p2->lpos = p->pos;
    p2->ipos_flag = 0;
}

void hideCb (setting *s)
{
    showParticles = !GETVAL32(*s);
}
void trailsCb (setting *s)
{
    if (GETVAL32(*s))
    {
        walkAllParticles ((FUNCPTR)resetTrails, 0);
    }
}

void threadWork (int thread, int scrUpdate)
{
    particle                         *p = NULL;
    particle2                         *p2;
    particle                         *prev;
    int                                 sx, sy;
    screeninfo                        *pSi;
    threadData                        *td = tData[thread];
    screeninfo                        *oldSi;
#ifdef OPENMP
    omp_lock_t                        *lock = NULL;
#else
    void                        *lock = NULL;
#endif
    MOVEFUNC                        moveParticle;
#ifdef DR_PIXEL_OPTION
#ifdef ZOOM
    uint32                        oldx=0, oldy=0;
#endif
#endif
    int                                pi;
    int                                decel;
    int                                colRestore = 0;
    int                                off;
    pdesc                       *pd;
    int                         start, end, dir;
    int                                handover_adjust;

    if (GETVAL32(colRestoreSpeed) > 0)
    {
        if ((systemLoops() % 5) == 0)
        {
            colRestore = 1;
        }
    }
  
    decel= ((systemTime() & (deceleration-1)) == 0);

    /* reverse direction of particle processing with each iteration
     */
    if (workOrder == 0)
    {
        start = 0;
        end = td->numParticles;
        dir = 1;
        handover_adjust = 1;
    }
    else
    {
        end = -1;
        start = td->numParticles-1;
        dir = -1;
        handover_adjust = 0;
    }

    for (pi = start; pi != end; pi += dir)
    {
        prev = p;
        p = &partArray[td->particleList[pi]];
        p2 = &partArray2[td->particleList[pi]];

        if (colRestore)
            restoreParticleColor (p, p2);

        lock = NULL;

        pd = pdescs[PTYPE(p)];

        moveParticle = pd->moveParticleCurrent;

        sx = SCREENC(p->pos.d.x);
        sy = SCREENC(p->pos.d.y);
        off = sx + sy*wid;
        oldSi = &siArray[off];

#ifdef USE_OPENGL
        td->drawLinkLine = scrUpdate;
#endif

#ifdef DR_PIXEL_OPTION
#ifdef ZOOM
        if (options->drawMode & D_PIXEL)
        {
            oldx = SCREENC_ZX(p->pos.d.x);
            oldy = SCREENC_ZY(p->pos.d.y);
        }
#endif
#endif

#ifdef MT_BORDERS
        /* if the pixel is adjacent to the border to another thread's
         * work area, get the border lock so that there can be no race condition
         * with the other thread.
         */
#ifdef THREAD_DIST_HORIZ
        if (unlikely(sx <= td->workLeft+2))
#else
        if (unlikely(sy <= td->workTop+2))
#endif
        {
#ifdef OPENMP
            lock = td->lock[0];
            if (lock)
            {
//                setPixel (sx, sy, 0x00ffff00);
                omp_set_lock (lock);
            }
#else
            lock = (void*)1;
#endif
        }
#ifdef THREAD_DIST_HORIZ
        else if (unlikely(sx >= td->workRight - 2))
#else
        else if (unlikely(sy >= td->workBot - 2))
#endif
        {
#ifdef OPENMP
            lock = td->lock[1];
            if (lock)
            {
//                setPixel (sx, sy, 0x00ffff00);
                omp_set_lock (lock);
            }
#else
            lock = (void*)1;
#endif
        }
#endif

#ifdef DR_PIXEL_OPTION
        if (options->drawMode & D_PIXEL)
        {
            if ((pSi = moveParticle (p, oldSi, td, off, p2)) != NULL)
            {
#ifndef USE_OPENGL
                if (showParticles)
                {
#ifdef ZOOM
                if (unlikely(zoom > 1))
                {
                    if (!showGravity && (oldx < wid) && (oldy < hei))
                    {
                        SCR_SET (oldx, oldy, COL_EMPTY);

                        if (zoom > 2)
                        {
                            if (oldx > 0)
                                SCR_SET (oldx-1, oldy, COL_EMPTY);
                            if (oldx < wid)
                                SCR_SET (oldx+1, oldy, COL_EMPTY);
                            if (oldy > 0)
                                SCR_SET (oldx, oldy-1, COL_EMPTY);
                            if (oldy < hei)
                                SCR_SET (oldx, oldy+1, COL_EMPTY);
                        }
                    }
                    if (((uint32)(p->pos.d.x - zoomx*GRID) < (wid * GRID)/zoom) &&
                        ((uint32)(p->pos.d.y - zoomy*GRID) < (hei * GRID)/zoom))
                    {
                        int zx = SCREENC_ZX(p->pos.d.x);
                        int zy = SCREENC_ZY(p->pos.d.y);

                        COLOR(zx, zy, p, pSi);

                        if (zoom > 2)
                        {
                            if (zx > 0)
                                COLOR(zx-1, zy, p, pSi);
                            if (zx < wid)
                                COLOR(zx+1, zy, p, pSi);
                            if (zy > 0)
                                COLOR(zx, zy-1, p, pSi);
                            if (zy < hei)
                                COLOR(zx, zy+1, p, pSi);
                        }
                    }
                }
                else
#endif
                {
                    if (!showGravity)
                        SCR_SET (sx, sy, COL_EMPTY);
                    COLOR(SCREENC(p->pos.d.x), SCREENC(p->pos.d.y), p, pSi);
                }
                }
#endif
            }
        }
        else
#endif
        {
            if ((pSi = moveParticle (p, oldSi, td, off, p2)) != NULL)
            {
#ifndef USE_OPENGL
                if (paintPixel)
                {
#ifdef ZOOM
                    if (unlikely (zoom > 1))
                    {
                        if (((uint32)(p->pos.d.x - zoomx*GRID) < (wid * GRID)/zoom) &&
                            ((uint32)(p->pos.d.y - zoomy*GRID) < (hei * GRID)/zoom-1))
                        {
                            int zx = SCREENC_ZX(p->pos.d.x);
                            int zy = SCREENC_ZY(p->pos.d.y);

                            COLOR(zx, zy, p, pSi);

                            if (zoom > 2)
                            {
                                if (zx > 0)
                                    COLOR(zx-1, zy, p, pSi);
                                if (zx < wid-1)
                                    COLOR(zx+1, zy, p, pSi);
                                if (zy > 0)
                                    COLOR(zx, zy-1, p, pSi);
                                if (zy < hei-1)
                                    COLOR(zx, zy+1, p, pSi);
                            }
                        }
                    }
                    else
#endif
                    {
                        COLOR(SCREENC(p->pos.d.x), SCREENC(p->pos.d.y), p, pSi);
                    }
                }
#endif
            }
        }

        MM_EMPTY;

#ifdef USE_OPENGL
        if (GETVAL32(particleTrails))
        {
            if (p2->ipos_flag)
            {
                ADD_LINK_LINE_V(p2->ipos, p2->lpos, p->color.cv, p->color.cv);
                p2->lpos = p2->ipos;
                
                p2->ipos_flag = 0;
            }
            else if (td->drawLinkLine)
            {
                ADD_LINK_LINE_V(p->pos, p2->lpos, p->color.cv, p->color.cv);
                p2->lpos = p->pos;
            }
            
        }
#endif


        if (decel && !pd->noDecel)
        {
            ACCEL_TO(p, (p->vec.d.x * 99) / 100, (p->vec.d.y * 99) / 100);
        }

        if ((pSi == NULL) || (p->mass == 0))
        {
            p2->del_next = td->deleteList;
            td->deleteList = p2;

#ifdef MT_BORDERS
            if (lock)
            {
#ifdef OPENMP
                omp_unset_lock (lock);
                lock = NULL;
#else
                lock = 0;
#endif
            }
#endif
        }
#ifdef MT_BORDERS
        else if (lock)
        {
            td->v.nLock++;

#ifdef THREAD_DIST_HORIZ
            sx = SCREENC(p->pos.d.x);
            
            if (sx > td->workRight)
            { 
                /* hand particle over to thread on right */
                handoverPToThread (thread, thread+1, pi, p);
                pi -= handover_adjust;
                end -= handover_adjust;
            }
            else if (sx < td->workLeft)
            {
                /* hand particle over to thread on left */
                handoverPToThread (thread, thread-1, pi, p);
                pi -= handover_adjust;
                end -= handover_adjust;
            }
#else
            sy = SCREENC(p->pos.d.y);

            if (sy > td->workBot)
            { 
                /* hand particle over to thread on right */
                handoverPToThread (thread, thread+1, pi, p);
                pi -= handover_adjust;
                end -= handover_adjust;
            }
            else if (sy < td->workTop)
            {
                /* hand particle over to thread on left */
                handoverPToThread (thread, thread-1, pi, p);
                pi -= handover_adjust;
                end -= handover_adjust;
            }
#endif

#ifdef OPENMP
            omp_unset_lock (lock);
            lock = NULL;
#else
            lock = 0;
#endif

        }
#endif
        
        if (doMeasure)
        {
            td->v.impuls += p->vec.d.x * p->vec.d.x + p->vec.d.y * p->vec.d.y;
        }

        if (pSi != oldSi)
        {
            /* remove mass from old pixel */
            SI_CHARGE(oldSi) -= (DENSITY_TYPE)PCHARGE(p);

            if (pSi)
            {
                SI_CHARGE(pSi) += (DENSITY_TYPE)PCHARGE(p);

            }
        }

        if ((workOrder == 0) && (prev > p))
        {
            td->v.unordered++;
        }
    }

}


#ifdef OPENMP
#ifdef MT_BORDERS
static omp_lock_t borderLocks[MAX_THREADS+1];
#endif
#endif

static void threadsInit ()
{
    int        i;
    OMPVARS

#ifdef MT_BORDERS
    for (i = 0; i < options->numThreads+1; i++)
    {
        omp_init_lock (&borderLocks[i]);
    }
#endif

    memset ((void*)&tData, 0, sizeof(tData));

    /* Do the per-thread initialization also threded to give the kernel a chance to
     * associate the thread's memory to the CPU/Core it's running on (for NUMA architectures)
     */
#pragma omp parallel for
    for (i = 0; i < options->numThreads; i++)
    {
        OMP_THREAD_NAME("OMP", i);
#ifdef OPENMP
#ifdef BIND_THREADS
        cpu_set_t *cset;
        int setsize;

        cset = CPU_ALLOC(numCores);
        setsize = CPU_ALLOC_SIZE(numCores);
        CPU_ZERO_S(setsize, cset);
        CPU_SET_S ((i + options->cpuCoreBase) % numCores, setsize, cset);

//        pthread_setaffinity_np (pthread_self(), setsize, cset);

//        printf ("Thread %d bound to CPU %d\n", i, (i + options->cpuCoreBase) % numCores);
#endif
#endif

        tData[i] = (threadData*)memalign (PAGE_ALIGN, sizeof(*tData[i]));
        memset ((char*)tData[i], 0, sizeof(*tData[i]));

        tData[i]->linkLines = (vec_t*)malloc (options->maxParticles + options->maxParticles * MAX_LINKS * sizeof(vec_t) * 2);
        tData[i]->linkLines2 = (vec_t*)malloc (options->maxParticles + options->maxParticles * MAX_LINKS * sizeof(vec_t) * 2);
        tData[i]->linkColors = (v4sf*)memalign (16, options->maxParticles + options->maxParticles * MAX_LINKS * sizeof(v4sf) * 2);
        tData[i]->linkColors2 = (v4sf*)memalign (16, options->maxParticles + options->maxParticles * MAX_LINKS * sizeof(v4sf) * 2);
        tData[i]->numLinkLines = tData[i]->numLinkLines2 = 0;

#ifdef BIND_THREADS
        tData[i]->set = cset;
        tData[i]->setsize = setsize;
#endif

#ifdef OPENMP
#ifdef MT_BORDERS
        if (i == 0)
            tData[i]->lock[0] = NULL;
        else
            tData[i]->lock[0] = &borderLocks[i];

        if (i == options->numThreads - 1)
            tData[i]->lock[1] = NULL;
        else
            tData[i]->lock[1] = &borderLocks[i+1];

        if ((options->numThreads > 4) && (i == (options->numThreads/2-1)))
            tData[i]->fixedBorder = 1;
#endif
#endif

        tData[i]->particleList = memalign(PAGE_ALIGN, options->maxParticles * sizeof(*tData[i]->particleList)) ;
        memset ((char*)tData[i]->particleList, 0, options->maxParticles * sizeof(*tData[i]->particleList));
        tData[i]->numParticles = 0;
        MADVISE (tData[i]->particleList, options->maxParticles * sizeof(*tData[i]->particleList), MADV_SEQUENTIAL);

        tData[i]->rightMarker = NULL;
        
#ifdef USE_SSE2
        /* these four arrays are needed in genmove.c when calculating the nearest particle
         */
        {
            void *nb_search_arrays = memalign(PAGE_ALIGN, PART_COLL_MAX * 4 * 4);
            MADVISE (nb_search_arrays, PART_COLL_MAX * 4 * 4, MADV_SEQUENTIAL);
            memset ((char*)nb_search_arrays, 0, PART_COLL_MAX * 4 * 4);

            tData[i]->xa_arr        = (int*)(nb_search_arrays + 0 * PART_COLL_MAX * 4);
            tData[i]->ya_arr        = (int*)(nb_search_arrays + 1 * PART_COLL_MAX * 4);
            tData[i]->pa_arr        = (int*)(nb_search_arrays + 2 * PART_COLL_MAX * 4);
            tData[i]->rada_arr        = (int*)(nb_search_arrays + 3 * PART_COLL_MAX * 4);
        }
#endif
    }
    OMPDONE

 
#ifdef MT_BORDERS
#ifdef THREAD_DIST_HORIZ
    tData[0]->workLeft = 0;
    tData[0]->workRight = wid-1;
#else
    tData[0]->workTop = 0;
    tData[0]->workBot = hei-1;
#endif
    threadBorders (1);
#endif
}

#ifdef MT_BORDERS
#define MIN_THREAD_AREA        10

#ifdef THREAD_DIST_HORIZ
void threadBorders (int initial)
{
    int i;

    if (options->numThreads == 1)
    {
        return;
    }

    if (initial)
    {
        int perThread = wid/options->numThreads;

        tData[0]->workLeft = 0;
        tData[0]->workRight = perThread - 1;

        for (i = 1; i < options->numThreads; i++)
        {
            tData[i]->workLeft = tData[i-1]->workRight + 1;
            tData[i]->workRight = tData[i]->workLeft + perThread - 1;
        }
        tData[i-1]->workRight = wid-1;

        return;
    }

    for (i = 0; i < options->numThreads-1; i++)
    {
        tData[i]->cpuScore /= 8;
#ifndef USE_OPENGL
        SCR_SET(tData[i]->workRight, hei, COL_EMPTY);
#endif
    }
    tData[i]->cpuScore /= 8;

    if (!bordersLocked())
    {
        for (i = 0; i < options->numThreads; i++)
        {
            if (i > 0)
            {
#if 0
                if ((tData[i]->v.partCount > tData[i-1]->v.partCount + 10) && 
#else
                if ((tData[i]->cpuScore > tData[i-1]->cpuScore) && 
#endif
                    (tData[i]->workLeft < tData[i]->workRight - MIN_THREAD_AREA))
                {
                    tData[i]->workLeft++;
                    tData[i-1]->workRight++;
                }
            }

            if (i < options->numThreads-1)
            {
#if 0
                if ((tData[i]->v.partCount > tData[i+1]->v.partCount + 10) && 
#else
                if ((tData[i]->cpuScore > tData[i+1]->cpuScore) && 
#endif
                    (tData[i]->workRight > tData[i]->workLeft + MIN_THREAD_AREA))
                {
                    tData[i]->workRight--;
                    tData[i+1]->workLeft--;
                }
            }
        }
    }

    for (i = 0; i < options->numThreads-1; i++)
    {
#ifndef USE_OPENGL
        SCR_SET(tData[i]->workRight, hei, COL_ICONFG);
#endif
    }

}
#else
void threadBorders (int initial)
{
    int i;

    if (options->numThreads == 1)
    {
        return;
    }

    if (initial)
    {
        int perThread = hei/options->numThreads;

        tData[0]->workTop = 0;
        tData[0]->workBot = perThread - 1;

        for (i = 1; i < options->numThreads; i++)
        {
            tData[i]->workTop = tData[i-1]->workBot + 1;
            tData[i]->workBot = tData[i]->workTop + perThread - 1;
        }
        tData[i-1]->workBot = hei-1;

        return;
    }

#ifndef USE_OPENGL
    for (i = 0; i < options->numThreads-1; i++)
        SCR_SET(1, tData[i]->workBot, COL_EMPTY);
#endif

    if (!bordersLocked())
    {
        for (i = 0; i < options->numThreads; i++)
        {
            if (i > 0)
            {
                if ((tData[i]->v.partCount > tData[i-1]->v.partCount + 10) && 
                    (tData[i]->workTop < tData[i]->workBot - MIN_THREAD_AREA))
                {
                    if (!tData[i-1]->fixedBorder)
                    {
                        tData[i]->workTop++;
                        tData[i-1]->workBot++;
                    }
                }
            }

            if (i < options->numThreads-1)
            {
                if ((tData[i]->v.partCount > tData[i+1]->v.partCount + 10) && 
                    (tData[i]->workBot > tData[i]->workTop + MIN_THREAD_AREA))
                {
                    if (!tData[i]->fixedBorder)
                    {
                        tData[i]->workBot--;
                        tData[i+1]->workTop--;
                    }
                }
            }
        }
    }

#ifndef USE_OPENGL
    for (i = 0; i < options->numThreads-1; i++)
        SCR_SET(1, tData[i]->workBot, COL_ICONFG);
#endif
}
#endif
#endif
 
static void moveParticles (int scrUpdate)
{
    int i;
    uint32 count;
    int pi;
    OMPVARS
    int unordered = 0;

    if (numParticles == 0)
        return;

    if (!showParticles)
    {
        paintPixel = 0;
    }
    else
    {
        if (showGravity)
        {
            paintPixel = scrUpdate;
        }
        else
        {
            paintPixel = 1;
        }
    }

#ifdef MT_BORDERS
    /* recalculate work area borders */
    if ((systemLoops() & 0x7) == 0)
        threadBorders (0);
#endif

    for (i = 0; i < options->numThreads; i++)
    {
        tData[i]->handoverStart = tData[i]->numParticles;
        tData[i]->numHandover = 0;
    }

    for (i = 0; i < MAX_PDESCS; i++)
    {
        if (pdescs[i] == NULL)
            break;

        if (pdescs[i]->iterationCurrent)
        {
            pdescs[i]->iterationCurrent(scrUpdate);
        }
    }

    clipToDrawArea ();
    tdone = 0;
#pragma omp parallel for  schedule(static,1) 
    for (i = 0; i < options->numThreads; i++)
    {
        OMP_THREAD_NAME("moveParticles", i)
        OMP_THREAD_INIT_BIND(i)

        memset ((void*)&tData[i]->v, 0, sizeof(tData[i]->v));

        threadWork (i, scrUpdate);

#pragma omp critical
        {
            /* capture the order in which threads complete. This is required for
             * reassigning the work for each thread and takes both the number of
             * particles and the CPU core's work capacity into account.
             * Capacity might be affected by background load, memory latency
             * (in NUMA architectures), core speed ....
             */
            threadDone[tdone++] = i;
        }
    }

    clipDisable();

    OMPDONE

    if (workOrderToggle)
        workOrder = 1 - workOrder;

    count = 0;
    if (doMeasure)
        totImp = 0;

    /* process particle handover */
    locked=0;
    for (i = 0; i < options->numThreads; i++)
    {
        unordered += tData[i]->v.unordered;
        locked += tData[i]->v.locked;
        nomove += tData[i]->v.nomove;
        unshareFree += tData[i]->v.unshareFree;
        unshareSqueeze += tData[i]->v.unshareSqueeze;
        unshareStuck += tData[i]->v.unshareStuck;
        wallHit += tData[i]->v.wallHit;
        collissions += tData[i]->v.collissions;
        move += tData[i]->v.move;
        count += tData[i]->v.partCount;
        mean_n += tData[i]->v.gas_nb_total;
        mean_i += tData[i]->v.gas_nb_ccount;
        tData[i]->lastPartCount = tData[i]->v.partCount;

        if (doMeasure)
            totImp += tData[i]->v.impuls;

        for (pi = 0; pi < tData[i]->numHandover; pi++)
        {
            addPToThread (i, &partArray[tData[i]->particleList[tData[i]->handoverStart + pi]]);
        }

        /* the 1st thread to finish gets a score of 0, the second one 1, etc. */
        tData[threadDone[i]]->cpuScore += i;

    }
    partCount += count;

    deleteParticles();

#if 0
    /* If the order of the threads' particle lists cause too many non-sequential memory accesses
     * into the particle array, reorder them from time to time.
     * This greatly enhances overall performance
     */
    if (unordered > 1000)
    {
        INTERVAL_RUN(500, reorderParticles());
    }
#endif

//    if (doMeasure && count)
//        totImp /= count;

#if 0
    if (count != numParticles)
    {
            printf ("Bad particle count: got %d, exp %d (diff %d)\n", 
                    count, numParticles, numParticles - count); 
    }
#endif

    /* indicate to GPU that the density buffer from the CPU engine needs to be
     * merged with the GPU buffer
     */
    densityUpdate = 1;
}


uint32
usecs (void)
{
    struct timeval t;

    gettimeofday (&t, NULL);
    return t.tv_sec * 1000000 + t.tv_usec;
}

#ifdef USE_GPU
extern int gpuLoops;
#endif


void
status ()
{
    static char statusMsg[64];
    static uint32 lastTick = 0;
    uint32 curTick, td;
    static int lastLoop;
    int loops;
    unsigned int fps;
#if defined(USE_GPU)
    unsigned int sps = 0;
#endif
#if defined(USE_OPENCL)
    unsigned int wps = 0;
//    float tdf;
#endif
    
    if (lastTick == 0)
    {
        lastLoop = systemLoops();
        lastTick = planschTime();
        return;
    }

    curTick = planschTime();

    td = (curTick - lastTick);

    if ((curTick - lastTick) < 1000)
        return;

    loops = systemLoops() - lastLoop;
    lastLoop = systemLoops();

    if (loops == 0)
        return;

    fps = (loops * 1000) / td;

#ifdef USE_GPU
    if (options->useGpu)
    {
        sps = (gpuLoops * 1000) / td;
        gpuLoops = 0;
    }
#endif
#ifdef USE_OPENCL
    {
        int totalClParticles;
        if ((totalClParticles = clNumParticles(-1)) != 0)
        {
            wps = (clParticleLoops * 1000) / td;
            wps *= totalClParticles;
            clParticleLoops = 0;
        }
    }
#endif


#if 0
    {
    unsigned int tTherm = 0;
    unsigned int tKin = sqrt((FLOAT)totImp) / (FLOAT)partCount;

    if (wallPixels)
    {
        tTherm = (unsigned int)(totWallTempr / wallPixels);
    }

    if (tKin || tTherm)
    {
        printf ("T(kin) = %u  T(therm) = %u  T = %u", tKin, tTherm, tKin + tTherm);
        if (lostImpuls && partCount)
        {
            printf (" Lost Imp: %u", (int)(lostImpuls / (partCount)));
            lostImpuls = 0;
        }
        printf ("\n");

#if 0
        if (wallUpdateCount)
        printf ("loops=%d wall=%d p=%d kin=%u therm=%u tot=%u\n", 
                loops,
                wallUpdateCount,
                partCount,
                (int)((totImp/loops)),
                (int)((totWallTempr/wallUpdateCount)), 
                (int)((totImp/loops) + (totWallTempr/wallUpdateCount)));
#endif

    }
    }
#endif

    if (statFunc)
        statFunc();

    partCount = (uint32)((FLOAT)partCount * ((FLOAT)(curTick-lastTick) / 1000.0));

#ifdef USE_GPU
    if (options->useGpu)
    {
#ifdef USE_OPENCL
        if ((partCount) < 10000)
            sprintf (statusMsg, "%dpps, ", (int)(partCount));
        else if ((partCount) < 100000)
            sprintf (statusMsg, "%dkpps, ", (int)(partCount / 1000));
        else
            sprintf (statusMsg, "%.2fmpps, ", 
                     (float)(partCount) / 1000000.f);

        if (wps < 10000)
            sprintf (statusMsg + strlen(statusMsg), "%dwps, ", (int)(wps));
        else if (wps < 100000)
            sprintf (statusMsg + strlen(statusMsg), "%dkwps, ", (int)(wps / 1000));
        else
            sprintf (statusMsg + strlen(statusMsg), "%.2fmwps, ", 
                     (float)(wps) / 1000000.f);

        sprintf (statusMsg + strlen(statusMsg), "%dsps", sps);

#if 0
        float tdf = (float)td / 1000.f;
        printf ("ToGpu: %.2fMBPS %.2fus FromGpu: %.2fMBPS\n",
            ((float)(gpuTransToGpu) / 1000000.f) / tdf,
            gpuTransToGpuCnt ? ((float)(gpuTransToGpuLat / gpuTransToGpuCnt)) / tdf : 0.f,
            ((float)(gpuTransToHost) / 1000000.f) / tdf);
        gpuTransToGpu = gpuTransToHost = gpuTransToGpuLat = gpuTransToGpuCnt = 0;
#endif
            

#else
        if ((partCount) < 10000)
            sprintf (statusMsg, "%dpps, %dfps, %dsps", (int)(partCount), fps, sps);
        else if ((partCount) < 100000)
            sprintf (statusMsg, "%dkpps, %dfps, %dsps", (int)(partCount / 1000), fps, sps);
        else
            sprintf (statusMsg, "%.2fmpps, %dfps, %dsps", 
                     (float)(partCount) / 1000000.f, fps, sps);
#endif
    }
    else
#endif
    {
        if ((partCount) < 10000)
            sprintf (statusMsg, "%dpps, %dfps", (int)(partCount), fps);
        else if ((partCount) < 100000)
            sprintf (statusMsg, "%dkpps, %dfps", (int)(partCount / 1000), fps);
        else
            sprintf (statusMsg, "%.2fmpps, %dfps", 
                     (float)(partCount) / 1000000.f, fps);
    }

    maxPps = MAX(maxPps, partCount);
    if (numParticles)
    {
        minPps = MIN(minPps, partCount);
    }

    totalPps += partCount;
    savedPpsCnt++;

    statText (1, statusMsg);

    sprintf (statusMsg, "Zoom=%d n=%d", zoom, mean_i ? (mean_n / mean_i) : 0);
    mean_n = mean_i = 0;

    statText (2, statusMsg);

#ifdef USE_OPENCL
    sprintf (statusMsg, "Count: %d/%d", numParticles, clNumParticles(0));
#else
    sprintf (statusMsg, "Count: %d", numParticles);
#endif
    statText (3, statusMsg);

#if 0
    if (throttle)
    {
        sprintf (statusMsg, "fps=%d", GETVAL32(maxFps));
        statText (3, statusMsg);
    }
#endif
    
    if (playbackTape.recording)
    {
        sprintf (statusMsg, "Recording: %d", playbackTape.tapePos);
        statText (4, statusMsg);
    }

//    statText (8, clStatString());

    lastTick = planschTime();

    partCount = 0;
    wallPixels = totWallTempr = 0;
    wallUpdateCount = 0;

    if (probeButtonVal)
    {
        probeShow();
        probeClear();
        probeStart (1);
    }
}

int tPixels[MAX_THREADS] = {0,0,0,0};
int tRndErr[MAX_THREADS] = {0,0,0,0};
int tTot[MAX_THREADS];

#ifndef USE_OPENCL
static unsigned int updateWallTemprPart (int y, int lines, int thread)
{
    screeninfo        *pSi;
    screeninfo        *nSi;
    int                i;
    int                t;
    int                altIdx = 1 - temprIdx;
    int                x;
    int                k;
    unsigned int    temp = 0;
#undef COMPENSATE
#ifdef COMPENSATE
    unsigned int                compensatePerPixel = 0;
    unsigned int                compensateCount = 0;
    unsigned int                lastCompensated = 0;
    unsigned int                compensated = 0;
    int                cDiff;
#endif
#if defined(ZOOM) && !defined(USE_OPENGL)
    SDL_Rect        rect;
#ifdef OPENMP
    int                xx, yy, col;
#endif
#endif

    x = 0;
    pSi = &siArray[y * wid];

#if defined(ZOOM) && !defined(USE_OPENGL)
    rect.w = rect.h = zoom;
#endif


#ifdef COMPENSATE
    if (tPixels[thread])
    {
        compensatePerPixel = (tRndErr[thread] * 10000) / tPixels[thread];
    }
//printf ("%d: %d.%d (p=%d re=%d)\n", thread, compensatePerPixel/10000, compensatePerPixel%10000, tPixels[thread], tRndErr[thread]);
#endif

    if (y + lines > hei)
        lines -= (y+lines) - hei;

#define LARGE
    for (i = 0; i < wid*lines; i++)
    {
        if (pSi->flags & SI_BRICK)
        {

            k = 1;
            t = TEMPR(pSi);

#ifdef LARGE
            nSi = pSi - wid*2;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // nn
                k++;
            }
            nSi += wid-1;
#else
            nSi = pSi - wid - 1;
#endif
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // nw
                k++;
            }
            nSi++;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // n
                k++;
            }
            nSi++;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // ne
                k++;
            }
#ifdef LARGE
            nSi += wid-3;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // ww
                k++;
            }
            nSi ++;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // w
                k++;
            }
#else
            nSi += wid-2;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // w
                k++;
            }
#endif
            nSi += 2;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // e
                k++;
            }
#ifdef LARGE
            nSi ++;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // ee
                k++;
            }
            nSi += wid-3;
#else
            nSi += wid-2;
#endif
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // sw
                k++;
            }
            nSi++;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // s
                k++;
            }
            nSi++;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // se
                k++;
            }
#ifdef LARGE
            nSi += wid-1;
            if (nSi->flags & SI_BRICK)
            {
                t += TEMPR(nSi);                // ss
                k++;
            }
#endif

            pSi->u.tempr[altIdx] = t / k;

#ifdef COMPENSATE
            rndErr += t - pSi->tempr[altIdx] * k;
            if (t)
            {
                p++;
                lastCompensated = compensateCount/10000;
                compensateCount += compensatePerPixel;
                cDiff = (compensateCount / 10000) - lastCompensated;

                if (cDiff)
                {
                    if (pSi->tempr[altIdx] + cDiff < MAX_TEMP)
                    {
                        pSi->tempr[altIdx] += cDiff;

                        compensated += cDiff;

//                        rndErr -= cDiff;
                    }
                }

            }
#endif

            if ((pSi->flags & SI_STENCIL) == 0)
            {
#if defined(ZOOM) && !defined(USE_OPENGL)
                if (zoom > 1)
                {
                    unsigned int sx, sy;

                    sx = (x - zoomx) * zoom;
                    sy = (y - zoomy) * zoom;

                    if ((sx <= wid-zoom) && (sy <= hei-zoom))
                    {
                        rect.x = sx;
                        rect.y = sy;
#ifndef OPENMP
                        SDL_FillRect (screen, &rect, WALL_COL(pSi->u.tempr[altIdx], WALL_MASS_GET(pSi)));
#else
                        col = WALL_COL(pSi->u.tempr[altIdx], WALL_MASS_GET(pSi));
                        for (yy = rect.y; yy < rect.y + rect.h; yy++)
                            for (xx = rect.x; xx < rect.x + rect.w; xx++)
                                SCR_SET (xx, yy, col);
#endif
                    }
                }
                else
                {
                    SCR_SET (x, y, WALL_COL(pSi->u.tempr[altIdx], WALL_MASS_GET(pSi)));
                }
#else
                SCR_SET (x, y, WALL_COL(pSi->u.tempr[altIdx], WALL_MASS_GET(pSi)));
#endif
            }
        }

        pSi++;
        x++;

        if (x >= wid)
        {
            x = 0;
            y++;
        }

    }

#ifdef COMPENSATE
    tPixels[thread] = p;
    if (tTot[thread] > temp)
    {
        tRndErr[thread] = tTot[thread] - temp;
    }
    else
    {
        tRndErr[thread] = 0;

    }
    tTot[thread] = temp;

#endif
    return temp;
}
#endif

static void updateWallTempr (void)
{
#ifndef USE_OPENCL
    int                t;
    int                tsize = hei / options->numThreads + 1;
    static uint32   lastUpdate = 0;
    OMPVARS

    if (!getValue (&wallTempr))
        return;

    if (lastUpdate++ < 20)
        return;

    lastUpdate = 0;

#pragma omp parallel for schedule(static,1) 
    for (t = 0; t < numParallel; t++)
    {
        OMP_THREAD_NAME("updateWallTempr", t)
        updateWallTemprPart (t * tsize, tsize, t);
    }
    OMPDONE

    temprIdx = 1-temprIdx;
    wallUpdateCount++;
#endif
}


static int doQuit = 0;

int doExit()
{
    return doQuit;
}


void
cleanup (void)
{
    doQuit = 1;
    sleep (1);
#ifdef USE_OPENGL
    if (oglStop() == 0)
        printf ("OGL stop timeout\n");
#endif

    SDL_Quit ();

    printf ("Bye\n");
    
    exit (0);
}

static void initGlobals (void)
{
        gd.dirSi[0] = (-wid-1) * (int)sizeof(screeninfo);
        gd.dirSi[1] = (-1    ) * (int)sizeof(screeninfo);
        gd.dirSi[2] = ( wid-1) * (int)sizeof(screeninfo);
        gd.dirSi[3] = (-wid  ) * (int)sizeof(screeninfo);
        gd.dirSi[4] = ( wid  ) * (int)sizeof(screeninfo);
        gd.dirSi[5] = (-wid+1) * (int)sizeof(screeninfo);
        gd.dirSi[6] = (     1) * (int)sizeof(screeninfo);
        gd.dirSi[7] = ( wid+1) * (int)sizeof(screeninfo);

        gd.quadSi[0] = (-wid-1) * (int)sizeof(screeninfo);
        gd.quadSi[1] = (-1    ) * (int)sizeof(screeninfo);
        gd.quadSi[2] = ( wid-1) * (int)sizeof(screeninfo);
        gd.quadSi[3] = (-wid  ) * (int)sizeof(screeninfo);
        gd.quadSi[4] = 0;
        gd.quadSi[5] = ( wid  ) * (int)sizeof(screeninfo);
        gd.quadSi[6] = (-wid+1) * (int)sizeof(screeninfo);
        gd.quadSi[7] = (     1) * (int)sizeof(screeninfo);
        gd.quadSi[8] = ( wid+1) * (int)sizeof(screeninfo);

        gd.quadSiNeg = &gd.quadSi[4];

        gd.dirNextSi9[0] = (-wid-1) * sizeof(screeninfo);
        gd.dirNextSi9[1] = (     1) * sizeof(screeninfo);
        gd.dirNextSi9[2] = (     1) * sizeof(screeninfo);
        gd.dirNextSi9[3] = ( wid-2) * sizeof(screeninfo);
        gd.dirNextSi9[4] = (     1) * sizeof(screeninfo);
        gd.dirNextSi9[5] = (     1) * sizeof(screeninfo);
        gd.dirNextSi9[6] = ( wid-2) * sizeof(screeninfo);
        gd.dirNextSi9[7] = (     1) * sizeof(screeninfo);
        gd.dirNextSi9[8] = (     1) * sizeof(screeninfo);
        gd.dirNextSi9[9] = 0;

        gd.dirNextSi8[0] = (-wid-1) * sizeof(screeninfo);
        gd.dirNextSi8[1] = (     1) * sizeof(screeninfo);
        gd.dirNextSi8[2] = (     1) * sizeof(screeninfo);
        gd.dirNextSi8[3] = ( wid-2) * sizeof(screeninfo);
        gd.dirNextSi8[4] = (     2) * sizeof(screeninfo);
        gd.dirNextSi8[5] = ( wid-2) * sizeof(screeninfo);
        gd.dirNextSi8[6] = (     1) * sizeof(screeninfo);
        gd.dirNextSi8[7] = (     1) * sizeof(screeninfo);
        gd.dirNextSi8[8] = 0;

        gd.dirNextSi5[0] = (-wid  ) * sizeof(screeninfo);
        gd.dirNextSi5[1] = (wid-1 ) * sizeof(screeninfo);
        gd.dirNextSi5[2] = (     1) * sizeof(screeninfo);
        gd.dirNextSi5[3] = (     1) * sizeof(screeninfo);
        gd.dirNextSi5[4] = ( wid-2) * sizeof(screeninfo);
        gd.dirNextSi9[5] = 0;
}

void memInit()
{
    unsigned int partArraySize;
    unsigned int part2ArraySize;
    unsigned int gravArraySize;
    unsigned int siArraySize;
    int                 gravDim = GRAVWID*(GRAVHEI+4);

    partArraySize = options->maxParticles * sizeof (*partArray);
    part2ArraySize = options->maxParticles * sizeof (*partArray2);

#ifdef GGRAV_1x3
    gravArraySize = (gravDim * sizeof(GRAV_TYPE) * 3);
#elif defined(GGRAV_1x2)
    gravArraySize = (gravDim * sizeof(GRAV_TYPE) * 2);
#elif defined(GGRAV_2x1)
    gravArraySize = (gravDim * sizeof(DENSITY_TYPE));
#endif

    siArraySize = wid * (hei+4) * sizeof (screeninfo);

    grvArray[0] = (GRAV_TYPE*)memalign (0x1000, gravArraySize);
    MADVISE(grvArray[0], gravArraySize, MADV_RANDOM);

#ifdef GGRAV_2x1
    grvArray[0] += wid;

    grvArray[1] = (GRAV_TYPE*)memalign (0x1000, gravArraySize);
    MADVISE(grvArray[1], gravArraySize, MADV_RANDOM);
    grvArray[1] += wid;
#endif

    densArray = (DENSITY_TYPE*)memalign (0x1000, gravArraySize);
    MADVISE(densArray, gravArraySize, MADV_RANDOM);

    diffArray = (DENSITY_TYPE*)memalign (0x1000, gravArraySize);
    MADVISE(diffArray, gravArraySize, MADV_RANDOM);

#ifdef USE_OPENCL
    fixDensArray = NULL;
#else
    fixDensArray = memalign (0x1000, gravDim);
    MADVISE(fixDensArray, gravDim, MADV_RANDOM);
#endif

    partArray = (particle *)memalign (0x1000, partArraySize);
    MADVISE(partArray, partArraySize, MADV_SEQUENTIAL);

    partArray2 = (particle2 *)memalign (0x1000, part2ArraySize);
    MADVISE(partArray2, part2ArraySize, MADV_SEQUENTIAL);

    siArray = (screeninfo *)memalign(0x1000, siArraySize);
    MADVISE(siArray, siArraySize, MADV_RANDOM);
    memset ((char*)siArray, 0, siArraySize);
    siArray += wid*2;
}


void doPan (vec_t *pStartPos, int end)
{
    static vec_t startPos;
    static vec_t panZoomStart;
    vec_t panDiff;
    int nx, ny;

    if (zoom == 1)
        return;

    if (pStartPos)
    {
        startPos = *pStartPos;

        panZoomStart.d.x = izoomx;
        panZoomStart.d.y = izoomy;

        lastRedrawPos.d.x = zoomx;
        lastRedrawPos.d.y = zoomy;

        panning = 1;
        return;
    }

    if (!panning)
        return;

    panDiff.d.x = (startPos.d.x - mousePos.d.x) * (GRID/zoom);
    panDiff.d.y = (startPos.d.y - mousePos.d.y) * (GRID/zoom);

    nx = panZoomStart.d.x + panDiff.d.x;
    ny = panZoomStart.d.y + panDiff.d.y;

    setPan (nx, ny, end);
}

void setPan(int nx, int ny, int end)
{

    if (nx > INTERNC(wid - wid/zoom))
        nx = INTERNC(wid - wid/zoom);
    else if (nx < 0)
        nx = 0;

    if (ny > INTERNC(hei - hei/zoom))
        ny = INTERNC(hei - hei/zoom);
    else if (ny < 0)
        ny = 0;

#ifndef USE_OPENGL
    izoomx &= ~MAX_SPEED;
    izoomy &= ~MAX_SPEED;
#endif


    izoomx = nx;
    izoomy = ny;
    zoomx = SCREENC(nx);
    zoomy = SCREENC(ny);

    if ((zoomx != lastRedrawPos.d.x) || (zoomx != lastRedrawPos.d.y))
    {
        if (end || ((planschTime() - lastRedrawTicks) > 50))
        {
            redrawScreen();

            lastRedrawPos.d.x = zoomx;
            lastRedrawPos.d.y = zoomy;
            lastRedrawTicks = planschTime();

            clearBorderOn();
        }
    }

    if (end)
        panning = 0;
}


static void dflHit (particle *p, int hx, int hy)
{
    p->vec.d.x += hx;
    p->vec.d.y += hy;
}

void initParticleTypes (void)
{
    int i;

    for (i = 0; i < MAX_PDESCS; i++)
    {
        if (pdescs[i] == NULL)
        {
            continue;
        }

        if (pdescs[i]->init)
            pdescs[i]->init (i);

        if (pdescs[i]->hit == NULL)
            pdescs[i]->hit = (FUNCPTR)dflHit;

        
        if (pdescs[i]->numLinks == NULL)
            pdescs[i]->numLinks = (FUNCPTR)dflNumLinks;

    }
}

void swapLinkLines()
{
    int thread;
    void *tmp;

    for (thread = 0; thread < options->numThreads; thread++)
    {

        tmp = tData[thread]->linkLines2;
        tData[thread]->linkLines2 = tData[thread]->linkLines;
        tData[thread]->linkLines = tmp;

        tmp = tData[thread]->linkColors2;
        tData[thread]->linkColors2 = tData[thread]->linkColors;
        tData[thread]->linkColors = tmp;

        tData[thread]->numLinkLines2 = tData[thread]->numLinkLines;
        tData[thread]->numLinkLines = 0;
    }
}

/*
 * getexename - Get the filename of the currently running executable
 *
 * The getexename() function copies an absolute filename of the currently 
 * running executable to the array pointed to by buf, which is of length size.
 *
 * If the filename would require a buffer longer than size elements, NULL is
 * returned, and errno is set to ERANGE; an application should check for this
 * error, and allocate a larger buffer if necessary.
 *
 * Return value:
 * NULL on failure, with errno set accordingly, and buf on success. The 
 * contents of the array pointed to by buf is undefined on error.
 *
 * Notes:
 * This function is tested on Linux only. It relies on information supplied by
 * the /proc file system.
 * The returned filename points to the final executable loaded by the execve()
 * system call. In the case of scripts, the filename points to the script 
 * handler, not to the script.
 * The filename returned points to the actual exectuable and not a symlink.
 *
 */
static char* getexename(char* buf, size_t size)
{
#if !defined(_MINGW)
    char linkname[64]; /* /proc/<pid>/exe */
    pid_t pid;
    int ret;
    
    /* Get our PID and build the name of the link in /proc */
    pid = getpid();
    
    if (snprintf(linkname, sizeof(linkname), "/proc/%i/exe", pid) < 0)
    {
        return NULL;
    }

    
    /* Now read the symbolic link */
    ret = readlink(linkname, buf, size);
    
    /* In case of an error, leave the handling up to the caller */
    if (ret == -1)
        return NULL;
    
    /* Report insufficient buffer size */
    if (ret >= size)
    {
        return NULL;
    }
    
    /* Ensure proper NUL termination */
    buf[ret] = 0;
    
    return buf;
#else
    return NULL;
#endif
}

int initDataDir (char *binName)
{
    char *epath = malloc(MAXPATHLEN);
    char tmpStr[MAXPATHLEN];
    int i;
    struct stat st;

    dataDir = getexename (epath, MAXPATHLEN);

    if (!dataDir)
    {
        strcpy (epath, binName );

        dataDir = epath;
    }

    for (i = strlen(dataDir); i > 0; i--)
        if (dataDir[i] == '/')
            break;
    dataDir[i] = 0;

    if (i == 0)
        strcpy (dataDir, ".");

    strcpy (tmpStr, dataDir);

    for (i = strlen(tmpStr); i > 0; i--)
        if (tmpStr[i] == '/')
            break;
    tmpStr[i] = 0;

    strcat (tmpStr, "/share/plansch");

    if (stat (tmpStr, &st) == 0)
    {
        dataDir = strdup (tmpStr);
    }

    printf ("Data directory is %s\n", dataDir);

    return 1;
}

int needScreenUpdate(uint64_t tick)
{
    static uint32 lastScrUpdateTicks = 0;
    int rc =  0;

    if (GETVAL32(updateInterval))
    {
        if ((tick - lastScrUpdateTicks) > GETVAL32(updateInterval))
        {
            rc = 1;

            lastScrUpdateTicks = planschTime();
        }
    }

    return rc;
}

void initOptions (void)
{
    int i;

    options = calloc (1, sizeof (*options));

#ifdef USE_GPU
    options->useGpu = 1;
#endif
    options->drawMode = D_CYCLING;
    options->bigBang = 0;
    options->maxParticles = 0xffff;
    options->numThreads = 1;

    options->minZoom = 1;
    options->numThreads = 1;
    options->scrWid = 1024;
    options->scrHei = 768;

    options->clGlSharing = 1;
    options->maxGpuParticles[0] = 256*1024;
    for (i = 1; i < CL_NUM_PARTICLE_SETS; i++)
        options->maxGpuParticles[i] = 16 * 1024;

}


void parseArgs (int argc, char **argv)
{
    char c;
    int i;
    int wimpSet = 0;

    while ((c = getopt (argc, argv, "AOUo:SD:Cm:Xc:GI:T:L:l:bBn:N:hi:t:g:d:P:Hz:fru:Mp12")) != -1)
        switch (c)
        {
#ifdef USE_OPENGL
        case '1':
            haveOcl2 = 0;
            break;
        case '2':
            haveOcl2 = -1;
            break;
        case 'U':
            options->useGlCursor = 1;
            break;
#endif

        case 'o':
#ifdef USE_OPENCL
            clUseLocalmem (atoi(optarg));
#endif
            break;
        case 'p':
#ifdef USE_OPENCL
            clTryPinned (1);
#endif
            break;

#ifdef USE_OPENGL
        case 'O':
            oglDedicatedThreadSet (0);
            break;
#endif

            case 'A':
#ifdef USE_OPENCL
            clUseHalfField (1);
#endif
            break;

        case 'C':
           options->clGlSharing = 0;
           break;

        case 'D':
        {
            char *s = strtok (optarg, ",");
            if (s)
                options->usedClDevice = atoi(s);

            s = strtok (NULL, ",");
            if (s)
                options->usedClPlatform = atoi(s);
            break;
        }

        case 'm':
            /* ingored */
            break;

        case 'X':
            exit (10);
            break;

        case 'G':
            options->useGpu = 0;
            break;

        case 'z':
            options->minZoom = atoi(optarg);

            if ((options->minZoom < 1) || (options->minZoom > MAX_ZOOM) ||
                ((options->minZoom & (options->minZoom-1)) != 0))
            {
                fprintf (stderr, "bad zoom value\n");
                exit(1);
            }
            break;
        case 'f':
            options->fullscreen = 1;
            break;

        case 'H':
            options->noScreenOutput = 1;
            break;

        case 'L':
            if (strstr (optarg, "exit"))
                options->exitOnTapeEnd = 1;
            if (strstr (optarg, "imm"))
                options->loadTapeImm = 1;
            break;

        case 'l':
            options->loadFile = optarg;

            break;


#ifdef DR_PIXEL_OPTION
        case 'd':
            options->drawMode = atoi(optarg);
            break;
#endif
            
        case 'g':
            sscanf (optarg, "%dx%d", &options->scrWid, &options->scrHei);
            
            options->scrWid = options->scrWid & ~0x1f;
            break;
            
        case 'b':
            options->bigBang = 2;
            break;

        case 'B':
            options->bigBang = 1;
            break;

        case 'n':
            options->maxParticles = atoi (optarg);
            break;

        case 'N':
            if (wimpSet < CL_NUM_PARTICLE_SETS)
                options->maxGpuParticles[wimpSet++] = atoi (optarg);
            break;

        case 'i':
            GETVAL32(updateInterval) = atoi (optarg);
            break;

        case 't':
            options->numThreads = atoi (optarg);

            break;

        case 'c':
            options->cpuCoreBase = atoi(optarg);
            break;

        case 'r':
            options->randomPosition = 1;
            break;

        case 'T':
            for (i = 0; i < MAX_PDESCS; i++)
            {
                if (pdescs[i] == NULL)
                    break;

                if (!strcmp (optarg, pdescs[i]->name))
                {
                    printf ("Default particle type is %s\n", optarg);
                    ptype.defaultValue = i;
                    break;
                }
            }
            break;
        
        case 'M':
            options->maxLoops = atoi(optarg);
            break;

        case 'I':
            options->maxTime = atoi(optarg);
            break;
 
        case 'h':
        default:
            printf (
                "usage: plansch [-bBrf] [-m <mode>] [-n <num-particles>] [-i <msec>] [-t <threads>] [-g <geom>]\n"
                "               [-u <iters>] [-M <iters>] [-I <secs>]\n"
                "               [-l <fname>] [-L <opt1>,<opt2>,...]\n"
                "  -n     Maximum number of CPU particles (default is %d)\n"
                "  -N     Maximum number of GPU particles (default is %d)\n"
                "  -c          Starting CPU core\n"
                "  -G          Force not to use GPU\n"
                "  -T          Default particle type\n"
                "  -m          Which mode to run at (shell wrapper option)\n"
                "  -X     Check whether mode is supported and exit\n"
                "  -b     Big Bang with half of particles\n"
                "  -B     Big Bang with all particles\n"
                "  -i     Screen update interval in msec (0 = no update)\n"
                "  -t     Number of particle processing threads (1, 2, 4)\n"
                "  -r     Place all particles randomly on the screen, using a random charge\n"
                "         between -1 and 1\n"
                "  -H     No screen output\n"
#ifdef DR_PIXEL_OPTION
                "  -d     Particle drawing mode:\n"
                "         1: Trails\n"
                "         2: Dots\n"
#endif
                "  -g     Screen geometry in <width>x<height>\n"
                "  -f     Fullscreen mode\n"
                "  -S     Synchronize GPU thread with CPU particle engine.\n"
                "  -u     Particle field iterations per movement iterations (if -S switch is given)\n"
                "         Default: 1 (5 with GPU support)\n"
                "  -M     Number of iterations.\n"
                "  -I     Runtime in seconds\n"
                "  -l     Load .tape file at startup.\n"
                "  -L     Options for startup file:\n"
                "         exit - Exit program when tape is finished\n"
                "         imm  - Load tape contents immedeately\n"
                "  -C     OpenCl: Do not try to directly render into OpenGl textures/PBOs.\n"
                "         Make the data take a detour via host memory instead, which is MUCH\n"
                "         slower but sometimes required (e.g. for CPU OpenCl platforms ...\n"
                "  -D <i>[,<p>] OpenCl: Use the i-th available device on the p-th platform (default = 0,0)\n"
                "  -O     OpenGl/Cl: Run in main loop instead of spawning a dedicated thread\n"
                "  -p     OpenCL: Try to used pinned memory\n"
                "  -o <v> OpenCL: Force to use/not to use local memory\n"
                "  -A     OpenCL: Use half float (cl_khr_fp16 extension) for field calculation\n"
                "  -1     OpenCL: Force not to use OpenCL 2 code even if supported by device.\n"
                "  -2     OpenCL: Use OpenCL 2 code if supported by device.\n"
                "  -U     OpenGL: Use a cursor rendered by OpenGL\n"
                ,
                options->maxParticles,
                options->maxGpuParticles[0]
            );

            exit (0);
        }

        drawWithScreen.defaultValue = (options->clGlSharing == 0);
}

int
main (int argc, char **argv)
{
    int i;
    particle *p;
    uint32  curTick;
    uint32 endTime;
    int isIntro = 1;
    int scrUpdate;
    static uint32 measureTick = 0;
    static uint32 throttleTick = 0;

    printf ("PLANSCH Version %s [%s]\n", VERSION, VARIANT);
#ifdef GITVER
    printf ("        git: %s\n", GITVER);
#endif
    printf ("\nFuer Maxi\n\n");

    initOptions();

    if (!initDataDir(argv[0]))
        exit(1);
 
#if !defined(_MINGW) && defined(USE_OPENGL)
    XInitThreads();
#endif

#if 1
    printf ("particle: %d\n", (int)sizeof (particle));
    printf ("particle2: %d\n", (int)sizeof (particle2));
    printf ("si: %d\n", (int)sizeof (screeninfo));
#endif

    assert (sizeof (particle) == 64);
    assert (sizeof(value_u) <= sizeof(uint64_t));


#ifdef TTF_SUPPORT
    if (TTF_Init() == -1)
    {
        printf ("Failed to init TTF library\n");
    }
    else
    {
        char *fontpath = alloca(MAXPATHLEN);

        sprintf (fontpath, "%s/%s", dataDir, TTF_FONT);
        sdlFont = TTF_OpenFont(fontpath, 16);

        if (sdlFont == NULL)
        {
            sprintf (fontpath, "%s/../../%s", dataDir, TTF_FONT);
            sdlFont = TTF_OpenFont(fontpath, 16);

            if (sdlFont == NULL)
            {
                sdlFont = TTF_OpenFont(TTF_FONT, 16);

                if (sdlFont == NULL)
                    printf ("failed to open font: %s\n", TTF_GetError());
            }
        }
    }
#endif

#ifdef OPENMP
    numCores = omp_get_num_procs();
#endif


#ifdef USE_OPENGL
    oglDedicatedThreadSet (1);
#endif

    parseArgs (argc, argv);

    zoom = options->minZoom;

    if (options->numThreads > MAX_THREADS)
    {
        printf ("Max. %d threads supported\n", MAX_THREADS);
        options->numThreads = MAX_THREADS;
    }

    numParallel = numCores;
#ifdef OPENMP
    omp_set_num_threads (numCores+10);
#endif

    get_cpu_caps();

    printf ("SSE2:  %ssupported\n", have_sse2 ? "" : "not ");
    printf ("SSSE3: %ssupported\n", have_ssse3 ? "" : "not ");
#ifdef USE_SSE41
    printf ("SSE41: %ssupported\n", have_sse41 ? "" : "not ");
#else
    printf ("SSE41: %ssupported\n", have_sse41 ? "not compiled in but " : "not ");
#endif
    printf ("AVX:   %ssupported\n", have_avx ? "not used but " : "not ");

    mathInit ();

    memset (&brick, 0, sizeof(brick));
    brick.particleList = SCR_BRICK;

#if 0
    atexit (cleanup);
#endif

    initScreen (190, options->fullscreen);

    printf ("numThreads=%d numCores=%d\n", options->numThreads, numCores);

#ifdef INCLUDE_CCSH
    ccsh_init(NULL,NULL);
    THREAD_NAME("main");
    ccsh_spawn (ccsh_start_net, NULL, "netserver");
#endif

    cpuGravInit();
    printf ("cpuGravInit()\n");
    initPolys();
    printf ("initPolys()\n");
    initGlobals();
    printf ("initGlobals()\n");
    threadsInit ();
    printf ("threadsInit ()\n");
    initParticles();
    printf ("initParticles()\n");
    initParticleCq();
    printf ("initParticleCq()\n");
    initBackStore();
    printf ("initBackStore()\n");
    initSettings();
    printf ("initSettings()\n");
    initStencil();
    printf ("initStencil()\n");
    initMpTemplates();
    printf ("initMpTemplates()\n");
    linesInit();
    printf ("linesInit()\n");
    markInit();
    printf ("markInit()\n");
    makeButtons ();
    printf ("makeButtons ()\n");
    initParticleTypes();
    initParticleTypes();
    moveInit();
    moveInit();
    makeNormals (1, 1, wid - 1, hei - 1, NORM_SIMPLE_SAVE);
    makeNormals (1, 1, wid - 1, hei - 1, NORM_SIMPLE_SAVE);

    printf ("Init complete\n");

    planschInitialized = 1;

#ifdef USE_OPENGL
    {
            void *r = scrRect (0, INTERNC(hei), INTERNC(wid), INTERNC(options->scrHei-hei), 0x40909000);

        polyChangeColor (r, 2, 0x40404000);
        polyChangeColor (r, 3, 0x40404000);

        polyChangeFlags (r, POLY_CONVEX|POLY_FILLED, POLY_CONVEX|POLY_FILLED);
    }
#endif
    if (options->randomPosition)
    {
        for (i = 0; i < options->maxParticles; i++)
        {
            if ((p = newParticle (FRND(INTERNC(wid)), FRND(INTERNC(hei)), 0, 0)) != NULL)
            {
                SET_CHARGE(p, (int)(FRND(4)-2));
            }
        }

    }
    else if (options->bigBang)
    {
        showGravity = 1;
        modifySetting (&particleAttraction, 1, 0);
        srand (0);
        for (i = 0; i < options->maxParticles / options->bigBang; i++)
        {
            double rad = FRND(2*M_PI);
            
            p = newParticle (INTERNC (wid/2) + (sin(rad) * FRND(GRID * 100.0)),
                             INTERNC (hei/2) + (cos(rad) * FRND(GRID * 100.0)),
                             0, 0);
            if (!p)
                break;

            SET_CHARGE(p, (int)(FRND(4)-2));
            p->mass = 1 + (int) (255.0 * (rand () / (RAND_MAX + 1.0)));
                
            p->vec.d.x =
                1 + (int) (1000.0 * (rand () / (RAND_MAX + 1.0))) - 500;
            p->vec.d.y =
                1 + (int) (1000.0 * (rand () / (RAND_MAX + 1.0))) - 500;
        }


        gravVec.d.x = gravVec.d.y = 0;
    }
    partCount = 0;
    runSecs = 0;


    dflCursor = SDL_GetCursor ();
    emptyCursor = makeEmptyCursor ();

    /* default button selections */
    drawOpSelect (DO_WALL);
    selectStencil (10);
    selectStencil (ST_BALL);
    setDrawMode (DR_DRAW);
    applyDefaultSettings();

    redrawScreen();

    endTime = 0;
    if (options->maxTime)
        endTime = planschTime() + options->maxTime * 1000;

    startParticleMovement (1);

    if (options->loadFile)
    {
        if (tapeLoad (&playbackTape, options->loadFile))
        {
            if (options->loadTapeImm)
            {
                playbackAll (&playbackTape, NULL, NULL, 0, 0, 0xffffffff);
                eraseTape (&playbackTape);
            }
            else
            {
                startPlayback (1);
            }
        }
        else
        {
            isIntro = 0;
        }
    }

    /* main loop */
    while (!exitButton)
    {
        systemTimeTick();

        /* show status
         */
        status();

        /* handle keyboard/mouse input 
         */
        exitButton = handleInput();
 
        scrUpdate = 0;

        /* create particles
         */
        createAsyncParticles();

        /* Process the callout queue
         */
        processCalloutQueue();

        /* Process running playback
         */
        playbackIter(0);

        /* Process particle callout queue
         */
        processParticleCq();

        /* continuous draw while button pressed
         */
        if (mouseButtonPressed(1) && (getDrawFct() == DO_PARTICLES) && !pdescs[GETVAL32(ptype)]->noContDraw)
            drawOperation (iPositionLast.d.x, iPositionLast.d.y, MKEVENT(BUTTON_MOVE, 1));

        /* continuous grab
         */
        if (getDrawFct() == DO_MOVE)
            handleMove (iPositionLast.d.x, iPositionLast.d.y);

        /* get current time
         */
        curTick = planschTime();

        /* global screen update flag
         */
        scrUpdate = needScreenUpdate (curTick);

        /* whether to measure statistics in this loop
         */
        doMeasure = expired (&measureTick, 1000, curTick);

        /* update some border normals after walls have been changed
         */
        updateNormals (100);

        /* do screen paning
         */
        doPan (NULL, 0);

        /* limit engine speed to a fixed FPS value
         */
        if (isThrottled())
        {
            startParticleMovement (expired (&throttleTick, 1000-GETVAL32(maxFps), curTick));
        }

#ifdef USE_GPU
        /* Start GPU engine
         */
        if (options->useGpu)
        {
            gpuStart (scrUpdate);
        }
        else
#endif
        {
            /* calculate force field with CPU
             */
            if (getValue (&particleAttraction) /*&& !freezeParticles*/)
            {
                gravCalc (scrUpdate);
            }

#if !defined(USE_OPENGL) 
            /* cycle colors
             */
            if (scrUpdate)
            {
                cycleColors();
            }
#endif
        }

        /* CPU particle engine
         */
        moveParticles (scrUpdate);

        if (scrUpdate)
        {
            /* handle following specific particles
             */
            followParticles();

#if defined(USE_OPENGL) || defined(USE_OPENCL)
            /* wait for GPU to finish screen update
             */
            if (options->useGpu)
            {
                gpuWaitScreenUpdateDone();
            }

            /* swap the link lines buffer
             */
            swapLinkLines();
#endif
        }

#ifdef STATS
        if (probeButtonVal)
        {
            probe (probePosition.d.x, probePosition.d.y);
            probeHistory (statAreaX1, statAreaY1, statAreaX2 - statAreaX1, statAreaY2 - statAreaY1);
        }
#endif

        /* update the wall temperatures
         */
        updateWallTempr();

#ifdef USE_GPU
        if (options->useGpu)
        {
            gpuWait();
        }
        else
#endif
        {
            if (scrUpdate)
            {
                doUpdateScreen ();
            }
        }

        if (isIntro && playbackComplete())
        {
            eraseTape (&playbackTape);

            if (options->exitOnTapeEnd)
            {
                printf (" Pps: %ud %ud %ud\n", minPps, savedPpsCnt ? (uint)(totalPps / savedPpsCnt) : 0, maxPps);
                break;
            }
        }

        if ((options->maxLoops && (systemLoops() >= options->maxLoops)) ||
            (endTime && (curTick > endTime))
           )
        {
            printf (" Pps: %ud %ud %ud\n", minPps, savedPpsCnt ? (uint)(totalPps / savedPpsCnt) : 0, maxPps);
            break;
        }
    }

    SDL_UnlockSurface (screen);
    SDL_FreeSurface (screen);

    return 0;
}


        
void frandomize (FLOAT dx, FLOAT dy, FLOAT len, vec_t *out)
{
    FLOAT r1, r2;
    FLOAT rdx, rdy, rlen;

    r1 = ((50.0 * rand()) / (RAND_MAX + 1.0));
    r2 = ((101.0 * rand()) / (RAND_MAX + 1.0)) - 50.0;

    rdx = dx * r1 - dy * r2;
    rdy = dy * r1 + dx * r2;
    rlen = sqrt (rdx*rdx + rdy*rdy);

    out->d.x = (rdx * len) / rlen;
    out->d.y = (rdy * len) / rlen;
}



void
remgrav ()
{
    screeninfo *pSi = siArray;
    int        i;

    for (i = 0; i < wid*hei; i++, pSi++)
    {
        if (pSi->flags & SI_IS_PUMP)
            continue;
    }

    memset (grvArray[0], 0, wid * hei * sizeof(*grvArray[0]));
    memset (grvArray[1], 0, wid * hei * sizeof(*grvArray[1]));
}


FT_TYPE *scrFlagFieldAlloc()
{
    FT_TYPE *rc = malloc((wid*hei + 7) / 8);

    if (rc == NULL)
    {
        printf ("pflagAlloc failed\n");
        return NULL;
    }
    SIFLAG_CLRALL(rc);
    return rc;
}

FT_TYPE *flagFieldAlloc()
{
    FT_TYPE *rc = malloc((options->maxParticles + 7) / 8);

    if (rc == NULL)
    {
        printf ("pflagAlloc failed\n");
        return NULL;
    }
    FLAG_CLRALL(rc);
    return rc;
}
