/* Copyright (C) 2014 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/


#include "plansch.h"
#include "SDL_rotozoom.h"

int maxMpTemplates = 0;
int curMpTemplate = -1;
button *mptButton = NULL;


mPartTpl_t mpOne =
{
    .name = "One",
    .num = 1,
    .tbl = {
        {0.0f, 0.0f, 6, 0, {0,0,0,0,0,0}}
    }
};

mPartTpl_t mpTwo =
{
    .name = "Two",
    .num = 2,
    .tbl = {
        {-0.5f, 0.0f, 1, 1, {2,0,0,0,0,0}},
        { 0.5f, 0.0f, 1, 1, {1,0,0,0,0,0}}
    }
};

mPartTpl_t mpBucky =
{
    .name = "Bucky",
    .num = 7,
    .tbl = {
        {  0.0f,   -1.0f,  4, 3, {2,6,7,0,0,0}},
        {  0.86f,   -0.5f, 3, 3, {3,1,7,0,0,0}},
        {  0.86f,    0.5f, 3, 3, {4,2,7,0,0,0}},
        {  0.0f,    1.0f,  4, 3, {5,3,7,0,0,0}},
        { -0.86f,    0.5f, 3, 3, {6,4,7,0,0,0}},
        { -0.86f,   -0.5f, 3, 3, {1,5,7,0,0,0}},
        {  0.0f,    0.0f,  6, 6, {1,2,3,4,5,6}},
    }
};

mPartTpl_t mpIgel =
{
    .name = "Igel",
    .num = 7,
    .tbl = {
        {  0.0f,   -1.0f, 1, 1, {7,0,0,0,0,0}},
        {  0.86f,  -0.5f, 1, 1, {7,0,0,0,0,0}},
        {  0.86f,   0.5f, 1, 1, {7,0,0,0,0,0}},
        {  0.0f,    1.0f, 1, 1, {7,0,0,0,0,0}},
        { -0.86f,   0.5f, 1, 1, {7,0,0,0,0,0}},
        { -0.86f,  -0.5f, 1, 1, {7,0,0,0,0,0}},
        {  0.0f,    0.0f, 6, 6, {1,2,3,4,5,6}},
    }
};

mPartTpl_t mpTriangle =
{
    .name = "Triangle",
    .num = 3,
    .tbl = {
        {  0.0f,    0.0f,  2, 2, {2,3,0,0,0,0}},
        { -1.0f,   0.75f,  2, 2, {1,3,0,0,0,0}},
        {  1.0f,   0.75f,  2, 2, {1,2,0,0,0,0}},
    }
};


mPartTpl_t *mpTemplates[] =
{
    &mpOne,
    &mpTwo,
    &mpBucky,
    &mpIgel,
    &mpTriangle,
    NULL
};

void initMpTemplates (void)
{
    int i, j, k;
    SDL_Rect rect;
    SDL_Surface *s;
    int x1, y1;
    int x2, y2;
    int o;
    uint32_t col;

    rect.x = rect.y = 0;
    rect.w = rect.h = 32;


    for (i = 0; mpTemplates[i] != NULL; i++)
    {
        s = SDL_CreateRGBSurface (SDL_SWSURFACE, 32, 32, 32, 0xff, 0xff00, 0xff0000, 0x000000);
        SDL_FillRect (s, &rect, 0);

        for (j = 0; j < mpTemplates[i]->num; j++)
        {
            x1 = 16 + (int)(mpTemplates[i]->tbl[j].ox * 16.0f);
            y1 = 16 + (int)(mpTemplates[i]->tbl[j].oy * 16.0f);

            for (k = 0; k < 6; k++)
            {
                o = mpTemplates[i]->tbl[j].linkTo[k] - 1;

                if (o < j)
                    continue;

                x2 = 16 + (int)(mpTemplates[i]->tbl[o].ox * 16.0f);
                y2 = 16 + (int)(mpTemplates[i]->tbl[o].oy * 16.0f);

                mylineColor (s, x1-1, y1, x2-1, y2, 0x7f00087f);
                mylineColor (s, x1+1, y1, x2+1, y2, 0x7f00087f);
                mylineColor (s, x1, y1-1, x2, y2-1, 0x7f00087f);
                mylineColor (s, x1, y1+1, x2, y2+1, 0x7f00087f);
                mylineColor (s, x1, y1, x2, y2, 0xff0010ff);
            }
        }

        for (j = 0; j < mpTemplates[i]->num; j++)
        {
            x1 = 16 + (int)(mpTemplates[i]->tbl[j].ox * 16.0f);
            y1 = 16 + (int)(mpTemplates[i]->tbl[j].oy * 16.0f);
            
            if (mpTemplates[i]->tbl[j].nLinks > mpTemplates[i]->tbl[j].usedLinks)
                col = 0xff00ffff;
            else
                col = 0xffffff00;
            
            SURFACE_SET32(s, x1, y1, 0);
            SURFACE_SET32(s, x1-1, y1, col);
            SURFACE_SET32(s, x1+1, y1, col);
            SURFACE_SET32(s, x1, y1-1, col);
            SURFACE_SET32(s, x1, y1+1, col);
            col &= 0xff7f7f7f;
            SURFACE_SET32(s, x1-1, y1-1, col);
            SURFACE_SET32(s, x1+1, y1-1, col);
            SURFACE_SET32(s, x1-1, y1+1, col);
            SURFACE_SET32(s, x1+1, y1+1, col);

        }

        mpTemplates[i]->s = s;
    }
    maxMpTemplates = i;
}

void initMpSelector (button *b)
{
    button *sb;
    int i;

    for (i = 0; mpTemplates[i] != NULL; i++)
    {
        sb = addSubButton (b, 32, 32, NULL, mpTemplates[i]->name, i, ((i % 7) == 0) ? B_NEXTRC : B_NEXT);

        if (sb)
            setButtonSurface (sb, mpTemplates[i]->s);
    }
    mptButton = b;
}

mPartTpl_t *mptFind (char *name)
{
    int i;
    
    for (i = 0; mpTemplates[i] != NULL; i++)
    {
        if (!strcmp (mpTemplates[i]->name, name))
            return mpTemplates[i];
    }
    return NULL;
}

void setMpTemplate (int index)
{
    if (index >= maxMpTemplates)
        return;
    
    curMpTemplate = index;
    if ((index >= 0) && mptButton)
    {
        setButtonSurface (mptButton, mpTemplates[index]->s);
        redrawButton (mptButton);
    }
}

mPartTpl_t *getMpTemplate(void)
{
    if (curMpTemplate <= 0)
        return NULL;
    
    return mpTemplates[curMpTemplate];
}
    
