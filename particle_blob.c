
/*
Copyright (C) 2008 Felix Schmidt.

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, see <http://www.gnu.org/licenses/>.
*/

#include "plansch.h"

#define __USE_GNU
#include "pthread.h"

#include "sse_mathfuni.c"


static int oldBlobContinueMode = 0;
static int blobContinueMode = 0;

char tmpStr[256];

#define CANSHARE

/* onf of three sin/cos calculation methods */
#undef USE_SINCOS_TABLE
#define USE_SINCOS_SSE
#undef USE_SINCOS_PLAIN



#define ROT_INTERVAL 1


#define MAX_BLOBS   10000

#define BLOB(p)        ((blobParticle*)(p->pPtr))->pBlob

#define IS_BORDER(p)        PFLAG1(p)
#define SET_BORDER(p)        PFLAG1_SET(p)
#define CLR_BORDER(p)        PFLAG1_CLR(p)

#define AT_WALL(p)        PFLAG2(p)
#define SET_AT_WALL(p)        PFLAG2_SET(p)
#define CLR_AT_WALL(p)        PFLAG2_CLR(p)

SETTING(rotSpeed) =
{
    .minValue = -100,
    .maxValue = 100,
    .defaultValue = 0,
    .minDiff = 1,
    .partAttr = 1,
    .canBeRandom = 1,
    .name = "Rotation Speed"
};

SETTING(hardness) =
{
    .minValue = 10,
    .maxValue = 999,
    .defaultValue = 750,
    .minDiff = 1,
    .partAttr = 1,
    .name = "Hardness"
};

SETTING(hardness2) =
{
    .minValue = 2,
    .maxValue = 1000,
    .defaultValue = 8,
    .minDiff = 1,
    .partAttr = 1,
    .name = "Inner strain 2"
};

typedef struct
{
    struct blob *pBlob;
    particle *p;        /* prticle pointer */
    vec_t blobPos;        /* blob-relative posision */
    float rad;
    float vecl;
    float imp;
} blobParticle;

typedef struct blob
{
    blobParticle *particleTbl;

    float aSpeed;
    float rot;
    float eSpeed;
    int rotSpeed;
    int rotCount;
    int doRot;
    int doAccel;

    int mass;
    vec_t center;
    int numParticles;
    int flags;
    int lastBlobIteration;
    int updated;
    vec_t newBlobVec, curBlobVec;
    int active;
    int hardness;

    struct blob *parent;
    struct blob *sibling;
    struct blob *children;
    vec_t parentOffset;
} blobData;

static int numBlobs = 0;
static blobData *blobList[MAX_BLOBS];
static blobParticle *tmpParticleTbl = NULL;

static blobData *currentBlob = NULL;
static int blobInitialized = 0;
static int myType;

static void recalBlob (blobData *pBlob);


void unlinkBlob (blobData *pBlob)
{
    blobData *b, *next, *prev;

    /* orphan children */
    for (b = pBlob->children; b != NULL; b = next)
    {
        next = b->sibling;

        b->parent = NULL;
        b->sibling = NULL;
    }

    if (!b)
        return;

    /* unlink from parent's children list 
     */
    if (b->parent)
    {
        prev = NULL;
        for (b = b->parent->children; b != NULL; b = b->sibling)
        {
            if (b == pBlob)
            {
                if (prev == NULL)
                {
                    b->parent->children = NULL;
                }
                else
                {
                    prev->sibling = pBlob->sibling;
                }
                break;
            }
            prev = b;
        }
    }

    pBlob->parent = NULL;
    pBlob->sibling = NULL;
    pBlob->children = NULL;
}

void linkBlobTo (blobData *pBlob, blobData *parent)
{
    unlinkBlob (pBlob);

    pBlob->parent = parent;
    pBlob->sibling = parent->sibling;
    parent->children = pBlob;

    pBlob->parentOffset.d.x = pBlob->center.d.x - parent->center.d.x;
    pBlob->parentOffset.d.y = pBlob->center.d.y - parent->center.d.y;

}

static void initBlobs (int type)
{
    int i;

    myType = type;

    currentBlob = NULL;

    /* release memory of existing blobs (if re-initializing) */
    for (i = 0; i < numBlobs; i++)
    {
        if (blobList[i]->particleTbl && (blobList[i]->particleTbl != tmpParticleTbl))
        {
            free (blobList[i]->particleTbl);
        }
        free (blobList[i]);
        blobList[i] = NULL;
    }

    numBlobs = 0;

    if (tmpParticleTbl == NULL)
    {
        tmpParticleTbl = malloc(options->maxParticles * sizeof(*tmpParticleTbl));

        if (tmpParticleTbl == NULL)
        {
            printf ("initBlobs failed to allocate %d bytes\n", (int)(options->maxParticles * sizeof(*tmpParticleTbl)));
            return;
        }
    }

    blobInitialized = 1;
}

void finishCurrentBlob(void)
{
    int i;
    int ptsize;

    if (currentBlob == NULL)
        return;

    if (currentBlob->numParticles == 0)
    {
        goto cleanExit;
    }

    /* size of particle table. Add 4 since SSE sin/cos calculation probably accesses up to 3
     * elements after the last one .. thanks to valgrind.
     */
    ptsize = (currentBlob->numParticles + 4) * sizeof(blobParticle);

    /* allocate properly size table, copy it and re-initialize pointers */
    currentBlob->particleTbl = malloc(ptsize);

    if (currentBlob->particleTbl == NULL)
    {
        printf ("finishCurrentBlob: failed to allocate %d bytes\n", ptsize);
        goto cleanExit;
    }

    /* copy particle data */
    memcpy (currentBlob->particleTbl, tmpParticleTbl, currentBlob->numParticles * sizeof(blobParticle));

    /* reinhitialize pointers from particle struct to blob particle struct
     */
    for (i = 0; i < currentBlob->numParticles; i++)
    {
        currentBlob->particleTbl[i].p->pPtr = (void*)&currentBlob->particleTbl[i];
    }

    recalBlob (currentBlob);
    currentBlob->active = 1;

    blobList[numBlobs++] = currentBlob;

    currentBlob = NULL;

    return;

cleanExit:
    if (currentBlob->particleTbl && (currentBlob->particleTbl != tmpParticleTbl))
        free (currentBlob->particleTbl);

    free (currentBlob);
    currentBlob = NULL;
    return;
}


static int selectNewBlob ()
{
    finishCurrentBlob();

    currentBlob = (void*)malloc(sizeof(*currentBlob));

    if (currentBlob == NULL)
        return 0;

    memset ((char*)currentBlob, 0, sizeof(*currentBlob));

    currentBlob->aSpeed = 0.0;
    currentBlob->rotSpeed = ROT_INTERVAL;
    currentBlob->rotCount = currentBlob->rotSpeed;

    currentBlob->doAccel = getValue (&rotSpeed);
    currentBlob->eSpeed = (2.0 * M_PI) / 360.0 * ((float)currentBlob->doAccel / 100.0);

    currentBlob->hardness = getValue (&hardness);

    currentBlob->doRot = 1;

    /* use static particle table able to hold maxParticles. THe properly dimensioned 
     * table is allocated when the blob is finished
     */
    currentBlob->particleTbl = tmpParticleTbl;

    return 1;
}

static int blobRotDir (particle *p)
{
    blobParticle *blobPart = (blobParticle*)p->pPtr;
    blobData *pBlob;

    if (!blobPart)
            return 0;

    pBlob = blobPart->pBlob;

    if (!pBlob)
        return 0;

    return (int)((pBlob->rot * 360.0) / (2.0*M_PI));
}

static void blobDrawStart ()
{
    if (blobContinueMode)
        return;

    selectNewBlob();
}

static void blobDrawStop ()
{
    if (blobContinueMode)
        return;

    finishCurrentBlob();
}

static int canAddToBlob (int ix, int iy)
{
#ifdef SI_BLOB_COUNT
    int x = SCREENC(ix);
    int y = SCREENC(iy);
#endif

    blobData *pBlob;

    if (numBlobs >= MAX_BLOBS)
        return 0;

    if (currentBlob == NULL)
    {
        selectNewBlob();

        if (currentBlob == NULL)
            return 0;
    }

    pBlob = currentBlob;

#ifdef SI_BLOB_COUNT
    if (siArray[x + y*wid].b && siArray[x + y*wid].b != pBlob)
    {
        return 0;
    }
#endif


    if (pBlob->numParticles >= options->maxParticles)
    {
        return 0;
    }

    return 1;
}

static void blobBorders (blobData *pBlob)
{
    int i, j;
    int x, y;
    int isBorder;
    screeninfo *pSi;
    blobParticle *blobPart;

    /* mark all pixels with a blob particle */
    blobPart = pBlob->particleTbl;
    for (i = 0; i < pBlob->numParticles; i++, blobPart++)
    {
        x = pBlob->center.d.x + blobPart->blobPos.d.x;
        y = pBlob->center.d.y + blobPart->blobPos.d.y;

        x = SCREENC(x);
        y = SCREENC(y);

        if (x < 0)
            x = 0;
        else if (x >= wid)
            x = wid-1;

        if (y < 0)
            y = 0;
        else if (y >= hei)
            y = hei;

        pSi = &siArray[x + y*wid];

        pSi->flags |= SI_MARKER;
    }

    /* check each blob pixel whether it has at least one neighbour pixel not marked
     */
    blobPart = pBlob->particleTbl;
    for (i = 0; i < pBlob->numParticles; i++, blobPart++)
    {
        isBorder = 0;

        x = pBlob->center.d.x + blobPart->blobPos.d.x;
        y = pBlob->center.d.y + blobPart->blobPos.d.y;
        x = SCREENC(x);
        y = SCREENC(y);

        if (x < 0)
            x = 0;
        else if (x >= wid)
            x = wid-1;

        if (y < 0)
            y = 0;
        else if (y >= hei)
            y = hei;

        pSi = &siArray[x + y*wid];

        for (j = 0; j < 8; j++)
        {
            if ((SI_DIR(pSi, j)->flags & SI_MARKER) == 0)
            {
                isBorder = 1;
                break;
            }
        }

        if (isBorder)
        {
            SET_BORDER(blobPart->p);
        }
        else
        {
            CLR_BORDER(blobPart->p);
        }
        /* XXX set color */
    }


    /* clear markers
     */
    blobPart = pBlob->particleTbl;
    for (i = 0; i < pBlob->numParticles; i++, blobPart++)
    {
        x = pBlob->center.d.x + blobPart->blobPos.d.x;
        y = pBlob->center.d.y + blobPart->blobPos.d.y;
        x = SCREENC(x);
        y = SCREENC(y);

        if (x < 0)
            x = 0;
        else if (x >= wid)
            x = wid-1;

        if (y < 0)
            y = 0;
        else if (y >= hei)
            y = hei;

        pSi = &siArray[x + y*wid];

        pSi->flags &= ~SI_MARKER;
    }

}

#if 0
static int blobColor (particle *p)
{
    if (IS_BORDER(p))
    {
        return 3;
    }
    return -1;
}
#endif

static void recalBlob (blobData *pBlob)
{
    blobParticle *blobPart;
    llTuple center;
    int i;
    llTuple vm;
    float moi = 0.0;

    if (pBlob->numParticles == 0)
        return;

    /* recalculate blob center */

    center.d.x = 0;
    center.d.y = 0;
    vm.d.x = 0;
    vm.d.y = 0;
    pBlob->mass = 0;

    blobPart = pBlob->particleTbl;
    for (i = 0; i < pBlob->numParticles; i++, blobPart++)
    {
        center.d.x += (pBlob->center.d.x + blobPart->blobPos.d.x);
        center.d.y += (pBlob->center.d.y + blobPart->blobPos.d.y);
        vm.d.x += blobPart->p->vec.d.x;
        vm.d.y += blobPart->p->vec.d.y;
        pBlob->mass += blobPart->p->mass;
    }
    center.d.x /= i;
    center.d.y /= i;
    vm.d.x /= pBlob->numParticles;
    vm.d.y /= pBlob->numParticles;

    /* recalculate relative positions and blob's MOI (moment of inertia) of current particles .. */
    blobPart = pBlob->particleTbl;
    for (i = 0; i < pBlob->numParticles; i++, blobPart++)
    {
        blobPart->blobPos.d.x += pBlob->center.d.x - center.d.x;
        blobPart->blobPos.d.y += pBlob->center.d.y - center.d.y;

        moi += (float)blobPart->p->mass * ((float)blobPart->blobPos.d.x * (float)blobPart->blobPos.d.x +
                                         (float)blobPart->blobPos.d.y * (float)blobPart->blobPos.d.y);

        blobPart->p->vec.d.x = vm.d.x;
        blobPart->p->vec.d.y = vm.d.y;

    }

    pBlob->center.d.x = center.d.x;
    pBlob->center.d.y = center.d.y;

    blobPart = pBlob->particleTbl;
    for (i = 0; i < pBlob->numParticles; i++, blobPart++)
    {
        float r;

        if ((blobPart->blobPos.d.y == 0) && (blobPart->blobPos.d.x == 0))
        {
            blobPart->vecl = 0.0;
            blobPart->rad = 0.0;
            blobPart->imp = 0.0;
        }
        else
        {
            r = atan2 (-blobPart->blobPos.d.y, blobPart->blobPos.d.x);
            if (r < 0)
                r += 2 * M_PI;

            blobPart->rad = r;
            blobPart->vecl = VEC_LEN(blobPart->blobPos.d.x, blobPart->blobPos.d.y);

#define MOI_EXACT
#ifndef MOI_EXACT
            blobPart->imp = ((float)blobPart->p->mass / (float)pBlob->mass) * (blobPart->vecl / MAX_SPEEDF);
            blobPart->imp /= 500.0 * MAX_SPEEDF;
#else
            /* When the particle is hit with a force F we need to calculate the difference to the angular speed w, based
             * on the formula w' = D/moi, where D is F * dist[p].
             * dist[p] is the distance of the particle from the mass center, so we can precalculate dist[p]/moi here and 
             * hist have to multiply F later.
             * 10 is some constant ..
             */
            blobPart->imp = blobPart->vecl / (moi /* /1.0 */);
#endif
        }

    }

    pBlob->rot = 0.0;

    blobBorders (pBlob);
}


static int addToBlob (particle *p)
{
    blobData *pBlob;
    blobParticle *blobPart;
#ifdef SI_BLOB_COUNT
    screeninfo *pSi;
    int sx = SCREENC(p->pos.d.x), sy = SCREENC(p->pos.d.y);
#endif

    if (!canAddToBlob(p->pos.d.x, p->pos.d.y))
        return 0;

#ifdef SI_BLOB_COUNT
    pSi = &siArray[sx + sy*wid];
#endif

    pBlob = currentBlob;

    blobPart = &pBlob->particleTbl[pBlob->numParticles];
    blobPart->p = p;
    blobPart->pBlob = (struct blob*)pBlob;
    blobPart->blobPos.d.x = p->pos.d.x;
    blobPart->blobPos.d.y = p->pos.d.y;
         
    p->pPtr = (void*)blobPart;

    pBlob->numParticles++;

#ifdef SI_BLOB_COUNT
    pSi->b = pBlob;
    pSi->nb ++;
#endif

//    recalBlob (pBlob);

    return 1;
}

static void removeFromBlob (particle *p)
{
    int            i;
    blobParticle *blobPart = (blobParticle*)p->pPtr;
    blobData *pBlob;
#ifdef SI_BLOB_COUNT
    screeninfo *pSi;
    int sx = SCREENC(p->pos.d.x), sy = SCREENC(p->pos.d.y);
#endif

    if (!blobPart)
            return;

    pBlob = blobPart->pBlob;

    if (!pBlob)
        return;

    pBlob->numParticles--;

#ifdef SI_BLOB_COUNT
    pSi = &siArray[sx + sy*wid];
    pSi->b = pBlob;
    pSi->nb --;
    if (pSi->nb <= 0)
    {
        pSi->nb = 0;
        pSi->b = NULL;
    }
#endif

    if (pBlob->numParticles <= 0)
    {
#pragma omp critical
        {
            for (i = 0; i < numBlobs; i++)
            {
                if (blobList[i] == pBlob)
                {
                    blobList[i] = blobList[numBlobs-1];
                    numBlobs--;

                    if (pBlob->particleTbl && (pBlob->particleTbl != tmpParticleTbl))
                        free (pBlob->particleTbl);

                    free (pBlob);
                    break;
                }
            }
        }
        p->pPtr = NULL;
        return;
    }

    /* Overwrite particles entry in this blob with entry of last particle in blob
     */
    *blobPart = pBlob->particleTbl[pBlob->numParticles];
    blobPart->p->pPtr = (void*)blobPart;

    recalBlob (pBlob);
}

static void updateBlob (blobData *pBlob)
{
    int64_t cx, cy;
    vec_t vm;
    int i;
    particle *p;
#ifdef USE_SINCOS_SSE
//    __m128 mrad, msin, mcos, mvecl;
    float rad4[4] __attribute__((aligned(16)));
    int sin4[4] __attribute__((aligned(16)));
    int cos4[4] __attribute__((aligned(16)));
    float vecl4[4] __attribute__((aligned(16)));
#endif

    vm.d.x = vm.d.y = 0;
    for (i = 0; i < pBlob->numParticles; i++)
    {
        p = pBlob->particleTbl[i].p;

        vm.d.x += p->vec.d.x;
        vm.d.y += p->vec.d.y;
    }

    vm.d.x /= pBlob->numParticles;
    vm.d.y /= pBlob->numParticles;

    cx = cy = 0;

    for (i = 0; i < pBlob->numParticles; i++)
    {
        p = pBlob->particleTbl[i].p;

        p->vec.d.x = (p->vec.d.x*3 + vm.d.x) / 4;
        p->vec.d.y = (p->vec.d.y*3 + vm.d.y) / 4;

        cx += p->pos.d.x;
        cy += p->pos.d.y;
    }

    pBlob->center.d.x = cx / i;
    pBlob->center.d.y = cy / i;

    if (pBlob->doRot)
    {
        pBlob->rotCount --;

        if (pBlob->rotCount <= 0)
        {
            pBlob->rotCount = pBlob->rotSpeed;
            pBlob->rot += pBlob->aSpeed * pBlob->rotSpeed;

            if (pBlob->doAccel > 0)
            {
                if (pBlob->aSpeed < pBlob->eSpeed)
                    pBlob->aSpeed += (pBlob->eSpeed - pBlob->aSpeed) / 100.0; 
            }
            else if (pBlob->doAccel < 0)
            {
                if (pBlob->aSpeed > pBlob->eSpeed)
                    pBlob->aSpeed += (pBlob->eSpeed - pBlob->aSpeed) / 100.0; 
            }


#ifdef USE_SINCOS_SSE
            for (i = 0; i < pBlob->numParticles; i+=4)
            {
                rad4[0] = pBlob->rot + pBlob->particleTbl[i].rad;
                rad4[1] = pBlob->rot + pBlob->particleTbl[i+1].rad;
                rad4[2] = pBlob->rot + pBlob->particleTbl[i+2].rad;
                rad4[3] = pBlob->rot + pBlob->particleTbl[i+3].rad;

                vecl4[0] = pBlob->particleTbl[i].vecl;
                vecl4[1] = pBlob->particleTbl[i+1].vecl;
                vecl4[2] = pBlob->particleTbl[i+2].vecl;
                vecl4[3] = pBlob->particleTbl[i+3].vecl;

//                sincos_ps ((v4sf)_mm_load_si128((__m128i*)rad4), (v4sf*)sin4, (v4sf*)cos4, (v4sf)_mm_load_si128((__m128i*)vecl4));
                sincos_psi ((v4sf)_mm_load_si128((__m128i*)rad4), (v4si*)sin4, (v4si*)cos4, (v4sf)_mm_load_si128((__m128i*)vecl4));

                pBlob->particleTbl[i].blobPos.d.x = cos4[0];
                pBlob->particleTbl[i].blobPos.d.y = -sin4[0];
                pBlob->particleTbl[i+1].blobPos.d.x = cos4[1];
                pBlob->particleTbl[i+1].blobPos.d.y = -sin4[1];
                pBlob->particleTbl[i+2].blobPos.d.x = cos4[2];
                pBlob->particleTbl[i+2].blobPos.d.y = -sin4[2];
                pBlob->particleTbl[i+3].blobPos.d.x = cos4[3];
                pBlob->particleTbl[i+3].blobPos.d.y = -sin4[3];
            }
#else
            for (i = 0; i < pBlob->numParticles; i++)
            {
#ifdef USE_SINCOS_PLAIN
                pBlob->particleTbl[i].blobPos.d.x =
                        cos(pBlob->rot + pBlob->particleTbl[i].rad) * pBlob->particleTbl[i].vecl + 0.5;
                pBlob->particleTbl[i].blobPos.d.y =
                        -sin(pBlob->rot + pBlob->particleTbl[i].rad) * pBlob->particleTbl[i].vecl + 0.5;
#endif
#ifdef USE_SINCOS_TABLE
                int j = RAD2SCT(pBlob->rot + pBlob->particleTbl[i].rad);

                pBlob->particleTbl[i].blobPos.d.x = TBL_COS(j) * pBlob->particleTbl[i].vecl;
                pBlob->particleTbl[i].blobPos.d.y = TBL_NSIN(j) * pBlob->particleTbl[i].vecl;
#endif

            }
#endif

        }
    }
}

void updateBlobRange (int first, int last)
{
    int i;

    for (i = first; i < last; i++)
    {
        if (blobList[i]->active)
            updateBlob (blobList[i]);
    }
}

void updateAllBlobs (void)
{
    int perThread = numBlobs / numCores;
    int t;
    OMPVARS

    if (!blobContinueMode && oldBlobContinueMode)
        finishCurrentBlob();

    oldBlobContinueMode = blobContinueMode;

#pragma omp parallel for 
    for (t = 0; t < numParallel; t++)
    {
        OMP_THREAD_NAME("blob", t);
        updateBlobRange (t * perThread, (t == numCores-1) ? numBlobs : (t+1) * perThread);
    }
    OMPDONE
}

static void blobApplySetting (particle *p, setting *s)
{
    blobParticle *blobPart = (blobParticle*)p->pPtr;
    blobData *pBlob;

    if (!blobPart)
            return;

    pBlob = blobPart->pBlob;

    if (!pBlob)
        return;

    if (s == &rotSpeed)
    {
        pBlob->doAccel = getValue(&rotSpeed);
        pBlob->eSpeed = (2.0 * M_PI) / 360.0 * ((float)pBlob->doAccel / 100.0);
    }
    else if (s == &hardness)
    {
        pBlob->hardness = getValue (s);
    }

}


/* add impuls to a particle which is part of a blob 
 */
void hitBlobParticle (particle *p, int hx, int hy )
{
    float hlen;
    float hxn, hyn;
    float nx, ny;
    blobParticle *blobPart = (blobParticle*)p->pPtr;
    blobData *pBlob;
    float imp;
    float rFrac;
    int mass;

    if ((blobPart == NULL) || ((hx == 0) && (hy == 0)))
        return;

    pBlob = blobPart->pBlob;

    if (!pBlob->active)
        return;


    mass = p->mass;

    if (blobPart->blobPos.d.x || blobPart->blobPos.d.y)
    {
        /* the length of the impuls vector 
         */
        hlen = VEC_LEN(hx, hy);

        /* normlize impuls vector 
         */
        hxn = (float)hx / hlen;
        hyn = (float)hy / hlen;

        hlen *= mass;

        nx = (float)blobPart->blobPos.d.x / blobPart->vecl;
        ny = (float)blobPart->blobPos.d.y / blobPart->vecl;
        
        /* now we need the part of the force which applies in the direction of the tangent (of the
         * circle around the blob center) at the point  where the force is applied.
         */
        rFrac = hxn * ny - hyn * nx;

#ifndef MOI_EXACT
        imp = rFrac * hlen * blobPart->imp ;
#else
        /* the difference in angular speed w' is D/J, where D is force times distance from center of
         * rotation, J is the moment of inertia of the body. dist/J has been precalculated, so just multiply
         * the force
         */
        imp = rFrac * hlen * blobPart->imp;
#endif

        if (imp > 0.00001)
            imp = 0.00001;
        else if (imp < -0.00001)
            imp = -0.00001;

        pBlob->doRot = 1;
        pBlob->rotSpeed = ROT_INTERVAL;
        
        pBlob->aSpeed += imp;

#if 0
        /* limit speed .. */
        if (pBlob->aSpeed > (2*M_PI)/10.0)
            pBlob->aSpeed *= 0.99;
        else if (pBlob->aSpeed < -(2*M_PI)/10.0)
            pBlob->aSpeed *= 0.99;
#endif

    
#if 1
        /* Apply what's left of the impuls to the particle's vector.
         * This will later be applied to the whole blob (in updateBlob)
         */
        rFrac = (1.0 - fabs(rFrac) / (float)pBlob->numParticles);
        if (rFrac < 0.0)
            rFrac = 0.0;

        p->vec.d.x += (int)(((float)hx) * rFrac);
        p->vec.d.y += (int)(((float)hy) * rFrac);
#else
        p->vec.d.x += hx;
        p->vec.d.y += hy;
#endif
    }
    else
    {
        p->vec.d.x += hx;
        p->vec.d.y += hy;
    }


}

screeninfo *
moveBlob2 (particle *p, screeninfo *oldSi, threadData *td, int off, particle2 *pa2 )
{
    vec_t screenPos;
    vec_t screenPosOld;
    vec_t screenPosDiff;
    vec_t newPos;
    vec_t normVec;
    vec_t ttvec, tvec;
    particle *pp, *tpp;
    screeninfo *newSi;
    int cx, cy;
    int i;
    int dir;
    FLOAT vec_len;
    int64_t spq;
    int64_t minSpq;
#if 0
    int64_t minPpDistQ;
    int64_t curDistq;
#endif
    screeninfo *tmpSi;
    unsigned int pRad, ppRad;
    blobParticle *blobPart = (blobParticle*)p->pPtr;
    blobData *pBlob = blobPart->pBlob;
    vec_t newVec;
    vec_t tdiff;
    unsigned int minRad = GRID;
    vec_t wv;

    if (!pBlob->active)
    {
        return oldSi;
    }

    PART_GRAV (p, oldSi);

    V_TO_SCREEN(screenPosOld, p->pos);

#if 0
    tdiff.d.x = ((pBlob->center.d.x + blobPart->blobPos.d.x) - p->pos.d.x);
    tdiff.d.y = ((pBlob->center.d.y + blobPart->blobPos.d.y) - p->pos.d.y);

    tdiff.d.x = (tdiff.d.x * 50) / 100;
    tdiff.d.y = (tdiff.d.y * 50) / 100;

    hitBlobParticle (p, tdiff.d.x, tdiff.d.y);

    tvec = p->vec;
    spq = (int64_t)tvec.d.x * (int64_t)tvec.d.x + (int64_t)tvec.d.y * (int64_t)tvec.d.y;
    if (spq >= MAX_SPEEDQ )
    {
        vec_len = sqrt(spq);
        tvec.d.x = (((FLOAT)tvec.d.x / vec_len) * MAX_SPEEDF);
        tvec.d.y = (((FLOAT)tvec.d.y / vec_len) * MAX_SPEEDF);
    }
#else
    {
        tvec = p->vec;

        /* diff between current and supposed poition relative to blob center */
        tdiff.d.x = ((pBlob->center.d.x + blobPart->blobPos.d.x) - p->pos.d.x);
        tdiff.d.y = ((pBlob->center.d.y + blobPart->blobPos.d.y) - p->pos.d.y);

        /* impuls to get there */
        tdiff.d.x = (tdiff.d.x * pBlob->hardness) / 1000;
        tdiff.d.y = (tdiff.d.y * pBlob->hardness) / 1000;

        /* resulting vector */
        ttvec.d.x = tvec.d.x + tdiff.d.x;
        ttvec.d.y = tvec.d.y + tdiff.d.y;

        spq = (int64_t)ttvec.d.x * (int64_t)ttvec.d.x + (int64_t)ttvec.d.y * (int64_t)ttvec.d.y;
        if (spq >= MAX_SPEEDQ )
        {
            /* resulting vector is above maximum ..
            */
            spq = (int64_t)tvec.d.x * (int64_t)tvec.d.x + (int64_t)tvec.d.y * (int64_t)tvec.d.y;

            if (spq < MAX_SPEEDQ)
            {
                /* original vector is OK, so limit the additional impuls to get to proper location
                 */
                vec_len = VEC_LEN(tdiff.d.x, tdiff.d.y);
                tdiff.d.x = (((FLOAT)tdiff.d.x / vec_len) * (MAX_SPEEDF - sqrt(spq)));
                tdiff.d.y = (((FLOAT)tdiff.d.y / vec_len) * (MAX_SPEEDF - sqrt(spq)));

                tvec.d.x = tvec.d.x + tdiff.d.x;
                tvec.d.y = tvec.d.y + tdiff.d.y;

                p->vec.d.x += tdiff.d.x;
                p->vec.d.y += tdiff.d.y;
            }
            else
            {
                /* original impulse is too high, limit it and don't even try to obtain correct position
                 * now 
                 */
                vec_len = sqrt(spq);
                tvec.d.x = (((FLOAT)tvec.d.x / vec_len) * MAX_SPEEDF);
                tvec.d.y = (((FLOAT)tvec.d.y / vec_len) * MAX_SPEEDF);
            }
        }
        else
        {
            tvec = ttvec;

            p->vec.d.x += tdiff.d.x;
            p->vec.d.y += tdiff.d.y;
        }
    }
#endif

    if ((oldSi->flags & SI_LINE) && pHitLine (p, &wv, off, td, oldSi))
    {
        hitBlobParticle (p, wv.d.x - p->vec.d.x, wv.d.y - p->vec.d.y);
    }

    V_ADD(newPos, p->pos, tvec);

    V_TO_SCREEN(screenPos, newPos);
    V_SUB(screenPosDiff, screenPos, screenPosOld);

    dir = QUAD2DIR((screenPosDiff.d.x + 1) * 3 + screenPosDiff.d.y + 1);

    td->v.partCount++;
    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

    if (newSi->particleList == SCR_BRICK)
    {
        FLOAT cxf, cyf, vpf;

        td->v.wallHit++;

        /* get normal vector of current screen position (direction based) 
         *
         * This is fixed-point arithmetic, except vector length calculation.
         * Normals are stored in NORM_LEN units (+-127).
         *
         */
        /* normal vector in normVec */
        normVec.d.x = oldSi->normals[dir * 2];
        normVec.d.y = oldSi->normals[dir * 2 + 1];

#if 1
        /* wall hit */
        if (AT_WALL(p))
        {
            CLR_AT_WALL(p);

            /* still hitting a wall after reflection. */
            vec_len = VEC_LEN(p->vec.d.x, p->vec.d.y);

            newVec.d.x = (normVec.d.x * vec_len) / NORM_LEN;
            newVec.d.y = (normVec.d.y * vec_len) / NORM_LEN;

            hitBlobParticle (p, newVec.d.x - p->vec.d.x, newVec.d.y - p->vec.d.y);

            newPos.d.x = p->pos.d.x + p->vec.d.x;
            newPos.d.y = p->pos.d.y + p->vec.d.y;
            V_TO_SCREEN(screenPos, newPos);

            newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

            if (newSi == oldSi)
            {
                p->pos = newPos;
                return oldSi;
            }

            if (newSi->particleList == SCR_BRICK)
            {
                newPos = p->pos;
                toBorder (&newPos, &p->vec);
                p->pos = newPos;
                SET_AT_WALL(p);

                return oldSi;

            }


            goto move;
        }
#endif

        cxf = tvec.d.x;
        cyf = tvec.d.y;


        /* normalize */
        vec_len = VEC_LEN (cxf, cyf);

        cxf = (cxf * NORM_LEN) / vec_len;
        cyf = (cyf * NORM_LEN) / vec_len;


        if (FRICTION(newSi))
        {
            float vl;

            normVec.d.x = (normVec.d.x * (100 - FRICTION(newSi)) - cxf * FRICTION(newSi));
            normVec.d.y = (normVec.d.y * (100 - FRICTION(newSi)) - cyf * FRICTION(newSi));
            vl = VEC_LEN(normVec.d.x, normVec.d.y);
            normVec.d.x = (normVec.d.x * NORM_LEN) / vl;
            normVec.d.y = (normVec.d.y * NORM_LEN) / vl;
        }
        /* reflect at normal vector */
        /*        reflect (&cx, &cy, normVec.d.x, normVec.d.y);*/
        vpf = ((cxf * normVec.d.x) + (cyf * normVec.d.y));
        cxf = cxf - (2 * normVec.d.x * vpf) / (NORM_LEN*NORM_LEN);
        cyf = cyf - (2 * normVec.d.y * vpf) / (NORM_LEN*NORM_LEN);

        /* handle energy transfer between blob and wall.  */

        if (GETVAL32(wallTempr))
        {
            int t = (FLOAT)((vec_len - TEMPR(newSi))/2) * wallDamping;

            TEMPR(newSi) += t;

            if (t > 0.0)
            {
                vec_len -= t;

                newVec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                newVec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));
            }
            else
            {
                vec_t tmpvec;

                frandomize (cxf, cyf, -t, &tmpvec);

                newVec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
                newVec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));

                newVec.d.x += tmpvec.d.x;
                newVec.d.y += tmpvec.d.y;

            }

            /* Make sure perticle does not get too hot */
            spq = (QUAD_TYPE)newVec.d.x * (QUAD_TYPE)newVec.d.x + (QUAD_TYPE)newVec.d.y * (QUAD_TYPE)newVec.d.y;
            if (spq >= MAX_SPEEDQ)
            {
                vec_len = sqrt(spq);
                newVec.d.x = (((FLOAT)newVec.d.x * MAX_SPEEDF)) / vec_len;
                newVec.d.y = (((FLOAT)newVec.d.y * MAX_SPEEDF)) / vec_len;
            }
        }
        else
        {
            /* apply original impuls to reflection vector */
            vec_len *= wallDamping;

            newVec.d.x = (int) (((vec_len) * (FLOAT)cxf) / (FLOAT)(NORM_LEN));
            newVec.d.y = (int) (((vec_len) * (FLOAT)cyf) / (FLOAT)(NORM_LEN));
        }

        /* bring the particle exactly to the wall border in the direction of the current movement */
        newPos = p->pos;
        toBorder (&newPos, &p->vec);
        p->pos = newPos;
        SET_AT_WALL(p);

        hitBlobParticle (p, newVec.d.x - p->vec.d.x, newVec.d.y - p->vec.d.y);

        /* .. not leaving current pixel */
        return oldSi;

    }

    CLR_AT_WALL(p);

#ifdef SI_BLOB_COUNT
    if (newSi->b && newSi->b != pBlob)
    {
        for (pp = tmpSi->particleList; pp != NULL; pp = pp->next)
        {
            if (PTYPE(pp) == PTYPE(p))
                break;
        }

        hitBlobParticle (p, -p->vec.d.x, -p->vec.d.y);

        if (pp)
            hitBlobParticle (pp, -pp->vec.d.x, -pp->vec.d.y);

        return oldSi;

    }
#endif

    {
        pp = NULL;
        minSpq = MAX_SPEEDQ;
        pRad = MAX_RADIUS;

        tmpSi = newSi;
        for (i = -1; i < 8;)
        {
            if (tmpSi->particleList <= SCR_BRICK)
            {
                i++;
                tmpSi = SI_DIR(newSi, i);
                continue;
            }
            
            for (tpp = tmpSi->particleList; tpp != NULL; tpp = tpp->next)
            {
                int64_t ppDistQ;

#if 1
                if (tpp == p)
                    continue;


                if (PTYPE(tpp) == PTYPE(p))
                {
                    if (BLOB(tpp) == pBlob)
                        continue;

                    ppRad = MAX_RADIUS;
                }
                else if (!IS_BORDER(p))
                {
                    continue;
                }
                else
                {
                    ppRad = PART_RADIUS(tpp);
                }
#else
                if (tpp == p)
                    continue;
#endif

                ppDistQ = (QUAD_TYPE)(pRad + ppRad) * (QUAD_TYPE)(pRad + ppRad);

                cx = tpp->pos.d.x - newPos.d.x;
                cy = tpp->pos.d.y - newPos.d.y;

                spq = (int64_t)cx * (int64_t)cx + (int64_t )cy * (int64_t)cy;

                if (spq < ppDistQ)
                {
                    if (spq < minSpq)
                    {
                        minRad = ppRad;
                        minSpq = spq;
#if 0
                        minPpDistQ = ppDistQ;
#endif
                        pp = tpp;
                    }
                }
            }
            i++;
            tmpSi = SI_DIR(oldSi, i);
        }

        if (pp == NULL)
        {
            if (newSi != oldSi)
                goto move;
            
            p->pos = newPos;


            return oldSi;
        }

        ppRad = minRad;

        if (minSpq > 0)
        {
            FLOAT dlen;
            FLOAT dx, dy;
            FLOAT frc;

            FLOAT diffy = pp->pos.d.y - newPos.d.y;
            FLOAT diffx = pp->pos.d.x - newPos.d.x;
            FLOAT impx1, impy1, impx2, impy2;
            FLOAT off;

#if 0
            curDistq = (int64_t)(pp->pos.d.x - p->pos.d.x) * (pp->pos.d.x - p->pos.d.x) +
                       (int64_t)(pp->pos.d.y - p->pos.d.y) * (pp->pos.d.y - p->pos.d.y);
#endif

            dlen = minSpq;

            /* We need the relative mass difference of the two involved particles.
             * wp is in the range of 0..1, 0.5 for the case that the particles are of same
             * mass.
             */
            FLOAT wp = (FLOAT)(pp->mass) / (FLOAT)(p->mass + pp->mass);

            dlen = sqrt(dlen) + 0.5;

            dx = (FLOAT)diffx / dlen;
            dy = (FLOAT)diffy / dlen;

            td->v.collissions++;

            cx = pp->pos.d.x - p->pos.d.x;
            cy = pp->pos.d.y - p->pos.d.y;

            /* check whether the two particles move away from each other or approach eath other
             */
            if ((-cy)*(tvec.d.y - pp->vec.d.y) - (cx)*(tvec.d.x - pp->vec.d.x) < 0)
            {
                {
                    /* particle are approaching each other: do a collisison
                     * In the next iteration we will probably do the "else" path (move particles further
                     * away from each other if they are still within their collission area).
                     */
                    FLOAT wf;
                    
                    if (GETVAL32(preassure))
                    {
                        wf = ((FLOAT)(pRad + ppRad) / (dlen)) * ((float)GETVAL32(preassure)/1000.0);
                    }
                    else
                    {
                        wf = 0.0;
                    }

#if 0
                    if (curDistq > minPpDistQ)
                    {
                        newPos.d.x = p->pos.d.x;
                        newPos.d.y = p->pos.d.y;
                        screenPos = screenPosOld;
                    }
                    else
#endif
                    {
#if 1
                        newPos.d.x -= p->vec.d.x;
                        newPos.d.y -= p->vec.d.y;
#else

                        /* XXX immedeately do the "squeeze-out", 
                         */
                        off = (((dlen - (pRad + ppRad))) * wp );

                        newPos.d.x += (dx) * off;
                        newPos.d.y += (dy) * off;
#endif

                        screenPos.d.x = SCREENC (newPos.d.x);
                        screenPos.d.y = SCREENC (newPos.d.y);
                    }

                    frc = (p->vec.d.x * dx + p->vec.d.y * dy);
                    impx1 = frc * dx;
                    impy1 = frc * dy;

                    dx = -dx;
                    dy = -dy;

                    frc = (pp->vec.d.x * dx + pp->vec.d.y * dy);
                    impx2 = frc * dx;
                    impy2 = frc * dy;

                    /* adjust both particle's vectors, based on
                     * - The impuls resulting from the collission, adjusted by the relative weight
                     *   of the two particles
                     * - Constant collLoss, which can be adjusted so that particles lose energy
                     *   when colliding.
                     * - "push-out" direction times 1/(relative weight) times perticle preassure:
                     *   Heavy particle gets less impuls from light particle and vice versa.
                     */
                    wp *= 2;

                    hitBlobParticle (p, ((impx2 - impx1)*wp) + dx * wf * wp, ((impy2 - impy1)*wp) + dy * wf * wp);

                    wp = 2 - wp;

                    pdescs[PTYPE(pp)]->hit (pp, (int)(((impx1 - impx2)*wp) - dx * wf * wp), (int)(((impy1 - impy2)*wp) - dy * wf * wp));

                    mergeParticleColors (p, pp);

                }
            }
            else
            {
#if 0
//                        newPos = p->pos;
//                        screenPos = screenPosOld;
#else
 
                /* particles move away from each other:
                 * move particles in the direction of the collisison radis, but dont actually do a collission
                 */
                FLOAT wf;
                if (GETVAL32(preassure))
                {
                    wf = ((FLOAT)(pRad + ppRad) / (dlen)) * ((float)GETVAL32(preassure)/1000.0);
                }
                else
                {
                    wf = 0.0;
                }
                /* when two particles collide, the distance will temporarily be smaller than allowed (i.e. one
                 * particle is inside the other. off is the distance we need to move the particle
                 * "out" so that it is exatly at the other particle's border.
                 *
                 * Divide by 8 so that the particle is "pushed out" gradually.
                 * Multiply by wp so that (relativly) heavy particles don't get pushed out as fast.
                 */
                off = (((dlen - (pRad + ppRad))) * wp );

                /* push out the particle without changing the impuls */
                newPos.d.x += (dx) * off;
                newPos.d.y += (dy) * off;

                screenPos.d.x = SCREENC (newPos.d.x);
                screenPos.d.y = SCREENC (newPos.d.y);

                wp *= 2;

                hitBlobParticle (p, dx * wf * wp, dy * wf * wp);

                wp = 2 - wp;

                pdescs[PTYPE(pp)]->hit (pp, (int)(-dx * wf * wp), (int)(-dy * wf * wp));
#endif
            }
        }
    }


    newSi = &siArray[screenPos.d.x + screenPos.d.y * wid];

    if (newSi->particleList == SCR_BRICK)
    {
        return oldSi;
    }
    
move:

#ifdef SI_BLOB_COUNT
    if (newSi->b && newSi->b != pBlob)
    {
        return oldSi;
    }
    
    newSi->b = pBlob;
    newSi->nb++;
    oldSi->nb--;
    if (oldSi->nb <= 0)
    {
        oldSi->nb = 0;
        oldSi->b = NULL;
    }
#endif


    
#include "do_move.c"
}


/* compare two particles whether they belong to the same blob.
 * returns 0 if not (even if one is not associated to a blob), 
 * or 1 if it's the same blob
 * Assumes p1 and p2 are both blob types
 */
int blobCompare (particle *p1, particle *p2)
{
    blobParticle *blobPart1, *blobPart2; 

    blobPart1 = (blobParticle*)p1->pPtr;
    blobPart2 = (blobParticle*)p2->pPtr;

    if ((blobPart1 == NULL) || (blobPart2 == NULL))
        return 0;

    if (blobPart1->pBlob != blobPart2->pBlob)
        return 0;

    return 1;
}

int blobPlacementHint (void)
{
    return PHINT_FIXED;
}

static void blobWalk (particle *p, FUNCPTR cb, void *arg)
{
    blobParticle *blobPart = (blobParticle*)p->pPtr;
    blobData *pBlob;
    int i;

    if (!blobPart)
            return;

    pBlob = blobPart->pBlob;

    for (i = 0; i < pBlob->numParticles; i++)
    {
        cb (pBlob->particleTbl[i].p, arg);
    }
}

static int getNumLinks (particle *p)
{
    return MAX_LINKS;
}

pdesc MOD = {
    .moveParticle = moveBlob2,
    .name = "Blob",
    .descr = "",
    .setType = (FUNCPTR)addToBlob,
    .unsetType = (FUNCPTR)removeFromBlob,
    .startDraw = (FUNCPTR)blobDrawStart,
    .stopDraw = (FUNCPTR)blobDrawStop,
    .init = (FUNCPTR)initBlobs,
    .canCreate = (FUNCPTR)canAddToBlob,
    .hit = (FUNCPTR)hitBlobParticle,
    .getRot = (FUNCPTR)blobRotDir,
    .compare = (FUNCPTR)blobCompare,
    .applySetting = (FUNCPTR)blobApplySetting,
    .iteration = (FUNCPTR)updateAllBlobs,
    .placementHint = (FUNCPTR)blobPlacementHint,
    .walkSiblings = (FUNCPTR)blobWalk,
    .numLinks = (FUNCPTR)getNumLinks,
    .help = "blob",
    .noContDraw = 1,
    .isWimp = 0,

    .pm = blob_pm,
    .widgets = {
        {(FUNCPTR)slider, 32, 67, heavy_pm, "Particle Weight", 0, &particleWeight[0], B_NEXT },
        {(FUNCPTR)onOffGroupS, 32, 21, minus_pm, "Negative Charge", -1, &particleAttractionForce[0], B_NEXTRC },
        {(FUNCPTR)onOffGroupS, 32, 21, neutral_pm, "No Charge", 0, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffGroupS, 32, 21, plus_pm, "Positive Charge", 1, &particleAttractionForce[0], B_NEXT|B_SPC(1) },
        {(FUNCPTR)onOffButtonS, 32, 67, gravity_sign_pm, "GInvert", 0, &particleForceInv[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, psize_pm, "Particle Size", 0, &particleSize[0], B_NEXTRC },
        {(FUNCPTR)slider, 34, 67, lifetime_pm, "Particle Lifetime", 0, &particleLifetime[0], B_NEXTRC},

            {(FUNCPTR)onOffButtonS, 32, 32, connect_wimp_xpm, "Pair_with_WIMP", 0, &wimpPair[0], B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, rotate_pm, "Rotation Accel.", 0, &rotSpeed, B_NEXTRC },
        {(FUNCPTR)slider, 32, 67, hardness_xpm, "Inner Strain", 0, &hardness, B_NEXTRC },
//        {(FUNCPTR)slider, 32, 67, NULL, "Inner Strain 2", 0, &hardness2, B_NEXTRC },
        {(FUNCPTR)onOffButton, 32, 32, resume_pm, "blobContinueMode", (intptr_t)&blobContinueMode, NULL, B_NEXTRC },
        }
};
